/**************************************************************************************
***************************************************************************************
							Validaci�n de Datos de Ingreso
****************************************************************************************
***************************************************************************************/

/*
112 hasta el 123 son los Fn
09 = Tab 
11 = Home 
13 = Enter
32 = Space Bar	
33 = ! 
34 = "
35 = # 
36 = $ 
37 = %
38 = & 
39 = ' 
40 = (
41 = ) 
42 = * 
43 = +
44 = , 
45 = - 
46 = .
47 = / 
Del 48 hasta el 57 son los numero 0 - 9
58 = :
59 = ; 
60 = < 
61 = =
62 = > 
63 = ? 
64 = @
Del 65 hasta el 90 son las letras A - Z Mayusculas
91 = [
92 = 
93 = ] 
94 = ^
95 = - 
96 = ` 
Del 97 hasta el 122 son las letras a - z Minusculas
123 = { 
124 = |
125 = } 
126 = ~
*/

/*************************************************************************************/
//------------ validaciones correctos en varios navegadores
/*************************************************************************************/

document.oncontextmenu = function(){
	return false;
}



/*************************************************************************************/

function solo_texto(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "qwertyuiopasdfghjkl�zxcvbnm QWERTYUIOPASDFGHJKL�ZXCVBNM-�@.";
    especiales = [8,9,37,46,209,241,225,233,237,243,250,192,201,205,211,218,193,176];
	//241-->�
	//225
	//39-->'
 
    tecla_especial = false;
    for(var i in especiales){ if(key == especiales[i]){ tecla_especial = true; break; } }
 
    if(letras.indexOf(tecla)==-1 && !tecla_especial)
    return false;
}



function solo_numeros(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "0123456789";
    especiales = [8,37,46,9];
 
    tecla_especial = false;
    for(var i in especiales){ if(key == especiales[i]){ tecla_especial = true; break; } }
 
    if(letras.indexOf(tecla)==-1 && !tecla_especial)
    return false;
}

function solo_numeros_enter(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "0123456789";
    especiales = [13,8,37,46,9];
 
    tecla_especial = false;
    for(var i in especiales){ if(key == especiales[i]){ tecla_especial = true; break; } }
 
    if(letras.indexOf(tecla)==-1 && !tecla_especial)
    return false;
}



function solo_numeros_guion(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "-0123456789";
    especiales = [8,37,46];
 
    tecla_especial = false;
    for(var i in especiales){ if(key == especiales[i]){ tecla_especial = true; break; } }
 
    if(letras.indexOf(tecla)==-1 && !tecla_especial)
    return false;
}


function solo_texto_numero(e){
    key = e.keyCode || e.which;
    tecla = String.fromCharCode(key).toLowerCase();
    letras = "qwertyuiopasdfghjkl�zxcvbnm QWERTYUIOPASDFGHJKL�ZXCVBNM-�@.0123456789";
    especiales = [8,37,46,209,241,225,233,237,243,250,192,201,205,211,218,193,176];
	//241-->�
	//225
	//39-->'
 
    tecla_especial = false;
    for(var i in especiales){ if(key == especiales[i]){ tecla_especial = true; break; } }
 
    if(letras.indexOf(tecla)==-1 && !tecla_especial)
    return false;
}

/*************************************************************************************/
function vlx(e){
	if(e.ctrlKey && e.keyCode==86){return false;}
}


function precise_round(num,decimals) {
		return Math.round(num * Math.pow(10, decimals)) / Math.pow(10, decimals);
}
	
function validar_solo_numero(){
	if((event.keyCode >= 48 && event.keyCode <= 57) ) {
	}else{
		event.returnValue = false;	
	}
}

function validar_solo_numero_puntos(){
	if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode == 46)) {
	}else{
		event.returnValue = false;	
	}
}

function validar_solo_texto(){
	//(event.keyCode >= 65 && event.keyCode <= 90) //mayuscula
	//(event.keyCode >= 97 && event.keyCode <= 122)  //minuscula
	
	if ((event.keyCode >= 65 && event.keyCode <= 90) ||  (event.keyCode >= 97 && event.keyCode <= 122) || ( event.keyCode == 32)  || (event.keyCode >= 192 && event.keyCode <= 255) || (event.keyCode == 189) || (event.keyCode == 8) || (event.keyCode == 46)) {
	}else{
		event.returnValue = false;	
	}
}

function validar_solo_texto_numero(){
	if ((event.keyCode >= 45 && event.keyCode <= 90) ||  (event.keyCode >= 95 && event.keyCode <= 122) || ( event.keyCode == 32)  || (event.keyCode >= 192 && event.keyCode <= 255) ) {
	}else{
		event.returnValue = false;	
	}
}

function validar_telefono(){
	if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode == 40) || (event.keyCode == 41) || (event.keyCode == 45) || (event.keyCode == 20)) {
	}else{
		event.returnValue = false;	
	}
}

function validar_numero_guion(){
	if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode == 189) || (event.keyCode == 8) || (event.keyCode == 46) ) {
	}else{
		event.returnValue = false;	
	}
}

function valida_mun_decimal(num) { 
	re=/^0[.]{0,1}[0-9]{0,2}$/
    if(!re.exec(num))    {
        alert("El valor " + num + " es incorrecta.");
    }else{
        return true;
    }
} 

function valEmail(valor){
    re=/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/
    if(!re.exec(valor))    {
        alert("La direcci�n de email " + valor + " es incorrecta.");
    }else{
        return true;
    }
}




/**************************************************************************************
***************************************************************************************
							Utilitarios
****************************************************************************************
***************************************************************************************/

	function cambiar_color_over(celda){ 
	   celda.style.backgroundColor="#D4D4D4";
	} 
	
	function cambiar_color_out(celda){ 
	   
	  // celda.style.backgroundColor="#F4F4F4" 
	   celda.style.backgroundColor="#FFFFFF";
	} 
	

function maximaLongitud(texto,maxlong) {
	var tecla, in_value, out_value;

	if (texto.value.length > maxlong) {
		in_value = texto.value;
		out_value = in_value.substring(0,maxlong);
		texto.value = out_value;
		return false;
	}
	return true;
}

function Left(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}

function redondeo2decimales(numero){
	var original=parseFloat(numero);
	var result=Math.round(original*100)/100 ;
	return result;
}

//setTimeout(function(){ $(".mensaje").fadeOut(800).fadeIn(800).fadeOut(500).fadeIn(500).fadeOut(300);}, 3000);
//$("#mensaje").stop().animate({opacity: "show", top: "0"}, "slow");
//$("#mensaje").stop().animate({opacity: "hide", top: "-300"}, 1);
//$("#mensaje").fadeOut(800).fadeIn(800).fadeOut(500).fadeIn(500).fadeOut(300);

function mensagebox(box,contenido){
	$("#"+box+"").animate({left:"+=10px"}).animate({left:"-5000px"});
	$("#"+box+"").html('<center>'+contenido+'</center>');
	$("#"+box+"").fadeOut(800).fadeIn(800).fadeOut(800).fadeIn(800).fadeOut(800);
	$("#"+box+"").stop().animate({opacity: "hide", top: "-300"}, 1000);
}

function mensaje_resultado(div, clase, mensaje){
	//clase='exito'; 	mensaje="Se registro correctamente un registro.";
	//clase='info'; 	mensaje="Se actualizo correctamente un registro.";
	//clase='alerta'; mensaje="Se elimino un registro  correctamente.";
	//clase='error';	mensaje="No se registro correctamente.";
	
	$("#"+div+"").stop().animate({opacity: "show", top: "0"}, "slow");
	$("#"+div+"").removeClass(clase,1000);
	$("#"+div+"").html(mensaje).addClass(clase);
	$("#"+div+"").fadeIn(700).fadeOut(600).fadeIn(700).fadeOut(600);
	$("#"+div+"").stop().animate({opacity: "show", top: "0"}, "slow");
}

function mostrarmensaje(divmsg,tipo,msg){
	//div_mensaje = document.getElementById(divmsg);
	//div_mensaje.innerHTML = "<div class="+tipo+">" + msg + "</div>";
	$("#"+divmsg+"").stop().animate({opacity: "show", top: "0"}, "slow");	
	$("#"+divmsg+"").removeClass("error",1000);
	$("#"+divmsg+"").html(msg).addClass(tipo); 
}

function fcnjquery_AccionMensaje(opt) { 
	var myArray = new Array(2);
	if(opt==1){
		myArray[0]="exito";
		myArray[1] ="Se registro correctamente un registro.";
	}else if($opt==2){
		myArray[0]='info';
		myArray[1]="Se actualizo correctamente un registro.";
	}else if($opt==3){
		myArray[0]='alerta';
		myArray[1]="Se elimino un registro  correctamente.";
	}else if($opt==4){
		myArray[0]='error';
		myArray[1]="Este Registro ya existe en la Base de Datos.";
	}
	return myArray; 
}

//****************************************** validar fecha **************************************//

function esDigito(sChr){
var sCod = sChr.charCodeAt(0);
return ((sCod > 47) && (sCod < 58));
}

function valSep(oTxt){
var bOk = false;
bOk = bOk || ((oTxt.value.charAt(2) == "-") && (oTxt.value.charAt(5) == "-"));
//bOk = bOk || ((oTxt.value.charAt(2) == "/") && (oTxt.value.charAt(5) == "/"));
return bOk;
}

function finMes(oTxt){
var nMes = parseInt(oTxt.value.substr(3, 2), 10);
var nRes = 0;
switch (nMes){
case 1: nRes = 31; break;
case 2: nRes = 29; break;
case 3: nRes = 31; break;
case 4: nRes = 30; break;
case 5: nRes = 31; break;
case 6: nRes = 30; break;
case 7: nRes = 31; break;
case 8: nRes = 31; break;
case 9: nRes = 30; break;
case 10: nRes = 31; break;
case 11: nRes = 30; break;
case 12: nRes = 31; break;
}
return nRes;
}

function valDia(oTxt){
var bOk = false;
var nDia = parseInt(oTxt.value.substr(0, 2), 10);
bOk = bOk || ((nDia >= 1) && (nDia <= finMes(oTxt)));
return bOk;
}

function valMes(oTxt){
var bOk = false;
var nMes = parseInt(oTxt.value.substr(3, 2), 10);
bOk = bOk || ((nMes >= 1) && (nMes <= 12));
return bOk;
}

function valAno(oTxt){
var bOk = true;
var nAno = oTxt.value.substr(6);
bOk = bOk && ((nAno.length == 2) || (nAno.length == 4));
if (bOk){
for (var i = 0; i < nAno.length; i++){
bOk = bOk && esDigito(nAno.charAt(i));
}
}
return bOk;
}

function valFecha(oTxt){
var bOk = true;
if (oTxt.value != ""){
bOk = bOk && (valAno(oTxt));
bOk = bOk && (valMes(oTxt));
bOk = bOk && (valDia(oTxt));
bOk = bOk && (valSep(oTxt));
if (!bOk){
alert("Error. El formato de la fecha es dd-mm-aaaa");
oTxt.value = "";
oTxt.focus();
}
}
}



//-----------------------------------------------
//Validar el formato de la fecha:
//-----------------------------------------------

function validarFormatoFecha(campo) {
      var RegExPattern = /^\d{1,2}\-\d{1,2}\-\d{2,4}$/;
      if ((campo.match(RegExPattern)) && (campo!='')) {
            return true;
      } else {
            return false;
      }
}

//-----------------------------------------------
//Validar si la fecha introducida es real:
//-----------------------------------------------

function validarFecha(fecha){
		 //Funcion validarFecha 
		 //Escrita por Buzu feb 18 2010. (FELIZ CUMPLE BUZU!!!
		 //valida fecha en formato aaaa-mm-dd
		 var fechaArr = fecha.split('-');
		 var dia = fechaArr[0];
		 var mes = fechaArr[1];
		 var aho = fechaArr[2];
		 
		 var plantilla = new Date(aho, mes - 1, dia);//mes empieza de cero Enero = 0
		
		 if(!plantilla || plantilla.getFullYear() == aho && plantilla.getMonth() == mes -1 && plantilla.getDate() == dia){
		 return true;
		 }else{
		 return false;
		 }
}

function existeFecha(fecha) {
        var fechaf = fecha.split("-");
        var d = fechaf[0];
        var m = fechaf[1];
        var y = fechaf[2];
        return m > 0 && m < 13 && y > 0 && y < 32768 && d > 0 && d <= (new Date(y, m, 0)).getDate();
}

//--------------------------------------------------------------------
//Validar si la fecha introducida es anterior o menor a la actual:
//--------------------------------------------------------------------

function validarFechaMenorActual(date){
      var x=new Date();
      var fecha = date.split("-");
      x.setFullYear(fecha[2],fecha[1]-1,fecha[0]);
      var today = new Date();
 
      if (x >= today)
        return false;
      else
        return true;
}

//****************************************** validar fecha **************************************//


function validar_fechas_horas (fecha1,fecha2) {   
	var result='';
	
	n_fecha1=new Array();
	n_fecha2=new Array();
	
	n_fecha1 = fecha1.split('-');
	nDi0=n_fecha1[0];
	nMe0=n_fecha1[1];
	nAn0=n_fecha1[2];
	nHora0=n_fecha1[3];
	

	n_fecha2 = fecha2.split('-');
	nDi1=n_fecha2[0];
	nMe1=n_fecha2[1];
	nAn1=n_fecha2[2];
	nHora1=n_fecha2[3];

//alert("Fecha_Inicio:"+nAn0+","+nMe0+","+nDi0+","+nHora0+"===Fecha_Fin:"+nAn1+","+nMe1+","+nDi1+","+nHora1);


	var Fecha_Inicio = new Date(nAn0,nMe0,nDi0,nHora0)    
	var Fecha_Fin = new Date(nAn1,nMe1,nDi1,nHora1)     
	if(Fecha_Inicio > Fecha_Fin)    {      
		result=false;
	}else{
		result=true;
	}
	
	return result; 
}   
  
function DiferenciaFechas_horas (CadenaFecha1,CadenaFecha2) {   
	var myArray = new Array(4);
	
	Fecha1_AUX = CadenaFecha1.split("-"); 
	fecha_ini = new Date() 
	fecha_ini.setDate(Fecha1_AUX[0]); 
	fecha_ini.setMonth(eval(Fecha1_AUX[1])-1); 
	fecha_ini.setYear(Fecha1_AUX[2]); 
	fecha_ini.setHours(Fecha1_AUX[3]); 
	fecha_ini.setMinutes(0); 
	fecha_ini.setSeconds(0); 

	Fecha2_AUX = CadenaFecha2.split("-"); 
	fecha_fin = new Date() 
	fecha_fin.setDate(Fecha2_AUX[0]); 
	fecha_fin.setMonth(eval(Fecha2_AUX[1])-1); 
	fecha_fin.setYear(Fecha2_AUX[2]); 
	fecha_fin.setHours(Fecha2_AUX[3]); 
	fecha_fin.setMinutes(0); 
	fecha_fin.setSeconds(0); 

	num_dias = Math.floor((fecha_fin.getTime()/(1000 * 60 * 60 * 24))-(fecha_ini.getTime()/(1000 * 60 * 60 * 24))); 
	num_horas = Math.floor((fecha_fin.getTime()/(1000 * 60 * 60))-(fecha_ini.getTime()/(1000 * 60 * 60)));
	num_minutos = (fecha_fin.getTime()/(1000 * 60))-(fecha_ini.getTime()/(1000 * 60)); 
	num_segundos = (fecha_fin.getTime()/(1000))-(fecha_ini.getTime()/(1000)); 

    myArray[0]	=	num_dias;
	myArray[1] 	=	num_horas;
	myArray[2] 	=	num_minutos;
	myArray[3] 	=	num_segundos;
		
	return myArray; 
} 

function imprimir(que) {
	var ventana = window.open("../funciones/print.php","ventana","width=760");
	var contenido = "<html><head><style type='text/css'> @import url('../../../utils/css/stylesWebContenido.css');</style></head><body onload='window.print();window.close();'>" + document.getElementById(que).innerHTML + "</body></html>";
	ventana.document.open();
	ventana.document.write(contenido);
	ventana.document.close();
}

/*****************************   VALIDAR FECHA MEJORADO   ***********************************/

function Validar_fecha_new(obj_fecha){ 

    var Fecha= new String(obj_fecha.value)   // Crea un string   
    var RealFecha= new Date()   // Para sacar la fecha de hoy   
    // Cadena A�o   
    var Ano= new String(Fecha.substring(Fecha.lastIndexOf("-")+1,Fecha.length))   
    // Cadena Mes   
    var Mes= new String(Fecha.substring(Fecha.indexOf("-")+1,Fecha.lastIndexOf("-")))   
    // Cadena D�a   
    var Dia= new String(Fecha.substring(0,Fecha.indexOf("-")))   
  
    // Valido el a�o   
    if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){   
		alert('A\xf1o Inv\xe1lida')   
        obj_fecha.value = "";
		obj_fecha.focus();
        //return false     
    }   
    // Valido el Mes   
    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){   
      	alert('Mes inv\xe1lida') 
        obj_fecha.value = "";
		obj_fecha.focus();
        //return false     
    }   
    // Valido el Dia   
    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){   
     	alert('D\xeda inv\xe1lida') 
		obj_fecha.value = "";
		obj_fecha.focus();
        //return false   
    }   
    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {   
        if (Mes==2 && Dia > 28 || Dia>30) {   
           alert('D\xeda inv\xe1lida') 
            obj_fecha.value = "";
			obj_fecha.focus();
			//return false    
        }   
    }   
 
  //para que envie los datos, quitar las  2 lineas siguientes   
  //alert("Fecha correcta.")   
 //return false     
}  

function checkTodos (id,pID) {
				
 $( "#" + pID + " :checkbox").attr('checked', $('#' + id).is(':checked')); 
				   
}
/*
function caracter(valor){
	var nRes = '';
	switch (valor){
		case "�": nRes = "e1"; break;
		case "�": nRes = "e9"; break;
		case "�": nRes = "ed"; break;
		case "�": nRes = "f3"; break;
		case "�": nRes = "fa"; break;
		case "�": nRes = "c1"; break;
		case "�": nRes = "c9"; break;
		case "�": nRes = "cd"; break;
		case "�": nRes = "d3"; break;
		case "�": nRes = "da"; break;
		case "�": nRes = "b0"; break;
	}
	n_result="\x"+nRes;
}
*/
/************************************************/


function div_mostrar(div){
	$("#"+div+"").show();
}

function div_ocultar(div){
	$("#"+div+"").hide();
}

function div_limpiar(div){
	$("#"+div+"").val('');
}

/**************************************************************************************
***************************************************************************************
							Mostrar Cargas Loading
****************************************************************************************
***************************************************************************************/

function cargar_loading(div_id){
	$("#"+div_id+"").html('<img src="../webimages/iconos/loading_save.gif" />');
}

function cargar_loading_barra(div_id){
	$("#"+div_id+"").html('<img src="../webimages/iconos/loading_time.gif" />');
}

function cargar_contenido_x1(contenido){
	var html = '<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#004669"><tr><td>&nbsp;</td></tr><tr><td bgcolor="#FFFFFF" class="TABLE_border4"><center><p><img src="../webimages/iconos/ajax-loader.gif" width="125" height="123" /><br />&nbsp;&nbsp;&nbsp;<span class="arial_13_black_negrita">'+contenido+' </span><img src="../webimages/iconos/loadingpuntos01.gif" width="18" height="7" /><br></p></center></td></tr><tr><td>&nbsp;</td></tr></table>';
	return html;
}

function mostrarCargaLoading_SI(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#004669"><tr><td>&nbsp;</td></tr><tr><td bgcolor="#FFFFFF" class="TABLE_border4"><center><p><img src="../SGISBN/webimages/iconos/ajax-loader.gif" width="125" height="123" /><br />&nbsp;&nbsp;&nbsp;<span class="arial_13_black_negrita">'+contenido+' </span><img src="../SGISBN/webimages/iconos/loadingpuntos01.gif" width="18" height="7" /><br><br></p></center></td></tr><tr><td>&nbsp;</td></tr></table>');
}

function mostrarCargaLogin(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="99%" border="0" align="center" cellpadding="0" cellspacing="0" class="TABLE_border1" ><tr><td height="40" bgcolor="#FFFFFF"><div align="center"><table width="162" border="0" cellspacing="0" cellpadding="0"><tr><td width="54"><div align="center"><img src="webimages/iconos/load.gif" width="32" height="32"></div></td><td width="99" class="texto_arial_azul_12">'+contenido+' </td></tr></table></div></td></tr></table>');
}

function mostrarCargaContenido(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#004669"><tr><td>&nbsp;</td></tr><tr><td bgcolor="#FFFFFF" class="TABLE_border4"><center><p><img src="../webimages/iconos/ajax-loader.gif" width="125" height="123" /><br />&nbsp;&nbsp;&nbsp;<span class="arial_13_black_negrita">'+contenido+' </span><img src="../webimages/iconos/loadingpuntos01.gif" width="18" height="7" /><br><br></p></center></td></tr><tr><td>&nbsp;</td></tr></table>');
}

function mostrarCargaContenido_2(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#004669"><tr><td>&nbsp;</td></tr><tr><td bgcolor="#FFFFFF" class="TABLE_border4"><center><p><img src="webimages/iconos/ajax-loader.gif" width="125" height="123" /><br />&nbsp;&nbsp;&nbsp;<span class="arial_13_black_negrita">'+contenido+' </span><img src="webimages/iconos/loadingpuntos01.gif" width="18" height="7" /><br></p></center></td></tr><tr><td>&nbsp;</td></tr></table>');
}

function mostrarCargaContenido_externo(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#004669"><tr><td>&nbsp;</td></tr><tr><td bgcolor="#FFFFFF" class="TABLE_border4"><center><p><img src="CERTIFICACIONVIRTUAL/webimages/iconos/ajax-loader.gif" width="125" height="123" /><br />&nbsp;&nbsp;&nbsp;<span class="arial_13_black_negrita">'+contenido+' </span><img src="CERTIFICACIONVIRTUAL/webimages/iconos/loadingpuntos01.gif" width="18" height="7" /><br></p></center></td></tr><tr><td>&nbsp;</td></tr></table>');
}

function mostrarCargaContenido_externo_2(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#004669"><tr><td>&nbsp;</td></tr><tr><td bgcolor="#FFFFFF" class="TABLE_border4"><center><p><img src="../CERTIFICACIONVIRTUAL/webimages/iconos/ajax-loader.gif" width="125" height="123" /><br />&nbsp;&nbsp;&nbsp;<span class="arial_13_black_negrita">'+contenido+' </span><img src="../CERTIFICACIONVIRTUAL/webimages/iconos/loadingpuntos01.gif" width="18" height="7" /><br></p></center></td></tr><tr><td>&nbsp;</td></tr></table>');
}

function MostraCargarLoading(div_mensaje, contenido){
		$("#"+div_mensaje+"").html('<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#004669"><tr><td  bgcolor="#FFFFFF" class="TABLE_border4"><p align="left"><img src="webimages/iconos/loading.gif" width="31" height="31" align="absmiddle" /> '+contenido+'</p></td></tr></table>');

}

function MostraCargarLoading_centro(div_mensaje, contenido){
		$("#"+div_mensaje+"").html('<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#004669" ><tr><td  bgcolor="#FFFFFF" class="TABLE_border4"><br /><p style="text-align:center"><img src="webimages/iconos/loading.gif" width="31" height="31" align="absmiddle" /> '+contenido+'</p><br /></td></tr></table>');

}
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/


function mostrarCarga02(div_mensaje){
	$("#"+div_mensaje+"").html('<center><table width="95%" border="1" bordercolor="#004669"><tr><td bgcolor="#FFFFFF"><center><img src="../imagesweb/ajax-loader.gif" width="125" height="123" /></center></td></tr></table></center>');
}



function mostrarCarga04(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<center><table width="95%" border="1" bordercolor="#004669"><tr><td bgcolor="#FFFFFF"><center><img src="../imagesweb/ajax-loader.gif" width="125" height="123" /><br>'+contenido+'<br><br></center></td></tr></table></center><br>');
}

function mostrarCarga05(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="96%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="table_Listing2" align="center"><tr><td height="130"><div align="center"><img src="Administrator/imagesweb/ajax-loader.gif" width="125" height="123" /></div></td></tr><tr><td height="30"><div align="center"><font color="#333333">'+contenido+'</font> <img src="Administrator/imagesweb/loadingpuntos.gif" width="18" height="7" /></div></td></tr></table>');
}



function mostrarCarga(div_mensaje){
	$("#"+div_mensaje+"").html('<center><img src="../../../imagesweb/ajax-loader.gif" width="125" height="123" /></center>');
}	
	

function mostrarPequenio(div_mensaje){
	$("#"+div_mensaje+"").animate({left:"+=10px"}).animate({left:"-5000px"});
	$("#"+div_mensaje+"").html('<div align="left">&nbsp;&nbsp;<img src="../imagesweb/load.gif" width="18" height="18" /></div>');
	$("#"+div_mensaje+"").stop().animate({opacity: "hide", top: "-300"}, 1000);
}

function Enviar_Datos_Sinabip(){
	$('#iniciar').attr('disabled','disabled');
	var v_Sistemas 		=	$("#Sistemas").val();
	var v_usuario		=	$("#Usuario").val();
	var v_contrasena	=	$("#password").val();
	var txh_url			=	$("#txh_url").val();
	var txt_entidad		=   $('#cod_entidad').val();
	var csrf_token		=   $('#csrf_token').val();
	let cerrar 			= '';
	
	if (v_usuario==''){
		$('#msg').show();
		$('#msg').html(cerrar+"Ingrese Usuario");
		$("#Usuario").focus();
		
	}else if (v_contrasena==''){
		$('#msg').show();
		$('#msg').html(cerrar+"Ingrese su Contraseña");
		$("#password").focus();
		
	}else{
		// MostraCargarLoading('div_loading','Cargando...');
		$.post("System/Model/M_simi_web_user.php?operacion=validar_usuarios_muebles",{ 
			v_Sistemas:v_Sistemas,
			v_usuario:v_usuario,
			v_contrasena:v_contrasena,
			txh_url:txh_url,
			txt_entidad: txt_entidad,
			csrf_token:csrf_token
		},function(response){
			$('#iniciar').removeAttr('disabled');
			// var response = JSON.parse(response);
			// var valor 	= response.split("[*]")[0];
			// var nombre	= response.split("[*]")[1];			
			console.log(response);
			var valor 	= response.valor;
			var nombre	= response.nombre;
			// console.log(response.jwt);
			sessionStorage.setItem("token", response.jwt);    
			// return;
			$('#msg').html('');
			switch (valor) {
				case 0:
					$("#div_loading").html('');
					$('#msg').show();
					$('#msg').html(cerrar+"<strong>Usuario no Existe!</strong> Para mayor información comunicarse con el área de sistemas.");
					// alert('Usuario no Existe! \n\rPara mayor información comunicarse con el área de sistemas. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
					// location.href='#';
				break;
				case 1:
					location.href='System/'+nombre;	//Panel sistema
				break;
				case 2:
					alert('Ocurrio un problema vuelve a intentarlo');
					location.reload();
				break;
				case 3:
					$("#div_loading").html('');
					$('#msg').show();
					$('#msg').html(cerrar+"<strong>Existe mas de un usuario!</strong> Para mayor información comunicarse con el área de sistemas.");
					// alert('Existe mas de un usuario! \n\rPara mayor información comunicarse con el área de sistemas. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
					// location.href='#';
				break;
				case 4:
					$("#div_loading").html('');
					$('#msg').show();
					$('#msg').html(cerrar+'<strong>Clave incorrecta!</strong> Para mayor información comunicarse con el área de sistemas.')
					// alert('Clave incorrecta! \n\rPara mayor información comunicarse con el área de sistemas. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
					// location.href='#';
				break;
				case 5:
					$("#div_loading").html('');
					$('#msg').show();
					$('#msg').html(cerrar+'<strong>Personal desactivado!</strong> Para mayor información comunicarse con el área de sistemas. SUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.')
					// alert('Personal desactivado! \n\rPara mayor información comunicarse con el área de sistemas. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
					// location.href='#';
				break;
				case 6:
					$("#div_loading").html('');
					$('#msg').show();
					$('#msg').html(cerrar+'<strong>SU ENTIDAD NO EXISTE</strong>')
					// alert('SU ENTIDAD NO CONFORMA EL SISTEMA NACIONAL DE BIENES ESTATALES (SBNE)');
					// location.href='#';
				break;
				case 10:
					$("#div_loading").html('');
					$('#msg').show();
					$('#msg').html(cerrar+'<strong>EL USUARIO HA SIDO BLOQUEADO POR 1 MINUTO POR SUPERAR EL LIMITE DE INTENTO</strong>')
				break;
				default:
					$('#msg').hide();
					$('#msg').html(cerrar+' '+response)
					// alert(response);
				break;
			}
		})
	}
}

function Enviar_Datos_Sinabip_superadmin(v_usuario1,v_contrasena1,v_sistema1,v_cod_entidad1){
	
	var v_Sistemas 		=	v_sistema1;
	var v_usuario		=	v_usuario1;
	var v_contrasena	=	v_contrasena1;
	var txh_url			=	$("#txh_url").val();
	var v_admint		=	$("#admint").val();
	var v_cod_entidad  = 	v_cod_entidad1;

	if (v_usuario==''){
		alert('Ingrese Usuario');
		$("#Usuario").focus();
		
	}else if (v_contrasena==''){
		alert('Ingrese su Contraseña');
		$("#password").focus();
		
	}else{
		MostraCargarLoading('div_loading','Cargando...');
		var url ="System/Model/M_simi_web_user.php?operacion=validar_usuarios_muebles_superadmin";
		var var2 ='System/';

		if(v_admint=='1'){
			url ="Model/M_simi_web_user.php?operacion=validar_usuarios_muebles_superadmin";
			var2 ='';
		} 

		if(v_admint=='2'){
			url ="Model/M_simi_web_user.php?operacion=validar_usuarios_muebles";
			var2 ='';
		}
		// /alert(url); /  Model/M_simi_web_user.php?operacion=validar_usuarios_muebles
		$.post(url,{ 
			v_Sistemas:v_Sistemas,
			v_usuario:v_usuario,
			v_contrasena:v_contrasena,
			txh_url:txh_url,
			v_admint:v_admint,
			txt_entidad:v_cod_entidad
		},function(response){
			
			var valor 	= response.split("[*]")[0];
			var nombre	= response.split("[*]")[1];
						
			if(valor==1){		
				location.href=var2+nombre;	//Panel sistema
				
			}else if( valor==0 ){
				
				$("#div_loading").html('');
				alert('Usuariod no Existe! \n\rPara mayor informaci�n comunicarse con el �rea de sistemas. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
				location.href='#';
				
			}else if( valor==3 ){
				
				$("#div_loading").html('');
				alert('Existe mas de un usuario! \n\rPara mayor informaci�n comunicarse con el �rea de sistemas. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
				location.href='#';
				
			}else if( valor==4 ){
				
				$("#div_loading").html('');
				alert('Clave incorrecta! \n\rPara mayor informaci�n comunicarse con el �rea de sistemas. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
				location.href='#';
				
			}else if( valor==5 ){
				
				$("#div_loading").html('');
				alert('Personal desactivado! \n\rPara mayor informaci�n comunicarse con el �rea de sistemas. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
				location.href='#';
				
			}else{
				alert(response);
			}
	
		})
	
	}
}


function Valida_Ruc_Sbn(){
	
	var v_Sistemas 		=	$("#Sistemas").val();
	var v_ruc			=	$("#txt_ruc").val();
	var txh_url			=	$("#txh_url").val();

	if (v_ruc==''){
		alert('Ingrese N° RUC');
		$("#txt_ruc").focus();
	}else{
		MostraCargarLoading('div_loading','Cargando...');
		
		$.post("System/Model/M_simi_web_user.php?operacion=validar_ruc_decreto",{ 
			v_Sistemas:v_Sistemas,
			v_ruc:v_ruc,
			txh_url:txh_url
		},function(response){
			var valor 	= response.split("[*]")[0];
			//console.log(response);
			//return;
			var name 	= response.split("[*]")[1];
			var cod 	= response.split("[*]")[2];
			if(valor==1 || valor==2){
				$('.entidad_class').show();
				$('#entidad').val(name);
				$('#valida_ruc_boton').text('CONTINUAR');
				$('#valida_ruc_boton').attr('onclick','Siguiente_valida()');
				$('#name_entidad').val(name);
				$('#cod_entidad').val(cod);
			}else if( valor==0 ){
				$("#div_loading").html('');
				alert('Este número de RUC no tiene acceso a este sistema, porque no está obligado a remitir información inmobiliaria en mérito al D.U. 005-2018. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');

			}else{
				alert(response);
			}
	
		})
	
	}
}


function Siguiente_valida(){
	var v_Sistemas 		=	$("#Sistemas").val();
	var v_ruc			=	$("#txt_ruc").val();
	var v_entidad 		=	$('#entidad').val();
	var txh_url			=	$("#txh_url").val();

	if (v_ruc=='' && v_entidad==''){
		alert('Ingrese el numero de ruc nuevamente');
	}else{
		MostraCargarLoading('div_loading','Cargando...');
		$.post("System/Model/M_simi_web_user.php?operacion=validar_ruc_decreto",{ 
			v_Sistemas:v_Sistemas,
			v_ruc:v_ruc,
			txh_url:txh_url
		},function(response){
			var valor 	= response.split("[*]")[0];
			var name 	= response.split("[*]")[1];
			var cod 	= response.split("[*]")[2];
			if(valor==1){
				$('#frm_login').show();
				$('#frm_ruc_valid').hide();
				$('#name_entidad').val(name);
				$('#cod_entidad').val(cod);
				$('#name_entidad').val(code);
			}else if( valor==0 ){
				$("#div_loading").html('');
				alert('Este número de RUC no tiene acceso a este sistema, porque no está obligado a remitir información inmobiliaria en mérito al D.U. 005-2018. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
			
			}else if( valor==2 ){
				$("#div_loading").html('');
				alert('Usted no puede ingresar porque ya concluyó con el proceso de remisión de información Inmobiliaria. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
			}else{
				alert(response);
			}
	
		})
	
	}

}



function Enviar_Datos_Inmuebles(){
	
	var v_Sistemas 		=	$("#Sistemas").val();
	var v_usuario		=	$("#Usuario").val();
	var v_contrasena	=	$("#password").val();
	var txh_url			=	$("#txh_url").val();

	if (v_usuario==''){
		alert('Ingrese Usuario');
		$("#Usuario").focus();
		
	}else if (v_contrasena==''){
		alert('Ingrese su Contrase�a');
		$("#password").focus();
		
	}else{
		MostraCargarLoading('div_loading','Cargando...');
		
		$.post("System/Model/M_inmuebles_web_user.php?operacion=validar_usuarios_inmuebles",{ 
			v_Sistemas:v_Sistemas,
			v_usuario:v_usuario,
			v_contrasena:v_contrasena,
			txh_url:txh_url
		},function(response){
			
			var valor 	= response.split("[*]")[0];
			var nombre	= response.split("[*]")[1];
			
			if(valor==1){		
				location.href='System/'+nombre;	//Panel sistema
				
			}else if( valor==0 ){
				
				$("#div_loading").html('');
				alert('Usuario no Existe! \n\rPara mayor informaci�n comunicarse con el �rea de sistemas. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
				location.href='#';
				
			}else if( valor==3 ){
				
				$("#div_loading").html('');
				alert('Existe mas de un usuario! \n\rPara mayor informaci�n comunicarse con el �rea de sistemas. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
				location.href='#';
				
			}else if( valor==4 ){
				
				$("#div_loading").html('');
				alert('Clave incorrecta! \n\rPara mayor informaci�n comunicarse con el �rea de sistemas. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
				location.href='#';
				
			}else if( valor==5 ){
				
				$("#div_loading").html('');
				alert('Personal desactivado! \n\rPara mayor informaci�n comunicarse con el �rea de sistemas. \n\rSUPERINTENDENCIA NACIONAL DE BIENES ESTATALES.');
				location.href='#';
				
			}else{
				alert(response);
			}
	
		})
	
	}
}

/***********************************************************************************/
/************************************ INTRANET *************************************/
/***********************************************************************************/

function Cambiar_Clave_Intranet(){
	
	var v_TXH_ID_USUARIO				=	$("#TXH_ID_USUARIO").val();
	var v_TXH_NOM_USUARIO				=	$("#TXH_NOM_USUARIO").val();
	var v_txt_clave_actual				=	$("#txt_clave_actual").val();
	var v_txt_clave_nueva				=	$("#txt_clave_nueva").val();
	var v_txt_clave_nueva_repite		=	$("#txt_clave_nueva_repite").val();
	
	if(v_txt_clave_actual == ''){
		alert('Ingrese clave actual');
		$("#txt_clave_actual").focus();
		
	}else if(v_txt_clave_nueva == ''){
		alert('Ingrese clave nueva');
		$("#txt_clave_nueva").focus();
		
	}else if(v_txt_clave_nueva_repite == ''){
		alert('Repita clave nueva');
		$("#txt_clave_nueva_repite").focus();
		
	}else if(v_txt_clave_nueva != v_txt_clave_nueva_repite){
		alert('Las nuevas claves no coenciden. \n\r Ingrese nuevamente.');
		$("#txt_clave_nueva").val('');
		$("#txt_clave_nueva_repite").val('');
		
	}else{
	
		if(confirm('Confirma cambiar clave?')){
	
			$.post("../System/Model/M_sbn_usuarios.php?operacion=Cambiar_Clave_Intranet",{ 
				v_TXH_ID_USUARIO:v_TXH_ID_USUARIO,
				v_TXH_NOM_USUARIO:v_TXH_NOM_USUARIO,
				v_txt_clave_actual:v_txt_clave_actual,
				v_txt_clave_nueva:v_txt_clave_nueva
			},function(valor){
				
				if(valor==1){
					
					alert('Se procedio de manera correcta el cambio de clave.');
					location.href='../intranet.php';
					
				}else if(valor  == 5){
					alert(' Error. Datos incorrectos del usuario, no puede cambiar clave.');
					
				}else{
					alert(valor);
				}
				
			})
		
		
		}
	}

}


/***********************************************************************************/
/************************************ SINABIP *************************************/
/***********************************************************************************/

function Cambiar_Clave_Sinabip(){
	
	var v_TXH_SIMI_COD_USUARIO			=	$("#TXH_SIMI_COD_USUARIO").val();
	var v_TXH_SIMI_NOM_USUARIO			=	$("#TXH_SIMI_NOM_USUARIO").val();
	var v_txt_clave_actual_simi			=	$("#txt_clave_actual_simi").val();
	var v_txt_clave_nueva_simi			=	$("#txt_clave_nueva_simi").val();
	var v_txt_clave_nueva_repite_simi	=	$("#txt_clave_nueva_repite_simi").val();
	
	if(v_txt_clave_actual_simi == ''){
		alert('Ingrese clave actual');
		$("#v_txt_clave_actual_simi").focus();
		
	}else if(v_txt_clave_nueva_simi == ''){
		alert('Ingrese clave nueva');
		$("#v_txt_clave_nueva_simi").focus();
		
	}else if(v_txt_clave_nueva_repite_simi == ''){
		alert('Repita clave nueva');
		$("#v_txt_clave_nueva_repite_simi").focus();
		
	}else if(v_txt_clave_nueva_simi != v_txt_clave_nueva_repite_simi){
		alert('Las nuevas claves no coenciden. \n\r Ingrese nuevamente.');
		$("#txt_clave_nueva_simi").val('');
		$("#txt_clave_nueva_repite_simi").val('');
		
	}else{
	
		if(confirm('Confirma cambiar clave?')){
	
			$.post("../System/Model/M_simi_web_user.php?operacion=Cambiar_Clave_Sinabip",{ 
				v_TXH_SIMI_COD_USUARIO:v_TXH_SIMI_COD_USUARIO,
				v_TXH_SIMI_NOM_USUARIO:v_TXH_SIMI_NOM_USUARIO,
				v_txt_clave_actual_simi:v_txt_clave_actual_simi,
				v_txt_clave_nueva_simi:v_txt_clave_nueva_simi
			},function(valor){
				
				if(valor==1){
					
					alert('Se procedio de manera correcta el cambio de clave.');
					location.href='../sinabip.php';
					
				}else if(valor  == 5){
					alert(' Error. Datos incorrectos del usuario, no puede cambiar clave.');
					
				}else{
					alert(valor);
				}
				
			})
		
		
		}
	}

}



function APMenu(sUrl,idContenido){
	NiDivDisplay('Mflotante');
	ajaxpage(sUrl,idContenido);
}


function Actualiza_Menu_Direccionar_URL(sUrl,idContenido, codmod){
	
	if(sUrl == '-'){
		ajaxpage("sinabip_mantenimiento.php",idContenido);
	}else{
		ajaxpage(sUrl,idContenido);
	}
}

function Actualiza_Menu_Direccionar_URL_saliente(sUrl,idContenido, codmod){
	
	if(sUrl == '-'){
		
		$.get("sinabip_modulos_menu.php?idm="+codmod,{ 
		},function(data){
			$("#DIV_MENU_SINABIP").html(data);
			ajaxpage("sinabip_mantenimiento.php",idContenido);
		});
	}else{
		
		$.get("sinabip_modulos_menu.php?idm="+codmod,{ 
		},function(data){
			$("#DIV_MENU_SINABIP").html(data);
			ajaxpage(sUrl,idContenido);
		});
	}
}


function NiDivDisplay(sDiv){
	
	var oDiv = document.getElementById(sDiv);
	if(oDiv.style.display=='block' || oDiv.style.display=='' ){
		oDiv.style.display='none';
		oDiv.style.visibility= "hidden";	
	}else{
		oDiv.style.display='block';
		oDiv.style.visibility= "visible";	
	}
}


function ajaxpage(url, containerid){

	carga_loading(containerid);
	
	//$(this).dialog("destroy");s
	//document.getElementById(containerid).innerHTML = '';
	
	$.get(url,{},function(data){
		$("#"+containerid+"").html(data);
		//$("#menu_sistema").css({"background-image": "url(../webimages/iconos/menu1.png)", "background-repeat":"no-repeat"} );
	});
	
}

function carga_loading(containerid){
	var oDiv = document.getElementById(containerid);
	oDiv.style.display='block';
	oDiv.style.visibility= "visible";
	document.getElementById(containerid).innerHTML=	"<div style='vertical-align:middle;text-align:center;max-width:100%;heigh:100%;' ><img src='../webimages/iconos/loadingnew2.gif' /></div>";
}

function direccionar(url, containerid){	
	var oDiv = document.getElementById('Mflotante');
	oDiv.style.display='none';
	oDiv.style.visibility= "hidden";
	ajaxpage(url, containerid);
}


function cerrar_session(NOM_MODULO,archivo){
	
	var TXH_NOM_USUARIO	=	$("#TXH_NOM_USUARIO").val();
	var TXH_URL_MODULO	=	$("#TXH_URL_MODULO").val();
	
	if(NOM_MODULO == "INTRANET"){
		var URL = "Model/M_sbn_usuarios.php?operacion=Cerrar_Session_Intranet_SBN";
		
	}else if(NOM_MODULO == "SINABIP"){
		var URL = "Model/M_simi_web_user.php?operacion=Cerrar_Session_Modulo_Muebles";
		
	}else if(NOM_MODULO == "INMUEBLES"){
		var URL = "Model/M_inmuebles_web_user.php?operacion=Cerrar_Session_Modulo_Inmuebles";

	}else if(NOM_MODULO == "DECREURG"){
		var URL = "Model/M_simi_web_user.php?operacion=Cerrar_Session_Modulo_Muebles";
	}
	
	sessionStorage.clear();
	localStorage.clear();
	sessionStorage.removeItem('token');
	sessionStorage.setItem("token", null);
	console.log('Saliendo..');
	$.post(URL,{ 
		NOM_MODULO:NOM_MODULO,
		TXH_NOM_USUARIO:TXH_NOM_USUARIO,
		TXH_URL_MODULO:TXH_URL_MODULO
	},function(data){
		if(data==0){
			location.href='../'+archivo;
		}
	})
}

function mOvr2(src) {
	//if(!src.contains(event.fromElement)){
	src.style.cursor = 'hand';
	src.className="ListMouseOver";
	//}
}

function mOut2(src){
	//if(!src.contains(event.toElement)){
		src.style.cursor = 'default';
		src.className="";
	//}
}


//////////////////////////////////////////////////////

function mOvr3(ID) {	
	//$("#"+ID+"").addClass("");
	//$("#"+ID+"").css("cursor","hand");	

	$("#"+ID+"").mouseover(function(event){
	   $("#"+ID+"").addClass("ListMouseOver");
	});
}

function mOut3(ID){
//		$("#"+ID+"").addClass("ListMouseOver");
	//	$("#"+ID+"").css("cursor","default");
		
	$("#"+ID+"").mouseout(function(event){
	   $("#"+ID+"").removeClass("ListMouseOver");
	});
}

//////////////////////////////////////////////////////

function CharLimit(input, maxChar) {
	var len = $(input).val().length;
	$('#textCounter').text(maxChar - len + ' Caracteres restantes');
	
	if (len > maxChar) {
		$(input).val($(input).val().substring(0, maxChar));
		$('#textCounter').text(0 + ' Caracteres restantes');
	}
}