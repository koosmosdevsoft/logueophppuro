function creaObjecto(sA,sB){
	var chartData ;
	var sNI = sA.split("]"); 
	var sVI = sB.split("]"); 
	var instanciaA = new Object();
	for ( var c = 0; c < sNI.length;++c){
		instanciaA[sNI[c]] = parseInt(sVI[c]);
	}
	chartData = instanciaA;
	return chartData;
 }
 
function setGraficoA(sData,sTitulo,sCategoria){
		// SERIAL CHART
		chart = new AmCharts.AmSerialChart();
		chart.dataProvider = sData;
		chart.categoryField = ""+sCategoria+"";

		// AXES
		// category
		var categoryAxis = chart.categoryAxis;
		categoryAxis.fillAlpha = 1;
		categoryAxis.fillColor = "#FAFAFA";
		categoryAxis.gridAlpha = 0;
		categoryAxis.axisAlpha = 0;
		categoryAxis.gridPosition = "start";
		categoryAxis.position = "top";
	
		// value
		var valueAxis = new AmCharts.ValueAxis();
		valueAxis.title = ""+sTitulo+"";
		valueAxis.dashLength = 5;
		valueAxis.axisAlpha = 0;
		valueAxis.integersOnly = true;
		valueAxis.gridCount = 10;
		valueAxis.reversed = false; // this line makes the value axis reversed
		chart.addValueAxis(valueAxis);
 } 
 
function creaLinea(sTitulo,sLineaId,sStyle){
		// SERIAL CHART
			var sTyleX = sStyle.split("-"); 	
			var graph = new AmCharts.AmGraph();
			graph.title = ""+sTitulo+"";
			graph.lineColor = ""+sTyleX[0]+"";
			graph.bulletBorderColor = ""+sTyleX[1]+"";
			graph.lineThickness = 3;
			graph.valueField = ""+sLineaId+"";
			graph.hidden = false; // this line makes the graph initially hidden           
			graph.balloonText = ""+sTitulo+" [[category]]: [[value]]";
			graph.lineAlpha = 1;
			graph.bullet = "round";
			chart.addGraph(graph);

 } 	
 
 function setCursor(){
		// SERIAL CHART
		var chartCursor = new AmCharts.ChartCursor();
		chartCursor.cursorPosition = "mouse";
		chartCursor.zoomable = true;
		chartCursor.cursorAlpha = 0;
		chart.addChartCursor(chartCursor);     
 } 	
 
 function setLegenda(){
		// SERIAL CHART
		var legend = new AmCharts.AmLegend();
		legend.useGraphSettings = true;
		chart.addLegend(legend);
 } 	
 

 
function muestraGrafico(sTitulo,sCategoria,xId,xValues,sDiv,sStyle){
		this.chart;
		var xLineas = [];
		var xNI = xId;
		var xP= xValues;	

		var sCl = xP.split("}"); 
		 
		for ( var b = 0; b < sCl.length;++b){
		 var valObjectK = creaObjecto(xNI,sCl[b]);
		 xLineas.push(valObjectK);
		}

		 this.setGrafico = setGraficoA(xLineas,sTitulo,sCategoria);
		 var sL = xNI.split("]"); 
		 var sStyleX = sStyle.split("]"); 
		 for ( var c = 1; c < sL.length;++c){
		   sTitulo = sL[c];
		   sIdLinea = sL[c];
		   sStyle = sStyleX[c];
		   this.sLinea = creaLinea(sTitulo,sIdLinea,sStyle);
		 }
			 
		this.sCursor = setCursor();
		this.sLegenda= setLegenda();
		chart.write(sDiv);
 }