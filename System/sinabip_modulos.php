<?php session_start();

 error_reporting(0);

/* CREACION:        WILLIAMS ARENAS */
/* FECHA CREACION:  25-05-2020 */
/* DESCRIPCION:     IMPORTACION DE LIBRERIAS PDO */
require_once("Controller/BaseBD_PDO.php");
require_once('Model/BaseBD_PDOModel.php');
/* FIN DE IMPORTACION DE LIBRERIAS PDO */

require_once('../utils/funciones/funciones.php');
require_once('Controller/C_simi_web_user.php');
require_once('Controller/C_simi_web_user_permiso.php');
require_once('Controller/C_simi_web_modulos.php');
require_once('Model/BienPatrimonialModel.php');
require_once('../class/sesion_time.php');
require_once("../class/nocsrf.php");

$oSimi_Web_User = new Simi_Web_User;
$oMuebles_Menu_Permiso  = new Muebles_Menu_Permiso;
$oSimi_Modulos  = new Simi_Modulos;

$objModel = new BienPatrimonialModel();


$S_SIMI_COD_USUARIO = $_SESSION['th_SIMI_COD_USUARIO'];
$S_SIMI_NOM_USUARIO = $_SESSION['th_SIMI_NOM_USUARIO'];
$S_SIMI_COD_ENTIDAD = $_SESSION['th_SIMI_COD_ENTIDAD'];
$supeadmin = $_SESSION['th_SIMI_SUPERADMIN'];



// try{
  if ( !isset( $S_SIMI_COD_USUARIO ) && $S_SIMI_COD_USUARIO == '' ) {
    echo '<script language="javascript">parent.location.href="../sinabip.php";</script>';
  }
//   NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
// }catch ( Exception $e ){
//   echo '<script language="javascript">parent.location.href="../sinabip.php";</script>';
// }

//$z_COD_MODULO = $_GET['idm'];
// $FECHA_ACTUAL = date('Y-m-d');
// $FECHA_CIERRE = '2019-01-01';
// $CERRAR = 0;
// if($FECHA_ACTUAL >= $FECHA_CIERRE){
//     $CERRAR = 1;
// }

//$z_COD_MODULO = addslashes($_GET['idm']); 
$z_COD_MODULO = $_GET['idm'];



if ( $supeadmin == '1' ){
    $resultUserSuperadmin   = $oSimi_Web_User->Ver_Datos_Usuario_SIMI_x_CODIGO_2($S_SIMI_COD_USUARIO);
    if($resultUserSuperadmin){
      $F_COD_ENTIDAD_Superadmin   = utf8_encode(odbc_result($resultUserSuperadmin,"COD_ENTIDAD"));
      $F_RUC_ENTIDAD_Superadmin   = utf8_encode(odbc_result($resultUserSuperadmin,"RUC_ENTIDAD"));
      $F_NOM_ENTIDAD_Superadmin   = utf8_encode(odbc_result($resultUserSuperadmin,"NOM_ENTIDAD"));
    }

    $S_SIMI_COD_USUARIO= $_SESSION['admin_coduser'];
    $S_SIMI_NOM_USUARIO = $_SESSION['admin_nombuser'];
    $S_SIMI_COD_ENTIDAD = $_SESSION['admin_codenti'];
}

$resultUserSimi   = $oSimi_Web_User->Ver_Datos_Usuario_SIMI_x_CODIGO_2($S_SIMI_COD_USUARIO);

if($resultUserSimi){
    $F_NOM_USUARIO    = utf8_encode(odbc_result($resultUserSimi,"NOM_USUARIO"));
    $F_FUNCIONARIO    = utf8_encode(odbc_result($resultUserSimi,"FUNCIONARIO"));
    $F_CARGO      = utf8_encode(odbc_result($resultUserSimi,"CARGO"));
    $F_COD_ENTIDAD    = utf8_encode(odbc_result($resultUserSimi,"COD_ENTIDAD"));
    $F_RUC_ENTIDAD    = utf8_encode(odbc_result($resultUserSimi,"RUC_ENTIDAD"));
    $F_NOM_ENTIDAD    = utf8_encode(odbc_result($resultUserSimi,"NOM_ENTIDAD"));
    
    /*
        $resulModuloOK = $oSimi_Modulos->Ver_Datos_Modulos_Simi_x_Codigo($z_COD_MODULO);
        $z_DESC_MUEBLE_MODULO = utf8_encode(odbc_result($resulModuloOK,"DESC_MUEBLE_MODULO"));
        $NOM_ARCHIVO      = utf8_encode(odbc_result($resulModuloOK,"NOM_ARCHIVO"));
    */

    /* CREACION:        WILLIAMS ARENAS */
    /* FECHA CREACION:  25-05-2020 */
    /* DESCRIPCION:     VER DATOS MODULOS */
    $data = array();   
    //$TXH_COD_MODULO = addslashes($_GET['idm']);  
    $TXH_COD_MODULO    = isset($_GET['idm'])? $_GET['idm'] : "";
    
    $dataModel_01 = [
      'TXH_COD_MODULO'=>$TXH_COD_MODULO
    ];
    
    $data['DataListaBienPatrimonial'] = $objModel->Ver_Datos_Modulos_Simi_x_Codigo2($dataModel_01);
    $z_DESC_MUEBLE_MODULO = $data['DataListaBienPatrimonial'][0]['DESC_MUEBLE_MODULO'];
    $NOM_ARCHIVO          = $data['DataListaBienPatrimonial'][0]['NOM_ARCHIVO'];
    /* FIN DE VER DATOS MODULOS */

}

if ( $supeadmin == '1' ){
  $F_COD_ENTIDAD    = $F_COD_ENTIDAD_Superadmin;
  $F_RUC_ENTIDAD    = $F_RUC_ENTIDAD_Superadmin;
  $F_NOM_ENTIDAD    = $F_NOM_ENTIDAD_Superadmin;
}

$menu = true;

if(isset($_GET['menu']) and $_GET['menu']== '0'){
  $menu = false;
}


?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../utils/css/SGI_Estilos.css" rel="stylesheet">
<LINK href="../utils/css/font-awesome.css" rel="stylesheet">
<!--<LINK href="../utils/css/font-awesome.css" rel="stylesheet">-->
<!--<link rel="stylesheet" type="text/css" href="../utils/css/menu_vertical_c.css"/>-->

<link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

<script language="javascript" src="../utils/jquery_vs/jquery-ui-1.11.1.custom/external/jquery/jquery.js"></script>
<script src="../utils/jquery_vs/jquery-ui-1.11.1.custom/jquery-ui.js"></script>
<link rel="stylesheet" href="../utils/jquery_vs/jquery-ui-1.11.1.custom/jquery-ui.css">
<script language="javascript" src="../utils/javascript/funcion.js"></script>
<script language="javascript" type="text/javascript" src="../utils/javascript/ajaxupload.3.5.js"></script>

<script src="../utils/javascript/jquery.dataTables.min.js"></script>
<link rel="stylesheet" type="text/css" href="../utils/javascript/jquery.dataTables.min.css"/>

<script src="../utils/javascript/dataTables.buttons.min.js"></script>
<script src="../utils/javascript/jszip.min.js"></script>
<script src="../utils/javascript/buttons.flash.min.js"></script>
<script src="../utils/javascript/pdfmake.min.js"></script>
<script src="../utils/javascript/vfs_fonts.js"></script>
<script src="../utils/javascript/buttons.html5.min.js"></script>
<script src="../utils/javascript/buttons.print.min.js"></script>

<link rel="stylesheet" type="text/css" href="../utils/javascript/buttons.dataTables.min.css" />


<title>SINABIP</title>

<SCRIPT type="text/javascript">

  $(function() {  
    //NiDivDisplay('Mflotante');
  });
</SCRIPT>
<style type="text/css">
  body{
    padding: 0;
    margin: 0;
  }
  .menu_arriba span{
    padding: 0 !important;
  }
</style>

</head>

<body>
<div id="Nav" style="<?php if(!$menu): ?>display:none<?php endif; ?>">
  <div id="BarraSup">
    <div id="BarraSupLogoMenus">
      <div style="width: 42%; text-align: left; float: left;"> 
        <!--<span><img width="30" height="30" align="top" src="../webimages/iconos/Terra.gif" /></span> -->
        <!-- <span><strong>SBN </strong>&nbsp;&nbsp; &nbsp;Superintendencia Nacional de Bienes Estatales&nbsp;&nbsp;</span>  -->
        <span><strong>MINISTERIO DE ECONOMIA Y FINANZAS </strong></span> 
      </div>
      <div style="width: 57%; text-align: right; float: left;" class="menu_arriba"><?php if($supeadmin == '1'){ ?>
        <input name="Sistemas" type="hidden" id="Sistemas" value="SINABIP" />
        <input name="txh_url" type="hidden" id="txh_url" value="https://wstest.mineco.gob.pe/SGISBN/System/sinabip.php" size="150" /> 
        <input name="admint" type="hidden" id="admint" value="2" size="150" /> 
        <span><a href="#" onclick="Enviar_Datos_Sinabip_superadmin('-','-','-')"><i class="fa fa-sign-out" aria-hidden="true" style="font-size:18px"></i>Regresar</a></span><?php } ?> 
        <span><a href="sinabip.php"><i class="fa fa-home fa-fw" aria-hidden="true" style="font-size:18px"></i>Página Principal</a></span>&nbsp;&nbsp;&nbsp;|&nbsp; 
        <span><a href="#" onclick="ajaxpage('View/intranet_contendios/cambio_clave_sinabip.php','Cuerpo')" ><i class="fa fa-cog fa-fw" aria-hidden="true" style="font-size:18px"></i> Cambiar clave&nbsp;</a></span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp; 
        <span><a href="sinabip.php" target="_blank"><i class="fa fa-file-text-o" aria-hidden="true" style="font-size:18px"></i>(+)</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp; 
        <span><a href="#" onclick="cerrar_session('SINABIP','sinabip.php')"><i class="fa fa-sign-out" aria-hidden="true" style="font-size:18px"></i> Cerrar Sesion</a></span> 
      </div>
    </div>
  </div>
</div>

<DIV class="SiteBienes" style="<?php if(!$menu): ?>display:none<?php endif; ?>">
<TABLE width="100%" style="width: 100%; background-color:#EEF4FD">
  <TR>
    <TD width="1705" height="45">
      <DIV style="width: 90px; float: left; padding-top:18px;">
      <!-- <a href="#">
      <img src="../webimages/sinabip_modulos/<?=$NOM_ARCHIVO?>" width="69" height="50" border="0" align="absmiddle"/>
      </a> -->
      </DIV>
      <DIV id="div_nom_titulo_mod" class="Titulo_plomo_20px" style="width: 348px; float: left; padding-top:18px; "><?=$z_DESC_MUEBLE_MODULO?></DIV>
      
      
      <DIV style="float: left;">
        <table width="500" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="123" rowspan="5" align="center">&nbsp;</td>
              <td align="left">&nbsp;</td>
            </tr>
            <tr>
              <td width="884" align="left"><span class="texto_arial_azul_n_14">Bienvenido: 
                <span class="texto_arial_plomo_n_12">
                <?=$F_FUNCIONARIO?>
                </span>
                <input type="hidden" name="TXH_SIMI_COD_USUARIO" id="TXH_SIMI_COD_USUARIO" value="<?=$S_SIMI_COD_USUARIO?>" />
                <input type="hidden" name="TXH_SIMI_NOM_USUARIO" id="TXH_SIMI_NOM_USUARIO" value="<?=$F_NOM_USUARIO?>" />
                <input type="hidden" name="TXH_SIMI_COD_ENTIDAD" id="TXH_SIMI_COD_ENTIDAD" value="<?=$F_COD_ENTIDAD?>" />
                <input type="hidden" name="TXH_URL_MODULO" id="TXH_URL_MODULO" value="<?=ObtenerURLPage()?>" />
                <input type="hidden" name="TXH_NOM_USUARIO" id="TXH_NOM_USUARIO" value="<?=$F_NOM_USUARIO?>" />
                
              </span></td>
            </tr>
          <tr>
            <td align="left"><span class="texto_arial_plomito_11"><?=$F_NOM_ENTIDAD?>
              </span></td>
          </tr>
          <tr>
            <td align="left"><span class="texto_arial_plomito_11">
              <?=$F_CARGO?>
              </span></td>
          </tr>
          <tr>
            <td align="left">&nbsp;</td>
          </tr>
          </table>
      </DIV>
    </TD>
    <TD width="40">&nbsp;</TD>
  </TR>
</TABLE>
</DIV>

<div style="background-color:#5693b4; font-size:1px">&nbsp;</div>

<table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td height="400" valign="top" bgcolor="#EEF4FD"style="<?php if(!$menu): ?>display:none;width: 0px !important;<?php else: ?>width: 340PX<?php endif; ?>">
    
    

      <div id="DIV_MENU_SINABIP" >

              <? include_once('sinabip_modulos_menu.php')?>
     
      </div>
    
    
    </td>
    <td valign="top" >
    <div id="Cuerpo"  style="padding: 0px 0px 0px 30px" >
     
    <? //if($z_COD_MODULO == '1'){ ?>
    <!--
    <table width="720" border="0" cellpadding="0" cellspacing="3" bgcolor="#f4f4f4" class="TABLE_border4">
      <tr>
        <td width="11" rowspan="7">&nbsp;</td>
        <td width="81" height="10" style="text-align:center">&nbsp;</td>
        <td colspan="2" class="texto_arial_rojo_n_12">&nbsp;</td>
        <td class="texto_arial_rojo_n_12">&nbsp;</td>
      </tr>
      <tr>
        <td width="81" rowspan="5" style="text-align:center"><img src="../webimages/iconos/aviso.png" width="52" height="46" /></td>
        <td height="12" colspan="2" class="texto_arial_azul_n_13">Aviso:</td>
        <td width="30" class="texto_arial_rojo_n_12">&nbsp;</td>
        </tr>
      <tr>
        <td colspan="2"><hr class="hr_01" /></td>
        <td class="texto_arial_rojo_n_12">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2" class="texto_arial_rojo_n_12">Estimado usuarios se comunica que debido al Inventario 2016 que concluye el 31 de Marzo de 2017, solo estara habilitado los menús para el proceso de registro de Inventarios :</td>
        <td class="texto_arial_rojo_n_12">&nbsp;</td>
      </tr>
      <tr>
        <td class="texto_arial_rojo_n_12">&nbsp;</td>
        <td class="texto_arial_rojo_n_12">&nbsp;</td>
        <td class="texto_arial_rojo_n_12">&nbsp;</td>
      </tr>
      <tr>
        <td width="46" height="27" class="texto_arial_rojo_n_12"><p>&nbsp;</p></td>
        <td width="532" class="texto_arial_rojo_n_12"><p>- Registro de Predios y/o Local<br />
          - 
          Registro del Responsable de Control Patrimonial</p>
          <p>- Registro del Inventario Anual 2016</p></td>
        <td class="texto_arial_rojo_n_12">&nbsp;</td>
      </tr>
      <tr>
        <td width="81" style="text-align:center">&nbsp;</td>
        <td colspan="2" class="texto_arial_rojo_n_12">&nbsp;</td>
        <td class="texto_arial_rojo_n_12">&nbsp;</td>
      </tr>
    </table>
    !-->
    <? //} ?>

    <div style="text-align:center;"><? include_once('sinabip_modulos_contenido.php');?></div> 
      
    </div>
    </td>
  </tr>
</table>





<div id="DIV_PIE">
  <table width="100%" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td colspan="3"><hr class="hr_01" /></td>
      </tr>
      <tr>
        <td width="40%">&nbsp;</td>
        <td width="32%"><span class="Pie_centro">Derecho Reservado por la Ministerio de Economia y Finanzas - <?php echo date('Y')?> &reg;</span></td>
        <td width="28%">&nbsp;</td>
      </tr>
  </table>
</div>
<?php if(isset($_GET['opt'])): ?>
  <script type="text/javascript">
    $(function(){
      var a = $($('.gw-nav-list').find('a')[<?=$_GET['opt']?>]).attr('onclick');
      console.log(a);
      var b = a.split('Actualiza_Menu_Direccionar_URL(');
      var c = b[1].split(')');
      var d = c[0].split(',');
      // alert(d);
      Actualiza_Menu_Direccionar_URL(d[0].split('"')[1],d[1].split('"')[1],d[2].split('"')[1]);
    });
  </script>
<?php endif; ?>


</body>
</html>