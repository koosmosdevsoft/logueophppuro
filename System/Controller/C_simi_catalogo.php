<?
require_once("C_Interconexion_SQL.php");

class Simi_Catalogo{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
	
	
	function Simi_Generar_Codigo_Catalogo(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(ID_CATALOGO),0) + 1) as CODIGO FROM TBL_MUEBLES_CATALOGO ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Simi_Generar_Codigo_Correlativo_Grupo_Clase_Bien($COD_GRUPO, $COD_CLASE){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT RIGHT('000000'+CONVERT(VARCHAR,(MAX(CAST(ISNULL(NRO_CORRELATIVO,0) AS INT)+1))),4) AS CODIGO
				FROM TBL_MUEBLES_CATALOGO 
				WHERE COD_GRUPO = '$COD_GRUPO' AND COD_CLASE = '$COD_CLASE' 
				";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Simi_Insertar_Catalogo($ID_CATALOGO, $COD_GRUPO, $COD_CLASE, $NRO_BIEN, $NRO_GRUPO, $NRO_CLASE, $NRO_CORRELATIVO, $DENOM_BIEN, $COD_RESOL_CATALOGO, $USUARIO_CREACION, $CONDICION_R, $ANOTACIONES_R){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_CATALOGO  (ID_CATALOGO, COD_GRUPO, COD_CLASE, NRO_BIEN, NRO_GRUPO, NRO_CLASE, NRO_CORRELATIVO, DENOM_BIEN, COD_RESOL_CATALOGO, FECHA_REGISTRO, USUARIO_CREACION, FECHA_CREACION, ID_ESTADO, CONDICION, ANOTACIONES)
						VALUES ('$ID_CATALOGO', '$COD_GRUPO', '$COD_CLASE', '$NRO_BIEN', '$NRO_GRUPO', '$NRO_CLASE', '$NRO_CORRELATIVO', '$DENOM_BIEN', '$COD_RESOL_CATALOGO', GETDATE(), '$USUARIO_CREACION', GETDATE(), 1, '$CONDICION_R', '$ANOTACIONES_R') ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Simi_Actualizar_Catalogo($ID_CATALOGO, $COD_GRUPO, $COD_CLASE, $NRO_GRUPO, $NRO_CLASE, $NRO_CORRELATIVO, $DENOM_BIEN, $COD_RESOL_CATALOGO, $USUARIO_MODIFICA, $CONDICION_R, $ANOTACIONES_R){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_MUEBLES_CATALOGO 
			SET COD_GRUPO = '$COD_GRUPO', COD_CLASE = '$COD_CLASE', NRO_GRUPO = '$NRO_GRUPO', NRO_CLASE = '$NRO_CLASE', NRO_CORRELATIVO = '$NRO_CORRELATIVO', DENOM_BIEN = '$DENOM_BIEN', COD_RESOL_CATALOGO = '$COD_RESOL_CATALOGO', USUARIO_MODIFICA = '$USUARIO_MODIFICA', FECHA_MODIFICA = GETDATE(), CONDICION = '$CONDICION_R', ANOTACIONES = '$ANOTACIONES_R'
			WHERE ID_CATALOGO = $ID_CATALOGO";
			$result = $this->oDBManager->execute($sql);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Simi_Eliminar_Catalogo($ID_CATALOGO, $USUARIO_ELIMINA){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_CATALOGO SET ID_ESTADO = 2, USUARIO_ELIMINA = '$USUARIO_ELIMINA', FECHA_ELIMINA = GETDATE() WHERE ID_CATALOGO = '$ID_CATALOGO' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}	
		
	function Simi_Ver_Catalogo_x_Codigo($ID_CATALOGO){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT CA.*, 
			G.DESC_GRUPO,
			CL.DESC_CLASE,
			ISNULL(CAF.NOMB_ARCHIVO,'') [NOMB_ARCHIVO], CAF.PESO_ARCHIVO,CAF.FECHA_ARCHIVO
			FROM TBL_MUEBLES_CATALOGO CA
			LEFT JOIN TBL_MUEBLES_GRUPO G ON (CA.COD_GRUPO = G.COD_GRUPO)
			LEFT JOIN TBL_MUEBLES_CLASE CL ON (CA.COD_CLASE = CL.COD_CLASE)
			LEFT JOIN TBL_MUEBLES_CATALOGO_FOTO CAF ON (CA.ID_CATALOGO = CAF.ID_CATALOGO)
			WHERE CA.ID_CATALOGO = '$ID_CATALOGO' ";
			//ECHO $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
		
	function Buscar_Bien_x_Nombre($DENOM_BIEN){
		if($this->oDBManager->conectar()==true){
			
			if($DENOM_BIEN == '' ){
				$top = " TOP 100 ";
			}else{
				$top = " ";
			}
			
			$sql="SELECT $top * FROM TBL_MUEBLES_CATALOGO WHERE ID_ESTADO = '1' AND DENOM_BIEN LIKE '%$DENOM_BIEN%' order by DENOM_BIEN ASC ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Validar_Existe_Nro_Bien($NRO_BIEN){
		if($this->oDBManager->conectar()==true){
			
			$sql="SELECT COUNT(*) AS TOT_REGISTRO FROM TBL_MUEBLES_CATALOGO C WHERE C.NRO_BIEN = '$NRO_BIEN' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Ver_Datos_Bien_x_Nro_Bien($NRO_BIEN){
		if($this->oDBManager->conectar()==true){
			
			$sql="SELECT C.* FROM TBL_MUEBLES_CATALOGO C WHERE C.NRO_BIEN = '$NRO_BIEN' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Total_Catalogo_x_Denominacion_Bien_BUSQUEDA($COD_GRUPO, $COD_CLASE, $NRO_BIEN, $DENOM_BIEN, $CONDICION, $COD_RESOL_CATALOGO){
		if($this->oDBManager->conectar()==true){
			
			if($COD_GRUPO != '-'){
				$COND1 = " AND CA.COD_GRUPO = '$COD_GRUPO' ";	
			}else{
				$COND1 = "";
			}
			
			if($NRO_BIEN != ''){
				$COND2 = " AND CA.NRO_BIEN LIKE '%$NRO_BIEN%' ";	
			}else{
				$COND2 = "";
			}
			
			if($DENOM_BIEN != ''){
				$COND3 = " AND CA.DENOM_BIEN LIKE '%$DENOM_BIEN%' ";	
			}else{
				$COND3 = "";
			}
			
			if($CONDICION != '-'){
				$COND4 = " AND CA.CONDICION = '$CONDICION' ";	
			}else{
				$COND4 = "";
			}
			
			if($COD_RESOL_CATALOGO != '-'){
				$COND5 = " AND CA.COD_RESOL_CATALOGO = '$COD_RESOL_CATALOGO' ";	
			}else{
				$COND5 = "";
			}
			
			if($COD_CLASE != '0'){
				$COND6 = " AND CA.COD_CLASE = '$COD_CLASE' ";	
			}else{
				$COND6 = "";
			}
			
			$sql="SELECT COUNT(*) AS TOT_REG FROM TBL_MUEBLES_CATALOGO CA WHERE CA.ID_ESTADO  = '1' $COND1 $COND2 $COND3 $COND4 $COND5 $COND6 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function LISTA_Catalogo_x_Denominacion_Bien_BUSQUEDA($INI, $FIN, $COD_GRUPO, $COD_CLASE, $NRO_BIEN, $DENOM_BIEN, $CONDICION, $COD_RESOL_CATALOGO){
		if($this->oDBManager->conectar()==true){
			
			if($COD_GRUPO != '-'){
				$COND1 = " AND CA.COD_GRUPO = '$COD_GRUPO' ";	
			}else{
				$COND1 = "";
			}
			
			if($NRO_BIEN != ''){
				$COND2 = " AND CA.NRO_BIEN LIKE '%$NRO_BIEN%' ";	
			}else{
				$COND2 = "";
			}
			
			if($DENOM_BIEN != ''){
				$COND3 = " AND CA.DENOM_BIEN LIKE '%$DENOM_BIEN%' ";	
			}else{
				$COND3 = "";
			}
			
			if($CONDICION != '-'){
				$COND4 = " AND CA.CONDICION = '$CONDICION' ";	
			}else{
				$COND4 = "";
			}
			
			if($COD_RESOL_CATALOGO != '-'){
				$COND5 = " AND CA.COD_RESOL_CATALOGO = '$COD_RESOL_CATALOGO' ";	
			}else{
				$COND5 = "";
			}
			
			if($COD_CLASE != '0'){
				$COND6 = " AND CA.COD_CLASE = '$COD_CLASE' ";	
			}else{
				$COND6 = "";
			}
			
			$SQL_ORIGEN="SELECT 
			ROW_NUMBER() OVER (ORDER BY CA.NRO_BIEN, CA.DENOM_BIEN) AS ROW_NUMBER_ID,
			CA.*, G.DESC_GRUPO, CL.DESC_CLASE, RS.NRO_RESOLUCION,
			ISNULL(CAF.NOMB_ARCHIVO,'') [NOMB_ARCHIVO], CAF.PESO_ARCHIVO,CAF.FECHA_ARCHIVO
			FROM TBL_MUEBLES_CATALOGO CA
			LEFT JOIN TBL_MUEBLES_GRUPO G ON (CA.COD_GRUPO = G.COD_GRUPO)
			LEFT JOIN TBL_MUEBLES_CLASE CL ON (CA.COD_CLASE = CL.COD_CLASE)
			LEFT JOIN TBL_MUEBLES_RESOLUC_CATALOGO RS ON (CA.COD_RESOL_CATALOGO = RS.COD_RESOL_CATALOGO)
			LEFT JOIN TBL_MUEBLES_CATALOGO_FOTO CAF ON (CA.ID_CATALOGO = CAF.ID_CATALOGO)
			WHERE CA.ID_ESTADO = 1 $COND1 $COND2 $COND3 $COND4 $COND5 $COND6 ";
			
			$sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ORDER BY 1";
			//echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function LISTA_Catalogo_Bienes_Muebles_Todo(){
		if($this->oDBManager->conectar()==true){
	
			$sql="SELECT 
			ROW_NUMBER() OVER (ORDER BY CA.NRO_BIEN, CA.DENOM_BIEN) AS ROW_NUMBER_ID,
			CA.*, G.DESC_GRUPO, CL.DESC_CLASE, RS.NRO_RESOLUCION
			FROM TBL_MUEBLES_CATALOGO CA
			LEFT JOIN TBL_MUEBLES_GRUPO G ON (CA.COD_GRUPO = G.COD_GRUPO)
			LEFT JOIN TBL_MUEBLES_CLASE CL ON (CA.COD_CLASE = CL.COD_CLASE)
			LEFT JOIN TBL_MUEBLES_RESOLUC_CATALOGO RS ON (CA.COD_RESOL_CATALOGO = RS.COD_RESOL_CATALOGO)
			WHERE CA.ID_ESTADO = 1 ";
			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
}
?>