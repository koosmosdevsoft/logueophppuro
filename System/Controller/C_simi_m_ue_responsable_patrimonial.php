<?
require_once("C_Interconexion_SQL.php");

class Simi_UE_Responsable_Patrimonial{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
	
	
	function Generar_Codigo_UE_Responsable_Patrimonial(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(COD_UE_RESP_PATRI),0) + 1) as CODIGO FROM TBL_MUEBLES_UE_RESPONSABLE_PATRIMONIAL ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}


	function Insertar_Simi_UE_Responsable_Patrimonial($COD_ENTIDAD, $ID_PREDIO_SBN_RESP, $COD_UE_AREA_RESP, $COD_UE_OFIC_RESP, $COD_UE_PERS_RESP, $SIMI_USUARIO_CREACION){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_UE_RESPONSABLE_PATRIMONIAL (COD_ENTIDAD, ID_PREDIO_SBN_RESP, COD_UE_AREA_RESP, COD_UE_OFIC_RESP, COD_UE_PERS_RESP, FECHA_REGISTRO, SIMI_USUARIO_CREACION, SIMI_FECHA_CREACION, ID_ESTADO)
			VALUES ('$COD_ENTIDAD', '$ID_PREDIO_SBN_RESP', '$COD_UE_AREA_RESP', '$COD_UE_OFIC_RESP', '$COD_UE_PERS_RESP', GETDATE(), '$SIMI_USUARIO_CREACION', GETDATE(), '1') ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Verifica_Existe_Responsable_Patrimonial_x_CODENTIDAD($COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT COD_UE_RESP_PATRI FROM TBL_MUEBLES_UE_RESPONSABLE_PATRIMONIAL WHERE ID_ESTADO = '1' AND COD_ENTIDAD = '$COD_ENTIDAD' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Ver_Datos_UE_Responsable_Patrimonial_x_CODIGO($COD_UE_RESP_PATRI){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT 
RP.*,
RESP_LOCAL = P.DENOMINACION_PREDIO,
RESP_AREA = A.DESC_AREA,
-- RESP_OFICINA = O.DESC_OFIC,
RESP_NRO_DOCUMENTO = PS.NRO_DOCUMENTO,
RESP_APEPAT_PERS = PS.UE_APEPAT_PERS,
RESP_APEMAT_PERS = PS.UE_APEMAT_PERS,
RESP_NOMBRES_PERS = PS.UE_NOMBRES_PERS,
RESP_NOM_COMPLETO = PS.UE_NOMBRES_PERS+', '+PS.UE_APEPAT_PERS+' '+PS.UE_APEMAT_PERS
FROM TBL_MUEBLES_UE_RESPONSABLE_PATRIMONIAL RP
LEFT JOIN TBL_PADRON_PREDIOS P ON (P.ID_PREDIO_SBN = RP.ID_PREDIO_SBN_RESP)
LEFT JOIN TBL_MUEBLES_UE_AREA A ON (A.COD_UE_AREA = RP.COD_UE_AREA_RESP)
-- LEFT JOIN TBL_MUEBLES_UE_OFICINA O ON (O.COD_UE_OFIC = RP.COD_UE_OFIC_RESP)
LEFT JOIN TBL_MUEBLES_UE_PERSONAL PS ON (PS.COD_UE_PERS = RP.COD_UE_PERS_RESP)
WHERE RP.ID_ESTADO ='1'
AND RP.COD_UE_RESP_PATRI = '$COD_UE_RESP_PATRI' 
			";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Actualiza_Simi_UE_Responsable_Patrimonial($COD_UE_RESP_PATRI, $COD_ENTIDAD, $ID_PREDIO_SBN_RESP, $COD_UE_AREA_RESP, $COD_UE_OFIC_RESP, $COD_UE_PERS_RESP, $SIMI_USUARIO_MODIFICA){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_UE_RESPONSABLE_PATRIMONIAL SET 
COD_ENTIDAD = '$COD_ENTIDAD',
ID_PREDIO_SBN_RESP = '$ID_PREDIO_SBN_RESP',
COD_UE_AREA_RESP = '$COD_UE_AREA_RESP',
COD_UE_OFIC_RESP = '$COD_UE_OFIC_RESP',
COD_UE_PERS_RESP = '$COD_UE_PERS_RESP',
SIMI_USUARIO_MODIFICA = '$SIMI_USUARIO_MODIFICA',
SIMI_FECHA_MODIFICA = GETDATE()
WHERE COD_UE_RESP_PATRI = '$COD_UE_RESP_PATRI'
";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Listar_UE_Responsable_Patrimonial_x_Entidad($COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){
			
			$sql="SELECT RP.*,
			RESP_DENOMINACION_PREDIO = PP.DENOMINACION_PREDIO,
			RESP_DESC_AREA = A.DESC_AREA,
			RESP_DESC_OFIC = O.DESC_OFIC,
			RESP_UE_APEMAT_PERS = P.UE_APEMAT_PERS,
			RESP_UE_APEPAT_PERS = P.UE_APEPAT_PERS,
			RESP_UE_NOMBRES_PERS = P.UE_NOMBRES_PERS,
			RESP_NOMB_COMPLETO = (P.UE_NOMBRES_PERS+', '+P.UE_APEPAT_PERS+' '+P.UE_APEMAT_PERS), 
			P.NRO_DOCUMENTO
			FROM TBL_MUEBLES_UE_RESPONSABLE_PATRIMONIAL RP
			LEFT JOIN TBL_PADRON_PREDIOS PP ON (RP.ID_PREDIO_SBN_RESP = PP.ID_PREDIO_SBN)
			LEFT JOIN TBL_MUEBLES_UE_AREA A ON (RP.COD_UE_AREA_RESP = A.COD_UE_AREA)
			LEFT JOIN TBL_MUEBLES_UE_OFICINA O ON (RP.COD_UE_OFIC_RESP = O.COD_UE_OFIC)
			LEFT JOIN TBL_MUEBLES_UE_PERSONAL P ON (RP.COD_UE_PERS_RESP = P.COD_UE_PERS)			
			WHERE RP.ID_ESTADO = '1' AND RP.COD_ENTIDAD = '$COD_ENTIDAD' ";			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Lista_Responsables_Bien_Patrimonial($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_UE_AREA, $COD_UE_OFIC){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT P.* 
				FROM TBL_MUEBLES_UE_PERSONAL P 
				WHERE P.ID_ESTADO = 1 AND P.COD_UE_PERS IN ( 
				SELECT COD_UE_PERS FROM TBL_MUEBLES_UE_RESPONSABLE_PATRIMONIAL 
				WHERE ID_ESTADO = '1' AND COD_ENTIDAD = '$COD_ENTIDAD' AND ID_PREDIO_SBN = '$ID_PREDIO_SBN' AND COD_UE_AREA = '$COD_UE_AREA' AND COD_UE_OFIC = '$COD_UE_OFIC' 
				) ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	/****************************************************************************/
		
	function Verifica_Existe_Registra_Responsable_Control_Patrimonial($COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT COUNT(*) AS TOTAL, TOT_MIGRO_INV = ISNULL((SELECT sum(ISNULL(TOTAL_BIENES_GRAL,0))
																	FROM TBL_MUEBLES_UE_INVENTARIO_ENTIDAD 
																	WHERE ID_ESTADO ='1' AND COD_ENTIDAD = '$COD_ENTIDAD' ) ,0)
					FROM TBL_MUEBLES_IMPORTAR_EXCEL_RESP 
					WHERE ID_ESTADO = '1' AND ENVIO_DAT_RESPONSABLE = 'X'  AND COD_ENTIDAD = '$COD_ENTIDAD' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}	

		
}
?>