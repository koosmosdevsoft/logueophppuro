<?
require_once("C_Interconexion_SQL.php");

class ContactenosMuebles{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
	
		
	function Generar_Contacto_Mueble(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(COD_CONTACT_MUEBLE),0) + 1) as COD_CONTACT_MUEBLE FROM TBL_WEB_CONTACTENOS_MUEBLES ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Existe_Contacto_Mueble($COD_CONTACT_MUEBLE){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT count(*) as tot_reg FROM TBL_WEB_CONTACTENOS_MUEBLES WHERE COD_CONTACT_MUEBLE = $COD_CONTACT_MUEBLE ";
			$result = $this->oDBManager->execute($sql);
			return $result;
		}
	}
	
	
	function Insertar_Contacto_Mueble($COD_CONTACT_MUEBLE, $MODULO_CONTACT_MUEBLE, $NOM_CONTACT_MUEBLE, $APE_CONTACT_MUEBLE, $INST_CONTACT_MUEBLE, $CARGO_CONTACT_MUEBLE, $TELF_CONTACT_MUEBLE, $CORREO_CONTACT_MUEBLE, $ASUNTO_CONTACT_MUEBLE, $MENSAJE_CONTACT_MUEBLE){
		if($this->oDBManager->conectar()==true){
		$consulta="INSERT INTO TBL_WEB_CONTACTENOS_MUEBLES(
COD_CONTACT_MUEBLE,
MODULO_CONTACT_MUEBLE,
NOM_CONTACT_MUEBLE,
APE_CONTACT_MUEBLE,
COD_UNID_EJEC_CONTACT_MUEBLE,
INST_CONTACT_MUEBLE,
CARGO_CONTACT_MUEBLE,
TELF_CONTACT_MUEBLE,
CORREO_CONTACT_MUEBLE,
ASUNTO_CONTACT_MUEBLE,
MENSAJE_CONTACT_MUEBLE,
FECHA_REGISTRO,
ID_ESTADO
)
VALUES(
'$COD_CONTACT_MUEBLE',
'$MODULO_CONTACT_MUEBLE',
'$NOM_CONTACT_MUEBLE',
'$APE_CONTACT_MUEBLE',
NULL,
'$INST_CONTACT_MUEBLE',
'$CARGO_CONTACT_MUEBLE',
'$TELF_CONTACT_MUEBLE',
'$CORREO_CONTACT_MUEBLE',
'$ASUNTO_CONTACT_MUEBLE',
'$MENSAJE_CONTACT_MUEBLE',
GETDATE(),
'1'
) ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	

function LISTAR_CONTACTO_MUEBLES($COD_CONTACT_MUEBLE){
		if($this->oDBManager->conectar()==true){
		$consulta="SELECT * FROM TBL_WEB_CONTACTENOS_MUEBLES 
WHERE COD_CONTACT_MUEBLE = '$COD_CONTACT_MUEBLE' AND ID_ESTADO = 1
ORDER BY ID_ESTADO ASC, FECHA_REGISTRO ASC
";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Enviar_Correo_FROM_SQLSERVER($CorreoDestinoSBN, $Asunto, $Mensaje){
		if($this->oDBManager->conectar()==true){
			//--@copy_recipients ='adacosta@sbn.gob.pe',
		 $consulta="EXEC msdb.dbo.sp_send_dbmail @profile_name='EnviarMailSQL', @recipients= '$CorreoDestinoSBN', @subject= '$Asunto', @body= '$Mensaje', @body_format = 'HTML' ;";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}	
	
	function TOTAL_REGISTRO_CONTACT_MUEBLES_X_PARAMETROS($txt_nombre, $txt_apellidos){
		if($this->oDBManager->conectar()==true){
			
			if($txt_nombre != ''){
				$nombre = " AND NOM_CONTACT_MUEBLE LIKE '%$txt_nombre%' ";
			}else{
				$nombre = "  ";
			}
			
			if($apellidos != ''){
				$apepat = " AND APE_CONTACT_MUEBLE LIKE '%$txt_apellidos%' ";
			}else{
				$apepat = "  ";
			}
			
			
			$sql="SELECT COUNT(*) AS TOT_REG FROM TBL_WEB_CONTACTENOS_MUEBLES WHERE ID_ESTADO = '1' $nombre $apellidos";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function LISTA_CONTACT_MUEBLES_X_PARAMETROS($INI, $FIN, $txt_nombre, $txt_apellidos){
		if($this->oDBManager->conectar()==true){
			
			if($txt_nombre != ''){
				$nombre = " AND NOM_CONTACT_MUEBLE LIKE '%$txt_nombre%' ";
			}else{
				$nombre = "  ";
			}
			
			if($apellidos != ''){
				$apepat = " AND APE_CONTACT_MUEBLE LIKE '%$txt_apellidos%' ";
			}else{
				$apepat = "  ";
			}
			
			$SQL_TABLA ="SELECT * ,ROW_NUMBER() OVER (ORDER BY FECHA_REGISTRO DESC ) AS ROW_NUMBER_ID
				FROM TBL_WEB_CONTACTENOS_MUEBLES WHERE ID_ESTADO = 1 $nombre $apellidos
				";
			$sql = "SELECT * FROM ( ".$SQL_TABLA." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	
}
?>