<?
require_once("C_Interconexion_SQL.php");

class Muebles_Menu_Permiso{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
	
	function GENERAR_CODIGO_MODULO_PERMISOS(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(Max(COD_MODULO_PERMISO),0)+1) as CODIGO FROM TBL_MUEBLES_MODULO_PERMISO";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function ELIMINAR_MODULO_PERMISOS(){
		if($this->oDBManager->conectar()==true){
			$sql="DELETE FROM TBL_MUEBLES_MODULO_PERMISO";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function INSERTA_MODULO_PERMISOS($COD_MODULO_PERMISO, $COD_MUEBLE_MODULO, $COD_MUEBLE_MENU, $COD_MUEBLE_MENUSUB){
		if($this->oDBManager->conectar()==true){
			$sql="INSERT INTO TBL_MUEBLES_MODULO_PERMISO(COD_MODULO_PERMISO, COD_MUEBLE_MODULO, COD_MUEBLE_MENU, COD_MUEBLE_MENUSUB, ID_ESTADO) 
				VALUES ('$COD_MODULO_PERMISO', '$COD_MUEBLE_MODULO', '$COD_MUEBLE_MENU', '$COD_MUEBLE_MENUSUB', '1')  ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function LISTA_MODULO_PERMISOS(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_MODULO_PERMISO WHERE ID_ESTADO ='1' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function LISTA_MODULO_PERMISOS_X_DEFAULT_X_MODULO($COD_MUEBLE_MODULO){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_MODULO_PERMISO WHERE ID_ESTADO ='1' AND COD_MUEBLE_MODULO = '$COD_MUEBLE_MODULO' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
		
	function GENERAR_CODIGO_Muebles_Menu_Permiso(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(Max(COD_USER_MUEBLE_PERMISO),0)+1) as CODIGO FROM TBL_MUEBLES_USUARIO_PERMISO";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Eliminar_Muebles_Menu_Permiso_x_Usuario($COD_USUARIO){
		if($this->oDBManager->conectar()==true){
			$sql="DELETE FROM TBL_MUEBLES_USUARIO_PERMISO WHERE COD_USUARIO = '$COD_USUARIO' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function Insertar_Muebles_Menu_Permiso($COD_USER_MUEBLE_PERMISO, $COD_USUARIO, $COD_MUEBLE_MODULO, $COD_MUEBLE_MENU, $COD_MUEBLE_MENUSUB){
		if($this->oDBManager->conectar()==true){
			$sql="INSERT INTO TBL_MUEBLES_USUARIO_PERMISO(COD_USER_MUEBLE_PERMISO, COD_USUARIO, COD_MUEBLE_MODULO, COD_MUEBLE_MENU, COD_MUEBLE_MENUSUB) 
				VALUES ('$COD_USER_MUEBLE_PERMISO', '$COD_USUARIO', '$COD_MUEBLE_MODULO', '$COD_MUEBLE_MENU', '$COD_MUEBLE_MENUSUB')  ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Listar_Muebles_Menu_Permiso_x_USUARIO($COD_USUARIO){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_USUARIO_PERMISO WHERE COD_USUARIO ='$COD_USUARIO' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Listar_Permisos_Modulos_x_USUARIO($COD_USUARIO){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_MODULO WHERE COD_MUEBLE_MODULO IN ( SELECT DISTINCT COD_MUEBLE_MODULO FROM TBL_MUEBLES_USUARIO_PERMISO WHERE COD_USUARIO ='$COD_USUARIO') 
				  ORDER BY ORDEN_MUEBLE_MODULO ";
				 // echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Listar_Permisos_Menu_x_USUARIO($COD_MUEBLE_MODULO, $COD_USUARIO){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_MENU WHERE COD_MUEBLE_MENU IN ( 
						SELECT DISTINCT COD_MUEBLE_MENU 
						FROM TBL_MUEBLES_USUARIO_PERMISO 
						WHERE COD_USUARIO ='$COD_USUARIO' AND COD_MUEBLE_MODULO = '$COD_MUEBLE_MODULO'
						) 
				  ORDER BY ORDEN_MUEBLE";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Listar_Permisos_SubMenu_x_USUARIO($COD_MUEBLE_MENU, $COD_USUARIO){
		if($this->oDBManager->conectar()==true){
			$sql="
SELECT * FROM TBL_MUEBLES_MENU_SUB 
WHERE ID_ESTADO ='1' 
AND COD_MUEBLE_MENUSUB IN ( 
	SELECT DISTINCT COD_MUEBLE_MENUSUB 
	FROM TBL_MUEBLES_USUARIO_PERMISO 
	WHERE COD_USUARIO ='$COD_USUARIO' AND COD_MUEBLE_MENU = '$COD_MUEBLE_MENU' 
) 
ORDER BY ORDEN_MUEBLESUB
			";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Total_Entidades_Reg_Altas_Sin_Registrar_Inv2016($COD_ENTIDAD, $X_PERIODO, $X_COD_INVENTARIO){
		if($this->oDBManager->conectar()==true){
			
			$sql="
				SELECT 
				COD_ENTIDAD,
				COUNT(*) AS TOT_REGISTRO
				FROM TBL_MUEBLES_UE_BIEN_ALTA 
				WHERE YEAR(FECHA_DOCUMENTO_ADQUIS) = '$X_PERIODO'
				AND ID_ESTADO = '1'
				AND COD_ENTIDAD  = '$COD_ENTIDAD'
				AND COD_ENTIDAD NOT IN (SELECT COD_ENTIDAD 
										FROM TBL_MUEBLES_UE_INVENTARIO_ENTIDAD 
										WHERE COD_INVENTARIO = '$X_COD_INVENTARIO' AND ID_ESTADO = '1')
										
				GROUP BY COD_ENTIDAD
			";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Listar_Menu_Permiso_Basico_Si_No_Registro_Inv($z_COD_MODULO){
		if($this->oDBManager->conectar()==true){
		
			$sql="
				SELECT * FROM TBL_MUEBLES_MENU 
				WHERE COD_MUEBLE_MENU IN ( 
					SELECT DISTINCT COD_MUEBLE_MENU 
					FROM TBL_MUEBLES_MODULO_PERMISO 
					WHERE  ID_ESTADO = '1'
					AND COD_MUEBLE_MODULO = '$z_COD_MODULO'	
				) 
				ORDER BY ORDEN_MUEBLE
			";
			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Listar_Menu_x_COD_MUEBLE_MODULO($z_COD_MODULO){
		if($this->oDBManager->conectar()==true){
							
			$sql="
			SELECT * FROM TBL_MUEBLES_MENU 
			WHERE ID_ESTADO ='1'
			AND COD_MUEBLE_MODULO = '$z_COD_MODULO'	
			ORDER BY ORDEN_MUEBLE
			";
				
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Listar_SubMenu_Permiso_Basico_Si_No_Registro_Inv($COD_MUEBLE_MENU){

		if($this->oDBManager->conectar()==true){
			
			$sql="
				SELECT * FROM TBL_MUEBLES_MENU_SUB 
				WHERE ID_ESTADO ='1' 
				AND COD_MUEBLE_MENUSUB IN ( 
					SELECT DISTINCT COD_MUEBLE_MENUSUB 
					FROM TBL_MUEBLES_MODULO_PERMISO 
					WHERE ID_ESTADO = '1'
					AND COD_MUEBLE_MENU = '$COD_MUEBLE_MENU' 
				) 
				ORDER BY ORDEN_MUEBLESUB
			";
			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Listar_SubMenu_x_COD_MUEBLE_MENU($COD_MUEBLE_MENU){
		if($this->oDBManager->conectar()==true){
							
			$sql="
			SELECT * FROM TBL_MUEBLES_MENU_SUB 
			WHERE ID_ESTADO ='1' 
			AND COD_MUEBLE_MENU = '$COD_MUEBLE_MENU'
			ORDER BY ORDEN_MUEBLESUB
			";
						
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
}
?>