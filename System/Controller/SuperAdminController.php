<?php
class SuperAdminController{
	
	const NRO_FILA_LIST	= 50;
	const NRO_LINK_PAGE = 6;

		
    public function index(){
		$data = array();	
		$data['Departamento'] 			=  array();
		$this->render('principal.tpl.php', $data);
   }

	public function Buscar_Supadmin_x_Parametros(){
		
		$objSuperAdminModel 	= new SuperAdminModel();
		$data = array();

		$nom_entidad = isset($_GET['txb_busc_nom_entidad']) ? $_GET['txb_busc_nom_entidad'] : '';			
		$ruc= isset($_GET['txb_busc_ruc']) ? utf8_encode($_GET['txb_busc_ruc']) : '';			
		$pageNumber = isset($_GET['numeroPagina']) ? $_GET['numeroPagina'] : 1;
		
		$xNRO_FILA_LIST 	= self::NRO_FILA_LIST;
		$xNRO_LINK_PAGE 	= self::NRO_LINK_PAGE;
		
		/*********************************************************/
		
		$RS = $objSuperAdminModel->TOTAL_REGISTRO_SUPERADMIN_X_PARAMETROS($nom_entidad, $ruc);
		$Total_Registros = odbc_result($RS,"TOT_REG");
		
		$Total_Paginas = ceil($Total_Registros / $xNRO_FILA_LIST);
		
		$data['pager']['indicePagina'] 		= $pageNumber;
		$data['pager']['cantidadRegistros'] = $Total_Registros;
		$data['pager']['cantidadPaginas'] 	= $Total_Paginas;	
		$data['pager']['limiteInferior'] 	= (($pageNumber - $xNRO_LINK_PAGE) <= 0) ?  1 : ($pageNumber - $xNRO_LINK_PAGE);
		$data['pager']['limiteSuperior'] 	= (($pageNumber + $xNRO_LINK_PAGE) > $Total_Paginas) ? $Total_Paginas : ($pageNumber + $xNRO_LINK_PAGE);
		$data['pager']['handlerFunction'] 	= "handle_paginar_personal";	

					
		/*********************************************************/
		
		$Item_Ini = ($pageNumber == '1')? (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST) : (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST)+1 ;
		$Item_Fin = ($pageNumber == '1')? ($Item_Ini + $xNRO_FILA_LIST) : ($Item_Ini + $xNRO_FILA_LIST)-1;


		$Result_Predios = $objSuperAdminModel->LISTA_SUPERADMIN_X_PARAMETROS($Item_Ini, $Item_Fin, $nom_entidad, $ruc);
		while (odbc_fetch_row($Result_Predios)){
						
			$local = array();

			$local['ROW_NUMBER_ID'] 			= utf8_encode(odbc_result($Result_Predios,"ROW_NUMBER_ID"));
			$local['RUC_ENTIDAD'] 			= utf8_encode(odbc_result($Result_Predios,"RUC_ENTIDAD"));
			$local['NOM_ENTIDAD'] 		= utf8_encode(odbc_result($Result_Predios,"NOM_ENTIDAD"));
			$local['NOM_USUARIO'] 			= utf8_encode(odbc_result($Result_Predios,"NOM_USUARIO"));
			$local['MOD_MUEBLES'] 	= utf8_encode(odbc_result($Result_Predios,"MOD_MUEBLES"));
			$local['MOD_INMUEBLES'] 		= utf8_encode(odbc_result($Result_Predios,"MOD_INMUEBLES"));
			$local['MOD_DU'] 		= utf8_encode(odbc_result($Result_Predios,"MOD_DU"));
			$local['NOM_CLAVE'] 		= utf8_encode(odbc_result($Result_Predios,"NOM_CLAVE"));
			$local['COD_ENTIDAD'] 		= utf8_encode(odbc_result($Result_Predios,"COD_ENTIDAD"));
			
			
			$data['Predio_Locales'][] = $local;
			
		}
		
		//$this->dump($data);
		
		/*********************************************************/
		
		//$this->dump($data);		
		//$this->dump($_SESSION);
		//$this->dump($_REQUEST);
		//require_once 'page.php';
		//require_once 'v.pri_buscar.php';
		$this->render('v.predio_listar.php', $data);
   }
   
   
   
   	

	
  /**
   * Renderiza la plantilla, con los valores indicados.
   * @param $template Nombre de la plantilla, debe ser el nombre de el archivo plantilla.
   * @param $values   Array con los datos que seran pasados a la plantilla.
   *
   * @return Template renderizado con los valores de las variables.
   */
  public function render($template, $values){
      $render = function () use ($template, $values){
          $data = $values;
          include "$template";
      };
      return $render();
  }

  /**
   * Imprime una variable respetando sangrias y espaciado, con la funcion printr.
   * @param $data Valor a imprimir en la pantalla del navegador.
   * @return Nothing.
   */
  public function dump($data){
      print '<pre>';
      print_r($data);
      print '</pre>';
  }
  
  
  public function Nombre_Carpeta($COD_TIP_DOC_PRED){
		
		if($COD_TIP_DOC_PRED == '5'){
			$carpeta = 'partida_registral';
			
		}else if($COD_TIP_DOC_PRED == '6'){
			$carpeta = 'plano_perimetrico';
			
		}else if($COD_TIP_DOC_PRED == '7'){
			$carpeta = 'plano_ubicacion';
			
		}else if($COD_TIP_DOC_PRED == '8'){
			$carpeta = 'plano_habilitacion';
			
		}else if($COD_TIP_DOC_PRED == '9'){
			$carpeta = 'tasacion';
			
		}else if($COD_TIP_DOC_PRED == '10'){
			$carpeta = 'fotografia';
			
		}else if($COD_TIP_DOC_PRED == '11'){
			$carpeta = 'escritura_publica';
			
		}else if($COD_TIP_DOC_PRED == '12'){
			$carpeta = 'memoria_descriptiva';
			
		}else if($COD_TIP_DOC_PRED == '13'){
			$carpeta = 'titulo_propiedad';
			
		}else if($COD_TIP_DOC_PRED == '14'){
			$carpeta = 'informe_tec_legal';
			
		}else if($COD_TIP_DOC_PRED == '15'){
			$carpeta = 'informe_dispos_legal';
			
		}else if($COD_TIP_DOC_PRED == '16'){
			$carpeta = 'archivo_dxf';
		}else{
			$carpeta = 'otros';
		}
		return $carpeta;
		
	}
	
	
}


?>