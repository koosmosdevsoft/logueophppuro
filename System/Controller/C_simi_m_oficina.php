<?
require_once("C_Interconexion_SQL.php");

class Simi_UE_Oficina{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
	
	
	function Simi_UE_Oficina_Generar_Codigo(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(COD_UE_OFIC),0) + 1) as CODIGO FROM TBL_MUEBLES_UE_OFICINA ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Insertar_Simi_UE_Oficina($ID_PREDIO_SBN, $COD_UE_AREA, $DESC_OFIC, $SIGLA_OFIC, $PISO_OFIC, $USUARIO_CREACION){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_UE_OFICINA(ID_PREDIO_SBN, COD_UE_AREA, DESC_OFIC, SIGLA_OFIC, PISO_OFIC, FECHA_REGISTRO, USUARIO_CREACION, FECHA_CREACION, ID_ESTADO)
			VALUES('$ID_PREDIO_SBN', '$COD_UE_AREA', '$DESC_OFIC', '$SIGLA_OFIC', '$PISO_OFIC', GETDATE(), '$USUARIO_CREACION', GETDATE(),'1') ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Actualizar_Simi_UE_Oficina($COD_UE_OFIC, $ID_PREDIO_SBN, $COD_UE_AREA, $DESC_OFIC, $SIGLA_OFIC, $PISO_OFIC, $USUARIO_MODIFICA){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_MUEBLES_UE_OFICINA 
			SET ID_PREDIO_SBN = '$ID_PREDIO_SBN', COD_UE_AREA='$COD_UE_AREA', DESC_OFIC='$DESC_OFIC', SIGLA_OFIC='$SIGLA_OFIC', PISO_OFIC='$PISO_OFIC', USUARIO_MODIFICA = '$USUARIO_MODIFICA', FECHA_MODIFICA = GETDATE() 
			WHERE COD_UE_OFIC = $COD_UE_OFIC";
			$result = $this->oDBManager->execute($sql);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Eliminar_Simi_UE_Oficina($COD_UE_OFIC, $USUARIO_ELIMINA){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_UE_OFICINA SET ID_ESTADO = 2, USUARIO_ELIMINA = '$USUARIO_ELIMINA', FECHA_ELIMINA = GETDATE() WHERE COD_UE_OFIC = '$COD_UE_OFIC' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}	


	function Eliminar_Oficina($COD_UE_OFIC, $USUARIO_ELIMINA){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_UE_OFICINA SET ID_ESTADO = 2, USUARIO_ELIMINA = '$USUARIO_ELIMINA', FECHA_ELIMINA = GETDATE() WHERE COD_UE_OFIC = '$COD_UE_OFIC' ";
			//echo $consulta;
			$result = $this->oDBManager->execute($consulta);
			//$this->oDBManager->close();
			return $result;
		}
	}	


	function Eliminar_Oficina_Validacion( $COD_ENTIDAD, $COD_UE_OFIC){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT COUNT(COD_UE_BIEN_PATRI)[CANTIDAD] FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL WHERE COD_ENTIDAD = $COD_ENTIDAD AND COD_UE_OFIC = '$COD_UE_OFIC' AND ID_ESTADO = '1' ";
			//echo $consulta; 
			$result = $this->oDBManager->execute($consulta);
			//$this->oDBManager->close();
			return $result;
		}
	}	

	function Listar_Simi_UE_Oficina_x_area($COD_UE_AREA){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_UE_OFICINA WHERE ID_ESTADO = 1 and COD_UE_AREA = '$COD_UE_AREA' ORDER BY DESC_OFIC ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function TOTAL_Simi_UE_Oficina_X_PARAMETROS($COD_UE_AREA, $DESC_OFIC){
		if($this->oDBManager->conectar()==true){
			
			if($DESC_OFIC != ''){
				$COND1 = " AND DESC_OFIC LIKE '%$DESC_OFIC%' ";
			}else{
				$COND1 = "";
			}
			
			$sql="SELECT COUNT(*) AS TOT_REG FROM TBL_MUEBLES_UE_OFICINA WHERE ID_ESTADO  = '1' AND COD_UE_AREA = '$COD_UE_AREA' $COND1 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function LISTA_Simi_UE_Oficina_X_PARAMETROS($INI,$FIN, $COD_UE_AREA, $DESC_OFIC){
		if($this->oDBManager->conectar()==true){
			
			if($DESC_OFIC != ''){
				$COND1 = " AND DESC_OFIC LIKE '%$DESC_OFIC%' ";	
			}else{
				$COND1 = "";
			}
			
			$SQL_ORIGEN="SELECT *, ROW_NUMBER() OVER (ORDER BY COD_UE_OFIC) AS ROW_NUMBER_ID
						FROM TBL_MUEBLES_UE_OFICINA
						WHERE ID_ESTADO = '1' AND COD_UE_AREA = '$COD_UE_AREA' $COND1 
				";			
			$sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
		
	function Ver_Datos_Simi_UE_Oficina_x_CODIGO($COD_UE_OFIC){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_UE_OFICINA WHERE COD_UE_OFIC = '$COD_UE_OFIC' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	
}
?>