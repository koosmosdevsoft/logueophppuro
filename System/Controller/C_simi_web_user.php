<?
require_once("C_Interconexion_SQL.php");

class Simi_Web_User{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
		
	
	function Validar_Solo_Usuario_Simi($usuario, $valida_decreto,$cod_entidad=NULL){
		if($this->oDBManager->conectar()==true){
			$msdg = '';
			if($valida_decreto == 1){
				$msdg = "AND MOD_INMUEBLES = 'X' AND COD_ENTIDAD=".$cod_entidad;
			}else{
				$msdg = '';
			}

			$sql="SELECT TOP 1 U.COD_USUARIO, U.NOM_USUARIO, count(U.COD_USUARIO) as TOT_USER
				FROM TBL_MUEBLES_USUARIO U
				WHERE U.ID_ESTADO = 1 AND U.NOM_USUARIO = '$usuario' $msdg 
				group by U.COD_USUARIO, U.NOM_USUARIO ";
			$resultado = $this->oDBManager->execute($sql);
			 //echo $sql;
			return $resultado;
		}
	}

	function Validar_Acceso_Entidad_a_Modulo($usuario, $cod_entidad=NULL){
		if($this->oDBManager->conectar()==true){

			$sql="EXEC PA_VALIDAR_ACCESO_ENTIDAD_A_MODULO '$usuario'";
			$resultado = $this->oDBManager->execute($sql);
			//echo $sql;
			return $resultado;
		}
	}

	function Validar_Acceso_Entidad_a_Modulo_X_RUC($num_ruc){
		if($this->oDBManager->conectar()==true){

			$sql="EXEC PA_VALIDAR_ACCESO_ENTIDAD_A_MODULO_RUC '$num_ruc'";
			$resultado = $this->oDBManager->execute($sql);
			//echo $sql;
			return $resultado;
		}
	}


	function Validar_RUC_SBN($ruc){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT TOP 1 U.RUC, U.NOMBRE_ENTIDAD,U.COD_ENTIDAD, count(U.ID_ENTIDADES_ACCESO_DU) as TOT_RUC, ISNULL(ID_ESTADO_CONCLUIDO,'1')[ID_ESTADO_CONCLUIDO]
			FROM TBL_ENTIDADES_ACCESO_DU U
			WHERE U.RUC = '$ruc' --AND ISNULL(ID_ESTADO_CONCLUIDO,'1') = '1' 
			GROUP BY U.RUC, U.NOMBRE_ENTIDAD,U.COD_ENTIDAD,u.ID_ESTADO_CONCLUIDO";
			$resultado = $this->oDBManager->execute($sql);
			//echo $sql;
			return $resultado;
		}
	}




	function Validar_Usuario_Simi($usuario, $clave, $Crypttext,$valida_decreto,$cod_entidad=NULL){

		$msdg = '';
		if($valida_decreto == 1){
			$msdg = "AND MOD_INMUEBLES = 'X' AND COD_ENTIDAD = ".$cod_entidad;
		}else{
			$msdg = '';
		}

		if($this->oDBManager->conectar()==true){
			$sql="SELECT count(*) as tot_reg FROM TBL_MUEBLES_USUARIO WHERE ID_ESTADO = '1' AND NOM_USUARIO = '$usuario' AND DECRYPTBYPASSPHRASE('$Crypttext', CLAVE_SQL) = '$clave' $msdg";
			 //echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
		
	function Mostrar_Datos_Usuario_Simi($usuario, $clave, $Crypttext){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT U.*, E.RUC_ENTIDAD, E.NOM_ENTIDAD 
				FROM TBL_MUEBLES_USUARIO U
				LEFT JOIN TBL_PADRON_ENTIDAD E ON (E.COD_ENTIDAD = U.COD_ENTIDAD)
				WHERE U.ID_ESTADO = '1' AND U.NOM_USUARIO = '$usuario' AND DECRYPTBYPASSPHRASE('$Crypttext', U.CLAVE_SQL) = '$clave'
			 ";
			// echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Verifica_Existe_Usuario_Simi_Activo($NOM_USUARIO, $NOM_CLAVE, $COD_USUARIO, $Crypttext){
		if($this->oDBManager->conectar()==true){
			$sql="
			SELECT count(*) AS TOT_USER 
			FROM TBL_MUEBLES_USUARIO  
			WHERE NOM_USUARIO='$NOM_USUARIO' 
			AND DECRYPTBYPASSPHRASE('$Crypttext', CLAVE_SQL) = '$NOM_CLAVE'
			AND COD_USUARIO = '$COD_USUARIO' 
			AND ID_ESTADO = 1
			";
			//echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Actualiza_Clave_Usuario_Simi($NOM_USUARIO, $NUEVO_NOM_CLAVE, $COD_USUARIO, $Crypttext){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_MUEBLES_USUARIO  
			SET CLAVE_SQL = ENCRYPTBYPASSPHRASE('$Crypttext', '$NUEVO_NOM_CLAVE' )
			WHERE NOM_USUARIO='$NOM_USUARIO' 
			AND COD_USUARIO='$COD_USUARIO' 
			AND ID_ESTADO = 1
			";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function TOTAL_USUARIOS_SIMI_WEB($ID_ESTADO, $NOM_ENTIDAD_B, $FUNCIONARIO_B, $NUM_SI_B, $NOM_USUARIO_B, $NRO_OFICIO_B, $RUC_ENTIDAD_B, $Modulo_B){
		if($this->oDBManager->conectar()==true){
			
			if($ID_ESTADO != '-'){
				$COND1 = " AND A.ID_ESTADO IN ('$ID_ESTADO') ";	
			}else{
				$COND1 = "";
			}
			
			if($NOM_ENTIDAD_B != ''){
				$COND2 = " AND E.NOM_ENTIDAD LIKE '%$NOM_ENTIDAD_B%' ";	
			}else{
				$COND2 = "";
			}
			
			if($NUM_SI_B != ''){
				$COND3 = " AND A.NUM_SI = '$NUM_SI_B' ";	
			}else{
				$COND3 = "";
			}
			
			if($NOM_USUARIO_B != ''){
				$COND4 = " AND A.NOM_USUARIO LIKE '%$NOM_USUARIO_B%' ";	
			}else{
				$COND4 = "";
			}
			
			if($FUNCIONARIO_B != ''){
				$COND5 = " AND A.FUNCIONARIO LIKE '%$FUNCIONARIO_B%' ";	
			}else{
				$COND5 = "";
			}
			
			if($NRO_OFICIO_B != ''){
				$COND6 = " AND A.NRO_OFICIO LIKE '%$NRO_OFICIO_B%' ";	
			}else{
				$COND6 = "";
			}
			
			
			if($RUC_ENTIDAD_B != ''){
				$COND7 = " AND E.RUC_ENTIDAD = '$RUC_ENTIDAD_B' ";	
			}else{
				$COND7 = "";
			}
			
			
			if($Modulo_B == 'checkbox_todos_busc'){
				
				$COND8 = "AND ( A.MOD_MUEBLES = 'X' OR A.MOD_INMUEBLES = 'X' OR ISNULL(A.MOD_PERSONALIZADO,'') = '-' ) ";
					
			}else if($Modulo_B == 'checkbox_inmuebles_busc'){
				
				$COND8 = "AND ( ISNULL(A.MOD_INMUEBLES,'') = 'X' AND ISNULL(A.MOD_MUEBLES,'') = '' AND ISNULL(A.MOD_PERSONALIZADO,'') = '' ) ";
				
			}else if($Modulo_B == 'checkbox_muebles_busc'){
				
				$COND8 = "AND ( ISNULL(A.MOD_INMUEBLES,'') = '' AND ISNULL(A.MOD_MUEBLES,'') = 'X' AND ISNULL(A.MOD_PERSONALIZADO,'') = '' ) ";
			
			}else if($Modulo_B == 'checkbox_muebleInmueble_busc'){
				
				$COND8 = "AND ( ISNULL(A.MOD_INMUEBLES,'') = 'X' AND ISNULL(A.MOD_MUEBLES,'') = 'X' AND ISNULL(A.MOD_PERSONALIZADO,'') = '' ) ";
				
			}else if($Modulo_B == 'checkbox_personaliz_busc'){
				
				$COND8 = "AND ( ISNULL(A.MOD_INMUEBLES,'') = '' AND ISNULL(A.MOD_MUEBLES,'') = '' AND ISNULL(A.MOD_PERSONALIZADO,'') = 'X' ) ";
			}else{
				
				$COND8 = "";
			}
			
			$sql="SELECT COUNT(*) AS TOT_REG FROM TBL_MUEBLES_USUARIO A 
			LEFT JOIN TBL_PADRON_ENTIDAD E ON (E.COD_ENTIDAD = A.COD_ENTIDAD)
			WHERE A.ID_ESTADO IN (1,3,4) $COND1 $COND2 $COND3 $COND4 $COND5 $COND6 $COND7 $COND8
			";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function LISTA_USUARIOS_SIMI_WEB_PAGINACION($INI,$FIN, $ID_ESTADO, $NOM_ENTIDAD_B, $FUNCIONARIO_B, $NUM_SI_B, $NOM_USUARIO_B, $NRO_OFICIO_B, $RUC_ENTIDAD_B, $Modulo_B){
		if($this->oDBManager->conectar()==true){
			
			if($ID_ESTADO != '-'){
				$COND1 = " AND A.ID_ESTADO IN ('$ID_ESTADO') ";	
			}else{
				$COND1 = "";
			}
			
			if($NOM_ENTIDAD_B != ''){
				$COND2 = " AND E.NOM_ENTIDAD LIKE '%$NOM_ENTIDAD_B%' ";	
			}else{
				$COND2 = "";
			}
			
			if($NUM_SI_B != ''){
				$COND3 = " AND A.NUM_SI = '$NUM_SI_B' ";	
			}else{
				$COND3 = "";
			}
			
			if($NOM_USUARIO_B != ''){
				$COND4 = " AND A.NOM_USUARIO LIKE '%$NOM_USUARIO_B%' ";	
			}else{
				$COND4 = "";
			}
			
			if($FUNCIONARIO_B != ''){
				$COND5 = " AND A.FUNCIONARIO LIKE '%$FUNCIONARIO_B%' ";	
			}else{
				$COND5 = "";
			}
			
			if($NRO_OFICIO_B != ''){
				$COND6 = " AND A.NRO_OFICIO LIKE '%$NRO_OFICIO_B%' ";	
			}else{
				$COND6 = "";
			}
			
			
			if($RUC_ENTIDAD_B != ''){
				$COND7 = " AND E.RUC_ENTIDAD = '$RUC_ENTIDAD_B' ";	
			}else{
				$COND7 = "";
			}
			
			
			if($Modulo_B == 'checkbox_todos_busc'){
				
				$COND8 = "AND ( A.MOD_MUEBLES = 'X' OR A.MOD_INMUEBLES = 'X' OR ISNULL(A.MOD_PERSONALIZADO,'') = '-' ) ";
					
			}else if($Modulo_B == 'checkbox_inmuebles_busc'){
				
				$COND8 = "AND ( ISNULL(A.MOD_INMUEBLES,'') = 'X' AND ISNULL(A.MOD_MUEBLES,'') = '' AND ISNULL(A.MOD_PERSONALIZADO,'') = '' ) ";
				
			}else if($Modulo_B == 'checkbox_muebles_busc'){
				
				$COND8 = "AND ( ISNULL(A.MOD_INMUEBLES,'') = '' AND ISNULL(A.MOD_MUEBLES,'') = 'X' AND ISNULL(A.MOD_PERSONALIZADO,'') = '' ) ";
			
			}else if($Modulo_B == 'checkbox_muebleInmueble_busc'){
				
				$COND8 = "AND ( ISNULL(A.MOD_INMUEBLES,'') = 'X' AND ISNULL(A.MOD_MUEBLES,'') = 'X' AND ISNULL(A.MOD_PERSONALIZADO,'') = '' ) ";
				
			}else if($Modulo_B == 'checkbox_personaliz_busc'){
				
				$COND8 = "AND ( ISNULL(A.MOD_INMUEBLES,'') = '' AND ISNULL(A.MOD_MUEBLES,'') = '' AND ISNULL(A.MOD_PERSONALIZADO,'') = 'X' ) ";
			}else{
				
				$COND8 = "";
			}
			
			
			$SQL_ORIGEN="SELECT
							A.COD_USUARIO,
							A.NOM_USUARIO,
							A.COD_ENTIDAD,
							E.RUC_ENTIDAD,
							E.NOM_ENTIDAD,
							E.SUNAT_NOM_DEPA,
							A.FUNCIONARIO,
							A.CARGO,
							A.DNI,
							A.CORREO,
							A.TELEFONO,
							A.ANEXO,
							A.NRO_OFICIO,
							FECHA_OFICIO = CONVERT(VARCHAR(10),A.FECHA_OFICIO,105),
							A.NUM_SI,
							FECHA_SI = CONVERT(VARCHAR(10),A.FECHA_SI,105),
							FECH_HABILITACION = CONVERT(VARCHAR(10),A.FECH_HABILITACION,105),
							FECH_DESACTIVACION = CONVERT(VARCHAR(10),A.FECH_DESACTIVACION,105),
							FECHA_REGISTRO = CONVERT(VARCHAR(10),A.FECHA_REGISTRO,105),
							A.ID_ESTADO,
							A.MOD_MUEBLES,
							A.MOD_INMUEBLES,
							A.MOD_PERSONALIZADO,
							ROW_NUMBER() OVER (ORDER BY A.COD_USUARIO DESC) AS ROW_NUMBER_ID
						FROM TBL_MUEBLES_USUARIO A
						LEFT JOIN TBL_PADRON_ENTIDAD E ON (E.COD_ENTIDAD = A.COD_ENTIDAD)
						WHERE A.ID_ESTADO IN (1,3,4) $COND1 $COND2 $COND3 $COND4 $COND5 $COND6 $COND7 $COND8
				";			
			$sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ORDER BY COD_USUARIO DESC ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
		
	function LISTA_USUARIOS_HABILITADO_POI_PAGINACION($INI,$FIN, $ANIO, $MES, $OPCION){
		if($this->oDBManager->conectar()==true){
			
			
			if($MES != ''){
				$COND_MES	= " AND MONTH(FECH_HABILITACION) 	= '$MES' ";
			}else{
				$COND_MES	= "";
			}
			
			
			$SQL_PURO_MUEBLES ="
						SELECT *, 
						SISTEMA = 'MUEBLES', 
						cod_documento = 'MBM_COD_'+CAST(COD_USUARIO AS VARCHAR(10))
						FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= 'X' AND ISNULL(MOD_INMUEBLES,'') = '') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";
							
			$SQL_PURO_INMUEBLES ="
						SELECT *, 
						SISTEMA = 'INMUEBLES', 
						cod_documento = 'MBI_COD_'+CAST(COD_USUARIO AS VARCHAR(10))
						FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= '' AND ISNULL(MOD_INMUEBLES,'') = 'X') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";

			$SQL_MUEB_INMUEB ="
						SELECT *, 
						SISTEMA = 'MUEBLES E INMUEBLES' ,
						cod_documento = 'MBM_MBI_COD_'+CAST(COD_USUARIO AS VARCHAR(10))
						FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= 'X' AND ISNULL(MOD_INMUEBLES,'') = 'X') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";
						
			$SQL_MUEB_INMUEB_MUEBLES ="
						SELECT *, 
						SISTEMA = 'MUEBLES',
						cod_documento = 'MBM_COD_'+CAST(COD_USUARIO AS VARCHAR(10))
						FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= 'X' AND ISNULL(MOD_INMUEBLES,'') = 'X') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";
			
			$SQL_MUEB_INMUEB_INMUEBLES ="
						SELECT *, 
						SISTEMA = 'INMUEBLES',
						cod_documento = 'MBI_COD_'+CAST(COD_USUARIO AS VARCHAR(10))
						FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= 'X' AND ISNULL(MOD_INMUEBLES,'') = 'X') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";
			
			
			if($OPCION == 'checkbox_muebles'){
				
				$TABLA_USUARIO = "
					SELECT * FROM (
						( ".$SQL_PURO_MUEBLES." ) 
						UNION
						( ".$SQL_MUEB_INMUEB_MUEBLES." ) 
					) TBL 
				";
				// print_r($TABLA_USUARIO);
			}else if($OPCION == 'checkbox_inmuebles'){
				
				$TABLA_USUARIO = "
					SELECT * FROM (
						( ".$SQL_PURO_INMUEBLES." ) 
						UNION
						( ".$SQL_MUEB_INMUEB_INMUEBLES." ) 
					) TBL 
				";
				
			}else if($OPCION == 'checkbox_muebles_inmuebles'){
				
				$TABLA_USUARIO = $SQL_MUEB_INMUEB;
				
			}else if($OPCION == 'checkbox_Todo'){
				
				$TABLA_USUARIO = "
						( ".$SQL_PURO_MUEBLES." ) 
						UNION
						( ".$SQL_PURO_INMUEBLES." ) 
						UNION
						( ".$SQL_MUEB_INMUEB_MUEBLES." ) 
						UNION
						( ".$SQL_MUEB_INMUEB_INMUEBLES." ) 
				";
				
			} 
			
			
			$SQL_ORIGEN = "
			SELECT
				ROW_NUMBER() OVER (ORDER BY U.FECH_HABILITACION) AS ITEM,
				U.NOM_USUARIO, 
				E.NOM_ENTIDAD,
				U.FUNCIONARIO,
				U.NUM_SI,
				U.FECHA_SI,
				U.NRO_OFICIO,
				U.FECHA_OFICIO,
				FECHA_HABILITADO = CAST(U.FECH_HABILITACION AS DATE),
				SISTEMA,
				cod_documento,
				RESPONSABLE = (
				
					SELECT top 1 p.loggin
					FROM SRVBDP.sbndb.DBO.tes_documentos POI, SRVBDP.sbndb.DBO.personal P
					WHERE POI.anho_proceso			=	YEAR(U.FECH_HABILITACION)
					AND POI.tipo_registro			=	'SW' 
					AND POI.tipo_documento			=	'0202'
					AND POI.dsc_documento			=	'REMISION DE SINABIP WEB'
					AND POI.codigo_direccion		=	'054'
					AND POI.usuario_edicion			=	P.codigo_personal
					AND POI.cod_documento			=	U.cod_documento
					AND POI.est_dcto				=	'H'
					
				),
				FECHA_POI = (
				
					SELECT top 1 convert(varchar(10),POI.fecha_documento,105)
					FROM SRVBDP.sbndb.DBO.tes_documentos POI
					WHERE POI.anho_proceso			=	YEAR(U.FECH_HABILITACION)
					AND POI.tipo_registro			=	'SW' 
					AND POI.tipo_documento			=	'0202'
					AND POI.dsc_documento			=	'REMISION DE SINABIP WEB'
					AND POI.codigo_direccion		=	'054'
					AND POI.cod_documento			=	U.cod_documento
					AND POI.est_dcto				=	'H'
					
				),
				ANIO = YEAR(U.FECH_HABILITACION),
				MES = MONTH(U.FECH_HABILITACION),
				MES_DESC = UPPER(DATENAME(MONTH, U.FECH_HABILITACION)) 
			FROM ( ".$TABLA_USUARIO." ) U 
			LEFT JOIN TBL_PADRON_ENTIDAD E ON (U.COD_ENTIDAD = E.COD_ENTIDAD)
			WHERE U.ID_ESTADO = '1' 
			AND U.COD_ENTIDAD IS NOT NULL
			";

			$sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TABLEWITHROW_NUMBER WHERE ITEM BETWEEN $INI AND $FIN ";
			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	
	function TOTAL_USUARIOS_HABILITADO_POI_PAGINACION($ANIO, $MES, $OPCION){
		if($this->oDBManager->conectar()==true){
				
			
			if($MES != ''){
				$COND_MES	= " AND MONTH(FECH_HABILITACION) = '$MES' ";
			}else{
				$COND_MES	= "";
			}
			
			
			$SQL_PURO_MUEBLES ="
						SELECT *, SISTEMA = 'MUEBLES' FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= 'X' AND ISNULL(MOD_INMUEBLES,'') = '') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";
							
			$SQL_PURO_INMUEBLES ="
						SELECT *, SISTEMA = 'INMUEBLES' FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= '' AND ISNULL(MOD_INMUEBLES,'') = 'X') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";

			$SQL_MUEB_INMUEB ="
						SELECT *, SISTEMA = 'MUEBLES E INMUEBLES' FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= 'X' AND ISNULL(MOD_INMUEBLES,'') = 'X') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";
						
			$SQL_MUEB_INMUEB_MUEBLES ="
						SELECT *, SISTEMA = 'MUEBLES' FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= 'X' AND ISNULL(MOD_INMUEBLES,'') = 'X') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";
			
			$SQL_MUEB_INMUEB_INMUEBLES ="
						SELECT *, SISTEMA = 'INMUEBLES' FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= 'X' AND ISNULL(MOD_INMUEBLES,'') = 'X') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";
			
			
			if($OPCION == 'checkbox_muebles'){
				
				$TABLA_USUARIO = "
					SELECT * FROM (
						( ".$SQL_PURO_MUEBLES." ) 
						UNION
						( ".$SQL_MUEB_INMUEB_MUEBLES." ) 
					) TBL 
				";
				
			}else if($OPCION == 'checkbox_inmuebles'){
				
				$TABLA_USUARIO = "
					SELECT * FROM (
						( ".$SQL_PURO_INMUEBLES." ) 
						UNION
						( ".$SQL_MUEB_INMUEB_INMUEBLES." ) 
					) TBL 
				";
				
			}else if($OPCION == 'checkbox_muebles_inmuebles'){
				
				$TABLA_USUARIO = $SQL_MUEB_INMUEB;
				
			}else if($OPCION == 'checkbox_Todo'){
				
				$TABLA_USUARIO = "
						( ".$SQL_PURO_MUEBLES." ) 
						UNION
						( ".$SQL_PURO_INMUEBLES." ) 
						UNION
						( ".$SQL_MUEB_INMUEB_MUEBLES." ) 
						UNION
						( ".$SQL_MUEB_INMUEB_INMUEBLES." ) 
				";
				
			} 
			
			
			$SQL_ORIGEN = "
			SELECT
				TOT_REG = COUNT(*)
			FROM ( ".$TABLA_USUARIO." ) U 
			LEFT JOIN TBL_PADRON_ENTIDAD E ON (U.COD_ENTIDAD = E.COD_ENTIDAD)
			WHERE U.ID_ESTADO = '1' 
			AND U.COD_ENTIDAD IS NOT NULL
			";
					
			$resultado = $this->oDBManager->execute($SQL_ORIGEN);
			return $resultado;
		}
	}
	
	
	
	
	
	function LISTA_USUARIOS_HABILITADO_EN_GENERAL($ANIO, $MES, $OPCION){
		if($this->oDBManager->conectar()==true){
			
			if($MES != ''){
				$COND_MES	= " AND MONTH(FECH_HABILITACION) 	= '$MES' ";
			}else{
				$COND_MES	= "";
			}
			
			
			$SQL_PURO_MUEBLES ="
						SELECT *, 
						SISTEMA = 'MUEBLES', 
						cod_documento = 'MBM_COD_'+CAST(COD_USUARIO AS VARCHAR(10))
						FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= 'X' AND ISNULL(MOD_INMUEBLES,'') = '') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";
							
			$SQL_PURO_INMUEBLES ="
						SELECT *, 
						SISTEMA = 'INMUEBLES', 
						cod_documento = 'MBI_COD_'+CAST(COD_USUARIO AS VARCHAR(10))
						FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= '' AND ISNULL(MOD_INMUEBLES,'') = 'X') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";

			$SQL_MUEB_INMUEB ="
						SELECT *, 
						SISTEMA = 'MUEBLES E INMUEBLES' ,
						cod_documento = 'MBM_MBI_COD_'+CAST(COD_USUARIO AS VARCHAR(10))
						FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= 'X' AND ISNULL(MOD_INMUEBLES,'') = 'X') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";
						
			$SQL_MUEB_INMUEB_MUEBLES ="
						SELECT *, 
						SISTEMA = 'MUEBLES',
						cod_documento = 'MBM_COD_'+CAST(COD_USUARIO AS VARCHAR(10))
						FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= 'X' AND ISNULL(MOD_INMUEBLES,'') = 'X') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";
			
			$SQL_MUEB_INMUEB_INMUEBLES ="
						SELECT *, 
						SISTEMA = 'INMUEBLES',
						cod_documento = 'MBI_COD_'+CAST(COD_USUARIO AS VARCHAR(10))
						FROM TBL_MUEBLES_USUARIO
						WHERE ID_ESTADO 				= '1' 
						AND (ISNULL(MOD_MUEBLES,'') 	= 'X' AND ISNULL(MOD_INMUEBLES,'') = 'X') 
						AND YEAR(FECH_HABILITACION) 	= '$ANIO' 
						$COND_MES
						";
			
			
			if($OPCION == 'checkbox_muebles'){
				
				$TABLA_USUARIO = "
					SELECT * FROM (
						( ".$SQL_PURO_MUEBLES." ) 
						UNION
						( ".$SQL_MUEB_INMUEB_MUEBLES." ) 
					) TBL 
				";
				
			}else if($OPCION == 'checkbox_inmuebles'){
				
				$TABLA_USUARIO = "
					SELECT * FROM (
						( ".$SQL_PURO_INMUEBLES." ) 
						UNION
						( ".$SQL_MUEB_INMUEB_INMUEBLES." ) 
					) TBL 
				";
				
			}else if($OPCION == 'checkbox_muebles_inmuebles'){
				
				$TABLA_USUARIO = $SQL_MUEB_INMUEB;
				
			}else if($OPCION == 'checkbox_Todo'){
				
				$TABLA_USUARIO = "
						( ".$SQL_PURO_MUEBLES." ) 
						UNION
						( ".$SQL_PURO_INMUEBLES." ) 
						UNION
						( ".$SQL_MUEB_INMUEB_MUEBLES." ) 
						UNION
						( ".$SQL_MUEB_INMUEB_INMUEBLES." ) 
				";
				
			} 
			
			
			$SQL_ORIGEN = "
			SELECT
				ROW_NUMBER() OVER (ORDER BY U.FECH_HABILITACION) AS ITEM,
				U.NOM_USUARIO, 
				E.NOM_ENTIDAD,
				U.FUNCIONARIO,
				U.NUM_SI,
				U.FECHA_SI,
				U.NRO_OFICIO,
				U.FECHA_OFICIO,
				FECHA_HABILITADO = CAST(U.FECH_HABILITACION AS DATE),
				SISTEMA,
				RESPONSABLE = (
				
					SELECT top 1 p.loggin
					FROM SRVBDP.sbndb.DBO.tes_documentos POI, SRVBDP.SBNDB.DBO.personal P
					WHERE POI.anho_proceso			=	YEAR(U.FECH_HABILITACION)
					AND POI.tipo_registro			=	'SW' 
					AND POI.tipo_documento			=	'0202'
					AND POI.dsc_documento			=	'REMISION DE SINABIP WEB'
					AND POI.codigo_direccion		=	'054'
					AND POI.usuario_edicion			=	P.codigo_personal
					AND POI.cod_documento			=	U.cod_documento
					AND POI.est_dcto				=	'H'
					
				),
				FECHA_POI = (
				
					SELECT top 1 convert(varchar(10),POI.fecha_documento,105)
					FROM SRVBDP.sbndb.DBO.tes_documentos POI
					WHERE POI.anho_proceso			=	YEAR(U.FECH_HABILITACION)
					AND POI.tipo_registro			=	'SW' 
					AND POI.tipo_documento			=	'0202'
					AND POI.dsc_documento			=	'REMISION DE SINABIP WEB'
					AND POI.codigo_direccion		=	'054'
					AND POI.cod_documento			=	U.cod_documento
					AND POI.est_dcto				=	'H'
					
				),
				ANIO = YEAR(U.FECH_HABILITACION),
				MES = MONTH(U.FECH_HABILITACION),
				MES_DESC = UPPER(DATENAME(MONTH, U.FECH_HABILITACION)) 
			FROM ( ".$TABLA_USUARIO." ) U 
			LEFT JOIN TBL_PADRON_ENTIDAD E ON (U.COD_ENTIDAD = E.COD_ENTIDAD)
			WHERE U.ID_ESTADO = '1' 
			AND U.COD_ENTIDAD IS NOT NULL
			";
			
			$resultado = $this->oDBManager->execute($SQL_ORIGEN);
			return $resultado;
		}
	}
	
	
	function TOTAL_USUARIOS_HABILITADOS_SINABIP_X_ANIO($ANIO){
		if($this->oDBManager->conectar()==true){
			
			
			$sql="
			
	SELECT
*,
TOT_GRAL_USUARIO = (TOT_GRAL_MUEBLES+TOT_GRAL_INMUEBLES)
FROM (
	SELECT
	MES				= M.ID_MES,
	MES_DESC		= M.DES_MES,
	TOT_SOLO_MUEBLES	= (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO UM WHERE UM.ID_ESTADO = '1' AND YEAR(UM.FECH_HABILITACION) = $ANIO AND M.ID_MES = MONTH(UM.FECH_HABILITACION) AND ISNULL(UM.MOD_MUEBLES,'') = 'X' AND ISNULL(UM.MOD_INMUEBLES,'') = ''),
	TOT_SOLO_INMUEBLES	= (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO UI WHERE UI.ID_ESTADO = '1' AND YEAR(UI.FECH_HABILITACION) = $ANIO AND M.ID_MES = MONTH(UI.FECH_HABILITACION) AND ISNULL(UI.MOD_MUEBLES,'') = ''  AND ISNULL(UI.MOD_INMUEBLES,'') = 'X'),
	TOT_SOLO_MUE_INM	= (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO UX WHERE UX.ID_ESTADO = '1' AND YEAR(UX.FECH_HABILITACION) = $ANIO AND M.ID_MES = MONTH(UX.FECH_HABILITACION) AND ISNULL(UX.MOD_MUEBLES,'') = 'X' AND ISNULL(UX.MOD_INMUEBLES,'') = 'X'),
	TOT_GRAL_MUEBLES	= (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO UMG WHERE UMG.ID_ESTADO = '1' AND YEAR(UMG.FECH_HABILITACION) = $ANIO AND M.ID_MES = MONTH(UMG.FECH_HABILITACION) AND ISNULL(UMG.MOD_MUEBLES,'') = 'X'),
	TOT_GRAL_INMUEBLES	= (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO UIG WHERE UIG.ID_ESTADO = '1' AND YEAR(UIG.FECH_HABILITACION) = $ANIO AND M.ID_MES = MONTH(UIG.FECH_HABILITACION) AND ISNULL(UIG.MOD_INMUEBLES,'') = 'X')
	FROM TBL_SBN_MESES M
	GROUP BY 
	M.ID_MES,
	M.DES_MES
) TBL 
ORDER BY TBL.MES";			
			//echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function TOTAL_USUARIOS_SINABIP_FILTRO_FORMA1($SISTEMA, $ANIO){
		if($this->oDBManager->conectar()==true){
			
			
			$sql="
SELECT
SISTEMA,
ANIO,
MES,
COUNT(*) AS TOT_REG
FROM(
SELECT
ANIO = YEAR(U.FECH_HABILITACION),
MES = MONTH(U.FECH_HABILITACION),
FECHA_HABILITADO = CAST(U.FECH_HABILITACION AS DATE),
SISTEMA = CASE WHEN (ISNULL(U.MOD_MUEBLES,'') = 'X' AND ISNULL(MOD_INMUEBLES,'') = '' ) THEN 'MODULO MUEBLES' 
			   WHEN (ISNULL(U.MOD_MUEBLES,'') = '' AND  ISNULL(U.MOD_INMUEBLES,'') = 'X') THEN 'MODULO INMUEBLES' 
			   WHEN (ISNULL(U.MOD_MUEBLES,'') = 'X' AND  ISNULL(U.MOD_INMUEBLES,'') = 'X') THEN 'MODULO MUEBLES E INMUEBLES' 
			   ELSE 'NINGUNO' END
FROM TBL_MUEBLES_USUARIO U
LEFT JOIN TBL_PADRON_ENTIDAD E ON (U.COD_ENTIDAD = E.COD_ENTIDAD)
WHERE U.ID_ESTADO = '1' 
AND U.COD_ENTIDAD IS NOT NULL
) TBL
WHERE SISTEMA = '$SISTEMA' AND ANIO = '$ANIO'
GROUP BY 
SISTEMA,
ANIO,
MES
ORDER BY ANIO, MES
				";			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function TOTAL_USUARIOS_SINABIP_FILTRO_FORMA2($SISTEMA, $ANIO){
		if($this->oDBManager->conectar()==true){
			
			
			$sql="

SELECT
M.COD_MES,
M.NOM_MES,
TBL2.ANIO,
TBL2.SISTEMA,
TOT_REG = ISNULL(TBL2.TOT_REG,0)
FROM TBL_MESES M
LEFT JOIN (
SELECT ANIO, MES, SISTEMA, COUNT(*) AS TOT_REG
FROM (
SELECT
ANIO = YEAR(U.FECH_HABILITACION),
MES = MONTH(U.FECH_HABILITACION),
FECHA_HABILITADO = CAST(U.FECH_HABILITACION AS DATE),
SISTEMA = CASE WHEN (ISNULL(U.MOD_MUEBLES,'') = 'X' AND  ISNULL(U.MOD_INMUEBLES,'') = '') THEN 'MODULO MUEBLES' 
			   WHEN (ISNULL(U.MOD_MUEBLES,'') = '' AND  ISNULL(U.MOD_INMUEBLES,'') = 'X') THEN 'MODULO INMUEBLES' 
			   WHEN (ISNULL(U.MOD_MUEBLES,'') = 'X' AND  ISNULL(U.MOD_INMUEBLES,'') = 'X') THEN 'MODULO MUEBLES E INMUEBLES' 
			   ELSE 'NINGUNO' END
FROM TBL_MUEBLES_USUARIO U
WHERE U.ID_ESTADO = '1' AND U.COD_ENTIDAD IS NOT NULL
) TBL
WHERE SISTEMA = '$SISTEMA' AND ANIO = '$ANIO'
GROUP BY ANIO, MES, SISTEMA
) TBL2 ON (TBL2.MES = M.COD_MES)
				";			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function LISTA_USUARIOS_SIMI_WEB_EXCEL($ID_ESTADO){
		if($this->oDBManager->conectar()==true){
			
			if($ID_ESTADO != '-'){
				$COND1 = " AND A.ID_ESTADO IN ('$ID_ESTADO') ";	
			}else{
				$COND1 = "";
			}
			
			
			$sql="SELECT
							A.COD_USUARIO,
							A.NOM_USUARIO,
							A.COD_ENTIDAD,
							E.RUC_ENTIDAD,
							E.NOM_ENTIDAD,
							A.FUNCIONARIO,
							A.CARGO,
							A.DNI,
							A.CORREO,
							A.TELEFONO,
							A.ANEXO,
							A.NRO_OFICIO,
							FECHA_OFICIO = CONVERT(VARCHAR(10),A.FECHA_OFICIO,105),
							A.NUM_SI,
							FECHA_SI = CONVERT(VARCHAR(10),A.FECHA_SI,105),
							FECH_HABILITACION = CONVERT(VARCHAR(10),A.FECH_HABILITACION,105),
							FECH_DESACTIVACION = CONVERT(VARCHAR(10),A.FECH_DESACTIVACION,105),
							FECHA_REGISTRO = CONVERT(VARCHAR(10),A.FECHA_REGISTRO,105),
							A.ID_ESTADO,
							A.MOD_MUEBLES,
							A.MOD_INMUEBLES
						FROM TBL_MUEBLES_USUARIO A
						LEFT JOIN TBL_PADRON_ENTIDAD E ON (E.COD_ENTIDAD = A.COD_ENTIDAD)
						WHERE A.ID_ESTADO IN (1,3,4) $COND1 
				";			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Existe_Usuario_SIMI($NOM_USUARIO){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT count(*) as tot_user FROM TBL_MUEBLES_USUARIO  where NOM_USUARIO='$NOM_USUARIO' AND ID_ESTADO IN (1,3) ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Existe_Usuario_SIMI_CODIGO_Y_LOGGIN($COD_USUARIO, $NOM_USUARIO){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT count(*) as tot_user_cod_y_login FROM TBL_MUEBLES_USUARIO  where COD_USUARIO = '$COD_USUARIO' and NOM_USUARIO='$NOM_USUARIO' AND ID_ESTADO IN (1,3) ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Ver_Datos_Usuario_SIMI_x_CODIGO_2($COD_USUARIO){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT U.*, 
E.RUC_ENTIDAD,
E.NOM_ENTIDAD,
E.SUNAT_NOM_DEPA
FROM TBL_MUEBLES_USUARIO U
LEFT JOIN TBL_PADRON_ENTIDAD E ON (E.COD_ENTIDAD = U.COD_ENTIDAD)
WHERE U.COD_USUARIO = '$COD_USUARIO'
				";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Ver_Datos_Usuario_SIMI_x_CODIGO_y_Crypttext($COD_USUARIO, $Crypttext){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT U.*, 
E.RUC_ENTIDAD,
E.NOM_ENTIDAD,
CLAVE_SQL_Ok = CAST(DECRYPTBYPASSPHRASE('$Crypttext', U.CLAVE_SQL) AS VARCHAR(8000))
FROM TBL_MUEBLES_USUARIO U
LEFT JOIN TBL_PADRON_ENTIDAD E ON (E.COD_ENTIDAD = U.COD_ENTIDAD)
WHERE U.COD_USUARIO = '$COD_USUARIO'
				";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function GENERAR_CODIGO_USER_SIMI_WEB(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(Max(COD_USUARIO),0)+1) as CODIGO FROM TBL_MUEBLES_USUARIO";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Insertar_Datos_Usuario_SIMI_Web($COD_USUARIO, $NOM_USUARIO, $NOM_CLAVE, $COD_ENTIDAD, $X_INSTITUCION, $NUM_SI, $FECHA_SI, $SOLICITA, $NRO_OFICIO, $FECHA_OFICIO, $FIRMA_OFICIO, $FUNCIONARIO, $CARGO, $DNI, $CORREO, $TELEFONO, $CELULAR, $ANEXO, $FECH_HABILITACION, $FECH_DESACTIVACION, $USUARIO_CREACION, $ID_ESTADO, $MOD_MUEBLES, $MOD_INMUEBLES, $MOD_PERSONALIZADO, $Crypttext){


		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_USUARIO (COD_USUARIO, NOM_USUARIO, NOM_CLAVE, COD_ENTIDAD, X_INSTITUCION, NUM_SI, FECHA_SI, SOLICITA, NRO_OFICIO, FECHA_OFICIO, FIRMA_OFICIO, FUNCIONARIO, CARGO, DNI, CORREO, TELEFONO, CELULAR, ANEXO, FECH_HABILITACION, FECH_DESACTIVACION, FECHA_REGISTRO, USUARIO_CREACION, FECHA_CREACION, ID_ESTADO, MOD_MUEBLES, MOD_INMUEBLES, MOD_PERSONALIZADO, CLAVE_SQL )
VALUES ('$COD_USUARIO', '$NOM_USUARIO', '$NOM_CLAVE', '$COD_ENTIDAD', '$X_INSTITUCION', '$NUM_SI', '$FECHA_SI', '$SOLICITA', '$NRO_OFICIO', '$FECHA_OFICIO', '$FIRMA_OFICIO', '$FUNCIONARIO', '$CARGO', '$DNI', '$CORREO', '$TELEFONO', '$CELULAR', '$ANEXO', $FECH_HABILITACION, $FECH_DESACTIVACION, GETDATE(), '$USUARIO_CREACION', GETDATE(), '$ID_ESTADO', '$MOD_MUEBLES', '$MOD_INMUEBLES', '$MOD_PERSONALIZADO', ENCRYPTBYPASSPHRASE('$Crypttext', '$NOM_CLAVE' ))";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	
	
	function Actualiza_Datos_Usuario_SIMI_Web($COD_USUARIO, $NOM_USUARIO, $NOM_CLAVE, $COD_ENTIDAD, $X_INSTITUCION, $NUM_SI, $FECHA_SI, $SOLICITA, $NRO_OFICIO, $FECHA_OFICIO, $FIRMA_OFICIO, $FUNCIONARIO, $CARGO, $DNI, $CORREO, $TELEFONO, $CELULAR, $ANEXO, $FECH_HABILITACION, $FECH_DESACTIVACION, $USUARIO_MODIFICA, $ID_ESTADO, $MOD_MUEBLES, $MOD_INMUEBLES, $MOD_PERSONALIZADO, $Crypttext){
		if($this->oDBManager->conectar()==true){
			$consulta="
			SET DATEFORMAT YMD 
			UPDATE TBL_MUEBLES_USUARIO  SET 
NOM_USUARIO = '$NOM_USUARIO',
CLAVE_SQL = ENCRYPTBYPASSPHRASE('$Crypttext', '$NOM_CLAVE' ),
COD_ENTIDAD = '$COD_ENTIDAD',
X_INSTITUCION = '$X_INSTITUCION',
NUM_SI = '$NUM_SI',
FECHA_SI = '$FECHA_SI',
SOLICITA = '$SOLICITA',
NRO_OFICIO = '$NRO_OFICIO',
FECHA_OFICIO = '$FECHA_OFICIO',
FIRMA_OFICIO = '$FIRMA_OFICIO',
FUNCIONARIO = '$FUNCIONARIO',
CARGO = '$CARGO',
DNI = '$DNI',
CORREO = '$CORREO',
TELEFONO = '$TELEFONO',
CELULAR = '$CELULAR',
ANEXO = '$ANEXO',
$FECH_HABILITACION
FECH_DESACTIVACION = $FECH_DESACTIVACION,
USUARIO_MODIFICA = '$USUARIO_MODIFICA',
FECHA_MODIFICA = GETDATE(),
ID_ESTADO = '$ID_ESTADO',
MOD_MUEBLES = '$MOD_MUEBLES',
MOD_INMUEBLES = '$MOD_INMUEBLES',
MOD_PERSONALIZADO = '$MOD_PERSONALIZADO'
WHERE  COD_USUARIO = '$COD_USUARIO'
";
 // print_r($consulta );
 // die;
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Eliminar_Usuario_SIMI_Web($COD_USUARIO){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_USUARIO SET ID_ESTADO = '2' WHERE COD_USUARIO = '$COD_USUARIO' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Verifica_Total_Usuarios_Pendiente_Con_SI($NUM_SI){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT NUM_SI, COUNT(*) AS TOTAL_USER FROM TBL_MUEBLES_USUARIO
					WHERE ID_ESTADO IN (4) AND NUM_SI IS NOT NULL AND NUM_SI='$NUM_SI'
					GROUP BY NUM_SI ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Verifica_Total_Usuarios_Autorizados_Con_SI($NUM_SI){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT NUM_SI, COUNT(*) AS TOTAL_USER FROM TBL_MUEBLES_USUARIO
					WHERE ID_ESTADO IN (1) AND NUM_SI IS NOT NULL AND NUM_SI='$NUM_SI' AND ISNULL(COD_ENTIDAD,'') != ''
					GROUP BY NUM_SI ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Verifica_Total_Usuarios_Autorizados_Con_SI_entidad_null($NUM_SI){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT NUM_SI, COUNT(*) AS TOTAL_USER 
					FROM TBL_MUEBLES_USUARIO
					WHERE ID_ESTADO IN (1) AND NUM_SI IS NOT NULL AND NUM_SI='$NUM_SI'
					GROUP BY NUM_SI ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Verifica_Total_Usuarios_Autorizados_Con_SI_entidad_x_INMUEBLES($NUM_SI){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT NUM_SI, COUNT(*) AS TOTAL_USER 
					FROM TBL_MUEBLES_USUARIO
					WHERE ID_ESTADO IN (1) 
					AND NUM_SI IS NOT NULL 
					AND NUM_SI='$NUM_SI'
					AND MOD_INMUEBLES = 'X'
					GROUP BY NUM_SI ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Verifica_Total_Usuarios_Autorizados_Con_SI_entidad_x_MUEBLES($NUM_SI){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT NUM_SI, COUNT(*) AS TOTAL_USER 
					FROM TBL_MUEBLES_USUARIO
					WHERE ID_ESTADO IN (1) 
					AND NUM_SI IS NOT NULL 
					AND NUM_SI='$NUM_SI'
					AND MOD_MUEBLES = 'X'
					GROUP BY NUM_SI ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Total_Usuarios_CON_SI($NUM_SI){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT NUM_SI, COUNT(*) AS TOTAL_USER FROM TBL_MUEBLES_USUARIO
					WHERE ID_ESTADO IN (1,3,4) AND NUM_SI IS NOT NULL AND NUM_SI='$NUM_SI'
					GROUP BY NUM_SI ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	
	function Ver_Datos_Usuario_CON_SI($NUM_SI){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT * FROM TBL_MUEBLES_USUARIO WHERE ID_ESTADO IN (1,3,4) AND NUM_SI = '$NUM_SI' ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function INSERTAR_POI_USUARIO_SINABIP_WEB($COD_USER){
		if($this->oDBManager->conectar()==true){
			$sql="EXEC USP_SBN_REGISTRAR_USER_POI_SINABIP_WEB '$COD_USER' ";
			//echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Actualiza_Datos_Beneficiario_Usuario_SIMI($COD_USUARIO, $COD_ENTIDAD, $X_INSTITUCION, $NUM_SI, $FECHA_SI, $SOLICITA, $NRO_OFICIO, $FECHA_OFICIO, $FIRMA_OFICIO, $FUNCIONARIO, $CARGO, $DNI, $CORREO, $TELEFONO, $ANEXO, $MOD_MUEBLES, $MOD_INMUEBLES){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_USUARIO  SET 
COD_ENTIDAD = '$COD_ENTIDAD',
X_INSTITUCION ='$X_INSTITUCION',
FECHA_SI = '$FECHA_SI',
SOLICITA = '$SOLICITA',
NRO_OFICIO = '$NRO_OFICIO',
FECHA_OFICIO = '$FECHA_OFICIO',
FIRMA_OFICIO = '$FIRMA_OFICIO',
FUNCIONARIO = '$FUNCIONARIO',
CARGO = '$CARGO',
DNI = '$DNI',
CORREO = '$CORREO',
TELEFONO = '$TELEFONO',
ANEXO = '$ANEXO',
MOD_MUEBLES = '$MOD_MUEBLES',
MOD_INMUEBLES = '$MOD_INMUEBLES',
FECH_HABILITACION = NULL,
FECHA_MODIFICA = GETDATE(),
ID_ESTADO = '4'
WHERE  COD_USUARIO = '$COD_USUARIO'
";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Inserta_Datos_Beneficiario_Usuario_SIMI($COD_USUARIO, $COD_ENTIDAD, $X_INSTITUCION, $NUM_SI, $FECHA_SI, $SOLICITA, $NRO_OFICIO, $FECHA_OFICIO, $FIRMA_OFICIO, $FUNCIONARIO, $CARGO, $DNI, $CORREO, $TELEFONO, $ANEXO, $MOD_MUEBLES, $MOD_INMUEBLES){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_USUARIO (COD_USUARIO, COD_ENTIDAD, X_INSTITUCION, NUM_SI, FECHA_SI, SOLICITA, NRO_OFICIO, FECHA_OFICIO, FIRMA_OFICIO, FUNCIONARIO, CARGO, DNI, CORREO, TELEFONO, ANEXO, FECHA_REGISTRO, FECHA_CREACION, ID_ESTADO, MOD_MUEBLES, MOD_INMUEBLES) VALUES ('$COD_USUARIO', '$COD_ENTIDAD', '$X_INSTITUCION', '$NUM_SI', '$FECHA_SI', '$SOLICITA', '$NRO_OFICIO', '$FECHA_OFICIO', '$FIRMA_OFICIO', '$FUNCIONARIO', '$CARGO', '$DNI', '$CORREO', '$TELEFONO', '$ANEXO', GETDATE(), GETDATE(), '4', '$MOD_MUEBLES', '$MOD_INMUEBLES' )";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Desactiva_Usuario_SIMI_x_Mas_SI($NUM_SI){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_USUARIO SET ID_ESTADO = '3', FECH_HABILITACION = NULL, FECH_DESACTIVACION = GETDATE()  WHERE NUM_SI = '$NUM_SI' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}

	function GENERAR_CODIGO_MUEBLES_USUARIO_CONTACTENOS(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(Max(COD_USU_CONTACT),0)+1) as CODIGO FROM TBL_MUEBLES_USUARIO_CONTACTENOS";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Inserta_Datos_USUARIO_MUEBLES_CONTACTENOS($COD_USU_CONTACT, $NUM_SI, $NUM_RUC, $NOM_ENTIDAD, $NUM_OFICIO, $NOM_REPRESENTANTE, $NOM_CARGO_REPRESENTANTE, $DNI_REPRESENTANTE, $CORREO_REPRESENTANTE, $TELEF_REPRESENTANTE, $ANEXO_REPRESENTANTE, $COD_UNID_EJECUTORA, $NOM_UNID_EJECUTORA, $ASUNTO, $COMENTARIO){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_USUARIO_CONTACTENOS 
			(COD_USU_CONTACT, NUM_SI, NUM_RUC, NOM_ENTIDAD, NUM_OFICIO, NOM_REPRESENTANTE, NOM_CARGO_REPRESENTANTE, DNI_REPRESENTANTE, CORREO_REPRESENTANTE, TELEF_REPRESENTANTE, ANEXO_REPRESENTANTE, COD_UNID_EJECUTORA, NOM_UNID_EJECUTORA, ASUNTO, COMENTARIO, FECHA_REGISTRO, ID_ESTADO) VALUES ('$COD_USU_CONTACT', '$NUM_SI', '$NUM_RUC', '$NOM_ENTIDAD', '$NUM_OFICIO', '$NOM_REPRESENTANTE', '$NOM_CARGO_REPRESENTANTE', '$DNI_REPRESENTANTE', '$CORREO_REPRESENTANTE', '$TELEF_REPRESENTANTE', '$ANEXO_REPRESENTANTE', '$COD_UNID_EJECUTORA', '$NOM_UNID_EJECUTORA', '$ASUNTO', '$COMENTARIO', GETDATE(), '1')";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Listar_Usuarios_autorizados_muebles($fecha){
		if($this->oDBManager->conectar()==true){
	    $sql="SELECT  YEAR(FECH_HABILITACION) AS ANIO, 
       	      MONTH(FECH_HABILITACION) AS MES,
 			  COUNT(*) AS TOTAL  FROM TBL_MUEBLES_USUARIO
 			  WHERE year(FECH_HABILITACION) = '$fecha' AND ID_ESTADO = 1  AND  MOD_MUEBLES='X' and MOD_INMUEBLES=''         
 			  GROUP BY 
 		      YEAR(FECH_HABILITACION),
   			  MONTH(FECH_HABILITACION)
 			  ORDER BY 1";
		$resultado = $this->oDBManager->execute($sql);
		return $resultado;
			}
		}

	function Listar_Usuarios_Autrizados_Muebles_Imbuebles($FECHA){
	 if($this->oDBManager->conectar()==true){
		$sql="SELECT  YEAR(FECH_HABILITACION) AS ANIO, 
			  MONTH(FECH_HABILITACION) AS MES,
			  COUNT(*) AS TOTAL  FROM TBL_MUEBLES_USUARIO
			  WHERE year(FECH_HABILITACION) = '$FECHA' AND ID_ESTADO = 1  AND MOD_MUEBLES='X' and MOD_INMUEBLES='X'
			  GROUP BY 
			  YEAR(FECH_HABILITACION),
			  MONTH(FECH_HABILITACION)
			  ORDER BY 1";
			  $resultado=$this->oDBManager->execute($sql);
			  return $resultado;
		 
		
		}
	}	
		
		
	function Listar_Usuarios_autorizados_Inmuebles($fecha){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT  YEAR(FECH_HABILITACION) AS ANIO, 
       				MONTH(FECH_HABILITACION) AS MES_IN,
 					COUNT(*) AS TOTAL_IN  FROM TBL_MUEBLES_USUARIO
 					WHERE year(FECH_HABILITACION) = '$fecha' AND ID_ESTADO = 1  AND MOD_INMUEBLES='X' AND MOD_MUEBLES=' '
 					GROUP BY 
 					YEAR(FECH_HABILITACION),
   					MONTH(FECH_HABILITACION)
 					ORDER BY 1";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		}
		
		function Total_Entidades_x_Usuarios_autorizados($ANIO,$MES){
		if($this->oDBManager->conectar()==true){
		 $sql="select count(*) as TOT_REG from  
		       (SELECT
				U.COD_ENTIDAD,
				E.NOM_ENTIDAD,
				U.NOM_USUARIO,
				COUNT(PRE.COD_ENTIDAD) AS TOT_USUARIOS
				FROM TBL_MUEBLES_USUARIO U
				LEFT JOIN TBL_PADRON_ENTIDAD E ON (U.COD_ENTIDAD = E.COD_ENTIDAD) left join TBL_PADRON_PREDIOS PRE ON(E.COD_ENTIDAD=PRE.COD_ENTIDAD)
				WHERE U.COD_ENTIDAD IS NOT NULL
				AND U.ID_ESTADO = 1
				AND PRE.ID_ESTADO='1'
				AND YEAR(U.FECH_HABILITACION) = '$ANIO'
				AND MONTH(U.FECH_HABILITACION) = '$MES'
				AND U.MOD_MUEBLES='X' AND U.MOD_INMUEBLES=''  
				GROUP BY U.COD_ENTIDAD, E.NOM_ENTIDAD,U.NOM_USUARIO ) TBL ";  
		 $resultado=$this->oDBManager->execute($sql);
		 return $resultado;
		 }
		}
		
		
		function Listar_Entidades_x_Usuarios_autorizados($INI,$FIN,$ANIO,$MES){
		if($this->oDBManager->conectar()==true){
			$sql_tabla="SELECT ROW_NUMBER () OVER  (ORDER BY COD_ENTIDAD DESC) AS ROW_NUMBER_ID  ,* FROM
				(SELECT
				U.COD_ENTIDAD,
				E.NOM_ENTIDAD,
				U.NOM_USUARIO,
				COUNT(PRE.COD_ENTIDAD) AS TOT_USUARIOS
				FROM TBL_MUEBLES_USUARIO U
				LEFT JOIN TBL_PADRON_ENTIDAD E ON (U.COD_ENTIDAD = E.COD_ENTIDAD) left join TBL_PADRON_PREDIOS PRE ON(E.COD_ENTIDAD=PRE.COD_ENTIDAD)
				WHERE U.COD_ENTIDAD IS NOT NULL
				AND U.ID_ESTADO = 1
				AND PRE.ID_ESTADO='1'
				AND YEAR(U.FECH_HABILITACION) = '$ANIO'
				AND MONTH(U.FECH_HABILITACION) = '$MES'
				AND U.MOD_MUEBLES='X' AND U.MOD_INMUEBLES=''  
				GROUP BY U.COD_ENTIDAD, E.NOM_ENTIDAD,U.NOM_USUARIO ) TBL";
				 $sql= "SELECT * FROM ( ".$sql_tabla." ) AS TBLNUMBER  WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		}
		
		function Total_Entidades_Usuarios_Muebles_Imnuebles($ANIO,$MES){
		 if($this->oDBManager->conectar()==true){
			 $sql="SELECT COUNT(*) AS TOT_REG FROM
					(SELECT
					U.COD_ENTIDAD,
					E.NOM_ENTIDAD,
					U.NOM_USUARIO,
					COUNT(PRE.COD_ENTIDAD) AS TOT_USUARIOS
					FROM TBL_MUEBLES_USUARIO U
					LEFT JOIN TBL_PADRON_ENTIDAD E ON (U.COD_ENTIDAD = E.COD_ENTIDAD) left join TBL_PADRON_PREDIOS PRE ON(E.COD_ENTIDAD=PRE.COD_ENTIDAD)
					WHERE U.COD_ENTIDAD IS NOT NULL
					AND U.ID_ESTADO = 1
					AND PRE.ID_ESTADO='1'
					AND YEAR(U.FECH_HABILITACION) = '$ANIO'
					AND MONTH(U.FECH_HABILITACION)='$MES'
					AND U.MOD_MUEBLES='X' AND U.MOD_INMUEBLES='X'  
					GROUP BY U.COD_ENTIDAD, E.NOM_ENTIDAD,U.NOM_USUARIO ) TBL";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;	
			 }
		 }
		
		function Listar_Entidades_Usuarios_Muebles_Inmuebles($INI,$FIN,$ANIO,$MES){
		if($this->oDBManager->conectar()==true){
		  $sql_tabla="SELECT ROW_NUMBER () OVER  (ORDER BY COD_ENTIDAD DESC) AS ROW_NUMBER_ID  ,* FROM
			    (SELECT
				U.COD_ENTIDAD,
				E.NOM_ENTIDAD,
				U.NOM_USUARIO,
				COUNT(PRE.COD_ENTIDAD) AS TOT_USUARIOS
				FROM TBL_MUEBLES_USUARIO U
				LEFT JOIN TBL_PADRON_ENTIDAD E ON (U.COD_ENTIDAD = E.COD_ENTIDAD) left join TBL_PADRON_PREDIOS PRE ON(E.COD_ENTIDAD=PRE.COD_ENTIDAD)
				WHERE U.COD_ENTIDAD IS NOT NULL
				AND U.ID_ESTADO = 1
				AND PRE.ID_ESTADO='1'
				AND YEAR(U.FECH_HABILITACION) = '$ANIO'
				AND MONTH(U.FECH_HABILITACION) = '$MES'
				AND U.MOD_MUEBLES='X' AND U.MOD_INMUEBLES='X'  
				GROUP BY U.COD_ENTIDAD, E.NOM_ENTIDAD,U.NOM_USUARIO ) TBL"; 
				$sql= "SELECT * FROM ( ".$sql_tabla." ) AS TBLNUMBER  WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ";
		  $resultado=$this->oDBManager->execute($sql);
		  return $resultado;
		  }
		}
		
		function Total_Entidades_x_Usuarios_autorizados_inmuebles($ANIO,$MES){
		if($this->oDBManager->conectar()==true){
		   $sql=" SELECT COUNT(*) AS TOT_REG FROM (SELECT
				U.COD_ENTIDAD,
				E.NOM_ENTIDAD,
				U.NOM_USUARIO,
				COUNT(PRE.COD_ENTIDAD) AS TOT_USUARIOS
				FROM TBL_MUEBLES_USUARIO U
				LEFT JOIN TBL_PADRON_ENTIDAD E ON (U.COD_ENTIDAD = E.COD_ENTIDAD) left join TBL_PADRON_PREDIOS PRE ON(E.COD_ENTIDAD=PRE.COD_ENTIDAD)
				WHERE U.COD_ENTIDAD IS NOT NULL
				AND U.ID_ESTADO = 1
				AND PRE.ID_ESTADO='1'
				AND YEAR(U.FECH_HABILITACION) = '$ANIO'
				AND MONTH(U.FECH_HABILITACION) = '$MES'
				AND U.MOD_INMUEBLES='X' AND U.MOD_MUEBLES=' '
				GROUP BY U.COD_ENTIDAD, E.NOM_ENTIDAD,U.NOM_USUARIO ) TBL";
		    $resultado=$this->oDBManager->execute($sql);
			return $resultado;
		  }
		}
		
		
		function Listar_Entidades_x_Usuarios_autorizados_inmuebles($INI,$FIN,$ANIO,$MES){
		if($this->oDBManager->conectar()==true){
			$sql_tabla="SELECT ROW_NUMBER () OVER  (ORDER BY COD_ENTIDAD DESC) AS ROW_NUMBER_ID  ,* FROM
				 (SELECT
				U.COD_ENTIDAD,
				E.NOM_ENTIDAD,
				U.NOM_USUARIO,
				COUNT(PRE.COD_ENTIDAD) AS TOT_USUARIOS
				FROM TBL_MUEBLES_USUARIO U
				LEFT JOIN TBL_PADRON_ENTIDAD E ON (U.COD_ENTIDAD = E.COD_ENTIDAD) left join TBL_PADRON_PREDIOS PRE ON(E.COD_ENTIDAD=PRE.COD_ENTIDAD)
				WHERE U.COD_ENTIDAD IS NOT NULL
				AND U.ID_ESTADO = 1
				AND PRE.ID_ESTADO='1'
				AND YEAR(U.FECH_HABILITACION) = '$ANIO'
				AND MONTH(U.FECH_HABILITACION) = '$MES'
				AND U.MOD_INMUEBLES='X' AND U.MOD_MUEBLES=' '
				GROUP BY U.COD_ENTIDAD, E.NOM_ENTIDAD,U.NOM_USUARIO ) TBL ";
				 $sql= "SELECT * FROM ( ".$sql_tabla." ) AS TBLNUMBER  WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		} 
		
		
		function Mostrar_Datos_Entidad_x_Codigo($COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_PADRON_ENTIDAD WHERE COD_ENTIDAD = '$COD_ENTIDAD' AND ID_ESTADO = '1'";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		}
		
		function Total_Locales_x_Entidad_Muebles($COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){
		 $sql="SELECT  COUNT(*) AS TOT_REG FROM  
(SELECT
L.ID_PREDIO_SBN,
L.DENOMINACION_PREDIO,
L.COD_DEPA,
DEPA.nmbubigeo AS DEPARTAMENTO,
L.COD_PROV,
PROV.nmbubigeo AS PROVINCIA,
L.COD_DIST,
DIST.nmbubigeo AS DISTRITO,
L.COD_TVIA,
TV.DESC_VIA,
L.NOMBRE_VIA,
L.NRO_PRED,
L.MZA_PRED,
L.LTE_PRED
FROM TBL_PADRON_PREDIOS L
LEFT JOIN TBL_UBIGEO DEPA ON (DEPA.FLAG = 'D' AND DEPA.CODREGION !='00' AND DEPA.coddep = L.COD_DEPA)
LEFT JOIN TBL_UBIGEO PROV ON (PROV.FLAG = 'P' AND PROV.coddep = L.COD_DEPA AND PROV.codprov = L.COD_PROV )
LEFT JOIN TBL_UBIGEO DIST ON (DIST.FLAG = 'T' AND DIST.coddep = L.COD_DEPA AND DIST.codprov = L.COD_PROV AND DIST.coddist = L.COD_DIST )
LEFT JOIN TBL_TIPO_VIA TV ON (TV.COD_TVIA = L.COD_TVIA)
WHERE L.COD_ENTIDAD = '$COD_ENTIDAD'
AND L.ID_ESTADO = '1') TBL";
		 $resultado=$this->oDBManager->execute($sql);
		 return $resultado;
		 }
		}
		
		function Listar_Locales_x_Entidad_Muebles($INI,$FIN,$COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){
			$sql_tabla="SELECT  ROW_NUMBER () OVER(ORDER BY ID_PREDIO_SBN DESC) AS ROW_NUMBER_ID ,* FROM  
		(SELECT
		L.ID_PREDIO_SBN,
		L.DENOMINACION_PREDIO,
		L.COD_DEPA,
		DEPA.nmbubigeo AS DEPARTAMENTO,
		L.COD_PROV,
		PROV.nmbubigeo AS PROVINCIA,
		L.COD_DIST,
		DIST.nmbubigeo AS DISTRITO,
		L.COD_TVIA,
		TV.DESC_VIA,
		L.NOMBRE_VIA,
		L.NRO_PRED,
		L.MZA_PRED,
		L.LTE_PRED
		FROM TBL_PADRON_PREDIOS L
		LEFT JOIN TBL_UBIGEO DEPA ON (DEPA.FLAG = 'D' AND DEPA.CODREGION !='00' AND DEPA.coddep = L.COD_DEPA)
		LEFT JOIN TBL_UBIGEO PROV ON (PROV.FLAG = 'P' AND PROV.coddep = L.COD_DEPA AND PROV.codprov = L.COD_PROV )
		LEFT JOIN TBL_UBIGEO DIST ON (DIST.FLAG = 'T' AND DIST.coddep = L.COD_DEPA AND DIST.codprov = L.COD_PROV AND DIST.coddist = L.COD_DIST )
		LEFT JOIN TBL_TIPO_VIA TV ON (TV.COD_TVIA = L.COD_TVIA)
		WHERE L.COD_ENTIDAD = '$COD_ENTIDAD'
		AND L.ID_ESTADO = '1' ) TBL ";
		$sql= "SELECT * FROM ( ".$sql_tabla." ) AS TBLNUMBER  WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		} 
		
function Listar_Local_x_Detalle_Muebles($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT
			L.ID_PREDIO_SBN,
			L.DENOMINACION_PREDIO,
			L.COD_DEPA,
			DEPA.nmbubigeo AS DEPARTAMENTO,
			L.COD_PROV,
			PROV.nmbubigeo AS PROVINCIA,
			L.COD_DIST,
			DIST.nmbubigeo AS DISTRITO,
			L.COD_TVIA,
			TV.DESC_VIA,
			L.NOMBRE_VIA,
			L.NRO_PRED,
			L.MZA_PRED,
			L.LTE_PRED
			FROM TBL_PADRON_PREDIOS L
			LEFT JOIN TBL_UBIGEO DEPA ON (DEPA.FLAG = 'D' AND DEPA.CODREGION !='00' AND DEPA.coddep = L.COD_DEPA)
			LEFT JOIN TBL_UBIGEO PROV ON (PROV.FLAG = 'P' AND PROV.coddep = L.COD_DEPA AND PROV.codprov = L.COD_PROV )
			LEFT JOIN TBL_UBIGEO DIST ON (DIST.FLAG = 'T' AND DIST.coddep = L.COD_DEPA AND DIST.codprov = L.COD_PROV AND DIST.coddist = L.COD_DIST )
			LEFT JOIN TBL_TIPO_VIA TV ON (TV.COD_TVIA = L.COD_TVIA)
			WHERE L.ID_PREDIO_SBN = '$ID_PREDIO_SBN'
			AND L.ID_ESTADO = '1'";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		} 
	
		
	function Listar_Entidades_x_registradas_detalle($ANIO,$MES){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT  E.NOM_ENTIDAD,
E.COD_ENTIDAD,
COUNT(PRE.COD_ENTIDAD) AS TOTAL_LOCAL
FROM TBL_PADRON_ENTIDAD E LEFT JOIN TBL_PADRON_PREDIOS PRE ON(E.COD_ENTIDAD=PRE.COD_ENTIDAD)
WHERE 
E.ID_ESTADO = 1
AND YEAR(E.FECHA_REGISTRO) = '$ANIO'
AND MONTH(E.FECHA_REGISTRO) = '$MES'
AND E.SUNAT_COD_IDENT='02'
GROUP BY  E.NOM_ENTIDAD,E.COD_ENTIDAD
";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		}	
		
		function Listar_Locales_x_Entidad_REGISTRADOS($COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT
					L.ID_PREDIO_SBN,
					L.DENOMINACION_PREDIO,
					L.COD_DEPA,
					L.COD_ENTIDAD,
					DEPA.nmbubigeo AS DEPARTAMENTO,
					L.COD_PROV,
					PROV.nmbubigeo AS PROVINCIA,
					L.COD_DIST,
					DIST.nmbubigeo AS DISTRITO,
					L.COD_TVIA,
					TV.DESC_VIA,
					L.NOMBRE_VIA,
					L.NRO_PRED,
					L.MZA_PRED,
					L.LTE_PRED
					FROM TBL_PADRON_PREDIOS L
					LEFT JOIN TBL_UBIGEO DEPA ON (DEPA.FLAG = 'D' AND DEPA.CODREGION !='00' AND DEPA.coddep = L.COD_DEPA)
					LEFT JOIN TBL_UBIGEO PROV ON (PROV.FLAG = 'P' AND PROV.coddep = L.COD_DEPA AND PROV.codprov = L.COD_PROV )
		LEFT JOIN TBL_UBIGEO DIST ON (DIST.FLAG = 'T' AND DIST.coddep = L.COD_DEPA AND DIST.codprov = L.COD_PROV AND DIST.coddist = L.COD_DIST )
					LEFT JOIN TBL_TIPO_VIA TV ON (TV.COD_TVIA = L.COD_TVIA)
					WHERE L.COD_ENTIDAD = '$COD_ENTIDAD'
					AND L.ID_ESTADO = '1'";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		} 
		
		
		
		function TOTAL_ENTIDADES_REGISTRADA_PAGINACION($ANIO,$MES){ 
		if($this->oDBManager->conectar()==true){
			$sql="select 
COUNT(*) AS TOT_EN
FROM TBL_PADRON_ENTIDAD E 
WHERE E.ID_ESTADO = 1
AND YEAR(E.FECHA_REGISTRO) = '$ANIO'
AND MONTH(E.FECHA_REGISTRO) = '$MES'
AND E.SUNAT_COD_IDENT='02' ";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		}
		
		
		function Listar_Entidades_x_registradas_detalle_PAGINACION($INI,$FIN,$ANIO,$MES){
		if($this->oDBManager->conectar()==true){
		   $SQL_ORIGEN="SELECT  
						E.COD_ENTIDAD,
						E.NOM_ENTIDAD,
						COUNT(PRE.ID_PREDIO_SBN) AS TOTAL_LOCAL,
						ROW_NUMBER () OVER (ORDER BY E.COD_ENTIDAD DESC) AS ROW_NUMBER_ID
						FROM TBL_PADRON_ENTIDAD E 
						LEFT JOIN TBL_PADRON_PREDIOS PRE ON(E.COD_ENTIDAD=PRE.COD_ENTIDAD AND PRE.ID_ESTADO ='1')
						WHERE E.ID_ESTADO = '1'
						AND YEAR(E.FECHA_REGISTRO) = '$ANIO'
						AND MONTH(E.FECHA_REGISTRO) = '$MES'
						AND E.SUNAT_COD_IDENT='02'
						GROUP BY  E.NOM_ENTIDAD,E.COD_ENTIDAD 
						";
				$sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TBLNUMBER  WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ORDER BY COD_ENTIDAD DESC ";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		}	
		
		
	function Listar_Locales_Registradas_fecha($fecha){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT YEAR(PRE.FECHA_REGISTRO) AS ANIO,
				  MONTH(PRE.FECHA_REGISTRO) AS MES,
				  COUNT(PRE.ID_PREDIO_SBN) AS TOTAL_LOCAL
				  FROM TBL_PADRON_PREDIOS PRE LEFT JOIN TBL_PADRON_ENTIDAD P ON (PRE.COD_ENTIDAD=P.COD_ENTIDAD)   
				  WHERE YEAR(PRE.FECHA_REGISTRO) ='$fecha' AND PRE.ID_ESTADO='1' AND P.SUNAT_COD_IDENT='02'
				  GROUP BY 
				  YEAR(PRE.FECHA_REGISTRO),
				  MONTH(PRE.FECHA_REGISTRO)
				  ORDER BY 1   ";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		} 
		
		function Listar_Locales_x_registradas_detalle_pru($ANIO,$MES){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT  E.NOM_ENTIDAD,
				  YEAR(PRE.FECHA_REGISTRO),
                  MONTH(PRE.FECHA_REGISTRO),
                  E.COD_ENTIDAD,
                  COUNT(PRE.COD_ENTIDAD) AS TOTAL_LOCAL
                  FROM TBL_PADRON_PREDIOS PRE INNER JOIN TBL_PADRON_ENTIDAD E  ON(E.COD_ENTIDAD=PRE.COD_ENTIDAD)
                  WHERE 
                  PRE.ID_ESTADO = 1
                  AND YEAR(PRE.FECHA_REGISTRO) = '$ANIO'
                  AND MONTH(PRE.FECHA_REGISTRO) = '$MES'
                  AND E.SUNAT_COD_IDENT='02'
                  GROUP BY  E.NOM_ENTIDAD,E.COD_ENTIDAD, YEAR(PRE.FECHA_REGISTRO),MONTH(PRE.FECHA_REGISTRO) ";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		}
		
		function Listar_Locales_x_Entidad_Perido($COD_ENTIDAD, $ANIO,$MES){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT
				L.ID_PREDIO_SBN,
				L.DENOMINACION_PREDIO,
    			L.COD_DEPA,
				L.COD_ENTIDAD,
			    DEPA.nmbubigeo AS DEPARTAMENTO,
                L.COD_PROV,
                PROV.nmbubigeo AS PROVINCIA,
                L.COD_DIST,
                DIST.nmbubigeo AS DISTRITO,
                L.COD_TVIA,
     		    TV.DESC_VIA,
				L.NOMBRE_VIA,
				L.NRO_PRED,
				L.MZA_PRED,
				L.LTE_PRED
				FROM TBL_PADRON_PREDIOS L LEFT JOIN TBL_PADRON_ENTIDAD PD ON(L.COD_ENTIDAD=PD.COD_ENTIDAD)
				LEFT JOIN TBL_UBIGEO DEPA ON (DEPA.FLAG = 'D' AND DEPA.CODREGION !='00' AND DEPA.coddep = L.COD_DEPA)
				LEFT JOIN TBL_UBIGEO PROV ON (PROV.FLAG = 'P' AND PROV.coddep = L.COD_DEPA AND PROV.codprov = L.COD_PROV )
			LEFT JOIN TBL_UBIGEO DIST ON (DIST.FLAG = 'T' AND DIST.coddep = L.COD_DEPA AND DIST.codprov = L.COD_PROV AND DIST.coddist = L.COD_DIST )
				LEFT JOIN TBL_TIPO_VIA TV ON (TV.COD_TVIA = L.COD_TVIA)
				WHERE L.COD_ENTIDAD = '$COD_ENTIDAD' AND YEAR(L.FECHA_REGISTRO)='$ANIO' AND MONTH(L.FECHA_REGISTRO) ='$MES'
                AND L.ID_ESTADO = '1'
				";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		} 
		
		 
		function Total_Locales_Registrados($ANIO,$MES){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT 
				 COUNT(DISTINCT E.COD_ENTIDAD) AS TOTAL_ENTIDAD,
				 COUNT(PRE.COD_ENTIDAD) AS TOTAL_LOCALES
                 FROM TBL_PADRON_PREDIOS PRE INNER JOIN TBL_PADRON_ENTIDAD E  ON(E.COD_ENTIDAD=PRE.COD_ENTIDAD)
                 WHERE 
                 PRE.ID_ESTADO = 1
                 AND YEAR(PRE.FECHA_REGISTRO) = '$ANIO'
                 AND MONTH(PRE.FECHA_REGISTRO) = '$MES'
                 AND E.SUNAT_COD_IDENT='02'  
                 ";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		}  
		 
		 
		 function Listar_Locales_x_registradas_detalle_paginacion($INI,$FIN,$ANIO,$MES){
		if($this->oDBManager->conectar()==true){
			$SQL_ORIGEN="SELECT  E.NOM_ENTIDAD,
				  YEAR(PRE.FECHA_REGISTRO) AS ANIO,
				  MONTH(PRE.FECHA_REGISTRO) AS MES,
				  E.COD_ENTIDAD,
				 COUNT(PRE.COD_ENTIDAD) AS TOTAL_LOCAL,
                 ROW_NUMBER () OVER (ORDER BY E.COD_ENTIDAD DESC) AS ROW_NUMBER_ID
                 FROM TBL_PADRON_PREDIOS PRE INNER JOIN TBL_PADRON_ENTIDAD E  ON(E.COD_ENTIDAD=PRE.COD_ENTIDAD)
                 WHERE 
                 PRE.ID_ESTADO = 1
				 AND YEAR(PRE.FECHA_REGISTRO) = '$ANIO'
				 AND MONTH(PRE.FECHA_REGISTRO) = '$MES'
				 AND E.SUNAT_COD_IDENT='02'
				 GROUP BY  E.NOM_ENTIDAD,E.COD_ENTIDAD, YEAR(PRE.FECHA_REGISTRO),MONTH(PRE.FECHA_REGISTRO)";
		  $sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TBLNUMBER  WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ORDER BY COD_ENTIDAD DESC ";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		}
		
		
		function TOTAL_LOCAL_DETALLE_PREDIOS($COD_ENTIDAD, $ANIO,$MES){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT
				  count(L.COD_ENTIDAD) AS TOTAL_DETALLE
				  FROM TBL_PADRON_PREDIOS L LEFT JOIN TBL_PADRON_ENTIDAD PD ON(L.COD_ENTIDAD=PD.COD_ENTIDAD)
				  LEFT JOIN TBL_UBIGEO DEPA ON (DEPA.FLAG = 'D' AND DEPA.CODREGION !='00' AND DEPA.coddep = L.COD_DEPA)
				  LEFT JOIN TBL_UBIGEO PROV ON (PROV.FLAG = 'P' AND PROV.coddep = L.COD_DEPA AND PROV.codprov = L.COD_PROV )
				  LEFT JOIN TBL_UBIGEO DIST ON (DIST.FLAG = 'T' AND DIST.coddep = L.COD_DEPA AND DIST.codprov = L.COD_PROV AND DIST.coddist = 			                  L.COD_DIST )
				  LEFT JOIN TBL_TIPO_VIA TV ON (TV.COD_TVIA = L.COD_TVIA)
                  WHERE L.COD_ENTIDAD = '$COD_ENTIDAD' AND YEAR(L.FECHA_REGISTRO)='$ANIO' AND MONTH(L.FECHA_REGISTRO) ='$MES'
                  AND L.ID_ESTADO = '1'
				";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		} 
		 
		
		function Listar_Locales_x_Entidad_Perido_paginacion($INI,$FIN,$COD_ENTIDAD, $ANIO,$MES){
		if($this->oDBManager->conectar()==true){
			$SQL_ORIGEN="SELECT
				L.ID_PREDIO_SBN,
				L.DENOMINACION_PREDIO,
    			L.COD_DEPA,
				L.COD_ENTIDAD,
			    DEPA.nmbubigeo AS DEPARTAMENTO,
                L.COD_PROV,
                PROV.nmbubigeo AS PROVINCIA,
                L.COD_DIST,
                DIST.nmbubigeo AS DISTRITO,
                L.COD_TVIA,
     		    TV.DESC_VIA,
				L.NOMBRE_VIA,
				L.NRO_PRED,
				L.MZA_PRED,
				L.LTE_PRED, 
				ROW_NUMBER () OVER (ORDER BY L.COD_ENTIDAD DESC) AS ROW_NUMBER_ID
				FROM TBL_PADRON_PREDIOS L LEFT JOIN TBL_PADRON_ENTIDAD PD ON(L.COD_ENTIDAD=PD.COD_ENTIDAD)
				LEFT JOIN TBL_UBIGEO DEPA ON (DEPA.FLAG = 'D' AND DEPA.CODREGION !='00' AND DEPA.coddep = L.COD_DEPA)
				LEFT JOIN TBL_UBIGEO PROV ON (PROV.FLAG = 'P' AND PROV.coddep = L.COD_DEPA AND PROV.codprov = L.COD_PROV )
			LEFT JOIN TBL_UBIGEO DIST ON (DIST.FLAG = 'T' AND DIST.coddep = L.COD_DEPA AND DIST.codprov = L.COD_PROV AND DIST.coddist = L.COD_DIST )
				LEFT JOIN TBL_TIPO_VIA TV ON (TV.COD_TVIA = L.COD_TVIA)
				WHERE L.COD_ENTIDAD = '$COD_ENTIDAD' AND YEAR(L.FECHA_REGISTRO)='$ANIO' AND MONTH(L.FECHA_REGISTRO) ='$MES'
                AND L.ID_ESTADO = '1' ";
			$sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TBLNUMBER  WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ORDER BY COD_ENTIDAD DESC ";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
			}
		} 
		
		
	function Listar_Entidad_Habilitados ($ANIO){
		if($this->oDBManager->conectar()==true){
		$sql="SELECT 
ANIO,
MES_IN,
COUNT(*) AS TOTAL_IN
FROM (
      SELECT 
      ANIO = YEAR(U.FECH_HABILITACION),
      MES_IN = MONTH(U.FECH_HABILITACION),
      E.COD_ENTIDAD,
      E.NOM_ENTIDAD,
      E.RUC_ENTIDAD
      FROM TBL_MUEBLES_USUARIO U
      INNER JOIN TBL_PADRON_ENTIDAD E ON (E.COD_ENTIDAD = U.COD_ENTIDAD AND E.ID_ESTADO ='1')
      WHERE U.ID_ESTADO ='1'
      AND YEAR(U.FECH_HABILITACION)='$ANIO' 
      --AND MONTH(U.FECH_HABILITACION)='1' 
      GROUP BY
      E.COD_ENTIDAD,
      E.NOM_ENTIDAD,
      E.RUC_ENTIDAD,
      YEAR(U.FECH_HABILITACION) ,
      MONTH(U.FECH_HABILITACION)
) TBL
WHERE ANIO = '$ANIO'
GROUP BY 
ANIO,
MES_IN
 ";
				$resultado=$this->oDBManager->execute($sql);
				return $resultado;
		}
	 
	}
	
	function Total_Entidad_Numero_Usuarios($ANIO,$MES){
	  if($this->oDBManager->conectar()==true){
		 $sql="SELECT  
				COUNT(*)  AS TOT_REG
				FROM
				(SELECT
				E.COD_ENTIDAD,
				E.NOM_ENTIDAD,
				TOTAL_USER_GRAL = (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO X1 WHERE X1.ID_ESTADO= '1' AND X1.COD_ENTIDAD = E.COD_ENTIDAD),
				TOTAL_USER_MUEBLES = (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO X1 WHERE X1.ID_ESTADO= '1' AND X1.COD_ENTIDAD = E.COD_ENTIDAD AND ISNULL(X1.MOD_MUEBLES,'') = 'X' AND ISNULL(X1.MOD_INMUEBLES,'') = ''),
				TOTAL_USER_INMUEBLES = (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO X1 WHERE X1.ID_ESTADO= '1' AND X1.COD_ENTIDAD = E.COD_ENTIDAD AND ISNULL(X1.MOD_MUEBLES,'') = '' AND ISNULL(X1.MOD_INMUEBLES,'') = 'X'),
				TOTAL_USER_MUEB_INMUEB = (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO X1 WHERE X1.ID_ESTADO= '1' AND X1.COD_ENTIDAD = E.COD_ENTIDAD AND ISNULL(X1.MOD_MUEBLES,'') = 'X' AND ISNULL(X1.MOD_INMUEBLES,'') = 'X')
				FROM TBL_MUEBLES_USUARIO U
				INNER JOIN TBL_PADRON_ENTIDAD E ON (E.COD_ENTIDAD = U.COD_ENTIDAD AND E.ID_ESTADO ='1')
				WHERE U.ID_ESTADO ='1'
				AND YEAR(U.FECH_HABILITACION)='$ANIO' 
				AND MONTH(U.FECH_HABILITACION)='$MES'
				GROUP BY 
				E.COD_ENTIDAD,
				E.NOM_ENTIDAD
				) TBL";
  $RESULTADO=$this->oDBManager->execute($sql);
  return $RESULTADO;
		  }
	  }
	
	function Listar_Entidad_Numero_Usuarios($INI,$FIN,$ANIO,$MES){
	if($this->oDBManager->conectar()==true){
		$sql_tabla="SELECT  
					ROW_NUMBER ()OVER(ORDER BY COD_ENTIDAD) AS ROW_NUMBER_ID ,*
					FROM
					(SELECT
					E.COD_ENTIDAD,
					E.NOM_ENTIDAD,
					TOTAL_USER_GRAL = (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO X1 WHERE X1.ID_ESTADO= '1' AND X1.COD_ENTIDAD = E.COD_ENTIDAD),
					TOTAL_USER_MUEBLES = (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO X1 WHERE X1.ID_ESTADO= '1' AND X1.COD_ENTIDAD = E.COD_ENTIDAD AND ISNULL(X1.MOD_MUEBLES,'') = 'X' AND ISNULL(X1.MOD_INMUEBLES,'') = ''),
					TOTAL_USER_INMUEBLES = (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO X1 WHERE X1.ID_ESTADO= '1' AND X1.COD_ENTIDAD = E.COD_ENTIDAD AND ISNULL(X1.MOD_MUEBLES,'') = '' AND ISNULL(X1.MOD_INMUEBLES,'') = 'X'),
					TOTAL_USER_MUEB_INMUEB = (SELECT COUNT(*) FROM TBL_MUEBLES_USUARIO X1 WHERE X1.ID_ESTADO= '1' AND X1.COD_ENTIDAD = E.COD_ENTIDAD AND ISNULL(X1.MOD_MUEBLES,'') = 'X' AND ISNULL(X1.MOD_INMUEBLES,'') = 'X'),
					ALQUILADO=(SELECT COUNT(*) FROM TBL_PADRON_PREDIOS PREX1 WHERE PREX1.COD_TIP_PROPIEDAD='1' AND PREX1.COD_ENTIDAD=E.COD_ENTIDAD), --AND TP.COD_TIP_PROPIEDAD=PRE.COD_TIP_PROPIEDAD)
					PROPIO=(SELECT COUNT(*) FROM TBL_PADRON_PREDIOS PREX1 WHERE PREX1.COD_TIP_PROPIEDAD='2' AND PREX1.COD_ENTIDAD=E.COD_ENTIDAD),
					BAJO_AD=(SELECT COUNT(*) FROM TBL_PADRON_PREDIOS PREX1 WHERE PREX1.COD_TIP_PROPIEDAD='3' AND PREX1.COD_ENTIDAD=E.COD_ENTIDAD ),
					EN_USO=(SELECT COUNT(*) FROM TBL_PADRON_PREDIOS PREX1 WHERE PREX1.COD_TIP_PROPIEDAD='4'  AND PREX1.COD_ENTIDAD=E.COD_ENTIDAD),
					TOTAL_PREDIOS=(SELECT COUNT(*) FROM TBL_PADRON_PREDIOS PREX1 WHERE PREX1.COD_ENTIDAD=E.COD_ENTIDAD )
					FROM TBL_MUEBLES_USUARIO U
					INNER JOIN TBL_PADRON_ENTIDAD E ON (E.COD_ENTIDAD = U.COD_ENTIDAD AND E.ID_ESTADO ='1')
					LEFT JOIN TBL_PADRON_PREDIOS PRE ON (PRE.COD_ENTIDAD=E.COD_ENTIDAD AND PRE.ID_ESTADO='1' )
					WHERE U.ID_ESTADO ='1'
					AND YEAR(U.FECH_HABILITACION)='$ANIO' 
					AND MONTH(U.FECH_HABILITACION)='$MES'
					GROUP BY 
					E.COD_ENTIDAD,
					E.NOM_ENTIDAD) TBL";
			$sql= "SELECT * FROM ( ".$sql_tabla." ) AS TBLNUMBER  WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ";
		$resultado=$this->oDBManager->execute($sql);
		return $resultado;
	   
	  }
	}
	
	function Listar_Usuario_x_Entidad ($COD_ENTIDAD){
	if($this->oDBManager->conectar()==true){
	  $sql="SELECT P.NOM_ENTIDAD,* FROM TBL_MUEBLES_USUARIO U 
			INNER JOIN TBL_PADRON_ENTIDAD P ON(U.COD_ENTIDAD=P.COD_ENTIDAD)
			WHERE   U.COD_ENTIDAD='$COD_ENTIDAD' AND U.ID_ESTADO='1' ";
	  $resultado=$this->oDBManager->execute($sql);
	  return $resultado;
	  }
	}
	
	function Total_Tipos_Predios($COD_ENTIDAD,$COD_TIPO){
	  if($this->oDBManager->conectar()==true){
		  $sql="select 
				COUNT(*) AS TOT_REG
				 from TBL_PADRON_PREDIOS PRE
				INNER JOIN  TBL_PADRON_ENTIDAD P 
				ON(P.COD_ENTIDAD=PRE.COD_ENTIDAD) 
				WHERE PRE.COD_ENTIDAD='$COD_ENTIDAD'
				AND PRE.COD_TIP_PROPIEDAD='$COD_TIPO'
				AND P.ID_ESTADO='1' 
				";
		  $resultado=$this->oDBManager->execute($sql);
		  return $resultado;
		 }
	 }
	
	function Listar_tipos_Predio($INI,$FIN,$COD_ENTIDAD,$COD_TIPO){
	 if($this->oDBManager->conectar()==true){
			$sql_tabla="select 
			ROW_NUMBER ()over(order by PRE.FECHA_REGISTRO) AS ROW_NUMBER_ID,
			pre.COD_ENTIDAD,
			pre.DENOMINACION_PREDIO,
			pre.COD_TIP_PROPIEDAD
			 from TBL_PADRON_PREDIOS PRE
			INNER JOIN  TBL_PADRON_ENTIDAD P 
			ON(P.COD_ENTIDAD=PRE.COD_ENTIDAD) 
			WHERE PRE.COD_ENTIDAD='$COD_ENTIDAD'
			AND PRE.COD_TIP_PROPIEDAD='$COD_TIPO'
			AND P.ID_ESTADO='1'";
			$sql="SELECT * FROM (".$sql_tabla.")  AS TBLNUMBER  WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ";
		 $resultado=$this->oDBManager->execute($sql);
		 return $resultado;
		 }
	 }
	 
	 
	 function Lista_Cod_Modulo_x_LISTA($COD_MODULO_LISTA){
	 if($this->oDBManager->conectar()==true){
			$sql="
SELECT   
COD_MUEBLES		 = ISNULL((SELECT DISTINCT COD_MUEBLE_MODULO FROM TBL_MUEBLES_MODULO WHERE COD_MUEBLE_MODULO IN ($COD_MODULO_LISTA) AND COD_MUEBLE_MODULO = '1'),'0'),
COD_INMUEBLES	 = ISNULL((SELECT DISTINCT COD_MUEBLE_MODULO FROM TBL_MUEBLES_MODULO WHERE COD_MUEBLE_MODULO IN ($COD_MODULO_LISTA) AND COD_MUEBLE_MODULO = '2'),'0'),
COD_PERSONALIZADO = ISNULL((SELECT DISTINCT COD_MUEBLE_MODULO FROM TBL_MUEBLES_MODULO WHERE COD_MUEBLE_MODULO IN ($COD_MODULO_LISTA) AND COD_MUEBLE_MODULO = '3'),'0')


";
		 $resultado=$this->oDBManager->execute($sql);
		 return $resultado;
		 }
	 }
	 
	 
	 function INSERTAR_POI_HABILITACION_FUNCIONARIO_SINABIP_MUEBLES_E_INMUEBLES($anho_proceso ,$codigo_actividad ,$tipo_registro ,$cod_documento ,$tipo_documento ,$codigo_programado ,$dsc_documento ,$codigo_direccion ,$fecha_documento ,$codigo_tarea ,$usuario_edicion ,$fecha_edicion ,$usuario_modificacion ,$fecha_modificacion ,$cantidad ,$est_dcto ,$codigo_inmueble ,$codigo_expediente ,$codigo_matriz ,$codigo_legal ,$codigo_tecnico ,$usuario_solicitante ,$codigo_dpto ,$periodo_inventario ,$funcionario ,$nmb_sistema){
	 if($this->oDBManager->conectar()==true){
			$sql="
INSERT INTO SRVBDP.sbndb.DBO.tes_documentos(
   anho_proceso ,
   codigo_actividad ,
   tipo_registro ,
   cod_documento ,
   tipo_documento ,
   codigo_programado ,
   dsc_documento ,
   codigo_direccion ,
   fecha_documento ,
   codigo_tarea ,
   usuario_edicion ,
   fecha_edicion   ,
   usuario_modificacion ,
   fecha_modificacion,
   cantidad  ,
   est_dcto ,
   codigo_inmueble,
   codigo_expediente,
   codigo_matriz,
   codigo_legal,
   codigo_tecnico,
   usuario_solicitante,
   codigo_dpto ,
   periodo_inventario ,
   funcionario,
   nmb_sistema
)
VALUES (
   '$anho_proceso' ,
   '$codigo_actividad' ,
   '$tipo_registro' ,
   '$cod_documento' ,
   '$tipo_documento' ,
   '$codigo_programado' ,
   '$dsc_documento' ,
   '$codigo_direccion' ,
   '$fecha_documento' ,
   '$codigo_tarea' ,
   '$usuario_edicion' ,
   '$fecha_edicion' ,
   '$usuario_modificacion' ,
   '$fecha_modificacion' ,
   '$cantidad' ,
   '$est_dcto' ,
   '$codigo_inmueble' ,
   '$codigo_expediente' ,
   '$codigo_matriz' ,
   '$codigo_legal' ,
   '$codigo_tecnico' ,
   '$usuario_solicitante' ,
   '$codigo_dpto' ,
   '$periodo_inventario' ,
   '$funcionario' ,
   '$nmb_sistema'
)
";
		 $resultado=$this->oDBManager->execute($sql);
		 return $resultado;
		 }
	 }
	
	
	function Listar_POI_ANIO_ACTUAL_HABILITACION_USUARIO($Anio){
		if($this->oDBManager->conectar()==true){
			$sql="
			SELECT 
			TAD.*,
			D.descripcion_direccion,
			D.descripcion_abreviada
			FROM SRVBDP.sbndb.DBO.tes_actividades_default TAD
			LEFT JOIN SRVBDP.sbndb.DBO.direccion D on (TAD.codigo_direccion = D.codigo_direccion)
			WHERE TAD.anho_proceso 	= $Anio
			AND TAD.control_opcion  = 'SINABIP WEB'  
			AND TAD.codigo_direccion = '054'--CATASTRO
			";
			$resultado=$this->oDBManager->execute($sql);
			return $resultado;
		}
	 }
	 
	 
	 function Ver_POI_HABILITACION_USUARIO_AÑO_ACTUAL($ANIO){
		if($this->oDBManager->conectar()==true){
			$sql="
			SELECT 
			TAD.control_opcion,
			TAD.tipo_registro,
			TAD.tipo_documento,
			TAD.anho_proceso,
			TAD.codigo_direccion,
			TAD.codigo_actividad,
			ACT.nro_actividad,
			ACT.dsc_actividad,
			D.descripcion_direccion,
			D.descripcion_abreviada,
			TAD.codigo_programado,
			PRO.descripcion
			FROM SRVBDP.sbndb.DBO.tes_actividades_default TAD
			LEFT JOIN SRVBDP.sbndb.DBO.direccion D on (TAD.codigo_direccion = D.codigo_direccion)
			LEFT JOIN SRVBDP.sbndb.DBO.tes_actividades ACT on (TAD.codigo_direccion = ACT.codigo_direccion AND TAD.anho_proceso = ACT.anho_proceso AND TAD.codigo_actividad = ACT.codigo_actividad)
			LEFT JOIN SRVBDP.sbndb.DBO.tes_programados PRO on (TAD.codigo_direccion = PRO.codigo_direccion AND TAD.anho_proceso = PRO.anho_proceso AND TAD.codigo_actividad = PRO.codigo_actividad AND TAD.codigo_programado = PRO.codigo_programado)
			WHERE TAD.anho_proceso 	= $ANIO
			AND TAD.control_opcion  = 'SINABIP WEB'  
			AND TAD.codigo_direccion = '054'--CATASTRO
			";
			$resultado=$this->oDBManager->execute($sql);
			return $resultado;
		}
	 }
}
?>