<?php

    class C_TBL_MUEBLES_UE_BIEN_PATRIMONIAL_Controller
    {

      const NRO_FILA_LIST	= 20;
    	const NRO_LINK_PAGE = 10;


        private $ObjModel;
        

        //Metodo Constructor
        function __construct()
        {
            $this->ObjModel = new M_TBL_MUEBLES_UE_BIEN_PATRIMONIAL_Model();
        }


        public function index()
        {
          $data = array();
          $this->render('principal.tpl.php', $data);
        }

        public function Buscar_BienesPatrimoniales_Vehiculares(){

          $objModel2 = new BienPatrimonialModel();
      		$data = array();

          $txt_entidad = $_SESSION['th_SIMI_COD_ENTIDAD'];
      		$txt_codigo = isset($_GET['txt_codigo']) ? $_GET['txt_codigo'] : '';
      		$txt_denominacion = isset($_GET['txt_denominacion']) ? $_GET['txt_denominacion'] : '';

      		$pageNumber = isset($_GET['numeroPagina']) ? $_GET['numeroPagina'] : 1;

      		$xNRO_FILA_LIST 	= self::NRO_FILA_LIST;
      		$xNRO_LINK_PAGE 	= self::NRO_LINK_PAGE;

          /* CREACION:        WILLIAMS ARENAS */
          /* FECHA CREACION:  09-06-2020 */
          /* DESCRIPCION:     TOTAL DE BIENES VEHICULOS */
          $data = array();    
          
          $dataModel_01 = [
            'ACCION'          =>'T',
            'txt_entidad'     =>$txt_entidad,
            'txt_codigo'      =>$txt_codigo,
            'txt_denominacion'=>$txt_denominacion
          ];
          
          $data['DataTotalBienes'] = $objModel2->F_Total_Bienes_Vehiculares_PDO($dataModel_01);
          $Total_Registros = $data['DataTotalBienes'][0]['TOTAL_REGISTROS'];
          /* FIN DE TOTAL DE BIENES VEHICULOS */


      		$Total_Paginas = ceil($Total_Registros / $xNRO_FILA_LIST);

      		$data['pager']['indicePagina'] 		= $pageNumber;
      		$data['pager']['cantidadRegistros'] = $Total_Registros;
      		$data['pager']['cantidadPaginas'] 	= $Total_Paginas;
      		$data['pager']['limiteInferior'] 	= (($pageNumber - $xNRO_LINK_PAGE) <= 0) ?  1 : ($pageNumber - $xNRO_LINK_PAGE);
      		$data['pager']['limiteSuperior'] 	= (($pageNumber + $xNRO_LINK_PAGE) > $Total_Paginas) ? $Total_Paginas : ($pageNumber + $xNRO_LINK_PAGE);
      		$data['pager']['handlerFunction'] 	= "handle_paginar_personal";

      		$Item_Ini = ($pageNumber == '1')? (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST) : (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST)+1 ;
      		$Item_Fin = ($pageNumber == '1')? ($Item_Ini + $xNRO_FILA_LIST) : ($Item_Ini + $xNRO_FILA_LIST)-1;

          /*
      		$Result_Personal = $this->ObjModel->F_Listado_Bienes_Vehiculares('L', $txt_entidad, $txt_codigo, $txt_denominacion, $Item_Ini, $Item_Fin);
          */

          /* CREACION:        WILLIAMS ARENAS */
          /* FECHA CREACION:  09-06-2020 */
          /* DESCRIPCION:     BIENES VEHICULOS */
          $data = array();    
          
          $dataModel_01 = [
            'ACCION'          =>'L',
            'txt_entidad'     =>$txt_entidad,
            'txt_codigo'      =>$txt_codigo,
            'txt_denominacion'=>$txt_denominacion,
            'Item_Ini'        =>$Item_Ini,
            'Item_Fin'        =>$Item_Fin
          ];
          
          $data['DataListadoBienes'] = $objModel2->F_Listado_Bienes_Vehiculares_PDO($dataModel_01);
          $ArrayListadoBienes = $data['DataListadoBienes'];
          /* FIN DE BIENES VEHICULOS */

          if($ArrayListadoBienes) foreach ($ArrayListadoBienes as $ListadoBienes): 

      			$personal = array();
      			$personal['ROW_NUMBER_ID'] 		    = utf8_encode($ListadoBienes["ROW_NUMBER_ID"]);
            $personal['COD_UE_BIEN_PATRI'] 		= utf8_encode($ListadoBienes["COD_UE_BIEN_PATRI"]);
      			$personal['CODIGO_PATRIMONIAL'] 	= utf8_encode($ListadoBienes["CODIGO_PATRIMONIAL"]);
      			$personal['DENOMINACION_BIEN'] 		= utf8_encode($ListadoBienes["DENOMINACION_BIEN"]);
            $personal['COD_UE_FT_VEHICULAR'] 	= utf8_encode($ListadoBienes["COD_UE_FT_VEHICULAR"]);
            $personal['VALOR_ADQUIS'] 		    = utf8_encode($ListadoBienes["VALOR_ADQUIS"]);

      			$data['personales'][] = $personal;

      		endforeach; 
          //print_r($Item_Fin);
          $this->render('v.pri_listar.php', $data);
        }


        public function Mostrar_Datos_FichaVehicular(){

          $data = array();

      		$objFichaVModel 	= new M_TBL_MUEBLES_UE_BIE_FT_VEHICULAR_Model();


          //print_r($codPersonal);
      		$codBien = isset($_GET['CodBien']) ? $_GET['CodBien'] : '';
          $codPatrimonial = isset($_GET['codPatrimonial']) ? $_GET['codPatrimonial'] : '';
          $Denominacion = isset($_GET['Denominacion']) ? $_GET['Denominacion'] : '';



      		/*********************************************************/

      		$result_Perso_E = $objFichaVModel->F_Consulta_Ficha_Vehicular($codBien);

          $data['DatPersonal']['COD_UE_BIEN_PATRI']	 	= $codBien;
          $data['DatPersonal']['CODIGO_PATRIMONIAL']	 	= $codPatrimonial;
          $data['DatPersonal']['DENOMINACION']	 	= $Denominacion;
          $data['DatPersonal']['COD_UE_FT_VEHICULAR']	 	= utf8_encode(odbc_result($result_Perso_E,"COD_UE_FT_VEHICULAR"));
          $data['DatPersonal']['DENOMINACION_BIEN']	 	= utf8_encode(odbc_result($result_Perso_E,"DENOMINACION_BIEN"));
      		$data['DatPersonal']['PLACA']	 	= utf8_encode(odbc_result($result_Perso_E,"PLACA"));
      		$data['DatPersonal']['CARROCERIA']	= utf8_encode(odbc_result($result_Perso_E,"CARROCERIA"));
      		$data['DatPersonal']['MARCA']	= utf8_encode(odbc_result($result_Perso_E,"MARCA"));
      		$data['DatPersonal']['MODELO']	 		= utf8_encode(odbc_result($result_Perso_E,"MODELO"));
      		$data['DatPersonal']['CATEGORIA']	 	= utf8_encode(odbc_result($result_Perso_E,"CATEGORIA"));
      		$data['DatPersonal']['NRO_CHASIS']	= utf8_encode(odbc_result($result_Perso_E,"NRO_CHASIS"));
      		$data['DatPersonal']['NRO_EJES']	 			= utf8_encode(odbc_result($result_Perso_E,"NRO_EJES"));
      		$data['DatPersonal']['NRO_MOTOR']	 	= utf8_encode(odbc_result($result_Perso_E,"NRO_MOTOR"));
      		$data['DatPersonal']['NRO_SERIE']	 	= utf8_encode(odbc_result($result_Perso_E,"NRO_SERIE"));
      		$data['DatPersonal']['ANIO_FABRICACION']	 	= utf8_encode(odbc_result($result_Perso_E,"ANIO_FABRICACION"));
      		$data['DatPersonal']['COLOR']	 	= utf8_encode(odbc_result($result_Perso_E,"COLOR"));
      		$data['DatPersonal']['COMBUSTIBLE']	 	= utf8_encode(odbc_result($result_Perso_E,"COMBUSTIBLE"));
      		$data['DatPersonal']['TRANSMISION']	 	= utf8_encode(odbc_result($result_Perso_E,"TRANSMISION"));
      		$data['DatPersonal']['CILINDRADA']	= utf8_encode(odbc_result($result_Perso_E,"CILINDRADA"));
      		$data['DatPersonal']['KILOMETRAJE']	= utf8_encode(odbc_result($result_Perso_E,"KILOMETRAJE"));
      		$data['DatPersonal']['TARJETA_PROPIEDAD']	= utf8_encode(odbc_result($result_Perso_E,"TARJETA_PROPIEDAD"));
      		$data['DatPersonal']['CILINDROS']	= utf8_encode(odbc_result($result_Perso_E,"CILINDROS"));
      		$data['DatPersonal']['B_CILINDROS']	= utf8_encode(odbc_result($result_Perso_E,"B_CILINDROS"));
      		$data['DatPersonal']['CARBURADOR']	 	= utf8_encode(odbc_result($result_Perso_E,"CARBURADOR"));
      		$data['DatPersonal']['B_CARBURADOR']	 	= utf8_encode(odbc_result($result_Perso_E,"B_CARBURADOR"));
      		$data['DatPersonal']['DISTRIBUIDOR']	= utf8_encode(odbc_result($result_Perso_E,"DISTRIBUIDOR"));
      		$data['DatPersonal']['B_DISTRIBUIDOR']	 	= utf8_encode(odbc_result($result_Perso_E,"B_DISTRIBUIDOR"));
      		$data['DatPersonal']['BOMBA_GASOLINA']	 		= utf8_encode(odbc_result($result_Perso_E,"BOMBA_GASOLINA"));
      		$data['DatPersonal']['B_BOMBA_GASOLINA']	 	= utf8_encode(odbc_result($result_Perso_E,"B_BOMBA_GASOLINA"));
      		$data['DatPersonal']['PURIFICADOR_AIRE']	 	= utf8_encode(odbc_result($result_Perso_E,"PURIFICADOR_AIRE"));
      		$data['DatPersonal']['B_PURIFICADOR_AIRE']	 	= utf8_encode(odbc_result($result_Perso_E,"B_PURIFICADOR_AIRE"));
      		$data['DatPersonal']['BOMBA_FRENOS']	 		= utf8_encode(odbc_result($result_Perso_E,"BOMBA_FRENOS"));
      		$data['DatPersonal']['B_BOMBA_FRENOS']	 	= utf8_encode(odbc_result($result_Perso_E,"B_BOMBA_FRENOS"));
      		$data['DatPersonal']['ZAPATAS_TAMBORES']	= utf8_encode(odbc_result($result_Perso_E,"ZAPATAS_TAMBORES"));
      		$data['DatPersonal']['B_ZAPATAS_TAMBORES']	 		= utf8_encode(odbc_result($result_Perso_E,"B_ZAPATAS_TAMBORES"));
      		$data['DatPersonal']['DISCOS_PASTILLAS']	 	= utf8_encode(odbc_result($result_Perso_E,"DISCOS_PASTILLAS"));
      		$data['DatPersonal']['B_DISCOS_PASTILLAS']	= utf8_encode(odbc_result($result_Perso_E,"B_DISCOS_PASTILLAS"));
      		$data['DatPersonal']['RADIOADOR']	 		= utf8_encode(odbc_result($result_Perso_E,"RADIOADOR"));
      		$data['DatPersonal']['B_RADIOADOR']	 	= utf8_encode(odbc_result($result_Perso_E,"B_RADIOADOR"));
      		$data['DatPersonal']['VENTILADOR']	 	= utf8_encode(odbc_result($result_Perso_E,"VENTILADOR"));
      		$data['DatPersonal']['B_VENTILADOR']	 	= utf8_encode(odbc_result($result_Perso_E,"B_VENTILADOR"));
          $data['DatPersonal']['BOMBA_AGUA']	 	= utf8_encode(odbc_result($result_Perso_E,"BOMBA_AGUA"));
          $data['DatPersonal']['B_BOMBA_AGUA']	 	= utf8_encode(odbc_result($result_Perso_E,"B_BOMBA_AGUA"));
          $data['DatPersonal']['MOTOR_ARRANQUE']	 	= utf8_encode(odbc_result($result_Perso_E,"MOTOR_ARRANQUE"));
          $data['DatPersonal']['B_MOTOR_ARRANQUE']	 	= utf8_encode(odbc_result($result_Perso_E,"B_MOTOR_ARRANQUE"));
          $data['DatPersonal']['BATERIA']	 	= utf8_encode(odbc_result($result_Perso_E,"BATERIA"));
          $data['DatPersonal']['B_BATERIA']	 	= utf8_encode(odbc_result($result_Perso_E,"B_BATERIA"));
          $data['DatPersonal']['ALTERNADOR']	 	= utf8_encode(odbc_result($result_Perso_E,"ALTERNADOR"));
          $data['DatPersonal']['B_ALTERNADOR']	 	= utf8_encode(odbc_result($result_Perso_E,"B_ALTERNADOR"));
          $data['DatPersonal']['BOBINA']	 	= utf8_encode(odbc_result($result_Perso_E,"BOBINA"));
          $data['DatPersonal']['B_BOBINA']	 	= utf8_encode(odbc_result($result_Perso_E,"B_BOBINA"));

          $data['DatPersonal']['RELAY_ALTERNADOR']	 	= utf8_encode(odbc_result($result_Perso_E,"RELAY_ALTERNADOR"));
          $data['DatPersonal']['B_RELAY_ALTERNADOR']	 	= utf8_encode(odbc_result($result_Perso_E,"B_RELAY_ALTERNADOR"));
          $data['DatPersonal']['FAROS_DELANTEROS']	 	= utf8_encode(odbc_result($result_Perso_E,"FAROS_DELANTEROS"));
          $data['DatPersonal']['B_FAROS_DELANTEROS']	 	= utf8_encode(odbc_result($result_Perso_E,"B_FAROS_DELANTEROS"));
          $data['DatPersonal']['DIRECCIONALES_DELANTERAS']	 	= utf8_encode(odbc_result($result_Perso_E,"DIRECCIONALES_DELANTERAS"));
          $data['DatPersonal']['B_DIRECCIONALES_DELANTERAS']	 	= utf8_encode(odbc_result($result_Perso_E,"B_DIRECCIONALES_DELANTERAS"));
          $data['DatPersonal']['LUCES_POSTERIORES']	 	= utf8_encode(odbc_result($result_Perso_E,"LUCES_POSTERIORES"));
          $data['DatPersonal']['B_LUCES_POSTERIORES']	 	= utf8_encode(odbc_result($result_Perso_E,"B_LUCES_POSTERIORES"));
          $data['DatPersonal']['DIRECCIONALES_POSTERIORES']	 	= utf8_encode(odbc_result($result_Perso_E,"DIRECCIONALES_POSTERIORES"));
          $data['DatPersonal']['B_DIRECCIONALES_POSTERIORES']	 	= utf8_encode(odbc_result($result_Perso_E,"B_DIRECCIONALES_POSTERIORES"));
          $data['DatPersonal']['AUTO_RADIO']	 	= utf8_encode(odbc_result($result_Perso_E,"AUTO_RADIO"));
          $data['DatPersonal']['B_AUTO_RADIO']	 	= utf8_encode(odbc_result($result_Perso_E,"B_AUTO_RADIO"));
          $data['DatPersonal']['PARLANTES']	 	= utf8_encode(odbc_result($result_Perso_E,"PARLANTES"));
          $data['DatPersonal']['B_PARLANTES']	 	= utf8_encode(odbc_result($result_Perso_E,"B_PARLANTES"));
          $data['DatPersonal']['CLAXON']	 	= utf8_encode(odbc_result($result_Perso_E,"CLAXON"));
          $data['DatPersonal']['B_CLAXON']	 	= utf8_encode(odbc_result($result_Perso_E,"B_CLAXON"));
          $data['DatPersonal']['CIRCUITO_LUCES']	 	= utf8_encode(odbc_result($result_Perso_E,"CIRCUITO_LUCES"));
          $data['DatPersonal']['B_CIRCUITO_LUCES']	 	= utf8_encode(odbc_result($result_Perso_E,"B_CIRCUITO_LUCES"));
          $data['DatPersonal']['CAJA_CAMBIOS']	 	= utf8_encode(odbc_result($result_Perso_E,"CAJA_CAMBIOS"));
          $data['DatPersonal']['B_CAJA_CAMBIOS']	 	= utf8_encode(odbc_result($result_Perso_E,"B_CAJA_CAMBIOS"));
          $data['DatPersonal']['BOMBA_EMBRAGUE']	 	= utf8_encode(odbc_result($result_Perso_E,"BOMBA_EMBRAGUE"));
          $data['DatPersonal']['B_BOMBA_EMBRAGUE']	 	= utf8_encode(odbc_result($result_Perso_E,"B_BOMBA_EMBRAGUE"));
          $data['DatPersonal']['CAJA_TRANSFERENCIA']	 	= utf8_encode(odbc_result($result_Perso_E,"CAJA_TRANSFERENCIA"));
          $data['DatPersonal']['B_CAJA_TRANSFERENCIA']	 	= utf8_encode(odbc_result($result_Perso_E,"B_CAJA_TRANSFERENCIA"));
          $data['DatPersonal']['DIFERENCIAL_TRASERO']	 	= utf8_encode(odbc_result($result_Perso_E,"DIFERENCIAL_TRASERO"));
          $data['DatPersonal']['B_DIFERENCIAL_TRASERO']	 	= utf8_encode(odbc_result($result_Perso_E,"B_DIFERENCIAL_TRASERO"));
          $data['DatPersonal']['DIFERENCIAL_DELANTERO']	 	= utf8_encode(odbc_result($result_Perso_E,"DIFERENCIAL_DELANTERO"));
          $data['DatPersonal']['B_DIFERENCIAL_DELANTERO']	 	= utf8_encode(odbc_result($result_Perso_E,"B_DIFERENCIAL_DELANTERO"));
          $data['DatPersonal']['VOLANTE']	 	= utf8_encode(odbc_result($result_Perso_E,"VOLANTE"));
          $data['DatPersonal']['B_VOLANTE']	 	= utf8_encode(odbc_result($result_Perso_E,"B_VOLANTE"));
          $data['DatPersonal']['CANA_DIRECCION']	 	= utf8_encode(odbc_result($result_Perso_E,"CANA_DIRECCION"));
          $data['DatPersonal']['B_CANA_DIRECCION']	 	= utf8_encode(odbc_result($result_Perso_E,"B_CANA_DIRECCION"));
          $data['DatPersonal']['CREMALLERA']	 	= utf8_encode(odbc_result($result_Perso_E,"CREMALLERA"));
          $data['DatPersonal']['B_CREMALLERA']	 	= utf8_encode(odbc_result($result_Perso_E,"B_CREMALLERA"));
          $data['DatPersonal']['ROTULAS']	 	= utf8_encode(odbc_result($result_Perso_E,"ROTULAS"));
          $data['DatPersonal']['B_ROTULAS']	 	= utf8_encode(odbc_result($result_Perso_E,"B_ROTULAS"));
          $data['DatPersonal']['AMORTIGUADORES']	 	= utf8_encode(odbc_result($result_Perso_E,"AMORTIGUADORES"));
          $data['DatPersonal']['B_AMORTIGUADORES']	 	= utf8_encode(odbc_result($result_Perso_E,"B_AMORTIGUADORES"));
          $data['DatPersonal']['BARRA_TORSION']	 	= utf8_encode(odbc_result($result_Perso_E,"BARRA_TORSION"));
          $data['DatPersonal']['B_BARRA_TORSION']	 	= utf8_encode(odbc_result($result_Perso_E,"B_BARRA_TORSION"));
          $data['DatPersonal']['BARRA_ESTABILIZADOR']	 	= utf8_encode(odbc_result($result_Perso_E,"BARRA_ESTABILIZADOR"));
          $data['DatPersonal']['B_BARRA_ESTABILIZADOR']	 	= utf8_encode(odbc_result($result_Perso_E,"B_BARRA_ESTABILIZADOR"));
          $data['DatPersonal']['LLANTAS']	 	= utf8_encode(odbc_result($result_Perso_E,"LLANTAS"));
          $data['DatPersonal']['B_LLANTAS']	 	= utf8_encode(odbc_result($result_Perso_E,"B_LLANTAS"));
          $data['DatPersonal']['CAPOT_MOTOR']	 	= utf8_encode(odbc_result($result_Perso_E,"CAPOT_MOTOR"));
          $data['DatPersonal']['B_CAPOT_MOTOR']	 	= utf8_encode(odbc_result($result_Perso_E,"B_CAPOT_MOTOR"));
          $data['DatPersonal']['CAPOT_MALETERA']	 	= utf8_encode(odbc_result($result_Perso_E,"CAPOT_MALETERA"));
          $data['DatPersonal']['B_CAPOT_MALETERA']	 	= utf8_encode(odbc_result($result_Perso_E,"B_CAPOT_MALETERA"));
          $data['DatPersonal']['PARACHOQUES_DELANTEROS']	 	= utf8_encode(odbc_result($result_Perso_E,"PARACHOQUES_DELANTEROS"));
          $data['DatPersonal']['B_PARACHOQUES_DELANTEROS']	 	= utf8_encode(odbc_result($result_Perso_E,"B_PARACHOQUES_DELANTEROS"));
          $data['DatPersonal']['PARACHOQUES_POSTERIORES']	 	= utf8_encode(odbc_result($result_Perso_E,"PARACHOQUES_POSTERIORES"));
          $data['DatPersonal']['B_PARACHOQUES_POSTERIORES']	 	= utf8_encode(odbc_result($result_Perso_E,"B_PARACHOQUES_POSTERIORES"));
          $data['DatPersonal']['LUNAS_LATERALES']	 	= utf8_encode(odbc_result($result_Perso_E,"LUNAS_LATERALES"));
          $data['DatPersonal']['B_LUNAS_LATERALES']	 	= utf8_encode(odbc_result($result_Perso_E,"B_LUNAS_LATERALES"));
          $data['DatPersonal']['LUNAS_CORTAVIENTO']	 	= utf8_encode(odbc_result($result_Perso_E,"LUNAS_CORTAVIENTO"));
          $data['DatPersonal']['B_LUNAS_CORTAVIENTO']	 	= utf8_encode(odbc_result($result_Perso_E,"B_LUNAS_CORTAVIENTO"));
          $data['DatPersonal']['PARABRISAS_DELANTERO']	 	= utf8_encode(odbc_result($result_Perso_E,"PARABRISAS_DELANTERO"));
          $data['DatPersonal']['B_PARABRISAS_DELANTERO']	 	= utf8_encode(odbc_result($result_Perso_E,"B_PARABRISAS_DELANTERO"));
          $data['DatPersonal']['PARABRISAS_POSTERIOR']	 	= utf8_encode(odbc_result($result_Perso_E,"PARABRISAS_POSTERIOR"));
          $data['DatPersonal']['B_PARABRISAS_POSTERIOR']	 	= utf8_encode(odbc_result($result_Perso_E,"B_PARABRISAS_POSTERIOR"));
          $data['DatPersonal']['TANQUE_COMBUSTIBLE']	 	= utf8_encode(odbc_result($result_Perso_E,"TANQUE_COMBUSTIBLE"));
          $data['DatPersonal']['B_TANQUE_COMBUSTIBLE']	 	= utf8_encode(odbc_result($result_Perso_E,"B_TANQUE_COMBUSTIBLE"));
          $data['DatPersonal']['PUERTAS']	 	= utf8_encode(odbc_result($result_Perso_E,"PUERTAS"));
          $data['DatPersonal']['B_PUERTAS']	 	= utf8_encode(odbc_result($result_Perso_E,"B_PUERTAS"));
          $data['DatPersonal']['ASIENTOS']	 	= utf8_encode(odbc_result($result_Perso_E,"ASIENTOS"));
          $data['DatPersonal']['B_ASIENTOS']	 	= utf8_encode(odbc_result($result_Perso_E,"B_ASIENTOS"));
          $data['DatPersonal']['AIRE_ACONDICIONADO']	 	= utf8_encode(odbc_result($result_Perso_E,"AIRE_ACONDICIONADO"));
          $data['DatPersonal']['B_AIRE_ACONDICIONADO']	 	= utf8_encode(odbc_result($result_Perso_E,"B_AIRE_ACONDICIONADO"));
          $data['DatPersonal']['ALARMA']	 	= utf8_encode(odbc_result($result_Perso_E,"ALARMA"));
          $data['DatPersonal']['B_ALARMA']	 	= utf8_encode(odbc_result($result_Perso_E,"B_ALARMA"));
          $data['DatPersonal']['PLUMILLAS']	 	= utf8_encode(odbc_result($result_Perso_E,"PLUMILLAS"));
          $data['DatPersonal']['B_PLUMILLAS']	 	= utf8_encode(odbc_result($result_Perso_E,"B_PLUMILLAS"));
          $data['DatPersonal']['ESPEJOS']	 	= utf8_encode(odbc_result($result_Perso_E,"ESPEJOS"));
          $data['DatPersonal']['B_ESPEJOS']	 	= utf8_encode(odbc_result($result_Perso_E,"B_ESPEJOS"));
          $data['DatPersonal']['CINTURONES_SEGURIDAD']	 	= utf8_encode(odbc_result($result_Perso_E,"CINTURONES_SEGURIDAD"));
          $data['DatPersonal']['B_CINTURONES_SEGURIDAD']	 	= utf8_encode(odbc_result($result_Perso_E,"B_CINTURONES_SEGURIDAD"));
          $data['DatPersonal']['ANTENA']	 	= utf8_encode(odbc_result($result_Perso_E,"ANTENA"));
          $data['DatPersonal']['B_ANTENA']	 	= utf8_encode(odbc_result($result_Perso_E,"B_ANTENA"));
          $data['DatPersonal']['OTRAS_CARACTERISTICAS_RELEVANTES']	 	= utf8_encode(odbc_result($result_Perso_E,"OTRAS_CARACTERISTICAS_RELEVANTES"));
          $data['DatPersonal']['VALOR_TASACION']	 	= utf8_encode(odbc_result($result_Perso_E,"VALOR_TASACION"));

      		$this->render('v.form_personal.php', $data);
      	}


        public function F_Guardar_Form_Registrar_FichaVehicular(){

          $data = array();

          $objFichaVModel 	= new M_TBL_MUEBLES_UE_BIE_FT_VEHICULAR_Model();

          $TXH_SIMI_COD_ENTIDAD = isset($_GET['TXH_SIMI_COD_ENTIDAD']) ? $_GET['TXH_SIMI_COD_ENTIDAD'] : '';
          $TXH_ID_USUARIO = isset($_GET['TXH_ID_USUARIO']) ? $_GET['TXH_ID_USUARIO'] : '';

          $txt_COD_UE_BIEN_PATRI = isset($_GET['txt_COD_UE_BIEN_PATRI']) ? $_GET['txt_COD_UE_BIEN_PATRI'] : '';
          $txt_DENOMINACION = isset($_GET['txt_DENOMINACION']) ? $_GET['txt_DENOMINACION'] : '';
          $txt_COD_UE_FT_VEHICULAR = isset($_GET['txt_COD_UE_FT_VEHICULAR']) ? $_GET['txt_COD_UE_FT_VEHICULAR'] : '';


          $txt_placa = isset($_GET['txt_placa']) ? $_GET['txt_placa'] : '';
          $txt_carroceria = isset($_GET['txt_carroceria']) ? $_GET['txt_carroceria'] : '';
          $txt_marca = isset($_GET['txt_marca']) ? $_GET['txt_marca'] : '';
          $txt_modelo = isset($_GET['txt_modelo']) ? $_GET['txt_modelo'] : '';
          $txt_categoria = isset($_GET['txt_categoria']) ? $_GET['txt_categoria'] : '';
          $txt_nrochasis = isset($_GET['txt_nrochasis']) ? $_GET['txt_nrochasis'] : '';
          $txt_nroejes = isset($_GET['txt_nroejes']) ? $_GET['txt_nroejes'] : '';
          $txt_nromotor = isset($_GET['txt_nromotor']) ? $_GET['txt_nromotor'] : '';
          $txt_nroserie = isset($_GET['txt_nroserie']) ? $_GET['txt_nroserie'] : '';
          $txt_aniofabricacion = isset($_GET['txt_aniofabricacion']) ? $_GET['txt_aniofabricacion'] : '';
          $txt_color = isset($_GET['txt_color']) ? $_GET['txt_color'] : '';
          $txt_combustible = isset($_GET['txt_combustible']) ? $_GET['txt_combustible'] : '';
          $txt_transmision = isset($_GET['txt_transmision']) ? $_GET['txt_transmision'] : '';
          $txt_cilindrada = isset($_GET['txt_cilindrada']) ? $_GET['txt_cilindrada'] : '';
          $txt_kilometraje = isset($_GET['txt_kilometraje']) ? $_GET['txt_kilometraje'] : '';
          $txt_tarjetapropiedad = isset($_GET['txt_tarjetapropiedad']) ? $_GET['txt_tarjetapropiedad'] : '';


          $txt_cilindros = isset($_GET['txt_cilindros']) ? $_GET['txt_cilindros'] : '';
          $txt_carburador = isset($_GET['txt_carburador']) ? $_GET['txt_carburador'] : '';
          $txt_distribuidor = isset($_GET['txt_distribuidor']) ? $_GET['txt_distribuidor'] : '';
          $txt_bombagasolina = isset($_GET['txt_bombagasolina']) ? $_GET['txt_bombagasolina'] : '';
          $txt_purificadoraire = isset($_GET['txt_purificadoraire']) ? $_GET['txt_purificadoraire'] : '';
          $txt_bombafrenos = isset($_GET['txt_bombafrenos']) ? $_GET['txt_bombafrenos'] : '';
          $txt_zapatas = isset($_GET['txt_zapatas']) ? $_GET['txt_zapatas'] : '';
          $txt_discos = isset($_GET['txt_discos']) ? $_GET['txt_discos'] : '';


          $txt_radiador = isset($_GET['txt_radiador']) ? $_GET['txt_radiador'] : '';
          $txt_ventilador = isset($_GET['txt_ventilador']) ? $_GET['txt_ventilador'] : '';
          $txt_bomba_agua = isset($_GET['txt_bomba_agua']) ? $_GET['txt_bomba_agua'] : '';
          $txt_motor_arranque = isset($_GET['txt_motor_arranque']) ? $_GET['txt_motor_arranque'] : '';
          $txt_bateria = isset($_GET['txt_bateria']) ? $_GET['txt_bateria'] : '';
          $txt_alternador = isset($_GET['txt_alternador']) ? $_GET['txt_alternador'] : '';
          $txt_bobina = isset($_GET['txt_bobina']) ? $_GET['txt_bobina'] : '';
          $txt_relay_alternador = isset($_GET['txt_relay_alternador']) ? $_GET['txt_relay_alternador'] : '';
          $txt_farosdelanteros = isset($_GET['txt_farosdelanteros']) ? $_GET['txt_farosdelanteros'] : '';
          $txt_direccionalesdelanteros = isset($_GET['txt_direccionalesdelanteros']) ? $_GET['txt_direccionalesdelanteros'] : '';
          $txt_lucesposteriores = isset($_GET['txt_lucesposteriores']) ? $_GET['txt_lucesposteriores'] : '';
          $txt_direccionalesposteriores = isset($_GET['txt_direccionalesposteriores']) ? $_GET['txt_direccionalesposteriores'] : '';
          $txt_autoradio = isset($_GET['txt_autoradio']) ? $_GET['txt_autoradio'] : '';
          $txt_parlantes = isset($_GET['txt_parlantes']) ? $_GET['txt_parlantes'] : '';
          $txt_claxon = isset($_GET['txt_claxon']) ? $_GET['txt_claxon'] : '';
          $txt_circuito_luces = isset($_GET['txt_circuito_luces']) ? $_GET['txt_circuito_luces'] : '';


          $txt_caja_cambios = isset($_GET['txt_caja_cambios']) ? $_GET['txt_caja_cambios'] : '';
          $txt_bomba_embrague = isset($_GET['txt_bomba_embrague']) ? $_GET['txt_bomba_embrague'] : '';
          $txt_caja_transferencia = isset($_GET['txt_caja_transferencia']) ? $_GET['txt_caja_transferencia'] : '';
          $txt_diferencialtrasero = isset($_GET['txt_diferencialtrasero']) ? $_GET['txt_diferencialtrasero'] : '';
          $txt_diferencialdelantero = isset($_GET['txt_diferencialdelantero']) ? $_GET['txt_diferencialdelantero'] : '';
          $txt_volante = isset($_GET['txt_volante']) ? $_GET['txt_volante'] : '';
          $txt_cania_direccion = isset($_GET['txt_cania_direccion']) ? $_GET['txt_cania_direccion'] : '';
          $txt_cremallera = isset($_GET['txt_cremallera']) ? $_GET['txt_cremallera'] : '';
          $txt_rotulas = isset($_GET['txt_rotulas']) ? $_GET['txt_rotulas'] : '';


          $txt_amortiguadores_muelles = isset($_GET['txt_amortiguadores_muelles']) ? $_GET['txt_amortiguadores_muelles'] : '';
          $txt_barra_torsion = isset($_GET['txt_barra_torsion']) ? $_GET['txt_barra_torsion'] : '';
          $txt_barraestabilizadora = isset($_GET['txt_barraestabilizadora']) ? $_GET['txt_barraestabilizadora'] : '';
          $txt_llantas = isset($_GET['txt_llantas']) ? $_GET['txt_llantas'] : '';
          $txt_capot_motor = isset($_GET['txt_capot_motor']) ? $_GET['txt_capot_motor'] : '';
          $txt_capot_maletera = isset($_GET['txt_capot_maletera']) ? $_GET['txt_capot_maletera'] : '';
          $txt_parachoquesdelanteros = isset($_GET['txt_parachoquesdelanteros']) ? $_GET['txt_parachoquesdelanteros'] : '';
          $txt_parachoquesposteriores = isset($_GET['txt_parachoquesposteriores']) ? $_GET['txt_parachoquesposteriores'] : '';
          $txt_lunaslaterales = isset($_GET['txt_lunaslaterales']) ? $_GET['txt_lunaslaterales'] : '';
          $txt_lunascortavientos = isset($_GET['txt_lunascortavientos']) ? $_GET['txt_lunascortavientos'] : '';
          $txt_parabrisasdelanteras = isset($_GET['txt_parabrisasdelanteras']) ? $_GET['txt_parabrisasdelanteras'] : '';
          $txt_parabrisasposteriores = isset($_GET['txt_parabrisasposteriores']) ? $_GET['txt_parabrisasposteriores'] : '';
          $txt_tanque_combustible = isset($_GET['txt_tanque_combustible']) ? $_GET['txt_tanque_combustible'] : '';
          $txt_puertas = isset($_GET['txt_puertas']) ? $_GET['txt_puertas'] : '';
          $txt_asientos = isset($_GET['txt_asientos']) ? $_GET['txt_asientos'] : '';

          $txt_aireacondicionado = isset($_GET['txt_aireacondicionado']) ? $_GET['txt_aireacondicionado'] : '';
          $txt_alarma = isset($_GET['txt_alarma']) ? $_GET['txt_alarma'] : '';
          $txt_plumillas = isset($_GET['txt_plumillas']) ? $_GET['txt_plumillas'] : '';
          $txt_espejos = isset($_GET['txt_espejos']) ? $_GET['txt_espejos'] : '';
          $txt_cinturones = isset($_GET['txt_cinturones']) ? $_GET['txt_cinturones'] : '';
          $txt_antena = isset($_GET['txt_antena']) ? $_GET['txt_antena'] : '';


          /**** Controles CheckBox ****/
          $chk_cilindros = isset($_GET['chk_cilindros']) ? $_GET['chk_cilindros'] : '';
          $chk_carburador = isset($_GET['chk_carburador']) ? $_GET['chk_carburador'] : '';
          $chk_distribuidor = isset($_GET['chk_distribuidor']) ? $_GET['chk_distribuidor'] : '';
          $chk_bombagasolina = isset($_GET['chk_bombagasolina']) ? $_GET['chk_bombagasolina'] : '';
          $chk_purificadoraire = isset($_GET['chk_purificadoraire']) ? $_GET['chk_purificadoraire'] : '';
          $chk_bombafrenos = isset($_GET['chk_bombafrenos']) ? $_GET['chk_bombafrenos'] : '';
          $chk_zapatas = isset($_GET['chk_zapatas']) ? $_GET['chk_zapatas'] : '';
          $chk_discos = isset($_GET['chk_discos']) ? $_GET['chk_discos'] : '';


          $chk_radiador = isset($_GET['chk_radiador']) ? $_GET['chk_radiador'] : '';
          $chk_ventilador = isset($_GET['chk_ventilador']) ? $_GET['chk_ventilador'] : '';
          $chk_bomba_agua = isset($_GET['chk_bomba_agua']) ? $_GET['chk_bomba_agua'] : '';
          $chk_motor_arranque = isset($_GET['chk_motor_arranque']) ? $_GET['chk_motor_arranque'] : '';
          $chk_bateria = isset($_GET['chk_bateria']) ? $_GET['chk_bateria'] : '';
          $chk_alternador = isset($_GET['chk_alternador']) ? $_GET['chk_alternador'] : '';
          $chk_bobina = isset($_GET['chk_bobina']) ? $_GET['chk_bobina'] : '';
          $chk_relay_alternador = isset($_GET['chk_relay_alternador']) ? $_GET['chk_relay_alternador'] : '';
          $chk_farosdelanteros = isset($_GET['chk_farosdelanteros']) ? $_GET['chk_farosdelanteros'] : '';
          $chk_direccionalesdelanteros = isset($_GET['chk_direccionalesdelanteros']) ? $_GET['chk_direccionalesdelanteros'] : '';
          $chk_lucesposteriores = isset($_GET['chk_lucesposteriores']) ? $_GET['chk_lucesposteriores'] : '';
          $chk_direccionalesposteriores = isset($_GET['chk_direccionalesposteriores']) ? $_GET['chk_direccionalesposteriores'] : '';
          $chk_autoradio = isset($_GET['chk_autoradio']) ? $_GET['chk_autoradio'] : '';
          $chk_parlantes = isset($_GET['chk_parlantes']) ? $_GET['chk_parlantes'] : '';
          $chk_claxon = isset($_GET['chk_claxon']) ? $_GET['chk_claxon'] : '';
          $chk_circuito_luces = isset($_GET['chk_circuito_luces']) ? $_GET['chk_circuito_luces'] : '';


          $chk_caja_cambios = isset($_GET['chk_caja_cambios']) ? $_GET['chk_caja_cambios'] : '';
          $chk_bomba_embrague = isset($_GET['chk_bomba_embrague']) ? $_GET['chk_bomba_embrague'] : '';
          $chk_caja_transferencia = isset($_GET['chk_caja_transferencia']) ? $_GET['chk_caja_transferencia'] : '';
          $chk_diferencialtrasero = isset($_GET['chk_diferencialtrasero']) ? $_GET['chk_diferencialtrasero'] : '';
          $chk_diferencialdelantero = isset($_GET['chk_diferencialdelantero']) ? $_GET['chk_diferencialdelantero'] : '';
          $chk_volante = isset($_GET['chk_volante']) ? $_GET['chk_volante'] : '';
          $chk_cania_direccion = isset($_GET['chk_cania_direccion']) ? $_GET['chk_cania_direccion'] : '';
          $chk_cremallera = isset($_GET['chk_cremallera']) ? $_GET['chk_cremallera'] : '';
          $chk_rotulas = isset($_GET['chk_rotulas']) ? $_GET['chk_rotulas'] : '';


          $chk_amortiguadores_muelles = isset($_GET['chk_amortiguadores_muelles']) ? $_GET['chk_amortiguadores_muelles'] : '';
          $chk_barra_torsion = isset($_GET['chk_barra_torsion']) ? $_GET['chk_barra_torsion'] : '';
          $chk_barraestabilizadora = isset($_GET['chk_barraestabilizadora']) ? $_GET['chk_barraestabilizadora'] : '';
          $chk_llantas = isset($_GET['chk_llantas']) ? $_GET['chk_llantas'] : '';
          $chk_capot_motor = isset($_GET['chk_capot_motor']) ? $_GET['chk_capot_motor'] : '';
          $chk_capot_maletera = isset($_GET['chk_capot_maletera']) ? $_GET['chk_capot_maletera'] : '';
          $chk_parachoquesdelanteros = isset($_GET['chk_parachoquesdelanteros']) ? $_GET['chk_parachoquesdelanteros'] : '';
          $chk_parachoquesposteriores = isset($_GET['chk_parachoquesposteriores']) ? $_GET['chk_parachoquesposteriores'] : '';
          $chk_lunaslaterales = isset($_GET['chk_lunaslaterales']) ? $_GET['chk_lunaslaterales'] : '';
          $chk_lunascortavientos = isset($_GET['chk_lunascortavientos']) ? $_GET['chk_lunascortavientos'] : '';
          $chk_parabrisasdelanteras = isset($_GET['chk_parabrisasdelanteras']) ? $_GET['chk_parabrisasdelanteras'] : '';
          $chk_parabrisasposteriores = isset($_GET['chk_parabrisasposteriores']) ? $_GET['chk_parabrisasposteriores'] : '';
          $chk_tanque_combustible = isset($_GET['chk_tanque_combustible']) ? $_GET['chk_tanque_combustible'] : '';
          $chk_puertas = isset($_GET['chk_puertas']) ? $_GET['chk_puertas'] : '';
          $chk_asientos = isset($_GET['chk_asientos']) ? $_GET['chk_asientos'] : '';

          $chk_aireacondicionado = isset($_GET['chk_aireacondicionado']) ? $_GET['chk_aireacondicionado'] : '';
          $chk_alarma = isset($_GET['chk_alarma']) ? $_GET['chk_alarma'] : '';
          $chk_plumillas = isset($_GET['chk_plumillas']) ? $_GET['chk_plumillas'] : '';
          $chk_espejos = isset($_GET['chk_espejos']) ? $_GET['chk_espejos'] : '';
          $chk_cinturones = isset($_GET['chk_cinturones']) ? $_GET['chk_cinturones'] : '';
          $chk_antena = isset($_GET['chk_antena']) ? $_GET['chk_antena'] : '';


          $txa_caracteristicasrelevantes = isset($_GET['txa_caracteristicasrelevantes']) ? $_GET['txa_caracteristicasrelevantes'] : '';
          $txt_tasacion = isset($_GET['txt_tasacion']) ? $_GET['txt_tasacion'] : 0;


          $rs_Resultado = $objFichaVModel->F_Guardar_Ficha_Vehicular(
            $txt_COD_UE_FT_VEHICULAR,
            $TXH_SIMI_COD_ENTIDAD,
            $TXH_ID_USUARIO,
            $txt_COD_UE_BIEN_PATRI,
            $txt_DENOMINACION,
            $txt_placa,
            $txt_carroceria,
            $txt_marca,
            $txt_modelo,
            $txt_categoria,
            $txt_nrochasis,
            $txt_nroejes,
            $txt_nromotor,
            $txt_nroserie,
            $txt_aniofabricacion,
            $txt_color,
            $txt_combustible,
            $txt_transmision,
            $txt_cilindrada,
            $txt_kilometraje,
            $txt_tarjetapropiedad,
            $txt_cilindros,
            $chk_cilindros,
            $txt_carburador,
            $chk_carburador,
            $txt_distribuidor,
            $chk_distribuidor,
            $txt_bombagasolina,
            $chk_bombagasolina,
            $txt_purificadoraire,
            $chk_purificadoraire,
            $txt_bombafrenos,
            $chk_bombafrenos,
            $txt_zapatas,
            $chk_zapatas,
            $txt_discos,
            $chk_discos,
            $txt_radiador,
            $chk_radiador,
            $txt_ventilador,
            $chk_ventilador,
            $txt_bomba_agua,
            $chk_bomba_agua,
            $txt_motor_arranque,
            $chk_motor_arranque,
            $txt_bateria,
            $chk_bateria,
            $txt_alternador,
            $chk_alternador,
            $txt_bobina,
            $chk_bobina,
            $txt_relay_alternador,
            $chk_relay_alternador,
            $txt_farosdelanteros,
            $chk_farosdelanteros,
            $txt_direccionalesdelanteros,
            $chk_direccionalesdelanteros,
            $txt_lucesposteriores,
            $chk_lucesposteriores,
            $txt_direccionalesposteriores,
            $chk_direccionalesposteriores,
            $txt_autoradio,
            $chk_autoradio,
            $txt_parlantes,
            $chk_parlantes,
            $txt_claxon,
            $chk_claxon,
            $txt_circuito_luces,
            $chk_circuito_luces,
            $txt_caja_cambios,
            $chk_caja_cambios,
            $txt_bomba_embrague,
            $chk_bomba_embrague,
            $txt_caja_transferencia,
            $chk_caja_transferencia,
            $txt_diferencialtrasero,
            $chk_diferencialtrasero,
            $txt_diferencialdelantero,
            $chk_diferencialdelantero,
            $txt_volante,
            $chk_volante,
            $txt_cania_direccion,
            $chk_cania_direccion,
            $txt_cremallera,
            $chk_cremallera,
            $txt_rotulas,
            $chk_rotulas,
            $txt_amortiguadores_muelles,
            $chk_amortiguadores_muelles,
            $txt_barra_torsion,
            $chk_barra_torsion,
            $txt_barraestabilizadora,
            $chk_barraestabilizadora,
            $txt_llantas,
            $chk_llantas,
            $txt_capot_motor,
            $chk_capot_motor,
            $txt_capot_maletera,
            $chk_capot_maletera,
            $txt_parachoquesdelanteros,
            $chk_parachoquesdelanteros,
            $txt_parachoquesposteriores,
            $chk_parachoquesposteriores,
            $txt_lunaslaterales,
            $chk_lunaslaterales,
            $txt_lunascortavientos,
            $chk_lunascortavientos,
            $txt_parabrisasdelanteras,
            $chk_parabrisasdelanteras,
            $txt_parabrisasposteriores,
            $chk_parabrisasposteriores,
            $txt_tanque_combustible,
            $chk_tanque_combustible,
            $txt_puertas,
            $chk_puertas,
            $txt_asientos,
            $chk_asientos,
            $txt_aireacondicionado,
            $chk_aireacondicionado,
            $txt_alarma,
            $chk_alarma,
            $txt_plumillas,
            $chk_plumillas,
            $txt_espejos,
            $chk_espejos,
            $txt_cinturones,
            $chk_cinturones,
            $txt_antena,
            $chk_antena,
            $txa_caracteristicasrelevantes,
            $txt_tasacion
          );

            $valor = (isset($rs_Resultado)) ? '1' : '0';
            echo $valor;



        }

        function F_ListadoBienesPatrimoniales(
            $ACCION, $INI, $FIN, $COD_ENTIDAD, $ANIO_ADQUIS, $COD_GRUPO, $COD_CLASE,
            $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN )
        {
            return $this->ObjModel->F_ListadoBienesPatrimoniales(
                $ACCION, $INI, $FIN, $COD_ENTIDAD, $ANIO_ADQUIS, $COD_GRUPO, $COD_CLASE,
            $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN );
        }

        function F_TotalBienesPatrimoniales(
              $ACCION, $INI, $FIN, $COD_ENTIDAD, $ANIO_ADQUIS, $COD_GRUPO, $COD_CLASE,
            $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN )
        {
            return $this->ObjModel->F_TotalBienesPatrimoniales(
                $ACCION, $INI, $FIN, $COD_ENTIDAD, $ANIO_ADQUIS, $COD_GRUPO, $COD_CLASE,
            $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN );
        }

        function F_BuscarBienPatrimonial_X_Codigo( $COD_UE_BIEN_PATRI )
        {
            return $this->ObjModel->F_BuscarBienPatrimonial_X_Codigo( $COD_UE_BIEN_PATRI );
        }


        function F_ListadoBienesPatrimoniales_X_Predios( $ACCION, $COD_ENTIDAD, $ID_PREDIO_SBN, $INI, $FIN )
        {
            return $this->ObjModel->F_ListadoBienesPatrimoniales_X_Predios( $ACCION, $COD_ENTIDAD, $ID_PREDIO_SBN, $INI, $FIN );
        }

        function F_Clasificacion_Bienes_X_Depreciacion( $COD_ENTIDAD, $ANIO_DEP, $MES_DEP )
        {
            return $this->ObjModel->F_Clasificacion_Bienes_X_Depreciacion( $COD_ENTIDAD, $ANIO_DEP, $MES_DEP );
        }

        function F_ListadoClasificacion_Bienes
        (
            $ACCION, $COD_ENTIDAD, $ANIO_DEP, $MES_DEP, $GRUPO_BIEN_DEP, $INI, $FIN
        )
        {
            return $this->ObjModel->F_ListadoClasificacion_Bienes( $ACCION, $COD_ENTIDAD, $ANIO_DEP, $MES_DEP, $GRUPO_BIEN_DEP, $INI, $FIN );
        }


     
        function F_ListadoBienesActivos_NO_Catalogados( $COD_ENTIDAD )
        {
            return $this->ObjModel->F_ListadoBienesActivos_NO_Catalogados( $COD_ENTIDAD );
        }


        function F_ListadoBienesBaja_NO_Catalogados( $COD_ENTIDAD )
        {
            return $this->ObjModel->F_ListadoBienesBaja_NO_Catalogados( $COD_ENTIDAD );
        }


        function F_Exporta_Excel_FichasVehiculares(){

          $objFichaVModel 	= new M_TBL_MUEBLES_UE_BIE_FT_VEHICULAR_Model();
          $TXH_SIMI_COD_ENTIDAD = isset($_GET['TXH_SIMI_COD_ENTIDAD']) ? $_GET['TXH_SIMI_COD_ENTIDAD'] : '';

          $rs_Resultado = $objFichaVModel->F_Exportar_Excel_FichasVehiculares( $TXH_SIMI_COD_ENTIDAD );

          $this->render('Exportar_Excel_FichaVehicular.php', $rs_Resultado);

        }


        function F_ListadoBienesComponentes
        (
            $ACCION, $INI, $FIN, $COD_ENTIDAD, $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN
        )
        {
            return $this->ObjModel->F_ListadoBienesComponentes( $ACCION, $INI, $FIN, $COD_ENTIDAD, $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN );
        }


        public function render($template, $values){
            $render = function () use ($template, $values){
                $data = $values;
                include "$template";
            };
            return $render();
        }

        /**
         * Imprime una variable respetando sangrias y espaciado, con la funcion printr.
         * @param $data Valor a imprimir en la pantalla del navegador.
         * @return Nothing.
         */
        public function dump($data){
            print '<pre>';
            print_r($data);
            print '</pre>';
        }

    }

?>
