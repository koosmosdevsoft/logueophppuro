<?
require_once("C_Interconexion_SQL.php");

class Simi_Resolucion_Catalogo{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
	
	
	function Simi_Generar_Codigo_Resolucion_Catalogo(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(COD_RESOL_CATALOGO),0) + 1) as CODIGO FROM TBL_MUEBLES_RESOLUC_CATALOGO ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Simi_Insertar_Resolucion_Catalogo($COD_RESOL_CATALOGO, $NRO_RESOLUCION, $FECHA_EMISION, $FECHA_PUBLICACION, $CODIGO_RESOL_SID, $USUARIO_CREACION){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_RESOLUC_CATALOGO  (COD_RESOL_CATALOGO, NRO_RESOLUCION, FECHA_EMISION, FECHA_PUBLICACION, CODIGO_RESOL_SID, FECHA_REGISTRO, USUARIO_CREACION, FECHA_CREACION, ID_ESTADO)
						VALUES ('$COD_RESOL_CATALOGO', '$NRO_RESOLUCION', '$FECHA_EMISION', '$FECHA_PUBLICACION', '$CODIGO_RESOL_SID', GETDATE(), '$USUARIO_CREACION', GETDATE(), 1) ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Simi_Actualizar_Resolucion_Catalogo($COD_RESOL_CATALOGO, $NRO_RESOLUCION, $FECHA_EMISION, $FECHA_PUBLICACION, $CODIGO_RESOL_SID, $USUARIO_MODIFICA){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_MUEBLES_RESOLUC_CATALOGO 
			SET NRO_RESOLUCION = '$NRO_RESOLUCION', FECHA_EMISION = '$FECHA_EMISION', FECHA_PUBLICACION = '$FECHA_PUBLICACION', CODIGO_RESOL_SID = '$CODIGO_RESOL_SID', USUARIO_MODIFICA = '$USUARIO_MODIFICA', FECHA_MODIFICA = GETDATE() 
			WHERE COD_RESOL_CATALOGO = $COD_RESOL_CATALOGO";
			$result = $this->oDBManager->execute($sql);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Simi_Eliminar_Resolucion_Catalogo($COD_RESOL_CATALOGO, $USUARIO_ELIMINA){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_RESOLUC_CATALOGO SET ID_ESTADO = 2, USUARIO_ELIMINA = '$USUARIO_ELIMINA', FECHA_ELIMINA = GETDATE() WHERE COD_RESOL_CATALOGO = '$COD_RESOL_CATALOGO' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Simi_Listar_Resolucion_Catalogo(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_RESOLUC_CATALOGO WHERE ID_ESTADO = '1' order by CAST(FECHA_EMISION AS DATE) DESC";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Simi_Ver_Resolucion_Catalogo_x_Codigo($COD_RESOL_CATALOGO){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_RESOLUC_CATALOGO WHERE COD_RESOL_CATALOGO = '$COD_RESOL_CATALOGO' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
		
	
}
?>