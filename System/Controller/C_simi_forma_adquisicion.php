<?
require_once("C_Interconexion_SQL.php");

class Simi_Forma_Adquisicion{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
	
	
	function Simi_Generar_Codigo_Forma_Adquisicion(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(COD_FORM_ADQUIS),0) + 1) as CODIGO FROM TBL_MUEBLES_FORM_ADQUISICION ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Simi_Listar_Forma_Adquisicion(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_FORM_ADQUISICION WHERE ID_ESTADO = '1' ORDER BY ORDEN ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	
}
?>