<?
require_once("C_Interconexion_SQL.php");

class Simi_UE_Area{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
	
	
	function Generar_Codigo_Area_SIMI(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(COD_UE_AREA),0) + 1) as CODIGO FROM TBL_MUEBLES_UE_AREA ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Insertar_Simi_UE_Area($ID_PREDIO_SBN, $DESC_AREA, $SIGLAS_AREA, $USUARIO_CREACION){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_UE_AREA (ID_PREDIO_SBN, DESC_AREA, SIGLAS_AREA, FECHA_REGISTRO, USUARIO_CREACION, FECHA_CREACION, ID_ESTADO)
						VALUES ('$ID_PREDIO_SBN', '$DESC_AREA', '$SIGLAS_AREA', GETDATE(), '$USUARIO_CREACION', GETDATE(), '1') ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Actualizar_Simi_UE_Area($COD_UE_AREA, $ID_PREDIO_SBN, $DESC_AREA, $SIGLAS_AREA, $USUARIO_MODIFICA){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_MUEBLES_UE_AREA 
			SET ID_PREDIO_SBN = '$ID_PREDIO_SBN', DESC_AREA = '$DESC_AREA', SIGLAS_AREA = '$SIGLAS_AREA', USUARIO_MODIFICA = '$USUARIO_MODIFICA', FECHA_MODIFICA = GETDATE() 
			WHERE COD_UE_AREA = $COD_UE_AREA";
			$result = $this->oDBManager->execute($sql);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Eliminar_Simi_UE_Area($COD_UE_AREA, $USUARIO_ELIMINA){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_UE_AREA SET ID_ESTADO = 2, USUARIO_ELIMINA = '$USUARIO_ELIMINA', FECHA_ELIMINA = GETDATE() WHERE COD_UE_AREA = '$COD_UE_AREA' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}	


	function Eliminar_Area($COD_UE_AREA, $USUARIO_ELIMINA){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_UE_AREA SET ID_ESTADO = 2, USUARIO_ELIMINA = '$USUARIO_ELIMINA', FECHA_ELIMINA = GETDATE() WHERE COD_UE_AREA = '$COD_UE_AREA' ";

			//echo $consulta;
			$result = $this->oDBManager->execute($consulta);
			//$this->oDBManager->close();
			return $result;
		}
	}	

	function Eliminar_Area_Validacion( $COD_ENTIDAD, $COD_UE_AREA ){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT COUNT(COD_UE_BIEN_PATRI)[CANTIDAD] FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL WHERE COD_ENTIDAD = $COD_ENTIDAD AND COD_UE_AREA = '$COD_UE_AREA' AND ID_ESTADO = '1' and COD_TIPO_MOV in (2) ";

			//echo $consulta;
			$result = $this->oDBManager->execute($consulta);
			//$this->oDBManager->close();
			return $result;
		}
	}	
	
	
	
	function Listar_Simi_UE_Area($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_UE_AREA WHERE ID_ESTADO = 1 AND ID_PREDIO_SBN = '$ID_PREDIO_SBN' ORDER BY DESC_AREA ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	//=============================================================================================================================
	
	
	function TOTAL_Simi_UE_Area_X_PARAMETROS($ID_PREDIO_SBN, $DESC_AREA){
		if($this->oDBManager->conectar()==true){
			
			if($DESC_AREA != ''){
				$COND1 = " AND DESC_AREA LIKE '%$DESC_AREA%' ";
			}else{
				$COND1 = "";
			}
			
			$sql="SELECT COUNT(*) AS TOT_REG FROM TBL_MUEBLES_UE_AREA WHERE ID_ESTADO  = '1' AND ID_PREDIO_SBN = '$ID_PREDIO_SBN' $COND1 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function LISTA_Simi_UE_Area_X_PARAMETROS($INI,$FIN, $ID_PREDIO_SBN, $DESC_AREA){
		if($this->oDBManager->conectar()==true){
			
			if($DESC_AREA != ''){
				$COND1 = " AND DESC_AREA LIKE '%$DESC_AREA%' ";	
			}else{
				$COND1 = "";
			}
			
			$SQL_ORIGEN="SELECT *, ROW_NUMBER() OVER (ORDER BY COD_UE_AREA) AS ROW_NUMBER_ID
						FROM TBL_MUEBLES_UE_AREA
						WHERE ID_ESTADO = '1' AND ID_PREDIO_SBN = '$ID_PREDIO_SBN' $COND1 
				";			
			$sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Ver_Datos_Simi_UE_Area_x_CODIGO($COD_UE_AREA){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_UE_AREA WHERE COD_UE_AREA = '$COD_UE_AREA' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}



	public function validar_entero(string $numero): bool
	{
		return (filter_var($numero, FILTER_VALIDATE_INT) === FALSE) ? False : True;
	}

	public function validar_caracteresEspeciales(string $texto): bool
	{
		$patron = "[^?]";
		//$res = array("options"=>array("regexp"=>"/[^0-9a-zA-ZñÑáéíóúÁÉÍÓÚ.()/::,@ _-]/g"));
		echo (filter_var($texto, FILTER_VALIDATE_REGEXP,$patron));
		//echo preg_match($patron,$texto);
		
		
	}
		
}
?>