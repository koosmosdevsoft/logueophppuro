
<?php

$oPrediosfunction	=	new PrediosController;


class PrediosController{

	
	
	

	const NRO_FILA_LIST	= 50;
	const NRO_LINK_PAGE = 6;

		
    public function index(){
		$data = array();
		$data['Departamento'] 			= $this->Mostrar_Lista_Departamento();
		//$data['Provincia'] 				= $this->Mostrar_Lista_Provincia(utf8_encode(odbc_result($result_Predio_E,"COD_DEPA")));
		//$data['Distrito'] 				= $this->Mostrar_Lista_Distrito(utf8_encode(odbc_result($result_Predio_E,"COD_DEPA")), utf8_encode(odbc_result($result_Predio_E,"COD_PROV")));
		
		$data['TipoZona'] 				= $this->Mostrar_Lista_Tipo_Zona_Inmueble();
		$data['TipoVia'] 				= $this->Mostrar_Lista_Tipo_Via();
		$data['TipoHabilitacion'] 		= $this->Mostrar_Lista_Tipo_Habilitacion();
		$data['TipoPropiedad'] 			= $this->Mostrar_Listar_Tipo_Propiedad();
		
		$data['DatOficRegistral'] 		= $this->Mostrar_Lista_Oficina_Registral();
		$data['DatUnidMedida'] 			= $this->Mostrar_Listar_Unidad_Medida();
		$data['DatTipTerreno'] 			= $this->Mostrar_Listar_Tipo_Terreno();
		$data['DatEstConservacion'] 	= $this->Mostrar_Lista_Estado_Conservacion();
		$data['DatEstSaneamiento'] 		= $this->Mostrar_Lista_Estado_Saneamiento();
		//$data['DatDetalleSaneamiento'] 	= $this->Mostrar_Lista_Estado_Saneamiento_Detalle(utf8_encode(odbc_result($result_Predio_E,"COD_ESANEAMIENTO")));
		$data['DatUsoPredio'] 			= $this->Mostrar_Lista_Uso_Predio();
		$data['DatTipoDocTasa'] 		= $this->Mostrar_Lista_Inmuebles_Tipo_Doc_X_COD_TIP_DOC_PRED('9');//DOC TASACION
		$data['DatTipoValorizacion'] 	= $this->Mostrar_Lista_Tipo_Valorizacion();
		$data['DatTipoMoneda'] 			= $this->Mostrar_Lista_Moneda();
		
		$this->render('principal.tpl.php', $data);
   }


   public function Mostrar_Form_tab1_Datos_Registrales(){
		
		$data = array();
		$objPrediosModel 	= new PrediosModel();
		
		$xID_PREDIO_SBN = isset($_GET['wID_PREDIO_SBN']) ? $_GET['wID_PREDIO_SBN'] : '';			
		
		$result_Predio_E = $objPrediosModel->Ver_Datos_Local_x_CODIGO($xID_PREDIO_SBN);
		
		$data['DatPredioLocal']['E_ID_PREDIO_SBN']	 	= utf8_encode(odbc_result($result_Predio_E,"ID_PREDIO_SBN"));
	
		$data['DatPredioLocal']['DR_CUS']					= utf8_encode(odbc_result($result_Predio_E,"CUS"));
		$data['DatPredioLocal']['DR_RSINABIP']				= utf8_encode(odbc_result($result_Predio_E,"RSINABIP"));
		$data['DatPredioLocal']['DR_NOM_PROP_REGISTRAL']	= utf8_encode(odbc_result($result_Predio_E,"NOM_PROP_REGISTRAL"));
		$data['DatPredioLocal']['DR_COD_OFIC_REGISTRAL']	= utf8_encode(odbc_result($result_Predio_E,"COD_OFIC_REGISTRAL"));
		$data['DatPredioLocal']['DR_AREA_REGISTRAL']		= number_format(odbc_result($result_Predio_E,"AREA_REGISTRAL"),2);
		$data['DatPredioLocal']['DR_COD_TIP_UNID_MED']		= utf8_encode(odbc_result($result_Predio_E,"COD_TIP_UNID_MED"));
		$data['DatPredioLocal']['DR_CODIGO_PREDIO']			= utf8_encode(odbc_result($result_Predio_E,"CODIGO_PREDIO"));
		$data['DatPredioLocal']['DR_PARTIDA_ELECTRONICA']	= utf8_encode(odbc_result($result_Predio_E,"PARTIDA_ELECTRONICA"));
		$data['DatPredioLocal']['DR_TOMO']					= utf8_encode(odbc_result($result_Predio_E,"TOMO"));
		$data['DatPredioLocal']['DR_ASIENTO']				= utf8_encode(odbc_result($result_Predio_E,"ASIENTO"));
		$data['DatPredioLocal']['DR_FOJAS']					= utf8_encode(odbc_result($result_Predio_E,"FOJAS"));
		$data['DatPredioLocal']['DR_FICHA']					= utf8_encode(odbc_result($result_Predio_E,"FICHA"));
		$data['DatPredioLocal']['DR_ANOTACION_PREVENTIVA']	= utf8_encode(odbc_result($result_Predio_E,"ANOTACION_PREVENTIVA"));

		
		$data['DatUsoPredioGral'] 		= $this->Listado_Uso_Predio_Gral();
		$data['DatOficRegistral'] 		= $this->Mostrar_Lista_Oficina_Registral();
		$data['DatUnidMedida'] 			= $this->Mostrar_Listar_Unidad_Medida();
		
		$this->render('v.predio_form_pestania_P1_cab.php', $data);
	}




	public function Buscar_Predios_x_Parametros(){
		
		$objPrediosModel 	= new PrediosModel();
		$objModel = new BienPatrimonialModel();
		
		$data = array();
    	
		$COD_ENTIDAD = isset($_GET['TXH_SIMI_COD_ENTIDAD']) ? $_GET['TXH_SIMI_COD_ENTIDAD'] : '';			
		$DENOMINACION_PREDIO = isset($_GET['txb_busc_nom_predios']) ? utf8_encode($_GET['txb_busc_nom_predios']) : '';			
		$pageNumber = isset($_GET['numeroPagina']) ? $_GET['numeroPagina'] : 1;
		
		$xNRO_FILA_LIST 	= self::NRO_FILA_LIST;
		$xNRO_LINK_PAGE 	= self::NRO_LINK_PAGE;
	

		/* CREACION:        WILLIAMS ARENAS */
	    /* FECHA CREACION:  25-05-2020 */
	    /* DESCRIPCION:     OBTENER TOTAL DE REGISTROS DE PREDIOS */
	    $data = array();    
	    $COD_ENTIDAD = isset($_GET['TXH_SIMI_COD_ENTIDAD']) ? $_GET['TXH_SIMI_COD_ENTIDAD'] : '';			
		$DENOMINACION_PREDIO = isset($_GET['txb_busc_nom_predios']) ? utf8_encode($_GET['txb_busc_nom_predios']
			) : '';	

	    
	    $dataModel_01 = [
	      'COD_ENTIDAD'=>$COD_ENTIDAD,
	      'DENOMINACION_PREDIO'=>$DENOMINACION_PREDIO,
	    ];
	    
	    $data['DataListaBienPatrimonial'] = $objModel->TOTAL_REGISTRO_PREDIOS_X_PARAMETROS2($dataModel_01);
	    $Total_Registros = $data['DataListaBienPatrimonial'][0]['TOT_REG'];
	    /* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */


			
			$Total_Paginas = ceil($Total_Registros / $xNRO_FILA_LIST);
			
			$data['pager']['indicePagina'] 		= $pageNumber;
			$data['pager']['cantidadRegistros'] = $Total_Registros;
			$data['pager']['cantidadPaginas'] 	= $Total_Paginas;	
			$data['pager']['limiteInferior'] 	= (($pageNumber - $xNRO_LINK_PAGE) <= 0) ?  1 : ($pageNumber - $xNRO_LINK_PAGE);
			$data['pager']['limiteSuperior'] 	= (($pageNumber + $xNRO_LINK_PAGE) > $Total_Paginas) ? $Total_Paginas : ($pageNumber + $xNRO_LINK_PAGE);
			$data['pager']['handlerFunction'] 	= "handle_paginar_personal";	

						
			/*********************************************************/
			
			$Item_Ini = ($pageNumber == '1')? (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST) : (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST)+1 ;
			$Item_Fin = ($pageNumber == '1')? ($Item_Ini + $xNRO_FILA_LIST) : ($Item_Ini + $xNRO_FILA_LIST)-1;


		/* CREACION:        WILLIAMS ARENAS */
	    /* FECHA CREACION:  25-05-2020 */
	    /* DESCRIPCION:     LISTADO DE PREDIOS */
	    //$data = array();    
	    $COD_ENTIDAD = isset($_GET['TXH_SIMI_COD_ENTIDAD']) ? $_GET['TXH_SIMI_COD_ENTIDAD'] : '';			
		$DENOMINACION_PREDIO = isset($_GET['txb_busc_nom_predios']) ? utf8_encode($_GET['txb_busc_nom_predios']
			) : '';
	    
	    $dataModel_01 = [
	      	'COD_ENTIDAD'=>$COD_ENTIDAD,
	      	'DENOMINACION_PREDIO'=>$DENOMINACION_PREDIO,
	      	'ITEM_INI'=>$Item_Ini,
	    	'ITEM_FIN'=>$Item_Fin
	    ];
	    
	    $data['DataListadoPredios'] = $objModel->LISTA_PREDIOS_X_PARAMETROS2($dataModel_01);
	    $ArrayListadoPredios = $data['DataListadoPredios'];
	    

	    if($ArrayListadoPredios) foreach ($ArrayListadoPredios as $ListadoPredios): 
	    	$local = array();

	    	$local['ROW_NUMBER_ID'] 			= utf8_encode($ListadoPredios['ROW_NUMBER_ID']);
			$local['DENOMINACION_PREDIO'] 		= utf8_encode($ListadoPredios['DENOMINACION_PREDIO']);
			$local['ENVIO_REG_A_SBN'] 			= utf8_encode($ListadoPredios['ENVIO_REG_A_SBN']);
			$local['MUEBLES_ENVIO_REG_A_SBN'] 	= utf8_encode($ListadoPredios['MUEBLES_ENVIO_REG_A_SBN']);
			$local['TIPO_VALIDACION'] 			= utf8_encode($ListadoPredios['TIPO_VALIDACION']);
			$local['DESC_TIP_PROPIEDAD'] 		= utf8_encode($ListadoPredios['DESC_TIP_PROPIEDAD']);
			$local['COD_TIP_PROPIEDAD'] 		= utf8_encode($ListadoPredios['COD_TIP_PROPIEDAD']);
			$local['DESC_EST_PROPIEDAD'] 		= utf8_encode($ListadoPredios['DESC_EST_PROPIEDAD']);
			$local['ID_ESTADO'] 				= utf8_encode($ListadoPredios['ID_ESTADO']);
			$local['COD_TIP_PROPIEDAD'] 		= utf8_encode($ListadoPredios['COD_TIP_PROPIEDAD']);
			$local['DAR_BAJA'] 					= utf8_encode($ListadoPredios['DAR_BAJA']);
			$local['ID_ESTADO_OCULTAR'] 		= utf8_encode($ListadoPredios['ID_ESTADO_OCULTAR']);
			$local['ID_PREDIO_SBN'] 			= utf8_encode($ListadoPredios['ID_PREDIO_SBN']);

			$DAR_BAJA 			= $local['DAR_BAJA'];
			$ID_ESTADO_OCULTAR 	= $local['ID_ESTADO_OCULTAR'];
			$TIPO_VALIDACION 	= $local['TIPO_VALIDACION'];
			$ID_PREDIO_SBN      = $local['ID_PREDIO_SBN'];			

			/* CREACION:        WILLIAMS ARENAS */
		    /* FECHA CREACION:  25-05-2020 */
		    /* DESCRIPCION:     OBTENER TOTAL DE REGISTROS DE PREDIOS */  
		    $COD_ENTIDAD = isset($_GET['TXH_SIMI_COD_ENTIDAD']) ? $_GET['TXH_SIMI_COD_ENTIDAD'] : '';	


		    $dataModel_01 = [
		      'COD_ENTIDAD'=>$COD_ENTIDAD,
		      'ID_PREDIO_SBN'=> $ID_PREDIO_SBN
		    ];
		    
		    $data['DataListaTotalBienes'] = $objModel->TOTAL_BIENES_MUEBLES_x_PREDIO2($dataModel_01);
		    

		    if( count($data["DataListaTotalBienes"]) != 0)
		    {
		    	$TOTAL_BIEN = $data['DataListaTotalBienes'][0]['TOT_BIEN']; 

		    }
		    /* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */


			if($DAR_BAJA == 'X'){			
				$local['DESC_BAJA'] = 'BAJA';
				//$local['stylo_estado'] = 'color:#333333; text-align:center;';
			}else{				
				$local['DESC_BAJA'] = 'ALTA';
				//$local['stylo_estado'] = 'color:rgb(0, 156, 213); text-align:center;';				
			}


			if($ID_ESTADO_OCULTAR == '2'){
				//$local['DESC_BAJA'] = 'BAJA';
				$local['stylo_estado'] = 'color:rgb(255, 0, 0); text-align:center;';
			}else{
				//$local['DESC_BAJA'] = 'ALTA';
				$local['stylo_estado'] = 'color:#333333; text-align:center;';
				
			}
			
		
		
			if($TIPO_VALIDACION == 'O'){//--o: observado -- V:validado	
					
				$local['Desc_TIPO_VALIDACION'] = 'OBSERVADO';
				$local['RESP_OBSERV'] = 'S';
				
			}elseif($TIPO_VALIDACION == 'E'){
				
				$local['Desc_TIPO_VALIDACION'] = 'ENVIADO';			
				$local['RESP_OBSERV'] = '';
				
			}elseif($TIPO_VALIDACION == 'P'){
				
				$local['Desc_TIPO_VALIDACION'] = 'CON PLANO';
				$local['RESP_OBSERV'] = '';
				
			}elseif($TIPO_VALIDACION == 'S'){
				
				$local['Desc_TIPO_VALIDACION'] = 'SUBSANADO';
				$local['RESP_OBSERV'] = '';
				
			}elseif($TIPO_VALIDACION == 'V'){
				
				$local['Desc_TIPO_VALIDACION'] = 'VALIDADO';
				$local['RESP_OBSERV'] = '';
				
			}elseif($TIPO_VALIDACION == '' || $TIPO_VALIDACION == 'N'){ 
				
				$local['Desc_TIPO_VALIDACION'] = 'NUEVO';
				$local['RESP_OBSERV'] = 'E';
				
			}else{
				
				$local['Desc_TIPO_VALIDACION'] = '---';
				$local['RESP_OBSERV'] = 'N';
			}

			
			$data['Predio_Locales'][] = $local;

	   	 endforeach; 
	   	 //die();
	    /* FIN DE LISTADO DE PREDIOS */

		$this->render('v.predio_listar.php', $data);
	}


   public function Mostrar_Lista_Oficina_Registral(){
	   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
	   $rsOficRegistral = $objPrediosModel->Lista_Oficina_registral();
		while (odbc_fetch_row($rsOficRegistral)) {
			$ArrayOficRegistral = array();			
			$ArrayOficRegistral['COD_OFIC_REGISTRAL'] 	= odbc_result($rsOficRegistral, 'COD_OFIC_REGISTRAL');
			$ArrayOficRegistral['NOM_OFICINA_REGISTRAL'] 	= utf8_encode(odbc_result($rsOficRegistral, 'NOM_OFICINA_REGISTRAL'));			
			$data['DatOficRegistral'][] = $ArrayOficRegistral;					
		}
				
		return $data['DatOficRegistral'];
   }


   public function concluir_proceso_GRAL(){
	   
	  	$data = array();		
		$objUbigeoModel 	= new PrediosModel();
	   
		$xCOD_ENTIDAD = isset($_GET['xCOD_ENTIDAD']) ? $_GET['xCOD_ENTIDAD'] : '';
		//echo $xCOD_ENTIDAD;
		$RESULTADO			=	$objUbigeoModel->concluir_proceso_GRAL($xCOD_ENTIDAD);

		$valor = ($RESULTADO) ? '1' : 0;
		echo $valor;
   }


   public function Mostrar_Lista_Departamento(){
	   
	  	$data = array();		
		$objUbigeoModel 	= new UbigeoModel();
	   
   		$rsDepartamento = $objUbigeoModel->Lista_Departamento();
		while (odbc_fetch_row($rsDepartamento)) {			
			
			$ArrayDepartamento = array();			
			$ArrayDepartamento['COD_DPTO'] 		= odbc_result($rsDepartamento, 'COD_DPTO');
			$ArrayDepartamento['DEPARTAMENTO'] 	= utf8_encode(odbc_result($rsDepartamento, 'DEPARTAMENTO'));
			
			$data['Departamento'][] = $ArrayDepartamento;						
		}
		return $data['Departamento'];
   
   }



   public function Buscar_Predios_x_Parametros_DU(){
		
		$objPrediosModel 	= new PrediosModel();
		
		$data = array();
    	
		$COD_ENTIDAD = isset($_GET['TXH_SIMI_COD_ENTIDAD']) ? $_GET['TXH_SIMI_COD_ENTIDAD'] : '';			
		$DENOMINACION_PREDIO = isset($_GET['txb_busc_nom_predios']) ? utf8_encode($_GET['txb_busc_nom_predios']) : '';			
		$BUSCAR_CUS = isset($_GET['txb_busc_cus']) ? utf8_encode($_GET['txb_busc_cus']) : '';			
		$pageNumber = isset($_GET['numeroPagina']) ? $_GET['numeroPagina'] : 1;
		
		$xNRO_FILA_LIST 	= self::NRO_FILA_LIST;
		$xNRO_LINK_PAGE 	= self::NRO_LINK_PAGE;
		
		/*********************************************************/

		
		$RS = $objPrediosModel->TOTAL_REGISTRO_PREDIOS_X_PARAMETROS_GRAL($COD_ENTIDAD, $DENOMINACION_PREDIO, $BUSCAR_CUS);
		$Total_Registros = odbc_result($RS,"TOT_REG");
		
		$Total_Paginas = ceil($Total_Registros / $xNRO_FILA_LIST);
		
		$data['pager']['indicePagina'] 		= $pageNumber;
		$data['pager']['cantidadRegistros'] = $Total_Registros;
		$data['pager']['cantidadPaginas'] 	= $Total_Paginas;	
		$data['pager']['limiteInferior'] 	= (($pageNumber - $xNRO_LINK_PAGE) <= 0) ?  1 : ($pageNumber - $xNRO_LINK_PAGE);
		$data['pager']['limiteSuperior'] 	= (($pageNumber + $xNRO_LINK_PAGE) > $Total_Paginas) ? $Total_Paginas : ($pageNumber + $xNRO_LINK_PAGE);
		$data['pager']['handlerFunction'] 	= "handle_paginar_personal";	

					
		/*********************************************************/
		
		$Item_Ini = ($pageNumber == '1')? (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST) : (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST)+1 ;
		$Item_Fin = ($pageNumber == '1')? ($Item_Ini + $xNRO_FILA_LIST) : ($Item_Ini + $xNRO_FILA_LIST)-1;


		$Result_Predios = $objPrediosModel->LISTA_PREDIOS_X_PARAMETROS_GRAL($Item_Ini, $Item_Fin, $COD_ENTIDAD, $DENOMINACION_PREDIO, $BUSCAR_CUS);
		while (odbc_fetch_row($Result_Predios)){
						
			$local = array();

			$local['ROW_NUMBER_ID'] 			= utf8_encode(odbc_result($Result_Predios,"ROW_NUMBER_ID"));
			$local['ID_PREDIO_SBN'] 			= utf8_encode(odbc_result($Result_Predios,"ID_PREDIO_SBN"));
			$local['DENOMINACION_PREDIO'] 		= utf8_encode(odbc_result($Result_Predios,"DENOMINACION_PREDIO"));
			$local['ENVIO_REG_A_SBN'] 			= utf8_encode(odbc_result($Result_Predios,"ENVIO_REG_A_SBN"));
			$local['MUEBLES_ENVIO_REG_A_SBN'] 	= utf8_encode(odbc_result($Result_Predios,"MUEBLES_ENVIO_REG_A_SBN"));
			$TIPO_VALIDACION 					= utf8_encode(odbc_result($Result_Predios,"TIPO_VALIDACION"));
			$local['DESC_TIP_PROPIEDAD'] 		= utf8_encode(odbc_result($Result_Predios,"DESC_TIP_PROPIEDAD"));
			$DAR_BAJA 							= utf8_encode(odbc_result($Result_Predios,"DAR_BAJA"));
			$CUS 							 	= utf8_encode(odbc_result($Result_Predios,"CUS"));
			$PROP_HORIZ 						= utf8_encode(odbc_result($Result_Predios,"PROP_HORIZ"));
			
			$RESULT_TOT 	= $objPrediosModel->TOTAL_BIENES_MUEBLES_x_PREDIO($COD_ENTIDAD, utf8_encode(odbc_result($Result_Predios,"ID_PREDIO_SBN")));
			$TOTAL_BIEN 	= odbc_result($RESULT_TOT,"TOT_BIEN");	
			
			$local['TIPO_VALIDACION'] 	= $TIPO_VALIDACION;
			$local['DAR_BAJA'] 			= $DAR_BAJA;
			$local['CUS'] 				= $CUS;
			$local['PROP_HORIZ'] 		= $PROP_HORIZ;

			if($DAR_BAJA == 'X'){			
				$local['DESC_BAJA'] = 'BAJA';
				$local['stylo_estado'] = 'color:#333333; text-align:center;';
			}else{				
				$local['DESC_BAJA'] = 'ALTA';
				$local['stylo_estado'] = 'color:rgb(0, 156, 213); text-align:center;';				
			}
		
		
			if($TIPO_VALIDACION == 'O'){//--o: observado -- V:validado	
					
				$local['Desc_TIPO_VALIDACION'] = 'OBSERVADO';
				$local['RESP_OBSERV'] = 'S';
				
			}elseif($TIPO_VALIDACION == 'E'){
				
				$local['Desc_TIPO_VALIDACION'] = 'ENVIADO';			
				$local['RESP_OBSERV'] = '';
				
			}elseif($TIPO_VALIDACION == 'P'){
				
				$local['Desc_TIPO_VALIDACION'] = 'CON PLANO';
				$local['RESP_OBSERV'] = '';
				
			}elseif($TIPO_VALIDACION == 'S'){
				
				$local['Desc_TIPO_VALIDACION'] = 'SUBSANADO';
				$local['RESP_OBSERV'] = '';
				
			}elseif($TIPO_VALIDACION == 'V'){
				
				$local['Desc_TIPO_VALIDACION'] = 'VALIDADO';
				$local['RESP_OBSERV'] = '';
				
			}elseif($TIPO_VALIDACION == '' || $TIPO_VALIDACION == 'N'){
				
				$local['Desc_TIPO_VALIDACION'] = 'NUEVO';
				$local['RESP_OBSERV'] = 'E';
				
			}else{
				
				$local['Desc_TIPO_VALIDACION'] = '---';
				$local['RESP_OBSERV'] = 'N';
			}

			
			$data['Predio_Locales'][] = $local;
			
		}
		
		
		$this->render('v.predio_listar.php', $data);
   	}


   	public function Mostrar_Lista_Provincia($xcod_depa){
	   	
		$data = array();		
		$objUbigeoModel 	= new UbigeoModel();
		
   		$rsProvincia = $objUbigeoModel->Lista_Provincia($xcod_depa);
		while (odbc_fetch_row($rsProvincia)) {			
			
			$ArrayProvincia = array();			
			$ArrayProvincia['COD_PROV'] 	= odbc_result($rsProvincia, 'COD_PROV');
			$ArrayProvincia['PROVINCIA'] 	= utf8_encode(odbc_result($rsProvincia, 'PROVINCIA'));
			
			$data['Provincia'][] = $ArrayProvincia;						
		}
		return $data['Provincia'];
   }


   public function Mostrar_Lista_Distrito($xcod_depa, $xcod_prov){
	   
	   	$data = array();		
		$objUbigeoModel 	= new UbigeoModel();
		
   		$rsDistrito = $objUbigeoModel->Lista_Distrito($xcod_depa, $xcod_prov);
		while (odbc_fetch_row($rsDistrito)) {			
			
			$ArrayDistrito = array();			
			$ArrayDistrito['COD_DIST'] 	= odbc_result($rsDistrito, 'COD_DIST');
			$ArrayDistrito['DISTRITO'] 	= utf8_encode(odbc_result($rsDistrito, 'DISTRITO'));
			
			$data['Distrito'][] = $ArrayDistrito;
		}
		return $data['Distrito'];
   }


   public function Mostrar_Lista_Tipo_Zona_Inmueble(){
	   
	   	$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
   		$rsTipoZona = $objPrediosModel->Lista_Tipo_Zona_Inmueble();
		while (odbc_fetch_row($rsTipoZona)) {			
			
			$ArrayTipoZona = array();			
			$ArrayTipoZona['COD_ZONA'] 		= odbc_result($rsTipoZona, 'COD_ZONA');
			$ArrayTipoZona['NOM_ZONA'] 		= utf8_encode(odbc_result($rsTipoZona, 'NOM_ZONA'));
			
			$data['TipoZona'][] = $ArrayTipoZona;
						
		}
		return $data['TipoZona'];
   }


    public function Mostrar_Lista_Tipo_Via(){
	   
	   	$data = array();		
		$objPrediosModel 	= new PrediosModel();
   		
		$rsTipoVia = $objPrediosModel->Lista_Tipo_Via();
		while (odbc_fetch_row($rsTipoVia)) {			
			
			$ArrayTipoVia = array();			
			$ArrayTipoVia['COD_TVIA'] 		= odbc_result($rsTipoVia, 'COD_TVIA');
			$ArrayTipoVia['DESC_VIA'] 		= utf8_encode(odbc_result($rsTipoVia, 'DESC_VIA'));
			
			$data['TipoVia'][] = $ArrayTipoVia;
						
		}
		return $data['TipoVia'];
   }


   public function Mostrar_Lista_Tipo_Habilitacion(){
			   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$rsTipHabilitacion = $objPrediosModel->Lista_Tipo_Habilitacion();
		while (odbc_fetch_row($rsTipHabilitacion)) {			
			
			$ArrayTipHabilitacion = array();			
			$ArrayTipHabilitacion['COD_THABILITACION'] 		= odbc_result($rsTipHabilitacion, 'COD_THABILITACION');
			$ArrayTipHabilitacion['TXT_THABILITACION'] 		= utf8_encode(odbc_result($rsTipHabilitacion, 'TXT_THABILITACION'));
			
			$data['TipoHabilitacion'][] = $ArrayTipHabilitacion;
						
		}
			
		return $data['TipoHabilitacion'];
	}



	public function Mostrar_Listar_Tipo_Propiedad(){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$rsTipPropiedad = $objPrediosModel->Listar_Tipo_Propiedad();
		while (odbc_fetch_row($rsTipPropiedad)) {			
			
			$ArrayTipPropiedad = array();			
			$ArrayTipPropiedad['COD_TIP_PROPIEDAD'] 	= odbc_result($rsTipPropiedad, 'COD_TIP_PROPIEDAD');
			$ArrayTipPropiedad['DESC_TIP_PROPIEDAD'] 	= utf8_encode(odbc_result($rsTipPropiedad, 'DESC_TIP_PROPIEDAD'));
			
			$data['TipoPropiedad'][] = $ArrayTipPropiedad;
						
		}
			
		return $data['TipoPropiedad'];
	}


	public function Mostrar_Listar_Tipo_Propiedad_GRAL(){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$rsTipPropiedad = $objPrediosModel->Listar_Tipo_Propiedad_GRAL();
		while (odbc_fetch_row($rsTipPropiedad)) {			
			
			$ArrayTipPropiedad = array();			
			$ArrayTipPropiedad['COD_TIP_PROPIEDAD'] 	= odbc_result($rsTipPropiedad, 'COD_TIP_PROPIEDAD');
			$ArrayTipPropiedad['DESC_TIP_PROPIEDAD'] 	= utf8_encode(odbc_result($rsTipPropiedad, 'DESC_TIP_PROPIEDAD'));
			
			$data['TipoPropiedad'][] = $ArrayTipPropiedad;
						
		}
			
		return $data['TipoPropiedad'];
	}




	public function Mostrar_Listar_Unidad_Medida(){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();

		$rsListUnidMedida = $objPrediosModel->Listar_Unidad_Medida();
		while (odbc_fetch_row($rsListUnidMedida)) {			
			
			$ArrayUnidMedida = array();			
			$ArrayUnidMedida['COD_TIP_UNID_MED'] 		= odbc_result($rsListUnidMedida, 'COD_TIP_UNID_MED');
			$ArrayUnidMedida['ABREV_TIP_UNID_MED'] 	= utf8_encode(odbc_result($rsListUnidMedida, 'ABREV_TIP_UNID_MED'));
			
			$data['DatUnidMedida'][] = $ArrayUnidMedida;
						
		}
			
		return $data['DatUnidMedida'];
	}
	
	
	public function Mostrar_Listar_Tipo_Terreno(){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();

		$rsListTipoTerreno = $objPrediosModel->Listar_Tipo_Terreno();
		while (odbc_fetch_row($rsListTipoTerreno)) {			
			
			$ArrayTipTerreno = array();			
			$ArrayTipTerreno['COD_TIP_TERRENO'] 		= odbc_result($rsListTipoTerreno, 'COD_TIP_TERRENO');
			$ArrayTipTerreno['NOM_TIP_TERRENO'] 	= utf8_encode(odbc_result($rsListTipoTerreno, 'NOM_TIP_TERRENO'));
			
			$data['DatTipTerreno'][] = $ArrayTipTerreno;
						
		}

		return $data['DatTipTerreno'];
	}
	

	public function Mostrar_Lista_Estado_Conservacion(){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();

		$rsListEstConserv = $objPrediosModel->Lista_Estado_Conservacion();
		while (odbc_fetch_row($rsListEstConserv)) {			
			
			$ArrayEstConserv = array();			
			$ArrayEstConserv['COD_ECONSERVACION'] 		= odbc_result($rsListEstConserv, 'COD_ECONSERVACION');
			$ArrayEstConserv['TXT_CONSERVACION'] 	= utf8_encode(odbc_result($rsListEstConserv, 'TXT_CONSERVACION'));
			
			$data['DatEstConservacion'][] = $ArrayEstConserv;
						
		}

		return $data['DatEstConservacion'];
	}



	public function Mostrar_Lista_Estado_Saneamiento(){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();

		$rsListEstSaneamiento = $objPrediosModel->Lista_Estado_Saneamiento();
		while (odbc_fetch_row($rsListEstSaneamiento)) {			
			
			$ArrayEstSaneamiento = array();			
			$ArrayEstSaneamiento['COD_ESANEAMIENTO'] 		= odbc_result($rsListEstSaneamiento, 'COD_ESANEAMIENTO');
			$ArrayEstSaneamiento['TXT_ESANEAMIENTO'] 	= utf8_encode(odbc_result($rsListEstSaneamiento, 'TXT_ESANEAMIENTO'));
			
			$data['DatEstSaneamiento'][] = $ArrayEstSaneamiento;
						
		}
		
		return $data['DatEstSaneamiento'];
	}
		

	public function Mostrar_Lista_Estado_Saneamiento_Detalle($sCOD_ESANEAMIENTO){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();

		$rsListDetSanea = $objPrediosModel->Lista_Estado_Saneamiento_Detalle($sCOD_ESANEAMIENTO);
		while (odbc_fetch_row($rsListDetSanea)) {			
			
			$ArrayDetSanemaiento = array();			
			$ArrayDetSanemaiento['codigo_detalle'] 	= odbc_result($rsListDetSanea, 'codigo_detalle');
			$ArrayDetSanemaiento['dsc_detalle'] 	= utf8_encode(odbc_result($rsListDetSanea, 'dsc_detalle'));
			
			$data['DatDetalleSaneamiento'][] = $ArrayDetSanemaiento;
						
		}
			
		return $data['DatDetalleSaneamiento'];
	}
	

	public function Mostrar_Lista_Uso_Predio(){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();

		$rsListUsoPredio = $objPrediosModel->Lista_Uso_Predio();
		while (odbc_fetch_row($rsListUsoPredio)) {			
			
			$ArrayUsoPredio = array();			
			$ArrayUsoPredio['CODIGO_USO_GENERICO'] 	= odbc_result($rsListUsoPredio, 'CODIGO_USO_GENERICO');
			$ArrayUsoPredio['NOMBRE_USO_GENERICO'] 	= utf8_encode(odbc_result($rsListUsoPredio, 'NOMBRE_USO_GENERICO'));
			
			$data['DatUsoPredio'][] = $ArrayUsoPredio;
						
		}
		
		return $data['DatUsoPredio'];
	}

	public function Listado_Uso_Predio_Gral(){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();

		$rsListUsoPredio = $objPrediosModel->Listado_Uso_Predio_Gral();
		while (odbc_fetch_row($rsListUsoPredio)) {			
			
			$ArrayUsoPredio = array();			
			$ArrayUsoPredio['ID_USO_PREDIO'] 	= odbc_result($rsListUsoPredio, 'ID_USO_PREDIO');
			$ArrayUsoPredio['DESCRIPCION_USO'] 	= utf8_encode(odbc_result($rsListUsoPredio, 'DESCRIPCION_USO'));
			
			$data['DatUsoPredio'][] = $ArrayUsoPredio;
						
		}
		
		return $data['DatUsoPredio'];
	}


	public function Mostrar_Lista_Inmuebles_Tipo_Doc_X_COD_TIP_DOC_PRED($COD_TIP_DOC_PRED){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$rsListTipoDocTasac = $objPrediosModel->Lista_Inmuebles_Tipo_Doc_X_COD_TIP_DOC_PRED($COD_TIP_DOC_PRED);
		while (odbc_fetch_row($rsListTipoDocTasac)) {			
			
			$ArrayTipDocTasa = array();			
			$ArrayTipDocTasa['T1_COD_TIP_DOC_PRED'] 	= odbc_result($rsListTipoDocTasac, 'COD_TIP_DOC_PRED');
			$ArrayTipDocTasa['T1_NOM_TIP_DOC'] 	= utf8_encode(odbc_result($rsListTipoDocTasac, 'NOM_TIP_DOC'));
			
			$data['DatTipoDocTasa'][] = $ArrayTipDocTasa;
						
		}
			
		return $data['DatTipoDocTasa'];
	}


	public function Mostrar_Lista_Tipo_Valorizacion(){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();

		$rsListTipoValoriza = $objPrediosModel->Lista_Tipo_Valorizacion();
		while (odbc_fetch_row($rsListTipoValoriza)) {			
			
			$ArrayTipValoriza = array();			
			$ArrayTipValoriza['COD_TIP_VALORIZACION'] 	= odbc_result($rsListTipoValoriza, 'COD_TIP_VALORIZACION');
			$ArrayTipValoriza['NOM_TIP_VALORIZACION'] 		= utf8_encode(odbc_result($rsListTipoValoriza, 'NOM_TIP_VALORIZACION'));
			
			$data['DatTipoValorizacion'][] = $ArrayTipValoriza;
						
		}

		
		return $data['DatTipoValorizacion'];
	}


	public function Mostrar_Lista_Moneda(){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();

		$rsListTipoMoneda = $objPrediosModel->Lista_Moneda();
		while (odbc_fetch_row($rsListTipoMoneda)) {			
			
			$ArrayTipoMoneda = array();			
			$ArrayTipoMoneda['COD_MONEDA'] 	= odbc_result($rsListTipoMoneda, 'COD_MONEDA');
			$ArrayTipoMoneda['TXT_MONEDA'] 	= utf8_encode(odbc_result($rsListTipoMoneda, 'TXT_MONEDA'));
			
			$data['DatTipoMoneda'][] = $ArrayTipoMoneda;
						
		}
		
		return $data['DatTipoMoneda'];
	}
	
	
	public function Mostrar_Lista_Tipo_Dispositivo_Legal(){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();

		$rsListTipoMoneda = $objPrediosModel->Lista_Dispositivo_Resolucion();
		while (odbc_fetch_row($rsListTipoMoneda)) {			
			
			$ArrayTipoMoneda = array();			
			$ArrayTipoMoneda['COD_TIP_RESOL_DL'] 	= odbc_result($rsListTipoMoneda, 'COD_TIP_RESOL_DL');
			$ArrayTipoMoneda['DES_TIP_RESOL_DL'] 	= utf8_encode(odbc_result($rsListTipoMoneda, 'DES_TIP_RESOL_DL'));
			
			$data['DatTipoDispLegal'][] = $ArrayTipoMoneda;
						
		}
		
		return $data['DatTipoDispLegal'];
	}
	
	
	public function Mostrar_Lista_de_Documentacion_Registrados_x_Tipo($NombreArreglo, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED){
		   
		$data = array();		
		$objPrediosModel 	= new PrediosModel();

		$rsListTipoDocumento = $objPrediosModel->LISTAR_DOCUMENTOS_TECNICOS_X_ENTIDADES_X_COD_TIP_DOC_PRED($ID_PREDIO_SBN, $COD_TIP_DOC_PRED);
		while (odbc_fetch_row($rsListTipoDocumento)) {			
			
			$ArrayTipDocumento = array();	
			
			$ArrayTipDocumento['DOC_ITEM'] 					= utf8_encode(odbc_result($rsListTipoDocumento, 'ITEM'));
			$ArrayTipDocumento['DOC_COD_PREDIO_DOC_TECN'] 	= utf8_encode(odbc_result($rsListTipoDocumento, 'COD_PREDIO_DOC_TECN'));
			$ArrayTipDocumento['DOC_ID_PREDIO_SBN'] 		= utf8_encode(odbc_result($rsListTipoDocumento, 'ID_PREDIO_SBN'));
			$ArrayTipDocumento['DOC_COD_TIP_DOC_PRED'] 		= utf8_encode(odbc_result($rsListTipoDocumento, 'COD_TIP_DOC_PRED'));
			$ArrayTipDocumento['DOC_DESCRIP_ARCHIVO'] 		= utf8_encode(odbc_result($rsListTipoDocumento, 'DESCRIP_ARCHIVO'));
			$ArrayTipDocumento['DOC_NOM_ARCHIVO'] 			= utf8_encode(odbc_result($rsListTipoDocumento, 'NOM_ARCHIVO'));
			$ArrayTipDocumento['DOC_PESO_ARCHIVO'] 			= utf8_encode(odbc_result($rsListTipoDocumento, 'PESO_ARCHIVO'));
			$ArrayTipDocumento['DOC_NOM_ARCHIVO_CAD'] 		= utf8_encode(odbc_result($rsListTipoDocumento, 'NOM_ARCHIVO_CAD'));
			$ArrayTipDocumento['DOC_PESO_ARCHIVO_CAD'] 		= utf8_encode(odbc_result($rsListTipoDocumento, 'PESO_ARCHIVO_CAD'));
			$ArrayTipDocumento['DOC_FECHA_REGISTRO'] 			= cambiaf_a_normal(odbc_result($rsListTipoDocumento, 'FECHA_REGISTRO'));
			$ArrayTipDocumento['DOC_NRO_PART_REG'] 			= utf8_encode(odbc_result($rsListTipoDocumento, 'NRO_PART_REG'));
			$ArrayTipDocumento['DOC_FECHA_TASACION'] 		= utf8_encode(odbc_result($rsListTipoDocumento, 'FECHA_TASACION'));
			$ArrayTipDocumento['DOC_COD_TIP_RESOL_DL'] 		= utf8_encode(odbc_result($rsListTipoDocumento, 'COD_TIP_RESOL_DL'));
			$ArrayTipDocumento['DOC_COD_SISTEMA_COORDENADA'] 	= utf8_encode(odbc_result($rsListTipoDocumento, 'COD_SISTEMA_COORDENADA'));
			$ArrayTipDocumento['DOC_NOM_TIP_DOC'] 			= utf8_encode(odbc_result($rsListTipoDocumento, 'NOM_TIP_DOC'));
			$ArrayTipDocumento['DOC_DES_TIP_RESOL_DL'] 		= utf8_encode(odbc_result($rsListTipoDocumento, 'DES_TIP_RESOL_DL'));			
			$DOC_IDESTADO 									= utf8_encode(odbc_result($rsListTipoDocumento, 'ID_ESTADO'));
			$ArrayTipDocumento['DOC_DESC_ESTADO'] 			= ($DOC_IDESTADO == '1')? 'ACTIVO':'PENDIENTE';
			$ArrayTipDocumento['DOC_NOM_CARPETA'] 			= $this->Nombre_Carpeta(utf8_encode(odbc_result($rsListTipoDocumento, 'COD_TIP_DOC_PRED')));

			
			$data["'".$NombreArreglo."'"][] = $ArrayTipDocumento;
						
		}
		
		return $data["'".$NombreArreglo."'"];
	}
   
   
	public function Mostrar_Datos_Predio_Local(){
		
		$data = array();
		
		$objPrediosModel 	= new PrediosModel();
		$objUbigeoModel 		= new UbigeoModel();
		$objModel = new BienPatrimonialModel();

		$xID_PREDIO_SBN = isset($_GET['xID_PREDIO_SBN']) ? $_GET['xID_PREDIO_SBN'] : '';
		
		/*********************************************************/
		/*
		$result_Predio_E = $objPrediosModel->Ver_Datos_Local_x_CODIGO($xID_PREDIO_SBN);
		*/

		/* CREACION:        WILLIAMS ARENAS */
	    /* FECHA CREACION:  06-06-2020 */
	    /* DESCRIPCION:     OBTENER DATOS DE PREDIO */
	    $data = array();    
	    $xID_PREDIO_SBN = isset($_GET['xID_PREDIO_SBN']) ? $_GET['xID_PREDIO_SBN'] : '';			

	    $dataModel_01 = [
	      'xID_PREDIO_SBN'=>$xID_PREDIO_SBN
	    ];
	    
	    $data['DataLocal'] = $objModel->Ver_Datos_Local_x_CODIGO_PDO($dataModel_01);
	    /* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */


		$data['DatPredioLocal']['E_ID_PREDIO_SBN']	 	= utf8_encode($data['DataLocal'][0]['ID_PREDIO_SBN']);
		$data['DatPredioLocal']['COD_UNID_EJEC']	 	= utf8_encode($data['DataLocal'][0]['COD_UNID_EJEC']);
		$data['DatPredioLocal']['DENOMINACION_PREDIO']	= utf8_encode($data['DataLocal'][0]['DENOMINACION_PREDIO']);
		$data['DatPredioLocal']['E_COD_TIP_PROPIEDAD']	= utf8_encode($data['DataLocal'][0]['COD_TIP_PROPIEDAD']);
		$data['DatPredioLocal']['E_COD_DEPA']	 		= utf8_encode($data['DataLocal'][0]['COD_DEPA']);
		$data['DatPredioLocal']['E_COD_PROV']	 		= utf8_encode($data['DataLocal'][0]['COD_PROV']);
		$data['DatPredioLocal']['E_COD_DIST']			= utf8_encode($data['DataLocal'][0]['COD_DIST']);
		$data['DatPredioLocal']['E_COD_ZONA']	 		= utf8_encode($data['DataLocal'][0]['COD_ZONA']);
		$data['DatPredioLocal']['E_COD_TVIA']	 		= utf8_encode($data['DataLocal'][0]['COD_TVIA']);
		
		$data['DatPredioLocal']['NOMBRE_VIA']	 		= utf8_encode($data['DataLocal'][0]['NOMBRE_VIA']);
		$data['DatPredioLocal']['NRO_PRED']	 			= utf8_encode($data['DataLocal'][0]['NRO_PRED']);
		$data['DatPredioLocal']['MZA_PRED']	 			= utf8_encode($data['DataLocal'][0]['MZA_PRED']);
		$data['DatPredioLocal']['LTE_PRED']				= utf8_encode($data['DataLocal'][0]['LTE_PRED']);
		$data['DatPredioLocal']['E_UBIC_DET_PRED']		= utf8_encode($data['DataLocal'][0]['DET_PRED']);
		$data['DatPredioLocal']['E_UBIC_DET_PRED_NRO']	= utf8_encode($data['DataLocal'][0]['DET_PRED_NRO']);
		$data['DatPredioLocal']['E_COD_THABILITACION']	= utf8_encode($data['DataLocal'][0]['COD_THABILITACION']);
		$data['DatPredioLocal']['NOM_THABILITACION']	= utf8_encode($data['DataLocal'][0]['NOM_THABILITACION']);
		$data['DatPredioLocal']['NOM_SECTOR']	 		= utf8_encode($data['DataLocal'][0]['NOM_SECTOR']);
		$data['DatPredioLocal']['UBICADO_PISO']	 		= utf8_encode($data['DataLocal'][0]['UBICADO_PISO']);
		$data['DatPredioLocal']['DAR_BAJA']				= utf8_encode($data['DataLocal'][0]['DAR_BAJA']);
		$data['DatPredioLocal']['ENVIO_REG_A_SBN']		= utf8_encode($data['DataLocal'][0]['ENVIO_REG_A_SBN']);
		$data['DatPredioLocal']['TIPO_VALIDACION']		= utf8_encode($data['DataLocal'][0]['TIPO_VALIDACION']);
		$data['DatPredioLocal']['E_ENVIO_REG_A_SBN']		= utf8_encode($data['DataLocal'][0]['ENVIO_REG_A_SBN']);
	
		$data['DatPredioLocal']['DR_CUS']					= utf8_encode($data['DataLocal'][0]['CUS']);
		$data['DatPredioLocal']['DR_RSINABIP']				= utf8_encode($data['DataLocal'][0]['RSINABIP']);
		$data['DatPredioLocal']['DR_NOM_PROP_REGISTRAL']	= utf8_encode($data['DataLocal'][0]['NOM_PROP_REGISTRAL']);
		$data['DatPredioLocal']['DR_COD_OFIC_REGISTRAL']	= utf8_encode($data['DataLocal'][0]['COD_OFIC_REGISTRAL']);
		$data['DatPredioLocal']['DR_AREA_REGISTRAL']		= number_format($data['DataLocal'][0]['AREA_REGISTRAL'],2);
		$data['DatPredioLocal']['DR_COD_TIP_UNID_MED']		= utf8_encode($data['DataLocal'][0]['COD_TIP_UNID_MED']);
		$data['DatPredioLocal']['DR_CODIGO_PREDIO']			= utf8_encode($data['DataLocal'][0]['CODIGO_PREDIO']);
		$data['DatPredioLocal']['DR_PARTIDA_ELECTRONICA']	= utf8_encode($data['DataLocal'][0]['PARTIDA_ELECTRONICA']);
		$data['DatPredioLocal']['DR_TOMO']					= utf8_encode($data['DataLocal'][0]['TOMO']);
		$data['DatPredioLocal']['DR_ASIENTO']				= utf8_encode($data['DataLocal'][0]['ASIENTO']);
		$data['DatPredioLocal']['DR_FOJAS']					= utf8_encode($data['DataLocal'][0]['FOJAS']);
		$data['DatPredioLocal']['DR_FICHA']					= utf8_encode($data['DataLocal'][0]['FICHA']);
		$data['DatPredioLocal']['DR_ANOTACION_PREVENTIVA']	= utf8_encode($data['DataLocal'][0]['ANOTACION_PREVENTIVA']);
		$data['DatPredioLocal']['DT_ZONIFICACION']			= utf8_encode($data['DataLocal'][0]['ZONIFICACION']);
		$data['DatPredioLocal']['DT_COD_TIP_TERRENO']		= utf8_encode($data['DataLocal'][0]['COD_TIP_TERRENO']);
		$data['DatPredioLocal']['DT_PERIMETRO']				= number_format($data['DataLocal'][0]['PERIMETRO'],2);
		$data['DatPredioLocal']['DT_AREA_TERRENO']			= number_format($data['DataLocal'][0]['AREA_TERRENO'],2);
		$data['DatPredioLocal']['DT_AREA_CONSTRUIDA']		= number_format($data['DataLocal'][0]['AREA_CONSTRUIDA'],2);
		$data['DatPredioLocal']['DT_COD_ECONSERVACION']		= utf8_encode($data['DataLocal'][0]['COD_ECONSERVACION']);
		$data['DatPredioLocal']['DT_NUMERO_PISOS']			= utf8_encode($data['DataLocal'][0]['NUMERO_PISOS']);
		$data['DatPredioLocal']['DT_COD_ESANEAMIENTO']		= utf8_encode($data['DataLocal'][0]['COD_ESANEAMIENTO']);		
		$data['DatPredioLocal']['DT_CODIGO_DETALLE_ESANEAMIENTO']	= utf8_encode($data['DataLocal'][0]['CODIGO_DETALLE_ESANEAMIENTO']);
		$data['DatPredioLocal']['DT_CODIGO_USO_GENERICO']			= utf8_encode($data['DataLocal'][0]['CODIGO_USO_GENERICO']);
		$data['DatPredioLocal']['DT_OTROS_USO_PREDIO']				= utf8_encode($data['DataLocal'][0]['OTROS_USO_PREDIO']);


		$data['Departamento'] 			= $this->Mostrar_Lista_Departamento();
		$data['Provincia'] 				= $this->Mostrar_Lista_Provincia(utf8_encode($data['DataLocal'][0]['COD_DEPA']));
		$data['Distrito'] 				= $this->Mostrar_Lista_Distrito(utf8_encode($data['DataLocal'][0]['COD_DEPA']), utf8_encode($data['DataLocal'][0]['COD_PROV']));
		
		$data['TipoZona'] 				= $this->Mostrar_Lista_Tipo_Zona_Inmueble();
		$data['TipoVia'] 				= $this->Mostrar_Lista_Tipo_Via();
		$data['TipoHabilitacion'] 		= $this->Mostrar_Lista_Tipo_Habilitacion();
		$data['TipoPropiedad'] 			= $this->Mostrar_Listar_Tipo_Propiedad();
		
		$data['DatOficRegistral'] 		= $this->Mostrar_Lista_Oficina_Registral();
		//$data['DatUnidMedida'] 			= $this->Mostrar_Listar_Unidad_Medida();
		
		$data['DatTipTerreno'] 			= $this->Mostrar_Listar_Tipo_Terreno();
		//$data['DatEstConservacion'] 	= $this->Mostrar_Lista_Estado_Conservacion();
		//$data['DatEstSaneamiento'] 		= $this->Mostrar_Lista_Estado_Saneamiento();
		//$data['DatDetalleSaneamiento'] 	= $this->Mostrar_Lista_Estado_Saneamiento_Detalle(utf8_encode($data['DataLocal'][0]['COD_ESANEAMIENTO']));
		//$data['DatUsoPredio'] 			= $this->Mostrar_Lista_Uso_Predio();
		
		//$data['DatTipoDocTasa'] 		= $this->Mostrar_Lista_Inmuebles_Tipo_Doc_X_COD_TIP_DOC_PRED('9');//DOC TASACION
		//$data['DatTipoValorizacion'] 	= $this->Mostrar_Lista_Tipo_Valorizacion();
		//$data['DatTipoMoneda'] 			= $this->Mostrar_Lista_Moneda();
		//$data['DatTipoMoneda'] 			= $this->Mostrar_Lista_Moneda();
		

		
		/********************************************************************/
		/********************************************************************/
		/********************************************************************/
				
		
		$this->render('v.predio_form.php', $data);
	}


	public function Mostrar_Datos_Predio_Local_GRAL(){
		
		$data = array();
		
		$objPrediosModel 	= new PrediosModel();
		$objUbigeoModel 		= new UbigeoModel();
		
		$xID_PREDIO_SBN = isset($_GET['xID_PREDIO_SBN']) ? $_GET['xID_PREDIO_SBN'] : '';
		$COD_ENTIDAD = isset($_GET['TXH_SIMI_COD_ENTIDAD']) ? $_GET['TXH_SIMI_COD_ENTIDAD'] : '';

		echo $COD_ENTIDAD;


		
		/*********************************************************/
		
		
		
		$result_Predio_E = $objPrediosModel->Ver_Datos_Local_x_CODIGO_GRAL($xID_PREDIO_SBN);	
		
			

	
		
		$data['DatPredioLocal']['E_ID_PREDIO_SBN']	 	= utf8_encode(odbc_result($result_Predio_E,"ID_PREDIO_SBN"));
		$data['DatPredioLocal']['COD_UNID_EJEC']	 	= utf8_encode(odbc_result($result_Predio_E,"COD_UNID_EJEC"));
		$data['DatPredioLocal']['DENOMINACION_PREDIO']	= utf8_encode(odbc_result($result_Predio_E,"DENOMINACION_PREDIO"));
		$data['DatPredioLocal']['E_COD_TIP_PROPIEDAD']	= utf8_encode(odbc_result($result_Predio_E,"COD_TIP_PROPIEDAD"));
		$data['DatPredioLocal']['E_COD_DEPA']	 			= utf8_encode(odbc_result($result_Predio_E,"COD_DEPA"));
		$data['DatPredioLocal']['E_COD_PROV']	 			= utf8_encode(odbc_result($result_Predio_E,"COD_PROV"));
		$data['DatPredioLocal']['E_COD_DIST']				= utf8_encode(odbc_result($result_Predio_E,"COD_DIST"));
		$data['DatPredioLocal']['E_COD_ZONA']	 		= utf8_encode(odbc_result($result_Predio_E,"COD_ZONA"));
		$data['DatPredioLocal']['E_COD_TVIA']	 		= utf8_encode(odbc_result($result_Predio_E,"COD_TVIA"));
		
		$data['DatPredioLocal']['NOMBRE_VIA']	 		= utf8_encode(odbc_result($result_Predio_E,"NOMBRE_VIA"));
		$data['DatPredioLocal']['NRO_PRED']	 			= utf8_encode(odbc_result($result_Predio_E,"NRO_PRED"));
		$data['DatPredioLocal']['MZA_PRED']	 			= utf8_encode(odbc_result($result_Predio_E,"MZA_PRED"));
		$data['DatPredioLocal']['LTE_PRED']				= utf8_encode(odbc_result($result_Predio_E,"LTE_PRED"));
		$data['DatPredioLocal']['E_UBIC_DET_PRED']		= utf8_encode(odbc_result($result_Predio_E,"DET_PRED"));
		$data['DatPredioLocal']['E_UBIC_DET_PRED_NRO']	= utf8_encode(odbc_result($result_Predio_E,"DET_PRED_NRO"));
		$data['DatPredioLocal']['E_COD_THABILITACION']	= utf8_encode(odbc_result($result_Predio_E,"COD_THABILITACION"));
		$data['DatPredioLocal']['NOM_THABILITACION']	= utf8_encode(odbc_result($result_Predio_E,"NOM_THABILITACION"));
		$data['DatPredioLocal']['NOM_SECTOR']	 		= utf8_encode(odbc_result($result_Predio_E,"NOM_SECTOR"));
		$data['DatPredioLocal']['UBICADO_PISO']	 		= utf8_encode(odbc_result($result_Predio_E,"UBICADO_PISO"));
		$data['DatPredioLocal']['DAR_BAJA']				= utf8_encode(odbc_result($result_Predio_E,"DAR_BAJA"));
		$data['DatPredioLocal']['ENVIO_REG_A_SBN']		= utf8_encode(odbc_result($result_Predio_E,"ENVIO_REG_A_SBN"));
		$data['DatPredioLocal']['TIPO_VALIDACION']		= utf8_encode(odbc_result($result_Predio_E,"TIPO_VALIDACION"));
		$data['DatPredioLocal']['E_ENVIO_REG_A_SBN']		= utf8_encode(odbc_result($result_Predio_E,"ENVIO_REG_A_SBN"));
	
		$data['DatPredioLocal']['DR_CUS']					= utf8_encode(odbc_result($result_Predio_E,"CUS"));
		$data['DatPredioLocal']['DR_RSINABIP']				= utf8_encode(odbc_result($result_Predio_E,"RSINABIP"));
		$data['DatPredioLocal']['DR_NOM_PROP_REGISTRAL']	= utf8_encode(odbc_result($result_Predio_E,"NOM_PROP_REGISTRAL"));
		$data['DatPredioLocal']['DR_COD_OFIC_REGISTRAL']	= utf8_encode(odbc_result($result_Predio_E,"COD_OFIC_REGISTRAL"));
		$data['DatPredioLocal']['DR_AREA_REGISTRAL']		= number_format(odbc_result($result_Predio_E,"AREA_REGISTRAL"),2);
		$data['DatPredioLocal']['DR_COD_TIP_UNID_MED']		= utf8_encode(odbc_result($result_Predio_E,"COD_TIP_UNID_MED"));
		$data['DatPredioLocal']['DR_CODIGO_PREDIO']			= utf8_encode(odbc_result($result_Predio_E,"CODIGO_PREDIO"));
		$data['DatPredioLocal']['DR_PARTIDA_ELECTRONICA']	= utf8_encode(odbc_result($result_Predio_E,"PARTIDA_ELECTRONICA"));
		$data['DatPredioLocal']['DR_TOMO']					= utf8_encode(odbc_result($result_Predio_E,"TOMO"));
		$data['DatPredioLocal']['DR_ASIENTO']				= utf8_encode(odbc_result($result_Predio_E,"ASIENTO"));
		$data['DatPredioLocal']['DR_FOJAS']					= utf8_encode(odbc_result($result_Predio_E,"DU_P_FOJAS"));
		$data['DatPredioLocal']['DR_FICHA']					= utf8_encode(odbc_result($result_Predio_E,"FICHA"));
		$data['DatPredioLocal']['DR_TIPO_ARRENDAMIENTO']	= utf8_encode(odbc_result($result_Predio_E,"TIPO_ARRENDAMIENTO"));
		
		$data['DatPredioLocal']['DT_ZONIFICACION']		= utf8_encode(odbc_result($result_Predio_E,"ZONIFICACION"));
		$data['DatPredioLocal']['DT_COD_TIP_TERRENO']	= utf8_encode(odbc_result($result_Predio_E,"COD_TIP_TERRENO"));
		$data['DatPredioLocal']['DT_PERIMETRO']			= number_format(odbc_result($result_Predio_E,"PERIMETRO"),2);
		$data['DatPredioLocal']['DT_AREA_TERRENO']		= number_format(odbc_result($result_Predio_E,"AREA_TERRENO"),2);
		$data['DatPredioLocal']['DT_AREA_CONSTRUIDA']	= number_format(odbc_result($result_Predio_E,"AREA_CONSTRUIDA"),2);
		$data['DatPredioLocal']['DT_COD_ECONSERVACION']	= utf8_encode(odbc_result($result_Predio_E,"COD_ECONSERVACION"));
		$data['DatPredioLocal']['DT_NUMERO_PISOS']		= utf8_encode(odbc_result($result_Predio_E,"NUMERO_PISOS"));
		$data['DatPredioLocal']['DT_COD_ESANEAMIENTO']	= utf8_encode(odbc_result($result_Predio_E,"COD_ESANEAMIENTO"));		
		$data['DatPredioLocal']['DT_CODIGO_DETALLE_ESANEAMIENTO']	= utf8_encode(odbc_result($result_Predio_E,"CODIGO_DETALLE_ESANEAMIENTO"));
		$data['DatPredioLocal']['DT_CODIGO_USO_GENERICO']			= utf8_encode(odbc_result($result_Predio_E,"CODIGO_USO_GENERICO"));
		$data['DatPredioLocal']['DT_OTROS_USO_PREDIO']				= utf8_encode(odbc_result($result_Predio_E,"OTROS_USO_PREDIO"));
		$data['DatPredioLocal']['ID_USO_PREDIO_DU']				= utf8_encode(odbc_result($result_Predio_E,"ID_USO_PREDIO_DU"));
		$data['DatPredioLocal']['DU_P_ID_USO_PREDIO']				= utf8_encode(odbc_result($result_Predio_E,"DU_P_ID_USO_PREDIO"));	
		$data['DatPredioLocal']['DT_NRO_TRABAJADORES']				= utf8_encode(odbc_result($result_Predio_E,"DU_P_NRO_TRABAJADORES"));
		$data['DatPredioLocal']['DT_LICENCIA_FUNCIONAMIENTO']				= utf8_encode(odbc_result($result_Predio_E,"DU_P_LICENCIA_FUNCIONAMIENTO"));
		$data['DatPredioLocal']['DT_FECHA_VIGENCIA']				= utf8_encode(odbc_result($result_Predio_E,"DU_P_FECHA_VIGENCIA"));
		$data['DatPredioLocal']['DT_CERTIFICADO_DEFENSA_CIVIL']				= utf8_encode(odbc_result($result_Predio_E,"DU_P_CERTIFICADO_DEFENSA_CIVIL"));
		$data['DatPredioLocal']['DU_P_PARTIDA_REGISTRAL']				= utf8_encode(odbc_result($result_Predio_E,"DU_P_PARTIDA_REGISTRAL"));

		
		$data['DatPredioLocal']['ESPECIFIQUE_ACTOS']				= utf8_encode(odbc_result($result_Predio_E,"ESPECIFIQUE_ACTOS"));
		$data['DatPredioLocal']['ESPECIFIQUE_MODALIDAD']				= utf8_encode(odbc_result($result_Predio_E,"ESPECIFIQUE_MODALIDAD"));
		$data['DatPredioLocal']['FECHA_VIGENCIA_CERTIFICADO']				= utf8_encode(odbc_result($result_Predio_E,"FECHA_VIGENCIA_CERTIFICADO"));
		$data['DatPredioLocal']['FECHA_VIGENCIA_CERTIFICADO_DU']				= utf8_encode(odbc_result($result_Predio_E,"FECHA_VIGENCIA_CERTIFICADO_DU"));
		$data['DatPredioLocal']['COD_ECONSERVACION_CERTIFICADO_DU']				= utf8_encode(odbc_result($result_Predio_E,"COD_ECONSERVACION_CERTIFICADO_DU"));
		$data['DatPredioLocal']['VALOR_M_CUADRADO_DU']				= utf8_encode(odbc_result($result_Predio_E,"VALOR_M_CUADRADO_DU"));
		$data['DatPredioLocal']['MONEDA_VALOR_M_CUADRADO_DU']				= utf8_encode(odbc_result($result_Predio_E,"MONEDA_VALOR_M_CUADRADO_DU"));
		
		$data['DatPredioLocal']['PROP_HORIZ']	= utf8_encode(odbc_result($result_Predio_E,"PROP_HORIZ"));
		$data['DatPredioLocal']['ACTOS']	= utf8_encode(odbc_result($result_Predio_E,"ACTOS"));
		$data['DatPredioLocal']['NRO_RUC']	= utf8_encode(odbc_result($result_Predio_E,"NRO_RUC"));
		$data['DatPredioLocal']['DESOCUPADO']	= utf8_encode(odbc_result($result_Predio_E,"DESOCUPADO"));
		$data['DatPredioLocal']['DESOCUPADO']	= utf8_encode(odbc_result($result_Predio_E,"DESOCUPADO"));
		$data['DatPredioLocal']['UBICADO_PISO']	= utf8_encode(odbc_result($result_Predio_E,"UBICADO_PISO"));
		$data['DatPredioLocal']['TOMO']	= utf8_encode(odbc_result($result_Predio_E,"TOMO"));
		$data['DatPredioLocal']['FOJAS']	= utf8_encode(odbc_result($result_Predio_E,"FOJAS"));
		

		$data['DatPredioLocal']['AREA_TERRENO_DU']	= utf8_encode(odbc_result($result_Predio_E,"AREA_TERRENO_DU"));
		$data['DatPredioLocal']['PROP_HORIZ_DU']	= utf8_encode(odbc_result($result_Predio_E,"PROP_HORIZ_DU"));
		$data['DatPredioLocal']['NRO_PISOS_ARRENDADO_DU']	= utf8_encode(odbc_result($result_Predio_E,"NRO_PISOS_ARRENDADO_DU"));
		$data['DatPredioLocal']['AREA_TOTAL_ARRENDADO_DU']	= utf8_encode(odbc_result($result_Predio_E,"AREA_TOTAL_ARRENDADO_DU"));
		$data['DatPredioLocal']['ESPECIFIQUE_USO_DU']	= utf8_encode(odbc_result($result_Predio_E,"ESPECIFIQUE_USO_DU"));
		$data['DatPredioLocal']['ESPECIFIQUE_USO']	= utf8_encode(odbc_result($result_Predio_E,"ESPECIFIQUE_USO"));
		$data['DatPredioLocal']['NRO_TRABAJADORES_DU']	= utf8_encode(odbc_result($result_Predio_E,"NRO_TRABAJADORES_DU"));
		$data['DatPredioLocal']['LICENCIA_FUNCIONAMIENTO_DU']	= utf8_encode(odbc_result($result_Predio_E,"LICENCIA_FUNCIONAMIENTO_DU"));
		$data['DatPredioLocal']['FECHA_VIGENCIA_DU']	= utf8_encode(odbc_result($result_Predio_E,"FECHA_VIGENCIA_DU"));
		$data['DatPredioLocal']['CERTIFICADO_DEFENSA_CIVIL_DU']	= utf8_encode(odbc_result($result_Predio_E,"CERTIFICADO_DEFENSA_CIVIL_DU"));
		
		$data['DatPredioLocal']['NOMBRE_ARRENDADOR_DU']	= utf8_encode(odbc_result($result_Predio_E,"NOMBRE_ARRENDADOR_DU"));
		$data['DatPredioLocal']['DOCIDENTIDAD_DU']	= utf8_encode(odbc_result($result_Predio_E,"DOCIDENTIDAD_DU"));
		$data['DatPredioLocal']['NRODOCUMENTO_DU']	= utf8_encode(odbc_result($result_Predio_E,"NRODOCUMENTO_DU"));
		$data['DatPredioLocal']['FECHA_FIRMA_CONTRATO_DU']	= utf8_encode(odbc_result($result_Predio_E,"FECHA_FIRMA_CONTRATO_DU"));
		$data['DatPredioLocal']['RENTA_MENSUAL_DU']	= utf8_encode(odbc_result($result_Predio_E,"RENTA_MENSUAL_DU"));
		$data['DatPredioLocal']['MONEDA_RENTA_MENSUAL_DU']	= utf8_encode(odbc_result($result_Predio_E,"MONEDA_RENTA_MENSUAL_DU"));
		$data['DatPredioLocal']['FECHA_VCTO_CONTRATO_DU']	= utf8_encode(odbc_result($result_Predio_E,"FECHA_VCTO_CONTRATO_DU"));
		$data['DatPredioLocal']['ANIOS_TOTAL_ARRENDAMIENTO_DU']	= utf8_encode(odbc_result($result_Predio_E,"ANIOS_TOTAL_ARRENDAMIENTO_DU"));
		$data['DatPredioLocal']['MESES_TOTAL_ARRENDAMIENTO_DU']	= utf8_encode(odbc_result($result_Predio_E,"MESES_TOTAL_ARRENDAMIENTO_DU"));
		$data['DatPredioLocal']['MONTO_TOTAL_PAGADO_ARRENDAMIENTO_DU']	= utf8_encode(odbc_result($result_Predio_E,"MONTO_TOTAL_PAGADO_ARRENDAMIENTO_DU"));
		$data['DatPredioLocal']['MONEDA_MONTO_TOTAL_PAGADO_ARRENDAMIENTO_DU']	= utf8_encode(odbc_result($result_Predio_E,"MONEDA_MONTO_TOTAL_PAGADO_ARRENDAMIENTO_DU"));


		$data['Departamento'] 			= $this->Mostrar_Lista_Departamento();
		$data['Provincia'] 				= $this->Mostrar_Lista_Provincia(utf8_encode(odbc_result($result_Predio_E,"COD_DEPA")));
		$data['Distrito'] 				= $this->Mostrar_Lista_Distrito(utf8_encode(odbc_result($result_Predio_E,"COD_DEPA")), utf8_encode(odbc_result($result_Predio_E,"COD_PROV")));
		
		$data['TipoZona'] 				= $this->Mostrar_Lista_Tipo_Zona_Inmueble();
		$data['TipoVia'] 				= $this->Mostrar_Lista_Tipo_Via();
		$data['TipoHabilitacion'] 		= $this->Mostrar_Lista_Tipo_Habilitacion();
		$data['TipoPropiedad'] 			= $this->Mostrar_Listar_Tipo_Propiedad_GRAL();
		
		$data['DatOficRegistral'] 		= $this->Mostrar_Lista_Oficina_Registral();
		$data['DatUnidMedida'] 			= $this->Mostrar_Listar_Unidad_Medida();
		
		$data['DatTipTerreno'] 			= $this->Mostrar_Listar_Tipo_Terreno();
		$data['DatEstConservacion'] 	= $this->Mostrar_Lista_Estado_Conservacion();
		$data['DatEstSaneamiento'] 		= $this->Mostrar_Lista_Estado_Saneamiento();
		$data['DatDetalleSaneamiento'] 	= $this->Mostrar_Lista_Estado_Saneamiento_Detalle(utf8_encode(odbc_result($result_Predio_E,"COD_ESANEAMIENTO")));
		$data['DatUsoPredio'] 			= $this->Mostrar_Lista_Uso_Predio();
		
		$data['DatTipoDocTasa'] 		= $this->Mostrar_Lista_Inmuebles_Tipo_Doc_X_COD_TIP_DOC_PRED('9');//DOC TASACION
		$data['DatTipoValorizacion'] 	= $this->Mostrar_Lista_Tipo_Valorizacion();
		$data['DatTipoMoneda'] 			= $this->Mostrar_Lista_Moneda();
		$data['DatTipoMoneda'] 			= $this->Mostrar_Lista_Moneda();

		$data['DatUsoPredioGral'] 			= $this->Listado_Uso_Predio_Gral();

		$rsListValorizacion = $objPrediosModel->LISTAR_VALORIZACIONES_PREDIOS_X_ID_PREDIO_SBN($xID_PREDIO_SBN);
		while (odbc_fetch_row($rsListValorizacion)) {			
			
			$ArrayValorizacion = array();			
			$ArrayValorizacion['VAL_ITEM'] 				= odbc_result($rsListValorizacion, 'ITEM');
			$ArrayValorizacion['COD_VAL_PREDIO'] 				= odbc_result($rsListValorizacion, 'COD_VAL_PREDIO');
			$ArrayValorizacion['COD_TIP_VALORIZACION'] 			= utf8_encode(odbc_result($rsListValorizacion, 'COD_TIP_VALORIZACION'));
			$ArrayValorizacion['NOM_TIP_VALORIZACION'] 			= utf8_encode(odbc_result($rsListValorizacion, 'NOM_TIP_VALORIZACION'));
			$ArrayValorizacion['FECHA_VALORIZACION'] 			= cambiaf_a_normal(odbc_result($rsListValorizacion, 'FECHA_VALORIZACION'));
			$ArrayValorizacion['COD_MONEDA_VALORIZACION'] 		= utf8_encode(odbc_result($rsListValorizacion, 'COD_MONEDA_VALORIZACION'));
			$ArrayValorizacion['TXT_MONEDA'] 					= utf8_encode(odbc_result($rsListValorizacion, 'TXT_MONEDA'));
			$ArrayValorizacion['TXT_MONEDA_ABREV'] 				= odbc_result($rsListValorizacion, 'TXT_MONEDA_ABREV');
			$ArrayValorizacion['MONTO_VALOR_TERRENO'] 			= number_format(odbc_result($rsListValorizacion, 'MONTO_VALOR_TERRENO'),2);
			$ArrayValorizacion['MONTO_VALOR_CONTRUCCION'] 		= number_format(odbc_result($rsListValorizacion, 'MONTO_VALOR_CONTRUCCION'),2);
			$ArrayValorizacion['MONTO_VALOR_OBRA'] 				= number_format(odbc_result($rsListValorizacion, 'MONTO_VALOR_OBRA'),2);
			$ArrayValorizacion['MONTO_VALOR_TOTAL_INMUEBLE'] 	= number_format(odbc_result($rsListValorizacion, 'MONTO_VALOR_TOTAL_INMUEBLE'),2);
			$ArrayValorizacion['TIPO_CAMBIO_TOTAL_INMUEBLE'] 	= number_format(odbc_result($rsListValorizacion, 'TIPO_CAMBIO_TOTAL_INMUEBLE'),2);
			$ArrayValorizacion['MONTO_VALOR_TOTAL_INMUEBLE_SOL'] = number_format(odbc_result($rsListValorizacion, 'MONTO_VALOR_TOTAL_INMUEBLE_SOL'),2);
			$ID_ESTADO_VALORIZACION									= utf8_encode(odbc_result($rsListValorizacion, 'ID_ESTADO'));
			$ArrayValorizacion['ID_ESTADO'] 					= $id_estado_valoriz;
			$ArrayValorizacion['COD_PREDIO_DOC_TECN'] 			= utf8_encode(odbc_result($rsListValorizacion, 'COD_PREDIO_DOC_TECN'));
			$ArrayValorizacion['NOM_TIP_DOC'] 					= utf8_encode(odbc_result($rsListValorizacion, 'NOM_TIP_DOC'));
			$ArrayValorizacion['NOM_ARCHIVO'] 					= utf8_encode(odbc_result($rsListValorizacion, 'NOM_ARCHIVO'));	
			
			$ArrayValorizacion['VAL_ID_ID_ESTADO'] 					= $ID_ESTADO_VALORIZACION;
			$ArrayValorizacion['VAL_DESC_ESTADO'] 					= ($ID_ESTADO_VALORIZACION == '1')? 'Activo':'Pendiente';
			
			$data['DatValorizacion'][] = $ArrayValorizacion;
						
		}
		
		/*******************************************************************************/
				
		$rst_obs = $objPrediosModel->Ver_Historial_Tipo_Validacion_x_ID_PREDIO_SBN($xID_PREDIO_SBN);
		while (odbc_fetch_row($rst_obs)) {
			
			$ArrayHistorial = array();			
			$ArrayHistorial['H_ID_CONTROL_PREDIO'] = utf8_encode(odbc_result($rst_obs,"ID_CONTROL_PREDIO"));
			$ArrayHistorial['H_ID_CONTROL_PREDIO_OBS'] = utf8_encode(odbc_result($rst_obs,"ID_CONTROL_PREDIO_OBS"));
			$ArrayHistorial['H_FECHA'] = utf8_encode(odbc_result($rst_obs,"FECHA"));
			$ArrayHistorial['H_SBN_COD_USUARIO'] = utf8_encode(odbc_result($rst_obs,"SBN_COD_USUARIO"));
			$ArrayHistorial['H_SBN_NOM_USUARIO'] = utf8_encode(odbc_result($rst_obs,"SBN_NOM_USUARIO"));
			$ArrayHistorial['H_SBN_NOMBRES_PERS'] = utf8_encode(odbc_result($rst_obs,"SBN_NOMBRES_PERS"));
			$ArrayHistorial['H_TIPO_VALIDACION'] = utf8_encode(odbc_result($rst_obs,"TIPO_VALIDACION"));
			$ArrayHistorial['H_OBSERVACION'] = utf8_encode(odbc_result($rst_obs,"OBSERVACION"));
			$ArrayHistorial['H_ENT_COD_USUARIO'] = utf8_encode(odbc_result($rst_obs,"ENT_COD_USUARIO"));
			$ArrayHistorial['H_ENT_COD_USUARIO'] = utf8_encode(odbc_result($rst_obs,"ENT_COD_USUARIO"));
			$ArrayHistorial['H_ENT_NOM_ENTIDAD'] = utf8_encode(odbc_result($rst_obs,"ENT_NOM_ENTIDAD"));
			$ENT_TIPO_MODULO 					= utf8_encode(odbc_result($rst_obs,"TIPO_MODULO"));
			$ArrayHistorial['H_TIPO_MODULO'] 	= $ENT_TIPO_MODULO;
			
			if($ENT_TIPO_MODULO == 'M'){
				$DESC_MODULO = '(MODULO MUEBLES)';	
				
			}elseif($ENT_TIPO_MODULO == 'I'){				
				$DESC_MODULO = '(MODULO INMUEBLES)';	
			}
			
			
								
			if($SBN_COD_USUARIO == ''){
				
				$nombre_registrador = "<B>".$ENT_NOM_ENTIDAD."</B> <br> USUARIO EXTERNO - ".$DESC_MODULO." ";
			}else{
				$nombre_registrador = "<B>SUPERINTENDENCIA NACIONAL DE BIENES ESTATALES (SBN) - CATASTRO</B> <br> $SBN_NOMBRES_PERS (USUARIO $SBN_NOM_USUARIO)";
			}
			
			$ArrayHistorial['H_DESC_MODULO'] 			= $DESC_MODULO;
			$ArrayHistorial['H_nombre_registrador'] 	= $nombre_registrador;
			
			$data['DataHistorial'][] = $ArrayHistorial;
			
		}
		
		/*******************************************************************************/
				
		$rst_regInmobil = $objPrediosModel->LISTAR_DATOS_PADRON_PREDIOS_UNID_INMOBIL($xID_PREDIO_SBN);
		while (odbc_fetch_row($rst_regInmobil)) {
			
			$ArrayRegInmobil = array();
			
			$ArrayRegInmobil['B_ITEM'] 	= utf8_encode(odbc_result($rst_regInmobil,"ITEM"));
			$ArrayRegInmobil['B_COD_UNID_INMOBIL'] 	= utf8_encode(odbc_result($rst_regInmobil,"COD_UNID_INMOBIL"));
			$ArrayRegInmobil['B_NOM_UNID_INMOBIL'] 	= utf8_encode(odbc_result($rst_regInmobil,"NOM_UNID_INMOBIL"));
			$ArrayRegInmobil['B_AREA_UNID_INMOBIL'] = utf8_encode(odbc_result($rst_regInmobil,"AREA_UNID_INMOBIL"));
			$ArrayRegInmobil['B_VALOR_AUTOVALUO'] 	= utf8_encode(odbc_result($rst_regInmobil,"VALOR_AUTOVALUO"));
			$ArrayRegInmobil['B_OBS_UNID_INMOBIL'] 	= utf8_encode(odbc_result($rst_regInmobil,"OBS_UNID_INMOBIL"));
			$id_estado_reg_inmobi 					= utf8_encode(odbc_result($rst_regInmobil,"ID_ESTADO"));
			$ArrayRegInmobil['B_ID_ESTADO'] 		= $id_estado_reg_inmobi;
			$ArrayRegInmobil['B_DESC_ESTADO_INMOBIL'] 		= ($id_estado_reg_inmobi == '1') ? 'Activo' : 'Pendiente';
			

			$data['DataUnidInmobiliaria'][] = $ArrayRegInmobil;
			
		}
		
		/*******************************************************************************/
		
		
		$data['DataPartidaRegistral'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataPartidaRegistral', $xID_PREDIO_SBN, '5');
		$data['DataPlanoPerimetrico'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataPlanoPerimetrico', $xID_PREDIO_SBN, '6');
		$data['DataPlanoUbicacion'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataPlanoUbicacion', $xID_PREDIO_SBN, '7');
		$data['DataPlanoHabilitacion'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataPlanoHabilitacion', $xID_PREDIO_SBN, '8');
		$data['DataFotografias'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataFotografias', $xID_PREDIO_SBN, '10');
		$data['DataEscrituraPublica'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataEscrituraPublica', $xID_PREDIO_SBN, '11');
		$data['DataMemoriaDescriptiva'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataMemoriaDescriptiva', $xID_PREDIO_SBN, '12');
		$data['DataTituloPropiedad'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataTituloPropiedad', $xID_PREDIO_SBN, '13');
		$data['DataInformeTecnicoLegal'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataInformeTecnicoLegal', $xID_PREDIO_SBN, '14');
		$data['DataDispositivoLegal'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataDispositivoLegal', $xID_PREDIO_SBN, '15');
		$data['DataPlanoPerimetricoAutoCAD'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataPlanoPerimetricoAutoCAD', $xID_PREDIO_SBN, '16');
						
		//$this->dump($data['DataPartidaRegistral']);
		
		$this->render('v.predio_form.php', $data);	
		
		
	}

	public function Mostrar_Datos_Predio_Local_GRAL_V2(){
		
		$data = array();
		
		$objPrediosModel 	= new PrediosModel();
		$objUbigeoModel 		= new UbigeoModel();
		
		$xID_PREDIO_SBN = isset($_GET['xID_PREDIO_SBN']) ? $_GET['xID_PREDIO_SBN'] : '';
		$COD_ENTIDAD = isset($_GET['TXH_SIMI_COD_ENTIDAD']) ? $_GET['TXH_SIMI_COD_ENTIDAD'] : '';
		
		
		$result_Predio_E = $objPrediosModel->Ver_Datos_Local_x_CODIGO_GRAL_DU_v2($xID_PREDIO_SBN, $COD_ENTIDAD);	 

		
		$data['DatPredioLocal']['E_ID_PREDIO_SBN']	 	= utf8_encode(odbc_result($result_Predio_E,"ID_PREDIO_SBN"));
		$data['DatPredioLocal']['DENOMINACION_INMUEBLE']	= utf8_encode(odbc_result($result_Predio_E,"DENOMINACION_INMUEBLE"));
		//$data['DatPredioLocal']['E_COD_TIP_PROPIEDAD']	= utf8_encode(odbc_result($result_Predio_E,"COD_TIP_PROPIEDAD"));
		$data['DatPredioLocal']['E_COD_DEPA']	 			= utf8_encode(odbc_result($result_Predio_E,"COD_DEPA"));
		$data['DatPredioLocal']['E_COD_PROV']	 			= utf8_encode(odbc_result($result_Predio_E,"COD_PROV"));
		$data['DatPredioLocal']['E_COD_DIST']				= utf8_encode(odbc_result($result_Predio_E,"COD_DIST"));
		$data['DatPredioLocal']['E_COD_TVIA']	 		= utf8_encode(odbc_result($result_Predio_E,"COD_TVIA"));
		
		$data['DatPredioLocal']['NOMBRE_VIA']	 		= utf8_encode(odbc_result($result_Predio_E,"NOMBRE_VIA"));
		$data['DatPredioLocal']['NRO_PRED']	 			= utf8_encode(odbc_result($result_Predio_E,"NRO_PRED"));
		$data['DatPredioLocal']['MZA_PRED']	 			= utf8_encode(odbc_result($result_Predio_E,"MZA_PRED"));
		$data['DatPredioLocal']['LTE_PRED']				= utf8_encode(odbc_result($result_Predio_E,"LTE_PRED"));
		$data['DatPredioLocal']['E_UBIC_DET_PRED']		= utf8_encode(odbc_result($result_Predio_E,"URBANIZACION"));

		$data['DatPredioLocal']['PERS_CAS']	 	= utf8_encode(odbc_result($result_Predio_E,"PERS_CAS"));
		$data['DatPredioLocal']['PERS_CAP']	 	= utf8_encode(odbc_result($result_Predio_E,"PERS_CAP"));
		$data['DatPredioLocal']['PERS_FAG']	 	= utf8_encode(odbc_result($result_Predio_E,"PERS_FAG"));
		
		$data['DatPredioLocal']['PERS_TERCEROS']	 	= utf8_encode(odbc_result($result_Predio_E,"PERS_TERCEROS"));
		$data['DatPredioLocal']['PERS_PRACTICANTES']	 	= utf8_encode(odbc_result($result_Predio_E,"PERS_PRACTICANTES"));
		$data['DatPredioLocal']['PERS_TERC_VIGI']	 	= utf8_encode(odbc_result($result_Predio_E,"PERS_TERC_VIGI"));
		$data['DatPredioLocal']['PERS_TERC_LIMP']	 	= utf8_encode(odbc_result($result_Predio_E,"PERS_TERC_LIMP"));
		$data['DatPredioLocal']['PERS_TERC_JARD']	 	= utf8_encode(odbc_result($result_Predio_E,"PERS_TERC_JARD"));
		$data['DatPredioLocal']['PERS_TERC_MANT']	 	= utf8_encode(odbc_result($result_Predio_E,"PERS_TERC_MANT"));

		$data['DatPredioLocal']['AREA_TOTAL']	 	= utf8_encode(odbc_result($result_Predio_E,"AREA_TOTAL"));
		$data['DatPredioLocal']['AREA_UTIL']	 	= utf8_encode(odbc_result($result_Predio_E,"AREA_UTIL"));
		$data['DatPredioLocal']['RENTA_MENSUAL_SOLES']	 	= utf8_encode(odbc_result($result_Predio_E,"RENTA_MENSUAL_SOLES"));
		$data['DatPredioLocal']['TIPO_INMUEBLE']	 	= utf8_encode(odbc_result($result_Predio_E,"TIPO_INMUEBLE"));
		$data['DatPredioLocal']['COD_ECONSERVACION']	= utf8_encode(odbc_result($result_Predio_E,"COD_ECONSERVACION"));
		$data['DatPredioLocal']['ID_USO_PREDIO']	= utf8_encode(odbc_result($result_Predio_E,"ID_USO_PREDIO"));
		$data['DatPredioLocal']['ES_ARRENDADO']	= utf8_encode(odbc_result($result_Predio_E,"ES_ARRENDADO"));
		$data['DatPredioLocal']['SECTOR_NACIONAL']	= utf8_encode(odbc_result($result_Predio_E,"SECTOR_NACIONAL"));

		

		
		
		$data['Departamento'] 			= $this->Mostrar_Lista_Departamento();
		$data['Provincia'] 				= $this->Mostrar_Lista_Provincia(utf8_encode(odbc_result($result_Predio_E,"COD_DEPA")));
		$data['Distrito'] 				= $this->Mostrar_Lista_Distrito(utf8_encode(odbc_result($result_Predio_E,"COD_DEPA")), utf8_encode(odbc_result($result_Predio_E,"COD_PROV")));
		
		$data['TipoZona'] 				= $this->Mostrar_Lista_Tipo_Zona_Inmueble();
		$data['TipoVia'] 				= $this->Mostrar_Lista_Tipo_Via();
		$data['TipoHabilitacion'] 		= $this->Mostrar_Lista_Tipo_Habilitacion();
		$data['TipoPropiedad'] 			= $this->Mostrar_Listar_Tipo_Propiedad_GRAL();
		
		$data['DatOficRegistral'] 		= $this->Mostrar_Lista_Oficina_Registral();
		$data['DatUnidMedida'] 			= $this->Mostrar_Listar_Unidad_Medida();
		
		$data['DatTipTerreno'] 			= $this->Mostrar_Listar_Tipo_Terreno();
		$data['DatEstConservacion'] 	= $this->Mostrar_Lista_Estado_Conservacion();
		$data['DatEstSaneamiento'] 		= $this->Mostrar_Lista_Estado_Saneamiento();
		
		$data['DatUsoPredio'] 			= $this->Mostrar_Lista_Uso_Predio();
		
		$data['DatTipoDocTasa'] 		= $this->Mostrar_Lista_Inmuebles_Tipo_Doc_X_COD_TIP_DOC_PRED('9');//DOC TASACION
		$data['DatTipoValorizacion'] 	= $this->Mostrar_Lista_Tipo_Valorizacion();
		$data['DatTipoMoneda'] 			= $this->Mostrar_Lista_Moneda();
		$data['DatTipoMoneda'] 			= $this->Mostrar_Lista_Moneda();

		$data['DatUsoPredioGral'] 			= $this->Listado_Uso_Predio_Gral();

			
		
		$this->render('v.predio_form_V2.php', $data);	
	
		
	}

	
	public function Guardar_Datos_Local_Predio(){
		
		$objModel = new BienPatrimonialModel();
		$objPrediosModel 	= new PrediosModel();
		$data = array();
		
		$TXH_SIMI_COD_USUARIO = isset($_GET['TXH_SIMI_COD_USUARIO']) ? $_GET['TXH_SIMI_COD_USUARIO'] : '';
		$COD_ENTIDAD = isset($_GET['TXH_SIMI_COD_ENTIDAD']) ? $_GET['TXH_SIMI_COD_ENTIDAD'] : '';
		$txh_COD_UE_LOCAL = isset($_GET['txh_COD_UE_LOCAL']) ? $_GET['txh_COD_UE_LOCAL'] : '';
		$DENOMINACION_PREDIO = isset($_GET['txt_nom_inmueble']) ? utf8_decode(($_GET['txt_nom_inmueble'])) : '';
		$COD_DEPA = isset($_GET['cbo_departamento']) ? $_GET['cbo_departamento'] : '';
		$COD_PROV = isset($_GET['cbo_provincia']) ? $_GET['cbo_provincia'] : '';
		$COD_DIST = isset($_GET['cbo_distrito']) ? $_GET['cbo_distrito'] : '';
		$COD_ZONA = isset($_GET['cbo_zona']) ? $_GET['cbo_zona'] : '';
		$COD_TVIA = isset($_GET['cbo_via']) ? $_GET['cbo_via'] : '';
		$NOMBRE_VIA = isset($_GET['txt_nom_via']) ? $_GET['txt_nom_via'] : '';
		$NRO_PRED = isset($_GET['txt_nro_inmueble']) ? $_GET['txt_nro_inmueble'] : '';
		$MZA_PRED = isset($_GET['txt_manzana']) ? $_GET['txt_manzana'] : '';
		$LTE_PRED = isset($_GET['txt_lote']) ? $_GET['txt_lote'] : '';
		$DET_PRED = isset($_GET['txt_detalle']) ? $_GET['txt_detalle'] : '';
		$DET_PRED_NRO = isset($_GET['txt_nro_detalle']) ? $_GET['txt_nro_detalle'] : '';
		$COD_THABILITACION = isset($_GET['cbo_habilitacion']) ? $_GET['cbo_habilitacion'] : '';
		$NOM_THABILITACION = isset($_GET['txt_nom_habilitacion']) ? $_GET['txt_nom_habilitacion'] : '';
		$NOM_SECTOR = isset($_GET['txt_nom_sector']) ? $_GET['txt_nom_sector'] : '';
		$COD_TIP_PROPIEDAD = isset($_GET['cbo_propiedad']) ? $_GET['cbo_propiedad'] : '';
		$UBICADO_PISO = isset($_GET['txt_ubicado_piso_nro']) ? $_GET['txt_ubicado_piso_nro'] : '';
		
		$MODULO_REGISTRO			= 'MODULO INMUEBLES';
		
		$CUS = isset($_GET['txt_reg_cus']) ? $_GET['txt_reg_cus'] : '';
		$RSINABIP = isset($_GET['txt_reg_sinabip']) ? $_GET['txt_reg_sinabip'] : '';
		$NOM_PROP_REGISTRAL = isset($_GET['txt_propietario_registral']) ? $_GET['txt_propietario_registral'] : '';
		$COD_OFIC_REGISTRAL = isset($_GET['cbo_oficina_registral']) ? $_GET['cbo_oficina_registral'] : '';
		$PARTIDA_ELECTRONICA = isset($_GET['txt_partida_registral']) ? $_GET['txt_partida_registral'] : '';
		$CODIGO_PREDIO = isset($_GET['txt_cod_predio']) ? $_GET['txt_cod_predio'] : '';
		$AREA_REGISTRAL = isset($_GET['txt_area_registral']) ? $_GET['txt_area_registral'] : '';
		$COD_TIP_UNID_MED = isset($_GET['cbo_unid_medida']) ? $_GET['cbo_unid_medida'] : '';
		$TOMO = isset($_GET['txt_tomo']) ? $_GET['txt_tomo'] : '';
		$ASIENTO = isset($_GET['txt_asiento']) ? $_GET['txt_asiento'] : '';
		$FOJAS = isset($_GET['txt_fojas']) ? $_GET['txt_fojas'] : '';
		$FICHA = isset($_GET['txt_ficha']) ? $_GET['txt_ficha'] : '';
		$ANOTACION_PREVENTIVA = isset($_GET['txt_anotacion_preventiva']) ? $_GET['txt_anotacion_preventiva'] : '';
		
		
		$ZONIFICACION = isset($_GET['txt_zonficacion']) ? $_GET['txt_zonficacion'] : '';
		$COD_TIP_TERRENO = isset($_GET['cbo_tipo_terreno']) ? $_GET['cbo_tipo_terreno'] : '';
		$PERIMETRO = isset($_GET['txt_perimetro']) ? $_GET['txt_perimetro'] : '0';
		$AREA_TERRENO = isset($_GET['txt_area_terreno']) ? $_GET['txt_area_terreno'] : '0';
		$AREA_CONSTRUIDA = isset($_GET['txt_area_construida']) ? $_GET['txt_area_construida'] : '0';
		$COD_ECONSERVACION = isset($_GET['cbo_est_conservacion']) ? $_GET['cbo_est_conservacion'] : '';
		$NUMERO_PISOS = isset($_GET['txt_nro_pisos']) ? $_GET['txt_nro_pisos'] : '';
		$COD_ESANEAMIENTO = isset($_GET['cbo_estado_saneamiento']) ? $_GET['cbo_estado_saneamiento'] : '';
		$CODIGO_DETALLE_ESANEAMIENTO = isset($_GET['cbo_det_saneamiento']) ? $_GET['cbo_det_saneamiento'] : '';
		$CODIGO_USO_GENERICO = isset($_GET['cbo_uso_predio']) ? $_GET['cbo_uso_predio'] : '';
		$OTROS_USO_PREDIO = isset($_GET['txt_otros_usos']) ? $_GET['txt_otros_usos'] : '';
		$ID_ESTADO = '1';
		
		/* VALIDACIONES */
		/*
		if ($validaciones->validar_caracteresEspeciales($DENOMINACION_PREDIO)) {
			$error[] = 'El campo Nombre de Local contiene caracteres no admitidos.';
			$valor = 2;
		}
		*/
		
		$validaciones	=	new Clases;

		if (!$validaciones->validar_entero($NRO_PRED)) {
			$error[] = 'El campo de Numero debe ser un número.';
			$valor = 2;
		}
		if (!$validaciones->validar_entero($UBICADO_PISO )) {
			$error[] = 'El campo Nro de Pisos debe ser un número.';
			$valor = 2;
		}
		/**** FIN DE VALIDACIONES ****/
		
		if (isset($error)) {
			$valor = 2;
		}else{
			
			if($txh_COD_UE_LOCAL == ''){
				
				$RESULT_COD			=	$objPrediosModel->Padron_Generar_Codigo_Padron_Predios();
				$ID_PREDIO_SBN 		= 	odbc_result($RESULT_COD,"CODIGO");
			

				/* CREACION:        WILLIAMS ARENAS */
				/* FECHA CREACION:  06-06-2020 */
				/* DESCRIPCION:     REGISTRAR PREDIO */
				$data = array();    

				$dataModel_01 = [
					'ID_PREDIO_SBN'			=>$ID_PREDIO_SBN,
					'COD_ENTIDAD'			=>$COD_ENTIDAD,
					'DENOMINACION_PREDIO'	=>$DENOMINACION_PREDIO,
					'COD_DEPA'				=>$COD_DEPA,
					'COD_PROV'				=>$COD_PROV,
					'COD_DIST'				=>$COD_DIST,
					'COD_ZONA'				=>$COD_ZONA,
					'COD_TVIA'				=>$COD_TVIA,
					'NOMBRE_VIA'			=>$NOMBRE_VIA,
					'NRO_PRED'				=>$NRO_PRED,
					'MZA_PRED'				=>$MZA_PRED,
					'LTE_PRED'				=>$LTE_PRED,
					'UBICADO_PISO'			=>$UBICADO_PISO,
					'DET_PRED'				=>$DET_PRED,
					'DET_PRED_NRO'			=>$DET_PRED_NRO,
					'COD_THABILITACION'		=>$COD_THABILITACION,
					'NOM_THABILITACION'		=>$NOM_THABILITACION,
					'NOM_SECTOR'			=>$NOM_SECTOR,
					'COD_TIP_PROPIEDAD'		=>$COD_TIP_PROPIEDAD,
					'MODULO_REGISTRO'		=>$MODULO_REGISTRO,
					'USUARIO'				=>$TXH_SIMI_COD_USUARIO,
					'ID_ESTADO'				=>$ID_ESTADO
				];
				
				$RESULTADO = $objModel->Padron_Insertar_Local_Predios_x_Muebles_PDO($dataModel_01);
				/**** FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS ****/

				$error[] = '';
				$valor = ($RESULTADO>=0) ? '1':'0';
					
			}else{
				
				$ID_PREDIO_SBN		=	$txh_COD_UE_LOCAL;		
				$data = array();    

				$dataModel_01 = [
					'ID_PREDIO_SBN'			=>$ID_PREDIO_SBN,
					'COD_ENTIDAD'			=>$COD_ENTIDAD,
					'DENOMINACION_PREDIO'	=>$DENOMINACION_PREDIO,
					'COD_DEPA'				=>$COD_DEPA,
					'COD_PROV'				=>$COD_PROV,
					'COD_DIST'				=>$COD_DIST,
					'COD_ZONA'				=>$COD_ZONA,
					'COD_TVIA'				=>$COD_TVIA,
					'NOMBRE_VIA'			=>$NOMBRE_VIA,
					'NRO_PRED'				=>$NRO_PRED,
					'MZA_PRED'				=>$MZA_PRED,
					'LTE_PRED'				=>$LTE_PRED,
					'UBICADO_PISO'			=>$UBICADO_PISO,
					'DET_PRED'				=>$DET_PRED,
					'DET_PRED_NRO'			=>$DET_PRED_NRO,
					'COD_THABILITACION'		=>$COD_THABILITACION,
					'NOM_THABILITACION'		=>$NOM_THABILITACION,
					'NOM_SECTOR'			=>$NOM_SECTOR,
					'COD_TIP_PROPIEDAD'		=>$COD_TIP_PROPIEDAD,
					'MODULO_REGISTRO'		=>$MODULO_REGISTRO,
					'USUARIO'				=>$TXH_SIMI_COD_USUARIO,
					'ID_ESTADO'				=>$ID_ESTADO
				];
				
				$RESULTADO = $objModel->Padron_Actualizar_Local_Predios_x_Muebles_PDO($dataModel_01);
				/**** FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS ****/

				$error[] = '';
				$valor = ($RESULTADO>=0) ? '1':'0';
			}
		
		}
		echo json_encode(array('valor'=>$valor,'errores'=>$error));
	
	}
	
	public function Eliminar_Documento_Tecnico(){
		
		$data = array();
		
		$objPrediosModel 	= new PrediosModel();

		$COD_PREDIO_DOC_TECN 	= isset($_GET['txtH_COD_PREDIO_DOC_TECN']) ? $_GET['txtH_COD_PREDIO_DOC_TECN'] : '';
		$USUARIO 				= isset($_GET['TXH_SIMI_COD_USUARIO']) ? $_GET['TXH_SIMI_COD_USUARIO'] : '';
		
		$RESULTADO_Eliminar = $objPrediosModel->Eliminar_Documento_Tecnico($COD_PREDIO_DOC_TECN, $USUARIO);
			
		$valor = ($RESULTADO_Eliminar) ? 1 : 0;
			
		echo $valor;
	}
	
	
	public function Guardar_Datos_Valorizacion(){
		
		$data = array();
		
		$objPrediosModel 	= new PrediosModel();
		
		$txh_cod_val_predio = isset($_POST['txh_cod_val_predio']) ? $_POST['txh_cod_val_predio'] : '';
		$USUARIO = isset($_POST['txh_cod_val_cod_user']) ? $_POST['txh_cod_val_cod_user'] : '';
		$COD_ENTIDAD = isset($_POST['txh_cod_val_cod_ent']) ? $_POST['txh_cod_val_cod_ent'] : '';
		$txh_cod_val_cod_predio = isset($_POST['txh_cod_val_cod_ent']) ? $_POST['txh_cod_val_cod_predio'] : '';
		
		$COD_TIP_DOC_PRED = isset($_POST['CBO_tip_doc_tecnico_val']) ? $_POST['CBO_tip_doc_tecnico_val'] : '';
		$COD_TIP_VALORIZACION = isset($_POST['cbo_tipo_valorizacion']) ? $_POST['cbo_tipo_valorizacion'] : '';
		$FECHA_VALORIZACION = isset($_POST['txt_fecha_valorizacion']) ? cambiar_Fecha_a_SqlServer($_POST['txt_fecha_valorizacion']) : '';
		$COD_MONEDA_VALORIZACION = isset($_POST['cbo_tipo_moneda']) ? $_POST['cbo_tipo_moneda'] : '';
		$MONTO_VALOR_TERRENO = isset($_POST['txt_valor_terreno']) ? $_POST['txt_valor_terreno'] : '0';
		$MONTO_VALOR_CONTRUCCION = isset($_POST['txt_valor_construccion']) ? $_POST['txt_valor_construccion'] : '0';
		$MONTO_VALOR_OBRA = isset($_POST['txt_valor_obra']) ? $_POST['txt_valor_obra'] : '0';
		$MONTO_VALOR_TOTAL_INMUEBLE = isset($_POST['txt_valor_total_inmueble']) ? $_POST['txt_valor_total_inmueble'] : '0';
		$TIPO_CAMBIO_TOTAL_INMUEBLE = isset($_POST['txt_tipo_cambio']) ? $_POST['txt_tipo_cambio'] : '0';
		$MONTO_VALOR_TOTAL_INMUEBLE_SOL = isset($_POST['txt_valor_total_inmueble_soles']) ? $_POST['txt_valor_total_inmueble_soles'] : '0';
		
		$TXT_COD_PREDIO_DOC_TECN_X2 = isset($_POST['TXT_COD_PREDIO_DOC_TECN_X2']) ? $_POST['TXT_COD_PREDIO_DOC_TECN_X2'] : '';		
		$txf_archivo_tasacion = isset($_POST['txf_archivo_tasacion']) ? $_POST['txf_archivo_tasacion'] : '';
		$txt_indicador_reg_tasac = isset($_POST['txt_indicador_reg_tasac']) ? $_POST['txt_indicador_reg_tasac'] : '';
		$txt_w_nom_archivo_reg_tasac = isset($_POST['txt_w_nom_archivo_reg_tasac']) ? $_POST['txt_w_nom_archivo_reg_tasac'] : '';
		$txt_w_peso_archivo_reg_tasac = isset($_POST['txt_w_peso_archivo_reg_tasac']) ? $_POST['txt_w_peso_archivo_reg_tasac'] : '';
		
		
		$MONTO_VALOR_TERRENO = ($MONTO_VALOR_TERRENO == '') ? '0':$MONTO_VALOR_TERRENO;
		$MONTO_VALOR_CONTRUCCION = ($MONTO_VALOR_CONTRUCCION == '') ? '0':$MONTO_VALOR_CONTRUCCION;
		$MONTO_VALOR_OBRA = ($MONTO_VALOR_OBRA == '') ? '0':$MONTO_VALOR_OBRA;
		$MONTO_VALOR_TOTAL_INMUEBLE = ($MONTO_VALOR_TOTAL_INMUEBLE == '') ? '0':$MONTO_VALOR_TOTAL_INMUEBLE;
		
		$ID_ESTADO = 0;
		$MODULO_REGISTRO = 'MODULO INMUEBLES';
		
		if($txh_cod_val_cod_predio == ''){
			
			//inserta predio basico
			$RESULT_CODPredio = $objPrediosModel->Padron_Generar_Codigo_Padron_Predios();
			$ID_PREDIO_SBN	= 	odbc_result($RESULT_CODPredio,"CODIGO");	
			
			$RESULT_TASACION = $objPrediosModel->Padron_Insertar_Local_Predios_Basico($ID_PREDIO_SBN, $COD_ENTIDAD, $MODULO_REGISTRO, $USUARIO);
			
		}else if($txh_cod_val_cod_predio != ''){
			
			$ID_PREDIO_SBN = $txh_cod_val_cod_predio;
		}
		
		if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
		
			//obtenemos el archivo a subir
			$file = $_FILES['txf_archivo_tasacion']['name'];
			
			$ARCHIVO					= $_FILES['txf_archivo_tasacion']['tmp_name'];
			$ARCHIVO_NOMBRE				= basename($_FILES['txf_archivo_tasacion']['name']);
			$ARCHIVO_PESO				= $_FILES['txf_archivo_tasacion']['size'];
			$ARCHIVO_TIPO				= $_FILES['txf_archivo_tasacion']['type'];
			$ARCHIVO_EXTENSION			= extension_archivo($_FILES['txf_archivo_tasacion']['name']);
			
			$nombre_carpeta	 = $this->Nombre_Carpeta($COD_TIP_DOC_PRED);
			
			if($TXT_COD_PREDIO_DOC_TECN_X2 == '' || $TXT_COD_PREDIO_DOC_TECN_X2 == '0'){
						
				$RESULT_TASACION = $objPrediosModel->GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS();
				$COD_PREDIO_DOC_TECN	= 	odbc_result($RESULT_TASACION,"CODIGO");	
							
			}else{
						
				$COD_PREDIO_DOC_TECN = $TXT_COD_PREDIO_DOC_TECN_X2;		
					
			}
					
			$UPLOADDIR='../../../../Repositorio/predios_docs_tecnicos/'.$nombre_carpeta;
			//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
			
			$NUEVA_CADENA				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
			
			$ARCHIVO_NOMBRE_GENERADO	= $nombre_carpeta.'_TASACION_'.$COD_TIP_DOC_PRED.'_PR_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
			$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;
			
			
			$DESCRIP_ARCHIVO = '';
			$NOM_ARCHIVO = $ARCHIVO_NOMBRE_GENERADO;
			$PESO_ARCHIVO = $ARCHIVO_PESO;
			$NOM_ARCHIVO_CAD = '';
			$PESO_ARCHIVO_CAD = '';
			$NRO_PART_REG = '';
			$FECHA_TASACION = $FECHA_VALORIZACION;
			$COD_TIP_RESOL_DL = '';
			$COD_SISTEMA_COORDENADA = '';
			$ID_ESTADO = 0;	
			

	
			if ($file !='' && move_uploaded_file($ARCHIVO, $UPLOADFILE) && $txt_indicador_reg_tasac == '1') {
			
				
				if($TXT_COD_PREDIO_DOC_TECN_X2 == ''  || $TXT_COD_PREDIO_DOC_TECN_X2 == '0'){
										
					$RESULT_Archivo	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);	
					
					//echo  "55";

				}else{
					$RESULT_Archivo	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);

					//echo  "56";
				}
									
			}else if ($file =='' && $txt_indicador_reg_tasac == '') {

				$NOM_ARCHIVO = $txt_w_nom_archivo_reg_tasac;
				$PESO_ARCHIVO = $txt_w_peso_archivo_reg_tasac;
				
				if($TXT_COD_PREDIO_DOC_TECN_X2 == ''  || $TXT_COD_PREDIO_DOC_TECN_X2 == '0'){
							
					$RESULT_Archivo	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);						
				
					//echo  "57";
					
				}else{
					$RESULT_Archivo	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
					
					//echo  "58";
					
				}
				
			}
			
			
		}else{
			throw new Exception("Error Processing Request", 1);   
		}
	
	
		if($txh_cod_val_predio == ''){		
			
			$RESULT_COD_VAL		=	$objPrediosModel->GENERAR_CODIGO_PADRON_PREDIOS_VALORIZACION();
			$COD_VAL_PREDIO 	= 	odbc_result($RESULT_COD_VAL,"CODIGO");
		
			$RESULTADO			=	$objPrediosModel->INSERTAR_VALORIZACION_PADRON_PREDIOS($COD_VAL_PREDIO, $ID_PREDIO_SBN, $COD_ENTIDAD, $COD_TIP_DOC_PRED, $COD_TIP_VALORIZACION, $FECHA_VALORIZACION, $COD_MONEDA_VALORIZACION, $MONTO_VALOR_TERRENO, $MONTO_VALOR_CONTRUCCION, $MONTO_VALOR_OBRA, $MONTO_VALOR_TOTAL_INMUEBLE, $TIPO_CAMBIO_TOTAL_INMUEBLE, $MONTO_VALOR_TOTAL_INMUEBLE_SOL, $COD_PREDIO_DOC_TECN, $USUARIO, $ID_ESTADO);
			
			$valor = ($RESULTADO) ? '1'.'[*]'.$ID_PREDIO_SBN: 0 ;
			
		}else{
						
			$COD_VAL_PREDIO 	= 	$txh_cod_val_predio;
			
			$RESULTADO			=	$objPrediosModel->ACTUALIZAR_VALORIZACION_PADRON_PREDIOS($COD_VAL_PREDIO, $ID_PREDIO_SBN, $COD_ENTIDAD, $COD_TIP_DOC_PRED, $COD_TIP_VALORIZACION, $FECHA_VALORIZACION, $COD_MONEDA_VALORIZACION, $MONTO_VALOR_TERRENO, $MONTO_VALOR_CONTRUCCION, $MONTO_VALOR_OBRA, $MONTO_VALOR_TOTAL_INMUEBLE, $TIPO_CAMBIO_TOTAL_INMUEBLE, $MONTO_VALOR_TOTAL_INMUEBLE_SOL, $COD_PREDIO_DOC_TECN, $USUARIO);
			
			$valor = ($RESULTADO) ? '1'.'[*]'.$ID_PREDIO_SBN: 0 ;
						
		}
		
		//$this->dump($txt_fecha_valorizacion);
		
		echo $valor;
	
	}
	
	
	
	public function Listar_Valorizaciones(){
		
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$xID_PREDIO_SBN = isset($_GET['sID_PREDIO_SBN']) ? $_GET['sID_PREDIO_SBN'] : '';
		
		$rsListValorizacion = $objPrediosModel->LISTAR_VALORIZACIONES_PREDIOS_X_ID_PREDIO_SBN($xID_PREDIO_SBN);
		while (odbc_fetch_row($rsListValorizacion)) {			
			
			$ArrayValorizacion = array();			
			$ArrayValorizacion['VAL_ITEM'] 				= odbc_result($rsListValorizacion, 'ITEM');
			$ArrayValorizacion['COD_VAL_PREDIO'] 				= odbc_result($rsListValorizacion, 'COD_VAL_PREDIO');
			$ArrayValorizacion['COD_TIP_VALORIZACION'] 			= utf8_encode(odbc_result($rsListValorizacion, 'COD_TIP_VALORIZACION'));
			$ArrayValorizacion['NOM_TIP_VALORIZACION'] 			= utf8_encode(odbc_result($rsListValorizacion, 'NOM_TIP_VALORIZACION'));
			$ArrayValorizacion['FECHA_VALORIZACION'] 			= cambiaf_a_normal(odbc_result($rsListValorizacion, 'FECHA_VALORIZACION'));
			$ArrayValorizacion['COD_MONEDA_VALORIZACION'] 		= utf8_encode(odbc_result($rsListValorizacion, 'COD_MONEDA_VALORIZACION'));
			$ArrayValorizacion['TXT_MONEDA'] 					= utf8_encode(odbc_result($rsListValorizacion, 'TXT_MONEDA'));
			$ArrayValorizacion['TXT_MONEDA_ABREV'] 				= odbc_result($rsListValorizacion, 'TXT_MONEDA_ABREV');
			$ArrayValorizacion['MONTO_VALOR_TERRENO'] 			= number_format(odbc_result($rsListValorizacion, 'MONTO_VALOR_TERRENO'),2);
			$ArrayValorizacion['MONTO_VALOR_CONTRUCCION'] 		= number_format(odbc_result($rsListValorizacion, 'MONTO_VALOR_CONTRUCCION'),2);
			$ArrayValorizacion['MONTO_VALOR_OBRA'] 				= number_format(odbc_result($rsListValorizacion, 'MONTO_VALOR_OBRA'),2);
			$ArrayValorizacion['MONTO_VALOR_TOTAL_INMUEBLE'] 	= number_format(odbc_result($rsListValorizacion, 'MONTO_VALOR_TOTAL_INMUEBLE'),2);
			$ArrayValorizacion['TIPO_CAMBIO_TOTAL_INMUEBLE'] 	= number_format(odbc_result($rsListValorizacion, 'TIPO_CAMBIO_TOTAL_INMUEBLE'),2);
			$ArrayValorizacion['MONTO_VALOR_TOTAL_INMUEBLE_SOL'] = number_format(odbc_result($rsListValorizacion, 'MONTO_VALOR_TOTAL_INMUEBLE_SOL'),2);
			$ID_ESTADO_VALORIZACION 							= utf8_encode(odbc_result($rsListValorizacion, 'ID_ESTADO'));
			$ArrayValorizacion['COD_PREDIO_DOC_TECN'] 			= utf8_encode(odbc_result($rsListValorizacion, 'COD_PREDIO_DOC_TECN'));
			$ArrayValorizacion['NOM_TIP_DOC'] 					= utf8_encode(odbc_result($rsListValorizacion, 'NOM_TIP_DOC'));
			$ArrayValorizacion['NOM_ARCHIVO'] 					= utf8_encode(odbc_result($rsListValorizacion, 'NOM_ARCHIVO'));
			
			$ArrayValorizacion['VAL_ID_ID_ESTADO'] 					= $ID_ESTADO_VALORIZACION;
			$ArrayValorizacion['VAL_DESC_ESTADO'] 					= ($ID_ESTADO_VALORIZACION == '1')? 'Activo':'Pendiente';
			
			
			$data['DatValorizacion'][] = $ArrayValorizacion;
						
		}
		
		$this->render('v.predio_form_pestania_P3_list.php', $data);
			
	}
	
	
	
	public function Ver_Datos_Valorizacion_x_Codigo(){
		
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$eCOD_VAL_PREDIO = isset($_GET['eCOD_VAL_PREDIO']) ? $_GET['eCOD_VAL_PREDIO'] : '';
		
		$rsDat_VAlorizacion = $objPrediosModel->VER_DATOS_VALORIZACIONES_PREDIOS_X_COD_VAL_PREDIO($eCOD_VAL_PREDIO);
		
		$data['Dat_E_Valorizacion']['E1_COD_VAL_PREDIO'] = utf8_encode(odbc_result($rsDat_VAlorizacion,"COD_VAL_PREDIO"));
		$data['Dat_E_Valorizacion']['E1_COD_TIP_VALORIZACION'] = utf8_encode(odbc_result($rsDat_VAlorizacion,"COD_TIP_VALORIZACION"));
		$data['Dat_E_Valorizacion']['E1_FECHA_VALORIZACION'] = cambiaf_a_normal(odbc_result($rsDat_VAlorizacion,"FECHA_VALORIZACION"));
		$data['Dat_E_Valorizacion']['E1_COD_MONEDA_VALORIZACION'] = utf8_encode(odbc_result($rsDat_VAlorizacion,"COD_MONEDA_VALORIZACION"));
		$data['Dat_E_Valorizacion']['E1_MONTO_VALOR_TERRENO'] = utf8_encode(odbc_result($rsDat_VAlorizacion,"MONTO_VALOR_TERRENO"));
		$data['Dat_E_Valorizacion']['E1_MONTO_VALOR_CONTRUCCION'] = utf8_encode(odbc_result($rsDat_VAlorizacion,"MONTO_VALOR_CONTRUCCION"));
		$data['Dat_E_Valorizacion']['E1_MONTO_VALOR_OBRA'] = utf8_encode(odbc_result($rsDat_VAlorizacion,"MONTO_VALOR_OBRA"));
		$data['Dat_E_Valorizacion']['E1_MONTO_VALOR_TOTAL_INMUEBLE'] = utf8_encode(odbc_result($rsDat_VAlorizacion,"MONTO_VALOR_TOTAL_INMUEBLE"));
		$data['Dat_E_Valorizacion']['E1_TIPO_CAMBIO_TOTAL_INMUEBLE'] = utf8_encode(odbc_result($rsDat_VAlorizacion,"TIPO_CAMBIO_TOTAL_INMUEBLE"));
		$data['Dat_E_Valorizacion']['E1_MONTO_VALOR_TOTAL_INMUEBLE_SOL'] = utf8_encode(odbc_result($rsDat_VAlorizacion,"MONTO_VALOR_TOTAL_INMUEBLE_SOL"));
		$data['Dat_E_Valorizacion']['E1_COD_TIP_DOC_PRED'] = utf8_encode(odbc_result($rsDat_VAlorizacion,"COD_TIP_DOC_PRED"));
		$data['Dat_E_Valorizacion']['E1_COD_PREDIO_DOC_TECN'] = utf8_encode(odbc_result($rsDat_VAlorizacion,"COD_PREDIO_DOC_TECN"));
		
		$COD_MONEDA_VALORIZACION_ED = utf8_encode(odbc_result($rsDat_VAlorizacion,"COD_MONEDA_VALORIZACION"));
	
		$RST_DAT_DOC_TECN_VAL	= $objPrediosModel->VER_DATOS_DOCUMENTOS_TECNICOS_X_CODIGO(utf8_encode(odbc_result($rsDat_VAlorizacion,"COD_PREDIO_DOC_TECN")));	
	 	$data['Dat_E_Valorizacion']['E1_NOM_ARCHIVO_Valorizacion'] = utf8_encode(odbc_result($RST_DAT_DOC_TECN_VAL,"NOM_ARCHIVO"));
		$data['Dat_E_Valorizacion']['E1_PESO_ARCHIVO_Valorizacion'] = utf8_encode(odbc_result($RST_DAT_DOC_TECN_VAL,"PESO_ARCHIVO"));
	 
		 
		if($COD_MONEDA_VALORIZACION_ED == '2'){//dolar
			$DES_MONEDA_VALORIZACION = '$.';
			
		}else if($COD_MONEDA_VALORIZACION_ED == '1'){//soles
			$DES_MONEDA_VALORIZACION = 'S/.';
			
		}else{
			$DES_MONEDA_VALORIZACION = '-';
		}
			
		$data['Dat_E_Valorizacion']['E1_DES_MONEDA_VALORIZACION'] = $DES_MONEDA_VALORIZACION;
		
		$data['DatTipoDocTasa'] 		= $this->Mostrar_Lista_Inmuebles_Tipo_Doc_X_COD_TIP_DOC_PRED('9');//DOC TASACION
		$data['DatTipoValorizacion'] 	= $this->Mostrar_Lista_Tipo_Valorizacion();
		$data['DatTipoMoneda'] 			= $this->Mostrar_Lista_Moneda();

		
		$this->render('v.predio_form_pestania_P3_cab.php', $data);
			
	}
		
	
	
	public function Guardar_Datos_Unid_Inmobiliaria(){
		
		$data = array();
		
		$objPrediosModel 	= new PrediosModel();
		
		$txh_COD_UE_LOCAL = isset($_GET['txh_COD_UE_LOCAL']) ? $_GET['txh_COD_UE_LOCAL'] : '';
		$USUARIO = isset($_GET['TXH_SIMI_COD_USUARIO']) ? $_GET['TXH_SIMI_COD_USUARIO'] : '';
		
		$TXT_COD_UNID_INMOBIL = isset($_GET['TXT_COD_UNID_INMOBIL']) ? $_GET['TXT_COD_UNID_INMOBIL'] : '';		
		$NOM_UNID_INMOBIL = isset($_GET['TXT_NOM_UNID_INMOBIL']) ? $_GET['TXT_NOM_UNID_INMOBIL'] : '';
		$AREA_UNID_INMOBIL = isset($_GET['TXT_AREA_UNID_INMOBIL']) ? $_GET['TXT_AREA_UNID_INMOBIL'] : '';
		$VALOR_AUTOVALUO = isset($_GET['TXT_AUTOVALUO_UNID_INMOBIL']) ? $_GET['TXT_AUTOVALUO_UNID_INMOBIL'] : '';
		$OBS_UNID_INMOBIL = isset($_GET['TXT_OBS_UNID_INMOBIL']) ? $_GET['TXT_OBS_UNID_INMOBIL'] : '';
		
		
		
		if($txh_COD_UE_LOCAL == ''){
			$RESULT_CODPredio = $objPrediosModel->Padron_Generar_Codigo_Padron_Predios();
			$ID_PREDIO_SBN	= 	odbc_result($RESULT_CODPredio,"CODIGO");	
			
			$objPrediosModel->Padron_Insertar_Local_Predios_Basico($ID_PREDIO_SBN, $COD_ENTIDAD, $MODULO_REGISTRO, $USUARIO);
			
		}else if($txh_COD_UE_LOCAL != ''){
			
			$ID_PREDIO_SBN = $txh_COD_UE_LOCAL;
		}
		
		
		
		if($TXT_COD_UNID_INMOBIL == ''){
						
			$RESULT = $objPrediosModel->GENERAR_CODIGO_PADRON_PREDIOS_UNID_INMOBIL();
			$COD_UNID_INMOBIL	= 	odbc_result($RESULT,"CODIGO");
			
			$RESULTADO = $objPrediosModel->INSERTAR_PADRON_PREDIOS_UNID_INMOBIL($COD_UNID_INMOBIL, $ID_PREDIO_SBN, $NOM_UNID_INMOBIL, $AREA_UNID_INMOBIL, $VALOR_AUTOVALUO, $OBS_UNID_INMOBIL, $USUARIO);
			$valor = ($RESULTADO) ? '1'.'[*]'.$ID_PREDIO_SBN: 0 ;
			
		}else{
			
			$COD_UNID_INMOBIL = $TXT_COD_UNID_INMOBIL;
			
			$RESULTADO = $objPrediosModel->ACTUALIZA_PADRON_PREDIOS_UNID_INMOBIL($COD_UNID_INMOBIL, $ID_PREDIO_SBN, $NOM_UNID_INMOBIL, $AREA_UNID_INMOBIL, $VALOR_AUTOVALUO, $OBS_UNID_INMOBIL, $USUARIO);
			$valor = ($RESULTADO) ? '1'.'[*]'.$ID_PREDIO_SBN: 0 ;
			
		}
		
		
		echo $valor;
		//$this->dump($MODULO_REGISTRO);
	
	}
	
	
	
	
	public function Lista_Registros_Unid_Inmobiliaria(){
		
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$xID_PREDIO_SBN = isset($_GET['sID_PREDIO_SBN']) ? $_GET['sID_PREDIO_SBN'] : '';
		
		$rst_regInmobil = $objPrediosModel->LISTAR_DATOS_PADRON_PREDIOS_UNID_INMOBIL($xID_PREDIO_SBN);
		while (odbc_fetch_row($rst_regInmobil)) {
			
			$ArrayRegInmobil = array();
			
			$ArrayRegInmobil['B_ITEM'] 	= utf8_encode(odbc_result($rst_regInmobil,"ITEM"));
			$ArrayRegInmobil['B_COD_UNID_INMOBIL'] 	= utf8_encode(odbc_result($rst_regInmobil,"COD_UNID_INMOBIL"));
			$ArrayRegInmobil['B_NOM_UNID_INMOBIL'] 	= utf8_encode(odbc_result($rst_regInmobil,"NOM_UNID_INMOBIL"));
			$ArrayRegInmobil['B_AREA_UNID_INMOBIL'] = utf8_encode(odbc_result($rst_regInmobil,"AREA_UNID_INMOBIL"));
			$ArrayRegInmobil['B_VALOR_AUTOVALUO'] 	= utf8_encode(odbc_result($rst_regInmobil,"VALOR_AUTOVALUO"));
			$ArrayRegInmobil['B_OBS_UNID_INMOBIL'] 	= utf8_encode(odbc_result($rst_regInmobil,"OBS_UNID_INMOBIL"));
			$id_estado_reg_inmobi 					= utf8_encode(odbc_result($rst_regInmobil,"ID_ESTADO"));
			$ArrayRegInmobil['B_ID_ESTADO'] 		= $id_estado_reg_inmobi;
			$ArrayRegInmobil['B_DESC_ESTADO_INMOBIL'] 		= ($id_estado_reg_inmobi == '1') ? 'Activo' : 'Pendiente';

			$data['DataUnidInmobiliaria'][] = $ArrayRegInmobil;
			
		}
		
		$this->render('v.predio_form_pestania_P5_list.php', $data);
			
	}
	
	
	
	public function Ver_Datos_Unidad_Inmobiliaria_x_Codigo(){
		
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$sCOD_UNID_INMOBIL = isset($_GET['sCOD_UNID_INMOBIL']) ? $_GET['sCOD_UNID_INMOBIL'] : '';
		
		$RST_DAt_unid_inmob	= $objPrediosModel->VER_DATOS_PADRON_PREDIOS_UNID_INMOBIL($sCOD_UNID_INMOBIL);
		
		$data['Dat_E_UnidInmobil']['I_COD_UNID_INMOBIL'] = utf8_encode(odbc_result($RST_DAt_unid_inmob,"COD_UNID_INMOBIL"));
		$data['Dat_E_UnidInmobil']['I_ID_PREDIO_SBN'] = utf8_encode(odbc_result($RST_DAt_unid_inmob,"ID_PREDIO_SBN"));
		
		$data['Dat_E_UnidInmobil']['I_NOM_UNID_INMOBIL'] = utf8_encode(odbc_result($RST_DAt_unid_inmob,"NOM_UNID_INMOBIL"));
		$data['Dat_E_UnidInmobil']['I_AREA_UNID_INMOBIL'] = utf8_encode(odbc_result($RST_DAt_unid_inmob,"AREA_UNID_INMOBIL"));
		$data['Dat_E_UnidInmobil']['I_VALOR_AUTOVALUO'] = utf8_encode(odbc_result($RST_DAt_unid_inmob,"VALOR_AUTOVALUO"));
		$data['Dat_E_UnidInmobil']['I_OBS_UNID_INMOBIL'] = utf8_encode(odbc_result($RST_DAt_unid_inmob,"OBS_UNID_INMOBIL"));

		$this->render('v.predio_form_pestania_P5_cab.php', $data);
			
	}
	
	
	public function Mostrar_Form_Documento(){
		
		$data = array();		
		$objPrediosModel 	= new PrediosModel();


		$txh_COD_UE_LOCAL = isset($_GET['txh_COD_UE_LOCAL']) ? $_GET['txh_COD_UE_LOCAL'] : '';
		$sCOD_TIP_DOC_PRED = isset($_GET['sCOD_TIP_DOC_PRED']) ? $_GET['sCOD_TIP_DOC_PRED'] : '';
		$sCOD_PREDIO_DOC_TECN = isset($_GET['sCOD_PREDIO_DOC_TECN']) ? $_GET['sCOD_PREDIO_DOC_TECN'] : '';
		
		$RST_DAT_TIPDOC	= $objPrediosModel->VER_TIPO_DOCUMENTO_X_CODIGO($sCOD_TIP_DOC_PRED);		
		$data['Dat_eDoc']['D_COD_TIP_DOC_PRED'] = utf8_encode(odbc_result($RST_DAT_TIPDOC,"COD_TIP_DOC_PRED"));
		$data['Dat_eDoc']['D_NOM_TIP_DOC'] 		= utf8_encode(odbc_result($RST_DAT_TIPDOC,"NOM_TIP_DOC"));
		$data['Dat_eDoc']['D_NOM_CARPETA'] 		= $this->Nombre_Carpeta($sCOD_TIP_DOC_PRED);
		$data['Dat_eDoc']['D_ID_PREDIO'] 		= $txh_COD_UE_LOCAL;
		
		$RST_DOC_PREDIO	= $objPrediosModel->VER_DATOS_DOCUMENTOS_TECNICOS_X_CODIGO($sCOD_PREDIO_DOC_TECN);	
		$data['Dat_eDoc']['E_COD_PREDIO_DOC_TECN'] = utf8_encode(odbc_result($RST_DOC_PREDIO,"COD_PREDIO_DOC_TECN"));
		$data['Dat_eDoc']['E_COD_TIP_DOC_PRED'] = utf8_encode(odbc_result($RST_DOC_PREDIO,"COD_TIP_DOC_PRED"));
		$data['Dat_eDoc']['E_DESCRIP_ARCHIVO'] = utf8_encode(odbc_result($RST_DOC_PREDIO,"DESCRIP_ARCHIVO"));
		$data['Dat_eDoc']['E_NOM_ARCHIVO'] = utf8_encode(odbc_result($RST_DOC_PREDIO,"NOM_ARCHIVO"));
		$data['Dat_eDoc']['E_PESO_ARCHIVO'] = utf8_encode(odbc_result($RST_DOC_PREDIO,"PESO_ARCHIVO"));
		$data['Dat_eDoc']['E_NOM_ARCHIVO_CAD'] = utf8_encode(odbc_result($RST_DOC_PREDIO,"NOM_ARCHIVO_CAD"));
		$data['Dat_eDoc']['E_PESO_ARCHIVO_CAD'] = utf8_encode(odbc_result($RST_DOC_PREDIO,"PESO_ARCHIVO_CAD"));
		$data['Dat_eDoc']['E_FECHA_REGISTRO'] = cambiaf_a_normal(odbc_result($RST_DOC_PREDIO,"FECHA_REGISTRO"));
		$data['Dat_eDoc']['E_NRO_PART_REG'] = utf8_encode(odbc_result($RST_DOC_PREDIO,"NRO_PART_REG"));
		$data['Dat_eDoc']['E_FECHA_TASACION'] = cambiaf_a_normal(odbc_result($RST_DOC_PREDIO,"FECHA_TASACION"));
		$data['Dat_eDoc']['E_COD_TIP_RESOL_DL'] = utf8_encode(odbc_result($RST_DOC_PREDIO,"COD_TIP_RESOL_DL"));
		$data['Dat_eDoc']['E_COD_SISTEMA_COORDENADA'] = utf8_encode(odbc_result($RST_DOC_PREDIO,"COD_SISTEMA_COORDENADA"));
		
		$data['DatTipDisposicionLegal'] = $this->Mostrar_Lista_Tipo_Dispositivo_Legal();
		
		//$this->dump($data['Dat_eDoc']);
			
		$this->render('v.predio_form_pestania_P4_form.php', $data);		
		
	}
	
	
	
	public function Registrar_Predio_Documentos(){
		
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
				
		$txtH_wID_PREDIO_SBN 		= isset($_POST['txtH_wID_PREDIO_SBN']) ? $_POST['txtH_wID_PREDIO_SBN'] : '';
		$COD_TIP_DOC_PRED 			= isset($_POST['txtH_COD_TIP_DOC']) ? $_POST['txtH_COD_TIP_DOC'] : '';
		$txtH_COD_PREDIO_DOC_TECN 	= isset($_POST['txtH_COD_PREDIO_DOC_TECN']) ? $_POST['txtH_COD_PREDIO_DOC_TECN'] : '';
		$txtH_USUARIO 				= isset($_POST['WWW_SIMI_COD_USUARIO']) ? $_POST['WWW_SIMI_COD_USUARIO'] : '';
		$COD_ENTIDAD 				= isset($_POST['WWW_SIMI_COD_ENTIDAD']) ? $_POST['WWW_SIMI_COD_ENTIDAD'] : '';
		
		$nombre_carpeta	 			= $this->Nombre_Carpeta($COD_TIP_DOC_PRED);
		
		$ID_ESTADO_PREDIO = 0;
		$MODULO_REGISTRO = 'MODULO INMUEBLES';
		
		if($txtH_wID_PREDIO_SBN == ''){
			
			//inserta predio basico
			$RESULT_CODPredio = $objPrediosModel->Padron_Generar_Codigo_Padron_Predios();
			$ID_PREDIO_SBN	= 	odbc_result($RESULT_CODPredio,"CODIGO");	
			
			$objPrediosModel->Padron_Insertar_Local_Predios_Basico($ID_PREDIO_SBN, $COD_ENTIDAD, $MODULO_REGISTRO, $txtH_USUARIO);
			
		}else if($txtH_wID_PREDIO_SBN != ''){
			
			$ID_PREDIO_SBN = $txtH_wID_PREDIO_SBN;
		}
		
		
		if($COD_TIP_DOC_PRED == '5'){
			
			$txt_indicador_part_reg			= isset($_POST['txt_indicador_part_reg']) ? $_POST['txt_indicador_part_reg'] : '';
			$txt_w_nom_archivo_part_reg		= isset($_POST['txt_w_nom_archivo_part_reg']) ? $_POST['txt_w_nom_archivo_part_reg'] : '';
			$txt_w_peso_archivo_part_reg	= isset($_POST['txt_w_peso_archivo_part_reg']) ? $_POST['txt_w_peso_archivo_part_reg'] : '';
			
			//comprobamos que sea una petición ajax
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
				//obtenemos el archivo a subir
				$file = $_FILES['txf_partida_reg']['name'];
				
				$ARCHIVO					= $_FILES['txf_partida_reg']['tmp_name'];
				$ARCHIVO_NOMBRE				= basename($_FILES['txf_partida_reg']['name']);
				$ARCHIVO_PESO				= $_FILES['txf_partida_reg']['size'];
				$ARCHIVO_TIPO				= $_FILES['txf_partida_reg']['type'];
				$ARCHIVO_EXTENSION			= extension_archivo($_FILES['txf_partida_reg']['name']);

				$UPLOADDIR='../../../../Repositorio/predios_docs_tecnicos/'.$nombre_carpeta;
				//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
		
				if($txtH_COD_PREDIO_DOC_TECN == ''){
					$RS_COD_TECN 			= $objPrediosModel->GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS();
					$COD_PREDIO_DOC_TECN	= odbc_result($RS_COD_TECN,"CODIGO");
				}else{
					$COD_PREDIO_DOC_TECN = $txtH_COD_PREDIO_DOC_TECN;
				}
				
				
				$NUEVA_CADENA				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PR_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
				$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;

		
				$DESCRIP_ARCHIVO 		= '';
				$NOM_ARCHIVO			= $ARCHIVO_NOMBRE_GENERADO;
				$PESO_ARCHIVO			= $ARCHIVO_PESO;
				$NOM_ARCHIVO_CAD		= '';
				$PESO_ARCHIVO_CAD		= '';
				$NRO_PART_REG			= isset($_POST['txt_NUM_PART_REG']) ? $_POST['txt_NUM_PART_REG'] : '';
				$FECHA_TASACION			= '';
				$COD_TIP_RESOL_DL		= '';
				$COD_SISTEMA_COORDENADA	= '';
				$USUARIO				= $txtH_USUARIO;		
				$ID_ESTADO 				= '0';	
				
		
				if ($file !='' && move_uploaded_file($ARCHIVO, $UPLOADFILE) && $txt_indicador_part_reg == '1') {//SI ADJUNTO ARCHIVO
										
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file == '' && $txt_indicador_part_reg == '') {//SI NO ADJUNTO ARCHIVO
					
					$NOM_ARCHIVO			= $txt_w_nom_archivo_part_reg;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_part_reg;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else{
					$valor = 0;	
				}
	
				//unlink($UPLOADFILE);
				//unlink('\\\\srv-info\\www$\\Asientos\\A\\AP0089_5.PDF'); //ELIMINAR ARCHIVO
				//rmdir('\\\\srv-info\\www$\\Asientos\\A'); //ELIMINAR CARPETA
		
				echo $valor;
				
			}else{
				throw new Exception("Error Processing Request", 1);   
			}
				
		}
		
		
		
		if($COD_TIP_DOC_PRED == '6'){
			
			$txt_indicador_pperimetrico_pdf			= isset($_POST['txt_indicador_pperimetrico_pdf']) ? $_POST['txt_indicador_pperimetrico_pdf'] : '';
			$txt_w_nom_archivo_pperimetric_pdf		= isset($_POST['txt_w_nom_archivo_pperimetric_pdf']) ? $_POST['txt_w_nom_archivo_pperimetric_pdf'] : '';
			$txt_w_peso_archivo_pperimetric_pdf		= isset($_POST['txt_w_peso_archivo_pperimetric_pdf']) ? $_POST['txt_w_peso_archivo_pperimetric_pdf'] : '';

			$txt_indicador_pperimetrico_cad			= isset($_POST['txt_indicador_pperimetrico_cad']) ? $_POST['txt_indicador_pperimetrico_cad'] : '';
			$txt_w_nom_archivo_pperimetric_cad		= isset($_POST['txt_w_nom_archivo_pperimetric_cad']) ? $_POST['txt_w_nom_archivo_pperimetric_cad'] : '';
			$txt_w_peso_archivo_pperimetric_cad		= isset($_POST['txt_w_peso_archivo_pperimetric_cad']) ? $_POST['txt_w_peso_archivo_pperimetric_cad'] : '';

			
			//comprobamos que sea una petición ajax
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
				
				
				if($txtH_COD_PREDIO_DOC_TECN == ''){
					$RS_COD_TECN 			= $objPrediosModel->GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS();
					$COD_PREDIO_DOC_TECN	= odbc_result($RS_COD_TECN,"CODIGO");
				}else{
					$COD_PREDIO_DOC_TECN = $txtH_COD_PREDIO_DOC_TECN;
				}
				
				$UPLOADDIR='../../../../Repositorio/predios_docs_tecnicos/'.$nombre_carpeta;
				//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
				
				
				$file_PDF 						= $_FILES['txf_partida_pperimetrico_pdf']['name'];
				
				$ARCHIVO_PDF					= $_FILES['txf_partida_pperimetrico_pdf']['tmp_name'];
				$ARCHIVO_NOMBRE_PDF				= basename($_FILES['txf_partida_pperimetrico_pdf']['name']);
				$ARCHIVO_PESO_PDF				= $_FILES['txf_partida_pperimetrico_pdf']['size'];
				$ARCHIVO_TIPO_PDF				= $_FILES['txf_partida_pperimetrico_pdf']['type'];
				$ARCHIVO_EXTENSION_PDF			= extension_archivo($_FILES['txf_partida_pperimetrico_pdf']['name']);
				
				$NUEVA_CADENA_PDF				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO_PDF	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PP_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA_PDF.'.'.$ARCHIVO_EXTENSION_PDF;
				$UPLOADFILE_PDF 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO_PDF;


				//========================================
				//ARCHIVO CAD
				//========================================
				$file_CAD 						= $_FILES['txf_partida_pperimetrico_cad']['name'];
				
				$ARCHIVO_CAD					= $_FILES['txf_partida_pperimetrico_cad']['tmp_name'];
				$ARCHIVO_NOMBRE_CAD				= basename($_FILES['txf_partida_pperimetrico_cad']['name']);
				$ARCHIVO_PESO_CAD				= $_FILES['txf_partida_pperimetrico_cad']['size'];
				$ARCHIVO_TIPO_CAD				= $_FILES['txf_partida_pperimetrico_cad']['type'];
				$ARCHIVO_EXTENSION_CAD			= extension_archivo($_FILES['txf_partida_pperimetrico_cad']['name']);
				
				$NUEVA_CADENA_CAD				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO_CAD	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PP_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA_CAD.'.'.$ARCHIVO_EXTENSION_CAD;
				$UPLOADFILE_CAD 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO_CAD;

		
				$DESCRIP_ARCHIVO 		= isset($_POST['txt_NUM_DESCRIP_PPERMETRICO']) ? $_POST['txt_NUM_DESCRIP_PPERMETRICO'] : '';
				$NOM_ARCHIVO			= $ARCHIVO_NOMBRE_GENERADO_PDF;
				$PESO_ARCHIVO			= $ARCHIVO_PESO_PDF;
				$NOM_ARCHIVO_CAD		= $ARCHIVO_NOMBRE_GENERADO_CAD;
				$PESO_ARCHIVO_CAD		= $ARCHIVO_PESO_CAD;
				$NRO_PART_REG			= '';
				$FECHA_TASACION			= '';
				$COD_TIP_RESOL_DL		= '';
				$COD_SISTEMA_COORDENADA	= '';
				$USUARIO				= $txtH_USUARIO;		
				$ID_ESTADO 				= '0';	
				
				
				if ($file_PDF != '' && $file_CAD != '' && move_uploaded_file($ARCHIVO_PDF, $UPLOADFILE_PDF) && move_uploaded_file($ARCHIVO_CAD, $UPLOADFILE_CAD) && $txt_indicador_pperimetrico_pdf == '1' && $txt_indicador_pperimetrico_cad == '1') {//SI ADJUNTO ARCHIVO PDF
										
					if($txtH_COD_PREDIO_DOC_TECN == ''){
																
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "22";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
					
				}else if ($file_PDF != '' && $file_CAD == '' && move_uploaded_file($ARCHIVO_PDF, $UPLOADFILE_PDF) && $txt_indicador_pperimetrico_pdf == '1') {//SI ADJUNTO ARCHIVO PDF
					
					$NOM_ARCHIVO_CAD	= $txt_w_nom_archivo_pperimetric_cad;
					$PESO_ARCHIVO_CAD	= $txt_w_peso_archivo_pperimetric_cad;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){
																
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "33";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
					
				}else if ($file_PDF == '' && $file_CAD != '' && move_uploaded_file($ARCHIVO_CAD, $UPLOADFILE_CAD) && $txt_indicador_pperimetrico_cad == '1') {//SI ADJUNTO ARCHIVO CAD
								
					$NOM_ARCHIVO			= $txt_w_nom_archivo_pperimetric_pdf;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_pperimetric_pdf;
							
					if($txtH_COD_PREDIO_DOC_TECN == ''){
																
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "44";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
					
				}else if ($file_PDF == '' && $file_CAD != '' && $txt_indicador_pperimetrico_pdf == '' && $txt_indicador_pperimetrico_cad == '1' ) {//SI NO ADJUNTO ARCHIVO PDF
				
					$NOM_ARCHIVO			= $txt_w_nom_archivo_pperimetric_pdf;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_pperimetric_pdf;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "55";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file_PDF != '' && $file_CAD == '' && $txt_indicador_pperimetrico_pdf == '1' && $txt_indicador_pperimetrico_cad == '' ) {//SI NO ADJUNTO ARCHIVO CAD
				
					$NOM_ARCHIVO_CAD	= $txt_w_nom_archivo_pperimetric_cad;
					$PESO_ARCHIVO_CAD	= $txt_w_peso_archivo_pperimetric_cad;
					
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "66";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file_PDF == '' && $file_CAD == '' && $txt_indicador_pperimetrico_pdf == '' && $txt_indicador_pperimetrico_cad == '' ) {//SI NO ADJUNTO ARCHIVO PDF Y CAD
				
					$NOM_ARCHIVO		= $txt_w_nom_archivo_pperimetric_pdf;
					$PESO_ARCHIVO		= $txt_w_peso_archivo_pperimetric_pdf;
					
					$NOM_ARCHIVO_CAD	= $txt_w_nom_archivo_pperimetric_cad;
					$PESO_ARCHIVO_CAD	= $txt_w_peso_archivo_pperimetric_cad;
					
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "77";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else{
					$valor = 0;	
				}
	
				//unlink($UPLOADFILE);
				//unlink('\\\\srv-info\\www$\\Asientos\\A\\AP0089_5.PDF'); //ELIMINAR ARCHIVO
				//rmdir('\\\\srv-info\\www$\\Asientos\\A'); //ELIMINAR CARPETA
		
				echo $valor;
				
			}else{
				throw new Exception("Error Processing Request", 1);   
			}
				
		}
		
		
		
		if($COD_TIP_DOC_PRED == '7'){
			
			$txt_indicador_pperimetrico_pdf			= isset($_POST['txt_indicador_pperimetrico_pdf']) ? $_POST['txt_indicador_pperimetrico_pdf'] : '';
			$txt_w_nom_archivo_pperimetric_pdf		= isset($_POST['txt_w_nom_archivo_pperimetric_pdf']) ? $_POST['txt_w_nom_archivo_pperimetric_pdf'] : '';
			$txt_w_peso_archivo_pperimetric_pdf		= isset($_POST['txt_w_peso_archivo_pperimetric_pdf']) ? $_POST['txt_w_peso_archivo_pperimetric_pdf'] : '';

			$txt_indicador_pperimetrico_cad			= isset($_POST['txt_indicador_pperimetrico_cad']) ? $_POST['txt_indicador_pperimetrico_cad'] : '';
			$txt_w_nom_archivo_pperimetric_cad		= isset($_POST['txt_w_nom_archivo_pperimetric_cad']) ? $_POST['txt_w_nom_archivo_pperimetric_cad'] : '';
			$txt_w_peso_archivo_pperimetric_cad		= isset($_POST['txt_w_peso_archivo_pperimetric_cad']) ? $_POST['txt_w_peso_archivo_pperimetric_cad'] : '';

			
			//comprobamos que sea una petición ajax
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
				
				
				if($txtH_COD_PREDIO_DOC_TECN == ''){
					$RS_COD_TECN 			= $objPrediosModel->GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS();
					$COD_PREDIO_DOC_TECN	= odbc_result($RS_COD_TECN,"CODIGO");
				}else{
					$COD_PREDIO_DOC_TECN = $txtH_COD_PREDIO_DOC_TECN;
				}
				
				$UPLOADDIR='../../../../Repositorio/predios_docs_tecnicos/'.$nombre_carpeta;
				//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
				
				
				$file_PDF 						= $_FILES['txf_partida_pperimetrico_pdf']['name'];
				
				$ARCHIVO_PDF					= $_FILES['txf_partida_pperimetrico_pdf']['tmp_name'];
				$ARCHIVO_NOMBRE_PDF				= basename($_FILES['txf_partida_pperimetrico_pdf']['name']);
				$ARCHIVO_PESO_PDF				= $_FILES['txf_partida_pperimetrico_pdf']['size'];
				$ARCHIVO_TIPO_PDF				= $_FILES['txf_partida_pperimetrico_pdf']['type'];
				$ARCHIVO_EXTENSION_PDF			= extension_archivo($_FILES['txf_partida_pperimetrico_pdf']['name']);
				
				$NUEVA_CADENA_PDF				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO_PDF	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PP_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA_PDF.'.'.$ARCHIVO_EXTENSION_PDF;
				$UPLOADFILE_PDF 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO_PDF;


				//========================================
				//ARCHIVO CAD
				//========================================
				$file_CAD 						= $_FILES['txf_partida_pperimetrico_cad']['name'];
				
				$ARCHIVO_CAD					= $_FILES['txf_partida_pperimetrico_cad']['tmp_name'];
				$ARCHIVO_NOMBRE_CAD				= basename($_FILES['txf_partida_pperimetrico_cad']['name']);
				$ARCHIVO_PESO_CAD				= $_FILES['txf_partida_pperimetrico_cad']['size'];
				$ARCHIVO_TIPO_CAD				= $_FILES['txf_partida_pperimetrico_cad']['type'];
				$ARCHIVO_EXTENSION_CAD			= extension_archivo($_FILES['txf_partida_pperimetrico_cad']['name']);
				
				$NUEVA_CADENA_CAD				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO_CAD	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PP_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA_CAD.'.'.$ARCHIVO_EXTENSION_CAD;
				$UPLOADFILE_CAD 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO_CAD;

		
				$DESCRIP_ARCHIVO 		= isset($_POST['txt_NUM_DESCRIP_PPERMETRICO']) ? $_POST['txt_NUM_DESCRIP_PPERMETRICO'] : '';
				$NOM_ARCHIVO			= $ARCHIVO_NOMBRE_GENERADO_PDF;
				$PESO_ARCHIVO			= $ARCHIVO_PESO_PDF;
				$NOM_ARCHIVO_CAD		= $ARCHIVO_NOMBRE_GENERADO_CAD;
				$PESO_ARCHIVO_CAD		= $ARCHIVO_PESO_CAD;
				$NRO_PART_REG			= '';
				$FECHA_TASACION			= '';
				$COD_TIP_RESOL_DL		= '';
				$COD_SISTEMA_COORDENADA	= '';
				$USUARIO				= $txtH_USUARIO;		
				$ID_ESTADO 				= '0';	
				
				
				//*********************************************************************
				//*********************************************************************
				//*********************************************************************
				
				if ($file_PDF != '' && $file_CAD != '' && move_uploaded_file($ARCHIVO_PDF, $UPLOADFILE_PDF) && move_uploaded_file($ARCHIVO_CAD, $UPLOADFILE_CAD) && $txt_indicador_pperimetrico_pdf == '1' && $txt_indicador_pperimetrico_cad == '1') {//SI ADJUNTO ARCHIVO PDF
										
					if($txtH_COD_PREDIO_DOC_TECN == ''){
																
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "22";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
					
				}else if ($file_PDF != '' && $file_CAD == '' && move_uploaded_file($ARCHIVO_PDF, $UPLOADFILE_PDF) && $txt_indicador_pperimetrico_pdf == '1') {//SI ADJUNTO ARCHIVO PDF
					
					$NOM_ARCHIVO_CAD	= $txt_w_nom_archivo_pperimetric_cad;
					$PESO_ARCHIVO_CAD	= $txt_w_peso_archivo_pperimetric_cad;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){
																
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "33";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
					
				}else if ($file_PDF == '' && $file_CAD != '' && move_uploaded_file($ARCHIVO_CAD, $UPLOADFILE_CAD) && $txt_indicador_pperimetrico_cad == '1') {//SI ADJUNTO ARCHIVO CAD
								
					$NOM_ARCHIVO			= $txt_w_nom_archivo_pperimetric_pdf;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_pperimetric_pdf;
							
					if($txtH_COD_PREDIO_DOC_TECN == ''){
																
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "44";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
					
				}else if ($file_PDF == '' && $file_CAD != '' && $txt_indicador_pperimetrico_pdf == '' && $txt_indicador_pperimetrico_cad == '1' ) {//SI NO ADJUNTO ARCHIVO PDF
				
					$NOM_ARCHIVO			= $txt_w_nom_archivo_pperimetric_pdf;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_pperimetric_pdf;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "55";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file_PDF != '' && $file_CAD == '' && $txt_indicador_pperimetrico_pdf == '1' && $txt_indicador_pperimetrico_cad == '' ) {//SI NO ADJUNTO ARCHIVO CAD
				
					$NOM_ARCHIVO_CAD	= $txt_w_nom_archivo_pperimetric_cad;
					$PESO_ARCHIVO_CAD	= $txt_w_peso_archivo_pperimetric_cad;
					
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "66";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file_PDF == '' && $file_CAD == '' && $txt_indicador_pperimetrico_pdf == '' && $txt_indicador_pperimetrico_cad == '' ) {//SI NO ADJUNTO ARCHIVO PDF Y CAD
				
					$NOM_ARCHIVO		= $txt_w_nom_archivo_pperimetric_pdf;
					$PESO_ARCHIVO		= $txt_w_peso_archivo_pperimetric_pdf;
					
					$NOM_ARCHIVO_CAD	= $txt_w_nom_archivo_pperimetric_cad;
					$PESO_ARCHIVO_CAD	= $txt_w_peso_archivo_pperimetric_cad;
					
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "77";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else{
					$valor = 0;	
				}
	
				//unlink($UPLOADFILE);
				//unlink('\\\\srv-info\\www$\\Asientos\\A\\AP0089_5.PDF'); //ELIMINAR ARCHIVO
				//rmdir('\\\\srv-info\\www$\\Asientos\\A'); //ELIMINAR CARPETA
		
				echo $valor;
				
			}else{
				throw new Exception("Error Processing Request", 1);   
			}
				
		}
		
		
		
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		//=======================================================
		//================ PLANO de Habilitación
		//=======================================================
		
		
		if($COD_TIP_DOC_PRED == '8'){
			
			$txt_indicador_pperimetrico_pdf			= isset($_POST['txt_indicador_pperimetrico_pdf']) ? $_POST['txt_indicador_pperimetrico_pdf'] : '';
			$txt_w_nom_archivo_pperimetric_pdf		= isset($_POST['txt_w_nom_archivo_pperimetric_pdf']) ? $_POST['txt_w_nom_archivo_pperimetric_pdf'] : '';
			$txt_w_peso_archivo_pperimetric_pdf		= isset($_POST['txt_w_peso_archivo_pperimetric_pdf']) ? $_POST['txt_w_peso_archivo_pperimetric_pdf'] : '';

			$txt_indicador_pperimetrico_cad			= isset($_POST['txt_indicador_pperimetrico_cad']) ? $_POST['txt_indicador_pperimetrico_cad'] : '';
			$txt_w_nom_archivo_pperimetric_cad		= isset($_POST['txt_w_nom_archivo_pperimetric_cad']) ? $_POST['txt_w_nom_archivo_pperimetric_cad'] : '';
			$txt_w_peso_archivo_pperimetric_cad		= isset($_POST['txt_w_peso_archivo_pperimetric_cad']) ? $_POST['txt_w_peso_archivo_pperimetric_cad'] : '';

			
			//comprobamos que sea una petición ajax
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
				
				
				if($txtH_COD_PREDIO_DOC_TECN == ''){
					$RS_COD_TECN 			= $objPrediosModel->GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS();
					$COD_PREDIO_DOC_TECN	= odbc_result($RS_COD_TECN,"CODIGO");
				}else{
					$COD_PREDIO_DOC_TECN = $txtH_COD_PREDIO_DOC_TECN;
				}
				
				$UPLOADDIR='../../../../Repositorio/predios_docs_tecnicos/'.$nombre_carpeta;
				//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
				
				
				//========================================
				//ARCHIVO PDF
				//========================================
				$file_PDF 						= $_FILES['txf_partida_pperimetrico_pdf']['name'];
				
				$ARCHIVO_PDF					= $_FILES['txf_partida_pperimetrico_pdf']['tmp_name'];
				$ARCHIVO_NOMBRE_PDF				= basename($_FILES['txf_partida_pperimetrico_pdf']['name']);
				$ARCHIVO_PESO_PDF				= $_FILES['txf_partida_pperimetrico_pdf']['size'];
				$ARCHIVO_TIPO_PDF				= $_FILES['txf_partida_pperimetrico_pdf']['type'];
				$ARCHIVO_EXTENSION_PDF			= extension_archivo($_FILES['txf_partida_pperimetrico_pdf']['name']);
				
				$NUEVA_CADENA_PDF				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO_PDF	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PP_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA_PDF.'.'.$ARCHIVO_EXTENSION_PDF;
				$UPLOADFILE_PDF 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO_PDF;


				//========================================
				//ARCHIVO CAD
				//========================================
				$file_CAD 						= $_FILES['txf_partida_pperimetrico_cad']['name'];
				
				$ARCHIVO_CAD					= $_FILES['txf_partida_pperimetrico_cad']['tmp_name'];
				$ARCHIVO_NOMBRE_CAD				= basename($_FILES['txf_partida_pperimetrico_cad']['name']);
				$ARCHIVO_PESO_CAD				= $_FILES['txf_partida_pperimetrico_cad']['size'];
				$ARCHIVO_TIPO_CAD				= $_FILES['txf_partida_pperimetrico_cad']['type'];
				$ARCHIVO_EXTENSION_CAD			= extension_archivo($_FILES['txf_partida_pperimetrico_cad']['name']);
				
				$NUEVA_CADENA_CAD				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO_CAD	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PP_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA_CAD.'.'.$ARCHIVO_EXTENSION_CAD;
				$UPLOADFILE_CAD 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO_CAD;

		
				$DESCRIP_ARCHIVO 		= isset($_POST['txt_NUM_DESCRIP_PPERMETRICO']) ? $_POST['txt_NUM_DESCRIP_PPERMETRICO'] : '';
				$NOM_ARCHIVO			= $ARCHIVO_NOMBRE_GENERADO_PDF;
				$PESO_ARCHIVO			= $ARCHIVO_PESO_PDF;
				$NOM_ARCHIVO_CAD		= $ARCHIVO_NOMBRE_GENERADO_CAD;
				$PESO_ARCHIVO_CAD		= $ARCHIVO_PESO_CAD;
				$NRO_PART_REG			= '';
				$FECHA_TASACION			= '';
				$COD_TIP_RESOL_DL		= '';
				$COD_SISTEMA_COORDENADA	= '';
				$USUARIO				= $txtH_USUARIO;		
				$ID_ESTADO 				= '0';	
				
				
				//*********************************************************************
				//*********************************************************************
				//*********************************************************************
				
				if ($file_PDF != '' && $file_CAD != '' && move_uploaded_file($ARCHIVO_PDF, $UPLOADFILE_PDF) && move_uploaded_file($ARCHIVO_CAD, $UPLOADFILE_CAD) && $txt_indicador_pperimetrico_pdf == '1' && $txt_indicador_pperimetrico_cad == '1') {//SI ADJUNTO ARCHIVO PDF
										
					if($txtH_COD_PREDIO_DOC_TECN == ''){
																
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "22";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
					
				}else if ($file_PDF != '' && $file_CAD == '' && move_uploaded_file($ARCHIVO_PDF, $UPLOADFILE_PDF) && $txt_indicador_pperimetrico_pdf == '1') {//SI ADJUNTO ARCHIVO PDF
					
					$NOM_ARCHIVO_CAD	= $txt_w_nom_archivo_pperimetric_cad;
					$PESO_ARCHIVO_CAD	= $txt_w_peso_archivo_pperimetric_cad;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){
																
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "33";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
					
				}else if ($file_PDF == '' && $file_CAD != '' && move_uploaded_file($ARCHIVO_CAD, $UPLOADFILE_CAD) && $txt_indicador_pperimetrico_cad == '1') {//SI ADJUNTO ARCHIVO CAD
								
					$NOM_ARCHIVO			= $txt_w_nom_archivo_pperimetric_pdf;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_pperimetric_pdf;
							
					if($txtH_COD_PREDIO_DOC_TECN == ''){
																
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "44";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
					
				}else if ($file_PDF == '' && $file_CAD != '' && $txt_indicador_pperimetrico_pdf == '' && $txt_indicador_pperimetrico_cad == '1' ) {//SI NO ADJUNTO ARCHIVO PDF
				
					$NOM_ARCHIVO			= $txt_w_nom_archivo_pperimetric_pdf;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_pperimetric_pdf;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "55";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file_PDF != '' && $file_CAD == '' && $txt_indicador_pperimetrico_pdf == '1' && $txt_indicador_pperimetrico_cad == '' ) {//SI NO ADJUNTO ARCHIVO CAD
				
					$NOM_ARCHIVO_CAD	= $txt_w_nom_archivo_pperimetric_cad;
					$PESO_ARCHIVO_CAD	= $txt_w_peso_archivo_pperimetric_cad;
					
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "66";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file_PDF == '' && $file_CAD == '' && $txt_indicador_pperimetrico_pdf == '' && $txt_indicador_pperimetrico_cad == '' ) {//SI NO ADJUNTO ARCHIVO PDF Y CAD
				
					$NOM_ARCHIVO		= $txt_w_nom_archivo_pperimetric_pdf;
					$PESO_ARCHIVO		= $txt_w_peso_archivo_pperimetric_pdf;
					
					$NOM_ARCHIVO_CAD	= $txt_w_nom_archivo_pperimetric_cad;
					$PESO_ARCHIVO_CAD	= $txt_w_peso_archivo_pperimetric_cad;
					
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					//echo "77";
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$NOM_ARCHIVO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else{
					$valor = 0;	
				}
	
				//unlink($UPLOADFILE);
				//unlink('\\\\srv-info\\www$\\Asientos\\A\\AP0089_5.PDF'); //ELIMINAR ARCHIVO
				//rmdir('\\\\srv-info\\www$\\Asientos\\A'); //ELIMINAR CARPETA
		
				echo $valor;
				
			}else{
				throw new Exception("Error Processing Request", 1);   
			}
				
		}
		
		
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		
		//=======================================================
		//================ Fotografias
		//=======================================================
		if($COD_TIP_DOC_PRED == '10'){
			
			$txt_indicador_part_reg			= isset($_POST['txt_indicador_part_reg']) ? $_POST['txt_indicador_part_reg'] : '';
			$txt_w_nom_archivo_part_reg		= isset($_POST['txt_w_nom_archivo_part_reg']) ? $_POST['txt_w_nom_archivo_part_reg'] : '';
			$txt_w_peso_archivo_part_reg	= isset($_POST['txt_w_peso_archivo_part_reg']) ? $_POST['txt_w_peso_archivo_part_reg'] : '';
			
			//comprobamos que sea una petición ajax
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
				//obtenemos el archivo a subir
				$file = $_FILES['txf_partida_reg']['name'];
				
				$ARCHIVO					= $_FILES['txf_partida_reg']['tmp_name'];
				$ARCHIVO_NOMBRE				= basename($_FILES['txf_partida_reg']['name']);
				$ARCHIVO_PESO				= $_FILES['txf_partida_reg']['size'];
				$ARCHIVO_TIPO				= $_FILES['txf_partida_reg']['type'];
				$ARCHIVO_EXTENSION			= extension_archivo($_FILES['txf_partida_reg']['name']);

				$UPLOADDIR='../../../../Repositorio/predios_docs_tecnicos/'.$nombre_carpeta;
				//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
		
				if($txtH_COD_PREDIO_DOC_TECN == ''){
					$RS_COD_TECN 			= $objPrediosModel->GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS();
					$COD_PREDIO_DOC_TECN	= odbc_result($RS_COD_TECN,"CODIGO");
				}else{
					$COD_PREDIO_DOC_TECN = $txtH_COD_PREDIO_DOC_TECN;
				}
				
				
				$NUEVA_CADENA				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PR_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
				$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;

		
				$DESCRIP_ARCHIVO 		= isset($_POST['txt_NUM_PART_REG']) ? $_POST['txt_NUM_PART_REG'] : '';
				$NOM_ARCHIVO			= $ARCHIVO_NOMBRE_GENERADO;
				$PESO_ARCHIVO			= $ARCHIVO_PESO;
				$NOM_ARCHIVO_CAD		= '';
				$PESO_ARCHIVO_CAD		= '';
				$NRO_PART_REG			= '';
				$FECHA_TASACION			= '';
				$COD_TIP_RESOL_DL		= '';
				$COD_SISTEMA_COORDENADA	= '';
				$USUARIO				= $txtH_USUARIO;		
				$ID_ESTADO 				= '0';	
				
		
				if ($file !='' && move_uploaded_file($ARCHIVO, $UPLOADFILE) && $txt_indicador_part_reg == '1') {//SI ADJUNTO ARCHIVO
										
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file == '' && $txt_indicador_part_reg == '') {//SI NO ADJUNTO ARCHIVO
					
					$NOM_ARCHIVO			= $txt_w_nom_archivo_part_reg;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_part_reg;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else{
					$valor = 0;	
				}
	
				//unlink($UPLOADFILE);
				//unlink('\\\\srv-info\\www$\\Asientos\\A\\AP0089_5.PDF'); //ELIMINAR ARCHIVO
				//rmdir('\\\\srv-info\\www$\\Asientos\\A'); //ELIMINAR CARPETA
		
				echo $valor;
				
			}else{
				throw new Exception("Error Processing Request", 1);   
			}
				
		}
		
		
		
		
		
		
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		
		//=======================================================
		//================ Escritura Pública
		//=======================================================
		if($COD_TIP_DOC_PRED == '11'){
			
			$txt_indicador_part_reg			= isset($_POST['txt_indicador_part_reg']) ? $_POST['txt_indicador_part_reg'] : '';
			$txt_w_nom_archivo_part_reg		= isset($_POST['txt_w_nom_archivo_part_reg']) ? $_POST['txt_w_nom_archivo_part_reg'] : '';
			$txt_w_peso_archivo_part_reg	= isset($_POST['txt_w_peso_archivo_part_reg']) ? $_POST['txt_w_peso_archivo_part_reg'] : '';
			
			//comprobamos que sea una petición ajax
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
				//obtenemos el archivo a subir
				$file = $_FILES['txf_partida_reg']['name'];
				
				$ARCHIVO					= $_FILES['txf_partida_reg']['tmp_name'];
				$ARCHIVO_NOMBRE				= basename($_FILES['txf_partida_reg']['name']);
				$ARCHIVO_PESO				= $_FILES['txf_partida_reg']['size'];
				$ARCHIVO_TIPO				= $_FILES['txf_partida_reg']['type'];
				$ARCHIVO_EXTENSION			= extension_archivo($_FILES['txf_partida_reg']['name']);

				$UPLOADDIR='../../../../Repositorio/predios_docs_tecnicos/'.$nombre_carpeta;
				//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
		
				if($txtH_COD_PREDIO_DOC_TECN == ''){
					$RS_COD_TECN 			= $objPrediosModel->GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS();
					$COD_PREDIO_DOC_TECN	= odbc_result($RS_COD_TECN,"CODIGO");
				}else{
					$COD_PREDIO_DOC_TECN = $txtH_COD_PREDIO_DOC_TECN;
				}
				
				
				$NUEVA_CADENA				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PR_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
				$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;

		
				$DESCRIP_ARCHIVO 		= isset($_POST['txt_NUM_PART_REG']) ? $_POST['txt_NUM_PART_REG'] : '';
				$NOM_ARCHIVO			= $ARCHIVO_NOMBRE_GENERADO;
				$PESO_ARCHIVO			= $ARCHIVO_PESO;
				$NOM_ARCHIVO_CAD		= '';
				$PESO_ARCHIVO_CAD		= '';
				$NRO_PART_REG			= '';
				$FECHA_TASACION			= '';
				$COD_TIP_RESOL_DL		= '';
				$COD_SISTEMA_COORDENADA	= '';
				$USUARIO				= $txtH_USUARIO;		
				$ID_ESTADO 				= '0';	
				
		
				if ($file !='' && move_uploaded_file($ARCHIVO, $UPLOADFILE) && $txt_indicador_part_reg == '1') {//SI ADJUNTO ARCHIVO
										
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file == '' && $txt_indicador_part_reg == '') {//SI NO ADJUNTO ARCHIVO
					
					$NOM_ARCHIVO			= $txt_w_nom_archivo_part_reg;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_part_reg;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else{
					$valor = 0;	
				}
	
				//unlink($UPLOADFILE);
				//unlink('\\\\srv-info\\www$\\Asientos\\A\\AP0089_5.PDF'); //ELIMINAR ARCHIVO
				//rmdir('\\\\srv-info\\www$\\Asientos\\A'); //ELIMINAR CARPETA
		
				echo $valor;
				
			}else{
				throw new Exception("Error Processing Request", 1);   
			}
				
		}
		
		
				
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		
		//=======================================================
		//================ Memoria Descriptiva
		//=======================================================
		if($COD_TIP_DOC_PRED == '12'){
			
			$txt_indicador_part_reg			= isset($_POST['txt_indicador_part_reg']) ? $_POST['txt_indicador_part_reg'] : '';
			$txt_w_nom_archivo_part_reg		= isset($_POST['txt_w_nom_archivo_part_reg']) ? $_POST['txt_w_nom_archivo_part_reg'] : '';
			$txt_w_peso_archivo_part_reg	= isset($_POST['txt_w_peso_archivo_part_reg']) ? $_POST['txt_w_peso_archivo_part_reg'] : '';
			
			//comprobamos que sea una petición ajax
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
				//obtenemos el archivo a subir
				$file = $_FILES['txf_partida_reg']['name'];
				
				$ARCHIVO					= $_FILES['txf_partida_reg']['tmp_name'];
				$ARCHIVO_NOMBRE				= basename($_FILES['txf_partida_reg']['name']);
				$ARCHIVO_PESO				= $_FILES['txf_partida_reg']['size'];
				$ARCHIVO_TIPO				= $_FILES['txf_partida_reg']['type'];
				$ARCHIVO_EXTENSION			= extension_archivo($_FILES['txf_partida_reg']['name']);

				$UPLOADDIR='../../../../Repositorio/predios_docs_tecnicos/'.$nombre_carpeta;
				//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
		
				if($txtH_COD_PREDIO_DOC_TECN == ''){
					$RS_COD_TECN 			= $objPrediosModel->GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS();
					$COD_PREDIO_DOC_TECN	= odbc_result($RS_COD_TECN,"CODIGO");
				}else{
					$COD_PREDIO_DOC_TECN = $txtH_COD_PREDIO_DOC_TECN;
				}
				
				
				$NUEVA_CADENA				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PR_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
				$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;

		
				$DESCRIP_ARCHIVO 		= isset($_POST['txt_NUM_PART_REG']) ? $_POST['txt_NUM_PART_REG'] : '';
				$NOM_ARCHIVO			= $ARCHIVO_NOMBRE_GENERADO;
				$PESO_ARCHIVO			= $ARCHIVO_PESO;
				$NOM_ARCHIVO_CAD		= '';
				$PESO_ARCHIVO_CAD		= '';
				$NRO_PART_REG			= '';
				$FECHA_TASACION			= '';
				$COD_TIP_RESOL_DL		= '';
				$COD_SISTEMA_COORDENADA	= '';
				$USUARIO				= $txtH_USUARIO;		
				$ID_ESTADO 				= '0';	
				
		
				if ($file !='' && move_uploaded_file($ARCHIVO, $UPLOADFILE) && $txt_indicador_part_reg == '1') {//SI ADJUNTO ARCHIVO
										
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file == '' && $txt_indicador_part_reg == '') {//SI NO ADJUNTO ARCHIVO
					
					$NOM_ARCHIVO			= $txt_w_nom_archivo_part_reg;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_part_reg;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else{
					$valor = 0;	
				}
	
				//unlink($UPLOADFILE);
				//unlink('\\\\srv-info\\www$\\Asientos\\A\\AP0089_5.PDF'); //ELIMINAR ARCHIVO
				//rmdir('\\\\srv-info\\www$\\Asientos\\A'); //ELIMINAR CARPETA
		
				echo $valor;
				
			}else{
				throw new Exception("Error Processing Request", 1);   
			}
				
		}
		
		
		
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		
		//=======================================================
		//================ Titulo Propiedad
		//=======================================================
		if($COD_TIP_DOC_PRED == '13'){
			
			$txt_indicador_part_reg			= isset($_POST['txt_indicador_part_reg']) ? $_POST['txt_indicador_part_reg'] : '';
			$txt_w_nom_archivo_part_reg		= isset($_POST['txt_w_nom_archivo_part_reg']) ? $_POST['txt_w_nom_archivo_part_reg'] : '';
			$txt_w_peso_archivo_part_reg	= isset($_POST['txt_w_peso_archivo_part_reg']) ? $_POST['txt_w_peso_archivo_part_reg'] : '';
			
			//comprobamos que sea una petición ajax
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
				//obtenemos el archivo a subir
				$file = $_FILES['txf_partida_reg']['name'];
				
				$ARCHIVO					= $_FILES['txf_partida_reg']['tmp_name'];
				$ARCHIVO_NOMBRE				= basename($_FILES['txf_partida_reg']['name']);
				$ARCHIVO_PESO				= $_FILES['txf_partida_reg']['size'];
				$ARCHIVO_TIPO				= $_FILES['txf_partida_reg']['type'];
				$ARCHIVO_EXTENSION			= extension_archivo($_FILES['txf_partida_reg']['name']);

				$UPLOADDIR='../../../../Repositorio/predios_docs_tecnicos/'.$nombre_carpeta;
				//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
		
				if($txtH_COD_PREDIO_DOC_TECN == ''){
					$RS_COD_TECN 			= $objPrediosModel->GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS();
					$COD_PREDIO_DOC_TECN	= odbc_result($RS_COD_TECN,"CODIGO");
				}else{
					$COD_PREDIO_DOC_TECN = $txtH_COD_PREDIO_DOC_TECN;
				}
				
				
				$NUEVA_CADENA				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PR_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
				$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;

		
				$DESCRIP_ARCHIVO 		= isset($_POST['txt_NUM_PART_REG']) ? $_POST['txt_NUM_PART_REG'] : '';
				$NOM_ARCHIVO			= $ARCHIVO_NOMBRE_GENERADO;
				$PESO_ARCHIVO			= $ARCHIVO_PESO;
				$NOM_ARCHIVO_CAD		= '';
				$PESO_ARCHIVO_CAD		= '';
				$NRO_PART_REG			= '';
				$FECHA_TASACION			= '';
				$COD_TIP_RESOL_DL		= '';
				$COD_SISTEMA_COORDENADA	= '';
				$USUARIO				= $txtH_USUARIO;		
				$ID_ESTADO 				= '0';	
				
		
				if ($file !='' && move_uploaded_file($ARCHIVO, $UPLOADFILE) && $txt_indicador_part_reg == '1') {//SI ADJUNTO ARCHIVO
										
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file == '' && $txt_indicador_part_reg == '') {//SI NO ADJUNTO ARCHIVO
					
					$NOM_ARCHIVO			= $txt_w_nom_archivo_part_reg;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_part_reg;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else{
					$valor = 0;	
				}
	
				//unlink($UPLOADFILE);
				//unlink('\\\\srv-info\\www$\\Asientos\\A\\AP0089_5.PDF'); //ELIMINAR ARCHIVO
				//rmdir('\\\\srv-info\\www$\\Asientos\\A'); //ELIMINAR CARPETA
		
				echo $valor;
				
			}else{
				throw new Exception("Error Processing Request", 1);   
			}
				
		}
		
		
		
		
		
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		
		//=======================================================
		//================ Informe Tecnico Legal
		//=======================================================
		if($COD_TIP_DOC_PRED == '14'){
			
			$txt_indicador_part_reg			= isset($_POST['txt_indicador_part_reg']) ? $_POST['txt_indicador_part_reg'] : '';
			$txt_w_nom_archivo_part_reg		= isset($_POST['txt_w_nom_archivo_part_reg']) ? $_POST['txt_w_nom_archivo_part_reg'] : '';
			$txt_w_peso_archivo_part_reg	= isset($_POST['txt_w_peso_archivo_part_reg']) ? $_POST['txt_w_peso_archivo_part_reg'] : '';
			
			//comprobamos que sea una petición ajax
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
				//obtenemos el archivo a subir
				$file = $_FILES['txf_partida_reg']['name'];
				
				$ARCHIVO					= $_FILES['txf_partida_reg']['tmp_name'];
				$ARCHIVO_NOMBRE				= basename($_FILES['txf_partida_reg']['name']);
				$ARCHIVO_PESO				= $_FILES['txf_partida_reg']['size'];
				$ARCHIVO_TIPO				= $_FILES['txf_partida_reg']['type'];
				$ARCHIVO_EXTENSION			= extension_archivo($_FILES['txf_partida_reg']['name']);

				$UPLOADDIR='../../../../Repositorio/predios_docs_tecnicos/'.$nombre_carpeta;
				//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
		
				if($txtH_COD_PREDIO_DOC_TECN == ''){
					$RS_COD_TECN 			= $objPrediosModel->GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS();
					$COD_PREDIO_DOC_TECN	= odbc_result($RS_COD_TECN,"CODIGO");
				}else{
					$COD_PREDIO_DOC_TECN = $txtH_COD_PREDIO_DOC_TECN;
				}
				
				
				$NUEVA_CADENA				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PR_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
				$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;

		
				$DESCRIP_ARCHIVO 		= isset($_POST['txt_NUM_PART_REG']) ? $_POST['txt_NUM_PART_REG'] : '';
				$NOM_ARCHIVO			= $ARCHIVO_NOMBRE_GENERADO;
				$PESO_ARCHIVO			= $ARCHIVO_PESO;
				$NOM_ARCHIVO_CAD		= '';
				$PESO_ARCHIVO_CAD		= '';
				$NRO_PART_REG			= '';
				$FECHA_TASACION			= '';
				$COD_TIP_RESOL_DL		= '';
				$COD_SISTEMA_COORDENADA	= '';
				$USUARIO				= $txtH_USUARIO;		
				$ID_ESTADO 				= '0';	
				
		
				if ($file !='' && move_uploaded_file($ARCHIVO, $UPLOADFILE) && $txt_indicador_part_reg == '1') {//SI ADJUNTO ARCHIVO
										
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file == '' && $txt_indicador_part_reg == '') {//SI NO ADJUNTO ARCHIVO
					
					$NOM_ARCHIVO			= $txt_w_nom_archivo_part_reg;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_part_reg;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else{
					$valor = 0;	
				}
	
				//unlink($UPLOADFILE);
				//unlink('\\\\srv-info\\www$\\Asientos\\A\\AP0089_5.PDF'); //ELIMINAR ARCHIVO
				//rmdir('\\\\srv-info\\www$\\Asientos\\A'); //ELIMINAR CARPETA
		
				echo $valor;
				
			}else{
				throw new Exception("Error Processing Request", 1);   
			}
				
		}
		
		
		
		
		
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		
		//=======================================================
		//================  	Tipo Dispostivo Legal
		//=======================================================
		if($COD_TIP_DOC_PRED == '15'){
			
			$txt_indicador_part_reg			= isset($_POST['txt_indicador_part_reg']) ? $_POST['txt_indicador_part_reg'] : '';
			$txt_w_nom_archivo_part_reg		= isset($_POST['txt_w_nom_archivo_part_reg']) ? $_POST['txt_w_nom_archivo_part_reg'] : '';
			$txt_w_peso_archivo_part_reg	= isset($_POST['txt_w_peso_archivo_part_reg']) ? $_POST['txt_w_peso_archivo_part_reg'] : '';
			
			
			
			//comprobamos que sea una petición ajax
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
				//obtenemos el archivo a subir
				$file = $_FILES['txf_partida_reg']['name'];
				
				$ARCHIVO					= $_FILES['txf_partida_reg']['tmp_name'];
				$ARCHIVO_NOMBRE				= basename($_FILES['txf_partida_reg']['name']);
				$ARCHIVO_PESO				= $_FILES['txf_partida_reg']['size'];
				$ARCHIVO_TIPO				= $_FILES['txf_partida_reg']['type'];
				$ARCHIVO_EXTENSION			= extension_archivo($_FILES['txf_partida_reg']['name']);

				$UPLOADDIR='../../../../Repositorio/predios_docs_tecnicos/'.$nombre_carpeta;
				//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
		
				if($txtH_COD_PREDIO_DOC_TECN == ''){
					$RS_COD_TECN 			= $objPrediosModel->GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS();
					$COD_PREDIO_DOC_TECN	= odbc_result($RS_COD_TECN,"CODIGO");
				}else{
					$COD_PREDIO_DOC_TECN = $txtH_COD_PREDIO_DOC_TECN;
				}
				
				
				$NUEVA_CADENA				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PR_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
				$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;
								

				$DESCRIP_ARCHIVO 		= isset($_POST['txt_NUM_PART_REG']) ? $_POST['txt_NUM_PART_REG'] : '';
				$NOM_ARCHIVO			= $ARCHIVO_NOMBRE_GENERADO;
				$PESO_ARCHIVO			= $ARCHIVO_PESO;
				$NOM_ARCHIVO_CAD		= '';
				$PESO_ARCHIVO_CAD		= '';
				$NRO_PART_REG			= '';
				$FECHA_TASACION			= '';
				$COD_TIP_RESOL_DL		= isset($_POST['CBO_tip_dispos_legal']) ? $_POST['CBO_tip_dispos_legal'] : '';
				$COD_SISTEMA_COORDENADA	= '';
				$USUARIO				= $txtH_USUARIO;		
				$ID_ESTADO 				= '0';	
				
		
				if ($file !='' && move_uploaded_file($ARCHIVO, $UPLOADFILE) && $txt_indicador_part_reg == '1') {//SI ADJUNTO ARCHIVO
										
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file == '' && $txt_indicador_part_reg == '') {//SI NO ADJUNTO ARCHIVO
					
					$NOM_ARCHIVO			= $txt_w_nom_archivo_part_reg;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_part_reg;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else{
					$valor = 0;	
				}
	
				//unlink($UPLOADFILE);
				//unlink('\\\\srv-info\\www$\\Asientos\\A\\AP0089_5.PDF'); //ELIMINAR ARCHIVO
				//rmdir('\\\\srv-info\\www$\\Asientos\\A'); //ELIMINAR CARPETA
		
				echo $valor;
				
			}else{
				throw new Exception("Error Processing Request", 1);   
			}
				
		}
		
		
		
		
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		
		//=======================================================
		//================  Sistema de Coordenadas dxg
		//=======================================================
		if($COD_TIP_DOC_PRED == '16'){
			
			$txt_indicador_part_reg			= isset($_POST['txt_indicador_part_reg']) ? $_POST['txt_indicador_part_reg'] : '';
			$txt_w_nom_archivo_part_reg		= isset($_POST['txt_w_nom_archivo_part_reg']) ? $_POST['txt_w_nom_archivo_part_reg'] : '';
			$txt_w_peso_archivo_part_reg	= isset($_POST['txt_w_peso_archivo_part_reg']) ? $_POST['txt_w_peso_archivo_part_reg'] : '';
			
			
			//comprobamos que sea una petición ajax
			if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
	
				//obtenemos el archivo a subir
				$file = $_FILES['txf_partida_reg']['name'];
				
				$ARCHIVO					= $_FILES['txf_partida_reg']['tmp_name'];
				$ARCHIVO_NOMBRE				= basename($_FILES['txf_partida_reg']['name']);
				$ARCHIVO_PESO				= $_FILES['txf_partida_reg']['size'];
				$ARCHIVO_TIPO				= $_FILES['txf_partida_reg']['type'];
				$ARCHIVO_EXTENSION			= extension_archivo($_FILES['txf_partida_reg']['name']);

				$UPLOADDIR='../../../../Repositorio/predios_docs_tecnicos/'.$nombre_carpeta;
				//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
		
				if($txtH_COD_PREDIO_DOC_TECN == ''){
					$RS_COD_TECN 			= $objPrediosModel->GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS();
					$COD_PREDIO_DOC_TECN	= odbc_result($RS_COD_TECN,"CODIGO");
				}else{
					$COD_PREDIO_DOC_TECN = $txtH_COD_PREDIO_DOC_TECN;
				}
				
				
				//echo $UPLOADFILE;
				
				$NUEVA_CADENA				= date('Y').date('m').date('d').'_'.substr( md5(microtime()), 1, 4);
				$ARCHIVO_NOMBRE_GENERADO	= $nombre_carpeta.'_'.$COD_TIP_DOC_PRED.'_PR_'.$ID_PREDIO_SBN.'_DT_'.$COD_PREDIO_DOC_TECN.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
				$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;

				$DESCRIP_ARCHIVO 		= '';
				$NOM_ARCHIVO			= $ARCHIVO_NOMBRE_GENERADO;
				$PESO_ARCHIVO			= $ARCHIVO_PESO;
				$NOM_ARCHIVO_CAD		= '';
				$PESO_ARCHIVO_CAD		= '';
				$NRO_PART_REG			= '';
				$FECHA_TASACION			= '';
				$COD_TIP_RESOL_DL		= '';
				$COD_SISTEMA_COORDENADA	= isset($_POST['cbo_sist_coordenadas']) ? $_POST['cbo_sist_coordenadas'] : '';
				$USUARIO				= $txtH_USUARIO;		
				$ID_ESTADO 				= '0';	
				
		
				if ($file !='' && move_uploaded_file($ARCHIVO, $UPLOADFILE) && $txt_indicador_part_reg == '1') {//SI ADJUNTO ARCHIVO
										
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else if ($file == '' && $txt_indicador_part_reg == '') {//SI NO ADJUNTO ARCHIVO
					
					$NOM_ARCHIVO			= $txt_w_nom_archivo_part_reg;
					$PESO_ARCHIVO			= $txt_w_peso_archivo_part_reg;
					
					if($txtH_COD_PREDIO_DOC_TECN == ''){											
						$RESULT2	=	$objPrediosModel->INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}else{
						$RESULT2	=	$objPrediosModel->ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO);
						
					}
					
					$valor = ($RESULT2) ? '1'.'[*]'.$COD_PREDIO_DOC_TECN.'[*]'.$ARCHIVO_NOMBRE_GENERADO.'[*]'.$nombre_carpeta.'[*]'.$ID_PREDIO_SBN : 0 ;
					
				}else{
					$valor = 0;	
				}
				
				if($RESULT2){
					$objPrediosModel->Actualizar_Estado_TBL_CONTROL_PREDIO($ID_PREDIO_SBN);
				}
	
				//unlink($UPLOADFILE);
				//unlink('\\\\srv-info\\www$\\Asientos\\A\\AP0089_5.PDF'); //ELIMINAR ARCHIVO
				//rmdir('\\\\srv-info\\www$\\Asientos\\A'); //ELIMINAR CARPETA
		
				echo $valor;
				
			}else{
				throw new Exception("Error Processing Request", 1);   
			}
				
		}
		
		
		
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
		
		
		
		
		
	}
	
	
	
	
	
	public function Listar_Registro_Documentos_registrados(){
		
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$xID_PREDIO_SBN = isset($_GET['sID_PREDIO_SBN']) ? $_GET['sID_PREDIO_SBN'] : '';
		
		$data['DataPartidaRegistral'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataPartidaRegistral', $xID_PREDIO_SBN, '5');
		$data['DataPlanoPerimetrico'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataPlanoPerimetrico', $xID_PREDIO_SBN, '6');
		$data['DataPlanoUbicacion'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataPlanoUbicacion', $xID_PREDIO_SBN, '7');
		$data['DataPlanoHabilitacion'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataPlanoHabilitacion', $xID_PREDIO_SBN, '8');
		$data['DataFotografias'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataFotografias', $xID_PREDIO_SBN, '10');
		$data['DataEscrituraPublica'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataEscrituraPublica', $xID_PREDIO_SBN, '11');
		$data['DataMemoriaDescriptiva'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataMemoriaDescriptiva', $xID_PREDIO_SBN, '12');
		$data['DataTituloPropiedad'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataTituloPropiedad', $xID_PREDIO_SBN, '13');
		$data['DataInformeTecnicoLegal'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataInformeTecnicoLegal', $xID_PREDIO_SBN, '14');
		$data['DataDispositivoLegal'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataDispositivoLegal', $xID_PREDIO_SBN, '15');
		$data['DataPlanoPerimetricoAutoCAD'] = $this->Mostrar_Lista_de_Documentacion_Registrados_x_Tipo('DataPlanoPerimetricoAutoCAD', $xID_PREDIO_SBN, '16');
		
			
		$this->render('v.predio_form_pestania_P4_list.php', $data);		
		
	}
	
	
	public function Eliminar_Valorizacion_x_Codigo(){
		
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$sID_PREDIO_SBN = isset($_GET['sID_PREDIO_SBN']) ? $_GET['sID_PREDIO_SBN'] : '';
		$E1_COD_VAL_PREDIO = isset($_GET['E1_COD_VAL_PREDIO']) ? $_GET['E1_COD_VAL_PREDIO'] : '';
		$E1_COD_PREDIO_DOC_TECN = isset($_GET['E1_COD_PREDIO_DOC_TECN']) ? $_GET['E1_COD_PREDIO_DOC_TECN'] : '';
		
								
		$RESULT1 = $objPrediosModel->ELIMINAR_VALORIZACIONES_PREDIOS_X_CODIGO($E1_COD_VAL_PREDIO);
		
		if($RESULT1){
			$RESULT2 = $objPrediosModel->Eliminar_Documento_Tecnico($E1_COD_PREDIO_DOC_TECN, '');
			$valor = ($RESULT2) ? '1' : '0';
		}else{
			$valor = 0;
		}

			echo $valor;
		
	}
	
	
	public function Eliminar_Reg_Inmobiliaria_x_Codigo(){
		
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$COD_UNID_INMOBIL = isset($_GET['sCOD_UNID_INMOBIL']) ? $_GET['sCOD_UNID_INMOBIL'] : '';
								
		$RESULT2 = $objPrediosModel->ELIMINAR_UNID_INMOBIL($COD_UNID_INMOBIL);
		$valor = ($RESULT2) ? '1' : '0';
		
		echo $valor;
		
	}
	
	
	public function Dar_Baja_predio(){
		
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$BAJ_COD_USER = isset($_GET['TXH_SIMI_COD_USUARIO']) ? $_GET['TXH_SIMI_COD_USUARIO'] : '';
		$BAJ_COD_ENTIDAD = isset($_GET['TXH_SIMI_COD_ENTIDAD']) ? $_GET['TXH_SIMI_COD_ENTIDAD'] : '';
		$BAJ_ID_PREDIO_SBN = isset($_GET['txh_COD_PREDIO_LOCAL']) ? $_GET['txh_COD_PREDIO_LOCAL'] : '';
								
		$RESULT2 = $objPrediosModel->Dar_Baja_Predio_Local($BAJ_COD_USER, $BAJ_COD_ENTIDAD, $BAJ_ID_PREDIO_SBN);
		$valor = ($RESULT2) ? '1' : '0';
		
		echo $valor;
		
	}
	
	public function Registrar_Enviar_Predio(){
		
		$data = array();		
		$objPrediosModel 	= new PrediosModel();
		
		$sCOD_USUARIO = isset($_GET['TXH_SIMI_COD_USUARIO']) ? $_GET['TXH_SIMI_COD_USUARIO'] : '';
		$sID_PREDIO_SBN = isset($_GET['sID_PREDIO_SBN']) ? $_GET['sID_PREDIO_SBN'] : '';
								
		$objPrediosModel->Enviar_Predio_SBN($sID_PREDIO_SBN, $sCOD_USUARIO);
		
		$RS_ID_CONTROL_PREDIO = $objPrediosModel->Obtener_ID_CONTROL_PREDIO_Predio_Hitoria($sID_PREDIO_SBN);
		$ID_CONTROL_PREDIO	= 	odbc_result($RS_ID_CONTROL_PREDIO,"ID_CONTROL_PREDIO");	
		
		$OBSERVACION = utf8_decode('SE ENVIÓ INFORMACIÓN DESDE EL SINABIP - MÓDULO INMUEBLES');		
		$TIPO_MODULO = 'I';//INMUEBLE
		
		$RS_DET = $objPrediosModel->TOT_REG_TBL_CONTROL_PREDIO_OBS($sID_PREDIO_SBN);
		$TOT_REG	= 	odbc_result($RS_DET,"TOT_REG");
		
		if($TOT_REG>0){
			$TIPO_ESTADO = 'S';//SUBSANADO
		}else{
			$TIPO_ESTADO = 'N';//NUEVO
		}
		
		$RESULT2 = $objPrediosModel->REGISTRAR_TBL_CONTROL_PREDIO_OBS($OBSERVACION, $ID_CONTROL_PREDIO, $TIPO_ESTADO, $sCOD_USUARIO, $TIPO_MODULO);
		$valor = ($RESULT2) ? '1' : '0';
		
		echo $valor;
		
	}
	
	
	
	public function Imprimir_Locales_PDF(){
	
		$data = array();
		$objPrediosModel 	= new PrediosModel();
		
		$COD_ENTIDAD 	= isset($_GET['xCE']) ? ($_GET['xCE']) : '';
		
		if(is_numeric($COD_ENTIDAD)){
					
			$RESULTADO = $objPrediosModel->VER_DATOS_ENTIDAD_X_CODIGO($COD_ENTIDAD);
			$XNOM_ENTIDAD = utf8_encode(odbc_result($RESULTADO, 'NOM_ENTIDAD'));
			
			$pdf = new PDF_PrediosMuebles("L", "mm", "A4"); //Horizontal
			//$pdf = new AltasBienMueblesPDF("P", "mm", "A4"); //Vertical
			$pdf->AliasNbPages();
			
			$pdf->AddPage();
			
			$pdf->Ln(1);
			

			
			//****************************************
			// NOMBRE DE LA ENTIDAD
			//****************************************
					
			$pdf->SetFillColor(205,205,205);//color de fondo tabla
			$pdf->SetTextColor(10);
			$pdf->SetDrawColor(153,153,153);
			$pdf->SetLineWidth(.3);
			$pdf->SetFont('Arial','B',9);
			
			$pdf->SetFillColor(205,205,205);
			$pdf->Cell(50,8,utf8_decode('Nombre de la Entidad'),1,0,'L',true);		
			$pdf->SetFillColor(255,255,255);	
			$pdf->Cell(225,8,utf8_decode($XNOM_ENTIDAD),1,0,'L',true);
			
			$pdf->Ln(5);
			$pdf->Ln();
		
			
			// Datos
			$fill = false;
			
			
			$contador = 0;
			
			$RSTDatosPredio_L = $objPrediosModel->Lista_Predios_Locales_Detallado_x_entidad($COD_ENTIDAD);
			
			while (odbc_fetch_row($RSTDatosPredio_L)){
	
				$DENOMINACION_PREDIO		= utf8_encode(odbc_result($RSTDatosPredio_L,"DENOMINACION_PREDIO"));
				$DEPARTAMENTO		= utf8_encode(odbc_result($RSTDatosPredio_L,"DEPARTAMENTO"));
				$PROVINCIA		= utf8_encode(odbc_result($RSTDatosPredio_L,"PROVINCIA"));
				$DISTRITO		= utf8_encode(odbc_result($RSTDatosPredio_L,"DISTRITO"));
				$TXT_TVIA		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"TXT_TVIA")));
				$NOMBRE_VIA		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"NOMBRE_VIA")));
				$NRO_PRED		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"NRO_PRED")));
				
				$MZA_PRED		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"MZA_PRED")));
				$LTE_PRED		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"LTE_PRED")));
				
				$DIRECCION = $TXT_TVIA.' '.$NOMBRE_VIA.' '.$NRO_PRED;
				
				$TIPO_THABILITACION		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"TXT_THABILITACION")));
				$NOM_THABILITACION		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"NOM_THABILITACION")));
				$NOM_SECTOR		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"NOM_SECTOR")));
				$TIPO_PROPIEDAD		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"DESC_TIP_PROPIEDAD")));
				
				$CUS		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"CUS")));
				$RSINABIP		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"RSINABIP")));
				$NOM_PROP_REGISTRAL		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"NOM_PROP_REGISTRAL")));				
				$NOM_OFICINA_REGISTRAL		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"NOM_OFICINA_REGISTRAL")));
				$PARTIDA_ELECTRONICA		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"PARTIDA_ELECTRONICA")));
				$CODIGO_PREDIO		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"CODIGO_PREDIO")));
				$AREA_REGISTRAL		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"AREA_REGISTRAL")));
				$ABREV_TIP_UNID_MED		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"ABREV_TIP_UNID_MED")));
				$TOMO		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"TOMO")));
				$ASIENTO		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"ASIENTO")));
				$FOJAS		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"FOJAS")));
				$FICHA		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"FICHA")));
				$ANOTACION_PREVENTIVA		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"ANOTACION_PREVENTIVA")));
				
				$ZONIFICACION		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"ZONIFICACION")));
				$NOM_TIP_TERRENO	= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"NOM_TIP_TERRENO")));
				$PERIMETRO			= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"PERIMETRO")));
				$AREA_TERRENO		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"AREA_TERRENO")));
				$TXT_CONSERVACION	= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"TXT_CONSERVACION")));
				$NUMERO_PISOS		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"NUMERO_PISOS")));
				$TXT_ESANEAMIENTO	= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"TXT_ESANEAMIENTO")));
				$CODIGO_DETALLE_ESANEAMIENTO		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"CODIGO_DETALLE_ESANEAMIENTO")));
				$NOMBRE_USO_GENERICO	= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"NOMBRE_USO_GENERICO")));
				$OTROS_USO_PREDIO		= trim(utf8_encode(odbc_result($RSTDatosPredio_L,"OTROS_USO_PREDIO")));
				
				$contador++;
				
				if($contador == 3){
					$pdf->AddPage();
					$linea = 0;
				}else{
					$linea++;
				}
							
				if($contador > 3 && $linea!= 0 && $linea%2==0 ){
					$pdf->AddPage();
				 }
		
				$pdf->SetFont('Arial','B',8);
				$pdf->SetFillColor(205,205,205);
		
				$pdf->Cell(10,6,$contador,1,0,'C',true);
				$pdf->Cell(265,6,$DENOMINACION_PREDIO,1,0,'L',true);
				
				$pdf->Ln();
				
				$pdf->SetFillColor(255,255,255);
				$pdf->SetFont('Arial','',8);
				
				$pdf->Cell(25,6,utf8_decode('Departamento'),1,0,'L',true);
				$pdf->Cell(65,6,utf8_decode($DEPARTAMENTO),1,0,'L',false);
				
				$pdf->Cell(25,6,utf8_decode('Provincia'),1,0,'L',true);
				$pdf->Cell(65,6,utf8_decode($PROVINCIA),1,0,'L',true);
				
				$pdf->Cell(25,6,utf8_decode('Distrito'),1,0,'L',true);
				$pdf->Cell(70,6,utf8_decode($DISTRITO),1,0,'L',true);
				
				$pdf->Ln();
				
				$pdf->Cell(25,6,utf8_decode('Dirección'),1,0,'L',true);
				$pdf->Cell(190,6,$DIRECCION,1,0,'L',true);
				
				$pdf->Cell(15,6,utf8_decode('Mz.'),1,0,'L',true);
				$pdf->Cell(15,6,$MZA_PRED,1,0,'C',true);
				
				$pdf->Cell(15,6,utf8_decode('Lte.'),1,0,'L',true);
				$pdf->Cell(15,6,$LTE_PRED,1,0,'C',true);
				
				$pdf->Ln();
				
				$pdf->Cell(25,6,utf8_decode('Tipo Habilitación'),1,0,'L',true);
				$pdf->Cell(50,6,utf8_decode($TIPO_THABILITACION),1,0,'L',false);
				
				$pdf->Cell(30,6,utf8_decode('Nombre Habilitación'),1,0,'L',true);
				$pdf->Cell(40,6,utf8_decode($NOM_THABILITACION),1,0,'L',true);
				
				$pdf->Cell(12,6,utf8_decode('Sector'),1,0,'L',true);
				$pdf->Cell(30,6,utf8_decode($NOM_SECTOR),1,0,'L',true);
				
				$pdf->Cell(26,6,utf8_decode('Tipo de Propiedad'),1,0,'L',true);
				$pdf->Cell(62,6,utf8_decode($TIPO_PROPIEDAD),1,0,'L',true);
				
				$pdf->Ln();
				
				$pdf->SetFillColor(205,205,205);
				$pdf->Cell(275,6,utf8_decode('DATOS REGISTRALES'),1,0,'C',true);
				
				$pdf->Ln();
				
				// Restauración de colores y fuentes
				$pdf->SetFillColor(255,255,255);
				
				$pdf->Cell(25,6,utf8_decode('CUS'),1,0,'L',true);
				$pdf->Cell(25,6,utf8_decode($CUS),1,0,'L',false);
				
				$pdf->Cell(25,6,utf8_decode('Reg. SINABIP'),1,0,'L',true);
				$pdf->Cell(30,6,utf8_decode($RSINABIP),1,0,'L',true);
				
				$pdf->Cell(35,6,utf8_decode('Propietario Registral'),1,0,'L',true);
				$pdf->Cell(135,6,utf8_decode($NOM_PROP_REGISTRAL),1,0,'L',true);
				
				$pdf->Ln();
				
				$pdf->Cell(25,6,utf8_decode('Oficina Registral'),1,0,'L',true);
				$pdf->Cell(60,6,utf8_decode($NOM_OFICINA_REGISTRAL),1,0,'L',false);
				
				$pdf->Cell(30,6,utf8_decode('Partida Electrónica'),1,0,'L',true);
				$pdf->Cell(25,6,utf8_decode($PARTIDA_ELECTRONICA),1,0,'L',true);
				
				$pdf->Cell(30,6,utf8_decode('Código del Predio'),1,0,'L',true);
				$pdf->Cell(25,6,utf8_decode($CODIGO_PREDIO),1,0,'L',true);
				
				$pdf->Cell(25,6,utf8_decode('Área Registral'),1,0,'L',true);
				$pdf->Cell(55,6,utf8_decode($AREA_REGISTRAL).' '.utf8_decode($ABREV_TIP_UNID_MED) ,1,0,'L',true);
		
				$pdf->Ln();
				
				$pdf->Cell(25,6,utf8_decode('Tomo'),1,0,'L',true);
				$pdf->Cell(60,6,utf8_decode($TOMO),1,0,'L',false);
				
				$pdf->Cell(30,6,utf8_decode('Asiento'),1,0,'L',true);
				$pdf->Cell(25,6,utf8_decode($ASIENTO),1,0,'L',true);
				
				$pdf->Cell(30,6,utf8_decode('Fojas'),1,0,'L',true);
				$pdf->Cell(25,6,utf8_decode($FOJAS),1,0,'L',true);
				
				$pdf->Cell(25,6,utf8_decode('Ficha'),1,0,'L',true);
				$pdf->Cell(13,6,utf8_decode($FICHA),1,0,'L',true);
				
				$pdf->Cell(32,6,utf8_decode('Anotación Preventiva'),1,0,'L',true);
				$pdf->Cell(10,6,utf8_decode($ANOTACION_PREVENTIVA),1,0,'L',true);
				
				$pdf->Ln();
				
				$pdf->SetFillColor(205,205,205);
				$pdf->Cell(275,6,utf8_decode('DATOS TECNICOS'),1,0,'C',true);
				
				$pdf->Ln();
				
				$pdf->SetFillColor(255,255,255);
				
				$pdf->Cell(25,6,utf8_decode('Zonificación'),1,0,'L',true);
				$pdf->Cell(60,6,utf8_decode($ZONIFICACION),1,0,'L',false);
				
				$pdf->Cell(30,6,utf8_decode('Tipo Terreno'),1,0,'L',true);
				$pdf->Cell(40,6,utf8_decode($NOM_TIP_TERRENO),1,0,'L',true);
				
				$pdf->Cell(30,6,utf8_decode('Perimetro'),1,0,'L',true);
				$pdf->Cell(30,6,utf8_decode($PERIMETRO),1,0,'L',true);
				
				$pdf->Cell(20,6,utf8_decode('Area'),1,0,'L',true);
				$pdf->Cell(40,6,utf8_decode($AREA_TERRENO),1,0,'L',true);
				
				$pdf->Ln();
				
				$pdf->Cell(35,6,utf8_decode('Estado de Conservación'),1,0,'L',true);
				$pdf->Cell(30,6,utf8_decode($TXT_CONSERVACION),1,0,'L',false);
				
				$pdf->Cell(20,6,utf8_decode('Nro Pisos'),1,0,'L',true);
				$pdf->Cell(30,6,utf8_decode($NUMERO_PISOS),1,0,'L',true);
				
				$pdf->Cell(25,6,utf8_decode('Saneamiento'),1,0,'L',true);
				$pdf->Cell(30,6,utf8_decode($TXT_ESANEAMIENTO),1,0,'L',true);
				
				$pdf->Cell(35,6,utf8_decode('Uso del Predio'),1,0,'L',true);
				$pdf->Cell(70,6,utf8_decode($NOMBRE_USO_GENERICO),1,0,'L',true);
				
				$pdf->Ln();
				$pdf->Ln();
				$pdf->Ln(5);
				
				
				$fill = !$fill;
				
			}
			
				
			$pdf->Ln();
	
			$pdf->Output();
			
		}else{
			echo "Parametro no reconocido.";
		}
	  
	  //$this->dump($data);
   }
	
	
  /**
   * Renderiza la plantilla, con los valores indicados.
   * @param $template Nombre de la plantilla, debe ser el nombre de el archivo plantilla.
   * @param $values   Array con los datos que seran pasados a la plantilla.
   *
   * @return Template renderizado con los valores de las variables.
   */
  public function render($template, $values){
      $render = function () use ($template, $values){
          $data = $values;
          include "$template";
      };
      return $render();
  }

  /**
   * Imprime una variable respetando sangrias y espaciado, con la funcion printr.
   * @param $data Valor a imprimir en la pantalla del navegador.
   * @return Nothing.
   */
  public function dump($data){
      print '<pre>';
      print_r($data);
      print '</pre>';
  }
  
  
  public function Nombre_Carpeta($COD_TIP_DOC_PRED){
		
		if($COD_TIP_DOC_PRED == '5'){
			$carpeta = 'partida_registral';
			
		}else if($COD_TIP_DOC_PRED == '6'){
			$carpeta = 'plano_perimetrico';
			
		}else if($COD_TIP_DOC_PRED == '7'){
			$carpeta = 'plano_ubicacion';
			
		}else if($COD_TIP_DOC_PRED == '8'){
			$carpeta = 'plano_habilitacion';
			
		}else if($COD_TIP_DOC_PRED == '9'){
			$carpeta = 'tasacion';
			
		}else if($COD_TIP_DOC_PRED == '10'){
			$carpeta = 'fotografia';
			
		}else if($COD_TIP_DOC_PRED == '11'){
			$carpeta = 'escritura_publica';
			
		}else if($COD_TIP_DOC_PRED == '12'){
			$carpeta = 'memoria_descriptiva';
			
		}else if($COD_TIP_DOC_PRED == '13'){
			$carpeta = 'titulo_propiedad';
			
		}else if($COD_TIP_DOC_PRED == '14'){
			$carpeta = 'informe_tec_legal';
			
		}else if($COD_TIP_DOC_PRED == '15'){
			$carpeta = 'informe_dispos_legal';
			
		}else if($COD_TIP_DOC_PRED == '16'){
			$carpeta = 'archivo_dxf';
		}else{
			$carpeta = 'otros';
		}
		return $carpeta;
		
	}


	/* CREADO POR: 			WILLIAMS ARENAS */
	/* FECHA DE CREACION: 	21/08/2018 */
	public function Eliminacion_Baja_Predios(){
		
		$data = array();
		$objPrediosModel 	= new PrediosModel();
		
		$txt_entidad = $_SESSION['th_SIMI_COD_ENTIDAD'];
		$ID_PREDIO_SBN = isset($_GET['xID_PREDIO_SBN']) ? $_GET['xID_PREDIO_SBN'] : '';
		$COD_TIP_PROPIEDAD = isset($_GET['xCOD_TIP_PROPIEDAD']) ? $_GET['xCOD_TIP_PROPIEDAD'] : '';

		/*
		echo 'entidad: ' . $txt_entidad . '<br>';
		echo 'predio: ' . $ID_PREDIO_SBN . '<br>';
		echo 'propiedad: ' . $COD_TIP_PROPIEDAD . '<br>';
		*/

		$RESULTADO = $objPrediosModel->Eliminacion_Baja_Predios_Validacion( $txt_entidad, $ID_PREDIO_SBN );
		$CANTIDAD = odbc_result($RESULTADO,"CANTIDAD");

		$valor = ($CANTIDAD == '0') ? '1' : '2';

		if( $valor == 1 ){
			$RESULTADO =	$objPrediosModel->Eliminar_Baja_Predio( $txt_entidad, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD );
			$valor = ($RESULTADO) ? 1 : 0;
		}


		echo $valor;


	}
	
	// INICIO JMCR
	public function Validar_Eliminar_Predio(){
		$data = array();
		$objPrediosModel 	= new PrediosModel();

		$COD_ENTIDAD = isset($_GET['xTXH_SIMI_COD_ENTIDAD']) ? $_GET['xTXH_SIMI_COD_ENTIDAD'] : '';
		$ID_PREDIO_SBN = isset($_GET['xID_PREDIO_SBN']) ? $_GET['xID_PREDIO_SBN'] : '';
		$COD_TIP_PROPIEDAD = isset($_GET['xCOD_TIP_PROPIEDAD']) ? $_GET['xCOD_TIP_PROPIEDAD'] : '';

		$data['COD_ENTIDAD'] = $COD_ENTIDAD;
		$data['ID_PREDIO_SBN'] = $ID_PREDIO_SBN;
		$data['COD_TIP_PROPIEDAD'] = $COD_TIP_PROPIEDAD;

		$data['CANTIDAD_PREDIOS'] = odbc_result($objPrediosModel->Contar_muebles_predio($COD_ENTIDAD, $ID_PREDIO_SBN), "CANTIDAD_P");
		$data['CANTIDAD_AREAS'] = odbc_result($objPrediosModel->Contar_areas_predio($COD_ENTIDAD, $ID_PREDIO_SBN), "CANTIDAD_A");
		$data['CANTIDAD_PERSONAL'] = odbc_result($objPrediosModel->Contar_personal_predio($COD_ENTIDAD, $ID_PREDIO_SBN), "CANTIDAD_S");
		$data['DENOMINACION_PREDIO'] = odbc_result($objPrediosModel->Devolver_nombre_predio($COD_ENTIDAD, $ID_PREDIO_SBN), "DENOMINACION_PREDIO");
		$data['PREDIOS_MIGRAR'] = $objPrediosModel->Listar_predios_migrar($COD_ENTIDAD, $ID_PREDIO_SBN);

		$this->render('v.predio_eliminar.php', $data);
	}

	public function Eliminar_Predio(){
		$data = array();
		$objPrediosModel 	= new PrediosModel();
		$objModel = new BienPatrimonialModel();

		$COD_ENTIDAD = isset($_GET['xTXH_SIMI_COD_ENTIDAD']) ? $_GET['xTXH_SIMI_COD_ENTIDAD'] : '';
		$ID_PREDIO_SBN = isset($_GET['xID_PREDIO_SBN']) ? $_GET['xID_PREDIO_SBN'] : '';
		$COD_TIP_PROPIEDAD = isset($_GET['xCOD_TIP_PROPIEDAD']) ? $_GET['xCOD_TIP_PROPIEDAD'] : '';

		$data['COD_ENTIDAD'] = $COD_ENTIDAD;
		$data['ID_PREDIO_SBN'] = $ID_PREDIO_SBN;
		$data['COD_TIP_PROPIEDAD'] = $COD_TIP_PROPIEDAD;
/*
		$data['CANTIDAD_PREDIOS'] = odbc_result($objPrediosModel->Contar_muebles_predio($COD_ENTIDAD, $ID_PREDIO_SBN), "CANTIDAD_P");
		$data['CANTIDAD_AREAS'] = odbc_result($objPrediosModel->Contar_areas_predio($COD_ENTIDAD, $ID_PREDIO_SBN), "CANTIDAD_A");
		$data['CANTIDAD_PERSONAL'] = odbc_result($objPrediosModel->Contar_personal_predio($COD_ENTIDAD, $ID_PREDIO_SBN), "CANTIDAD_S");
		$data['DENOMINACION_PREDIO'] = odbc_result($objPrediosModel->Devolver_nombre_predio($COD_ENTIDAD, $ID_PREDIO_SBN), "DENOMINACION_PREDIO");
		$data['PREDIOS_MIGRAR'] = $objPrediosModel->Listar_predios_migrar($COD_ENTIDAD, $ID_PREDIO_SBN);
*/

		/* CREACION:        WILLIAMS ARENAS */
	    /* FECHA CREACION:  08-06-2020 */
	    /* DESCRIPCION:     CONTAR MUEBLES PREDIOS */
		$dataResultado = array();
		$data = array();
    
	    $dataModel_01 = [
	      'COD_ENTIDAD'		=>$COD_ENTIDAD,
		  'ID_PREDIO_SBN'	=>$ID_PREDIO_SBN,
		  'COD_TIP_PROPIEDAD' => $COD_TIP_PROPIEDAD
	    ];
	    
	    $dataResultado['DataContar_muebles_predio'] = $objModel->Contar_muebles_predio_PDO($dataModel_01);
		$data['CANTIDAD_PREDIOS']  = $dataResultado['DataContar_muebles_predio'][0]['CANTIDAD_P'];

		$dataResultado['DataContar_areas_predio'] = $objModel->Contar_areas_predio_PDO($dataModel_01);
		$data['CANTIDAD_AREAS'] = $dataResultado['DataContar_areas_predio'][0]['CANTIDAD_A'];

		$dataResultado['DataContar_personal_predio'] = $objModel->Contar_personal_predio_PDO($dataModel_01);
		$data['CANTIDAD_PERSONAL'] = $dataResultado['DataContar_personal_predio'][0]['CANTIDAD_S'];

		$dataResultado['DataDevolver_nombre_predio'] = $objModel->Devolver_nombre_predio_PDO($dataModel_01);
		$data['DENOMINACION_PREDIO']  = $dataResultado['DataDevolver_nombre_predio'][0]['DENOMINACION_PREDIO'];

		$data['PREDIOS_MIGRAR']  = $objModel->Listar_predios_migrar_PDO($dataModel_01);
		
		
	    /* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */
		// $valor = ($CANTIDAD == '0') ? '1' : '2';
		// if( $valor == 1 ){
			/*
			$RESULTADO =	$objPrediosModel->Eliminar_Predio($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD);
			
			$valor = ($RESULTADO) ? 1 : 0;
			*/
		// }
		// $this->render('v.predio_eliminar.php', $data);

		/* CREACION:        WILLIAMS ARENAS */
	    /* FECHA CREACION:  25-05-2020 */
	    /* DESCRIPCION:     OBTENER TOTAL DE REGISTROS DE PREDIOS */
	    	    
	    $RESULTADO = $objModel->Eliminar_Predio_PDO($dataModel_01);
	    $valor = ($RESULTADO>=0) ? '1':'0';
	    /* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */

	}

	public function Habilitar_Predios(){
		$data = array();
		$objPrediosModel 	= new PrediosModel();

		$COD_ENTIDAD = isset($_GET['xTXH_SIMI_COD_ENTIDAD']) ? $_GET['xTXH_SIMI_COD_ENTIDAD'] : '';
		$ID_PREDIO_SBN = isset($_GET['xID_PREDIO_SBN']) ? $_GET['xID_PREDIO_SBN'] : '';
		$COD_TIP_PROPIEDAD = isset($_GET['xCOD_TIP_PROPIEDAD']) ? $_GET['xCOD_TIP_PROPIEDAD'] : '';

		$RESULTADO =	$objPrediosModel->Habilitar_Predio($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD);
		$valor = ($RESULTADO) ? 1 : 0;
		echo $valor;
	}

	public function DarDeBaja_Predios(){
		$data = array();
		$objPrediosModel 	= new PrediosModel();

		$COD_ENTIDAD = isset($_GET['xTXH_SIMI_COD_ENTIDAD']) ? $_GET['xTXH_SIMI_COD_ENTIDAD'] : '';
		$ID_PREDIO_SBN = isset($_GET['xID_PREDIO_SBN']) ? $_GET['xID_PREDIO_SBN'] : '';
		$COD_TIP_PROPIEDAD = isset($_GET['xCOD_TIP_PROPIEDAD']) ? $_GET['xCOD_TIP_PROPIEDAD'] : '';

		$RESULTADO = $objPrediosModel->Contar_muebles_predio( $COD_ENTIDAD, $ID_PREDIO_SBN );
		$CANTIDAD_P = odbc_result($RESULTADO,"CANTIDAD_P");

		$valor = ($CANTIDAD_P == '0') ? '1' : '2';
		if( $valor == 1 ){
			$RESULTADO =	$objPrediosModel->DarDeBaja_Predios($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD);
			$valor = ($RESULTADO) ? 1 : 0;
		}
		echo $valor;
	}
	
	public function Migrar_Predios(){
		$data = array();
		$objPrediosModel 	= new PrediosModel();
		$objModel = new BienPatrimonialModel();

		$COD_ENTIDAD = isset($_GET['xTXH_SIMI_COD_ENTIDAD']) ? $_GET['xTXH_SIMI_COD_ENTIDAD'] : '';
		$ID_PREDIO_SBN = isset($_GET['xID_PREDIO_SBN']) ? $_GET['xID_PREDIO_SBN'] : '';
		$COD_TIP_PROPIEDAD = isset($_GET['xCOD_TIP_PROPIEDAD']) ? $_GET['xCOD_TIP_PROPIEDAD'] : '';
		$cbo_predio = isset($_GET['cbo_predio']) ? $_GET['cbo_predio'] : '';
/*
		$RES_AREA = $objPrediosModel->Migrar_predio_areas($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD, $cbo_predio);
		$RES_PERS = $objPrediosModel->Migrar_predio_personas($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD, $cbo_predio);
		$RES_MUEB = $objPrediosModel->Migrar_predio_muebles($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD, $cbo_predio);
*/

		/* CREACION:        WILLIAMS ARENAS */
	    /* FECHA CREACION:  08-06-2020 */
		/* DESCRIPCION:     MIGRACION */
		$dataModel_01 = [
			'COD_ENTIDAD'		=>$COD_ENTIDAD,
			'ID_PREDIO_SBN'		=>$ID_PREDIO_SBN,
			'COD_TIP_PROPIEDAD'	=>$COD_TIP_PROPIEDAD,
			'cbo_predio'		=>$cbo_predio,
		  ];

		$RES_AREA = $objModel->Migrar_predio_areas_PDO($dataModel_01);
		$RES_PERS = $objModel->Migrar_predio_personas_PDO($dataModel_01);
		$RES_MUEB = $objModel->Migrar_predio_muebles_PDO($dataModel_01);
		/* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */
	
		$valor_area = ($RES_AREA>=0) ? '1':'0';
		echo $valor_area;

		$valor_pers = ($RES_PERS>=0) ? '1':'0';
		echo $valor_pers;

		$valor_mueb = ($RES_MUEB>=0) ? '1':'0';
		echo $valor_mueb;
	}
	// FIN JMCR


}



?>