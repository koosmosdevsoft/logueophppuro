<?
require_once("C_Interconexion_SQL.php");

class Simi_Grupo{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
	
	
	function Simi_Generar_Codigo_Grupo(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(COD_GRUPO),0) + 1) as CODIGO FROM TBL_MUEBLES_GRUPO ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Simi_Insertar_Grupo($COD_GRUPO, $NRO_GRUPO, $DESC_GRUPO, $USUARIO_CREACION){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_GRUPO  (COD_GRUPO, NRO_GRUPO, DESC_GRUPO, FECHA_REGISTRO, USUARIO_CREACION, FECHA_CREACION, USUARIO_MODIFICA, FECHA_MODIFICA, USUARIO_ELIMINA, FECHA_ELIMINA, ID_ESTADO)
						VALUES ('$COD_GRUPO', '$NRO_GRUPO', '$DESC_GRUPO', GETDATE(), '$USUARIO_CREACION', GETDATE(), NULL, NULL, NULL, NULL, 1) ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Simi_Actualizar_Grupo($COD_GRUPO, $NRO_GRUPO, $DESC_GRUPO, $USUARIO_MODIFICA){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_MUEBLES_GRUPO 
			SET NRO_GRUPO = '$NRO_GRUPO', DESC_GRUPO = '$DESC_GRUPO', USUARIO_MODIFICA = '$USUARIO_MODIFICA', FECHA_MODIFICA = GETDATE() 
			WHERE COD_GRUPO = $COD_GRUPO";
			$result = $this->oDBManager->execute($sql);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Simi_Eliminar_Grupo($COD_GRUPO, $USUARIO_ELIMINA){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_GRUPO SET ID_ESTADO = 2, USUARIO_ELIMINA = '$USUARIO_ELIMINA', FECHA_ELIMINA = GETDATE() WHERE COD_GRUPO = '$COD_GRUPO' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}	
	
	
	function Simi_Listar_Grupo(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM  TBL_MUEBLES_GRUPO  WHERE ID_ESTADO = 1  ORDER BY COD_GRUPO ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Simi_Ver_Grupo_x_Codigo($COD_GRUPO){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_GRUPO WHERE COD_GRUPO = '$COD_GRUPO' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Simi_Generar_Codigo_Clase_x_Grupo(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(COD_DET_GRUPO_CLASE),0) + 1) as CODIGO FROM TBL_MUEBLES_GRUPO_CLASE ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	

	function Simi_Insertar_Clase_x_Grupo($COD_DET_GRUPO_CLASE, $COD_GRUPO, $COD_CLASE, $COD_CTA_CONTABLE, $COD_CTA_CONTABLE_DEPREC, $USUARIO_CREACION){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_GRUPO_CLASE  (COD_DET_GRUPO_CLASE, COD_GRUPO, COD_CLASE, COD_CTA_CONTABLE, COD_CTA_CONTABLE_DEPREC, FECHA_REGISTRO, USUARIO_CREACION, FECHA_CREACION, ID_ESTADO)
						VALUES ('$COD_DET_GRUPO_CLASE', '$COD_GRUPO', '$COD_CLASE', '$COD_CTA_CONTABLE', '$COD_CTA_CONTABLE_DEPREC', GETDATE(), '$USUARIO_CREACION', GETDATE(), 1) ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Simi_Actualizar_Clase_x_Grupo($COD_DET_GRUPO_CLASE, $COD_GRUPO, $COD_CLASE, $COD_CTA_CONTABLE, $COD_CTA_CONTABLE_DEPREC, $USUARIO_MODIFICA){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_MUEBLES_GRUPO_CLASE 
			SET COD_GRUPO = '$COD_GRUPO', COD_CLASE = '$COD_CLASE', COD_CTA_CONTABLE = '$COD_CTA_CONTABLE', COD_CTA_CONTABLE_DEPREC = '$COD_CTA_CONTABLE_DEPREC', USUARIO_MODIFICA = '$USUARIO_MODIFICA', FECHA_MODIFICA = GETDATE() 
			WHERE COD_DET_GRUPO_CLASE = $COD_DET_GRUPO_CLASE";
			$result = $this->oDBManager->execute($sql);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Simi_Eliminar_Clase_x_Grupo($COD_DET_GRUPO_CLASE, $USUARIO_ELIMINA){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_GRUPO_CLASE SET ID_ESTADO = 2, USUARIO_ELIMINA = '$USUARIO_ELIMINA', FECHA_ELIMINA = GETDATE() WHERE COD_DET_GRUPO_CLASE = '$COD_DET_GRUPO_CLASE' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}	
	
	
	function Simi_Listar_Clase_x_Grupo($COD_GRUPO){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT GC.*, C.NRO_CLASE, C.DESC_CLASE, CC.NRO_CTA_CONTABLE, CC.NOM_CTA_CONTABLE, CCD.NRO_CTA_CONTABLE AS NRO_CTA_CONTABLE_DEPREC, CCD.NOM_CTA_CONTABLE AS NOM_CTA_CONTABLE_DEPREC
FROM TBL_MUEBLES_GRUPO_CLASE GC
LEFT JOIN TBL_MUEBLES_CLASE C ON (GC.COD_CLASE = C.COD_CLASE)
LEFT JOIN TBL_MUEBLES_CUENTA_CONTABLE CC ON (CC.COD_CTA_CONTABLE = GC.COD_CTA_CONTABLE)
LEFT JOIN TBL_MUEBLES_CUENTA_CONTABLE CCD ON (CCD.COD_CTA_CONTABLE = GC.COD_CTA_CONTABLE_DEPREC)
WHERE GC.COD_GRUPO = '$COD_GRUPO' AND GC.ID_ESTADO = 1
ORDER BY C.NRO_CLASE, C.DESC_CLASE";
			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Simi_Ver_Clase_x_Grupo_x_Codigo_DET($COD_DET_GRUPO_CLASE){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_GRUPO_CLASE WHERE COD_DET_GRUPO_CLASE = '$COD_DET_GRUPO_CLASE' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Simi_Validar_Si_Existe_Clase_Duplicada_x_Grupo($COD_GRUPO, $COD_CLASE){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT COUNT(*) AS TOTAL FROM TBL_MUEBLES_GRUPO_CLASE WHERE COD_GRUPO = '$COD_GRUPO' AND COD_CLASE = '$COD_CLASE' AND ID_ESTADO = 1 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Simi_Ver_Datos_Cuenta_Contable_Default_Grupo_Clase($COD_GRUPO, $COD_CLASE){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT 
  GC.COD_DET_GRUPO_CLASE,
  GC.COD_GRUPO,
  GC.COD_CLASE,
  GC.COD_CTA_CONTABLE_DEPREC,
  GC.COD_CTA_CONTABLE,
  CC.USO_CUENTA,
  CC.TIP_CUENTA,
  CC.NRO_MAYOR,
  CC.NRO_SUBCUENTA,
  CC.NRO_CTA_CONTABLE,
  CC.NOM_CTA_CONTABLE
  FROM TBL_MUEBLES_GRUPO_CLASE GC
  LEFT JOIN TBL_MUEBLES_CUENTA_CONTABLE CC ON (GC.COD_CTA_CONTABLE = CC.COD_CTA_CONTABLE AND CC.ID_ESTADO = '1' )
  WHERE GC.ID_ESTADO = '1' AND GC.COD_GRUPO = '$COD_GRUPO' AND GC.COD_CLASE = '$COD_CLASE' 
  ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
}
?>