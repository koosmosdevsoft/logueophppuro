<?php
class BienPatrimonialController{
	
		
    public function index(){
		
		$data = array();		
		$objCatalogoModel = new CatalogoModel();		
		$data['DataListaGrupo'] = $objCatalogoModel->Listar_Grupos_Catalogo();		
		$this->render('principal.tpl.php', $data);
   }

	
	public function Filtrar_Bien_Patrimonial_x_Parametros(){
		
		$data = array();		
		$objBienPatrimonialModel = new BienPatrimonialModel();	
		
		$xNRO_FILA_LIST 	= 25;
		$xNRO_LINK_PAGE 	= 5;
		
		$TXH_COD_ENTIDAD 		= isset($_GET['TXH_COD_ENTIDAD'])? $_GET['TXH_COD_ENTIDAD'] : "";
		$busq_Grupo_Generico 	= isset($_GET['busq_Grupo_Generico'])? $_GET['busq_Grupo_Generico'] : "";
		$busq_Clase_generico 	= isset($_GET['busq_Clase_generico'])? $_GET['busq_Clase_generico'] : "";
		$busq_cod_bien 			= isset($_GET['busq_cod_bien'])? $_GET['busq_cod_bien'] : "";
		$busq_denom_bien 		= isset($_GET['busq_denom_bien'])? $_GET['busq_denom_bien'] : "";
		$pageNumber 			= isset($_GET['numeroPagina'])? $_GET['numeroPagina'] : "1";
		
		$dataModel_01 = [
			'TXH_COD_ENTIDAD'=>$TXH_COD_ENTIDAD,
			'busq_Grupo_Generico'=>$busq_Grupo_Generico,
			'busq_Clase_generico'=>$busq_Clase_generico,
			'busq_cod_bien'=>$busq_cod_bien,
			'busq_denom_bien'=>$busq_denom_bien			
		];
		
		$Total_Registros = $objBienPatrimonialModel->Total_Bienes_Patrimoniales_x_Parametros($dataModel_01);
		$Total_Paginas = ceil($Total_Registros / $xNRO_FILA_LIST);
		
		$data['pager']['indicePagina'] 		= $pageNumber;
		$data['pager']['cantidadRegistros'] = $Total_Registros;
		$data['pager']['cantidadPaginas'] 	= $Total_Paginas;	
		$data['pager']['limiteInferior'] 	= (($pageNumber - $xNRO_LINK_PAGE) <= 0) ?  1 : ($pageNumber - $xNRO_LINK_PAGE);
		$data['pager']['limiteSuperior'] 	= (($pageNumber + $xNRO_LINK_PAGE) > $Total_Paginas) ? $Total_Paginas : ($pageNumber + $xNRO_LINK_PAGE);
		
		$Item_Ini = ($pageNumber == '1')? (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST) : (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST)+1 ;
		$Item_Fin = ($pageNumber == '1')? ($Item_Ini + $xNRO_FILA_LIST) : ($Item_Ini + $xNRO_FILA_LIST)-1;
		
		$dataModel_02 = [
			'TXH_COD_ENTIDAD'=>$TXH_COD_ENTIDAD,
			'busq_Grupo_Generico'=>$busq_Grupo_Generico,
			'busq_Clase_generico'=>$busq_Clase_generico,
			'busq_cod_bien'=>$busq_cod_bien,
			'busq_denom_bien'=>$busq_denom_bien,
			'Item_Ini'=>$Item_Ini,
			'Item_Fin'=>$Item_Fin			
		];
		
		$data['DataListaBienPatrimonial'] = $objBienPatrimonialModel->Listar_Bienes_Patrimoniales_x_Parametros($dataModel_02);
					
		//$this->dump($data);		
		$this->render('principal_list.tpl.php', $data);
   }


	public function Mostrar_Formulario_Editar(){//modulo editar caracteristicas
		
		$data = array();		
		$objBienPatrimonialModel 	= new BienPatrimonialModel();
		$objCtaContableModel 		= new CtaContableModel();
		
		$sCOD_UE_BIEN_PATRI = isset($_GET['sCOD_UE_BIEN_PATRI']) ? $_GET['sCOD_UE_BIEN_PATRI'] : '';
		
		$dataModel = ['COD_UE_BIEN_PATRI'=>$sCOD_UE_BIEN_PATRI];
		$data['RegDatosBienPatrimonial'] = $objBienPatrimonialModel->Ver_Datos_Bien_Patrimonial_x_ID($dataModel);
		$data['DataEstadoBien'] = $objBienPatrimonialModel->Simi_Listar_Estado_Bien();
		
		$xUSO_CUENTA = $data['RegDatosBienPatrimonial'][0]['USO_CUENTA'];
		$xTIP_CUENTA = $data['RegDatosBienPatrimonial'][0]['TIP_CUENTA'];
		if($xUSO_CUENTA != '' && $xTIP_CUENTA != ''){
			$dataModel_CTA = ['USO_CUENTA'=>$xUSO_CUENTA, 'TIP_CUENTA'=>$xTIP_CUENTA];
			$data['DataListaCuentaContable_Parametro'] = $objCtaContableModel->Simi_Listar_Cuenta_Contable_x_Parametros($dataModel_CTA);
		}
		
		//$this->dump($data);		
		$this->render('principal_form.tpl.php', $data);
   }


	
	public function Guardar_Reg_Caracteristicas_Bien(){
		
		$data = array();
		$objBienPatrimonialModel 	= new BienPatrimonialModel();
		
		$COD_ENTIDAD = isset($_GET['TXH_SIMI_COD_ENTIDAD']) ? filtrar_caracteres($_GET['TXH_SIMI_COD_ENTIDAD']) : '';
		$SIMI_USUARIO = isset($_GET['TXH_SIMI_COD_USUARIO']) ? filtrar_caracteres($_GET['TXH_SIMI_COD_USUARIO']) : '';
		
		$COD_UE_BIEN_PATRI = isset($_GET['txh_f_COD_UE_BIEN_PATRI']) ? filtrar_caracteres($_GET['txh_f_COD_UE_BIEN_PATRI']) : '';		
		$DENOMINACION_BIEN = isset($_GET['txt_reg_denominacion_bien']) ? filtrar_caracteres($_GET['txt_reg_denominacion_bien']) : '';
		
		$USO_CUENTA = isset($_GET['CBO_USO_CTA_CONT']) ? filtrar_caracteres($_GET['CBO_USO_CTA_CONT']) : '';
		$TIP_CUENTA = isset($_GET['radio_uso_cuenta']) ? filtrar_caracteres($_GET['radio_uso_cuenta']) : '';
		$COD_CTA_CONTABLE = isset($_GET['CBO_CTA_NRO_CONTABLE']) ? filtrar_caracteres($_GET['CBO_CTA_NRO_CONTABLE']) : '';
		$VALOR_ADQUIS = isset($_GET['txt_f_valor_adquisicion']) ? filtrar_caracteres($_GET['txt_f_valor_adquisicion']) : '';
		$PORC_DEPREC = isset($_GET['txt_porcent_deprec']) ? filtrar_caracteres($_GET['txt_porcent_deprec']) : '';
		$OPC_ASEGURADO = isset($_GET['valor_checkbox_asegurado']) ? filtrar_caracteres($_GET['valor_checkbox_asegurado']) : '';
		$COD_ESTADO_BIEN = isset($_GET['cbo_estado_bien_patrim']) ? filtrar_caracteres($_GET['cbo_estado_bien_patrim']) : '';
		$OBSERVACION = isset($_GET['txt_f_observacion']) ? filtrar_caracteres($_GET['txt_f_observacion']) : '';
		
		$MARCA = isset($_GET['TXT_MARCA']) ? filtrar_caracteres($_GET['TXT_MARCA']) : '';
		$MODELO = isset($_GET['TXT_MODELO']) ? filtrar_caracteres($_GET['TXT_MODELO']) : '';
		$TIPO = isset($_GET['TXT_TIPO']) ? filtrar_caracteres($_GET['TXT_TIPO']) : '';
		$COLOR = isset($_GET['TXT_COLOR']) ? filtrar_caracteres($_GET['TXT_COLOR']) : '';
		$SERIE = isset($_GET['TXT_NUM_SERIE']) ? filtrar_caracteres($_GET['TXT_NUM_SERIE']) : '';
		$NRO_MOTOR = isset($_GET['TXT_NUM_MOTOR']) ? filtrar_caracteres($_GET['TXT_NUM_MOTOR']) : '';
		$PLACA = isset($_GET['TXT_NUM_PLACA']) ? filtrar_caracteres($_GET['TXT_NUM_PLACA']) : '';
		$DIMENSION = isset($_GET['TXT_DIMENSION']) ? filtrar_caracteres($_GET['TXT_DIMENSION']) : '';
		$NRO_CHASIS = isset($_GET['TXT_NUM_CHASIS']) ? filtrar_caracteres($_GET['TXT_NUM_CHASIS']) : '';
		$MATRICULA = isset($_GET['TXT_NUM_MATRICULA']) ? filtrar_caracteres($_GET['TXT_NUM_MATRICULA']) : '';
		$ANIO_FABRICACION = isset($_GET['TXT_FECHA_FABR']) ? filtrar_caracteres($_GET['TXT_FECHA_FABR']) : '';
		$LONGITUD = isset($_GET['TXT_LONGITUD']) ? filtrar_caracteres($_GET['TXT_LONGITUD']) : '';
		$ALTURA = isset($_GET['TXT_ALTURA']) ? filtrar_caracteres($_GET['TXT_ALTURA']) : '';
		$ANCHO = isset($_GET['TXT_ANCHO']) ? filtrar_caracteres($_GET['TXT_ANCHO']) : '';
		$RAZA = isset($_GET['TXT_RAZA']) ? filtrar_caracteres($_GET['TXT_RAZA']) : '';
		$ESPECIE = isset($_GET['TXT_ESPECIE']) ? filtrar_caracteres($_GET['TXT_ESPECIE']) : '';
		$EDAD = isset($_GET['TXT_EDAD']) ? filtrar_caracteres($_GET['TXT_EDAD']) : '';
		$PAIS = isset($_GET['TXT_PAIS']) ? filtrar_caracteres($_GET['TXT_PAIS']) : '';
		$OTRAS_CARACT = isset($_GET['TXT_OTRAS_CARACT']) ? filtrar_caracteres($_GET['TXT_OTRAS_CARACT']) : '';
		

		$dataModel_Caract = [
			'COD_ENTIDAD'=>$COD_ENTIDAD,
			'SIMI_USUARIO'=>$SIMI_USUARIO,
			'COD_UE_BIEN_PATRI'=>$COD_UE_BIEN_PATRI,
			'DENOMINACION_BIEN'=>$DENOMINACION_BIEN,
			'USO_CUENTA'=>$USO_CUENTA,
			'TIP_CUENTA'=>$TIP_CUENTA,
			'COD_CTA_CONTABLE'=>$COD_CTA_CONTABLE,
			'VALOR_ADQUIS'=>$VALOR_ADQUIS,
			'PORC_DEPREC'=>$PORC_DEPREC,
			'OPC_ASEGURADO'=>$OPC_ASEGURADO,
			'COD_ESTADO_BIEN'=>$COD_ESTADO_BIEN,
			'OBSERVACION'=>$OBSERVACION,
			'MARCA'=>$MARCA,
			'MODELO'=>$MODELO,
			'TIPO'=>$TIPO,
			'COLOR'=>$COLOR,
			'SERIE'=>$SERIE,
			'NRO_MOTOR'=>$NRO_MOTOR,
			'PLACA'=>$PLACA,
			'DIMENSION'=>$DIMENSION,
			'NRO_CHASIS'=>$NRO_CHASIS,
			'MATRICULA'=>$MATRICULA,
			'ANIO_FABRICACION'=>$ANIO_FABRICACION,
			'LONGITUD'=>$LONGITUD,
			'ALTURA'=>$ALTURA,
			'ANCHO'=>$ANCHO,
			'RAZA'=>$RAZA,
			'ESPECIE'=>$ESPECIE,
			'EDAD'=>$EDAD,
			'PAIS'=>$PAIS,
			'OTRAS_CARACT'=>$OTRAS_CARACT
		];
		
		$Resultado = $objBienPatrimonialModel->Actualiza_Datos_Bien_Patrimonial_Caracteristicas($dataModel_Caract);
		
		$valor = ($Resultado>=0) ? '1':'0';
		echo $valor;
		//$this->dump($dataModel_Caract);
   }

	
	public function Mostrar_Grid_CtaContable(){

		$data_x = array();	
		$objBienPatrimonialModel 	= new BienPatrimonialModel();
		
		$xCE = isset($_GET['xCE']) ? $_GET['xCE'] : '';
		
		$start = isset($_GET['start']) ? $_GET['start'] : '0';
		$length = isset($_GET['length']) ? $_GET['length'] : '10';
		
		$Item_Ini = ($start+1);
		$Item_Fin = ($start+$length);
				
		$Param = [
			'COD_ENTIDAD'=>$xCE,
			'Item_Ini'=>$Item_Ini,
			'Item_Fin'=>$Item_Fin
			
		];
				
		$TotalReg	= $objBienPatrimonialModel->Total_RPT_CtaCble_x_Entidad($Param);
		$data_x['DataCTACBLE'] = $objBienPatrimonialModel->Lista_RPT_CtaCble_x_Entidad($Param);
		
		
		$filas=array();
		$detalle=array();
		$i=0;
		foreach ($data_x['DataCTACBLE'] as $Columnas):
		 
			$detalle[0]=$Columnas['ROW_NUMBER_ID'];
			$detalle[1]=$Columnas['NRO_CTA_CONTABLE'];
			$detalle[2]=$Columnas['NOM_CTA_CONTABLE'];
			$detalle[3]=$Columnas['ADQ'];
			$detalle[4]=$Columnas['NETO'];
			$filas[$i]=$detalle;
			$i=$i+1;
	
		 endforeach; 
		
		$info["filas"]=$filas;
		
		$data = array(
			"COD_ENTIDAD"     => $xCE,
			"draw"            => isset ( $_REQUEST['draw'] ) ? $_REQUEST['draw'] : 1,
			"recordsTotal"    => intval($TotalReg),
			"recordsFiltered" => intval($TotalReg),
			"data"            => $info["filas"]
		);
		
		//$this->dump($data);		
		echo json_encode($data);
   }



	/*************************************************************/
	public function render($template, $values){
	  $render = function () use ($template, $values){
		  $data = $values;
		  include "$template";
	  };
	  return $render();
	}
	
	
	public function dump($data){
	  print '<pre>';
	  print_r($data);
	  print '</pre>';
	}


}


?>