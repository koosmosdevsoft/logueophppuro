<?php
class PersonalSBNController{
	
	const NRO_FILA_LIST	= 5;
	const NRO_LINK_PAGE = 6;

		
    public function index(){
		$data = array();
		$objPersonalSBNModel 	= new PersonalSBNModel();
		/********************************************************************/
		
		$rsModalidad = $objPersonalSBNModel->Listar_Tipo_Modalidad();
		while (odbc_fetch_row($rsModalidad)) {			
			
			$ArrayModalidad = array();			
			$ArrayModalidad['COD_MODALIDAD'] 		= odbc_result($rsModalidad, 'COD_MODALIDAD');
			$ArrayModalidad['DESC_MODALIDAD'] 		= utf8_encode(odbc_result($rsModalidad, 'DESC_MODALIDAD'));
			
			$data['TipoModalidad_Busqueda'][] = $ArrayModalidad;
						
		}
		/********************************************************************/
				
		$this->render('principal.tpl.php', $data);
   }

	public function Buscar_Personal_x_Parametros(){
		
		$objPersonalSBNModel 	= new PersonalSBNModel();
		
		$data = array();
    	//$codigoEntidad = $_SESSION['th_SIMI_COD_ENTIDAD'];
	
		$txb_nombes = isset($_GET['txb_nombes']) ? $_GET['txb_nombes'] : '';
		$txb_apepat = isset($_GET['txb_apepat']) ? $_GET['txb_apepat'] : '';
		$txb_apemat = isset($_GET['txb_apemat']) ? $_GET['txb_apemat'] : '';
		$txb_num_doc = isset($_GET['txb_num_doc']) ? $_GET['txb_num_doc'] : '';
		$cbob_estado = isset($_GET['cbob_estado']) ? $_GET['cbob_estado'] : '';
		$COD_MODALIDAD = isset($_GET['txb_cbo_TIP_MODALIDAD']) ? $_GET['txb_cbo_TIP_MODALIDAD'] : '';
			
		$pageNumber = isset($_GET['numeroPagina']) ? $_GET['numeroPagina'] : 1;
		
		$xNRO_FILA_LIST 	= self::NRO_FILA_LIST;
		$xNRO_LINK_PAGE 	= self::NRO_LINK_PAGE;
		
		/*********************************************************/
		
		$RS = $objPersonalSBNModel->TOTAL_REGISTRO_PERSONAL_SBN_X_PARAMETROS($txb_nombes, $txb_apepat, $txb_apemat, $txb_num_doc, $cbob_estado, $COD_MODALIDAD);
		$Total_Registros = odbc_result($RS,"TOT_REG");
		
		$Total_Paginas = ceil($Total_Registros / $xNRO_FILA_LIST);
		
		$data['pager']['indicePagina'] 		= $pageNumber;
		$data['pager']['cantidadRegistros'] = $Total_Registros;
		$data['pager']['cantidadPaginas'] 	= $Total_Paginas;	
		$data['pager']['limiteInferior'] 	= (($pageNumber - $xNRO_LINK_PAGE) <= 0) ?  1 : ($pageNumber - $xNRO_LINK_PAGE);
		$data['pager']['limiteSuperior'] 	= (($pageNumber + $xNRO_LINK_PAGE) > $Total_Paginas) ? $Total_Paginas : ($pageNumber + $xNRO_LINK_PAGE);
		$data['pager']['handlerFunction'] 	= "handle_paginar_personal";	

					
		/*********************************************************/
		
		$Item_Ini = ($pageNumber == '1')? (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST) : (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST)+1 ;
		$Item_Fin = ($pageNumber == '1')? ($Item_Ini + $xNRO_FILA_LIST) : ($Item_Ini + $xNRO_FILA_LIST)-1;

		$Result_Personal = $objPersonalSBNModel->LISTA_PERSONAL_SBN_X_PARAMETROS($Item_Ini, $Item_Fin, $txb_nombes, $txb_apepat, $txb_apemat, $txb_num_doc, $cbob_estado, $COD_MODALIDAD);
		while (odbc_fetch_row($Result_Personal)){
						
			$personal = array();

			$personal['ROW_NUMBER_ID'] 		= utf8_encode(odbc_result($Result_Personal,"ROW_NUMBER_ID"));
			$personal['COD_PERSONAL'] 		= utf8_encode(odbc_result($Result_Personal,"COD_PERSONAL"));
			$personal['NOMBRES_PERS'] 		= utf8_encode(odbc_result($Result_Personal,"NOMBRES_PERS"));
			$personal['APE_PATERNO_PERS'] 	= utf8_encode(odbc_result($Result_Personal,"APE_PATERNO_PERS"));
			$personal['APE_MATERNO_PERS'] 	= utf8_encode(odbc_result($Result_Personal,"APE_MATERNO_PERS"));
			$personal['NRO_DNI'] 			= utf8_encode(odbc_result($Result_Personal,"NRO_DNI"));
			$personal['CONDICION_PERS'] 	= utf8_encode(odbc_result($Result_Personal,"CONDICION_PERS"));
			$personal['DESC_MODALIDAD'] 	= utf8_encode(odbc_result($Result_Personal,"DESC_MODALIDAD"));

			if(odbc_result($Result_Personal,"CONDICION_PERS") == 1){
				$DESC_CONDIC_PERS 	= 'ACTIVO';
				$CLASE_PUBLIC_COND  = 'texto_arial_azul_n_11';
			}else{
				$DESC_CONDIC_PERS 	= 'CESADO';
				$CLASE_PUBLIC_COND 	= 'texto_arial_rojo_n_11';
			}
			
			$personal['DESC_CONDIC_PERS']	 	= $DESC_CONDIC_PERS;
			$personal['CLASE_PUBLIC_COND']	 	= $CLASE_PUBLIC_COND;
			
			$RESULT_foto 	= $objPersonalSBNModel->LISTA_FOTO_PERSONAL_X_CODIGO(odbc_result($Result_Personal,"COD_PERSONAL"));
			$NOM_FOTO_L 	= odbc_result($RESULT_foto,"NOM_FOTO");	
			
			$xNOM_FOTO_L = ($NOM_FOTO_L == '')? 'image_perfil_min.jpg' : $NOM_FOTO_L;
			
			$directorio_foto_l = '../../../../Repositorio/intranet/foto_personal/'.$xNOM_FOTO_L;
			
			if (file_exists($directorio_foto_l)){
				$personal['DIR_FOTO_PERSONAL'] = $directorio_foto_l;
			}else{
				$personal['DIR_FOTO_PERSONAL'] = $directorio_foto_l;
			}
			
			$data['personales'][] = $personal;
			
		}
		

		$this->render('v.pri_listar.php', $data);
   }
   
   

	public function Mostrar_Datos_Personal(){
		
		$data = array();
		
		$objPersonalSBNModel 	= new PersonalSBNModel();
		$objUbigeoModel 		= new UbigeoModel();
		
		$codPersonal = isset($_GET['codPersonal']) ? $_GET['codPersonal'] : '';
		
		/*********************************************************/
		
		$result_Perso_E = $objPersonalSBNModel->MOSTRAR_DATOS_PERSONAL_x_COD_PERSONAL($codPersonal);
	
		$data['DatPersonal']['COD_PERSONAL']	 	= utf8_encode(odbc_result($result_Perso_E,"COD_PERSONAL"));
		$data['DatPersonal']['NOMBRES_PERS']	 	= utf8_encode(odbc_result($result_Perso_E,"NOMBRES_PERS"));
		$data['DatPersonal']['APE_PATERNO_PERS']	= utf8_encode(odbc_result($result_Perso_E,"APE_PATERNO_PERS"));
		$data['DatPersonal']['APE_MATERNO_PERS']	= utf8_encode(odbc_result($result_Perso_E,"APE_MATERNO_PERS"));
		$data['DatPersonal']['SEXO_PERS']	 		= utf8_encode(odbc_result($result_Perso_E,"SEXO_PERS"));
		$data['DatPersonal']['ID_ESTADO_CIVIL']	 	= utf8_encode(odbc_result($result_Perso_E,"ID_ESTADO_CIVIL"));
		$data['DatPersonal']['ID_GRADO_INSTRUCCION']	= utf8_encode(odbc_result($result_Perso_E,"ID_GRADO_INSTRUCCION"));
		$data['DatPersonal']['NRO_DNI']	 			= utf8_encode(odbc_result($result_Perso_E,"NRO_DNI"));
		$data['DatPersonal']['FEC_NACIMIENTO']	 	= cambiaf_a_normal(odbc_result($result_Perso_E,"FEC_NACIMIENTO"));
		$data['DatPersonal']['CORREO_PERSONAL']	 	= utf8_encode(odbc_result($result_Perso_E,"CORREO_PERSONAL"));
		$data['DatPersonal']['CORREO_SBN']	 	= utf8_encode(odbc_result($result_Perso_E,"CORREO_SBN"));
		$data['DatPersonal']['TEL_CELULAR']	 	= utf8_encode(odbc_result($result_Perso_E,"TEL_CELULAR"));
		$data['DatPersonal']['TEL_FIJO']	 	= utf8_encode(odbc_result($result_Perso_E,"TEL_FIJO"));
		$data['DatPersonal']['TEL_ANEXO']	 	= utf8_encode(odbc_result($result_Perso_E,"TEL_ANEXO"));
		$data['DatPersonal']['DISCAPACITADO']	= utf8_encode(odbc_result($result_Perso_E,"DISCAPACITADO"));
		$data['DatPersonal']['CONDICION_PERS']	= utf8_encode(odbc_result($result_Perso_E,"CONDICION_PERS"));
		$data['DatPersonal']['COD_DEPA_PERS']	= utf8_encode(odbc_result($result_Perso_E,"COD_DEPA_PERS"));
		$data['DatPersonal']['COD_PROV_PERS']	= utf8_encode(odbc_result($result_Perso_E,"COD_PROV_PERS"));
		$data['DatPersonal']['COD_DIST_PERS']	= utf8_encode(odbc_result($result_Perso_E,"COD_DIST_PERS"));
		$data['DatPersonal']['COD_ZONA']	 	= utf8_encode(odbc_result($result_Perso_E,"COD_ZONA"));
		$data['DatPersonal']['NOMBRE_ZONA']	 	= utf8_encode(odbc_result($result_Perso_E,"NOMBRE_ZONA"));
		$data['DatPersonal']['NOMBRE_MANZANA']	= utf8_encode(odbc_result($result_Perso_E,"NOMBRE_MANZANA"));
		$data['DatPersonal']['NRO_LOTE']	 	= utf8_encode(odbc_result($result_Perso_E,"NRO_LOTE"));
		$data['DatPersonal']['NRO_INT']	 		= utf8_encode(odbc_result($result_Perso_E,"NRO_INT"));
		$data['DatPersonal']['NRO_DEPART']	 	= utf8_encode(odbc_result($result_Perso_E,"NRO_DEPART"));
		$data['DatPersonal']['COD_TVIA']	 	= utf8_encode(odbc_result($result_Perso_E,"COD_TVIA"));
		$data['DatPersonal']['NOMBRE_VIA']	 	= utf8_encode(odbc_result($result_Perso_E,"NOMBRE_VIA"));
		$data['DatPersonal']['NRO_VIA']	 		= utf8_encode(odbc_result($result_Perso_E,"NRO_VIA"));
		$data['DatPersonal']['DIRECCION_PERS']	 	= utf8_encode(odbc_result($result_Perso_E,"DIRECCION_PERS"));
		$data['DatPersonal']['DIRRECCION_REF_PERS']	= utf8_encode(odbc_result($result_Perso_E,"DIRRECCION_REF_PERS"));
		$data['DatPersonal']['COD_BANCO']	 		= utf8_encode(odbc_result($result_Perso_E,"COD_BANCO"));
		$data['DatPersonal']['NRO_CTA_BANCO']	 	= utf8_encode(odbc_result($result_Perso_E,"NRO_CTA_BANCO"));
		$data['DatPersonal']['NRO_CTA_INTERBANCARIO']	= utf8_encode(odbc_result($result_Perso_E,"NRO_CTA_INTERBANCARIO"));
		$data['DatPersonal']['COD_AFP']	 		= utf8_encode(odbc_result($result_Perso_E,"COD_AFP"));
		$data['DatPersonal']['COD_CUSPP']	 	= utf8_encode(odbc_result($result_Perso_E,"COD_CUSPP"));
		$data['DatPersonal']['COD_SALUD']	 	= utf8_encode(odbc_result($result_Perso_E,"COD_SALUD"));
		$data['DatPersonal']['OBSERVACION']	 	= utf8_encode(odbc_result($result_Perso_E,"OBSERVACION"));
		$data['DatPersonal']['COD_MODALIDAD']	 	= utf8_encode(odbc_result($result_Perso_E,"COD_MODALIDAD"));
		
		
		$data['DatPersonal']['check_DISCAPACITADO'] 	= (utf8_encode(odbc_result($result_Perso_E,"DISCAPACITADO")) == '1') ? 'checked="checked"' : '';
		
		$RESULT_foto = $objPersonalSBNModel->LISTA_FOTO_PERSONAL_X_CODIGO($codPersonal);
		$NOM_FOTO_L	 	= odbc_result($RESULT_foto,"NOM_FOTO");
		
		if($NOM_FOTO_L !='') {
			$data['DatPersonal']['Edit_URL_foto'] = '../../Repositorio/intranet/foto_personal/'.$NOM_FOTO_L;			
		}else{
			$data['DatPersonal']['Edit_URL_foto'] = '../../Repositorio/intranet/foto_personal/image_perfil.jpg';
		}
		
		/********************************************************************/
		
		$rsEstadoCivil = $objPersonalSBNModel->Lista_Estado_Civil();
		while (odbc_fetch_row($rsEstadoCivil)) {			
			
			$ArrayEstCivil = array();			
			$ArrayEstCivil['ID_ESTADO_CIVIL'] 		= odbc_result($rsEstadoCivil, 'ID_ESTADO_CIVIL');
			$ArrayEstCivil['NOM_ESTADO_CIVIL'] 		= utf8_encode(odbc_result($rsEstadoCivil, 'NOM_ESTADO_CIVIL'));
			
			$data['EstadoCivil'][] = $ArrayEstCivil;
						
		}
		
		/********************************************************************/
		
		$rsGInstruccion = $objPersonalSBNModel->Lista_Grado_Instruccion();
		while (odbc_fetch_row($rsGInstruccion)) {			
			
			$ArrayInstruccion = array();			
			$ArrayInstruccion['ID_GRADO_INSTRUCCION'] 		= odbc_result($rsGInstruccion, 'ID_GRADO_INSTRUCCION');
			$ArrayInstruccion['NOM_GRADO_INSTRUCCION'] 		= utf8_encode(odbc_result($rsGInstruccion, 'NOM_GRADO_INSTRUCCION'));
			
			$data['GradoInstruccion'][] = $ArrayInstruccion;
						
		}
		
		/********************************************************************/
		
		$rsModalidad = $objPersonalSBNModel->Listar_Tipo_Modalidad();
		while (odbc_fetch_row($rsModalidad)) {			
			
			$ArrayModalidad = array();			
			$ArrayModalidad['COD_MODALIDAD'] 		= odbc_result($rsModalidad, 'COD_MODALIDAD');
			$ArrayModalidad['DESC_MODALIDAD'] 		= utf8_encode(odbc_result($rsModalidad, 'DESC_MODALIDAD'));
			
			$data['TipoModalidad'][] = $ArrayModalidad;
						
		}
		
		/********************************************************************/
		
		$rsDepartamento = $objUbigeoModel->Lista_Departamento();
		while (odbc_fetch_row($rsDepartamento)) {			
			
			$ArrayDepartamento = array();			
			$ArrayDepartamento['COD_DPTO'] 		= odbc_result($rsDepartamento, 'COD_DPTO');
			$ArrayDepartamento['DEPARTAMENTO'] 	= utf8_encode(odbc_result($rsDepartamento, 'DEPARTAMENTO'));
			
			$data['Departamento'][] = $ArrayDepartamento;
						
		}
		
		
		$rsProvincia = $objUbigeoModel->Lista_Provincia(odbc_result($result_Perso_E,"COD_DEPA_PERS"));
		while (odbc_fetch_row($rsProvincia)) {			
			
			$ArrayProvincia = array();			
			$ArrayProvincia['COD_PROV'] 	= odbc_result($rsProvincia, 'COD_PROV');
			$ArrayProvincia['PROVINCIA'] 	= utf8_encode(odbc_result($rsProvincia, 'PROVINCIA'));
			
			$data['Provincia'][] = $ArrayProvincia;
						
		}
		
		
		$rsDistrito = $objUbigeoModel->Lista_Distrito(odbc_result($result_Perso_E,"COD_DEPA_PERS"), odbc_result($result_Perso_E,"COD_PROV_PERS"));
		while (odbc_fetch_row($rsDistrito)) {			
			
			$ArrayDistrito = array();			
			$ArrayDistrito['COD_DIST'] 	= odbc_result($rsDistrito, 'COD_DIST');
			$ArrayDistrito['DISTRITO'] 	= utf8_encode(odbc_result($rsDistrito, 'DISTRITO'));
			
			$data['Distrito'][] = $ArrayDistrito;
						
		}
		
		/********************************************************************/
		
		$rsTipoZona = $objPersonalSBNModel->Lista_Tipo_Zona();
		while (odbc_fetch_row($rsTipoZona)) {			
			
			$ArrayTipoZona = array();			
			$ArrayTipoZona['COD_ZONA'] 		= odbc_result($rsTipoZona, 'COD_ZONA');
			$ArrayTipoZona['NOM_ZONA'] 	= utf8_encode(odbc_result($rsTipoZona, 'NOM_ZONA'));
			
			$data['TipoZona'][] = $ArrayTipoZona;
						
		}
		
		/********************************************************************/
		
		$rsTipoVia = $objPersonalSBNModel->Lista_Tipo_Via();
		while (odbc_fetch_row($rsTipoVia)) {			
			
			$ArrayTipoVia = array();			
			$ArrayTipoVia['COD_TVIA'] 		= odbc_result($rsTipoVia, 'COD_TVIA');
			$ArrayTipoVia['TXT_TVIA'] 		= utf8_encode(odbc_result($rsTipoVia, 'TXT_TVIA'));
			
			$data['TipoVia'][] = $ArrayTipoVia;
						
		}
		
		/********************************************************************/
		
		$rsBancos = $objPersonalSBNModel->Lista_Bancos();
		while (odbc_fetch_row($rsBancos)) {			
			
			$ArrayBancos = array();			
			$ArrayBancos['COD_BANCO'] 		= odbc_result($rsBancos, 'COD_BANCO');
			$ArrayBancos['NOMBRE_BANCO'] 	= utf8_encode(odbc_result($rsBancos, 'NOMBRE_BANCO'));
			
			$data['Bancos'][] = $ArrayBancos;
						
		}
		
		/********************************************************************/
		
		$rsSeguroAFP = $objPersonalSBNModel->Lista_Seguro_AFP();
		while (odbc_fetch_row($rsSeguroAFP)) {			
			
			$ArrayAFP = array();			
			$ArrayAFP['COD_AFP'] 		= odbc_result($rsSeguroAFP, 'COD_AFP');
			$ArrayAFP['DES_AFP'] 	= utf8_encode(odbc_result($rsSeguroAFP, 'DES_AFP'));
			
			$data['AFP'][] = $ArrayAFP;
						
		}
		
		/********************************************************************/
		
		$rsAREA = $objPersonalSBNModel->Listar_Areas_SBN();
		while (odbc_fetch_row($rsAREA)) {			
			
			$ArrayAREA = array();			
			$ArrayAREA['COD_AREA'] 	= odbc_result($rsAREA, 'COD_AREA');
			$ArrayAREA['NOM_AREA'] 	= utf8_encode(odbc_result($rsAREA, 'NOM_AREA'));
			
			$data['NOM_AREAS'][] = $ArrayAREA;
						
		}
		
	
		/********************************************************************/
		
		$rsCargos_Pers = $objPersonalSBNModel->Listar_Cargos_Pers_x_IDPers($codPersonal);
		while (odbc_fetch_row($rsCargos_Pers)) {			
			
			$ArrayCarPers = array();			
			$ArrayCarPers['COD_PERS_CARGO'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'COD_PERS_CARGO'));
			$ArrayCarPers['NOM_AREA'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_AREA'));
			$ArrayCarPers['NOM_DIVISION'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_DIVISION'));
			$ArrayCarPers['NOM_UNIDAD'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_UNIDAD'));
			$ArrayCarPers['ABREVIATURA'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'ABREVIATURA'));
			$ArrayCarPers['NOM_CARGO'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_CARGO'));
			$ArrayCarPers['NRO_PLAZA'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NRO_PLAZA'));
			$ArrayCarPers['ESTADO_ACTIVO'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'ESTADO_ACTIVO'));
			
			$data['Cargos_Pers'][] = $ArrayCarPers;
						
		}
		
				
		//$this->dump($data['xDATPERSID']);
		$this->render('v.form_personal.php', $data);
	}


	public function Guardar_Datos_Personal(){
		
		$data = array();
		
		$objPersonalSBNModel 	= new PersonalSBNModel();
		
		$USUARIO = isset($_GET['TXH_ID_USUARIO']) ? $_GET['TXH_ID_USUARIO'] : '';
		$txt_cod_personal_x1 = isset($_GET['txt_cod_personal_x1']) ? $_GET['txt_cod_personal_x1'] : '';
		$NOMBRES_PERS = isset($_GET['txt_nom_pers_x1']) ? convertir_a_utf8($_GET['txt_nom_pers_x1']) : '';
		$APE_PATERNO_PERS = isset($_GET['txt_apepat_pers_x1']) ? convertir_a_utf8($_GET['txt_apepat_pers_x1']) : '';
		$APE_MATERNO_PERS = isset($_GET['txt_apemat_pers_x1']) ? convertir_a_utf8($_GET['txt_apemat_pers_x1']) : '';
		$SEXO_PERS = isset($_GET['cbo_sex_reg']) ? $_GET['cbo_sex_reg'] : '';
		$ID_ESTADO_CIVIL = isset($_GET['cbob_est_civil_reg']) ? $_GET['cbob_est_civil_reg'] : '';
		$ID_GRADO_INSTRUCCION = isset($_GET['cbo_grad_instrucc']) ? $_GET['cbo_grad_instrucc'] : '';
		$NRO_DNI = isset($_GET['txt_nro_dni_pers_x1']) ? convertir_a_utf8($_GET['txt_nro_dni_pers_x1']) : '';
		$FEC_NACIMIENTO = isset($_GET['txt_fech_nac_pers_x1']) ? cambiar_Fecha_a_SqlServer($_GET['txt_fech_nac_pers_x1']) : '';
		$CORREO_PERSONAL = isset($_GET['txt_correo_personal']) ? $_GET['txt_correo_personal'] : '';
		$CORREO_SBN = isset($_GET['txt_correo_sbn']) ? convertir_a_utf8($_GET['txt_correo_sbn']) : '';
		$TEL_FIJO = isset($_GET['txt_tel_fijo_pers_x1']) ? $_GET['txt_tel_fijo_pers_x1'] : '';
		$TEL_CELULAR = isset($_GET['txt_tel_cel_pers_x1']) ? $_GET['txt_tel_cel_pers_x1'] : '';
		$TEL_ANEXO = isset($_GET['txt_anexo_pers_x1']) ? $_GET['txt_anexo_pers_x1'] : '';
		$DISCAPACITADO = isset($_GET['val_check_discapacitado']) ? $_GET['val_check_discapacitado'] : '';
		$CONDICION_PERS = isset($_GET['cbo_CONDIC_ESTADO']) ? $_GET['cbo_CONDIC_ESTADO'] : '';
		$COD_DEPA_PERS = isset($_GET['cbo_departamento']) ? $_GET['cbo_departamento'] : '';
		$COD_PROV_PERS = isset($_GET['cbo_provincia']) ? $_GET['cbo_provincia'] : '';
		$COD_DIST_PERS = isset($_GET['cbo_distrito']) ? $_GET['cbo_distrito'] : '';
		$COD_ZONA = isset($_GET['cbo_TIP_ZONA']) ? $_GET['cbo_TIP_ZONA'] : '';
		$NOMBRE_ZONA = isset($_GET['txt_nom_zona']) ? convertir_a_utf8($_GET['txt_nom_zona']) : '';
		$NOMBRE_MANZANA = isset($_GET['txt_desc_mz']) ? convertir_a_utf8($_GET['txt_desc_mz']) : '';
		$NRO_LOTE = isset($_GET['txt_desc_lote']) ? convertir_a_utf8($_GET['txt_desc_lote']) : '';
		$NRO_INT = isset($_GET['txt_desc_interior']) ? convertir_a_utf8($_GET['txt_desc_interior']) : '';
		$NRO_DEPART = isset($_GET['txt_nro_depa']) ? convertir_a_utf8($_GET['txt_nro_depa']) : '';
		$COD_TVIA = isset($_GET['cbo_tip_via']) ? convertir_a_utf8($_GET['cbo_tip_via']) : '';
		$NOMBRE_VIA = isset($_GET['txt_nom_via']) ? convertir_a_utf8($_GET['txt_nom_via']) : '';
		$NRO_VIA = isset($_GET['txt_numero_via']) ? convertir_a_utf8($_GET['txt_numero_via']) : '';
		$DIRECCION_PERS = isset($_GET['txt_direccion']) ? convertir_a_utf8($_GET['txt_direccion']) : '';
		$DIRRECCION_REF_PERS = isset($_GET['txt_direccion_referencia']) ? convertir_a_utf8($_GET['txt_direccion_referencia']) : '';
		$COD_BANCO = isset($_GET['cbo_banco']) ? $_GET['cbo_banco'] : '';
		$NRO_CTA_BANCO = isset($_GET['txt_nro_cta_banc']) ? convertir_a_utf8($_GET['txt_nro_cta_banc']) : '';
		$NRO_CTA_INTERBANCARIO = isset($_GET['txt_nro_cta_interbanc']) ? convertir_a_utf8($_GET['txt_nro_cta_interbanc']) : '';
		$COD_AFP = isset($_GET['cbo_Seguro_AFP']) ? $_GET['cbo_Seguro_AFP'] : '';
		$COD_CUSPP = isset($_GET['TXT_COD_CUSPP']) ? convertir_a_utf8($_GET['TXT_COD_CUSPP']) : '';
		$COD_SALUD = isset($_GET['TXT_COD_SALUD']) ? convertir_a_utf8($_GET['TXT_COD_SALUD']) : '';
		$OBSERVACION = isset($_GET['txt_observacion']) ? convertir_a_utf8($_GET['txt_observacion']) : '';
		$COD_MODALIDAD = isset($_GET['cbo_TIP_MODALIDAD']) ? convertir_a_utf8($_GET['cbo_TIP_MODALIDAD']) : '';
		
		$ID_ESTADO 	= '1';
				
		/*********************************************************/
		
		if($txt_cod_personal_x1 !=''){	
		
			$COD_PERSONAL	= 	$txt_cod_personal_x1;
			
			$result_Reg_Perso = $objPersonalSBNModel->ACTUALIZAR_DATOS_DEL_PERSONAL($COD_PERSONAL, $NOMBRES_PERS, $APE_MATERNO_PERS, $APE_PATERNO_PERS, $SEXO_PERS, $ID_ESTADO_CIVIL, $ID_GRADO_INSTRUCCION, $NRO_DNI, $FEC_NACIMIENTO, $CORREO_PERSONAL, $CORREO_SBN, $TEL_FIJO, $TEL_CELULAR, $TEL_ANEXO, $DISCAPACITADO, $CONDICION_PERS, $COD_DEPA_PERS, $COD_PROV_PERS, $COD_DIST_PERS, $COD_ZONA, $NOMBRE_ZONA, $NOMBRE_MANZANA, $NRO_LOTE, $NRO_INT, $NRO_DEPART, $COD_TVIA, $NOMBRE_VIA, $NRO_VIA, $DIRECCION_PERS, $DIRRECCION_REF_PERS, $COD_BANCO, $NRO_CTA_BANCO, $NRO_CTA_INTERBANCARIO, $COD_AFP, $COD_CUSPP, $COD_SALUD, $OBSERVACION, $USUARIO, $ID_ESTADO, $COD_MODALIDAD);
			
			$valor = (isset($result_Reg_Perso)) ? '1' : '0';
			
		}else{
			
			$RESULT_Existe		=	$objPersonalSBNModel->VERIFICA_SI_EXISTE_DATOS_PERSONALES($NRO_DNI);		
			$tot_registro_pers	= 	odbc_result($RESULT_Existe,"total_registro");
		
			if($tot_registro_pers == 0){
			
				$RESULT_Pers	=	$objPersonalSBNModel->GENERAR_CODIGO_PERSONAL();			
				$COD_PERSONAL	= 	odbc_result($RESULT_Pers,"CODIGO");
			
				$result_Reg_Perso = $objPersonalSBNModel->INSERTAR_DATOS_DEL_PERSONAL($COD_PERSONAL, $NOMBRES_PERS, $APE_MATERNO_PERS, $APE_PATERNO_PERS, $SEXO_PERS, $ID_ESTADO_CIVIL, $ID_GRADO_INSTRUCCION, $NRO_DNI, $FEC_NACIMIENTO, $CORREO_PERSONAL, $CORREO_SBN, $TEL_FIJO, $TEL_CELULAR, $TEL_ANEXO, $DISCAPACITADO, $CONDICION_PERS, $COD_DEPA_PERS, $COD_PROV_PERS, $COD_DIST_PERS, $COD_ZONA, $NOMBRE_ZONA, $NOMBRE_MANZANA, $NRO_LOTE, $NRO_INT, $NRO_DEPART, $COD_TVIA, $NOMBRE_VIA, $NRO_VIA, $DIRECCION_PERS, $DIRRECCION_REF_PERS, $COD_BANCO, $NRO_CTA_BANCO, $NRO_CTA_INTERBANCARIO, $COD_AFP, $COD_CUSPP, $COD_SALUD, $OBSERVACION, $USUARIO, $ID_ESTADO, $COD_MODALIDAD);

				// CREACION DE ACCESOS BASICOS DEFAULT
				$rdo 					=	new Encryption();
				$Crypttext				= (string)($rdo->enviarCrypttext())."";	
				$objPersonalSBNModel->INSERTAR_DATOS_DEL_PERSONAL_ACCESO($Crypttext,$NRO_DNI);

				
				$valor = (isset($result_Reg_Perso)) ? '1' : '0';
				
			}else{
				$valor = '5';
			}
		}
		
		
		echo $valor;
		//$this->dump($valor);
	}
	
	
	public function Adjuntar_Foto_Personal(){
		
		$data = array();
		
		$objPersonalSBNModel 	= new PersonalSBNModel();
		
		$COD_PERSONAL = isset($_REQUEST['txt_cod_personal_x1']) ? $_REQUEST['txt_cod_personal_x1'] : '';
		$DNI_PERSONAL = isset($_REQUEST['txt_nro_dni_pers_x1']) ? $_REQUEST['txt_nro_dni_pers_x1'] : '';
		
		//=============== Directorio  =================================================
	
		$UPLOADDIR='../../../../Repositorio/intranet/foto_personal';
		//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
		
		$ARCHIVO					= $_FILES['txt_file_foto']['tmp_name'];
		$ARCHIVO_NOMBRE				= basename($_FILES['txt_file_foto']['name']);
		$ARCHIVO_PESO				= $_FILES['txt_file_foto']['size'];
		$ARCHIVO_TIPO				= $_FILES['txt_file_foto']['type'];
		$ARCHIVO_EXTENSION			= extension_archivo($_FILES['txt_file_foto']['name']);
		
		$NUEVA_CADENA				= substr( md5(microtime()), 1, 4);
		
		$ARCHIVO_NOMBRE_GENERADO	= 'F_'.$COD_PERSONAL.'_'.$DNI_PERSONAL.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
		$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;
		
		if (move_uploaded_file($ARCHIVO, $UPLOADFILE)) {
			
			$rsFoto	=	$objPersonalSBNModel->VERIFICA_EXISTE_FOTO_PERSONAL($COD_PERSONAL);
			$TOT_REG = odbc_result($rsFoto,"TOT_REG");
			
			if($TOT_REG == 0){
				$RESULT2	=	$objPersonalSBNModel->INSERTAR_FOTO_PERSONAL($COD_PERSONAL, $ARCHIVO_NOMBRE_GENERADO, $ARCHIVO_PESO);
			}else{
				$RESULT2	=	$objPersonalSBNModel->ACTUALIZAR_FOTO_PERSONAL($COD_PERSONAL, $ARCHIVO_NOMBRE_GENERADO, $ARCHIVO_PESO);
			}
			
			$valor = ($RESULT2) ? '1'.'[*]'.$COD_PERSONAL.'[*]'.$ARCHIVO_NOMBRE_GENERADO : 0 ;
		}else{
			$valor = 9;	
		}
		
		echo $valor;
		//$this->dump($valor);
		
	}
	
	
	//AAG INICIO
	public function Adjuntar_Foto_catalogo(){
		
		$data = array();
		
		$objPersonalSBNModel 	= new PersonalSBNModel();
		
		$COD_CATALOGO = isset($_REQUEST['txt_cod_catalogo_x1']) ? $_REQUEST['txt_cod_catalogo_x1'] : '';
		
		//=============== Directorio  =================================================
	
		$UPLOADDIR='../../../../repositorio_muebles/Inventarios_zip/foto_catalogo/';
		// $UPLOADDIR='../../../../Repositorio/intranet/foto_catalogo';
		//if(!is_dir($UPLOADDIR)) mkdir($UPLOADDIR, 0777);
		
		$ARCHIVO					= $_FILES['txt_file_foto']['tmp_name'];
		$ARCHIVO_NOMBRE				= basename($_FILES['txt_file_foto']['name']);
		$ARCHIVO_PESO				= $_FILES['txt_file_foto']['size'];
		$ARCHIVO_TIPO				= $_FILES['txt_file_foto']['type'];
		$ARCHIVO_EXTENSION			= extension_archivo($_FILES['txt_file_foto']['name']);
		
		$NUEVA_CADENA				= substr( md5(microtime()), 1, 4);
		
		$ARCHIVO_NOMBRE_GENERADO	= 'F_'.$COD_CATALOGO.'_'.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
		$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;
		//   echo($UPLOADFILE.'  '); 
		// if (!file_exists($UPLOADFILE)) {
		// 	echo('false dasda');
		// }else{
		// 	echo('true dasda');
		// }
		// die();

		if (move_uploaded_file($ARCHIVO, $UPLOADFILE)) {
			
			$rsFoto	=	$objPersonalSBNModel->VERIFICA_EXISTE_FOTO_CATALOGO($COD_CATALOGO);
			$TOT_REG = odbc_result($rsFoto,"TOT_REG");
			
			if($TOT_REG == 0){
				$RESULT2	=	$objPersonalSBNModel->INSERTAR_FOTO_CATALOGO($COD_CATALOGO, $ARCHIVO_NOMBRE_GENERADO, $ARCHIVO_PESO);
			}else{
				$RESULT2	=	$objPersonalSBNModel->ACTUALIZAR_FOTO_CATALOGO($COD_CATALOGO, $ARCHIVO_NOMBRE_GENERADO, $ARCHIVO_PESO);
			}
			
			$valor = ($RESULT2) ? '1'.'[*]'.$COD_CATALOGO.'[*]'.$ARCHIVO_NOMBRE_GENERADO : 0 ;
		}else{
			$valor = 9;	
		}
		
		echo $valor;		
	}
	//AAG FIN
	
	public function Buscar_Personal_en_SID(){
		
		$objPersonalSBN_SINABIPModel 	= new PersonalSBN_SINABIPModel();
		$objPersonalSBNModel 	= new PersonalSBNModel();
		
		$data = array();
    	//$codigoEntidad = $_SESSION['th_SIMI_COD_ENTIDAD'];
	
		$txb_nombes = isset($_GET['txt_busc2_nom_pers_x1']) ? $_GET['txt_busc2_nom_pers_x1'] : '';
		$txb_apepat = isset($_GET['txt_busc2_apepat_pers_x1']) ? $_GET['txt_busc2_apepat_pers_x1'] : '';
		$txb_apemat = isset($_GET['txt_busc2_apemat_pers_x1']) ? $_GET['txt_busc2_apemat_pers_x1'] : '';
		$txt_cod_pers_SID = isset($_GET['txt_cod_pers_SID']) ? $_GET['txt_cod_pers_SID'] : '';
		
			
		$pageNumber = isset($_GET['numeroPagina_x2']) ? $_GET['numeroPagina_x2'] : 1;
		
		$xNRO_FILA_LIST 	= '30';
		$xNRO_LINK_PAGE 	= '6';
		
		/*********************************************************/
		
		$RS = $objPersonalSBN_SINABIPModel->TOTAL_REGISTRO_PERSONAL_SID_X_PARAMETROS($txb_nombes, $txb_apepat, $txb_apemat);
		$Total_Registros = odbc_result($RS,"TOT_REG");
		
		$Total_Paginas = ceil($Total_Registros / $xNRO_FILA_LIST);
		
		
		$data['pager_2']['txt_cod_pers_SID'] 		= $txt_cod_pers_SID;
		$data['pager_2']['indicePagina'] 		= $pageNumber;
		$data['pager_2']['cantidadRegistros'] = $Total_Registros;
		$data['pager_2']['cantidadPaginas'] 	= $Total_Paginas;	
		$data['pager_2']['limiteInferior'] 	= (($pageNumber - $xNRO_LINK_PAGE) <= 0) ?  1 : ($pageNumber - $xNRO_LINK_PAGE);
		$data['pager_2']['limiteSuperior'] 	= (($pageNumber + $xNRO_LINK_PAGE) > $Total_Paginas) ? $Total_Paginas : ($pageNumber + $xNRO_LINK_PAGE);
		$data['pager_2']['handlerFunction'] 	= "handle_paginar_Personal_SID";	

					
		/*********************************************************/
		
		$Item_Ini = ($pageNumber == '1')? (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST) : (($pageNumber * $xNRO_FILA_LIST) - $xNRO_FILA_LIST)+1 ;
		$Item_Fin = ($pageNumber == '1')? ($Item_Ini + $xNRO_FILA_LIST) : ($Item_Ini + $xNRO_FILA_LIST)-1;

		$Result_Personal = $objPersonalSBN_SINABIPModel->LISTA_PERSONAL_SID_X_PARAMETROS($Item_Ini, $Item_Fin, $txb_nombes, $txb_apepat, $txb_apemat);
		while (odbc_fetch_row($Result_Personal)){
						
			$personalSID = array();

			$personalSID['SID_ROW_NUMBER_ID'] 		= utf8_encode(odbc_result($Result_Personal,"ROW_NUMBER_ID"));
			$personalSID['SID_codigo_personal'] 	= utf8_encode(odbc_result($Result_Personal,"codigo_personal"));
			$personalSID['SID_primer_apellido'] 	= utf8_encode(odbc_result($Result_Personal,"primer_apellido"));
			$personalSID['SID_segundo_apellido'] 	= utf8_encode(odbc_result($Result_Personal,"segundo_apellido"));
			$personalSID['SID_primer_nombre'] 		= utf8_encode(odbc_result($Result_Personal,"primer_nombre"));
			$personalSID['SID_segundo_nombre'] 		= utf8_encode(odbc_result($Result_Personal,"segundo_nombre"));
			$personalSID['SID_DESC_DIRECION'] 		= utf8_encode(odbc_result($Result_Personal,"DESC_DIRECION"));
			
			$Result_PersonalSID_Existe = $objPersonalSBNModel->TOTAL_REGISTRO_SI_EXISTE_COD_PERSONAL_SID(utf8_encode(odbc_result($Result_Personal,"codigo_personal")));
			$personalSID['SID_NRO_REG'] 			= utf8_encode(odbc_result($Result_PersonalSID_Existe,"NRO_REG"));

			$data['personalSID'][] = $personalSID;
			
		}
		
		$this->render('v.dialog_pers_list.php', $data);
   }
	
	
	
	public function Mostrar_Division(){
		
		$objPersonalSBNModel 	= new PersonalSBNModel();
		
		$data = array();
	
		$COD_AREA = isset($_GET['Cbo_Reg_Area']) ? $_GET['Cbo_Reg_Area'] : '';
		
		$Result_Div = $objPersonalSBNModel->Listar_Division_SBN_X_Area($COD_AREA);
		while (odbc_fetch_row($Result_Div)){
						
			$Division = array();

			$Division['COD_AREA_DIV'] 	= utf8_encode(odbc_result($Result_Div,"COD_AREA_DIV"));
			$Division['NOM_DIVISION'] 		= utf8_encode(odbc_result($Result_Div,"NOM_DIVISION"));
	
			$data['ListaDivision'][] = $Division;
			
		}
		
		print_r(json_encode($data['ListaDivision']));
		//print_r($data['ListaDivision']);
	}
	
	public function Mostrar_Unidad(){
		
		$objPersonalSBNModel 	= new PersonalSBNModel();
		
		$data = array();
	
		$COD_AREA_DIV = isset($_GET['Cbo_Reg_Division']) ? $_GET['Cbo_Reg_Division'] : '';
		
		$Result_Unid = $objPersonalSBNModel->Listar_Unidad_SBN_X_Division($COD_AREA_DIV);
		while (odbc_fetch_row($Result_Unid)){
						
			$Unidad = array();

			$Unidad['COD_AREA_DIV_UNID'] 	= utf8_encode(odbc_result($Result_Unid,"COD_AREA_DIV_UNID"));
			$Unidad['NOM_UNIDAD'] 			= utf8_encode(odbc_result($Result_Unid,"NOM_UNIDAD"));
			$Unidad['ABREVIATURA'] 			= utf8_encode(odbc_result($Result_Unid,"ABREVIATURA"));
	
			$data['ListaUnidad'][] = $Unidad;
			
		}
		
		print_r(json_encode($data['ListaUnidad']));
		//print_r($data['ListaDivision']);
	}
	
	public function Mostrar_Cargo(){
		
		$objPersonalSBNModel 	= new PersonalSBNModel();
		
		$data = array();
	
		$COD_AREA_DIV_UNID = isset($_GET['Cbo_Reg_Unidad']) ? $_GET['Cbo_Reg_Unidad'] : '';
		
		$Result_Cargo = $objPersonalSBNModel->Listar_Cargo_SBN_X_Unidad($COD_AREA_DIV_UNID);
		while (odbc_fetch_row($Result_Cargo)){
						
			$Cargos = array();

			$Cargos['COD_CARGO'] 	= utf8_encode(odbc_result($Result_Cargo,"COD_CARGO"));
			$Cargos['NOM_CARGO'] 	= utf8_encode(odbc_result($Result_Cargo,"NOM_CARGO"));
	
			$data['ListaCargo'][] = $Cargos;
			
		}
		
		print_r(json_encode($data['ListaCargo']));

	}
	
	
	
	public function Agregar_Cargo_Personal(){
		
		$data = array();
		
		$objPersonalSBNModel 	= new PersonalSBNModel();
		
		$COD_PERSONAL 		= isset($_REQUEST['sCOD_PERSONAL']) ? $_REQUEST['sCOD_PERSONAL'] : '';
		$COD_AREA 			= isset($_REQUEST['Cbo_Reg_Area']) ? $_REQUEST['Cbo_Reg_Area'] : '';
		$COD_AREA_DIV 		= isset($_REQUEST['Cbo_Reg_Division']) ? $_REQUEST['Cbo_Reg_Division'] : '';
		$COD_AREA_DIV_UNID 	= isset($_REQUEST['Cbo_Reg_Unidad']) ? $_REQUEST['Cbo_Reg_Unidad'] : '';
		$COD_CARGO 			= isset($_REQUEST['Cbo_Reg_Cargo']) ? $_REQUEST['Cbo_Reg_Cargo'] : '';
	
		$rsCOD	=	$objPersonalSBNModel->GENERAR_CODIGO_CARGO_PERSONAL();
		$COD_PERS_CARGO = odbc_result($rsCOD,"CODIGO");
	
		$RESULT2	=	$objPersonalSBNModel->INSERTAR_CARGO_PERSONAL($COD_PERS_CARGO, $COD_PERSONAL, $COD_AREA, $COD_AREA_DIV, $COD_AREA_DIV_UNID, $COD_CARGO);
		
		$valor = ($RESULT2) ? '1' : 0 ;
		
	
		$rsCargos_Pers = $objPersonalSBNModel->Listar_Cargos_Pers_x_IDPers($COD_PERSONAL);
		while (odbc_fetch_row($rsCargos_Pers)) {			
			
			$ArrayCarPers = array();			
			$ArrayCarPers['COD_PERS_CARGO'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'COD_PERS_CARGO'));
			$ArrayCarPers['NOM_AREA'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_AREA'));
			$ArrayCarPers['NOM_DIVISION'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_DIVISION'));
			$ArrayCarPers['NOM_UNIDAD'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_UNIDAD'));
			$ArrayCarPers['ABREVIATURA'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'ABREVIATURA'));
			$ArrayCarPers['NOM_CARGO'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_CARGO'));
			$ArrayCarPers['NRO_PLAZA'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NRO_PLAZA'));
			$ArrayCarPers['ESTADO_ACTIVO'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'ESTADO_ACTIVO'));
			
			$data['Cargos_Pers'][] = $ArrayCarPers;
						
		}
		
				
		//$this->dump($data['xDATPERSID']);
		$this->render('v.form_personal_cargo.tpl.php', $data);
				
	}
	
	
	public function Habilitar_Cargo_Pers(){
		
		$data = array();
		
		$objPersonalSBNModel 	= new PersonalSBNModel();
		
		$COD_PERSONAL 		= isset($_REQUEST['txt_cod_personal_x1']) ? $_REQUEST['txt_cod_personal_x1'] : '';
		$COD_PERS_CARGO		= isset($_REQUEST['sCOD_PERS_CARGO']) ? $_REQUEST['sCOD_PERS_CARGO'] : '';
	
		$RESULT2	=	$objPersonalSBNModel->ACTIVAR_CARGO_PERSONAL($COD_PERSONAL, $COD_PERS_CARGO);
				
	
		$rsCargos_Pers = $objPersonalSBNModel->Listar_Cargos_Pers_x_IDPers($COD_PERSONAL);
		while (odbc_fetch_row($rsCargos_Pers)) {			
			
			$ArrayCarPers = array();			
			$ArrayCarPers['COD_PERS_CARGO'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'COD_PERS_CARGO'));
			$ArrayCarPers['NOM_AREA'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_AREA'));
			$ArrayCarPers['NOM_DIVISION'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_DIVISION'));
			$ArrayCarPers['NOM_UNIDAD'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_UNIDAD'));
			$ArrayCarPers['ABREVIATURA'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'ABREVIATURA'));
			$ArrayCarPers['NOM_CARGO'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_CARGO'));
			$ArrayCarPers['NRO_PLAZA'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NRO_PLAZA'));
			$ArrayCarPers['ESTADO_ACTIVO'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'ESTADO_ACTIVO'));
			
			$data['Cargos_Pers'][] = $ArrayCarPers;
						
		}
		
				
		//$this->dump($data['xDATPERSID']);
		$this->render('v.form_personal_cargo.tpl.php', $data);
				
	}
	
	public function Eliminar_Cargo_Pers(){
		
		$data = array();
		
		$objPersonalSBNModel 	= new PersonalSBNModel();
		
		$COD_PERSONAL 		= isset($_REQUEST['txt_cod_personal_x1']) ? $_REQUEST['txt_cod_personal_x1'] : '';
		$COD_PERS_CARGO		= isset($_REQUEST['sCOD_PERS_CARGO']) ? $_REQUEST['sCOD_PERS_CARGO'] : '';
	
		$RESULT2	=	$objPersonalSBNModel->ELIMINAR_CARGO_PERSONAL($COD_PERS_CARGO);
				
	
		$rsCargos_Pers = $objPersonalSBNModel->Listar_Cargos_Pers_x_IDPers($COD_PERSONAL);
		while (odbc_fetch_row($rsCargos_Pers)) {			
			
			$ArrayCarPers = array();			
			$ArrayCarPers['COD_PERS_CARGO'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'COD_PERS_CARGO'));
			$ArrayCarPers['NOM_AREA'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_AREA'));
			$ArrayCarPers['NOM_DIVISION'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_DIVISION'));
			$ArrayCarPers['NOM_UNIDAD'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_UNIDAD'));
			$ArrayCarPers['ABREVIATURA'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'ABREVIATURA'));
			$ArrayCarPers['NOM_CARGO'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NOM_CARGO'));
			$ArrayCarPers['NRO_PLAZA'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'NRO_PLAZA'));
			$ArrayCarPers['ESTADO_ACTIVO'] 	= utf8_encode(odbc_result($rsCargos_Pers, 'ESTADO_ACTIVO'));
			
			$data['Cargos_Pers'][] = $ArrayCarPers;
						
		}
		
				
		//$this->dump($data['xDATPERSID']);
		$this->render('v.form_personal_cargo.tpl.php', $data);
				
	}
	

	/*
	function load_template($title='Sin Titulo'){
	  $pagina = $this->load_page('page.php');
	  $pagina = $this->replace_content('/\#TITLE_CONTENT\#/ms' ,$title , $pagina);
	  //$buscar_cab = $this->load_page('v.pri_buscar.php');
	  //$pagina = $this->replace_content('/\#BUSC_CAB_CONTENT\#/ms' ,$buscar_cab , $pagina);
	  return $pagina;
	}
	
	private function load_page($page){
  		return file_get_contents($page);
	}
	
	private function replace_content($in='/\#FORM_CONTENT\#/ms', $out,$pagina){
		return preg_replace($in, $out, $pagina);
	}
	
	private function view_page($html){
  		echo $html;
	}
	*/
	
	
  /**
   * Renderiza la plantilla, con los valores indicados.
   * @param $template Nombre de la plantilla, debe ser el nombre de el archivo plantilla.
   * @param $values   Array con los datos que seran pasados a la plantilla.
   *
   * @return Template renderizado con los valores de las variables.
   */
  public function render($template, $values){
      $render = function () use ($template, $values){
          $data = $values;
          include "$template";
      };
      return $render();
  }

  /**
   * Imprime una variable respetando sangrias y espaciado, con la funcion printr.
   * @param $data Valor a imprimir en la pantalla del navegador.
   * @return Nothing.
   */
  public function dump($data){
      print '<pre>';
      print_r($data);
      print '</pre>';
  }


}


?>