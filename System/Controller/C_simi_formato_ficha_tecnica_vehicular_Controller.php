<?php
	
	
	class C_simi_formato_ficha_tecnica_vehicular_Controller
	{
		
		private $ObjModel;
		
		
		function  __construct(){
		
			$this->ObjModel = new M_simi_formato_ficha_tecnica_vehicular_Model();
		
		}
		
		
		/* Metodo Total Filas */
		function F_Total_Buscar_BienesVehiculos_Parametros_Gral($COD_ENTIDAD, $COD_PATRIMONIAL, $DENOMINACION){
		
			return  $this->ObjModel->F_Total_Buscar_BienesVehiculos_Parametros_Gral($COD_ENTIDAD, $COD_PATRIMONIAL, $DENOMINACION);
		
		}
		
		/* Metodo Listar */
		function F_Lista_Buscar_Bienes_Vehiculos_x_Entidad_Parametros_Gral($INI, $FIN, $COD_ENTIDAD, $COD_PATRIMONIAL, $DENOMINACION){
		
			return $this->ObjModel->F_Lista_Buscar_Bienes_Vehiculos_x_Entidad_Parametros_Gral($INI, $FIN, $COD_ENTIDAD, $COD_PATRIMONIAL, $DENOMINACION);
		
		}
		
		
		//Metodo Listar
		function F_Listado_Entidades(){
		
			return $this->ObjModel->F_Listado_Entidades();
		
		}
		
		
		
		//4- Metodo Total Muebles bienes Patrimoniales-Vehiculos
		function F_Total_Muebles_BienesPatrimonialVehiculos_x_Entidad($COD_ENTIDAD, $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN){
		
			return  $this->ObjModel->F_Total_Muebles_BienesPatrimonialVehiculos_x_Entidad($COD_ENTIDAD, $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN);
		
		}
		
		
		
		//5- Metodo Listar
		function F_Lista_Muebles_BienesPatrimonialVehiculos_x_Entidad($INI, $FIN, $COD_ENTIDAD, $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN){
		
			return $this->ObjModel->F_Lista_Muebles_BienesPatrimonialVehiculos_x_Entidad($INI, $FIN, $COD_ENTIDAD, $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN);
		
		}
		
		
		//6- Metodo Insertar TBL_MUEBLES_UE_BIE_FT_VEHICULAR
		function F_Insertar_TBL_MUEBLES_UE_BIE_FT_VEHICULAR
		(
				$COD_UE_FT_VEHICULAR_hide,		$COD_USUARIO_hide,
				$COD_ENTIDAD,					$COD_UE_BIEN_PATRI,				$DENOMINACION,
				$PLACA,							$CARROCERIA,					$MARCA,
				$MODELO,						$CATEGORIA,						$NRO_CHASIS,
				$NRO_EJES,						$NRO_MOTOR,						$NRO_SERIE,
				$ANIO_FABRICACION,				$COLOR,							$COMBUSTIBLE,
				$TRANSMISION,					$CILINDRADA,					$KILOMETRAJE,
				$TARJETA_PROPIEDAD,				$CILINDROS,						$B_CILINDROS,
				$CARBURADOR,					$B_CARBURADOR,					$DISTRIBUIDOR,
				$B_DISTRIBUIDOR,				$BOMBA_GASOLINA,				$B_BOMBA_GASOLINA,
				$PURIFICADOR_AIRE,				$B_PURIFICADOR_AIRE,			$BOMBA_FRENOS,
				$B_BOMBA_FRENOS,				$ZAPATAS_TAMBORES,				$B_ZAPATAS_TAMBORES,
				$DISCOS_PASTILLAS,				$B_DISCOS_PASTILLAS,			$RADIOADOR,
				$B_RADIOADOR,					$VENTILADOR,					$B_VENTILADOR,
				$BOMBA_AGUA,					$B_BOMBA_AGUA,					$MOTOR_ARRANQUE,
				$B_MOTOR_ARRANQUE,				$BATERIA,						$B_BATERIA,
				$ALTERNADOR,					$B_ALTERNADOR,					$BOBINA,
				$B_BOBINA,						$RELAY_ALTERNADOR,				$B_RELAY_ALTERNADOR,
				$FAROS_DELANTEROS,				$B_FAROS_DELANTEROS,			$DIRECCIONALES_DELANTERAS,
				$B_DIRECCIONALES_DELANTERAS,	$LUCES_POSTERIORES,				$B_LUCES_POSTERIORES,
				$DIRECCIONALES_POSTERIORES,		$B_DIRECCIONALES_POSTERIORES,	$AUTO_RADIO,
				$B_AUTO_RADIO,					$PARLANTES,						$B_PARLANTES,
				$CLAXON,						$B_CLAXON,						$CIRCUITO_LUCES,
				$B_CIRCUITO_LUCES,				$CAJA_CAMBIOS,					$B_CAJA_CAMBIOS,
				$BOMBA_EMBRAGUE,				$B_BOMBA_EMBRAGUE,				$CAJA_TRANSFERENCIA,
				$B_CAJA_TRANSFERENCIA,			$DIFERENCIAL_TRASERO,			$B_DIFERENCIAL_TRASERO,
				$DIFERENCIAL_DELANTERO,			$B_DIFERENCIAL_DELANTERO,		$VOLANTE,
				$B_VOLANTE,						$CANA_DIRECCION,				$B_CANA_DIRECCION,
				$CREMALLERA,					$B_CREMALLERA,					$ROTULAS,
				$B_ROTULAS,						$AMORTIGUADORES,				$B_AMORTIGUADORES,
				$BARRA_TORSION,					$B_BARRA_TORSION,				$BARRA_ESTABILIZADOR,
				$B_BARRA_ESTABILIZADOR,			$LLANTAS,						$B_LLANTAS,
				$CAPOT_MOTOR,					$B_CAPOT_MOTOR,					$CAPOT_MALETERA,
				$B_CAPOT_MALETERA,				$PARACHOQUES_DELANTEROS,		$B_PARACHOQUES_DELANTEROS,
				$PARACHOQUES_POSTERIORES,		$B_PARACHOQUES_POSTERIORES,		$LUNAS_LATERALES,
				$B_LUNAS_LATERALES,				$LUNAS_CORTAVIENTO,				$B_LUNAS_CORTAVIENTO,
				$PARABRISAS_DELANTERO,			$B_PARABRISAS_DELANTERO,		$PARABRISAS_POSTERIOR,
				$B_PARABRISAS_POSTERIOR,		$TANQUE_COMBUSTIBLE,			$B_TANQUE_COMBUSTIBLE,
				$PUERTAS,						$B_PUERTAS,						$ASIENTOS,
				$B_ASIENTOS,					$AIRE_ACONDICIONADO,			$B_AIRE_ACONDICIONADO,
				$ALARMA,						$B_ALARMA,						$PLUMILLAS,
				$B_PLUMILLAS,					$ESPEJOS,						$B_ESPEJOS,
				$CINTURONES_SEGURIDAD,			$B_CINTURONES_SEGURIDAD,		$ANTENA,
				$B_ANTENA,						$OTRAS_CARACTERISTICAS_RELEVANTES,	$VALOR_TASACION,
				$ID_ESTADO,						$SIMI_USUARIO_CREACION,			$SIMI_FECHA_CREACION,
				$SIMI_USUARIO_MODIFICA,			$SIMI_FECHA_MODIFICA,			$SIMI_USUARIO_ELIMINA,
				$SIMI_FECHA_ELIMINA,			$FECHA_REGISTRO
		
		){
		
			return $this->ObjModel->F_Insertar_TBL_MUEBLES_UE_BIE_FT_VEHICULAR
			(
				$COD_UE_FT_VEHICULAR_hide,		$COD_USUARIO_hide, 
				$COD_ENTIDAD,					$COD_UE_BIEN_PATRI,				$DENOMINACION,
				$PLACA,							$CARROCERIA,					$MARCA,
				$MODELO,						$CATEGORIA,						$NRO_CHASIS,
				$NRO_EJES,						$NRO_MOTOR,						$NRO_SERIE,
				$ANIO_FABRICACION,				$COLOR,							$COMBUSTIBLE,
				$TRANSMISION,					$CILINDRADA,					$KILOMETRAJE,
				$TARJETA_PROPIEDAD,				$CILINDROS,						$B_CILINDROS,
				$CARBURADOR,					$B_CARBURADOR,					$DISTRIBUIDOR,
				$B_DISTRIBUIDOR,				$BOMBA_GASOLINA,				$B_BOMBA_GASOLINA,
				$PURIFICADOR_AIRE,				$B_PURIFICADOR_AIRE,			$BOMBA_FRENOS,
				$B_BOMBA_FRENOS,				$ZAPATAS_TAMBORES,				$B_ZAPATAS_TAMBORES,
				$DISCOS_PASTILLAS,				$B_DISCOS_PASTILLAS,			$RADIOADOR,
				$B_RADIOADOR,					$VENTILADOR,					$B_VENTILADOR,
				$BOMBA_AGUA,					$B_BOMBA_AGUA,					$MOTOR_ARRANQUE,
				$B_MOTOR_ARRANQUE,				$BATERIA,						$B_BATERIA,
				$ALTERNADOR,					$B_ALTERNADOR,					$BOBINA,
				$B_BOBINA,						$RELAY_ALTERNADOR,				$B_RELAY_ALTERNADOR,
				$FAROS_DELANTEROS,				$B_FAROS_DELANTEROS,			$DIRECCIONALES_DELANTERAS,
				$B_DIRECCIONALES_DELANTERAS,	$LUCES_POSTERIORES,				$B_LUCES_POSTERIORES,
				$DIRECCIONALES_POSTERIORES,		$B_DIRECCIONALES_POSTERIORES,	$AUTO_RADIO,
				$B_AUTO_RADIO,					$PARLANTES,						$B_PARLANTES,
				$CLAXON,						$B_CLAXON,						$CIRCUITO_LUCES,
				$B_CIRCUITO_LUCES,				$CAJA_CAMBIOS,					$B_CAJA_CAMBIOS,
				$BOMBA_EMBRAGUE,				$B_BOMBA_EMBRAGUE,				$CAJA_TRANSFERENCIA,
				$B_CAJA_TRANSFERENCIA,			$DIFERENCIAL_TRASERO,			$B_DIFERENCIAL_TRASERO,
				$DIFERENCIAL_DELANTERO,			$B_DIFERENCIAL_DELANTERO,		$VOLANTE,
				$B_VOLANTE,						$CANA_DIRECCION,				$B_CANA_DIRECCION,
				$CREMALLERA,					$B_CREMALLERA,					$ROTULAS,
				$B_ROTULAS,						$AMORTIGUADORES,				$B_AMORTIGUADORES,
				$BARRA_TORSION,					$B_BARRA_TORSION,				$BARRA_ESTABILIZADOR,
				$B_BARRA_ESTABILIZADOR,			$LLANTAS,						$B_LLANTAS,
				$CAPOT_MOTOR,					$B_CAPOT_MOTOR,					$CAPOT_MALETERA,
				$B_CAPOT_MALETERA,				$PARACHOQUES_DELANTEROS,		$B_PARACHOQUES_DELANTEROS,
				$PARACHOQUES_POSTERIORES,		$B_PARACHOQUES_POSTERIORES,		$LUNAS_LATERALES,
				$B_LUNAS_LATERALES,				$LUNAS_CORTAVIENTO,				$B_LUNAS_CORTAVIENTO,
				$PARABRISAS_DELANTERO,			$B_PARABRISAS_DELANTERO,		$PARABRISAS_POSTERIOR,
				$B_PARABRISAS_POSTERIOR,		$TANQUE_COMBUSTIBLE,			$B_TANQUE_COMBUSTIBLE,
				$PUERTAS,						$B_PUERTAS,						$ASIENTOS,
				$B_ASIENTOS,					$AIRE_ACONDICIONADO,			$B_AIRE_ACONDICIONADO,
				$ALARMA,						$B_ALARMA,						$PLUMILLAS,
				$B_PLUMILLAS,					$ESPEJOS,						$B_ESPEJOS,
				$CINTURONES_SEGURIDAD,			$B_CINTURONES_SEGURIDAD,		$ANTENA,
				$B_ANTENA,						$OTRAS_CARACTERISTICAS_RELEVANTES,	$VALOR_TASACION,
				$ID_ESTADO,						$SIMI_USUARIO_CREACION,			$SIMI_FECHA_CREACION,
				$SIMI_USUARIO_MODIFICA,			$SIMI_FECHA_MODIFICA,			$SIMI_USUARIO_ELIMINA,
				$SIMI_FECHA_ELIMINA,			$FECHA_REGISTRO
			
			);
		
		}
		
		
		/* Permite Ejecutar de acuerdo a operacion realizada */
		
		function F_Operacion($COD_ENTIDAD, $COD_UE_BIEN_PATRI)
		{
			
				return $this->ObjModel->F_Lista_BienVehicularXCodigo($COD_ENTIDAD, $COD_UE_BIEN_PATRI);
			
		}
		
		
		
//6- Metodo  Actualizar TBL_MUEBLES_UE_BIEN_PATRIMONIAL */
		function F_Actualizar_TBL_MUEBLES_UE_BIEN_PATRIMONIAL
		(
		
			$COD_ENTIDAD,			$COD_UE_BIEN_PATRI,		$CODIGO_PATRIMONIAL,	$DENOMINACION_BIEN,
			$NRO_BIEN,				$NRO_BIEN_CORRELATIVO,	$COD_CORRELATIVO,		$COD_UE_BIEN_ALTA,
			$USO_CUENTA,			$TIP_CUENTA,			$COD_CTA_CONTABLE,		$VALOR_ADQUIS,
			$VALOR_NETO,			$COD_ESTADO_BIEN,		$OPC_ASEGURADO,			$COND_BIEN,
			$OBSERVACION,			$COD_UE_BIEN_DEPREC,	$ALMACEN,				$ENVIADO_SBN,
			$FECHA_ENVIADO_SBN,		$FECHA_DEVUELTO,		$ID_PREDIO_SBN,			$COD_UE_AREA,
			$COD_UE_OFIC,			$COD_UE_PERS,			$COD_UE_BIEN_ASIG,		$COD_UE_BIEN_BAJA,
			$COD_UE_BIEN_DISPOSIC,	$COD_UE_BIEN_ADMINIS,	$COD_UE_BIEN_DEVOL,		$FECHA_REGISTRO,
			$SIMI_USUARIO_CREACION,	$SIMI_FECHA_CREACION,	$SIMI_USUARIO_MODIFICA,	$SIMI_FECHA_MODIFICA,
			$SIMI_USUARIO_ELIMINA,	$SIMI_FECHA_ELIMINA,	$ID_ESTADO,				$COD_IMPORT_EXCEL_FILE,
			$DOC_ALTA_SBN,			$DOC_BAJA_SBN,			$ID_PREDIO_EXCEL,		$MARCA,
			$MODELO,				$TIPO,					$COLOR,					$SERIE,
			$DIMENSION,				$PLACA,					$NRO_MOTOR,				$NRO_CHASIS,
			$MATRICULA,				$ANIO_FABRICACION,		$LONGITUD,				$ALTURA,
			$ANCHO,					$RAZA,					$ESPECIE,				$EDAD,
			$PAIS,					$OTRAS_CARACT,			$PORC_DEPREC
		
		){
		
			return $this->ObjModel->F_Actualizar_TBL_MUEBLES_UE_BIEN_PATRIMONIAL
			
			(
			
			$COD_ENTIDAD,			$COD_UE_BIEN_PATRI,		$CODIGO_PATRIMONIAL,	$DENOMINACION_BIEN,
			$NRO_BIEN,				$NRO_BIEN_CORRELATIVO,	$COD_CORRELATIVO,		$COD_UE_BIEN_ALTA,
			$USO_CUENTA,			$TIP_CUENTA,			$COD_CTA_CONTABLE,		$VALOR_ADQUIS,
			$VALOR_NETO,			$COD_ESTADO_BIEN,		$OPC_ASEGURADO,			$COND_BIEN,
			$OBSERVACION,			$COD_UE_BIEN_DEPREC,	$ALMACEN,				$ENVIADO_SBN,
			$FECHA_ENVIADO_SBN,		$FECHA_DEVUELTO,		$ID_PREDIO_SBN,			$COD_UE_AREA,
			$COD_UE_OFIC,			$COD_UE_PERS,			$COD_UE_BIEN_ASIG,		$COD_UE_BIEN_BAJA,
			$COD_UE_BIEN_DISPOSIC,	$COD_UE_BIEN_ADMINIS,	$COD_UE_BIEN_DEVOL,		$FECHA_REGISTRO,
			$SIMI_USUARIO_CREACION,	$SIMI_FECHA_CREACION,	$SIMI_USUARIO_MODIFICA,	$SIMI_FECHA_MODIFICA,
			$SIMI_USUARIO_ELIMINA,	$SIMI_FECHA_ELIMINA,	$ID_ESTADO,				$COD_IMPORT_EXCEL_FILE,
			$DOC_ALTA_SBN,			$DOC_BAJA_SBN,			$ID_PREDIO_EXCEL,		$MARCA,
			$MODELO,				$TIPO,					$COLOR,					$SERIE,
			$DIMENSION,				$PLACA,					$NRO_MOTOR,				$NRO_CHASIS,
			$MATRICULA,				$ANIO_FABRICACION,		$LONGITUD,				$ALTURA,
			$ANCHO,					$RAZA,					$ESPECIE,				$EDAD,
			$PAIS,					$OTRAS_CARACT,			$PORC_DEPREC
			
			);
		
		}

		/* Permite Listar una Ficha Vehicular Existente*/
		function F_Lista_Buscar_FichaTecnicaVehicular($COD_ENTIDAD, $COD_UE_BIEN_PATRI, $COD_UE_FT_VEHICULAR)
		{
			
				return $this->ObjModel->F_Lista_Buscar_FichaTecnicaVehicular($COD_ENTIDAD, $COD_UE_BIEN_PATRI, $COD_UE_FT_VEHICULAR);
			
		}
		
		

		/* Permite Eliminar Ficha Vehicular */
		function F_Eliminar_TBL_MUEBLES_UE_BIE_FT_VEHICULAR($COD_ENTIDAD, $COD_UE_BIEN_PATRI, $COD_UE_FT_VEHICULAR)
		{
			
				return $this->ObjModel->F_Eliminar_TBL_MUEBLES_UE_BIE_FT_VEHICULAR($COD_ENTIDAD, $COD_UE_BIEN_PATRI, $COD_UE_FT_VEHICULAR);
			
		}
		
		
		/* FUNCIONALIDAD	: Lista todos los datos de una ficha Tecnica Vehicular */
		function F_Listar_Ficha_Tecnica_Vehicular($COD_UE_FT_VEHICULAR)
		{
		
			return $this->ObjModel->F_Listar_Ficha_Tecnica_Vehicular($COD_UE_FT_VEHICULAR); 
			
		}
		
		
		/* FUNCIONALIDAD	: Obtiene el Nombre de la Entidad */ 
		function F_Obtener_Nombre_Entidad($COD_ENTIDAD)
		{
		
			return $this->ObjModel->F_Obtener_Nombre_Entidad($COD_ENTIDAD); 
			
		}
		
		
		
		/* FUNCIONALIDAD	: Lista las Fichas Vehiculares  */ 
		function F_Listado_Fichas_Tecnica_Vehiculares( $ACCION, $INI, $FIN, $COD_ENTIDAD,  $COD_GRUPO, $COD_CLASE, 
			$CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $ANIO_ADQUISICION,$MES_ADQUISICION)
		{
			return $this->ObjModel->F_Listado_Fichas_Tecnica_Vehiculares($ACCION, $INI, $FIN, $COD_ENTIDAD,  $COD_GRUPO, $COD_CLASE, 
			$CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $ANIO_ADQUISICION,$MES_ADQUISICION);
		}
		
		
		/* FUNCIONALIDAD	: Total de Fichas Vehiculares  */ 
		function F_Total_Fichas_Tecnica_Vehiculares( $ACCION, $COD_ENTIDAD,  $COD_GRUPO, $COD_CLASE, 
			$CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $ANIO_ADQUISICION,$MES_ADQUISICION)
		{
			return $this->ObjModel->F_Total_Fichas_Tecnica_Vehiculares($ACCION, $COD_ENTIDAD,  $COD_GRUPO, $COD_CLASE, 
			$CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $ANIO_ADQUISICION,$MES_ADQUISICION);
		}
		
		
		/* FUNCIONALIDAD	: Lista los años de acuerdo a la fechas de adquisicion de los bienes patrimoniales  */ 
		function F_Listado_Anios_Fecha_Adquisicion($COD_ENTIDAD)
		{
			return $this->ObjModel->F_Listado_Anios_Fecha_Adquisicion($COD_ENTIDAD);
		}


		/* FUNCIONALIDAD	: Lista las Fichas vehiculares para mostrarlos en la grafica  */
		function F_Listado_Fichas_Tecnica_Vehiculares_en_Grafica( $ACCION, $COD_ENTIDAD,  $ANIO_ADQUISICION)
		{
			return $this->ObjModel->F_Listado_Fichas_Tecnica_Vehiculares_en_Grafica($ACCION, $COD_ENTIDAD,  $ANIO_ADQUISICION);
		}
		
		/* FUNCIONALIDAD	: Lista las Fichas Vehiculares en Formato PDF  */ 
		function F_Listado_Fichas_Vehicular_Formato_PDF( $ACCION, $COD_ENTIDAD,  $COD_GRUPO, $COD_CLASE, 
			$CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $ANIO_ADQUISICION, $MES_ADQUISICION )
		{
			 
			return $this->ObjModel->F_Listado_Fichas_Vehicular_Formato_PDF($ACCION, $COD_ENTIDAD,  $COD_GRUPO, $COD_CLASE, 
			$CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $ANIO_ADQUISICION, $MES_ADQUISICION);
			
		}


		/* FUNCIONALIDAD	: Listado de Fichas Vehiculares por Entidad y Denominacion  */  
		function F_Listado_Fichas_Tecnica_Vehiculares_X_Entidad( $ACCION, $COD_ENTIDAD,
			$CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $INI, $FIN )
		{
				return $this->ObjModel->F_Listado_Fichas_Tecnica_Vehiculares_X_Entidad($ACCION, $COD_ENTIDAD,
			$CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $INI, $FIN) ;
		}
		
		
	}//fin de clase

if(isset($_POST["data"])){
	
	$datareg =array();
	require_once('C_Interconexion_SQL.php');
	require_once('../Model/M_simi_formato_ficha_tecnica_vehicular_Model.php');
	require_once('../../utils/funciones/funciones.php');
	
	$data = explode('&',$_POST["data"]);
	
	foreach($data as $key=>$val){
		$aux = explode('=',$val);
		$datareg[$aux[0]] = str_replace('+',' ',$aux[1]);
		
	}	
	/* Capturar Valores */
	$COD_ENTIDAD =   $_POST["P_cod_entidad"]; //utf8_encode($datareg['txt_entidad']);
	$COD_UE_FT_VEHICULAR_hide = utf8_encode($datareg['txt_COD_UE_FT_VEHICULAR_hide']);
	$COD_USUARIO_hide = utf8_encode($_POST["P_cod_usuario"]);
	$CODIGO_PATRIMONIAL = utf8_encode($datareg['txtCODIGO_PATRIMONIAL']);
	$COD_UE_BIEN_PATRI = utf8_encode($datareg['txt_COD_UE_BIEN_PATRI']);
	$DENOMINACION = utf8_encode($datareg['txt_denominacion']);
	$PLACA = utf8_encode($datareg['txt_placa']);
	$CARROCERIA = utf8_encode($datareg['txt_carroceria']);
	$MARCA = utf8_encode($datareg['txt_marca']);
	$MODELO = utf8_encode($datareg['txt_Modelo']);
	
	$CATEGORIA =  utf8_encode($datareg['txt_categoria']);
	$NRO_CHASIS = utf8_encode($datareg['txt_nchasis']);
	$NRO_EJES = utf8_encode($datareg['txt_Nejes']);
	$NRO_MOTOR = utf8_encode($datareg['txt_Nro_motor']);
	$NRO_SERIE = utf8_encode($datareg['txt_N_serie']);
	$ANIO_FABRICACION = utf8_encode($datareg['txt_anio_fabricacion']);
	
	$COLOR =  utf8_encode($datareg['txt_color']);
	$COMBUSTIBLE = utf8_encode($datareg['txt_combustible']);
	$TRANSMISION = utf8_encode($datareg['txt_transmision']);
	$CILINDRADA = utf8_encode($datareg['txt_cilindrada']);
	$KILOMETRAJE = utf8_encode($datareg['txt_Kilometraje']);
	$TARJETA_PROPIEDAD = utf8_encode($datareg['txt_tarjeta_propiedad']);

	$B_CILINDROS = ((isset($datareg['chk_cilindros']))?"1":"0");
	$CILINDROS = utf8_encode($datareg['txt_cilindros']);
	
	$B_CARBURADOR = ((isset($datareg['chk_carburador']))?"1":"0");
	$CARBURADOR = utf8_encode($datareg['txt_carburador']);
	
	$B_DISTRIBUIDOR = ((isset($datareg['chk_distribuidorbomba']))?"1":"0");
	$DISTRIBUIDOR = utf8_encode($datareg['txt_distribuidorbomba']);
	
	$B_BOMBA_GASOLINA = ((isset($datareg['chk_bombagasolina']))?"1":"0");
	$BOMBA_GASOLINA = utf8_encode($datareg['txt_bombagasolina']);
	
	$B_PURIFICADOR_AIRE = ((isset($datareg['chk_purificador_aire']))?"1":"0");
	$PURIFICADOR_AIRE = utf8_encode($datareg['txt_purificador_aire']);
	
	$B_BOMBA_FRENOS = ((isset($datareg['chk_bomba_frenos']))?"1":"0");
	$BOMBA_FRENOS = utf8_encode($datareg['txt_bomba_frenos']);
	
	$B_ZAPATAS_TAMBORES = ((isset($datareg['chk_zapatas_tambores']))?"1":"0");
	$ZAPATAS_TAMBORES = utf8_encode($datareg['txt_zapatas_tambores']);
	
	$B_DISCOS_PASTILLAS = ((isset($datareg['chk_discos_pastillas']))?"1":"0");
	$DISCOS_PASTILLAS = utf8_encode($datareg['txt_discos_pastillas']);
	
	$B_RADIOADOR = ((isset($datareg['chk_radiadores']))?"1":"0");
	$RADIOADOR = utf8_encode($datareg['txt_radiadores']);
	
	$B_VENTILADOR = ((isset($datareg['chk_ventilador']))?"1":"0");
	$VENTILADOR = utf8_encode($datareg['txt_ventilador']);
	
	$B_BOMBA_AGUA = ((isset($datareg['chk_bomba_agua']))?"1":"0");
	$BOMBA_AGUA = utf8_encode($datareg['txt_bomba_agua']);
	
	$B_MOTOR_ARRANQUE = ((isset($datareg['chk_motor_arranque']))?"1":"0"); 
	$MOTOR_ARRANQUE = utf8_encode($datareg['txt_motor_arranque']);
	
	$B_BATERIA = ((isset($datareg['chk_bateria']))?"1":"0");
	$BATERIA = utf8_encode($datareg['txt_bateria']);
	
	$B_ALTERNADOR = ((isset($datareg['chk_alternador']))?"1":"0");
	$ALTERNADOR = utf8_encode($datareg['txt_alternador']);
	
	$B_BOBINA = ((isset($datareg['chk_bobina']))?"1":"0");
	$BOBINA = utf8_encode($datareg['txt_bobina']);
	
	$B_RELAY_ALTERNADOR = ((isset($datareg['chk_relay_alternador']))?"1":"0");
	$RELAY_ALTERNADOR = utf8_encode($datareg['txt_relay_alternador']);
	
	$B_FAROS_DELANTEROS = ((isset($datareg['chk_faros_delanteros']))?"1":"0");
	$FAROS_DELANTEROS = utf8_encode($datareg['txt_faros_delanteros']);
	
	$B_DIRECCIONALES_DELANTERAS = ((isset($datareg['chk_direccionales_delanteras']))?"1":"0");
	$DIRECCIONALES_DELANTERAS = utf8_encode($datareg['txt_direccionales_delanteras']);
	
	$B_LUCES_POSTERIORES = ((isset($datareg['chk_luces_posteriores']))?"1":"0");
	$LUCES_POSTERIORES = utf8_encode($datareg['txt_luces_posteriores']);
	
	$B_DIRECCIONALES_POSTERIORES = ((isset($datareg['chk_direccionales_posteriores']))?"1":"0");
	$DIRECCIONALES_POSTERIORES = utf8_encode($datareg['txt_direccionales_posteriores']);
	
	$B_AUTO_RADIO = ((isset($datareg['chk_autoradio']))?"1":"0");
	$AUTO_RADIO = utf8_encode($datareg['txt_autoradio']);
	
	$B_PARLANTES = ((isset($datareg['chk_parlantes']))?"1":"0");
	$PARLANTES = utf8_encode($datareg['txt_parlantes']);
	
	$B_CLAXON = ((isset($datareg['chk_claxon']))?"1":"0");
	$CLAXON = utf8_encode($datareg['txt_claxon']);
	
	$B_CIRCUITO_LUCES = ((isset($datareg['chk_circuito_luces']))?"1":"0");
	$CIRCUITO_LUCES = utf8_encode($datareg['txt_circuito_luces']);
	
	$B_CAJA_CAMBIOS = ((isset($datareg['chk_caja_cambios']))?"1":"0");
	$CAJA_CAMBIOS = utf8_encode($datareg['txt_caja_cambios']);
	
	$B_BOMBA_EMBRAGUE = ((isset($datareg['chk_bomba_embrague']))?"1":"0");
	$BOMBA_EMBRAGUE = utf8_encode($datareg['txt_bomba_embrague']);
	
	$B_CAJA_TRANSFERENCIA = ((isset($datareg['chk_caja_transferencia']))?"1":"0");
	$CAJA_TRANSFERENCIA = utf8_encode($datareg['txt_caja_transferencia']);
	
	$B_DIFERENCIAL_TRASERO = ((isset($datareg['chk_diferencial_trasero']))?"1":"0");
	$DIFERENCIAL_TRASERO = utf8_encode($datareg['txt_diferencial_trasero']);
	
	$B_DIFERENCIAL_DELANTERO = ((isset($datareg['chk_diferencial_delantero']))?"1":"0");
	$DIFERENCIAL_DELANTERO = utf8_encode($datareg['txt_diferencial_delantero']);
	
	$B_VOLANTE = ((isset($datareg['chk_volante']))?"1":"0");
	$VOLANTE = utf8_encode($datareg['txt_volante']);
	
	$B_CANA_DIRECCION = ((isset($datareg['chk_cana_direccion']))?"1":"0");
	$CANA_DIRECCION = utf8_encode($datareg['txt_cana_direccion']);
	
	$B_CREMALLERA = ((isset($datareg['chk_cremallera']))?"1":"0");
	$CREMALLERA = utf8_encode($datareg['txt_cremallera']);
	
	$B_ROTULAS = ((isset($datareg['chk_rotulas']))?"1":"0");
	$ROTULAS = utf8_encode($datareg['txt_rotulas']);
	
	$B_AMORTIGUADORES = ((isset($datareg['chk_amortiguadores']))?"1":"0");
	$AMORTIGUADORES = utf8_encode($datareg['txt_amortiguadores']);
	
	$B_BARRA_TORSION = ((isset($datareg['chk_barra_torsion']))?"1":"0");
	$BARRA_TORSION = utf8_encode($datareg['txt_barra_torsion']);
	
	$B_BARRA_ESTABILIZADOR = ((isset($datareg['chk_barra_estabilizadora']))?"1":"0");
	$BARRA_ESTABILIZADOR = utf8_encode($datareg['txt_barra_estabilizadora']);
	
	$B_LLANTAS = ((isset($datareg['chk_llantas']))?"1":"0");
	$LLANTAS = utf8_encode($datareg['txt_llantas']);
	
	$B_CAPOT_MOTOR = ((isset($datareg['chk_capot_motor']))?"1":"0");
	$CAPOT_MOTOR = utf8_encode($datareg['txt_capot_motor']);
	
	$B_CAPOT_MALETERA = ((isset($datareg['chk_capot_maletera']))?"1":"0");
	$CAPOT_MALETERA = utf8_encode($datareg['txt_capot_maletera']);
	
	$B_PARACHOQUES_DELANTEROS = ((isset($datareg['chk_Parachoques_delanteros']))?"1":"0");
	$PARACHOQUES_DELANTEROS = utf8_encode($datareg['txt_Parachoques_delanteros']);
	
	$B_PARACHOQUES_POSTERIORES = ((isset($datareg['chk_Parachoques_posteriores']))?"1":"0");
	$PARACHOQUES_POSTERIORES = utf8_encode($datareg['txt_Parachoques_posteriores']);
	
	$B_LUNAS_LATERALES = ((isset($datareg['chk_Lunas_laterales']))?"1":"0");
	$LUNAS_LATERALES = utf8_encode($datareg['txt_Lunas_laterales']);
	
	$B_LUNAS_CORTAVIENTO = ((isset($datareg['chk_Lunas_cortavientos']))?"1":"0");
	$LUNAS_CORTAVIENTO = utf8_encode($datareg['txt_Lunas_cortavientos']);
	
	$B_PARABRISAS_DELANTERO = ((isset($datareg['chk_Parabrisas_delanteros']))?"1":"0");
	$PARABRISAS_DELANTERO = utf8_encode($datareg['txt_Parabrisas_delanteros']);
	
	$B_PARABRISAS_POSTERIOR = ((isset($datareg['chk_Parabrisas_posteriores']))?"1":"0");
	$PARABRISAS_POSTERIOR = utf8_encode($datareg['txt_Parabrisas_posteriores']);
	
	$B_TANQUE_COMBUSTIBLE = ((isset($datareg['chk_Tanque_combustible']))?"1":"0");
	$TANQUE_COMBUSTIBLE = utf8_encode($datareg['txt_Tanque_combustible']);
	
	$B_PUERTAS = ((isset($datareg['chk_Puertas']))?"1":"0");
	$PUERTAS = utf8_encode($datareg['txt_Puertas']);
	
	$B_ASIENTOS = ((isset($datareg['chk_Asientos']))?"1":"0");
	$ASIENTOS = utf8_encode($datareg['txt_Asientos']);
	
	$B_AIRE_ACONDICIONADO = ((isset($datareg['chk_aire_acondicionado']))?"1":"0");
	$AIRE_ACONDICIONADO = utf8_encode($datareg['txt_aire_acondicionado']);
	
	$B_ALARMA = ((isset($datareg['chk_Alarma']))?"1":"0");
	$ALARMA = utf8_encode($datareg['txt_Alarma']);
	
	$B_PLUMILLAS = ((isset($datareg['chk_plumillas']))?"1":"0");
	$PLUMILLAS = utf8_encode($datareg['txt_plumillas']);
	
	$B_ESPEJOS = ((isset($datareg['chk_espejos']))?"1":"0");
	$ESPEJOS = utf8_encode($datareg['txt_espejos']);
	
	$B_CINTURONES_SEGURIDAD = ((isset($datareg['chk_cinturones']))?"1":"0");
	$CINTURONES_SEGURIDAD = utf8_encode($datareg['txt_cinturones']);
	
	$B_ANTENA = ((isset($datareg['chk_antena']))?"1":"0");
	$ANTENA = utf8_encode($datareg['txt_antena']);
	
	$OTRAS_CARACTERISTICAS_RELEVANTES = utf8_encode($datareg['txt_otras_caracteristicas_relev']);
	$VALOR_TASACION = utf8_encode($datareg['txt_valor_tasacion']);
	
	$ID_ESTADO = "1";
	
	
	$ObjController = new C_simi_formato_ficha_tecnica_vehicular_Controller();
	$rpta = $ObjController->F_Insertar_TBL_MUEBLES_UE_BIE_FT_VEHICULAR(
				$COD_UE_FT_VEHICULAR_hide, 		$COD_USUARIO_hide, 
				$COD_ENTIDAD,					$COD_UE_BIEN_PATRI,				$DENOMINACION,
				$PLACA,							$CARROCERIA,					$MARCA,
				$MODELO,						$CATEGORIA,						$NRO_CHASIS,
				$NRO_EJES,						$NRO_MOTOR,						$NRO_SERIE,
				$ANIO_FABRICACION,				$COLOR,							$COMBUSTIBLE,
				$TRANSMISION,					$CILINDRADA,					$KILOMETRAJE,
				$TARJETA_PROPIEDAD,				$CILINDROS,						$B_CILINDROS,
				$CARBURADOR,					$B_CARBURADOR,					$DISTRIBUIDOR,
				$B_DISTRIBUIDOR,				$BOMBA_GASOLINA,				$B_BOMBA_GASOLINA,
				$PURIFICADOR_AIRE,				$B_PURIFICADOR_AIRE,			$BOMBA_FRENOS,
				$B_BOMBA_FRENOS,				$ZAPATAS_TAMBORES,				$B_ZAPATAS_TAMBORES,
				$DISCOS_PASTILLAS,				$B_DISCOS_PASTILLAS,			$RADIOADOR,
				$B_RADIOADOR,					$VENTILADOR,					$B_VENTILADOR,
				$BOMBA_AGUA,					$B_BOMBA_AGUA,					$MOTOR_ARRANQUE,
				$B_MOTOR_ARRANQUE,				$BATERIA,						$B_BATERIA,
				$ALTERNADOR,					$B_ALTERNADOR,					$BOBINA,
				$B_BOBINA,						$RELAY_ALTERNADOR,				$B_RELAY_ALTERNADOR,
				$FAROS_DELANTEROS,				$B_FAROS_DELANTEROS,			$DIRECCIONALES_DELANTERAS,
				$B_DIRECCIONALES_DELANTERAS,	$LUCES_POSTERIORES,				$B_LUCES_POSTERIORES,
				$DIRECCIONALES_POSTERIORES,		$B_DIRECCIONALES_POSTERIORES,	$AUTO_RADIO,
				$B_AUTO_RADIO,					$PARLANTES,						$B_PARLANTES,
				$CLAXON,						$B_CLAXON,						$CIRCUITO_LUCES,
				$B_CIRCUITO_LUCES,				$CAJA_CAMBIOS,					$B_CAJA_CAMBIOS,
				$BOMBA_EMBRAGUE,				$B_BOMBA_EMBRAGUE,				$CAJA_TRANSFERENCIA,
				$B_CAJA_TRANSFERENCIA,			$DIFERENCIAL_TRASERO,			$B_DIFERENCIAL_TRASERO,
				$DIFERENCIAL_DELANTERO,			$B_DIFERENCIAL_DELANTERO,		$VOLANTE,
				$B_VOLANTE,						$CANA_DIRECCION,				$B_CANA_DIRECCION,
				$CREMALLERA,					$B_CREMALLERA,					$ROTULAS,
				$B_ROTULAS,						$AMORTIGUADORES,				$B_AMORTIGUADORES,
				$BARRA_TORSION,					$B_BARRA_TORSION,				$BARRA_ESTABILIZADOR,
				$B_BARRA_ESTABILIZADOR,			$LLANTAS,						$B_LLANTAS,
				$CAPOT_MOTOR,					$B_CAPOT_MOTOR,					$CAPOT_MALETERA,
				$B_CAPOT_MALETERA,				$PARACHOQUES_DELANTEROS,		$B_PARACHOQUES_DELANTEROS,
				$PARACHOQUES_POSTERIORES,		$B_PARACHOQUES_POSTERIORES,		$LUNAS_LATERALES,
				$B_LUNAS_LATERALES,				$LUNAS_CORTAVIENTO,				$B_LUNAS_CORTAVIENTO,
				$PARABRISAS_DELANTERO,			$B_PARABRISAS_DELANTERO,		$PARABRISAS_POSTERIOR,
				$B_PARABRISAS_POSTERIOR,		$TANQUE_COMBUSTIBLE,			$B_TANQUE_COMBUSTIBLE,
				$PUERTAS,						$B_PUERTAS,						$ASIENTOS,
				$B_ASIENTOS,					$AIRE_ACONDICIONADO,			$B_AIRE_ACONDICIONADO,
				$ALARMA,						$B_ALARMA,						$PLUMILLAS,
				$B_PLUMILLAS,					$ESPEJOS,						$B_ESPEJOS,
				$CINTURONES_SEGURIDAD,			$B_CINTURONES_SEGURIDAD,		$ANTENA,
				$B_ANTENA,						$OTRAS_CARACTERISTICAS_RELEVANTES,	$VALOR_TASACION,
				$ID_ESTADO,						$SIMI_USUARIO_CREACION,			$SIMI_FECHA_CREACION,
				$SIMI_USUARIO_MODIFICA,			$SIMI_FECHA_MODIFICA,			$SIMI_USUARIO_ELIMINA,
				$SIMI_FECHA_ELIMINA,			$FECHA_REGISTRO);
	
	/* Se imprime para posteriormente Leerlo como Array */
	echo ($rpta)? '1' : '0';
		
		
	
}


/* CONSULTAR BIEN PATRIMONIAL Y/O CUANDO SE VA CREAR NUEVA FICHA TECNICA */
if(isset($_GET['operacion'])){
	if($_GET['operacion']=='consultaXcodBienPatrim'){
		
		require_once ('C_Interconexion_SQL.php');
		require_once ('../Model/M_simi_formato_ficha_tecnica_vehicular_Model.php');
		
		$ctrl = new C_simi_formato_ficha_tecnica_vehicular_Controller();
		$P_COD_ENTIDAD = $_POST['P_COD_ENTIDAD']; 
		$p_cod_bien_patrimonial = $_POST['p_cod_bien_patrimonial'];
		$rpta = $ctrl->F_Operacion($P_COD_ENTIDAD, $p_cod_bien_patrimonial);

		
		echo odbc_result( $rpta,"COD_UE_BIEN_PATRI") .  "|" . odbc_result( $rpta,"CODIGO_PATRIMONIAL") . "|" . odbc_result( $rpta,"DENOMINACION_BIEN") .  "|"  . odbc_result( $rpta,"PLACA") . "|" . odbc_result( $rpta,"MARCA") . "|" . odbc_result( $rpta,"MODELO") .  "|" . odbc_result( $rpta,"NRO_CHASIS") .  "|" . odbc_result( $rpta,"NRO_MOTOR") .  "|" . odbc_result( $rpta,"NRO_SERIE") .  "|" . odbc_result( $rpta,"ANIO_FABRICACION")  .  "|" . odbc_result( $rpta,"COLOR")  .  "|" . odbc_result( $rpta,"OTRAS_CARACTERISTICAS_RELEVANTES") .  "|" . odbc_result( $rpta,"NOM_ENTIDAD");
		
		
	}
}


/* LISTAR FICHA TECNICA VEHICULAR EXISTENTE */
if(isset($_GET['operacion'])){
	if($_GET['operacion']=='ModificarFichaVehicular'){
		
		require_once ('C_Interconexion_SQL.php');
		require_once ('../Model/M_simi_formato_ficha_tecnica_vehicular_Model.php');
		
		$ctrl = new C_simi_formato_ficha_tecnica_vehicular_Controller();
		
		$P_COD_ENTIDAD = $_POST['P_COD_ENTIDAD']; 
		$p_cod_bien_patrimonial = $_POST['p_cod_bien_patrimonial'];
		$p_cod_ue_FT_Vehicular = $_POST['p_cod_ue_FT_Vehicular'];
		$rpta = $ctrl->F_Lista_Buscar_FichaTecnicaVehicular($P_COD_ENTIDAD, $p_cod_bien_patrimonial, $p_cod_ue_FT_Vehicular);

		echo odbc_result( $rpta,"CODIGO_PATRIMONIAL") .  "|" . odbc_result( $rpta,"DENOMINACION_BIEN") . "|" . 
		odbc_result( $rpta,"COD_UE_FT_VEHICULAR") .  "|" . odbc_result( $rpta,"COD_ENTIDAD") . "|" . 
		odbc_result( $rpta,"COD_UE_BIEN_PATRI") . "|" . odbc_result( $rpta,"DENOMINACION") .  "|" . 
		odbc_result( $rpta,"PLACA") .  "|" . odbc_result( $rpta,"CARROCERIA") .  "|" . 
		odbc_result( $rpta,"MARCA") .  "|" . odbc_result( $rpta,"MODELO")  .  "|" . 
		odbc_result( $rpta,"CATEGORIA")  .  "|" . odbc_result( $rpta,"NRO_CHASIS") .  "|" . 
		odbc_result( $rpta,"NRO_EJES")  .  "|" . odbc_result( $rpta,"NRO_MOTOR").  "|" . 
		odbc_result( $rpta,"NRO_SERIE")  .  "|" . odbc_result( $rpta,"ANIO_FABRICACION") .  "|" . 
		odbc_result( $rpta,"COLOR")  .  "|" . odbc_result( $rpta,"COMBUSTIBLE") .  "|" . 
		odbc_result( $rpta,"TRANSMISION")  .  "|" . odbc_result( $rpta,"CILINDRADA") .  "|" . 
		odbc_result( $rpta,"KILOMETRAJE")  .  "|" . odbc_result( $rpta,"TARJETA_PROPIEDAD") .  "|" . 
		odbc_result( $rpta,"CILINDROS")  .  "|" . odbc_result( $rpta,"B_CILINDROS") .  "|" . 
		odbc_result( $rpta,"CARBURADOR")  .  "|" . odbc_result( $rpta,"B_CARBURADOR") .  "|" . 
		odbc_result( $rpta,"DISTRIBUIDOR")  .  "|" . odbc_result( $rpta,"B_DISTRIBUIDOR") .  "|" . 
		odbc_result( $rpta,"BOMBA_GASOLINA")  .  "|" . odbc_result( $rpta,"B_BOMBA_GASOLINA") .  "|" . 
		odbc_result( $rpta,"PURIFICADOR_AIRE")  .  "|" . odbc_result( $rpta,"B_PURIFICADOR_AIRE") .  "|" . 
		odbc_result( $rpta,"BOMBA_FRENOS")  .  "|" . odbc_result( $rpta,"B_BOMBA_FRENOS") .  "|" . 
		odbc_result( $rpta,"ZAPATAS_TAMBORES")  .  "|" . odbc_result( $rpta,"B_ZAPATAS_TAMBORES") .  "|" . 
		odbc_result( $rpta,"DISCOS_PASTILLAS")  .  "|" . odbc_result( $rpta,"B_DISCOS_PASTILLAS") .  "|" . 
		odbc_result( $rpta,"RADIOADOR")  .  "|" . odbc_result( $rpta,"B_RADIOADOR") .  "|" . 
		odbc_result( $rpta,"VENTILADOR")  .  "|" . odbc_result( $rpta,"B_VENTILADOR") .  "|" . 
		odbc_result( $rpta,"BOMBA_AGUA")  .  "|" . odbc_result( $rpta,"B_BOMBA_AGUA") .  "|" . 
		odbc_result( $rpta,"MOTOR_ARRANQUE")  .  "|" . odbc_result( $rpta,"B_MOTOR_ARRANQUE") .  "|" . 
		odbc_result( $rpta,"BATERIA")  .  "|" . odbc_result( $rpta,"B_BATERIA") .  "|" . 
		odbc_result( $rpta,"ALTERNADOR")  .  "|" . odbc_result( $rpta,"B_ALTERNADOR") .  "|" . 
		odbc_result( $rpta,"BOBINA")  .  "|" . odbc_result( $rpta,"B_BOBINA") .  "|" . 
		odbc_result( $rpta,"RELAY_ALTERNADOR")  .  "|" . odbc_result( $rpta,"B_RELAY_ALTERNADOR") .  "|" . 
		odbc_result( $rpta,"FAROS_DELANTEROS")  .  "|" . odbc_result( $rpta,"B_FAROS_DELANTEROS") .  "|" . 
		odbc_result( $rpta,"DIRECCIONALES_DELANTERAS")  .  "|" . odbc_result( $rpta,"B_DIRECCIONALES_DELANTERAS") .  "|" . 
		odbc_result( $rpta,"LUCES_POSTERIORES")  .  "|" . odbc_result( $rpta,"B_LUCES_POSTERIORES") .  "|" . 
		odbc_result( $rpta,"DIRECCIONALES_POSTERIORES")  .  "|" . odbc_result( $rpta,"B_DIRECCIONALES_POSTERIORES") .  "|" . 
		odbc_result( $rpta,"AUTO_RADIO")  .  "|" . odbc_result( $rpta,"B_AUTO_RADIO") .  "|" . 
		odbc_result( $rpta,"PARLANTES")  .  "|" . odbc_result( $rpta,"B_PARLANTES") .  "|" . 
		odbc_result( $rpta,"CLAXON")  .  "|" . odbc_result( $rpta,"B_CLAXON") .  "|" . 
		odbc_result( $rpta,"CIRCUITO_LUCES")  .  "|" . odbc_result( $rpta,"B_CIRCUITO_LUCES") .  "|" . 
		odbc_result( $rpta,"CAJA_CAMBIOS")  .  "|" . odbc_result( $rpta,"B_CAJA_CAMBIOS") .  "|" . 
		odbc_result( $rpta,"BOMBA_EMBRAGUE")  .  "|" . odbc_result( $rpta,"B_BOMBA_EMBRAGUE") .  "|" . 
		odbc_result( $rpta,"CAJA_TRANSFERENCIA")  .  "|" . odbc_result( $rpta,"B_CAJA_TRANSFERENCIA") .  "|" . 
		odbc_result( $rpta,"DIFERENCIAL_TRASERO")  .  "|" . odbc_result( $rpta,"B_DIFERENCIAL_TRASERO") .  "|" . 
		odbc_result( $rpta,"DIFERENCIAL_DELANTERO")  .  "|" . odbc_result( $rpta,"B_DIFERENCIAL_DELANTERO") .  "|" . 
		odbc_result( $rpta,"VOLANTE")  .  "|" . odbc_result( $rpta,"B_VOLANTE") .  "|" . 
		utf8_decode(odbc_result( $rpta,"CANA_DIRECCION"))  .  "|" . odbc_result( $rpta,"B_CANA_DIRECCION") .  "|" . 
		odbc_result( $rpta,"CREMALLERA")  .  "|" . odbc_result( $rpta,"B_CREMALLERA") .  "|" . 
		odbc_result( $rpta,"ROTULAS")  .  "|" . odbc_result( $rpta,"B_ROTULAS") .  "|" . 
		odbc_result( $rpta,"AMORTIGUADORES")  .  "|" . odbc_result( $rpta,"B_AMORTIGUADORES") .  "|" . 
		odbc_result( $rpta,"BARRA_TORSION")  .  "|" . odbc_result( $rpta,"B_BARRA_TORSION") .  "|" . 
		odbc_result( $rpta,"BARRA_ESTABILIZADOR")  .  "|" . odbc_result( $rpta,"B_BARRA_ESTABILIZADOR") .  "|" . 
		odbc_result( $rpta,"LLANTAS")  .  "|" . odbc_result( $rpta,"B_LLANTAS") .  "|" . 
		odbc_result( $rpta,"CAPOT_MOTOR")  .  "|" . odbc_result( $rpta,"B_CAPOT_MOTOR") .  "|" . 
		odbc_result( $rpta,"CAPOT_MALETERA")  .  "|" . odbc_result( $rpta,"B_CAPOT_MALETERA") .  "|" . 
		odbc_result( $rpta,"PARACHOQUES_DELANTEROS")  .  "|" . odbc_result( $rpta,"B_PARACHOQUES_DELANTEROS") .  "|" . 
		odbc_result( $rpta,"PARACHOQUES_POSTERIORES")  .  "|" . odbc_result( $rpta,"B_PARACHOQUES_POSTERIORES") .  "|" . 
	
		odbc_result( $rpta,"LUNAS_LATERALES")  .  "|" . odbc_result( $rpta,"B_LUNAS_LATERALES") .  "|" . 
		odbc_result( $rpta,"LUNAS_CORTAVIENTO")  .  "|" . odbc_result( $rpta,"B_LUNAS_CORTAVIENTO") .  "|" . 
		odbc_result( $rpta,"PARABRISAS_DELANTERO")  .  "|" . odbc_result( $rpta,"B_PARABRISAS_DELANTERO") .  "|" . 
		odbc_result( $rpta,"PARABRISAS_POSTERIOR")  .  "|" . odbc_result( $rpta,"B_PARABRISAS_POSTERIOR") .  "|" . 
		odbc_result( $rpta,"TANQUE_COMBUSTIBLE")  .  "|" . odbc_result( $rpta,"B_TANQUE_COMBUSTIBLE") .  "|" . 
		odbc_result( $rpta,"PUERTAS")  .  "|" . odbc_result( $rpta,"B_PUERTAS") .  "|" . 
		odbc_result( $rpta,"ASIENTOS")  .  "|" . odbc_result( $rpta,"B_ASIENTOS") .  "|" . 
		odbc_result( $rpta,"AIRE_ACONDICIONADO")  .  "|" . odbc_result( $rpta,"B_AIRE_ACONDICIONADO") .  "|" . 
		odbc_result( $rpta,"ALARMA")  .  "|" . odbc_result( $rpta,"B_ALARMA") .  "|" . 
		odbc_result( $rpta,"PLUMILLAS")  .  "|" . odbc_result( $rpta,"B_PLUMILLAS") .  "|" . 
		odbc_result( $rpta,"ESPEJOS")  .  "|" . odbc_result( $rpta,"B_ESPEJOS") .  "|" . 
		odbc_result( $rpta,"CINTURONES_SEGURIDAD")  .  "|" . odbc_result( $rpta,"B_CINTURONES_SEGURIDAD") .  "|" . 
		odbc_result( $rpta,"ANTENA")  .  "|" . odbc_result( $rpta,"B_ANTENA") .  "|" . 
		odbc_result( $rpta,"OTRAS_CARACTERISTICAS_RELEVANTES")  .  "|" . odbc_result( $rpta,"VALOR_TASACION") .  "|" . 
		odbc_result( $rpta,"ID_ESTADO") .  "|" . odbc_result( $rpta,"NOM_ENTIDAD") ;
		
								
		
		
		
		
		
		
	}
}


/* ELIMINAR FICHA TECNICA VEHICULAR */
if(isset($_GET['operacion'])){
	if($_GET['operacion']=='EliminarFichaVehicular'){
		
		require_once ('C_Interconexion_SQL.php');
		require_once ('../Model/M_simi_formato_ficha_tecnica_vehicular_Model.php');
		
		$ctrl = new C_simi_formato_ficha_tecnica_vehicular_Controller();
		
		$P_COD_ENTIDAD = $_POST['P_COD_ENTIDAD']; 
		$p_cod_bien_patrimonial = $_POST['p_cod_bien_patrimonial'];
		$p_cod_ue_FT_Vehicular = $_POST['p_cod_ue_FT_Vehicular'];
		$rpta = $ctrl->F_Eliminar_TBL_MUEBLES_UE_BIE_FT_VEHICULAR($P_COD_ENTIDAD, $p_cod_bien_patrimonial, $p_cod_ue_FT_Vehicular
		
		);
		
		echo ($rpta)? '1' : '0';
		
	}
}


?>