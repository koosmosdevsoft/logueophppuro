<?
require_once("C_Interconexion_SQL.php");

class Padron_Entidad{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
	
	
	function Generar_Codigo_Padron_Entidad(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(COD_ENTIDAD),0) + 1) as CODIGO FROM TBL_PADRON_ENTIDAD ";
			$resultado = $this->oDBManager->execute($sql);
			
			return $resultado;
		}
	}
	
	
	function Verifica_Existe_Entidad($RUC_ENTIDAD){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT COUNT(*) AS TOTAL FROM TBL_PADRON_ENTIDAD WHERE RUC_ENTIDAD = '$RUC_ENTIDAD' AND ID_ESTADO = 1 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Insertar_Padron_Entidad($COD_ENTIDAD, $RUC_ENTIDAD, $NOM_ENTIDAD, $SUNAT_COD_UBIGEO, $SUNAT_COD_DEPA, $SUNAT_NOM_DEPA, $SUNAT_COD_PROV, $SUNAT_NOM_PROV, $SUNAT_COD_DIST, $SUNAT_NOM_DIST, $SUNAT_COD_TIPVIA, $SUNAT_DES_TIPVIA, $SUNAT_NOM_VIA, $SUNAT_NUMERO, $SUNAT_INTERIOR, $SUNAT_REFERENCIA, $SUNAT_COD_TIPZON, $SUNAT_DES_TIPZON, $SUNAT_NOM_ZONA, $SUNAT_COD_CIIU, $SUNAT_NOM_CIIU, $SUNAT_COD_NUMREG, $SUNAT_NOM_NUMREG, $SUNAT_COD_IDENT, $SUNAT_NOM_IDENT, $SUNAT_COD_TIPEMP, $SUNAT_NOM_TIPEMP){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_PADRON_ENTIDAD (COD_ENTIDAD, RUC_ENTIDAD, NOM_ENTIDAD, SUNAT_COD_UBIGEO, SUNAT_COD_DEPA, SUNAT_NOM_DEPA, SUNAT_COD_PROV, SUNAT_NOM_PROV, SUNAT_COD_DIST, SUNAT_NOM_DIST, SUNAT_COD_TIPVIA, SUNAT_DES_TIPVIA, SUNAT_NOM_VIA, SUNAT_NUMERO, SUNAT_INTERIOR, SUNAT_REFERENCIA, SUNAT_COD_TIPZON, SUNAT_DES_TIPZON, SUNAT_NOM_ZONA, SUNAT_COD_CIIU, SUNAT_NOM_CIIU, SUNAT_COD_NUMREG, SUNAT_NOM_NUMREG, SUNAT_COD_IDENT, SUNAT_NOM_IDENT, SUNAT_COD_TIPEMP, SUNAT_NOM_TIPEMP, FECHA_REGISTRO, ID_ESTADO) VALUES ('$COD_ENTIDAD', '$RUC_ENTIDAD', '$NOM_ENTIDAD', '$SUNAT_COD_UBIGEO', '$SUNAT_COD_DEPA', '$SUNAT_NOM_DEPA', '$SUNAT_COD_PROV', '$SUNAT_NOM_PROV', '$SUNAT_COD_DIST', '$SUNAT_NOM_DIST', '$SUNAT_COD_TIPVIA', '$SUNAT_DES_TIPVIA', '$SUNAT_NOM_VIA', '$SUNAT_NUMERO', '$SUNAT_INTERIOR', '$SUNAT_REFERENCIA', '$SUNAT_COD_TIPZON', '$SUNAT_DES_TIPZON', '$SUNAT_NOM_ZONA', '$SUNAT_COD_CIIU', '$SUNAT_NOM_CIIU', '$SUNAT_COD_NUMREG', '$SUNAT_NOM_NUMREG', '$SUNAT_COD_IDENT', '$SUNAT_NOM_IDENT', '$SUNAT_COD_TIPEMP', '$SUNAT_NOM_TIPEMP', GETDATE(), '1') ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Ver_Datos_Padron_Entidad_x_RUC($RUC_ENTIDAD){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_PADRON_ENTIDAD WHERE RUC_ENTIDAD = '$RUC_ENTIDAD' AND ID_ESTADO = 1 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Ver_Datos_Padron_Entidad_x_COD_ENTIDAD($COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_PADRON_ENTIDAD WHERE COD_ENTIDAD = '$COD_ENTIDAD' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Actualiza_cod_unidad_ejecutora($COD_ENTIDAD, $COD_UNID_EJEC){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_PADRON_ENTIDAD SET COD_UNID_EJEC = '$COD_UNID_EJEC' WHERE COD_ENTIDAD = '$COD_ENTIDAD' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
}
?>