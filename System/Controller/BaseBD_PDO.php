<?php
class DB{
  private $_server 	= "SRV-SINABIP-BD";
  private $_dbname 	= "SINABIP_SBN";
  private $_port 	= "1433";
  private $_user 	= "appsinabip"; 
  private $_pass 	= "4tm0sf3r42020+";  

  private $_conexion;
  private $_parametros = [];


  public function connect(){
    try {
      $dsn = "sqlsrv:Server=$this->_server;Database=$this->_dbname";
      $options = [
        PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
      ];
     $this->_conexion = new PDO($dsn, $this->_user, $this->_pass,$options);

	   if (isset($this->strCharsetDBC) && ($this->strCharsetDBC == 'ISO-8859-1'))
			$this->_conexion->setAttribute( PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_SYSTEM );
		else
			 $this->_conexion->setAttribute( PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_UTF8 );
	  
	  //$this->_conexion = new PDO("mysql:host=".$this->_host.";dbname=".$this->_dbname.";", $this->_user, $this->_pass);
      // echo "conexion satisfactoria -> ".$this->_server." - ".$this->_dbname;
	  //print_r($this->_conexion);
    } catch (PDOException $e) {
      die("Error!: " . $e->getMessage() . "<br/>");
    }
  }


  public function createParameter($p_nombreParametro=0, $p_valor='', $p_tipo='', $p_longitud=0)
  {

	  
    if(!isset($p_nombreParametro))
      die("Error DB: no se a definido el nombre de un parametro");

    if(!isset($p_valor))
      die("Error DB: no se a definido el valor del parametro: ".$p_nombreParametro);

    if(!isset($p_tipo))
      die("Error DB: no se a definido el tipo del parametro: ".$p_nombreParametro);

    $tipoDB = '';
    switch ($p_tipo) {
      case 'int':
        $tipoDB = PDO::PARAM_INT; // para INTEGER
        break;
      case 'string':
        $tipoDB = PDO::PARAM_STR; // para CHAR, VARCHAR
        break;
      case 'bool':
        $tipoDB = PDO::PARAM_BOOL; // para Booleano
        break;
      case 'varbinary':
        $tipoDB = PDO::PARAM_LOB; // para binary
        break;
      case 'date':
        $tipoDB = PDO::PARAM_STR; // para CHAR, VARCHAR
        $p_valor = date_format(date_create_from_format("d/m/Y H:i:s",$p_valor),"Y-m-d H:i:s");
        break;

      default:
        $tipoDB = '';
        break;
    }
    if (!isset($tipoDB))
      die("Error DB: No se ha especificado equivalencia para el tipo de dato ".$p_tipo.", en la base de datos");

      array_push($this->_parametros, ["nombre"=>$p_nombreParametro, "valor"=>$p_valor, "tipo"=>$tipoDB]);
  }

 
  public function ExecuteNonQuery($p_nombreProcedimiento='')
  {
	$command = $this->_conexion->prepare($p_nombreProcedimiento);
    for ($i = 0; $i < count($this->_parametros); $i++) {
        $command->bindParam($this->_parametros[$i]["nombre"], $this->_parametros[$i]["valor"], $this->_parametros[$i]["tipo"]);
    }
    $command->execute();
    $result = $command->rowCount();
    return $result;
  }

  public function ExecuteReader($p_nombreProcedimiento='')
  {
	$command = $this->_conexion->prepare($p_nombreProcedimiento);
    for ($i = 0; $i < count($this->_parametros); $i++) {
        $command->bindParam($this->_parametros[$i]["nombre"], $this->_parametros[$i]["valor"], $this->_parametros[$i]["tipo"]);
    }
    try{
      $command->execute();
    }catch(Exception $e){
      //header('Location: http://www.google.com.pe');
      header('Location: https://wstest.mineco.gob.pe/SGISBN/System/sinabip_modulos.php?idm=2');
      die();
      //echo '505'; die();
     
      //echo "<pre>";print_r($e); die();

    }
    $result = [];
    $i = 0;
    while($row = $command->fetch(PDO::FETCH_ASSOC)){
      $result[$i] = $row;
      $i++;
    }
    return $result;
  }

  public function ExecuteScalar($p_nombreProcedimiento)
  {
	$command = $this->_conexion->prepare($p_nombreProcedimiento);
    for ($i = 0; $i < count($this->_parametros); $i++) {
        $command->bindParam($this->_parametros[$i]["nombre"], $this->_parametros[$i]["valor"], $this->_parametros[$i]["tipo"]);
    }
    $command->execute();
    $result = $command->fetchColumn();
    return $result;
  }

}
