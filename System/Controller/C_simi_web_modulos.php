<?
require_once("C_Interconexion_SQL.php");

class Simi_Modulos{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}

	
	function Generar_Codigo_Modulos_Sim(){
		if($this->oDBManager->conectar()==true){
			$sql="select (ISNULL(Max(COD_MUEBLE_MODULO),0)+1) as CODIGO from  TBL_MUEBLES_MODULO ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Lista_Modulos_Simi(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * from TBL_MUEBLES_MODULO WHERE ID_ESTADO = 1  ORDER BY ORDEN_MUEBLE_MODULO";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Ver_Datos_Modulos_Simi_x_Codigo($COD_MUEBLE_MODULO){

		if($this->oDBManager->conectar()==true){  
			$consulta="SELECT * FROM TBL_MUEBLES_MODULO WHERE COD_MUEBLE_MODULO = '$COD_MUEBLE_MODULO' ";
			//echo $COD_MUEBLE_MODULO
			// echo $consulta;
			$result = $this->oDBManager->execute($consulta);

			return $result;
		}
	}
	
	
	function Insertar_Archivo_Modulos_Simi($COD_MUEBLE_MODULO, $NOM_ARCHIVO, $PESO_ARCHIVO){
		if($this->oDBManager->conectar()==true){
			 $consulta="INSERT INTO TBL_MUEBLES_MODULO (COD_MUEBLE_MODULO, NOM_ARCHIVO, PESO_ARCHIVO, ID_ESTADO )
						VALUES('$COD_MUEBLE_MODULO', '$NOM_ARCHIVO', '$PESO_ARCHIVO', '0' )";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Actualiza_Archivo_Modulos_Simi($COD_MUEBLE_MODULO, $NOM_ARCHIVO, $PESO_ARCHIVO){
		if($this->oDBManager->conectar()==true){
			 $consulta="UPDATE TBL_MUEBLES_MODULO SET NOM_ARCHIVO='$NOM_ARCHIVO', PESO_ARCHIVO='$PESO_ARCHIVO' 
			 			WHERE COD_MUEBLE_MODULO = '$COD_MUEBLE_MODULO'";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Guarda_Datos_Modulos_Simi($COD_MUEBLE_MODULO, $DESC_MUEBLE_MODULO, $ORDEN_MUEBLE_MODULO){
		if($this->oDBManager->conectar()==true){
			 $consulta="UPDATE TBL_MUEBLES_MODULO SET DESC_MUEBLE_MODULO='$DESC_MUEBLE_MODULO', ORDEN_MUEBLE_MODULO='$ORDEN_MUEBLE_MODULO', ID_ESTADO = '1'
			 			WHERE COD_MUEBLE_MODULO = '$COD_MUEBLE_MODULO'";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Eliminar_Modulos_Simi($COD_MUEBLE_MODULO){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_MODULO SET ID_ESTADO=2 WHERE COD_MUEBLE_MODULO = '$COD_MUEBLE_MODULO' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}

	
}
?>