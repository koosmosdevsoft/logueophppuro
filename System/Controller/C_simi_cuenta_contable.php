<?
require_once("C_Interconexion_SQL.php");

class Simi_Cuenta_Contable{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
	
	
	function Simi_Generar_Codigo_Cuenta_Contable(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(COD_CTA_CONTABLE),0) + 1) as CODIGO FROM TBL_MUEBLES_CUENTA_CONTABLE ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
		
	function Simi_Insertar_Cuenta_Contable($COD_CTA_CONTABLE, $NRO_MAYOR, $NRO_SUBCUENTA, $NRO_CTA_CONTABLE, $NOM_CTA_CONTABLE, $TIP_CUENTA, $USO_CUENTA, $ACTIVO, $USUARIO_CREACION){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_CUENTA_CONTABLE  (COD_CTA_CONTABLE, NRO_MAYOR, NRO_SUBCUENTA, NRO_CTA_CONTABLE, NOM_CTA_CONTABLE, TIP_CUENTA, USO_CUENTA, ACTIVO, FECHA_REGISTRO, USUARIO_CREACION, FECHA_CREACION, ID_ESTADO)
						VALUES ('$COD_CTA_CONTABLE', '$NRO_MAYOR', '$NRO_SUBCUENTA', '$NRO_CTA_CONTABLE', '$NOM_CTA_CONTABLE', '$TIP_CUENTA', '$USO_CUENTA', '$ACTIVO', GETDATE(), '$USUARIO_CREACION', GETDATE(), 1) ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Simi_Actualizar_Cuenta_Contable($COD_CTA_CONTABLE, $NRO_MAYOR, $NRO_SUBCUENTA, $NRO_CTA_CONTABLE, $NOM_CTA_CONTABLE, $TIP_CUENTA, $USO_CUENTA, $ACTIVO, $USUARIO_MODIFICA){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_MUEBLES_CUENTA_CONTABLE
			SET NRO_MAYOR = '$NRO_MAYOR', NRO_SUBCUENTA = '$NRO_SUBCUENTA', NRO_CTA_CONTABLE = '$NRO_CTA_CONTABLE', NOM_CTA_CONTABLE = '$NOM_CTA_CONTABLE', TIP_CUENTA = '$TIP_CUENTA', USO_CUENTA = '$USO_CUENTA', ACTIVO = '$ACTIVO', USUARIO_MODIFICA = '$USUARIO_MODIFICA', FECHA_MODIFICA = GETDATE() 
			WHERE COD_CTA_CONTABLE = '$COD_CTA_CONTABLE' ";
			$result = $this->oDBManager->execute($sql);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Simi_Eliminar_Cuenta_Contable($COD_CTA_CONTABLE, $USUARIO_ELIMINA){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_CUENTA_CONTABLE SET ID_ESTADO = 2, USUARIO_ELIMINA = '$USUARIO_ELIMINA', FECHA_ELIMINA = GETDATE() WHERE COD_CTA_CONTABLE = '$COD_CTA_CONTABLE' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}	
	
	function Simi_Listar_Cuenta_Contable_Parametros($NRO_CTA_CONTABLE, $NOM_CTA_CONTABLE, $TIP_CUENTA, $USO_CUENTA, $ACTIVO){
		if($this->oDBManager->conectar()==true){
			
			if($NRO_CTA_CONTABLE != ''){
				$COND1 = " AND NRO_CTA_CONTABLE LIKE '%$NRO_CTA_CONTABLE%' ";
			}else{
				$COND1 = "";
			}
			
			if($NOM_CTA_CONTABLE != ''){
				$COND2 = " AND NOM_CTA_CONTABLE LIKE '%$NOM_CTA_CONTABLE%' ";
			}else{
				$COND2 = "";
			}
			
			if($TIP_CUENTA != '-'){
				$COND3 = " AND TIP_CUENTA = '$TIP_CUENTA' ";
			}else{
				$COND3 = "";
			}
			
			if($USO_CUENTA != '-'){
				$COND4 = " AND USO_CUENTA = '$USO_CUENTA' ";
			}else{
				$COND4 = "";
			}
			
			if($ACTIVO != '-'){
				$COND5 = " AND ACTIVO = '$ACTIVO' ";
			}else{
				$COND5 = "";
			}
			
			
			$sql="
  SELECT
  *,
  (CASE WHEN TIP_CUENTA = 'O' THEN 'CUENTA DE ORDEN'
		WHEN TIP_CUENTA = 'A' THEN 'ACTIVO FIJO'
		WHEN TIP_CUENTA = 'I' THEN 'OTROS'
	END) AS DES_TIP_CUENTA,
	(CASE WHEN USO_CUENTA = 'E' THEN 'USO ESTATAL'
		WHEN USO_CUENTA = 'P' THEN 'USO PRIVADO'
	END) AS DES_USO_CUENTA,
	(CASE WHEN ACTIVO = '0' THEN 'PRINCIPAL'
		WHEN ACTIVO = '1' THEN 'ESPECIFICA'
	END) AS DES_ESTADO_CUENTA,
	TEXT_NRO_CTA_CONTABLE = NRO_CTA_CONTABLE+REPLICATE(char(9),15)+NOM_CTA_CONTABLE 
  FROM  TBL_MUEBLES_CUENTA_CONTABLE 
  WHERE ID_ESTADO = 1 $COND1 $COND2 $COND3 $COND4 $COND5
  ORDER BY USO_CUENTA, CAST(NRO_MAYOR AS INT), CAST(NRO_SUBCUENTA AS INT)
  ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Simi_Listar_Cuenta_Contable_Activos(){
		if($this->oDBManager->conectar()==true){
			$sql="
  SELECT
  *,
  (CASE WHEN TIP_CUENTA = 'O' THEN 'CUENTA DE ORDEN'
		WHEN TIP_CUENTA = 'A' THEN 'ACTIVO FIJO'
		WHEN TIP_CUENTA = 'I' THEN 'OTROS'
	END) AS DES_TIP_CUENTA,
	(CASE WHEN USO_CUENTA = 'E' THEN 'USO ESTATAL'
		WHEN USO_CUENTA = 'P' THEN 'USO PRIVADO'
	END) AS DES_USO_CUENTA,
	(CASE WHEN ACTIVO = '0' THEN 'I'
		WHEN ACTIVO = '1' THEN 'A'
	END) AS DES_ESTADO_CUENTA
  FROM  TBL_MUEBLES_CUENTA_CONTABLE 
  WHERE ID_ESTADO = 1 AND ACTIVO = 1
  ORDER BY NRO_MAYOR, NOM_CTA_CONTABLE ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Simi_Ver_Cuenta_Contable_x_Codigo($COD_CTA_CONTABLE){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_CUENTA_CONTABLE WHERE COD_CTA_CONTABLE = '$COD_CTA_CONTABLE' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Simi_Listar_Cuenta_Contable_x_Parametros($USO_CUENTA, $TIP_CUENTA){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT *, 
			TEXT_NRO_CTA_CONTABLE = LEFT(NRO_CTA_CONTABLE+'**************',(SELECT max(len(NRO_CTA_CONTABLE)) FROM TBL_MUEBLES_CUENTA_CONTABLE)+5)
			FROM TBL_MUEBLES_CUENTA_CONTABLE WHERE USO_CUENTA = '$USO_CUENTA' AND TIP_CUENTA = '$TIP_CUENTA' AND ID_ESTADO = 1 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
}
?>