<?php

class UbigeoController{
	private $objModel;
	
	function __construct() {
		$this->objModel = new UbigeoModel();
	}
	
	function Lista_Region(){
		return $this->objModel->Lista_Region();
	}
	
	function Lista_Departamento(){
		return $this->objModel->Lista_Departamento();
	}
	
	function Lista_Provincia($cod_dpto){
		return $this->objModel->Lista_Provincia($cod_dpto);
	}
	
	function Lista_Distrito($cod_dpto, $cod_prov){
		return $this->objModel->Lista_Distrito($cod_dpto, $cod_prov);
	}
	
	function Lista_Departamento_x_ID($CODDEP){
		return $this->objModel->Lista_Departamento_x_ID($CODDEP);
	}
	
	function Lista_Provincia_x_ID($cod_dpto, $CODPROV){
		return $this->objModel->Lista_Provincia_x_ID($cod_dpto, $CODPROV);
	}
	
	function Lista_Distrito_x_ID($cod_dpto, $cod_prov, $CODDIST){
		return $this->objModel->Lista_Distrito_x_ID($cod_dpto, $cod_prov, $CODDIST);
	}
	
	function Lista_Departamento_SUNAT(){
		return $this->objModel->Lista_Departamento_SUNAT();
	}
	
	function Lista_Provincia_SUNAT($cod_dpto){
		return $this->objModel->Lista_Provincia_SUNAT($cod_dpto);
	}
	
	function Lista_Distrito_SUNAT($cod_dpto, $cod_prov){
		return $this->objModel->Lista_Distrito_SUNAT($cod_dpto, $cod_prov);
	}

}


?>