<?php
class Database{

	// set when connect() is called, defined in set_db_info() 
	private $dns	 		= ''; 
	private $userName 		= ''; 
	private $_server 		= ''; 
	private $_dbname 		= ''; 
	private $password 		= ''; 
	private $connectionID 	= -1; 
	private $row 			= -1; 	// a row counter, needed to loop through records in postgres. 
	private $result 		= null;	// point to result set. 

	public function __construct() {
		
		$this->dns 			= 'ODBC_SQLSERVER_64_SINABIPWEB'; 		
		$this->userName 	= 'appsinabip'; 
		$this->password 	= '4tm0sf3r42020+';
		$this->_server 		= "SRV-SINABIP-BD";
		$this->_dbname 		= "SINABIP_SBN";	
			
	}
	 
	 
	// connection function 
	public function conectar(){ 
		$conn = "DRIVER={ODBC Driver 17 for SQL Server};SERVER=$this->_server;PORT=1433;DATABASE=$this->_dbname";
		// odbc_connect($conn, $_user, $_pass);

		if(odbc_connect($conn,$this->userName,$this->password)){
			$connID = odbc_connect($conn,$this->userName,$this->password) or die("<h1>Lo sentimos, No hay conexión con el servidor de base de datos <br> Comunicarse con el Administrador del Sistema</h1>");	
			
			if ($connID != "") { 
				$this->connectionID = $connID; 
				return $this->connectionID ; 
			} else { 
				
				// FATAL ERROR - CONNECTI0N ERROR 
				$this->connectionID = -1; 
				return 0; 
			}
		}else{

			 echo "<script language='javascript'>alert('No Hay comunicación con la Base de Datos. Comuniquese con la SBN.')</script>";
	
		}
		 
	} 
	
	// standard method to close connection 
	public function close() { 
		if ($this->connectionID != "-1") { 
			$closed = odbc_close($this->connectionID);			
			return $closed; 
		} else { 
			return null; 
		} 
	} 
	
	
	public function freeMemory($Resultado_Query) { 
		$this->result = odbc_free_result($Resultado_Query);
		return $this->result; 
	}
	
	
	public function execute($consulta){
		$this->result = odbc_exec($this->connectionID,$consulta);
		return $this->result; 
	}
	
 
}
?>