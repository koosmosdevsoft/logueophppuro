<?
require_once("C_Interconexion_SQL.php");

class Simi_UE_Personal{
	
	private $oDBManager;

    function __construct(){
		$this->oDBManager	=	new Database;
	}
	
	
	function Simi_UE_Personal_Generar_Codigo(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(COD_UE_PERS),0) + 1) as CODIGO FROM TBL_MUEBLES_UE_PERSONAL ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Insertar_Simi_UE_Personal($COD_ENTIDAD, $UE_NOMBRES_PERS, $UE_APEPAT_PERS, $UE_APEMAT_PERS, $COD_MODALIDAD, $TIP_DOCUMENTO, $NRO_DOCUMENTO, $ID_PREDIO_SBN, $COD_UE_AREA, $COD_UE_OFIC, $USUARIO_CREACION){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_UE_PERSONAL(COD_ENTIDAD, UE_NOMBRES_PERS, UE_APEPAT_PERS, UE_APEMAT_PERS, COD_MODALIDAD, TIP_DOCUMENTO, NRO_DOCUMENTO, ID_PREDIO_SBN, COD_UE_AREA, COD_UE_OFIC, FECHA_REGISTRO, USUARIO_CREACION, FECHA_CREACION, ID_ESTADO)
						VALUES('$COD_ENTIDAD', '$UE_NOMBRES_PERS', '$UE_APEPAT_PERS', '$UE_APEMAT_PERS', '$COD_MODALIDAD', '$TIP_DOCUMENTO', '$NRO_DOCUMENTO', '$ID_PREDIO_SBN', '$COD_UE_AREA', '$COD_UE_OFIC', GETDATE(), '$USUARIO_CREACION', GETDATE(),'1') ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Actualizar_Simi_UE_Personal($COD_UE_PERS, $COD_ENTIDAD, $UE_NOMBRES_PERS, $UE_APEPAT_PERS, $UE_APEMAT_PERS, $COD_MODALIDAD, $TIP_DOCUMENTO, $NRO_DOCUMENTO, $ID_PREDIO_SBN, $COD_UE_AREA, $COD_UE_OFIC, $USUARIO_MODIFICA){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_MUEBLES_UE_PERSONAL 
			SET COD_ENTIDAD='$COD_ENTIDAD', UE_NOMBRES_PERS='$UE_NOMBRES_PERS', UE_APEPAT_PERS='$UE_APEPAT_PERS', UE_APEMAT_PERS='$UE_APEMAT_PERS', COD_MODALIDAD='$COD_MODALIDAD', TIP_DOCUMENTO = '$TIP_DOCUMENTO', NRO_DOCUMENTO = '$NRO_DOCUMENTO', ID_PREDIO_SBN = '$ID_PREDIO_SBN', COD_UE_AREA = '$COD_UE_AREA', COD_UE_OFIC = '$COD_UE_OFIC', USUARIO_MODIFICA = '$USUARIO_MODIFICA', FECHA_MODIFICA = GETDATE()
			WHERE COD_UE_PERS = $COD_UE_PERS";
			$result = $this->oDBManager->execute($sql);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Eliminar_Simi_UE_Personal($COD_UE_PERS, $USUARIO_ELIMINA){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_UE_PERSONAL SET ID_ESTADO = 2, USUARIO_ELIMINA = '$USUARIO_ELIMINA', FECHA_ELIMINA = GETDATE() WHERE COD_UE_PERS = '$COD_UE_PERS' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}	
	
	
	
	function Ver_Datos_Simi_UE_Personal_x_CODIGO($COD_UE_PERS){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT P.*, L.DENOMINACION_PREDIO, A.DESC_AREA, O.DESC_OFIC 
			FROM TBL_MUEBLES_UE_PERSONAL P
			LEFT JOIN TBL_PADRON_PREDIOS L ON (P.ID_PREDIO_SBN = L.ID_PREDIO_SBN AND L.ID_ESTADO = 1)
			LEFT JOIN TBL_MUEBLES_UE_AREA A ON (P.COD_UE_AREA = A.COD_UE_AREA AND A.ID_ESTADO = 1)
			LEFT JOIN TBL_MUEBLES_UE_OFICINA O ON (P.COD_UE_OFIC = O.COD_UE_OFIC AND O.ID_ESTADO = 1)
			WHERE P.COD_UE_PERS = '$COD_UE_PERS' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	
	function TOTAL_Simi_UE_Personal_X_PARAMETROS($COD_ENTIDAD, $NOMBRES){
		if($this->oDBManager->conectar()==true){
			
			if($NOMBRES != ''){
				$COND1 = " AND (UE_APEPAT_PERS+' '+UE_APEMAT_PERS+' '+UE_NOMBRES_PERS) LIKE '%$NOMBRES%' ";
			}else{
				$COND1 = "";
			}
			
			$sql="SELECT COUNT(*) AS TOT_REG FROM TBL_MUEBLES_UE_PERSONAL WHERE ID_ESTADO  = '1' AND COD_ENTIDAD = '$COD_ENTIDAD' $COND1 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function LISTA_Simi_UE_Personal_X_PARAMETROS($INI, $FIN, $COD_ENTIDAD, $NOMBRES){
		if($this->oDBManager->conectar()==true){
			
			if($NOMBRES != ''){
				$COND1 = " AND (P.UE_APEPAT_PERS+' '+P.UE_APEMAT_PERS+' '+P.UE_NOMBRES_PERS) LIKE '%$NOMBRES%' ";
			}else{
				$COND1 = "";
			}
			
			$SQL_ORIGEN="SELECT P.*, M.DESC_MODALIDAD, ROW_NUMBER() OVER (ORDER BY P.COD_UE_PERS) AS ROW_NUMBER_ID
						FROM TBL_MUEBLES_UE_PERSONAL P
						LEFT JOIN TBL_INC_MODALIDAD M ON (P.COD_MODALIDAD = M.COD_MODALIDAD)
						WHERE P.ID_ESTADO = '1' AND P.COD_ENTIDAD = '$COD_ENTIDAD' $COND1 
				";			
			$sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
		
	function Listar_Simi_UE_Personal_x_Entidad_Local_area_oficina($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_UE_AREA, $COD_UE_OFIC){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_UE_PERSONAL 
				  --WHERE ID_ESTADO = 1 AND COD_ENTIDAD = '$COD_ENTIDAD' AND ID_PREDIO_SBN = '$ID_PREDIO_SBN' AND COD_UE_AREA = '$COD_UE_AREA' AND COD_UE_OFIC = '$COD_UE_OFIC' 
				  WHERE ID_ESTADO = 1 AND COD_ENTIDAD = '$COD_ENTIDAD' AND ID_PREDIO_SBN = '$ID_PREDIO_SBN' AND COD_UE_AREA = '$COD_UE_AREA'
				  ";
				  
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function Listar_Simi_UE_Personal_x_Entidad_Local_area_oficina_NEWSINABIP($COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_MUEBLES_UE_PERSONAL 
				  WHERE ID_ESTADO = 1 AND COD_ENTIDAD = '$COD_ENTIDAD'";
			//echo $sql;				  
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	


	//-------- REPORTE PERSONAL X ENTIDAD
	
	function RPT_TOTAL_Personal_SIMI_X_Local($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_UE_AREA, $COD_UE_OFIC, $NRO_DOCUMENTO, $UE_NOMBRES_PERS, $UE_APEPAT_PERS, $UE_APEMAT_PERS){
		if($this->oDBManager->conectar()==true){
			
			if($ID_PREDIO_SBN != '-'){
				$COND1 = " AND P.ID_PREDIO_SBN = '$ID_PREDIO_SBN' ";
			}else{
				$COND1 = "";
			}
			
			if($COD_UE_AREA != '-'){
				$COND2 = " AND A.COD_UE_AREA = '$COD_UE_AREA' ";
			}else{
				$COND2 = "";
			}
			
			if($COD_UE_OFIC != '-'){
				$COND3 = " AND O.COD_UE_OFIC = '$COD_UE_OFIC' ";
			}else{
				$COND3 = "";
			}
			
			if($NRO_DOCUMENTO != ''){
				$COND4 = " AND PE.NRO_DOCUMENTO = '$NRO_DOCUMENTO' ";
			}else{
				$COND4 = "";
			}
			
			if($UE_NOMBRES_PERS != ''){
				$COND5 = " AND PE.UE_NOMBRES_PERS LIKE '%$UE_NOMBRES_PERS%' ";
			}else{
				$COND5 = "";
			}
			
			if($UE_APEPAT_PERS != ''){
				$COND6 = " AND PE.UE_APEPAT_PERS LIKE '%$UE_APEPAT_PERS%' ";
			}else{
				$COND6 = "";
			}
			
			if($UE_APEMAT_PERS != ''){
				$COND7 = " AND PE.UE_APEMAT_PERS LIKE '%$UE_APEMAT_PERS%' ";
			}else{
				$COND7 = "";
			}
			
			$sql="SELECT COUNT(*) AS TOT_REG 
				FROM TBL_MUEBLES_UE_PERSONAL PE
				LEFT JOIN TBL_PADRON_PREDIOS P ON (PE.ID_PREDIO_SBN = P.ID_PREDIO_SBN AND P.ID_ESTADO = 1)
				LEFT JOIN TBL_MUEBLES_UE_AREA A  ON (A.COD_UE_AREA = PE.COD_UE_AREA AND A.ID_ESTADO = 1)
				LEFT JOIN TBL_MUEBLES_UE_OFICINA O ON (O.COD_UE_OFIC = PE.COD_UE_OFIC AND O.ID_ESTADO =1)
				WHERE PE.COD_ENTIDAD = '$COD_ENTIDAD'  
				AND PE.ID_ESTADO = 1 
				$COND1 $COND2 $COND3 $COND4 $COND5 $COND6 $COND7
				";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function RPT_LISTA_Personal_SIMI_X_Local($INI, $FIN, $COD_ENTIDAD, $ID_PREDIO_SBN, $COD_UE_AREA, $COD_UE_OFIC, $NRO_DOCUMENTO, $UE_NOMBRES_PERS, $UE_APEPAT_PERS, $UE_APEMAT_PERS){
		
		if($this->oDBManager->conectar()==true){
			
			if($ID_PREDIO_SBN != '-'){
				$COND1 = " AND P.ID_PREDIO_SBN = '$ID_PREDIO_SBN' ";
			}else{
				$COND1 = "";
			}
			
			if($COD_UE_AREA != '-'){
				$COND2 = " AND A.COD_UE_AREA = '$COD_UE_AREA' ";
			}else{
				$COND2 = "";
			}
			
			if($COD_UE_OFIC != '-'){
				$COND3 = " AND O.COD_UE_OFIC = '$COD_UE_OFIC' ";
			}else{
				$COND3 = "";
			}
			
			if($NRO_DOCUMENTO != ''){
				$COND4 = " AND PE.NRO_DOCUMENTO = '$NRO_DOCUMENTO' ";
			}else{
				$COND4 = "";
			}
			
			if($UE_NOMBRES_PERS != ''){
				$COND5 = " AND PE.UE_NOMBRES_PERS LIKE '%$UE_NOMBRES_PERS%' ";
			}else{
				$COND5 = "";
			}
			
			if($UE_APEPAT_PERS != ''){
				$COND6 = " AND PE.UE_APEPAT_PERS LIKE '%$UE_APEPAT_PERS%' ";
			}else{
				$COND6 = "";
			}
			
			if($UE_APEMAT_PERS != ''){
				$COND7 = " AND PE.UE_APEMAT_PERS LIKE '%$UE_APEMAT_PERS%' ";
			}else{
				$COND7 = "";
			}
			
			$SQL_ORIGEN="SELECT
						ROW_NUMBER() OVER (ORDER BY PE.UE_APEPAT_PERS, PE.UE_APEMAT_PERS, PE.UE_NOMBRES_PERS) AS ROW_NUMBER_ID,
						PE.COD_UE_PERS,
						PE.UE_APEMAT_PERS,
						PE.UE_APEPAT_PERS,
						PE.UE_NOMBRES_PERS,
						PE.NRO_DOCUMENTO,
						P.ID_PREDIO_SBN,
						P.DENOMINACION_PREDIO,
						P.COD_DEPA,
						P.COD_PROV,
						P.COD_DIST,
						A.COD_UE_AREA,
						A.DESC_AREA,
						O.COD_UE_OFIC,
						O.DESC_OFIC
						FROM TBL_MUEBLES_UE_PERSONAL PE
						LEFT JOIN TBL_PADRON_PREDIOS P ON (PE.ID_PREDIO_SBN = P.ID_PREDIO_SBN AND P.ID_ESTADO = 1)
						LEFT JOIN TBL_MUEBLES_UE_AREA A  ON (A.COD_UE_AREA = PE.COD_UE_AREA AND A.ID_ESTADO = 1)
						LEFT JOIN TBL_MUEBLES_UE_OFICINA O ON (O.COD_UE_OFIC = PE.COD_UE_OFIC AND O.ID_ESTADO =1)
						WHERE PE.COD_ENTIDAD = '$COD_ENTIDAD'  
						AND PE.ID_ESTADO = 1
						$COND1 $COND2 $COND3 $COND4 $COND5 $COND6 $COND7
						";	
								
			$sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ORDER BY 1";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}


	function RPT_LISTA_TODO_Personal_SIMI_X_Local_X_Parametros($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_UE_AREA, $COD_UE_OFIC, $NRO_DOCUMENTO, $UE_NOMBRES_PERS, $UE_APEPAT_PERS, $UE_APEMAT_PERS){
		if($this->oDBManager->conectar()==true){
			
			if($ID_PREDIO_SBN != '-'){
				$COND1 = " AND P.ID_PREDIO_SBN = '$ID_PREDIO_SBN' ";
			}else{
				$COND1 = "";
			}
			
			if($COD_UE_AREA != '-'){
				$COND2 = " AND A.COD_UE_AREA = '$COD_UE_AREA' ";
			}else{
				$COND2 = "";
			}
			
			if($COD_UE_OFIC != '-'){
				$COND3 = " AND O.COD_UE_OFIC = '$COD_UE_OFIC' ";
			}else{
				$COND3 = "";
			}
			
			if($NRO_DOCUMENTO != ''){
				$COND4 = " AND PE.NRO_DOCUMENTO = '$NRO_DOCUMENTO' ";
			}else{
				$COND4 = "";
			}
			
			if($UE_NOMBRES_PERS != ''){
				$COND5 = " AND PE.UE_NOMBRES_PERS LIKE '%$UE_NOMBRES_PERS%' ";
			}else{
				$COND5 = "";
			}
			
			if($UE_APEPAT_PERS != ''){
				$COND6 = " AND PE.UE_APEPAT_PERS LIKE '%$UE_APEPAT_PERS%' ";
			}else{
				$COND6 = "";
			}
			
			if($UE_APEMAT_PERS != ''){
				$COND7 = " AND PE.UE_APEMAT_PERS LIKE '%$UE_APEMAT_PERS%' ";
			}else{
				$COND7 = "";
			}
			
			$SQL_ORIGEN="SELECT
						ROW_NUMBER() OVER (ORDER BY PE.UE_APEPAT_PERS, PE.UE_APEMAT_PERS, PE.UE_NOMBRES_PERS) AS ROW_NUMBER_ID,
						PE.COD_UE_PERS,
						PE.UE_APEMAT_PERS,
						PE.UE_APEPAT_PERS,
						PE.UE_NOMBRES_PERS,
						PE.NRO_DOCUMENTO,
						P.ID_PREDIO_SBN,
						P.DENOMINACION_PREDIO,
						P.COD_DEPA,
						P.COD_PROV,
						P.COD_DIST,
						A.COD_UE_AREA,
						A.DESC_AREA,
						O.COD_UE_OFIC,
						O.DESC_OFIC
						FROM TBL_MUEBLES_UE_PERSONAL PE
						LEFT JOIN TBL_PADRON_PREDIOS P ON (PE.ID_PREDIO_SBN = P.ID_PREDIO_SBN AND P.ID_ESTADO = 1)
						LEFT JOIN TBL_MUEBLES_UE_AREA A  ON (A.COD_UE_AREA = PE.COD_UE_AREA AND A.ID_ESTADO = 1)
						LEFT JOIN TBL_MUEBLES_UE_OFICINA O ON (O.COD_UE_OFIC = PE.COD_UE_OFIC AND O.ID_ESTADO =1)
						WHERE PE.COD_ENTIDAD = '$COD_ENTIDAD'  
						AND PE.ID_ESTADO = 1
						$COND1 $COND2 $COND3 $COND4 $COND5 $COND6 $COND7
						";	
			$resultado = $this->oDBManager->execute($SQL_ORIGEN);
			return $resultado;
		}
	}
	
	
	function TOTAL_UE_Busqueda_Personal_X_Varios_Parametros($COD_ENTIDAD, $NRO_DOCUMENTO, $UE_NOMBRES_PERS, $UE_APEPAT_PERS, $UE_APEMAT_PERS){
		if($this->oDBManager->conectar()==true){
			
			if($NRO_DOCUMENTO != ''){
				$COND1 = " AND P.NRO_DOCUMENTO = '$NRO_DOCUMENTO' ";
			}else{
				$COND1 = "";
			}
			
			
			if($UE_NOMBRES_PERS != ''){
				$COND2 = " AND P.UE_NOMBRES_PERS LIKE '%$UE_NOMBRES_PERS%' ";
			}else{
				$COND2 = "";
			}
			
			
			if($UE_APEPAT_PERS != ''){
				$COND3 = " AND P.UE_APEPAT_PERS LIKE '%$UE_APEPAT_PERS%' ";
			}else{
				$COND3 = "";
			}
			
			
			if($UE_APEMAT_PERS != ''){
				$COND4 = " AND P.UE_APEMAT_PERS LIKE '%$UE_APEMAT_PERS%' ";
			}else{
				$COND4 = "";
			}
			
			$sql="
			SELECT COUNT(*) AS TOT_REG 
			FROM TBL_MUEBLES_UE_PERSONAL P
			WHERE P.ID_ESTADO  = '1' 
			AND P.COD_ENTIDAD = '$COD_ENTIDAD' $COND1 $COND2 $COND3 $COND4 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Lista_UE_Busqueda_Personal_X_Varios_Parametros($INI, $FIN, $COD_ENTIDAD, $NRO_DOCUMENTO, $UE_NOMBRES_PERS, $UE_APEPAT_PERS, $UE_APEMAT_PERS){
		if($this->oDBManager->conectar()==true){
			
			
			if($NRO_DOCUMENTO != ''){
				$COND1 = " AND P.NRO_DOCUMENTO = '$NRO_DOCUMENTO' ";
			}else{
				$COND1 = "";
			}
			
			
			if($UE_NOMBRES_PERS != ''){
				$COND2 = " AND P.UE_NOMBRES_PERS LIKE '%$UE_NOMBRES_PERS%' ";
			}else{
				$COND2 = "";
			}
			
			
			if($UE_APEPAT_PERS != ''){
				$COND3 = " AND P.UE_APEPAT_PERS LIKE '%$UE_APEPAT_PERS%' ";
			}else{
				$COND3 = "";
			}
			
			
			if($UE_APEMAT_PERS != ''){
				$COND4 = " AND P.UE_APEMAT_PERS LIKE '%$UE_APEMAT_PERS%' ";
			}else{
				$COND4 = "";
			}
			
			
			$SQL_ORIGEN="
SELECT 
P.*, 
M.DESC_MODALIDAD, 
ROW_NUMBER() OVER (ORDER BY P.COD_UE_PERS) AS ROW_NUMBER_ID
FROM TBL_MUEBLES_UE_PERSONAL P
LEFT JOIN TBL_INC_MODALIDAD M ON (P.COD_MODALIDAD = M.COD_MODALIDAD)
WHERE P.ID_ESTADO = '1' 
AND P.COD_ENTIDAD = '$COD_ENTIDAD' $COND1 $COND2 $COND3 $COND4 
";
			$sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
}
?>