<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<?

require_once('../../../utils/funciones/funciones.php');
require_once('../../Controller/C_padron_entidad.php');
require_once('../../Controller/C_padron_predios.php');
require_once('../../Controller/C_simi_m_importar_excel_muebles.php');

$oPadron_Entidad				=	new Padron_Entidad;
$oPadron_Predios				=	new Padron_Predios;
$oSimi_Importar_Excel_Muebles	=	new Simi_Importar_Excel_Muebles;

$aCOD_ENTIDAD 		= $_GET['sCODENTIDAD'];
$sCodRespMuebles 	= $_GET['sCodRespMuebles'];
  

if($aCOD_ENTIDAD !=''){
	$RS_ENTIDAD 			= $oPadron_Entidad->Ver_Datos_Padron_Entidad_x_COD_ENTIDAD($aCOD_ENTIDAD);
	$NOM_ENTIDAD_RESP		= utf8_encode(odbc_result($RS_ENTIDAD,"NOM_ENTIDAD"));
	
	
}


if($sCodRespMuebles == ''){
	
	$RS_Existe 			= $oSimi_Importar_Excel_Muebles->Total_Datos_Responsable_Control_Patrimonial_x_Entidad($aCOD_ENTIDAD);
	$TOT_REG		= odbc_result($RS_Existe,"TOT_REG");

  //SE AGREGO
  $RS_RESPONSABLE_E   = '';
  $DNI_X1       = '';
  $NOMB_RESP_X1   = '';
  $APEPAT_RESP_X1   = '';
  $APEMAT_RESP_X1   = '';
  $ID_PREDIO_SBN_X1   = '';
  $NOMB_AREA_X1   = '';
  $NOMB_OFICINA_X1  = '';
  $ENVIO_DAT_RESPONSABLE_X1 = '';
  //FIN
  	
}else if($sCodRespMuebles !=''){
	
	$TOT_REG = 0;
	
	$RS_RESPONSABLE_E 	= $oSimi_Importar_Excel_Muebles->Ver_Datos_Responsable_Control_Patrimonial_x_Codigo($sCodRespMuebles);
	$DNI_X1				= utf8_encode(odbc_result($RS_RESPONSABLE_E,"DNI"));
	$NOMB_RESP_X1		= utf8_encode(odbc_result($RS_RESPONSABLE_E,"NOMB_RESP"));
	$APEPAT_RESP_X1		= utf8_encode(odbc_result($RS_RESPONSABLE_E,"APEPAT_RESP"));
	$APEMAT_RESP_X1		= utf8_encode(odbc_result($RS_RESPONSABLE_E,"APEMAT_RESP"));
	$ID_PREDIO_SBN_X1		= utf8_encode(odbc_result($RS_RESPONSABLE_E,"ID_PREDIO_SBN"));
	$NOMB_AREA_X1		= utf8_encode(odbc_result($RS_RESPONSABLE_E,"NOMB_AREA"));
	$NOMB_OFICINA_X1	= utf8_encode(odbc_result($RS_RESPONSABLE_E,"NOMB_OFICINA"));
	
	$ENVIO_DAT_RESPONSABLE_X1	= utf8_encode(odbc_result($RS_RESPONSABLE_E,"ENVIO_DAT_RESPONSABLE"));
	
	/*
	if($ENVIO_DAT_RESPONSABLE_X1 == 'X'){
		$disble_boton_resp ='disabled="disabled"';
		$disble_boton_resp_text ='readonly="readonly"';
	}else{
		$disble_boton_resp ='';
		$disble_boton_resp_text ='';
	}
	*/
	
}



?>

<? if($TOT_REG<1){ ?>

<table width="641" border="0" cellpadding="0" cellspacing="3">
  <tr>
    <td width="635" height="20" ><a style="float:right" href="#" onclick="$('#div_formulario').html('')" ><img src="../webimages/iconos/cerrar_2.png" width="20" height="20" border="0" /></a></td>
  </tr>
  <tr>
    <td height="20" class="Titulo_plomo_20px">Formulario registro de responsable de control patrimonial</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="623" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" bgcolor="#F4F4F4">
      <tr>
        <td width="20">&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td><table width="582" border="0" cellpadding="0" cellspacing="3" class="TABLE_border4">
          <tr>
            <td width="16">&nbsp;</td>
            <td width="105">&nbsp;</td>
            <td>&nbsp;</td>
            <td><input name="TXH_COD_ENTIDAD_RESP" type="hidden" id="TXH_COD_ENTIDAD_RESP" style="width:140px" value="<?=$aCOD_ENTIDAD?>" /></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td class="texto_arial_azul_n_10">Entidad</td>
            <td width="17" class="texto_arial_azul_n_10">:</td>
            <td width="427"><input name="TXT_NOM_ENTIDAD_RESP" type="text" id="TXT_NOM_ENTIDAD_RESP" style="width:400px" value="<?=$NOM_ENTIDAD_RESP?>" readonly="readonly" /></td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
        </table></td>
        </tr>
      <tr>
        <td height="30">&nbsp;</td>
        <td class="texto_arial_plomo_n_12">Datos Generales del Responsable de Control Patrimonial</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>
          
          <table width="577" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4">
            <tr>
              <td width="16">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td><input name="TXH_COD_IMPORT_EXCEL_RESP" type="hidden" id="TXH_COD_IMPORT_EXCEL_RESP" style="width:140px" value="<?=$sCodRespMuebles?>" /></td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td width="108" class="texto_arial_azul_n_10">Nro de DNI </td>
              <td width="21" class="texto_arial_azul_n_10">:</td>
              <td width="420"><input name="TXT_NRO_DNI_RESP" type="text" id="TXT_NRO_DNI_RESP" style="width:140px" value="<?=$DNI_X1?>" maxlength="8"   /></td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="texto_arial_azul_n_10">Nombres</td>
              <td class="texto_arial_azul_n_10">:</td>
              <td><input name="TXT_NOMB_RESP" type="text" id="TXT_NOMB_RESP" style="width:250px" value="<?=$NOMB_RESP_X1?>" /></td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="texto_arial_azul_n_10">Apellido Paterno</td>
              <td class="texto_arial_azul_n_10">:</td>
              <td><input name="TXT_APEPAT_RESP" type="text" id="TXT_APEPAT_RESP" style="width:250px" value="<?=$APEPAT_RESP_X1?>"  /></td>
              </tr>
            <tr>
              <td height="25">&nbsp;</td>
              <td><span class="texto_arial_azul_n_10">Apellido Materno</span></td>
              <td>:</td>
              <td><input name="TXT_APEMAT_RESP" type="text" id="TXT_APEMAT_RESP" style="width:250px" value="<?=$APEMAT_RESP_X1?>"  /></td>
              </tr>
            <tr>
              <td height="25">&nbsp;</td>
              <td colspan="3"><hr class="hr_01" /></td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="texto_arial_azul_n_10">Nombre Local</td>
              <td class="texto_arial_azul_n_10">:</td>
              <td><select name="cbo_local_Responsable" id="cbo_local_Responsable" style="width:400px" >
                <option value="0" selected="selected">:: Seleccione Local :: </option>
                <?
                
                $RSTDatosPredio_LResp = $oPadron_Predios->LISTA_PADRON_PREDIOS_X_ENTIDAD($aCOD_ENTIDAD);
                
                while (odbc_fetch_row($RSTDatosPredio_LResp)){
                
                $ID_PREDIO_SBN_LResp			= odbc_result($RSTDatosPredio_LResp,"ID_PREDIO_SBN");
                $DENOMINACION_PREDIO_LResp	= utf8_encode(odbc_result($RSTDatosPredio_LResp,"DENOMINACION_PREDIO"));
				
				if($ID_PREDIO_SBN_X1 == $ID_PREDIO_SBN_LResp){
					$select_Lresp = 'selected="selected"';
				}else{
					$select_Lresp = ' ';
				}
                
                ?>
                <option value="<?=$ID_PREDIO_SBN_LResp?>" <?=$select_Lresp?> >
                  <?=$DENOMINACION_PREDIO_LResp?>
                  </option>
                <?
                }
                ?>
                </select></td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td class="texto_arial_azul_n_10"> Area</td>
              <td class="texto_arial_azul_n_10">:</td>
              <td><input name="TXT_NOM_AREA_RESP" type="text" id="TXT_NOM_AREA_RESP" style="width:400px" value="<?=$NOMB_AREA_X1?>" /></td>
              </tr>
            <!-- <tr>
              <td>&nbsp;</td>
              <td class="texto_arial_azul_n_10">Oficina</td>
              <td class="texto_arial_azul_n_10">:</td>
              <td><input name="TXT_NOM_OFICINA_RESP" type="text" id="TXT_NOM_OFICINA_RESP" style="width:400px" value="<?=$NOMB_OFICINA_X1?>"  /></td>
            </tr> -->
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              </tr>
            </table>
          
          
          
        </td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td width="595">
		<? //if($ENVIO_DAT_RESPONSABLE_X1 == 'X'){ ?>
         <!-- <span class="texto_arial_rojo_n_11">* Los Datos del Responsable de Control Patrimonial ya fue enviado a la SBN</span>-->
          <? //}else{?>
          <table width="492" border="0" cellspacing="3" cellpadding="0">
            <tr>
              <td width="14">&nbsp;</td>
              <td width="85">
              <input name="button"   type="button"  class="btn btn-primary" id="button"  value="Registrar" onclick="GUARDAR_DATOS_RESPONSABLE()" /></td>
              <td width="381"></td>
              </tr>
            </table>
          <? //}?></td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
  </tr>
  <tr>
    <td height="30"><hr class="linea_separador_01" /></td>
  </tr>
</table>
<?
}else{
?>
<table width="638" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td width="19">&nbsp;</td>
    <td width="594">&nbsp;</td>
    <td width="25"><a href="#" onclick='$("#div_formulario").html("&nbsp;");'><img src="../webimages/iconos/cerrar_2.png" width="20" height="20" border="0" /></a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><table width="614" border="0" cellspacing="2" cellpadding="0" class="TABLE_border4" bgcolor="#F4F4F4">
      <tr>
        <td width="20">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td width="16">&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td width="68"><img src="../webimages/iconos/aviso.png" width="52" height="46" /></td>
        <td width="498" class="texto_arial_rojo_n_14">Solo puede registrar a un solo responsable de Control Patrimonial</td>
        <td>&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<?	
}
?>
