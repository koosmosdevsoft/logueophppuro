<? session_start();
// try{
	if ( !isset( $_SESSION['th_SIMI_COD_ENTIDAD'] ) && $_SESSION['th_SIMI_COD_ENTIDAD']=='' ) {
		echo '<script language="javascript">parent.location.href="../sinabip.php";</script>';
	}
	// NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
// }catch ( Exception $e ){
// 	echo '<script language="javascript">parent.location.href="../sinabip.php";</script>';
// }

// if($_SESSION['th_SIMI_COD_ENTIDAD']=='') echo '<script language="javascript">parent.location.href="../sinabip.php";</script>';
$COD_ENTIDAD_X1			= $_SESSION['th_SIMI_COD_ENTIDAD'];

require_once('../../Controller/C_simi_forma_adquisicion.php');
$oSimi_Forma_Adquisicion	=	new Simi_Forma_Adquisicion;

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 


<script>

function fShowFormulario_Agregar_Responsable(s_COD_IMPORT_EXCEL_CAB){
	
	var cod_tr = 'TR_'+s_COD_IMPORT_EXCEL_CAB;
	var TXH_SIMI_COD_ENTIDAD 		= $("#TXH_SIMI_COD_ENTIDAD").val();
	
	ajaxpage("View/simi_web_importar_add_resp/simi_web_importar_add_resp_principal_form.php?sCodRespMuebles="+s_COD_IMPORT_EXCEL_CAB+"&sCODENTIDAD="+TXH_SIMI_COD_ENTIDAD,"div_formulario") ;
	
	var ulA = document.getElementById("tbl_Resultado_Lista_Alta_BP"); 
	var liNodesA = ulA.getElementsByTagName("TR"); 
	var sTot =  liNodesA.length;
	
	var color = "";
	var obj = "";
	
	for( var s = 1; s <= sTot; s++ ){ 
		obj = liNodesA[s].id;
		
		if(cod_tr == obj){
			color = "#e2f2ff";
		}else{
			color = "";
		}
				
		$("#"+obj+"").css("background-color",color);
		
	}
}




function GUARDAR_DATOS_RESPONSABLE(){
	
	var TXH_SIMI_COD_USUARIO 		= $("#TXH_SIMI_COD_USUARIO").val();
	var TXH_COD_ENTIDAD_RESP 		= $("#TXH_COD_ENTIDAD_RESP").val();
	
	var TXH_COD_IMPORT_EXCEL_RESP 	= $("#TXH_COD_IMPORT_EXCEL_RESP").val();
	
	var TXT_NRO_DNI_RESP 			= $("#TXT_NRO_DNI_RESP").val();
	var TXT_NOMB_RESP 				= $("#TXT_NOMB_RESP").val();
	var TXT_APEPAT_RESP 			= $("#TXT_APEPAT_RESP").val();	
	var TXT_APEMAT_RESP 			= $("#TXT_APEMAT_RESP").val();
	var cbo_local_Responsable 			= $("#cbo_local_Responsable").val();
	var TXT_NOM_AREA_RESP 			= $("#TXT_NOM_AREA_RESP").val();
	var TXT_NOM_OFICINA_RESP 		= "";
	
	if(TXT_NRO_DNI_RESP == ''){
		alert('Ingrese Nro DNI del Responsable de Control Patrimonial');
		$("#TXT_NRO_DNI_RESP").focus();
		
	}else if(TXT_NOMB_RESP == ''){
		alert('Ingrese nombre del Responsable Control Patrimonial');
		$("#TXT_NOMB_RESP").focus();
		
	}else if(TXT_APEPAT_RESP == ''){	
		alert('Ingrese apellido paterno del Responsable Control Patrimonial');
		$("#TXT_APEPAT_RESP").focus();
	
	}else if(TXT_APEMAT_RESP == ''){		
		alert('Ingrese apellido materno del Responsable Control Patrimonial');
		$("#TXT_APEMAT_RESP").focus();
		
	}else if(cbo_local_Responsable == '0'){		
		alert('Seleccione Local donde se encuentra ubicado el Responsable Control Patrimonial');
		$("#cbo_local_Responsable").focus();
		
	}else if(TXT_NOM_AREA_RESP == ''){		
		alert('Ingrese Area donde se encuentra ubicado el Responsable Control Patrimonial');
		$("#TXT_NOM_AREA_RESP").focus();
		
	// }else if(TXT_NOM_OFICINA_RESP == ''){		
	// 	alert('Ingrese Oficina donde se encuentra ubicado el Responsable Control Patrimonial');
	// 	$("#TXT_NOM_OFICINA_RESP").focus();
		
	}else{
	
	
		if(confirm('Confirma guardar estos datos?')){
			
			carga_loading('div_formulario');
			
			
			$.post("Model/M_simi_m_importar_excel_muebles.php?operacion=Guardar_Datos_Responsable",{ 
				TXH_SIMI_COD_USUARIO:TXH_SIMI_COD_USUARIO,
				TXH_COD_ENTIDAD_RESP:TXH_COD_ENTIDAD_RESP,
				TXH_COD_IMPORT_EXCEL_RESP:TXH_COD_IMPORT_EXCEL_RESP,
				TXT_NRO_DNI_RESP:TXT_NRO_DNI_RESP,
				TXT_NOMB_RESP:TXT_NOMB_RESP,
				TXT_APEPAT_RESP:TXT_APEPAT_RESP,
				TXT_APEMAT_RESP:TXT_APEMAT_RESP,
				cbo_local_Responsable:cbo_local_Responsable,
				TXT_NOM_AREA_RESP:TXT_NOM_AREA_RESP,
				TXT_NOM_OFICINA_RESP:TXT_NOM_OFICINA_RESP
			},function(response){
				
				var valor 				= response.split("[*]")[0];
				var codigo 				= response.split("[*]")[1];
					
				if(valor == 1){				
					$("#TXH_COD_IMPORT_EXCEL_RESP").val(codigo);
					alert('Se guardo correctamente los datos del responsable de control patrimonial.');
					$("#div_formulario").html("&nbsp;");
					Listar_Responsable_Muebles();
				}else{
					alert(response);
				}
			})
			
		}
		
	}
	
}


function Listar_Responsable_Muebles(){
	
	var TXH_SIMI_COD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();
	
	carga_loading('div_lista_Responsable');
	
	$.post("View/simi_web_importar_add_resp/simi_web_importar_add_resp_principal_list.php?opt=listar",{ 
		TXH_SIMI_COD_ENTIDAD:TXH_SIMI_COD_ENTIDAD
	},function(data){
		$("#div_lista_Responsable").html(data);		
	})
}


function Enviar_Datos_Responsable_a_SBN(sCodRespMuebles){

	if(confirm('Confirma enviar los datos del responsable de control patrimonial a la SBN?')){
				
		carga_loading('div_formulario');
		
		$.post("Model/M_simi_m_importar_excel_muebles.php?operacion=Enviar_Datos_Responsable_a_SBN",{ 
			sCodRespMuebles:sCodRespMuebles
		},function(data){
			
			if(data==1){
				$("#div_formulario").html("&nbsp;");
				Listar_Responsable_Muebles();
			}else{
				alert(data);
			}
		})
	}
}








</script>



<table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td style="width:20px" valign="top">&nbsp;</td>
    <td style="width:860px" valign="top">
    
    <table width="840" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
      <tr>
        <td width="665"  class="Titulo_plomo_25px">Registrar responsable de control patrimonial</td>
        <td width="170"  class="Titulo_plomo_25px"><input name="button" type="button" class="btn btn-primary" id="button" value="Agregar Responsable" onclick="fShowFormulario_Agregar_Responsable('')" /></td>
      </tr>
      <tr>
        <td colspan="2"  class="Titulo_02_17px"><hr /></td>
      </tr>
      </table>
    <div id="div_lista_Responsable"><? include_once('simi_web_importar_add_resp_principal_list.php')?></div>
    
    </td>
    <td valign="top"><div id="div_formulario" >&nbsp;</div> 
    </td>
  </tr>
</table>

<div id="Div_Popup_Procesa"><div id="div_ejecuta_proceso"></div></div>