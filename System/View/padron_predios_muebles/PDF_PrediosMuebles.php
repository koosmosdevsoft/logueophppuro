<?php

class PDF_PrediosMuebles extends FPDF{
	
  public function Header(){

        // Logo
        $this->Image('../../../webimages/iconos/sbn_11.png',10,5,18);//ancho,alto

        //$this->MultiCell(42, 5, 'Superintendencia Nacional de Bienes Estatales', 0, 1);
        $this->SetFont('Arial','',9);

        $this->Ln(0);
        $this->Cell(19);
        $this->Cell(1,1,'Superintendencia Nacional',0,0,'L');

        $this->Cell(490,1,utf8_decode('Página : ').$this->PageNo().'/{nb}',0,0,'C');
        $this->Ln(4);
        $this->Cell(19);
        $this->Cell(1,1,'de Bienes Estatales',0,0,'L');
        $this->Cell(475,1,utf8_decode('Reporte : '.date('d/m/Y')),0,0,'C');

        //$this->MultiCell(42, 5, 'Superintendencia Nacional de Bienes Estatales', 0, 1);

        $this->Cell(150);


        //$this->Ln(-1);
        $this->SetFont('Arial','B',15);
        $this->Ln(-5);
        $this->Cell(0,1,utf8_decode('Reporte de Predios y/o Locales'),0,0,'C');

        $this->Ln(6);
        $this->SetFont('Arial','',10);
        $this->Cell(0,1,'de Bienes Muebles',0,0,'C');


        //Dibujamos una linea
        $this->Line(10, 20, 285, 20);//Line(10, alto, 196, alto)
        // Salto de línea
        $this->Ln(5);
  }
  
  	// Pie de página
	function Footer(){
    	
 
 		// Posición: a 1,5 cm del final
		$this->SetY(-30);
		$this->SetFont('Arial','',8);
 
 		$this->Ln();
		$this->Ln(10);
		
		$this->Cell(50);
		$this->Cell(1,1,'___________________________________________',0,0,'L');
		
		$this->Cell(105);
		$this->Cell(1,1,'___________________________________________',0,0,'L');
			
		$this->Ln();
		$this->Ln(5);		
		
		$this->Cell(55);
		$this->Cell(1,1,'Firma y Sello Responsable Control Patrimonial',0,0,'L');
		
		$this->Cell(112);
		$this->Cell(1,1,'Firma y Sello del Jefe Inmediato',0,0,'L');
		
		
		$this->Cell(0,10,utf8_decode('Página : ').$this->PageNo().'/{nb}',0,0,'R');
		/*
		// Posición: a 1,5 cm del final
	    $this->SetY(-15);
    	// Arial italic 8
	    $this->SetFont('Arial','I',8);
    	// Número de página
	    $this->Cell(0,10,'Control Patrimonial - Page '.$this->PageNo().'/{nb}',0,0,'R');
		*/
		
	}


  public function FancyTable($header, $data){
    $this->SetFillColor(255,0,0);
    $this->SetTextColor(255);
    $this->SetDrawColor(128,0,0);
    $this->SetLineWidth(.3);
    $this->SetFont('','B');
    // Cabecera
    $w = array(40, 35, 45, 40);
    for($i=0;$i<count($header);$i++)
      $this->Cell($w[$i],7,$header[$i],1,0,'C',true);
    $this->Ln();
    // Restauración de colores y fuentes
    $this->SetFillColor(224,235,255);
    $this->SetTextColor(0);
    $this->SetFont('');
    // Datos
    $fill = false;
    foreach($data as $row){
      $this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
      $this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
      $this->Cell($w[2],6,number_format($row[2]),'LR',0,'R',$fill);
      $this->Cell($w[3],6,number_format($row[3]),'LR',0,'R',$fill);
      $this->Ln();
      $fill = !$fill;
    }
    // Línea de cierre
    $this->Cell(array_sum($w),0,'','T');
  }

}
