<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<?php 
$DatosPredLocal = $data['DatPredioLocal'];
$ArrayPartidaRegistral = $data['DataPartidaRegistral'];
$ArrayPlanoPerimetrico = $data['DataPlanoPerimetrico'];
$ArrayPlanoUbicacion = $data['DataPlanoUbicacion'];
$ArrayPlanoHabilitacion = $data['DataPlanoHabilitacion'];
$ArrayFotografias = $data['DataFotografias'];
$ArrayEscrituraPublica= $data['DataEscrituraPublica'];
$ArrayMemoriaDescriptiva= $data['DataMemoriaDescriptiva'];
$ArrayTituloPropiedad= $data['DataTituloPropiedad'];
$ArrayInformeTecnicoLegal= $data['DataInformeTecnicoLegal'];
$ArrayDispositivoLegal= $data['DataDispositivoLegal'];
$ArrayPlanoPerimetricoAutoCAD= $data['DataPlanoPerimetricoAutoCAD'];
//$this->dump($ArrayFotografias);
?>
<table width="681" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td class="texto_arial_azul_n_12">1.- Partida Registral    </td>
  </tr>
  <tr>
    <td style="padding: 12px 0px 12px 12px;">
    
    <table width="645" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
      <tr>
        <td height="20" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Item</td>
        <td width="135" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Tipo de Documento</td>
        <td width="129" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Fecha<br />
          Registro</td>
        <td width="108" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Partida Registral</td>
        <td width="93" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descargar<br />
          Archivo PDF</td>
        <td width="77" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Estado</td>
        </tr>

<?php if($ArrayPartidaRegistral) foreach ($ArrayPartidaRegistral as $ListPartidaRegistral): ?>

      <tr>
        <td width="45" style="text-align:center" class="TABLE_border4"><?php print $ListPartidaRegistral['DOC_ITEM'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPartidaRegistral['DOC_NOM_TIP_DOC'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPartidaRegistral['DOC_FECHA_REGISTRO'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPartidaRegistral['DOC_NRO_PART_REG'] ?></td>
        <td height="25" style="text-align:center" class="TABLE_border4">   
        <?php if($ListPartidaRegistral['DOC_NOM_ARCHIVO'] != ''){ ?>
        <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListPartidaRegistral['DOC_NOM_CARPETA']?>/<?php print $ListPartidaRegistral['DOC_NOM_ARCHIVO'] ?>" target="_blank" class="Titulo_01_11px"><img src="../webimages/iconos/pdf.png" width="24" height="24" border="0" /></a>        
        <?php }?>     
        </td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php print $ListPartidaRegistral['DOC_DESC_ESTADO'] ?></td>
        </tr>
        
<?php endforeach; ?>

    </table>
    
    
    </td>
  </tr>
  <tr>
    <td class="texto_arial_azul_n_12">&nbsp;</td>
  </tr>
  <tr>
    <td class="texto_arial_azul_n_12">2.- Plano Perimétrico </td>
  </tr>
  <tr>
    <td style="padding: 12px 0px 12px 12px;">
    
    <table width="645" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
      <tr>
        <td height="20" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Item</td>
        <td width="134" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Tipo de Documento</td>
        <td width="73" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Fecha<br />
          Registro</td>
        <td width="128" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Número / Descripción</td>
        <td width="82" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Descargar<br />
          Archivo PDF</td>
        <td width="76" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Descargar<br />
          Archivo CAD</td>
        <td width="77" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Estado</td>
      </tr>
      
<?php if($ArrayPlanoPerimetrico) foreach ($ArrayPlanoPerimetrico as $ListPlanoPerimetrico): ?>
      
      <tr>
        <td width="36" style="text-align:center" class="TABLE_border4"><?php print $ListPlanoPerimetrico['DOC_ITEM'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPlanoPerimetrico['DOC_NOM_TIP_DOC'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPlanoPerimetrico['DOC_FECHA_REGISTRO'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPlanoPerimetrico['DOC_DESCRIP_ARCHIVO'] ?></td>
        <td style="text-align:center" class="TABLE_border4">
        <?php if($ListPlanoPerimetrico['DOC_NOM_ARCHIVO'] != ''){ ?>
        <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListPlanoPerimetrico['DOC_NOM_CARPETA']?>/<?php print $ListPlanoPerimetrico['DOC_NOM_ARCHIVO'] ?>" target="_blank" class="Titulo_01_11px">
        <img src="../webimages/iconos/pdf.png" width="24" height="24" border="0" /></a>
        <? }?>
        </td>
        <td height="25" style="text-align:center" class="TABLE_border4">
        
        <?php if($ListPlanoPerimetrico['DOC_NOM_ARCHIVO_CAD'] != ''){ ?>
        <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListPlanoPerimetrico['DOC_NOM_CARPETA']?>/<?php print $ListPlanoPerimetrico['DOC_NOM_ARCHIVO_CAD'] ?>" target="_blank" class="Titulo_01_11px">
        <img src="../webimages/iconos/autocad.png" width="23" height="29" border="0" /></a>
        <? }?>
        
        </td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php print $ListPlanoPerimetrico['DOC_DESC_ESTADO'] ?></td>
      </tr>
      
<?php endforeach; ?>

    </table>
    
    
    </td>
  </tr>
  <tr>
    <td width="657">&nbsp;</td>
  </tr>
  <tr>
    <td class="texto_arial_azul_n_12">3.- Plano de Ubicación </td>
  </tr>
  <tr>
    <td style="padding: 12px 0px 12px 12px;">
    
    <table width="645" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
      <tr>
        <td height="20" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Item</td>
        <td width="136" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Tipo de Documento</td>
        <td width="73" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Fecha<br />
          Registro</td>
        <td width="127" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Número / Descripción</td>
        <td width="80" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Descargar<br />
          Archivo PDF</td>
        <td width="79" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Descargar<br />
          Archivo CAD</td>
        <td width="77" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Estado</td>
      </tr>
      
<?php if($ArrayPlanoUbicacion) foreach ($ArrayPlanoUbicacion as $ListPlanoUbicacion): ?>
      
      <tr>
        <td width="35" style="text-align:center" class="TABLE_border4"><?php print $ListPlanoUbicacion['DOC_ITEM'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPlanoUbicacion['DOC_NOM_TIP_DOC'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPlanoUbicacion['DOC_FECHA_REGISTRO'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPlanoUbicacion['DOC_DESCRIP_ARCHIVO'] ?></td>
        <td style="text-align:center" class="TABLE_border4">
        <?php if($ListPlanoUbicacion['DOC_NOM_ARCHIVO'] != ''){ ?>
        <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListPlanoUbicacion['DOC_NOM_CARPETA']?>/<?php print $ListPlanoUbicacion['DOC_NOM_ARCHIVO'] ?>" target="_blank" class="Titulo_01_11px">
        <img src="../webimages/iconos/pdf.png" width="24" height="24" border="0" /></a>
        <? }?>
        </td>
        <td height="25" style="text-align:center" class="TABLE_border4">
        
        <?php if($ListPlanoUbicacion['DOC_NOM_ARCHIVO_CAD'] != ''){ ?>
        <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListPlanoUbicacion['DOC_NOM_CARPETA']?>/<?php print $ListPlanoUbicacion['DOC_NOM_ARCHIVO_CAD'] ?>" target="_blank" class="Titulo_01_11px">
        <img src="../webimages/iconos/autocad.png" width="23" height="29" border="0" /></a>
        <? }?>
        
        </td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php print $ListPlanoUbicacion['DOC_DESC_ESTADO'] ?></td>
      </tr>
      
<?php endforeach; ?>

    </table>
    
    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto_arial_azul_n_12">4.- Plano de Habilitación </td>
  </tr>
  <tr>
    <td style="padding: 12px 0px 12px 12px;">
    
    <table width="645" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
      <tr>
        <td height="20" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Item</td>
        <td width="136" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Tipo de Documento</td>
        <td width="73" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Fecha<br />
          Registro</td>
        <td width="127" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Número / Descripción</td>
        <td width="80" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Descargar<br />
          Archivo PDF</td>
        <td width="79" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Descargar<br />
          Archivo CAD</td>
        <td width="77" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Estado</td>
      </tr>
      
<?php if($ArrayPlanoHabilitacion) foreach ($ArrayPlanoHabilitacion as $ListPlanoHabilitacion): ?>
      
      <tr>
        <td width="35" style="text-align:center" class="TABLE_border4"><?php print $ListPlanoHabilitacion['DOC_ITEM'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPlanoHabilitacion['DOC_NOM_TIP_DOC'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPlanoHabilitacion['DOC_FECHA_REGISTRO'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPlanoHabilitacion['DOC_DESCRIP_ARCHIVO'] ?></td>
        <td style="text-align:center" class="TABLE_border4">
        <?php if($ListPlanoHabilitacion['DOC_NOM_ARCHIVO'] != ''){ ?>
        <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListPlanoHabilitacion['DOC_NOM_CARPETA']?>/<?php print $ListPlanoHabilitacion['DOC_NOM_ARCHIVO'] ?>" target="_blank" class="Titulo_01_11px">
        <img src="../webimages/iconos/pdf.png" width="24" height="24" border="0" /></a>
        <? }?>
        </td>
        <td height="25" style="text-align:center" class="TABLE_border4">
        
        <?php if($ListPlanoHabilitacion['DOC_NOM_ARCHIVO_CAD'] != ''){ ?>
        <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListPlanoHabilitacion['DOC_NOM_CARPETA']?>/<?php print $ListPlanoHabilitacion['DOC_NOM_ARCHIVO_CAD'] ?>" target="_blank" class="Titulo_01_11px">
        <img src="../webimages/iconos/autocad.png" width="23" height="29" border="0" /></a>
        <? }?>
        
        </td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php print $ListPlanoHabilitacion['DOC_DESC_ESTADO'] ?></td>
      </tr>
      
<?php endforeach; ?>

    </table>
    
    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto_arial_azul_n_12">5.- Fotografias </td>
  </tr>
  <tr>
    <td style="padding: 12px 0px 12px 12px;">
    
    <table width="645" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
      <tr>
        <td height="20" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Item</td>
        <td width="135" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Tipo de Documento</td>
        <td width="129" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Fecha<br />
          Registro</td>
        <td width="108" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descripción</td>
        <td width="93" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descargar<br />
          Imagen</td>
        <td width="77" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Estado</td>
        </tr>

<?php if($ArrayFotografias) foreach ($ArrayFotografias as $ListFotografias): ?>

      <tr>
        <td width="45" style="text-align:center" class="TABLE_border4"><?php print $ListFotografias['DOC_ITEM'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListFotografias['DOC_NOM_TIP_DOC'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListFotografias['DOC_FECHA_REGISTRO'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListFotografias['DOC_DESCRIP_ARCHIVO'] ?></td>
        <td height="25" style="text-align:center" class="TABLE_border4">   
        <?php if($ListFotografias['DOC_NOM_ARCHIVO'] != ''){ ?>
        <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListFotografias['DOC_NOM_CARPETA']?>/<?php print $ListFotografias['DOC_NOM_ARCHIVO'] ?>" target="_blank" class="Titulo_01_11px"><img src="../webimages/iconos/jpg-picture.png" width="26" height="30" border="0" /></a>        
        <?php }?>     
        </td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php print $ListFotografias['DOC_DESC_ESTADO'] ?></td>
        </tr>
        
<?php endforeach; ?>

    </table>
    
    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto_arial_azul_n_12">6.- Escritura Pública </td>
  </tr>
  <tr>
    <td style="padding: 12px 0px 12px 12px;">
    
    <table width="645" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
      <tr>
        <td height="20" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Item</td>
        <td width="135" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Tipo de Documento</td>
        <td width="129" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Fecha<br />
          Registro</td>
        <td width="108" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descripción</td>
        <td width="93" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descargar<br />
          PDF</td>
        <td width="77" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Estado</td>
        </tr>

<?php if($ArrayEscrituraPublica) foreach ($ArrayEscrituraPublica as $ListEscrituraPublica): ?>

      <tr>
        <td width="45" style="text-align:center" class="TABLE_border4"><?php print $ListEscrituraPublica['DOC_ITEM'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListEscrituraPublica['DOC_NOM_TIP_DOC'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListEscrituraPublica['DOC_FECHA_REGISTRO'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListEscrituraPublica['DOC_DESCRIP_ARCHIVO'] ?></td>
        <td height="25" style="text-align:center" class="TABLE_border4">   
        <?php if($ListEscrituraPublica['DOC_NOM_ARCHIVO'] != ''){ ?>
        <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListEscrituraPublica['DOC_NOM_CARPETA']?>/<?php print $ListEscrituraPublica['DOC_NOM_ARCHIVO'] ?>" target="_blank" class="Titulo_01_11px"><img src="../webimages/iconos/pdf.png" width="24" height="24" border="0" /></a>        
        <?php }?>     
        </td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php print $ListEscrituraPublica['DOC_DESC_ESTADO'] ?></td>
        </tr>
        
<?php endforeach; ?>

    </table>
    
    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto_arial_azul_n_12">7.- Memoria Descriptiva </td>
  </tr>
  <tr>
    <td style="padding: 12px 0px 12px 12px;">
    
    <table width="645" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
      <tr>
        <td height="20" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Item</td>
        <td width="135" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Tipo de Documento</td>
        <td width="129" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Fecha<br />
          Registro</td>
        <td width="108" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descripción</td>
        <td width="93" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descargar<br />
          PDF</td>
        <td width="77" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Estado</td>
        </tr>

<?php if($ArrayMemoriaDescriptiva) foreach ($ArrayMemoriaDescriptiva as $ListMemoriaDescriptiva): ?>

      <tr>
        <td width="45" style="text-align:center" class="TABLE_border4"><?php print $ListMemoriaDescriptiva['DOC_ITEM'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListMemoriaDescriptiva['DOC_NOM_TIP_DOC'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListMemoriaDescriptiva['DOC_FECHA_REGISTRO'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListMemoriaDescriptiva['DOC_DESCRIP_ARCHIVO'] ?></td>
        <td height="25" style="text-align:center" class="TABLE_border4">   
        <?php if($ListMemoriaDescriptiva['DOC_NOM_ARCHIVO'] != ''){ ?>
        <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListMemoriaDescriptiva['DOC_NOM_CARPETA']?>/<?php print $ListMemoriaDescriptiva['DOC_NOM_ARCHIVO'] ?>" target="_blank" class="Titulo_01_11px"><img src="../webimages/iconos/pdf.png" width="24" height="24" border="0" /></a>        
        <?php }?>     
        </td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php print $ListMemoriaDescriptiva['DOC_DESC_ESTADO'] ?></td>
        </tr>
        
<?php endforeach; ?>

    </table>
    
    
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto_arial_azul_n_12">8.- Titulo de Propiedad </td>
  </tr>
  <tr>
    <td style="padding: 12px 0px 12px 12px;"><table width="645" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
      <tr>
        <td height="20" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Item</td>
        <td width="135" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Tipo de Documento</td>
        <td width="129" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Fecha<br />
          Registro</td>
        <td width="108" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descripción</td>
        <td width="93" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descargar<br />
          PDF</td>
        <td width="77" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Estado</td>
      </tr>
      <?php if($ArrayTituloPropiedad) foreach ($ArrayTituloPropiedad as $ListTituloPropiedad): ?>
      <tr>
        <td width="45" style="text-align:center" class="TABLE_border4"><?php print $ListTituloPropiedad['DOC_ITEM'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListTituloPropiedad['DOC_NOM_TIP_DOC'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListTituloPropiedad['DOC_FECHA_REGISTRO'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListTituloPropiedad['DOC_DESCRIP_ARCHIVO'] ?></td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php if($ListTituloPropiedad['DOC_NOM_ARCHIVO'] != ''){ ?>
          <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListTituloPropiedad['DOC_NOM_CARPETA']?>/<?php print $ListTituloPropiedad['DOC_NOM_ARCHIVO'] ?>" target="_blank" class="Titulo_01_11px"><img src="../webimages/iconos/pdf.png" width="24" height="24" border="0" /></a>
          <?php }?></td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php print $ListTituloPropiedad['DOC_DESC_ESTADO'] ?></td>
      </tr>
      <?php endforeach; ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto_arial_azul_n_12">9.- Informe Técnico Legal </td>
  </tr>
  <tr>
    <td style="padding: 12px 0px 12px 12px;"><table width="645" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
      <tr>
        <td height="20" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Item</td>
        <td width="135" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Tipo de Documento</td>
        <td width="129" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Fecha<br />
          Registro</td>
        <td width="108" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descripción</td>
        <td width="93" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descargar<br />
          PDF</td>
        <td width="77" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Estado</td>
      </tr>
      <?php if($ArrayInformeTecnicoLegal) foreach ($ArrayInformeTecnicoLegal as $ListInformeTecnicoLegal): ?>
      <tr>
        <td width="45" style="text-align:center" class="TABLE_border4"><?php print $ListInformeTecnicoLegal['DOC_ITEM'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListInformeTecnicoLegal['DOC_NOM_TIP_DOC'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListInformeTecnicoLegal['DOC_FECHA_REGISTRO'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListInformeTecnicoLegal['DOC_DESCRIP_ARCHIVO'] ?></td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php if($ListInformeTecnicoLegal['DOC_NOM_ARCHIVO'] != ''){ ?>
          <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListInformeTecnicoLegal['DOC_NOM_CARPETA']?>/<?php print $ListInformeTecnicoLegal['DOC_NOM_ARCHIVO'] ?>" target="_blank" class="Titulo_01_11px"><img src="../webimages/iconos/pdf.png" width="24" height="24" border="0" /></a>
          <?php }?></td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php print $ListInformeTecnicoLegal['DOC_DESC_ESTADO'] ?></td>
      </tr>
      <?php endforeach; ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto_arial_azul_n_12">10.- Informe Dispositivo Legal </td>
  </tr>
  <tr>
    <td style="padding: 12px 0px 12px 12px;"><table width="645" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
      <tr>
        <td height="20" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Item</td>
        <td width="90" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Tipo <br />
          Documento</td>
        <td width="74" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Fecha<br />
          Registro</td>
        <td width="110" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Tipo Dispositivo<br />
          Legal</td>
        <td width="129" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descripción</td>
        <td width="62" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descargar<br />
          PDF</td>
        <td width="77" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Estado</td>
      </tr>
      <?php if($ArrayDispositivoLegal) foreach ($ArrayDispositivoLegal as $ListDispositivoLegal): ?>
      <tr>
        <td width="45" style="text-align:center" class="TABLE_border4"><?php print $ListDispositivoLegal['DOC_ITEM'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListDispositivoLegal['DOC_NOM_TIP_DOC'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListDispositivoLegal['DOC_FECHA_REGISTRO'] ?></td>
        <td class="TABLE_border4" style="text-align:center"><?php print $ListDispositivoLegal['DOC_DES_TIP_RESOL_DL'] ?></td>
        <td class="TABLE_border4" style="text-align:center"><?php print $ListDispositivoLegal['DOC_DESCRIP_ARCHIVO'] ?></td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php if($ListDispositivoLegal['DOC_NOM_ARCHIVO'] != ''){ ?>
          <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListDispositivoLegal['DOC_NOM_CARPETA']?>/<?php print $ListDispositivoLegal['DOC_NOM_ARCHIVO'] ?>" target="_blank" class="Titulo_01_11px"><img src="../webimages/iconos/pdf.png" width="24" height="24" border="0" /></a>
          <?php }?></td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php print $ListDispositivoLegal['DOC_DESC_ESTADO'] ?></td>
      </tr>
      <?php endforeach; ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td class="texto_arial_azul_n_12">11.- Plano Perimétrico - AutoCAD DXF </td>
  </tr>
  <tr>
    <td style="padding: 12px 0px 12px 12px;"><table width="645" border="0" cellpadding="0" cellspacing="0"  bgcolor="#FFFFFF">
      <tr>
        <td height="31" bgcolor="#E1E1E1" style="text-align:center" class="TABLE_border4">Item</td>
        <td width="172" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Tipo <br />
          Documento</td>
        <td width="93" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Fecha<br />
          Registro</td>
        <td width="160" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Sistema de <br />
          Coordenadas</td>
        <td width="63" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Descargar<br />
          DXF</td>
        <td width="77" bgcolor="#E1E1E1" class="TABLE_border4" style="text-align:center">Estado</td>
      </tr>
      <?php if($ArrayPlanoPerimetricoAutoCAD) foreach ($ArrayPlanoPerimetricoAutoCAD as $ListPlanoPerimetricoAutoCAD): ?>
      <tr>
        <td width="39" style="text-align:center" class="TABLE_border4"><?php print $ListPlanoPerimetricoAutoCAD['DOC_ITEM'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPlanoPerimetricoAutoCAD['DOC_NOM_TIP_DOC'] ?></td>
        <td style="text-align:center" class="TABLE_border4"><?php print $ListPlanoPerimetricoAutoCAD['DOC_FECHA_REGISTRO'] ?></td>
        <td class="TABLE_border4" style="text-align:center"><?php print $ListPlanoPerimetricoAutoCAD['DOC_COD_SISTEMA_COORDENADA'] ?></td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php if($ListPlanoPerimetricoAutoCAD['DOC_NOM_ARCHIVO'] != ''){ ?>
          <a href="../../Repositorio/predios_docs_tecnicos/<?php print $ListPlanoPerimetricoAutoCAD['DOC_NOM_CARPETA']?>/<?php print $ListPlanoPerimetricoAutoCAD['DOC_NOM_ARCHIVO'] ?>" target="_blank" class="Titulo_01_11px"><img src="../webimages/iconos/imagen_dwg02.png" width="22" height="24" border="0" /></a>
          <?php }?></td>
        <td height="25" style="text-align:center" class="TABLE_border4"><?php print $ListPlanoPerimetricoAutoCAD['DOC_DESC_ESTADO'] ?></td>
      </tr>
      <?php endforeach; ?>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>
