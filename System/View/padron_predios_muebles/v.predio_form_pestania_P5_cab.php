<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 

<?php
$DatosE_UnidInmobil = $data['Dat_E_UnidInmobil'];
//$this->dump($DatosE_Valorizacion);
?>
<table width="641" border="0" cellpadding="0" cellspacing="2" style="background-color:#f4f4f4" class="TABLE_border4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
      <td class="texto_arial_plomito_11_N">&nbsp;</td>
      <td colspan="3">&nbsp;</td>
  </tr>
  <tr id="tr_partida_registral_x1">
    <td width="17" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="212" class="texto_arial_plomito_11_N">Nombre de la unidad del inmueble</td>
    <td colspan="3"><input name="TXT_NOM_UNID_INMOBIL" type="text" id="TXT_NOM_UNID_INMOBIL" style="width:380px" value="<?php print $DatosE_UnidInmobil['I_NOM_UNID_INMOBIL'] ?>" /></td>
  </tr>
  <tr id="tr_descripcion_foto">
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td height="25" class="texto_arial_plomito_11_N">Área de la unidad del inmueble</td>
    <td width="142"><input name="TXT_AREA_UNID_INMOBIL" type="text" id="TXT_AREA_UNID_INMOBIL" style="width:120px" value="<?php print $DatosE_UnidInmobil['I_AREA_UNID_INMOBIL'] ?>" /></td>
    <td width="126"><span class="texto_arial_plomito_11_N">Valor de autovalúo</span></td>
    <td width="132"><input name="TXT_AUTOVALUO_UNID_INMOBIL" type="text" id="TXT_AUTOVALUO_UNID_INMOBIL" style="width:110px" value="<?php print $DatosE_UnidInmobil['I_VALOR_AUTOVALUO'] ?>" /></td>
  </tr>
  <tr id="tr_descripcion_gral">
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td height="25" valign="top" class="texto_arial_plomito_11_N">Observaciones
    <input name="TXT_COD_UNID_INMOBIL" type="hidden" id="TXT_COD_UNID_INMOBIL" style="width:120px" value="<?php print $DatosE_UnidInmobil['I_COD_UNID_INMOBIL'] ?>" /></td>
    <td colspan="3"><textarea name="TXT_OBS_UNID_INMOBIL" rows="2" id="TXT_OBS_UNID_INMOBIL" style="width:380px"><?php print $DatosE_UnidInmobil['I_OBS_UNID_INMOBIL'] ?></textarea></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="4" class="texto_arial_plomito_11_N"><hr class="hr_01"/></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="4" valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
  </tr>
</table>
