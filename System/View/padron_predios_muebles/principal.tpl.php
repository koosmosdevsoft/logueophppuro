<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../utils/css/stylesPaginacion.css" />
<br />
<table width="100%" border="0" cellspacing="3" cellpadding="0">
    <tr>
      <td colspan="2" valign="top"><table width="637" border="0" cellspacing="3" cellpadding="0" class="TABLE_border4" >
        <tr>
          <td width="80">&nbsp;</td>
          <td width="546">&nbsp;</td>
        </tr>
        <tr>
          <td style="text-align:center"><img src="../webimages/iconos/aviso.png" width="50" height="44" /></td>
          <td><span class="texto_arial_rojo_n_15">(*) Si desea registrar o modificar información de predios y/o locales, deberá ser ingresado a través del  Módulo Inmuebles.<br />
            Coordinar con el Responsable de Bienes Inmuebles. </span></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td width="636" valign="top">


<table width="99%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td >&nbsp;</td>
  </tr>
  <tr>
    <td width="82%" >
      <div class="Titulo_02_19px">
        <img src="../webimages/iconos/btn_admin_55.png" width="58" height="58" align="absmiddle" /> Consulta de Predios y/o Locales</div></td>
  </tr>
  <tr>
    <td ><hr /></td>
    </tr>
  <tr>
    <td >&nbsp;</td>
  </tr>
  </table>
<div id="div_tabs" >
  <ul>

    <li><a href="#tabs-1">Busqueda de Predios y/ o Local</a></li>


  </ul>

  <div id="tabs-1">
    <table width="100%" border="0" cellpadding="0" cellspacing="2" bgcolor="#f4f4f4" class="table-container TABLE_border4">
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <tr>
    <td width="3%">&nbsp;</td>
    <td width="21%" height="30">Buscar por denominación</td>
    <td width="64%"><input name="txb_busc_nom_predios" type="text" id="txb_busc_nom_predios" style="width:95%"/></td>
    <td width="12%">
  <a href="#" onclick='handle_paginar_predios(1); $("#detail-formulario").html("");'><img src="../webimages/iconos/ver_icono_b.png" width="20" height="22" border="0" /></a> &nbsp;&nbsp;
  <a href="#" onclick="handle_clear_busqueda()">
  <img src="../webimages/iconos/quitar_flitros.png" width="22" height="22" border="0" /></a>
      </td>
  </tr>
  <tr>
    <td colspan="4">&nbsp;</td>
    </tr>
</table>


<div id="detail-busqueda" style="padding:10px 0px 10px 0px"></div>

  </div>
    </div>
      </td>
      <td width="877" valign="top">
<div id="detail-formulario" style="padding:10px 0px 10px 10px;"></div>
      </td>
    </tr>
</table>
<script type="text/javascript" >
    <?php include_once('js/script.js') ?>
</script>