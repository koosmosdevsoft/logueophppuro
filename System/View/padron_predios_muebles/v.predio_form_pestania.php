<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<script>

  $(function() {
    $( "#tabs_Reg_Predio" ).tabs();
  });

</script>
<table width="760" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td width="754">
    <div id="tabs_Reg_Predio">
      <ul>
        <li><a href="#tabsPred-1">Datos Registrales</a></li>
        <li><a href="#tabsPred-2">Datos Técnicos</a></li>
        <li><a href="#tabsPred-3">Valorización</a></li>
        <li><a href="#tabsPred-4">Documentación</a></li>
        <li><a href="#tabsPred-5">Unid. Inmobiliarias del Predio</a></li>
        <li><a href="#tabsPred-6">Observaciones de la SBN</a></li>
        </ul>
      <div id="tabsPred-1"><? include_once('v.predio_form_pestania_P1.php')?></div>
      <div id="tabsPred-2"><? include_once('v.predio_form_pestania_P2.php')?></div>      
      <div id="tabsPred-3"><? include_once('v.predio_form_pestania_P3.php')?></div>
      <div id="tabsPred-4"><? include_once('v.predio_form_pestania_P4.php')?></div>
      <div id="tabsPred-5"><? include_once('v.predio_form_pestania_P5.php')?></div>
      <div id="tabsPred-6"><? include_once('v.predio_form_pestania_P6.php')?></div>
    </div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
</table>