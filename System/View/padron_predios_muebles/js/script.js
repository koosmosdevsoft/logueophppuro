$(function() {
    $("#div_tabs").tabs();
	handle_paginar_predios(1);
    /*
	$("#txt_busc_fecha_asignacion").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true
    });*/
});


function handle_clear_busqueda(){	
  $("#txb_busc_nom_predios").val('');
  handle_paginar_predios(1);
}


function handle_paginar_predios(numeroPagina) {
  
  var TXH_SIMI_COD_ENTIDAD = $("#TXH_SIMI_COD_ENTIDAD").val();
  var txb_busc_nom_predios = $("#txb_busc_nom_predios").val();
  
  carga_loading('detail-busqueda');  
  var url = "View/padron_predios_muebles/principal.php?operacion=Filtrar_Lista_Predios";
  url += (TXH_SIMI_COD_ENTIDAD == "") ? "" : "&TXH_SIMI_COD_ENTIDAD=" + TXH_SIMI_COD_ENTIDAD;
  url += (txb_busc_nom_predios == "") ? "" : "&txb_busc_nom_predios=" + txb_busc_nom_predios;
  url += (numeroPagina == "") ? "1" : "&numeroPagina=" + numeroPagina;
  $.ajax(url).done(function (response) {
    $("#detail-busqueda").html(response);
  });
  
}

function handle_Mostrar_Form_Predio_Local(xID_PREDIO_SBN) {

  carga_loading('detail-formulario');	
  var url = "View/padron_predios_muebles/principal.php?operacion=Mostrar_Form_Datos_Predio";
  url += (xID_PREDIO_SBN == "") ? "" : "&xID_PREDIO_SBN=" + xID_PREDIO_SBN;
  $.ajax(url).done(function (response) {
    $("#detail-formulario").html(response);	
  });
}



function mostrar_provincia(){
	var cbo_depa	= $("#cbo_departamento").val();		
	$.post("View/padron_ubigeo/listbox_provincia.php",{ 
		cbo_depa:cbo_depa
	},function(data){
		$("#cbo_provincia").html(data);
	});
}


function mostrar_distrito(){
	var cbo_depa	= $("#cbo_departamento").val();
	var cbo_prov	= $("#cbo_provincia").val();	
	$.post("View/padron_ubigeo/listbox_distrito.php",{ 
		cbo_depa:cbo_depa,
		cbo_prov:cbo_prov
	},function(data){
		$("#cbo_distrito").html(data);
	})
}


function Mostrar_Datos_Registro_Datos_Tecnicos(wID_PREDIO_SBN){

	var cbo_propiedad 		= $("#cbo_propiedad").val();
	if((cbo_propiedad != '') && (cbo_propiedad == '2' || cbo_propiedad == '3' )){		
		Mostrar_Datos_Registro_Datos_Tecnicos_parametros(wID_PREDIO_SBN);
	}else{
		
		$("#div-detail-area_tipo_propiedad").html('');
	}
}


function Mostrar_Datos_Registro_Datos_Tecnicos_parametros(wID_PREDIO_SBN){
	$.post("View/padron_predios_muebles/v.predio_form_pestania.php",{
	},function(data){
		$("#div-detail-area_tipo_propiedad").html(data);
		Mostrar_tab1_datos_registrales(wID_PREDIO_SBN);
		Mostrar_tab2_datos_tecnicos(wID_PREDIO_SBN);
		Mostrar_tab3_datos_valorizacion(wID_PREDIO_SBN);
	});
}


function Mostrar_tab1_datos_registrales(wID_PREDIO_SBN){
 
  carga_loading('div_tab1_contenido');  
  var url = "View/padron_predios_muebles/principal.php?operacion=Mostrar_Form_tab1_Datos_Registrales";
  url += (wID_PREDIO_SBN == "") ? "" : "&wID_PREDIO_SBN=" + wID_PREDIO_SBN;

  $.ajax(url).done(function (response) {
    $("#div_tab1_contenido").html(response);
  });
}


function Mostrar_tab2_datos_tecnicos(wID_PREDIO_SBN){
 
  carga_loading('div_tab2_contenido');  
  var url = "View/padron_predios_muebles/principal.php?operacion=Mostrar_Form_tab2_Datos_Tecnicos";
  url += (wID_PREDIO_SBN == "") ? "" : "&wID_PREDIO_SBN=" + wID_PREDIO_SBN;

  $.ajax(url).done(function (response) {
    $("#div_tab2_contenido").html(response);
  });
}


function Mostrar_tab3_datos_valorizacion(wID_PREDIO_SBN){
	
  Listar_Documentos_Valorizacion_Cabecera(wID_PREDIO_SBN);   
  Listar_Documentos_Valorizacion(wID_PREDIO_SBN);

}

function mostrar_det_saneamiento(){
	var cbo_tipo_saneamiento	= $("#cbo_estado_saneamiento").val();
	
	$.post("View/padron_predios/cbo_estado_saneamiento_detalle.php",{ 
		cbo_tipo_saneamiento:cbo_tipo_saneamiento
	},function(data){
		$("#cbo_det_saneamiento").html(data);
	})
}

	/**************************************************************/
	/**************************************************************/
	/**************************************************************/
	
	separadorDecimalesInicial="."; //Modifique este dato para poder obtener la nomenclatura que utilizamos en mi pais
	separadorDecimales="."; //Modifique este dato para poder obtener la nomenclatura que utilizamos en mi pais
	separadorMiles=","; //Modifique este dato para poder obtener la nomenclatura que utilizamos en mi pais
	
	function arreglar(numero){ 
		var numeroo=""; 
		numero=""+numero; 
		partes=numero.split(separadorDecimalesInicial);
		entero=partes[0];
		
		if(partes.length>1){ 
			decimal=partes[1]; 
		} 
		
		cifras=entero.length; 
		cifras2=cifras 
		
		for(a=0;a<cifras;a++){
			cifras2-=1;
			numeroo+=entero.charAt(a);
			
			if(cifras2%3==0 &&cifras2!=0){
				numeroo+=separadorMiles;
			}
		} 
		
		if(partes.length>1){
			numeroo+=separadorDecimales+decimal;
		}
		
		return numeroo 
	}

	function formatodecimales(input){
		var num = input.value.replace(/\,/g,'');		
		input.value = arreglar(num);		
		if(!isNaN(num)){
			num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
			num = num.split('').reverse().join('').replace(/^[\,]/,'');
			input.value = num;
		}else{ alert('Solo se permiten numeros');
			input.value = input.value.replace(/[^\d\.]*/g,'');
		}
	}

/*******************************************************/
/*******************************************************/
/*******************************************************/




function mostrar_tipo_moneda(){
	var cbo_tipo_moneda	= $("#cbo_tipo_moneda").val();
	
	if(cbo_tipo_moneda == '-'){
		$("#div_mon_val_terreno").html('-');
		$("#div_mon_val_construccion").html('-');
		$("#div_mon_val_obra").html('-');
		$("#div_mon_val_tot_inm").html('-');
		
	}else if(cbo_tipo_moneda == '1'){	//------soles
		$("#div_mon_val_terreno").html('S/.');
		$("#div_mon_val_construccion").html('S/.');
		$("#div_mon_val_obra").html('S/.');
		$("#div_mon_val_tot_inm").html('S/.');
		
		$("#txt_tipo_cambio").prop('disabled', true).val(0);
		
	}else if(cbo_tipo_moneda == '2'){	//------dolar
		$("#div_mon_val_terreno").html('$.');
		$("#div_mon_val_construccion").html('$.');
		$("#div_mon_val_obra").html('$.');
		$("#div_mon_val_tot_inm").html('$.');
		
		$("#txt_tipo_cambio").prop('disabled', false);
	}
	
	calcular_monto_total();
	
}

function calcular_monto_total(){
	
	var txt_valor_terreno = parseFloat(1*$("#txt_valor_terreno").val());
	var txt_valor_construccion = parseFloat(1*$("#txt_valor_construccion").val());
	var txt_valor_obra = parseFloat(1*$("#txt_valor_obra").val());

	var sumatoria_val_total = parseFloat((txt_valor_terreno+txt_valor_construccion+txt_valor_obra)*1);
	$("#txt_valor_total_inmueble").val(sumatoria_val_total);
	
	var cbo_tipo_moneda = $("#cbo_tipo_moneda").val();

	if(cbo_tipo_moneda == '2'){ // dolar
		var txt_tipo_cambio 		= parseFloat(1*$("#txt_tipo_cambio").val());
		var val_tot_mueble_soles	= parseFloat(sumatoria_val_total*txt_tipo_cambio);		
	}else{
		var txt_tipo_cambio 		= parseFloat(0);
		var val_tot_mueble_soles	= parseFloat(sumatoria_val_total);
	}
	
	$("#txt_valor_total_inmueble_soles").val(val_tot_mueble_soles);
}


function Listar_Documentos_Valorizacion_Cabecera(sID_PREDIO_SBN){
	
	carga_loading('div_cab_valorizacion');
	
	var url = "View/padron_predios_muebles/principal.php?operacion=Listar_Cab_Valorizacion";
	url += (sID_PREDIO_SBN == "") ? "" : "&sID_PREDIO_SBN=" + sID_PREDIO_SBN;
		
	$.ajax(url).done(function (data) {
		$("#div_cab_valorizacion").html(data);		
	});
	
}

function Listar_Documentos_Valorizacion(sID_PREDIO_SBN){
	
	carga_loading('div_list_valorizacion'); 
	 
	var url = "View/padron_predios_muebles/principal.php?operacion=Listar_Det_Valorizacion";
	url += (sID_PREDIO_SBN == "") ? "" : "&sID_PREDIO_SBN=" + sID_PREDIO_SBN;
		
	$.ajax(url).done(function (data) {
		$("#div_list_valorizacion").html(data);		
	});
	
}






function Lista_Registros_Unid_Inmobiliaria(sID_PREDIO_SBN){
	
	carga_loading('div_det_unidinmobiliaria'); 
	
	var url = "View/padron_predios_muebles/principal.php?operacion=Lista_Registros_Unid_Inmobiliaria";
	url += (sID_PREDIO_SBN == "") ? "" : "&sID_PREDIO_SBN=" + sID_PREDIO_SBN;
	$.ajax(url).done(function (data) {
		$("#div_det_unidinmobiliaria").html(data);		
	});
}




/***********************************************************************************************/
/***********************************************************************************************/
/***********************************************************************************************/


function Mostrar_Dialog_Reg_Documentos(sCOD_TIP_DOC_PRED, sCOD_PREDIO_DOC_TECN){
	$("#div_dialog_form_reg_documentos").dialog({
			//dialogClass: "no-close",
			title: 'Registro de la Documentación',
			width: 550,
			height: 500,
			resizable: "false",
			modal: true,
			open: function() {
				Mostrar_Contenido_Dialog(sCOD_TIP_DOC_PRED, sCOD_PREDIO_DOC_TECN);
			},
			close: function() {
				$(this).dialog("destroy");
				document.getElementById('Div_contenido_dialog').innerHTML = '';
			}
	});
}


function Mostrar_Contenido_Dialog(sCOD_TIP_DOC_PRED, sCOD_PREDIO_DOC_TECN){
	
	var txh_COD_UE_LOCAL = $("#txh_COD_UE_LOCAL").val();
	
	var url = "View/padron_predios_muebles/principal.php?operacion=Mostrar_Form_Documento";
	url += (txh_COD_UE_LOCAL == "") ? "" : "&txh_COD_UE_LOCAL=" + txh_COD_UE_LOCAL;
	url += (sCOD_TIP_DOC_PRED == "") ? "" : "&sCOD_TIP_DOC_PRED=" + sCOD_TIP_DOC_PRED;
	url += (sCOD_PREDIO_DOC_TECN == "") ? "" : "&sCOD_PREDIO_DOC_TECN=" + sCOD_PREDIO_DOC_TECN;
		
	$.ajax(url).done(function (data) {
		$("#Div_contenido_dialog").html(data);		
	});
}

function Listar_Registro_Documentacion(sID_PREDIO_SBN){
	
	carga_loading('div_det_reg_documentacion'); 
	var url = "View/padron_predios_muebles/principal.php?operacion=Listar_Registro_Documentos_registrados";
	url += (sID_PREDIO_SBN == "") ? "" : "&sID_PREDIO_SBN=" + sID_PREDIO_SBN;
		
	$.ajax(url).done(function (data) {
		$("#div_det_reg_documentacion").html(data);		
	});
	
}



function Eliminar_REG_INMOBILI(sCOD_UNID_INMOBIL){
	
	var sID_PREDIO_SBN 				= $("#txh_COD_UE_LOCAL").val();
	
	if(confirm('Confirma Eliminar este Registro Inmobiliario')){
			
		var url = "View/padron_predios_muebles/principal.php?operacion=Eliminar_Reg_Inmobiliaria";
		url += (sCOD_UNID_INMOBIL == "") ? "" : "&sCOD_UNID_INMOBIL=" + sCOD_UNID_INMOBIL;
			
		$.ajax(url).done(function (data) {
			if(data == '1'){			
				$("#div_dialog_form_reg_unidinmobiliaria").dialog("destroy");
				document.getElementById('div_cab_unidinmobiliaria').innerHTML = '';
				Lista_Registros_Unid_Inmobiliaria(sID_PREDIO_SBN);
					
			}else{
				alert(data)
			}
		});
	}
	
}

/***********************************************************************************************/
/***********************************************************************************************/
/***********************************************************************************************/


function Mostrar_Form_Reg_Inmobiliario(sCOD_UNID_INMOBIL){
		
	$("#div_dialog_form_reg_unidinmobiliaria").dialog({
			//dialogClass: "no-close",
			title: 'Registro de Unidades Inmobiliarias del Predio',
			width: 670,
			height: 260,
			resizable: "false",
			modal: true,
			open: function() {
				Mostrar_Contenido_Dialog_Reg_Inmobiliario(sCOD_UNID_INMOBIL);
			},
			close: function() {
				$(this).dialog("destroy");
				document.getElementById('div_cab_unidinmobiliaria').innerHTML = '';
			}
	});
}



function Mostrar_Contenido_Dialog_Reg_Inmobiliario(sCOD_UNID_INMOBIL){
	
	carga_loading('div_cab_unidinmobiliaria'); 
	var url = "View/padron_predios_muebles/principal.php?operacion=Ver_Datos_Unidad_Inmobiliaria_x_Codigo";
	url += (sCOD_UNID_INMOBIL == "") ? "" : "&sCOD_UNID_INMOBIL=" + sCOD_UNID_INMOBIL;
		
	$.ajax(url).done(function (data) {
		$("#div_cab_unidinmobiliaria").html(data);		
	});
}




/**************************************************************************************/


function Mostrar_Form_Valorizacion(eCOD_VAL_PREDIO){
		
	$("#div_dialog_form_reg_valorizacion").dialog({
			//dialogClass: "no-close",
			title: 'Registro de Valorizacion del Predio',
			width: 790,
			height: 350,
			resizable: "false",
			modal: true,
			open: function() {
				Mostrar_Contenido_Dialog_Valorizacion(eCOD_VAL_PREDIO);
			},
			close: function() {
				$(this).dialog("destroy");
				document.getElementById('div_cab_valorizacion').innerHTML = '';
			}
	});
}


function Mostrar_Contenido_Dialog_Valorizacion(eCOD_VAL_PREDIO){
	
	carga_loading('div_cab_valorizacion'); 
	var url = "View/padron_predios_muebles/principal.php?operacion=Ver_Datos_Valorizacion_x_Codigo";
	url += (eCOD_VAL_PREDIO == "") ? "" : "&eCOD_VAL_PREDIO=" + eCOD_VAL_PREDIO;
		
	$.ajax(url).done(function (data) {
		$("#div_cab_valorizacion").html(data);		
	});
	
}

function Imprimir_Locales_PDF(){
	var xCE 	= $("#TXH_SIMI_COD_ENTIDAD").val();
	
	var url = "View/padron_predios_muebles/principal.php?operacion=Imprimir_Locales_PDF";
		url += (xCE == "") ? "" : "&xCE=" + xCE;
	window.open(url,'_blank');

}
