<?php session_start();
require_once('../../../utils/funciones/funciones.php');
require_once('../../../utils/fpdf181/fpdf.php');
require_once("../../Controller/BaseBD_PDO.php");
require_once('../../Model/BaseBD_PDOModel.php');
require_once("../../Controller/C_Interconexion_SQL.php");

require_once('../../Controller/BienPatrimonialController.php');
require_once('../../Model/BienPatrimonialModel.php');

require_once('../../Controller/PrediosController.php');
require_once('../../Model/PrediosModel.php');


require_once('../../Controller/UbigeoController.php');
require_once('../../Model/UbigeoModel.php');


require_once('PDF_PrediosMuebles.php');
error_reporting(0);


$oPrediosController	=	new PrediosController; 

if( $_GET['operacion'] == 'Filtrar_Lista_Predios' ){  
  $oPrediosController->Buscar_Predios_x_Parametros();
  
}else if( $_GET['operacion'] == 'Mostrar_Form_Datos_Predio' ){
  $oPrediosController->Mostrar_Datos_Predio_Local();
  
}else if( $_GET['operacion'] == 'Listar_Cab_Valorizacion' ){
   $oPrediosController->Ver_Datos_Valorizacion_x_Codigo();

}else if( $_GET['operacion'] == 'Listar_Det_Valorizacion' ){
   $oPrediosController->Listar_Valorizaciones();

}else if( $_GET['operacion'] == 'Ver_Datos_Valorizacion_x_Codigo' ){
   $oPrediosController->Ver_Datos_Valorizacion_x_Codigo();
   
}else if( $_GET['operacion'] == 'Mostrar_Form_tab1_Datos_Registrales' ){
   $oPrediosController->Mostrar_Form_tab1_Datos_Registrales();
   
}else if( $_GET['operacion'] == 'Mostrar_Form_tab2_Datos_Tecnicos' ){
   $oPrediosController->Mostrar_Form_tab2_Datos_Tecnicos();

}else if( $_GET['operacion'] == 'Lista_Registros_Unid_Inmobiliaria' ){
   $oPrediosController->Lista_Registros_Unid_Inmobiliaria();

}else if( $_GET['operacion'] == 'Ver_Datos_Unidad_Inmobiliaria_x_Codigo' ){
   $oPrediosController->Ver_Datos_Unidad_Inmobiliaria_x_Codigo();

}else if( $_GET['operacion'] == 'Mostrar_Form_Documento' ){
   $oPrediosController->Mostrar_Form_Documento();

}else if( $_GET['operacion'] == 'Listar_Registro_Documentos_registrados' ){
   $oPrediosController->Listar_Registro_Documentos_registrados();

}else if( $_GET['operacion'] == 'Imprimir_Locales_PDF' ){
   $oPrediosController->Imprimir_Locales_PDF();

}else{
  $oPrediosController->index();  
}
?>
