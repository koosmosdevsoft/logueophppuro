<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="../utils/css/stylesPaginacion.css"/>
<?php
$PrediosLocal = $data['Predio_Locales'];
$pager = $data['pager'];
//$this->dump($data['pager']);
//$this->dump($pager);
?>
<div style="text-align:right">
<button type="button" class="btn btn-danger" onclick="Imprimir_Locales_PDF()"> <i class="fa fa-print" aria-hidden="true"></i> Reporte de Predios y/o Locales</button>
</div>
<br />
<table width="593" border="0" cellpadding="0" cellspacing="0" class="TABLE_border4" >
  <tr>
    <th width="36" height="30" bgcolor="#DDE4EC" style="text-align:center">Item</th>
    <th width="277" bgcolor="#DDE4EC" style="text-align:center">Denominación del Predio y/o Local</th>
    <th width="98" bgcolor="#DDE4EC">Tipo de <br />
    Propiedad</th>
    <th width="61" bgcolor="#DDE4EC" style="text-align:center">Estado</th>
    <th width="42" bgcolor="#DDE4EC" style="text-align:center">Ver</th>
  </tr>
  
<?php if($PrediosLocal) foreach ($PrediosLocal as $ListaLocales): ?>

  <tr  onmouseover="mOvr2(this)" onmouseout="mOut2(this)" >
    <td style="text-align:center"><?php print $ListaLocales['ROW_NUMBER_ID']?></td>
    <td style="text-align:left"><?php print $ListaLocales['DENOMINACION_PREDIO'] ?></td>
    <td><?php print $ListaLocales['DESC_TIP_PROPIEDAD'] ?></td>
    <td style="text-align:center"><?php print $ListaLocales['DESC_EST_PROPIEDAD'] ?></td>
    <td style="text-align:center"><a href="#" onclick="handle_Mostrar_Form_Predio_Local('<?php print $ListaLocales['ID_PREDIO_SBN']?>')"><img src="../webimages/iconos/ver_bl.png" width="16" height="18" border="0" /></a></td>
  </tr>
  
<?php endforeach; ?>

<tr  onmouseover="mOvr2(this)" onmouseout="mOut2(this)">
    <td colspan="5" bgcolor="#DDE4EC">
<div id="div_paginacion" >
<?php 
if ($pager['cantidadPaginas'] > 1): ?>
    <div class="pagination">
      <ul>
        <?php if ($pager['indicePagina'] != 1): ?>
        	<li><a href="javascript: handle_paginar_predios(1)" class="paginate">Inicial</a></li>
            <li><a href="javascript: handle_paginar_predios(<?php print $pager['indicePagina'] - 1; ?>)" class="paginate"><<</a></li>
        <?php endif; ?>
        <?php for ($index = $pager['limiteInferior']; $index <= $pager['limiteSuperior']; $index++): ?>
        	<?php if ($index == $pager['indicePagina']): ?>
                <li class="active"><a><?php print $index; ?></a></li>
            <?php else: ?>
            	<li><a href="javascript: handle_paginar_predios(<?php print $index; ?>)" class="paginate"><?php print $index; ?></a></li>
            <?php endif; ?>
		<?php endfor; ?>
        <?php if ($pager['indicePagina'] != $pager['cantidadPaginas']): ?>
        	<li><a href="javascript: handle_paginar_predios(<?php print $pager['indicePagina'] + 1; ?>)" class="paginate">>></a></li>
            <li><a href="javascript: handle_paginar_predios(<?php print $pager['cantidadPaginas']; ?>)" class="paginate">Final</a></li>
        <?php endif; ?>
      </ul>
    </div>
<?php endif; ?>
</div>
</td>
  </tr>
  
</table>