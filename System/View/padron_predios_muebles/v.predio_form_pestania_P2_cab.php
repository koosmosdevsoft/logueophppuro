<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<?
$DatosPredLocal = $data['DatPredioLocal'];
$ArrayTipTerreno = $data['DatTipTerreno'];
$ArrayEstConservacion = $data['DatEstConservacion'];
$ArrayEstSaneamiento = $data['DatEstSaneamiento'];
$ArrayDetalleSaneamiento = $data['DatDetalleSaneamiento'];
$ArrayUsoPredio = $data['DatUsoPredio'];

//$this->dump($DatosPredLocal);
?>
<table width="667" border="0" cellpadding="0" cellspacing="2" style="background-color:#f4f4f4">
      <tr>
        <td width="18">&nbsp;</td>
        <td width="148" class="texto_arial_plomito_11_N">Zonificación</td>
        <td width="182"><span class="texto_02_11">
          <input name="txt_zonficacion" type="text" id="txt_zonficacion" style="width:150px;" value="<?php print $DatosPredLocal['DT_ZONIFICACION']?>"/>
        </span></td>
        <td width="131">&nbsp;</td>
        <td width="174">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Tipo Terreno</td>
        <td><select style="width:150px" name="cbo_tipo_terreno" id="cbo_tipo_terreno" class="form-control">
          <option value="-">[.Seleccione.]</option>
                <?php if($ArrayTipTerreno) foreach ($ArrayTipTerreno as $ListTipTerreno): 
						$select_TipTerreno = ($DatosPredLocal['DT_COD_TIP_TERRENO'] == $ListTipTerreno['COD_TIP_TERRENO']) ? 'selected="selected"' : '';
                ?>
        <option value="<?php print $ListTipTerreno['COD_TIP_TERRENO'] ?>" <?php print $select_TipTerreno ?> ><?php print $ListTipTerreno['NOM_TIP_TERRENO'] ?></option>
                <?php endforeach; ?>
        </select></td>
        <td class="texto_arial_plomito_11_N">Perímetro (ml)</td>
        <td><span class="texto_02_11">
         <?
    $DT_PERIMETRO = ($DatosPredLocal['DT_PERIMETRO'] == '0')? '':$DatosPredLocal['DT_PERIMETRO'];
	?>
          <input name="txt_perimetro" type="text" id="txt_perimetro" style="width:150px;" value="<?php print $DT_PERIMETRO?>" onkeyup="formatodecimales(this)" />
        </span></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Area Terreno (m&sup2;)</td>
        <td><span class="texto_02_11">
         
          <?
    $DT_AREA_TERRENO = ($DatosPredLocal['DT_AREA_TERRENO'] == '0')? '':$DatosPredLocal['DT_AREA_TERRENO'];
	?>
          <input name="txt_area_terreno" type="text" id="txt_area_terreno" style="width:150px;" value="<?php print $DT_AREA_TERRENO?>" onkeyup="formatodecimales(this)" />
        </span></td>
        <td class="texto_arial_plomito_11_N">Area Construida (m&sup2;)</td>
        <td><span class="texto_02_11">
        
        <?
    $DT_AREA_CONSTRUIDA = ($DatosPredLocal['DT_AREA_CONSTRUIDA'] == '0')? '':$DatosPredLocal['DT_AREA_CONSTRUIDA'];
	?>
          <input name="txt_area_construida" type="text" id="txt_area_construida" style="width:150px;" value="<?php print $DT_AREA_CONSTRUIDA?>" onkeyup="formatodecimales(this)" />
        </span></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Estado de Conservación</td>
        <td><select style="width:150px" name="cbo_est_conservacion" id="cbo_est_conservacion" class="form-control">
          <option value="-">[.Seleccione.]</option>          
                <?php if($ArrayEstConservacion) foreach ($ArrayEstConservacion as $ListEstConservacion): 
						$select_EstConserv = ($DatosPredLocal['DT_COD_ECONSERVACION'] == $ListEstConservacion['COD_ECONSERVACION']) ? 'selected="selected"' : '';
                ?>
        <option value="<?php print $ListEstConservacion['COD_ECONSERVACION'] ?>" <?php print $select_EstConserv ?> ><?php print $ListEstConservacion['TXT_CONSERVACION'] ?></option>
                <?php endforeach; ?>
        </select></td>
        <td class="texto_arial_plomito_11_N">Nro de Pisos</td>
        <td><span class="texto_02_11">
               <?
    $DT_NUMERO_PISOS = ($DatosPredLocal['DT_NUMERO_PISOS'] == '0')? '':$DatosPredLocal['DT_NUMERO_PISOS'];
	?>
          <input name="txt_nro_pisos" type="text" id="txt_nro_pisos" style="width:150px;" value="<?php print $DT_NUMERO_PISOS?>"/>
        </span></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="texto_arial_plomito_11_N"> Esta Saneado</td>
        <td><select style="width:150px" name="cbo_estado_saneamiento" id="cbo_estado_saneamiento" class="form-control" onchange="mostrar_det_saneamiento()">
          <option value="-">[.Seleccione.]</option>
                <?php if($ArrayEstSaneamiento) foreach ($ArrayEstSaneamiento as $ListEstSaneamiento): 
						$select_EstSanea = ($DatosPredLocal['DT_COD_ESANEAMIENTO'] == $ListEstSaneamiento['COD_ESANEAMIENTO']) ? 'selected="selected"' : '';
                ?>
        <option value="<?php print $ListEstSaneamiento['COD_ESANEAMIENTO'] ?>" <?php print $select_EstSanea ?> ><?php print $ListEstSaneamiento['TXT_ESANEAMIENTO'] ?></option>
                <?php endforeach; ?>
        </select></td>
        <td class="texto_arial_plomito_11_N">Motivo No  Saneado</td>
        <td><select name="cbo_det_saneamiento" id="cbo_det_saneamiento" class="form-control" style="width:150px">
          <option value="-">[.Seleccione.]</option>
                <?php if($ArrayDetalleSaneamiento) foreach ($ArrayDetalleSaneamiento as $ListDetalleSaneamiento): 
						$select_DetalleSanea = ($DatosPredLocal['DT_CODIGO_DETALLE_ESANEAMIENTO'] == $ListDetalleSaneamiento['codigo_detalle']) ? 'selected="selected"' : '';
                ?>
        <option value="<?php print $ListDetalleSaneamiento['codigo_detalle'] ?>" <?php print $select_DetalleSanea ?> ><?php print $ListDetalleSaneamiento['dsc_detalle'] ?></option>
                <?php endforeach; ?>
        </select></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Uso del predio</td>
        <td><select style="width:150px" name="cbo_uso_predio" id="cbo_uso_predio" class="form-control" >
          <option value="-">[.Seleccione.]</option>ArrayUsoPredio
                <?php if($ArrayUsoPredio) foreach ($ArrayUsoPredio as $ListUsoPredio): 
						$select_UsoPredio = ($DatosPredLocal['DT_CODIGO_USO_GENERICO'] == $ListUsoPredio['CODIGO_USO_GENERICO']) ? 'selected="selected"' : '';
                ?>
        <option value="<?php print $ListUsoPredio['CODIGO_USO_GENERICO'] ?>" <?php print $select_UsoPredio ?> ><?php print $ListUsoPredio['NOMBRE_USO_GENERICO'] ?></option>
                <?php endforeach; ?>
        </select></td>
        <td class="texto_arial_plomito_11_N">Otros Usos</td>
        <td>
          <input name="txt_otros_usos" type="text" id="txt_otros_usos" style="width:150px;" value="<?php print $DatosPredLocal['DT_OTROS_USO_PREDIO']?>"/>
      </td>
      </tr>
    </table>