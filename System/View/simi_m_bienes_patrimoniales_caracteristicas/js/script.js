$(function() {
    $("#div_tabs").tabs();
	Busqueda_Bien_Patrimonial(1);
});


function mostrar_clases_en_Busqueda(){	
	var busq_Grupo_Generico 		= $("#busq_Grupo_Generico").val();	
			
	$.post("View/simi_grupo/listbox_clase_x_grupo.php",{
		CBO_GRUPO:busq_Grupo_Generico
	},function(data){
		$("#busq_Clase_generico").html(data);
	});	
}

function Clear_Busqueda(){	
  $("#busq_Grupo_Generico").val('');
  $("#busq_Clase_generico").val('');
  $("#busq_cod_bien").val('');
  $("#busq_denom_bien").val(''); 
  Busqueda_Bien_Patrimonial(1);
}


function Busqueda_Bien_Patrimonial(numeroPagina) {
  
  var TXH_COD_ENTIDAD 		= $("#TXH_SIMI_COD_ENTIDAD").val();
  var busq_Grupo_Generico 	= $("#busq_Grupo_Generico").find(":selected").val();
  var busq_Clase_generico 	= $("#busq_Clase_generico").find(":selected").val();
  var busq_cod_bien 		= $("#busq_cod_bien").val();
  var busq_denom_bien 		= $("#busq_denom_bien").val();
  
  carga_loading('detail-Resultado_Busq_Bien_Patrimonial');  
  var url = "View/simi_m_bienes_patrimoniales_caracteristicas/principal.php?operacion=Filtrar_Bien_Patrimonial_x_Parametros";
	  url += (TXH_COD_ENTIDAD == "") ? "" : "&TXH_COD_ENTIDAD=" + TXH_COD_ENTIDAD;
	  url += (busq_Grupo_Generico == "") ? "" : "&busq_Grupo_Generico=" + busq_Grupo_Generico;
	  url += (busq_Clase_generico == "") ? "" : "&busq_Clase_generico=" + busq_Clase_generico;
	  url += (busq_cod_bien == "") ? "" : "&busq_cod_bien=" + busq_cod_bien;
	  url += (busq_denom_bien == "") ? "" : "&busq_denom_bien=" + busq_denom_bien;
	  url += (numeroPagina == "") ? "1" : "&numeroPagina=" + numeroPagina;  
  $.ajax(url).done(function (response) {
    $("#detail-Resultado_Busq_Bien_Patrimonial").html(response);
  });
}





function Mostrar_Dialog_Form(sCOD_UE_BIEN_PATRI){
	$("#Div_Popup_Caracteristicas").dialog({
			//dialogClass: "no-close",
			title: 'Editar Caracteristicas de Bienes Muebles',
			width: 850,
			height: 790,
			resizable: "false",
			modal: true,
			open: function() { 
				Ver_Formulario_Editar(sCOD_UE_BIEN_PATRI);
			},
			close: function() { 
				$(this).dialog("destroy");
				document.getElementById('Div_Contenido_Caracteristicas').innerHTML = '';
			}
	});
}


function Ver_Formulario_Editar(sCOD_UE_BIEN_PATRI) {
  
  carga_loading('Div_Contenido_Caracteristicas');  
  var url = "View/simi_m_bienes_patrimoniales_caracteristicas/principal.php?operacion=Mostrar_Formulario_Editar";
	  url += (sCOD_UE_BIEN_PATRI == "") ? "" : "&sCOD_UE_BIEN_PATRI=" + sCOD_UE_BIEN_PATRI;
  $.ajax(url).done(function (response) {
    $("#Div_Contenido_Caracteristicas").html(response);
  });
}


function Limpiar_Tipo_Cuenta(){
	$('input[name="radio_uso_cuenta"]').attr('checked', false);
	$("#CBO_CTA_NRO_CONTABLE").html('<option value="0" selected="selected">:: SELECCIONE ::</option>');	
}

function Mostrar_Cuenta_Contable(){
	
	var CBO_USO_CTA_CONT  = $("#CBO_USO_CTA_CONT").val();
	var CBO_TIP_CTA_CONT  = $('input[name=radio_uso_cuenta]:checked').val();
	
	if(CBO_USO_CTA_CONT == ''){		
		alert('Seleccione Uso de Cuenta');
		$("#CBO_USO_CTA_CONT").focus();
		
	}else{
		
		$.post("View/simi_cta_contable/listbox_cta_contable_x_parametros.php",{ 
			CBO_USO_CTA_CONT:CBO_USO_CTA_CONT,
			CBO_TIP_CTA_CONT:CBO_TIP_CTA_CONT
		},function(data){
			$("#CBO_CTA_NRO_CONTABLE").html(data);		
		});
		
	}
	
}

function Guardar_Caracteristicas_Bien_Patrimonial(){
	
	var TXH_SIMI_COD_ENTIDAD 		= $("#TXH_SIMI_COD_ENTIDAD").val();
	var TXH_SIMI_COD_USUARIO 		= $("#TXH_SIMI_COD_USUARIO").val();
	
	var txh_f_COD_UE_BIEN_PATRI 	= $("#txh_f_COD_UE_BIEN_PATRI").val();
	var Txh_f_COD_UE_BIEN_ALTA 		= $("#Txh_f_COD_UE_BIEN_ALTA").val();	
	var txt_f_ID_CATALOGO 			= $("#txt_f_ID_CATALOGO").val();	
	var txt_f_NRO_CLASE				= $("#txt_f_NRO_CLASE").val();
	
	var txt_reg_denominacion_bien	= $("#txt_reg_denominacion_bien").val();
	
	var CBO_USO_CTA_CONT 		= $("#CBO_USO_CTA_CONT").val();
	var radio_uso_cuenta 		= $('input[name=radio_uso_cuenta]:checked').val();
	var CBO_CTA_NRO_CONTABLE 	= $("#CBO_CTA_NRO_CONTABLE").val();
	var txt_f_valor_adquisicion = ($("#txt_f_valor_adquisicion").val().replace(/[,]/gi, "")*1);
	
	var txt_porcent_deprec 		= $("#txt_porcent_deprec").val();
	var checkbox_asegurado 		= $('input[name=checkbox_asegurado]:checked').val();
	var cbo_estado_bien_patrim 	= $("#cbo_estado_bien_patrim").val();
	var txt_f_observacion 		= $("#txt_f_observacion").val();
	
	var txh_ctrl_patrim_id_predio 		= $("#txh_ctrl_patrim_id_predio").val();
	var txh_ctrl_patrim_cod_area 		= $("#txh_ctrl_patrim_cod_area").val();
	var txh_ctrl_patrim_cod_oficina 	= $("#txh_ctrl_patrim_cod_oficina").val();
	var txh_ctrl_patrim_cod_personal 	= $("#txh_ctrl_patrim_cod_personal").val();
	
	if(checkbox_asegurado){
		var valor_checkbox_asegurado = 'SI';
	}else{
		var valor_checkbox_asegurado = 'NO';
	}
		
	//****************************************************************************
	//****************************************************************************
		
	var TXT_MARCA 			= $("#TXT_MARCA").val();
	var TXT_MODELO 			= $("#TXT_MODELO").val();
	var TXT_TIPO 			= $("#TXT_TIPO").val();
	var TXT_COLOR 			= $("#TXT_COLOR").val();
	var TXT_NUM_SERIE 		= $("#TXT_NUM_SERIE").val();
	var TXT_NUM_MOTOR 		= $("#TXT_NUM_MOTOR").val();
	var TXT_NUM_PLACA 		= $("#TXT_NUM_PLACA").val();
	var TXT_DIMENSION 		= $("#TXT_DIMENSION").val();
	var TXT_NUM_CHASIS 		= $("#TXT_NUM_CHASIS").val();
	var TXT_NUM_MATRICULA 	= $("#TXT_NUM_MATRICULA").val();
	var TXT_FECHA_FABR 		= $("#TXT_FECHA_FABR").val();
	var TXT_LONGITUD 		= $("#TXT_LONGITUD").val();
	var TXT_ALTURA 			= $("#TXT_ALTURA").val();
	var TXT_ANCHO 			= $("#TXT_ANCHO").val();
	var TXT_RAZA 			= $("#TXT_RAZA").val();
	var TXT_ESPECIE 		= $("#TXT_ESPECIE").val();
	var TXT_EDAD 			= $("#TXT_EDAD").val();
	var TXT_PAIS 			= $("#TXT_PAIS").val();
	var TXT_OTRAS_CARACT 	= $("#TXT_OTRAS_CARACT").val();
		
	
	if(TXT_MARCA){
		TXT_MARCA = TXT_MARCA;
	}else{
		TXT_MARCA = '';
	}
	
	if(TXT_MODELO){
		TXT_MODELO = TXT_MODELO;
	}else{
		TXT_MODELO = '';
	}
	
	if(TXT_TIPO){
		TXT_TIPO = TXT_TIPO;
	}else{
		TXT_TIPO = '';
	}
	
	if(TXT_COLOR){
		TXT_COLOR = TXT_COLOR;
	}else{
		TXT_COLOR = '';
	}
	
	if(TXT_NUM_SERIE){
		TXT_NUM_SERIE = TXT_NUM_SERIE;
	}else{
		TXT_NUM_SERIE = '';
	}
	
	if(TXT_NUM_MOTOR){
		TXT_NUM_MOTOR = TXT_NUM_MOTOR;
	}else{
		TXT_NUM_MOTOR = '';
	}
	
	if(TXT_NUM_PLACA){
		TXT_NUM_PLACA = TXT_NUM_PLACA;
	}else{
		TXT_NUM_PLACA = '';
	}
	
	
	if(TXT_DIMENSION){
		TXT_DIMENSION = TXT_DIMENSION;
	}else{
		TXT_DIMENSION = '';
	}
	
	if(TXT_NUM_CHASIS){
		TXT_NUM_CHASIS = TXT_NUM_CHASIS;
	}else{
		TXT_NUM_CHASIS = '';
	}
	
	
	if(TXT_NUM_MATRICULA){
		TXT_NUM_MATRICULA = TXT_NUM_MATRICULA;
	}else{
		TXT_NUM_MATRICULA = '';
	}
	
	
	if(TXT_FECHA_FABR){
		TXT_FECHA_FABR = TXT_FECHA_FABR;
	}else{
		TXT_FECHA_FABR = '';
	}
	
	
	if(TXT_LONGITUD){
		TXT_LONGITUD = TXT_LONGITUD;
	}else{
		TXT_LONGITUD = '';
	}
	
	
	if(TXT_ALTURA){
		TXT_ALTURA = TXT_ALTURA;
	}else{
		TXT_ALTURA = '';
	}
	
	if(TXT_ANCHO){
		TXT_ANCHO = TXT_ANCHO;
	}else{
		TXT_ANCHO = '';
	}
	
	
	if(TXT_RAZA){
		TXT_RAZA = TXT_RAZA;
	}else{
		TXT_RAZA = '';
	}
	
	if(TXT_ESPECIE){
		TXT_ESPECIE = TXT_ESPECIE;
	}else{
		TXT_ESPECIE = '';
	}
	
	if(TXT_EDAD){
		TXT_EDAD = TXT_EDAD;
	}else{
		TXT_EDAD = '';
	}
	
	if(TXT_PAIS){
		TXT_PAIS = TXT_PAIS;
	}else{
		TXT_PAIS = '';
	}
	
	
	if(TXT_OTRAS_CARACT){
		TXT_OTRAS_CARACT = TXT_OTRAS_CARACT;
	}else{
		TXT_OTRAS_CARACT = '';
	}

	
	//****************************************************************************
	//****************************************************************************
	
	
	if(txt_reg_denominacion_bien == ''){
		alert('Ingrese denominación del bien');
		$("#txt_reg_denominacion_bien").focus();
		
	}else if(CBO_USO_CTA_CONT == '0'){
		alert('Seleccione Uso de la Cuenta');
		$("#CBO_USO_CTA_CONT").focus();
		
	}else if(!radio_uso_cuenta){
		alert('Seleccione Tipo de la Cuenta');
		
	}else if(CBO_CTA_NRO_CONTABLE == '0'){
		alert('Seleccione Cuenta Contable');
		$("#CBO_CTA_NRO_CONTABLE").focus();
		
	}else if(txt_f_valor_adquisicion == ''){
		alert('Ingrese valor de adquisición del Bien');
		$("#txt_f_valor_adquisicion").focus();
		
	}else if(cbo_estado_bien_patrim == ''){
		alert('Seleccione Estado del Bien');
		$("#cbo_estado_bien_patrim").focus();
		
	}else if(txt_porcent_deprec == ''){
		alert('Ingrese Porcentaje de Deprecion');
		$("#txt_porcent_deprec").focus();
		
	}else if(cbo_estado_bien_patrim == '0'){
		alert('Seleccione Estado del Bien');
		$("#cbo_estado_bien_patrim").focus();
		
	}else if(TXT_MARCA == '' && TXT_MODELO == '' && TXT_TIPO == '' && TXT_COLOR == '' && TXT_NUM_SERIE == '' && TXT_NUM_MOTOR == '' && TXT_NUM_PLACA == '' && TXT_DIMENSION == '' && TXT_NUM_CHASIS == '' && TXT_NUM_MATRICULA == '' && TXT_FECHA_FABR == '' && TXT_LONGITUD == '' && TXT_ALTURA == '' && TXT_ANCHO == '' && TXT_RAZA == '' && TXT_ESPECIE == '' && TXT_EDAD == '' && TXT_PAIS == '' && TXT_OTRAS_CARACT == ''){
		alert('Ingrese las caracteristicas técnicas del Bien');
	
	
	}else{
/*
alert(TXT_MARCA+ '---'+TXT_MODELO+ '---'+TXT_TIPO+ '---'+TXT_COLOR+ '---'+TXT_NUM_SERIE+ '---'+TXT_NUM_MOTOR+ '---'+TXT_NUM_PLACA+ '---'+TXT_DIMENSION+ '---'+TXT_NUM_CHASIS+ '---'+TXT_NUM_MATRICULA+ '---'+TXT_FECHA_FABR+ '---'+TXT_LONGITUD+ '---'+TXT_ALTURA+ '---'+TXT_ANCHO+ '---'+TXT_RAZA+ '---'+TXT_ESPECIE+ '---'+TXT_EDAD+ '---'+TXT_PAIS+ '---'+TXT_OTRAS_CARACT);
*/
//Model/M_simi_m_ue_bien_patrimonial.php?operacion=Guardar_UE_Caracteristicas_Bien_Patrimonial
		$.get("View/simi_m_bienes_patrimoniales_caracteristicas/principal.php?operacion=Guardar_Reg_Caracteristicas_Bien",{ 
	
					
				TXH_SIMI_COD_ENTIDAD:TXH_SIMI_COD_ENTIDAD,
				TXH_SIMI_COD_USUARIO:TXH_SIMI_COD_USUARIO,
				
				txh_f_COD_UE_BIEN_PATRI:txh_f_COD_UE_BIEN_PATRI,				
				txt_reg_denominacion_bien:txt_reg_denominacion_bien,								
				
				CBO_USO_CTA_CONT:CBO_USO_CTA_CONT,
				radio_uso_cuenta:radio_uso_cuenta,
				CBO_CTA_NRO_CONTABLE:CBO_CTA_NRO_CONTABLE,
				txt_f_valor_adquisicion:txt_f_valor_adquisicion,
				txt_porcent_deprec:txt_porcent_deprec,
				valor_checkbox_asegurado:valor_checkbox_asegurado,
				cbo_estado_bien_patrim:cbo_estado_bien_patrim,
				txt_f_observacion:txt_f_observacion,
								
				TXT_MARCA:TXT_MARCA,
				TXT_MODELO:TXT_MODELO,
				TXT_TIPO:TXT_TIPO,
				TXT_COLOR:TXT_COLOR,
				TXT_NUM_SERIE:TXT_NUM_SERIE,
				TXT_NUM_MOTOR:TXT_NUM_MOTOR,
				TXT_NUM_PLACA:TXT_NUM_PLACA,
				TXT_DIMENSION:TXT_DIMENSION,
				TXT_NUM_CHASIS:TXT_NUM_CHASIS,
				TXT_NUM_MATRICULA:TXT_NUM_MATRICULA,
				TXT_FECHA_FABR:TXT_FECHA_FABR,
				TXT_LONGITUD:TXT_LONGITUD,
				TXT_ALTURA:TXT_ALTURA,
				TXT_ANCHO:TXT_ANCHO,
				TXT_RAZA:TXT_RAZA,
				TXT_ESPECIE:TXT_ESPECIE,
				TXT_EDAD:TXT_EDAD,
				TXT_PAIS:TXT_PAIS,
				TXT_OTRAS_CARACT:TXT_OTRAS_CARACT
				
		},function(data){
			
			if(data == 1){
				alert('Se guardo Correctamente las caracteristicas de los bienes Muebles');
				$("#Div_Popup_Caracteristicas").dialog("destroy");
				document.getElementById('Div_Contenido_Caracteristicas').innerHTML = '';
				$("#busq_denom_bien").val(txt_reg_denominacion_bien);
				Busqueda_Bien_Patrimonial(1);
			}else{
				alert(data);
			}
		});
	}
	
}


/**************************************************************/
/**************************************************************/
/**************************************************************/
	
	separadorDecimalesInicial="."; //Modifique este dato para poder obtener la nomenclatura que utilizamos en mi pais
	separadorDecimales="."; //Modifique este dato para poder obtener la nomenclatura que utilizamos en mi pais
	separadorMiles=","; //Modifique este dato para poder obtener la nomenclatura que utilizamos en mi pais
	
	function arreglar(numero){ 
		var numeroo=""; 
		numero=""+numero; 
		partes=numero.split(separadorDecimalesInicial);
		entero=partes[0];
		
		if(partes.length>1){ 
			decimal=partes[1]; 
		} 
		
		cifras=entero.length; 
		cifras2=cifras 
		
		for(a=0;a<cifras;a++){
			cifras2-=1;
			numeroo+=entero.charAt(a);
			
			if(cifras2%3==0 &&cifras2!=0){
				numeroo+=separadorMiles;
			}
		} 
		
		if(partes.length>1){
			numeroo+=separadorDecimales+decimal;
		}
		
		return numeroo 
	}

	function formatodecimales(input){
		var num = input.value.replace(/\,/g,'');		
		input.value = arreglar(num);		
		if(!isNaN(num)){
			//num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
			//num = num.split('').reverse().join('').replace(/^[\,]/,'');			
			num=num.reverse().replace(/[^0-9.]/g,'').replace(/\.(?=\d*[.]\d*)/g,'').reverse();
			
			input.value = num;
		}else{ alert('Solo se permiten numeros');
			input.value = input.value.replace(/[^\d\.]*/g,'');
		}
	}

/*******************************************************/
/*******************************************************/
/*******************************************************/