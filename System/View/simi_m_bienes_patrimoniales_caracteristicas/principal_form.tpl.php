<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<?php

	$ArrayRegDatoBienPatrimonial = $data['RegDatosBienPatrimonial'];
	$ArrayDataEstadoBien = $data['DataEstadoBien'];
	$ArrayListaCuentaContable_Parametro = $data['DataListaCuentaContable_Parametro'];
	
	//$this->dump($ArrayRegDatoBienPatrimonial);
	
	$USO_CUENTA_BP = $ArrayRegDatoBienPatrimonial[0]['USO_CUENTA'];
	$TIP_CUENTA_BP = $ArrayRegDatoBienPatrimonial[0]['TIP_CUENTA'];	
	$VALOR_ADQUIS_BP = number_format($ArrayRegDatoBienPatrimonial[0]['VALOR_ADQUIS'],3);
	$PORC_DEPREC_BP = $ArrayRegDatoBienPatrimonial[0]['PORC_DEPREC'];
	$OPC_ASEGURADO_BP = $ArrayRegDatoBienPatrimonial[0]['OPC_ASEGURADO'];
	$COD_ESTADO_BIEN_BP = $ArrayRegDatoBienPatrimonial[0]['COD_ESTADO_BIEN'];
	$NRO_CLASE_BP = $ArrayRegDatoBienPatrimonial[0]['NRO_CLASE'];
	$eCOD_CTA_CONTABLE = $ArrayRegDatoBienPatrimonial[0]['COD_CTA_CONTABLE'];
		
	if($PORC_DEPREC_BP == ''){
		$VAL_PORCENT_DEPREC = number_format($ArrayRegDatoBienPatrimonial[0]['DEPRECIACION'],2);
	}else{
		$VAL_PORCENT_DEPREC = number_format($PORC_DEPREC_BP,2);
	}

	if($TIP_CUENTA_BP == 'A'){
		$SELECTED_TIP_CUENTA_A_BP = ' checked="checked"';
		$SELECTED_TIP_CUENTA_O_BP = '';
		
	}else if($TIP_CUENTA_BP == 'O'){
		$SELECTED_TIP_CUENTA_A_BP = '';
		$SELECTED_TIP_CUENTA_O_BP = 'checked="checked"';
		
	}else{
		$SELECTED_TIP_CUENTA_A_BP = '';
		$SELECTED_TIP_CUENTA_O_BP = '';		
	}
	
	$SELECTED_ASEGURADO = ($OPC_ASEGURADO_BP == 'SI') ? 'checked="checked"':'';
	
	
	if($NRO_CLASE_BP == '04'){//AERONAVE
		
		$TR_MARCA 		= 'X';
		$TR_MODELO 		= 'X';
		$TR_TIPO 		= 'X';
		$TR_COLOR 		= 'X';
		$TR_NUMERO_SERIE 	= 'X';
		$TR_NUMERO_MOTOR	= 'X';
		$TR_NUMERO_CHASIS 	= 'X';
		$TR_DIMENSION 	= 'X';
		$TR_RAZA 		= '';
		$TR_ESPECIE 	= '';
		$TR_PLACA 		= 'X';
		$TR_EDAD 		= '';
		$TR_PAIS 		= 'X';
		$TR_MATRICULA 	= 'X';
		$TR_ANIO_FABRICACION = 'X';
		$TR_LONGITUD 	= 'X';
		$TR_ALTURA 		= 'X';
		$TR_ANCHO 		= 'X';
		$TR_OTROS 		= 'X';
		
	}else if($NRO_CLASE_BP == '08'){//COMPUTO
		
		$TR_MARCA 		= 'X';
		$TR_MODELO 		= 'X';
		$TR_TIPO 		= 'X';
		$TR_COLOR 		= 'X';
		$TR_NUMERO_SERIE = 'X';
		$TR_NUMERO_MOTOR = '';
		$TR_NUMERO_CHASIS = '';
		$TR_DIMENSION 	= '';
		$TR_RAZA 		= '';
		$TR_ESPECIE 	= '';
		$TR_PLACA 		= '';
		$TR_EDAD 		= '';
		$TR_PAIS 		= '';
		$TR_MATRICULA 	= '';
		$TR_ANIO_FABRICACION = 'X';
		$TR_LONGITUD 	= '';
		$TR_ALTURA 		= '';
		$TR_ANCHO 		= '';
		$TR_OTROS 		= 'X';
		
	}else if($NRO_CLASE_BP == '22'){//EQUIPO 
		
		$TR_MARCA 		= 'X';
		$TR_MODELO 		= 'X';
		$TR_TIPO 		= 'X';
		$TR_COLOR 		= 'X';
		$TR_NUMERO_SERIE = 'X';
		$TR_NUMERO_MOTOR = '';
		$TR_NUMERO_CHASIS = 'X';
		$TR_DIMENSION 	= '';
		$TR_RAZA 		= '';
		$TR_ESPECIE 	= '';
		$TR_PLACA 		= '';
		$TR_EDAD 		= '';
		$TR_PAIS 		= '';
		$TR_MATRICULA 	= '';
		$TR_ANIO_FABRICACION = 'X';
		$TR_LONGITUD 	= '';
		$TR_ALTURA 		= '';
		$TR_ANCHO 		= '';
		$TR_OTROS 		= 'X';
		
	}else if($NRO_CLASE_BP == '29'){//FERROCARRIL 
		
		$TR_MARCA 		= 'X';
		$TR_MODELO 		= 'X';
		$TR_TIPO 		= 'X';
		$TR_COLOR 		= 'X';
		$TR_NUMERO_SERIE = '';
		$TR_NUMERO_MOTOR = 'X';
		$TR_NUMERO_CHASIS = 'X';
		$TR_DIMENSION 	= 'X';
		$TR_RAZA 		= '';
		$TR_ESPECIE 	= '';
		$TR_PLACA 		= 'X';
		$TR_EDAD 		= '';
		$TR_PAIS 		= '';
		$TR_MATRICULA 	= '';
		$TR_ANIO_FABRICACION = 'X';
		$TR_LONGITUD 	= '';
		$TR_ALTURA 		= '';
		$TR_ANCHO 		= '';
		$TR_OTROS 		= 'X';
		
	}else if($NRO_CLASE_BP == '36'){//MAQUINARIA PESADA 
		
		$TR_MARCA 		= 'X';
		$TR_MODELO 		= 'X';
		$TR_TIPO 		= 'X';
		$TR_COLOR 		= 'X';
		$TR_NUMERO_SERIE = 'X';
		$TR_NUMERO_MOTOR = 'X';
		$TR_NUMERO_CHASIS = '';
		$TR_DIMENSION 	= '';
		$TR_RAZA 		= '';
		$TR_ESPECIE 	= '';
		$TR_PLACA 		= '';
		$TR_EDAD 		= '';
		$TR_PAIS 		= '';
		$TR_MATRICULA 	= '';
		$TR_ANIO_FABRICACION = 'X';
		$TR_LONGITUD 	= '';
		$TR_ALTURA 		= '';
		$TR_ANCHO 		= '';
		$TR_OTROS 		= 'X';
		
	}else if($NRO_CLASE_BP == '50'){//MAQUINA 
		
		$TR_MARCA 		= 'X';
		$TR_MODELO 		= 'X';
		$TR_TIPO 		= 'X';
		$TR_COLOR 		= 'X';
		$TR_NUMERO_SERIE = 'X';
		$TR_NUMERO_MOTOR = '';
		$TR_NUMERO_CHASIS = '';
		$TR_DIMENSION 	= '';
		$TR_RAZA 		= '';
		$TR_ESPECIE 	= '';
		$TR_PLACA 		= '';
		$TR_EDAD 		= '';
		$TR_PAIS 		= '';
		$TR_MATRICULA 	= '';
		$TR_ANIO_FABRICACION = 'X';
		$TR_LONGITUD 	= '';
		$TR_ALTURA 		= '';
		$TR_ANCHO 		= '';
		$TR_OTROS 		= 'X';
		
	}else if($NRO_CLASE_BP == '64'){//MOBILIARIO 
		
		$TR_MARCA 		= 'X';
		$TR_MODELO 		= 'X';
		$TR_TIPO 		= 'X';
		$TR_COLOR 		= 'X';
		$TR_NUMERO_SERIE = '';
		$TR_NUMERO_MOTOR = '';
		$TR_NUMERO_CHASIS = '';
		$TR_DIMENSION 	= 'X';
		$TR_RAZA 		= '';
		$TR_ESPECIE 	= '';
		$TR_PLACA 		= '';
		$TR_EDAD 		= '';
		$TR_PAIS 		= '';
		$TR_MATRICULA 	= '';
		$TR_ANIO_FABRICACION = 'X';
		$TR_LONGITUD 	= '';
		$TR_ALTURA 		= '';
		$TR_ANCHO 		= '';
		$TR_OTROS 		= 'X';
		
	}else if($NRO_CLASE_BP == '71'){//NAVE O ARTEFACTO NAVAL 
		
		$TR_MARCA 		= 'X';
		$TR_MODELO 		= 'X';
		$TR_TIPO 		= 'X';
		$TR_COLOR 		= 'X';
		$TR_NUMERO_SERIE = 'X';
		$TR_NUMERO_MOTOR = 'X';
		$TR_NUMERO_CHASIS = 'X';
		$TR_DIMENSION 	= 'X';
		$TR_RAZA 		= '';
		$TR_ESPECIE 	= '';
		$TR_PLACA 		= 'X';
		$TR_EDAD 		= '';
		$TR_PAIS 		= '';
		$TR_MATRICULA 	= '';
		$TR_ANIO_FABRICACION = 'X';
		$TR_LONGITUD 	= '';
		$TR_ALTURA 		= '';
		$TR_ANCHO 		= '';
		$TR_OTROS 		= 'X';
		
	}else if($NRO_CLASE_BP == '78'){//PRODUCCION Y SEGURIDAD
		
		$TR_MARCA 		= '';
		$TR_MODELO 		= '';
		$TR_TIPO 		= 'X';
		$TR_COLOR 		= 'X';
		$TR_NUMERO_SERIE = '';
		$TR_NUMERO_MOTOR = '';
		$TR_NUMERO_CHASIS = '';
		$TR_DIMENSION 	= '';
		$TR_RAZA 		= 'X';
		$TR_ESPECIE 	= 'X';
		$TR_PLACA 		= '';
		$TR_EDAD 		= 'X';
		$TR_PAIS 		= '';
		$TR_MATRICULA 	= '';
		$TR_ANIO_FABRICACION = '';
		$TR_LONGITUD 	= '';
		$TR_ALTURA 		= '';
		$TR_ANCHO 		= '';
		$TR_OTROS 		= 'X';
		
	}else if($NRO_CLASE_BP == '82'){//VEHICULO
		
		$TR_MARCA 		= 'X';
		$TR_MODELO 		= 'X';
		$TR_TIPO 		= 'X';
		$TR_COLOR 		= 'X';
		$TR_NUMERO_SERIE = 'X';
		$TR_NUMERO_MOTOR = 'X';
		$TR_NUMERO_CHASIS = 'X';
		$TR_DIMENSION 	= '';
		$TR_RAZA 		= '';
		$TR_ESPECIE 	= '';
		$TR_PLACA 		= 'X';
		$TR_EDAD 		= '';
		$TR_PAIS 		= '';
		$TR_MATRICULA 	= '';
		$TR_ANIO_FABRICACION = 'X';
		$TR_LONGITUD 	= '';
		$TR_ALTURA 		= '';
		$TR_ANCHO 		= '';
		$TR_OTROS 		= 'X';
		
	}
	
?>
<table width="826" border="0" cellpadding="0" cellspacing="1">
  <tr>
    <td width="12">&nbsp;</td>
    <td colspan="2" align="center">
    <input name="txh_f_COD_UE_BIEN_PATRI" type="hidden" id="txh_f_COD_UE_BIEN_PATRI" style="width:80px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['COD_UE_BIEN_PATRI']?>" />
    <input name="Txh_f_COD_UE_BIEN_ALTA" type="hidden" id="Txh_f_COD_UE_BIEN_ALTA" style="width:80px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['COD_UE_BIEN_ALTA']?>" />
    <input name="txt_f_ID_CATALOGO" type="hidden" id="txt_f_ID_CATALOGO"  style="width:30px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['ID_CATALOGO']?>" />
    <input name="txt_f_NRO_CLASE" type="hidden" id="txt_f_NRO_CLASE"  style="width:30px" value="<?php print $NRO_CLASE_BP?>" /></td>
    <td width="14" align="center">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2"><fieldset class="TABLE_border4" style="background-color:#F4F4F4">
      <legend class="texto_arial_azul_n_11" style="background-color:#F4F4F4; padding: 0px 5px 0px 5px"> Documento de Adquisición </legend>
      <table width="784" border="0" cellspacing="3" cellpadding="0">
        <tr>
          <td>&nbsp;</td>
          <td >&nbsp;</td>
          <td colspan="3" style="text-align:left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td width="182" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;Forma de  Adquisición</td>
          <td colspan="3" style="text-align:left">
<input name="txt_f_forma_adquisicion" type="text" id="txt_f_forma_adquisicion" style="width:540px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['ABREV_ALTA']?> - <?php print $ArrayRegDatoBienPatrimonial[0]['NOM_FORM_ADQUIS']?>" readonly="readonly" /></td>
          </tr>
        <tr>
          <td width="14">&nbsp;</td>
          <td width="182" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;Nro. Documento Adquisición</td>
          <td width="276" style="text-align:left">
<input name="txt_f_nro_adquisicion" type="text" id="txt_f_nro_adquisicion" style="width:250px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['NRO_DOCUMENTO_ADQUIS']?>" readonly="readonly" /></td>
          <td width="138" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11" style="text-align:left">&nbsp;&nbsp;Fecha de  Adquisición</td>
          <td width="156" style="text-align:left">
<input name="txt_f_fcha_adquisicion" type="text" id="txt_f_fcha_adquisicion" style="width:120px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['FECHA_DOCUMENTO_ADQUIS']?>" readonly="readonly" /></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td colspan="4">&nbsp;</td>
        </tr>
      </table>
    </fieldset></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="25">&nbsp;</td>
    <td colspan="2"><fieldset class="TABLE_border4" style="background-color:#F4F4F4">
      <legend class="texto_arial_azul_n_11" style="background-color:#F4F4F4; padding: 0px 5px 0px 5px"> Bien Patrimonial</legend>
      <table width="784" border="0" cellspacing="3" cellpadding="0">
        <tr >
          <td width="8">&nbsp;</td>
          <td width="147">&nbsp;</td>
          <td width="132" height="17" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11" style="text-align:center">Código Patrimonial</td>
          <td width="283" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11" style="text-align:center">Nombrel del Grupo </td>
          <td width="196" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11" style="text-align:center">Nombre de la Clase</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;Codigo Patrimonial</td>
          <td style="text-align:left"><input name="txt_f_nro_bien" type="text" id="txt_f_nro_bien" style="width:100%" value="<?php print $ArrayRegDatoBienPatrimonial[0]['CODIGO_PATRIMONIAL']?>" readonly="readonly" /></td>
          <td>
            <input name="txt_nom_grupo" type="text" id="txt_nom_grupo" style="width:100%" value="<?php print $ArrayRegDatoBienPatrimonial[0]['NRO_GRUPO']?> - <?php print $ArrayRegDatoBienPatrimonial[0]['DESC_GRUPO']?>" readonly="readonly" />
          </td>
          <td><span style="text-align:left">
            <input name="txt_nom_clase" type="text" id="txt_nom_clase" style="width:100%" value="<?php print $ArrayRegDatoBienPatrimonial[0]['NRO_CLASE']?> - <?php print $ArrayRegDatoBienPatrimonial[0]['DESC_CLASE']?>" readonly="readonly" />
            </span></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11" style="text-align:left">&nbsp;&nbsp;Denominación  del Bien</td>
          <td colspan="3"><input name="txt_nom_cat_bien" type="text" id="txt_nom_cat_bien" style="width:100%" value="<?php print $ArrayRegDatoBienPatrimonial[0]['NOM_FAMILIA_BIEN']?>" readonly="readonly" />
            </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td colspan="4"><hr class="hr_01" /></td>
          </tr>
        <tr>
          <td>&nbsp;</td>
          <td width="147" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11" style="text-align:left">&nbsp;&nbsp;Descripción del Bien </td>
          <td colspan="3"><input name="txt_reg_denominacion_bien" type="text" id="txt_reg_denominacion_bien" style="width:100%" value="<?php print $ArrayRegDatoBienPatrimonial[0]['DENOMINACION_BIEN']?>" /></td>
          </tr>
        <tr>
          <td>&nbsp;</td>
          <td colspan="4">&nbsp;</td>
        </tr>
        </table>
    </fieldset></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td width="371" valign="top"><table width="361" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" style="background-color:#F4F4F4">
      <tr>
        <td>&nbsp;</td>
        <td class="texto_arial_blanco_n_11">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td width="10">&nbsp;</td>
        <td width="102" height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;Uso  de Cuenta</td>
        <td width="226">&nbsp;
          <select name="CBO_USO_CTA_CONT" id="CBO_USO_CTA_CONT" style="width: 210px" onchange="Limpiar_Tipo_Cuenta()" >
            <option value="0" selected="selected">:: SELECCIONE ::</option>
            <?
                if($USO_CUENTA_BP == 'P'){
					$selec_P = 'selected="selected"';
					$selec_E = '';
				}else if($USO_CUENTA_BP == 'E'){
					$selec_P = '';
					$selec_E = 'selected="selected"';
				}else{
					$selec_P = '';
					$selec_E = '';
				}
			?>
            <option value="P" <?=$selec_P?> > DE USO PRIVADO</option>
            <option value="E" <?=$selec_E?> > DE USO ESTATAL</option>
          </select></td>
        <td width="11">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;Tipo de Cuenta</td>
        <td><table width="216" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="97">&nbsp;
              <label>
                <input name="radio_uso_cuenta" type="radio" id="radio_uso_cuenta1" onclick="Mostrar_Cuenta_Contable()" value="A" <?=$SELECTED_TIP_CUENTA_A_BP?> />
                Activo Fijo</label></td>
            <td width="119"><label>
              <input type="radio" name="radio_uso_cuenta" id="radio_uso_cuenta2" value="O" onclick="Mostrar_Cuenta_Contable()" <?=$SELECTED_TIP_CUENTA_O_BP?> />
              Cuenta de Orden</label></td>
          </tr>
        </table></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td height="25" bgcolor="#8C8C8C"><span class="texto_arial_blanco_n_11">&nbsp;&nbsp;Cuenta Contable</span></td>
        <td>&nbsp;
          <select name="CBO_CTA_NRO_CONTABLE" id="CBO_CTA_NRO_CONTABLE" style="width: 210px" >
            <option value="0" selected="selected">:: SELECCIONE ::</option>
            <?php if($ArrayListaCuentaContable_Parametro) foreach ($ArrayListaCuentaContable_Parametro as $ListaCuentaContable_Parametro): 
				$selected_CtaCble = ($eCOD_CTA_CONTABLE == $ListaCuentaContable_Parametro['COD_CTA_CONTABLE'])? 'selected="selected"':'';
				
				$TEXT_NRO_CTA_CONTABLE_A = str_replace("*","&nbsp;",$ListaCuentaContable_Parametro['TEXT_NRO_CTA_CONTABLE']);
				$NOM_CTA_CONTABLE_A	= $ListaCuentaContable_Parametro['NOM_CTA_CONTABLE'];
				
			?>
            <option value="<?php print $ListaCuentaContable_Parametro['COD_CTA_CONTABLE']?>" <?=$selected_CtaCble?> ><?php print $TEXT_NRO_CTA_CONTABLE_A?> <?php print $NOM_CTA_CONTABLE_A?> </option>
            <?php endforeach; ?>
          </select></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td height="25" bgcolor="#8C8C8C"><span class="texto_arial_blanco_n_11">&nbsp;&nbsp;Valor Adq. S/.</span></td>
        <td>&nbsp;&nbsp;<input name="txt_f_valor_adquisicion" type="text" id="txt_f_valor_adquisicion" style="width:100px" value="<?=$VALOR_ADQUIS_BP?>" onkeyup="formatodecimales(this)" /></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;% Depreciación</td>
        <td>&nbsp;&nbsp;<input name="txt_porcent_deprec" type="text" id="txt_porcent_deprec" style="width:100px" value="<?=$VAL_PORCENT_DEPREC?>" disabled="disabled" /> 
        %</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;Asegurado</td>
        <td>&nbsp;
          <input type="checkbox" name="checkbox_asegurado" id="checkbox_asegurado" <?=$SELECTED_ASEGURADO?> /></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;Estado del Bien</td>
        <td>&nbsp;

			<select name="cbo_estado_bien_patrim" id="cbo_estado_bien_patrim" style="width: 210px" >
            <option value="" selected="selected">:: SELECCIONE ::</option>
            <?php if($ArrayDataEstadoBien) foreach ($ArrayDataEstadoBien as $ListEstadoBien): 
				$selected_estado_bien = ($COD_ESTADO_BIEN_BP == $ListEstadoBien['COD_ESTADO_BIEN'])? 'selected="selected"':'';
			?>
            <option value="<?php print $ListEstadoBien['COD_ESTADO_BIEN']?>" <?=$selected_estado_bien?> ><?php print $ListEstadoBien['ABRE_EST_BIEN']?> - <?php print $ListEstadoBien['NOM_EST_BIEN']?></option>
            <?php endforeach; ?>
            </select>
          
          </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td height="25" valign="top" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;Observación</td>
        <td>&nbsp;<span style="text-align:center">
          <textarea name="txt_f_observacion" rows="3" id="txt_f_observacion" style="width:210px"><?php print $ArrayRegDatoBienPatrimonial[0]['OBSERVACION']?></textarea>
        </span></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
      </tr>
      <tr>
        <td height="45" colspan="4" bgcolor="#FFFFFF" style="padding:10px;" class="TABLE_border4">
        
        <input name="button2" type="button" class="btn btn-primary" id="button2" value="Guardar Datos del Bien" onclick="Guardar_Caracteristicas_Bien_Patrimonial()" /></td>
      </tr>
    </table></td>
    <td width="422" valign="top"><fieldset class="TABLE_border4" style="background-color:#F4F4F4">
      <legend class="texto_arial_azul_n_11" style="background-color:#F4F4F4; padding: 0px 5px 0px 5px">Detalle Técnico del Bien</legend>
    
    <table width="407" border="0" cellspacing="2" cellpadding="0">
    
    <tr>
    <td width="11">&nbsp;</td>
    <td width="162">&nbsp;</td>
    <td width="234">&nbsp;</td>
    </tr>
 



<? if($TR_MARCA == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;MARCA</td>
    <td><input name="TXT_MARCA" type="text" id="TXT_MARCA" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['MARCA']?>" /></td>
  </tr>
<? } ?>

<? if($TR_MODELO == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;MODELO</td>
    <td><input name="TXT_MODELO" type="text" id="TXT_MODELO" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['MODELO']?>" /></td>
    </tr>
<? } ?>

<? if($TR_TIPO == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;TIPO</td>
    <td><input name="TXT_TIPO" type="text" id="TXT_TIPO" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['TIPO']?>" /></td>
    </tr>
  <tr>
<? } ?>

<? if($TR_COLOR == 'X'){ ?>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;COLOR</td>
    <td><input name="TXT_COLOR" type="text" id="TXT_COLOR" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['COLOR']?>" /></td>
    </tr>
<? } ?>

<? if($TR_NUMERO_SERIE == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;NRO. DE SERIE</td>
    <td><input name="TXT_NUM_SERIE" type="text" id="TXT_NUM_SERIE" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['SERIE']?>" /></td>
    </tr>
<? } ?>

<? if($TR_NUMERO_MOTOR == 'X'){ ?>
   <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;NRO. DE MOTOR</td>
    <td><input name="TXT_NUM_MOTOR" type="text" id="TXT_NUM_MOTOR" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['NRO_MOTOR']?>" /></td>
    </tr>
  <tr>
<? } ?>

<? if($TR_PLACA == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;PLACA</td>
    <td><input name="TXT_NUM_PLACA" type="text" id="TXT_NUM_PLACA" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['PLACA']?>" /></td>
    </tr>
<? } ?>

<? if($TR_DIMENSION == 'X'){ ?>
 <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;DIMENSIÓN</td>
    <td><input name="TXT_DIMENSION" type="text" id="TXT_DIMENSION" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['DIMENSION']?>" /></td>
    </tr>
<? } ?>

<? if($TR_NUMERO_CHASIS == 'X'){ ?>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;NRO. DE CHASIS</td>
    <td><input name="TXT_NUM_CHASIS" type="text" id="TXT_NUM_CHASIS" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['NRO_CHASIS']?>" /></td>
    </tr>
<? } ?>

<? if($TR_MATRICULA == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;MATRICULA</td>
    <td><input name="TXT_NUM_MATRICULA" type="text" id="TXT_NUM_MATRICULA" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['MATRICULA']?>" /></td>
    </tr>
<? } ?>

<? if($TR_ANIO_FABRICACION == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;AÑO DE FABRICACIÓN</td>
    <td><input name="TXT_FECHA_FABR" type="text" id="TXT_FECHA_FABR" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['ANIO_FABRICACION']?>" /></td>
    </tr>
<? } ?>

<? if($TR_LONGITUD == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;LONGITUD</td>
    <td><input name="TXT_LONGITUD" type="text" id="TXT_LONGITUD" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['LONGITUD']?>" /></td>
    </tr>
<? } ?>

<? if($TR_ALTURA == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;ALTURA</td>
    <td><input name="TXT_ALTURA" type="text" id="TXT_ALTURA" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['ALTURA']?>" /></td>
    </tr>
<? } ?>

<? if($TR_ANCHO == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;ANCHO</td>
    <td><input name="TXT_ANCHO" type="text" id="TXT_ANCHO" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['ANCHO']?>" /></td>
    </tr>
<? } ?>

<? if($TR_RAZA == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;RAZA</td>
    <td><input name="TXT_RAZA" type="text" id="TXT_RAZA" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['RAZA']?>" /></td>
    </tr>
<? } ?>

<? if($TR_ESPECIE == 'X'){ ?>

  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;ESPECIE</td>
    <td><input name="TXT_ESPECIE" type="text" id="TXT_ESPECIE" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['ESPECIE']?>" /></td>
    </tr>
<? } ?>

<? if($TR_EDAD == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;EDAD</td>
    <td><input name="TXT_EDAD" type="text" id="TXT_EDAD" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['EDAD']?>" /></td>
    </tr>
<? } ?>

<? if($TR_PAIS == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;PAIS</td>
    <td><input name="TXT_PAIS" type="text" id="TXT_PAIS" style="width:200px" value="<?php print $ArrayRegDatoBienPatrimonial[0]['PAIS']?>" /></td>
    </tr>
<? } ?>

<? if($TR_OTROS == 'X'){ ?>
  <tr>
    <td>&nbsp;</td>
    <td height="25" valign="top" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">&nbsp;&nbsp;OTRAS CARACTERISTICAS</td>
    <td><textarea name="TXT_OTRAS_CARACT" rows="5" id="TXT_OTRAS_CARACT" style="width:200px"><?php print $ArrayRegDatoBienPatrimonial[0]['OTRAS_CARACT']?></textarea></td>
  </tr>
<? } ?>


  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
    
</table>
    </fieldset></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>

