<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<?php 
$ArrayGrupo = $data['DataListaGrupo'];
//print_r($ArrayGrupo);
?>
<script>

$(function(){
	$( "#div_tabs" ).tabs();
});
  
</script>

<table width="100%" border="0" cellspacing="3" cellpadding="0">
    <tr>
      <td width="1045" valign="top">


<table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td width="82%" >
      <div class="Titulo_02_19px">
        <img src="../webimages/iconos/btn_admin_55.png" width="58" height="58" align="absmiddle" /> Modificación de Bienes Muebles</div></td>
    </tr>
  <tr>
    <td ><hr /></td>
    </tr>
  <tr>
    <td >&nbsp;</td>
  </tr>
  </table>
<div id="div_tabs" >
  <ul>

    <li><a href="#tabs-1">Busqueda de Bienes</a></li>


  </ul>

  <div id="tabs-1">
    <table width="1040" border="0" cellspacing="3" cellpadding="0" bgcolor="#f4f4f4" class="TABLE_border4">
      <tr>
        <td width="18">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td colspan="5">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td width="105" class="texto_arial_plomito_11_N">Grupo Generico</td>
        <td width="15" class="texto_arial_plomito_11_N">:</td>
        <td>
          <select name="busq_Grupo_Generico" id="busq_Grupo_Generico" style="width:200px;" onchange="mostrar_clases_en_Busqueda()">
            <option value="" selected="selected">:: TODOS ::</option>
            <?php if($ArrayGrupo) foreach ($ArrayGrupo as $ListGrupo): ?>
            <option value="<?php print $ListGrupo['COD_GRUPO']?>"><?php print $ListGrupo['NRO_GRUPO']?> - <?php print $ListGrupo['DESC_GRUPO']?></option>
            <?php endforeach; ?>
            </select>
          </td>
        <td class="texto_arial_plomito_11_N">Clase Generico</td>
        <td class="texto_arial_plomito_11_N">:</td>
        <td><select name="busq_Clase_generico" id="busq_Clase_generico" style="width:200px;">
          <option value="" selected="selected">:: TODOS ::</option>
          </select></td>
        <td width="101" rowspan="2"><table width="85" border="0" cellpadding="0" cellspacing="3" class="TABLE_border4" bgcolor="#FFFFFF">
          <tr>
            <td width="14">&nbsp;</td>
            <td width="60">&nbsp;</td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>
              <a href="#" onclick='Busqueda_Bien_Patrimonial(1); '><img src="../webimages/iconos/ver_icono_b.png" width="20" height="22" border="0" /></a> &nbsp;&nbsp;
              <a href="#" onclick="Clear_Busqueda()">
                <img src="../webimages/iconos/quitar_flitros.png" width="22" height="22" border="0" /></a>
              </td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Código del Bien</td>
        <td class="texto_arial_plomito_11_N">:</td>
        <td width="239"><input name="busq_cod_bien" type="text" id="busq_cod_bien" style="width:200px;"/>          </select>
          </td>
        <td width="137" class="texto_arial_plomito_11_N">Denominación del Bien</td>
        <td width="19" class="texto_arial_plomito_11_N">:</td>
        <td width="377"><input name="busq_denom_bien" type="text" id="busq_denom_bien" style="width:350px;"/></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
      </tr>
      </table>
    <div id="detail-Resultado_Busq_Bien_Patrimonial" style="padding:10px 0px 10px 0px"></div>

  </div>
    </div>
      </td>
      <td width="307" valign="top">
<div id="detail-formulario" style="padding:10px 0px 10px 25px;"></div>
      </td>
    </tr>
</table>

<script type="text/javascript" >
    <?php include_once('js/script.js') ?>
</script>

<div id="Div_Popup_Caracteristicas"><div id="Div_Contenido_Caracteristicas"></div></div>