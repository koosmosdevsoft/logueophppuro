<?php session_start();
require_once('../../../utils/fpdf181/fpdf.php');
require_once('../../../utils/funciones/funciones.php');
require_once("../../Controller/BaseBD_PDO.php");
require_once('../../Model/BaseBD_PDOModel.php');

require_once('../../Controller/BienPatrimonialController.php');
require_once('../../Model/BienPatrimonialModel.php');

require_once('../../Model/CatalogoModel.php');
require_once('../../Model/CtaContableModel.php');

error_reporting(0);
$oBienPatrimonialController	=	new BienPatrimonialController;

if( $_GET['operacion'] == 'Filtrar_Bien_Patrimonial_x_Parametros' ){
  $oBienPatrimonialController->Filtrar_Bien_Patrimonial_x_Parametros();
    
}else if( $_GET['operacion'] == 'Mostrar_Formulario_Editar' ){
  $oBienPatrimonialController->Mostrar_Formulario_Editar();
    
}else if( $_GET['operacion'] == 'Guardar_Reg_Caracteristicas_Bien' ){
  $oBienPatrimonialController->Guardar_Reg_Caracteristicas_Bien();
    
}else{
  $oBienPatrimonialController->index();  
}
?>
