<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="../utils/css/stylesPaginacion.css"/>
<?php
$ArrayListaBienPatrimonial = $data['DataListaBienPatrimonial'];
$pager = $data['pager'];
//$this->dump($pager);
?>

<table width="1040" border="0" cellpadding="0" cellspacing="0" class="TABLE_border4" >
  <tr>
    <th width="38" height="30" bgcolor="#DDE4EC" style="text-align:center">Item</th>
    <th width="80" bgcolor="#DDE4EC" style="text-align:center">Código <br />
    Patrimonial</th>
    <th width="272" bgcolor="#DDE4EC" style="text-align:center">Denominación Bien</th>
    <th width="267" bgcolor="#DDE4EC" style="text-align:center">Grupo</th>
    <th width="138" bgcolor="#DDE4EC" style="text-align:center">Clase</th>
    <th width="140" bgcolor="#DDE4EC" style="text-align:center">Nro Documento<br />
    Adquisición</th>
    <th width="74" bgcolor="#DDE4EC" style="text-align:center">Fecha<br />
      Adquisición</th>
    <th width="38" bgcolor="#DDE4EC" style="text-align:center">Editar</th>
  </tr>
  
<?php if($ArrayListaBienPatrimonial) foreach ($ArrayListaBienPatrimonial as $ListaBienPatrimonial): ?>

  <tr  onmouseover="mOvr2(this)" onmouseout="mOut2(this)">
    <td height="15" style="color:#09C; text-align:center;"><?php print $ListaBienPatrimonial['ROW_NUMBER_ID']?></td>
    <td style="color:#09C; text-align:left; padding: 0px 0px 0px 10px;"><?php print $ListaBienPatrimonial['CODIGO_PATRIMONIAL'] ?></td>
    <td style="color:#09C; text-align:left; padding: 0px 0px 0px 10px;"><?php print $ListaBienPatrimonial['DENOMINACION_BIEN'] ?></td>
    <td style="color:#09C; text-align:left; padding: 0px 0px 0px 10px;"><?php print $ListaBienPatrimonial['NRO_GRUPO'].' '.$ListaBienPatrimonial['DESC_GRUPO'] ?></td>
    <td style="color:#09C; text-align:left; padding: 0px 0px 0px 10px;"><?php print $ListaBienPatrimonial['NRO_CLASE'].' '.$ListaBienPatrimonial['DESC_CLASE'] ?></td>
    <td style="color:#09C; text-align:left; padding: 0px 0px 0px 10px;"><?php print $ListaBienPatrimonial['NRO_DOCUMENTO_ADQUIS'] ?></td>
    <td style="color:#09C; text-align:center;"><?php print $ListaBienPatrimonial['FECHA_DOCUMENTO_ADQUIS'] ?></td>
    <td style="text-align:center"><a href="#" onclick="Mostrar_Dialog_Form('<?php print $ListaBienPatrimonial['COD_UE_BIEN_PATRI']?>')"><img src="../webimages/iconos/editar_texto.png" width="24" height="20" border="0" /></a></td>
  </tr>
  
<?php endforeach; ?>

<tr  onmouseover="mOvr2(this)" onmouseout="mOut2(this)">
    <td colspan="8" bgcolor="#DDE4EC">
<div id="div_paginacion" style="float:left; width: 85%; " >
<?php 
if ($pager['cantidadPaginas'] > 1): ?>
    <div class="pagination">
      <ul>
        <?php if ($pager['indicePagina'] != 1): ?>
        	<li><a href="javascript: Busqueda_Bien_Patrimonial(1)" class="paginate">Inicial</a></li>
            <li><a href="javascript: Busqueda_Bien_Patrimonial(<?php print $pager['indicePagina'] - 1; ?>)" class="paginate"><<</a></li>
        <?php endif; ?>
        <?php for ($index = $pager['limiteInferior']; $index <= $pager['limiteSuperior']; $index++): ?>
        	<?php if ($index == $pager['indicePagina']): ?>
                <li class="active"><a><?php print $index; ?></a></li>
            <?php else: ?>
            	<li><a href="javascript: Busqueda_Bien_Patrimonial(<?php print $index; ?>)" class="paginate"><?php print $index; ?></a></li>
            <?php endif; ?>
		<?php endfor; ?>
        <?php if ($pager['indicePagina'] != $pager['cantidadPaginas']): ?>
        	<li><a href="javascript: Busqueda_Bien_Patrimonial(<?php print $pager['indicePagina'] + 1; ?>)" class="paginate">>></a></li>
            <li><a href="javascript: Busqueda_Bien_Patrimonial(<?php print $pager['cantidadPaginas']; ?>)" class="paginate">Final</a></li>
        <?php endif; ?>
      </ul>
    </div>
<?php endif; ?>
</div>
<div class="texto_arial_plomito_11_N" style="padding: 6px 0px 0px 0px">Total Registros: <?php print $pager['cantidadRegistros']; ?></div>
</td>
  </tr>
  
</table>