<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../utils/css/stylesPaginacion.css" />

<style>

.dataTables_length select {
	font-size:10px;
	background-color: #f4f4f4;
	color:#333;
	font-weight:bold;
}

.dataTables_length label {
	font-size:10px;
	font-weight:bold;
}

.dataTables_info {
	font-size:10px;
	font-weight:bold;
}

</style>

<!--
<table id="grid_personal" class="display" width="100%" cellspacing="0">
<table id="grid_personal" class="cell-border" width="100%" cellspacing="0">
<table id="grid_personal" class="display compact" width="100%" cellspacing="0">
<table id="grid_personal" class="hover" width="100%" cellspacing="0">
<table id="grid_personal" class="order-column" width="100%" cellspacing="0">
<table id="grid_personal" class="row-borde" width="100%" cellspacing="0">
<table id="grid_personal" class="stripe" width="100%" cellspacing="0">
<table id="grid_personal" class="cell-border display" width="100%" cellspacing="0">
-->
<style>
div.dataTables_length {
    float: left;
	font-size:10px;
	font-weight:bold;	
    background-color: transparent;
}

div.dataTables_filter {
    float: right;
    background-color: transparent;
}

div.dataTables_info {
    float: left;
    background-color: transparent;
	font-weight:bold;	
	font-size:10px;
}

/*
div.dataTables_paginate {
    float: right;
    background-color: transparent;
	font-weight:bold;
}
*/

div.dataTables_length,
div.dataTables_filter,
div.dataTables_paginate,
div.dataTables_info {
    padding: 6px;
}

div.dataTables_wrapper {
    background-color: transparent;
}

/* Header cells */
table thead th, tfoot th {
    text-align: center;
	background-color:#C8C8C8;
}

table thead td {
    text-align: center;
}
</style>

<table width="1083" border="0" cellspacing="3" cellpadding="0" >
  <tr>
    <td ><div class="Titulo_02_19px"> <img src="../webimages/iconos/btn_admin_55.png" width="58" height="58" align="absmiddle" /> Reporte de Cuenta Contable</div></td>
  </tr>
  <tr>
    <td ><hr /></td>
  </tr>
    <tr>
      <td valign="top">&nbsp;</td>
    </tr>
    <tr>
      <td width="1077" valign="top"><div id="div_tabs" >
        <ul>
      <li><a href="#tabs-1">Reporte</a></li>
      </ul>
    
    <div id="tabs-1">
       <div class="select" id="Busqueda_Resultado_Personales" style="padding:10px 8px 10px 20px; width:1045px;">

<table id="grid_CtaCble" class="display nowrap cell-border" width="100%" cellspacing="0">
        <thead>
            <tr>
              <th height="30" colspan="5" style="text-align:center"><h2>REPORTE DE CUENTA CONTABLE - BIENES ACTIVOS</h2></th>
              </tr>
            <tr>
                <th>Item</th>
                <th>Nro Cuenta</th>
                <th>Cuenta Contable</th>
                <th>Valor Adquisición</th>
                <th>Valor Neto</th>
                </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Item</th>
                <th>Nro Cuenta</th>
                <th>Cuenta Contable</th>
                <th>Valor Adquisición</th>
                <th>Valor Neto</th>
                </tr>
        </tfoot>
    </table>

    </div>
      
      </div>
    
    
   
    
  </div>
      </td>
    </tr>
</table>
<script type="text/javascript" >
    <?php include_once('js/script.js') ?>
</script>