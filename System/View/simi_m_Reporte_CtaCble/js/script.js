$(function() {
    $("#div_tabs").tabs();
	Mostrar_Grid();
});


function Mostrar_Grid() {
	var xCE = $("#TXH_SIMI_COD_ENTIDAD").val();
	//var Filas = 25;
	
	var grid = $('#grid_CtaCble').DataTable( {
		"lengthMenu": [[30, 50, 100], [30, 50, 100]],
		"information" : false,
		"destroy": true,
    	"searching": false,
        "scrollX": true,
		"language": {"url": "../utils/javascript/Spanish.json"},
		"processing": true,
        "serverSide": true,
		"ajax": {
            "url": "View/simi_m_Reporte_CtaCble/principal.php?operacion=Mostrar_Grid_CtaContable&xCE="+xCE
		},
				
        
		//dom: 'Bfrtip',
		//dom: 'B<"clear">lfrtip',
		//"dom": 'B<"wrapper"lfrtip>',
		"dom": 'Brt<"bottom"flp><"top"i><"clear">',
		//buttons: ['copy', 'csv', 'excel', 'pdf', 'print'],
		buttons: [           
			{ text: 'Copiar', extend: 'copyHtml5', footer: true },
            { text: 'Excel', extend: 'excelHtml5', footer: true },
            { text: 'CSV', extend: 'csvHtml5', footer: true, 
				exportOptions: {
                	modifier: {search: 'none'}
            	}
			},
			
			{ text: 'PDF', extend: 'pdfHtml5', footer: true, download: 'open'},
			
			{ text: 'Imprimir', extend: 'print', footer: true, autoPrint: false, 
				exportOptions: {columns: ':visible'},
            	customize: function (win) {
                	$(win.document.body).find('tr:nth-child(odd) td').each(function(index){
                    	$(this).css('background-color','#f4f4f4');
                	});
                	$(win.document.body).find('h1').css('text-align','center').html('Reporte Cuenta Contable');
					//.prepend('<img src="url" style="position:absolute; top:0; left:0;" />');
            	}
			}			
        ]
		
    });
	
	
	$('#grid_CtaCble tbody').on( 'click', 'tr', function () {
        $('#grid_CtaCble tbody tr').removeClass('selected');
		$(this).toggleClass('selected');
    } );

			 
}
	

	
