<?

require_once ('../../Controller/C_Interconexion_SQL.php');
require_once('../../Controller/C_simi_formato_ficha_tecnica_vehicular_Controller.php');
require_once ('../../Model/M_simi_formato_ficha_tecnica_vehicular_Model.php');

require_once('../../../utils/funciones/funciones.php');
require_once('../../../utils/fpdf181/fpdf.php');




$ObjController = new C_simi_formato_ficha_tecnica_vehicular_Controller;

$OPT = $_GET['OPT'];


if($OPT == 'VistaPDF'){
	
	
	$COD_UE_FT_VEHICULAR = $_GET['codFicha'];
	
	if($COD_UE_FT_VEHICULAR !=''){
		
		
		$Resultados	= $ObjController->F_Listar_Ficha_Tecnica_Vehicular($COD_UE_FT_VEHICULAR); 
		
		
		//DATOS DEVUELTOS
		$COD_UE_FT_VEHICULAR	= odbc_result($Resultados,'COD_UE_FT_VEHICULAR');
		$COD_ENTIDAD			=utf8_encode(odbc_result($Resultados,'COD_ENTIDAD'));
		$NOM_ENTIDAD			=utf8_encode(odbc_result($Resultados,'NOM_ENTIDAD'));
		$COD_UE_BIEN_PATRI		=utf8_encode(odbc_result($Resultados,'COD_UE_BIEN_PATRI'));
		$DENOMINACION			=utf8_encode(odbc_result($Resultados,'DENOMINACION'));
		$PLACA					=utf8_encode(odbc_result($Resultados,'PLACA'));
		$CARROCERIA				=utf8_encode(odbc_result($Resultados,'CARROCERIA'));
		$MARCA					=utf8_encode(odbc_result($Resultados,'MARCA'));
		$MODELO					=utf8_encode(odbc_result($Resultados,'MODELO'));
		$CATEGORIA				=utf8_encode(odbc_result($Resultados,'CATEGORIA'));
		$NRO_CHASIS				=utf8_encode(odbc_result($Resultados,'NRO_CHASIS'));
		$NRO_EJES				=utf8_encode(odbc_result($Resultados,'NRO_EJES'));
		$NRO_MOTOR				=utf8_encode(odbc_result($Resultados,'NRO_MOTOR'));
		$NRO_SERIE				=utf8_encode(odbc_result($Resultados,'NRO_SERIE'));
		$ANIO_FABRICACION		=utf8_encode(odbc_result($Resultados,'ANIO_FABRICACION'));
		$COLOR					=utf8_encode(odbc_result($Resultados,'COLOR'));
		$COMBUSTIBLE			=utf8_encode(odbc_result($Resultados,'COMBUSTIBLE'));
		$TRANSMISION			=utf8_encode(odbc_result($Resultados,'TRANSMISION'));
		$CILINDRADA				=utf8_encode(odbc_result($Resultados,'CILINDRADA'));
		$KILOMETRAJE			=utf8_encode(odbc_result($Resultados,'KILOMETRAJE'));
		$TARJETA_PROPIEDAD		=utf8_encode(odbc_result($Resultados,'TARJETA_PROPIEDAD'));
		$CILINDROS				=utf8_encode(odbc_result($Resultados,'CILINDROS'));
		$B_CILINDROS			=utf8_encode(odbc_result($Resultados,'B_CILINDROS'));
		$CARBURADOR				=utf8_encode(odbc_result($Resultados,'CARBURADOR'));
		$B_CARBURADOR			=utf8_encode(odbc_result($Resultados,'B_CARBURADOR'));
		$DISTRIBUIDOR			=utf8_encode(odbc_result($Resultados,'DISTRIBUIDOR'));
		$B_DISTRIBUIDOR			=utf8_encode(odbc_result($Resultados,'B_DISTRIBUIDOR'));
		$BOMBA_GASOLINA			=utf8_encode(odbc_result($Resultados,'BOMBA_GASOLINA'));
		$B_BOMBA_GASOLINA		=utf8_encode(odbc_result($Resultados,'B_BOMBA_GASOLINA'));
		$PURIFICADOR_AIRE		=utf8_encode(odbc_result($Resultados,'PURIFICADOR_AIRE'));
		$B_PURIFICADOR_AIRE		=utf8_encode(odbc_result($Resultados,'B_PURIFICADOR_AIRE'));
		$BOMBA_FRENOS			=utf8_encode(odbc_result($Resultados,'BOMBA_FRENOS'));
		$B_BOMBA_FRENOS			=utf8_encode(odbc_result($Resultados,'B_BOMBA_FRENOS'));
		$ZAPATAS_TAMBORES		=utf8_encode(odbc_result($Resultados,'ZAPATAS_TAMBORES'));
		$B_ZAPATAS_TAMBORES		=utf8_encode(odbc_result($Resultados,'B_ZAPATAS_TAMBORES'));
		$DISCOS_PASTILLAS		=utf8_encode(odbc_result($Resultados,'DISCOS_PASTILLAS'));
		$B_DISCOS_PASTILLAS		=utf8_encode(odbc_result($Resultados,'B_DISCOS_PASTILLAS'));
		$RADIOADOR				=utf8_encode(odbc_result($Resultados,'RADIOADOR'));
		$B_RADIOADOR			=utf8_encode(odbc_result($Resultados,'B_RADIOADOR'));
		$VENTILADOR				=utf8_encode(odbc_result($Resultados,'VENTILADOR'));
		$B_VENTILADOR			=utf8_encode(odbc_result($Resultados,'B_VENTILADOR'));
		$BOMBA_AGUA				=utf8_encode(odbc_result($Resultados,'BOMBA_AGUA'));
		$B_BOMBA_AGUA			=utf8_encode(odbc_result($Resultados,'B_BOMBA_AGUA'));
		$MOTOR_ARRANQUE			=utf8_encode(odbc_result($Resultados,'MOTOR_ARRANQUE'));
		$B_MOTOR_ARRANQUE		=utf8_encode(odbc_result($Resultados,'B_MOTOR_ARRANQUE'));
		$BATERIA				=utf8_encode(odbc_result($Resultados,'BATERIA'));
		$B_BATERIA				=utf8_encode(odbc_result($Resultados,'B_BATERIA'));
		$ALTERNADOR				=utf8_encode(odbc_result($Resultados,'ALTERNADOR'));
		$B_ALTERNADOR			=utf8_encode(odbc_result($Resultados,'B_ALTERNADOR'));
		$BOBINA					=utf8_encode(odbc_result($Resultados,'BOBINA'));
		$B_BOBINA				=utf8_encode(odbc_result($Resultados,'B_BOBINA'));
		$RELAY_ALTERNADOR		=utf8_encode(odbc_result($Resultados,'RELAY_ALTERNADOR'));
		$B_RELAY_ALTERNADOR		=utf8_encode(odbc_result($Resultados,'B_RELAY_ALTERNADOR'));
		$FAROS_DELANTEROS		=utf8_encode(odbc_result($Resultados,'FAROS_DELANTEROS'));
		$B_FAROS_DELANTEROS		=utf8_encode(odbc_result($Resultados,'B_FAROS_DELANTEROS'));
		$DIRECCIONALES_DELANTERAS	=utf8_encode(odbc_result($Resultados,'DIRECCIONALES_DELANTERAS'));
		$B_DIRECCIONALES_DELANTERAS	=utf8_encode(odbc_result($Resultados,'B_DIRECCIONALES_DELANTERAS'));
		$LUCES_POSTERIORES		=utf8_encode(odbc_result($Resultados,'LUCES_POSTERIORES'));
		$B_LUCES_POSTERIORES	=utf8_encode(odbc_result($Resultados,'B_LUCES_POSTERIORES'));
		$DIRECCIONALES_POSTERIORES	=utf8_encode(odbc_result($Resultados,'DIRECCIONALES_POSTERIORES'));
		$B_DIRECCIONALES_POSTERIORES=utf8_encode(odbc_result($Resultados,'B_DIRECCIONALES_POSTERIORES'));
		$AUTO_RADIO				=utf8_encode(odbc_result($Resultados,'AUTO_RADIO'));
		$B_AUTO_RADIO			=utf8_encode(odbc_result($Resultados,'B_AUTO_RADIO'));
		$PARLANTES				=utf8_encode(odbc_result($Resultados,'PARLANTES'));
		$B_PARLANTES			=utf8_encode(odbc_result($Resultados,'B_PARLANTES'));
		$CLAXON					=utf8_encode(odbc_result($Resultados,'CLAXON'));
		$B_CLAXON				=utf8_encode(odbc_result($Resultados,'B_CLAXON'));
		$CIRCUITO_LUCES			=utf8_encode(odbc_result($Resultados,'CIRCUITO_LUCES'));
		$B_CIRCUITO_LUCES		=utf8_encode(odbc_result($Resultados,'B_CIRCUITO_LUCES'));
		$CAJA_CAMBIOS			=utf8_encode(odbc_result($Resultados,'CAJA_CAMBIOS'));
		$B_CAJA_CAMBIOS			=utf8_encode(odbc_result($Resultados,'B_CAJA_CAMBIOS'));
		$BOMBA_EMBRAGUE			=utf8_encode(odbc_result($Resultados,'BOMBA_EMBRAGUE'));
		$B_BOMBA_EMBRAGUE		=utf8_encode(odbc_result($Resultados,'B_BOMBA_EMBRAGUE'));
		$CAJA_TRANSFERENCIA		=utf8_encode(odbc_result($Resultados,'CAJA_TRANSFERENCIA'));
		$B_CAJA_TRANSFERENCIA	=utf8_encode(odbc_result($Resultados,'B_CAJA_TRANSFERENCIA'));
		$DIFERENCIAL_TRASERO	=utf8_encode(odbc_result($Resultados,'DIFERENCIAL_TRASERO'));
		$B_DIFERENCIAL_TRASERO	=utf8_encode(odbc_result($Resultados,'B_DIFERENCIAL_TRASERO'));
		$DIFERENCIAL_DELANTERO	=utf8_encode(odbc_result($Resultados,'DIFERENCIAL_DELANTERO'));
		$B_DIFERENCIAL_DELANTERO=utf8_encode(odbc_result($Resultados,'B_DIFERENCIAL_DELANTERO'));
		$VOLANTE				=utf8_encode(odbc_result($Resultados,'VOLANTE'));
		$B_VOLANTE				=utf8_encode(odbc_result($Resultados,'B_VOLANTE'));
		$CANA_DIRECCION			=utf8_encode(odbc_result($Resultados,'CANA_DIRECCION'));
		$B_CANA_DIRECCION		=utf8_encode(odbc_result($Resultados,'B_CANA_DIRECCION'));
		$CREMALLERA				=utf8_encode(odbc_result($Resultados,'CREMALLERA'));
		$B_CREMALLERA			=utf8_encode(odbc_result($Resultados,'B_CREMALLERA'));
		$ROTULAS				=utf8_encode(odbc_result($Resultados,'ROTULAS'));
		$B_ROTULAS				=utf8_encode(odbc_result($Resultados,'B_ROTULAS'));
		$AMORTIGUADORES			=utf8_encode(odbc_result($Resultados,'AMORTIGUADORES'));
		$B_AMORTIGUADORES		=utf8_encode(odbc_result($Resultados,'B_AMORTIGUADORES'));
		$BARRA_TORSION			=utf8_encode(odbc_result($Resultados,'BARRA_TORSION'));
		$B_BARRA_TORSION		=utf8_encode(odbc_result($Resultados,'B_BARRA_TORSION'));
		$BARRA_ESTABILIZADOR	=utf8_encode(odbc_result($Resultados,'BARRA_ESTABILIZADOR'));
		$B_BARRA_ESTABILIZADOR	=utf8_encode(odbc_result($Resultados,'B_BARRA_ESTABILIZADOR'));
		$LLANTAS				=utf8_encode(odbc_result($Resultados,'LLANTAS'));
		$B_LLANTAS				=utf8_encode(odbc_result($Resultados,'B_LLANTAS'));
		$CAPOT_MOTOR			=utf8_encode(odbc_result($Resultados,'CAPOT_MOTOR'));
		$B_CAPOT_MOTOR			=utf8_encode(odbc_result($Resultados,'B_CAPOT_MOTOR'));
		$CAPOT_MALETERA			=utf8_encode(odbc_result($Resultados,'CAPOT_MALETERA'));
		$B_CAPOT_MALETERA		=utf8_encode(odbc_result($Resultados,'B_CAPOT_MALETERA'));
		$PARACHOQUES_DELANTEROS	=utf8_encode(odbc_result($Resultados,'PARACHOQUES_DELANTEROS'));
		$B_PARACHOQUES_DELANTEROS	=utf8_encode(odbc_result($Resultados,'B_PARACHOQUES_DELANTEROS'));
		$PARACHOQUES_POSTERIORES	=utf8_encode(odbc_result($Resultados,'PARACHOQUES_POSTERIORES'));
		$B_PARACHOQUES_POSTERIORES	=utf8_encode(odbc_result($Resultados,'B_PARACHOQUES_POSTERIORES'));
		$LUNAS_LATERALES		=utf8_encode(odbc_result($Resultados,'LUNAS_LATERALES'));
		$B_LUNAS_LATERALES		=utf8_encode(odbc_result($Resultados,'B_LUNAS_LATERALES'));
		$LUNAS_CORTAVIENTO		=utf8_encode(odbc_result($Resultados,'LUNAS_CORTAVIENTO'));
		$B_LUNAS_CORTAVIENTO	=utf8_encode(odbc_result($Resultados,'B_LUNAS_CORTAVIENTO'));
		$PARABRISAS_DELANTERO	=utf8_encode(odbc_result($Resultados,'PARABRISAS_DELANTERO'));
		$B_PARABRISAS_DELANTERO	=utf8_encode(odbc_result($Resultados,'B_PARABRISAS_DELANTERO'));
		$PARABRISAS_POSTERIOR	=utf8_encode(odbc_result($Resultados,'PARABRISAS_POSTERIOR'));
		$B_PARABRISAS_POSTERIOR	=utf8_encode(odbc_result($Resultados,'B_PARABRISAS_POSTERIOR'));
		$TANQUE_COMBUSTIBLE		=utf8_encode(odbc_result($Resultados,'TANQUE_COMBUSTIBLE'));
		$B_TANQUE_COMBUSTIBLE	=utf8_encode(odbc_result($Resultados,'B_TANQUE_COMBUSTIBLE'));
		$PUERTAS				=utf8_encode(odbc_result($Resultados,'PUERTAS'));
		$B_PUERTAS				=utf8_encode(odbc_result($Resultados,'B_PUERTAS'));
		$ASIENTOS				=utf8_encode(odbc_result($Resultados,'ASIENTOS'));
		$B_ASIENTOS				=utf8_encode(odbc_result($Resultados,'B_ASIENTOS'));
		$AIRE_ACONDICIONADO		=utf8_encode(odbc_result($Resultados,'AIRE_ACONDICIONADO'));
		$B_AIRE_ACONDICIONADO	=utf8_encode(odbc_result($Resultados,'B_AIRE_ACONDICIONADO'));
		$ALARMA					=utf8_encode(odbc_result($Resultados,'ALARMA'));
		$B_ALARMA				=utf8_encode(odbc_result($Resultados,'B_ALARMA'));
		$PLUMILLAS				=utf8_encode(odbc_result($Resultados,'PLUMILLAS'));
		$B_PLUMILLAS			=utf8_encode(odbc_result($Resultados,'B_PLUMILLAS'));
		$ESPEJOS				=utf8_encode(odbc_result($Resultados,'ESPEJOS'));
		$B_ESPEJOS				=utf8_encode(odbc_result($Resultados,'B_ESPEJOS'));
		$CINTURONES_SEGURIDAD	=utf8_encode(odbc_result($Resultados,'CINTURONES_SEGURIDAD'));
		$B_CINTURONES_SEGURIDAD	=utf8_encode(odbc_result($Resultados,'B_CINTURONES_SEGURIDAD'));
		$ANTENA					=utf8_encode(odbc_result($Resultados,'ANTENA'));
		$B_ANTENA				=utf8_encode(odbc_result($Resultados,'B_ANTENA'));
		$OTRAS_CARACTERISTICAS_RELEVANTES=utf8_encode(odbc_result($Resultados,'OTRAS_CARACTERISTICAS_RELEVANTES'));
		$VALOR_TASACION			=utf8_encode(odbc_result($Resultados,'VALOR_TASACION'));
		$ID_ESTADO				=utf8_encode(odbc_result($Resultados,'ID_ESTADO'));
		$SIMI_USUARIO_CREACION	=utf8_encode(odbc_result($Resultados,'SIMI_USUARIO_CREACION'));
		$SIMI_FECHA_CREACION	=utf8_encode(odbc_result($Resultados,'SIMI_FECHA_CREACION'));
		$SIMI_USUARIO_MODIFICA	=utf8_encode(odbc_result($Resultados,'SIMI_USUARIO_MODIFICA'));
		$SIMI_FECHA_MODIFICA	=utf8_encode(odbc_result($Resultados,'SIMI_FECHA_MODIFICA'));
		$SIMI_USUARIO_ELIMINA	=utf8_encode(odbc_result($Resultados,'SIMI_USUARIO_ELIMINA'));
		$SIMI_FECHA_ELIMINA		=utf8_encode(odbc_result($Resultados,'SIMI_FECHA_ELIMINA'));
		$FECHA_REGISTRO			=utf8_encode(odbc_result($Resultados,'FECHA_REGISTRO'));


	}

	
	class PDF extends FPDF{
		// Cabecera de página
		function Header(){
			
			// Logo	
			$this->Image('../../../webimages/iconos/sbn_11.png',10,5,18);//ancho,alto
	
			//$this->MultiCell(42, 5, 'Superintendencia Nacional de Bienes Estatales', 0, 1);
			$this->SetFont('Arial','',9);
			
			$this->Ln(0);
			$this->Cell(19);
			$this->Cell(1,1,'Superintendencia Nacional',0,0,'L');
			
			$this->Cell(294,1,utf8_decode('Página : ').$this->PageNo().'/{nb}',0,0,'C');
			$this->Ln(4);
			$this->Cell(19);
			$this->Cell(1,1,'de Bienes Estatales',0,0,'L');
			$this->Cell(300,1,utf8_decode('Reporte : '.date('d/m/Y')),0,0,'C');
			
			//$this->MultiCell(42, 5, 'Superintendencia Nacional de Bienes Estatales', 0, 1);
			
			$this->Cell(150);
			
			
			//$this->Ln(-1);
			$this->SetFont('Arial','B',10);		
			$this->Ln(-5);
			//$this->Cell(18);
			$this->Cell(0,1,utf8_decode('ANEXO Nª 5'),0,0,'C');
			
			$this->Ln(6);
			$this->SetFont('Arial','B',10);
			$this->Cell(0,1,utf8_decode('FORMATO DE FICHA TÉCNICA DEL VEHICULO'),0,0,'C');
			
	
			//Dibujamos una linea
			$this->Line(10, 20, 196, 20);
			// Salto de línea
			$this->Ln(5);
				
		}
		
		// Pie de página
		function Footer(){
			// Posición: a 1,5 cm del final
			
			
			
		}
		

		
		// Tabla coloreada
		function FancyTable($header, $data){
			// Colores, ancho de línea y fuente en negrita
			$this->SetFillColor(255,0,0);
			$this->SetTextColor(255);
			$this->SetDrawColor(128,0,0);
			$this->SetLineWidth(.3);
			$this->SetFont('','B');
			// Cabecera
			$w = array(40, 35, 45, 40);
			for($i=0;$i<count($header);$i++)
				$this->Cell($w[$i],7,$header[$i],1,0,'C',true);
			$this->Ln();
			// Restauración de colores y fuentes
			$this->SetFillColor(224,235,255);
			$this->SetTextColor(0);
			$this->SetFont('');
			// Datos
			$fill = false;
			foreach($data as $row){
				$this->Cell($w[0],6,$row[0],'LR',0,'L',$fill);
				$this->Cell($w[1],6,$row[1],'LR',0,'L',$fill);
				$this->Cell($w[2],6,number_format($row[2]),'LR',0,'R',$fill);
				$this->Cell($w[3],6,number_format($row[3]),'LR',0,'R',$fill);
				$this->Ln();
				$fill = !$fill;
			}
			// Línea de cierre
			$this->Cell(array_sum($w),0,'','T');
		}
		
	}
	
	
	
		// Creación del objeto de la clase heredada
		//$oFPDF = new FPDF();
		//$oFPDF = new FPDF('P', 'mm', 'A4'); //-> oficio
		//$oFPDF = new FPDF('P', 'mm', 'A4'); //-> oficio
		//$oFPDF = new FPDF('P','mm',array(600,150));
	
	
		// Creación del objeto de la clase heredada
		$pdf = new PDF();
		//$pdf = new FPDF('P', 'mm', 'A4'); //-> oficio
		$pdf->AliasNbPages();

		$pdf->SetFont('Arial','',10);
		$pdf->AddPage();
		$pdf->Ln(4);

		//****************************************
		// Títulos DATOS DE LA ALTA
		//****************************************
				
		$pdf->SetFillColor(205,205,205);//color de fondo tabla
		$pdf->SetTextColor(10);
		$pdf->SetDrawColor(153,153,153);
		$pdf->SetLineWidth(.3);
		$pdf->SetFont('Arial','B',9);
		
		
		/*
		$pdf->SetFillColor(205,205,205);
		$pdf->Cell(42,5,utf8_decode('FICHA TÉCNICA DEL VEHICULO'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(35,5,utf8_decode($xNRO_DOC_ALTA),1,0,'L',true);
		*/
		
		$pdf->SetFont('Arial','B',9);
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(184.5,5,utf8_decode('FICHA TÉCNICA DEL VEHICULO'),1,0,'C',true);
		
		
		
		$pdf->Ln();
		
						
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('ENTIDAD'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(138.2,5,utf8_decode($NOM_ENTIDAD),1,0,'L',true);
		
		$pdf->Ln();

		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('Nº DE MOTOR'),1,0,'L',true);
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(46,5,utf8_decode($NRO_MOTOR),1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('DENOMINACIÓN'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(46,5,utf8_decode($DENOMINACION),1,0,'L',true);
		
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('Nº DE SERIE'),1,0,'L',true);
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(46,5,utf8_decode($NRO_SERIE),1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('PLACA'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(46,5,utf8_decode($PLACA),1,0,'L',true);
		
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('AÑO DE FABRICACIÓN'),1,0,'L',true);
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(46,5,utf8_decode($ANIO_FABRICACION),1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('CARROCERÍA'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(46,5,utf8_decode($CARROCERIA),1,0,'L',true);
		
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('COLOR'),1,0,'L',true);
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(46,5,utf8_decode($COLOR),1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('MARCA'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(46,5,utf8_decode($MARCA),1,0,'L',true);
		
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('COMBUSTIBLE'),1,0,'L',true);
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(46,5,utf8_decode($COMBUSTIBLE),1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('MODELO'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(46,5,utf8_decode($MODELO),1,0,'L',true);
		
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('TRANSMISIÓN'),1,0,'L',true);
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(46,5,utf8_decode($TRANSMISION),1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('CATEGORÍA'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(46,5,utf8_decode($CATEGORIA),1,0,'L',true);
		
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('CILINDRADA'),1,0,'L',true);
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(46,5,utf8_decode($CILINDRADA),1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('Nª DE CHASIS (VIN)'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(46,5,utf8_decode($NRO_CHASIS),1,0,'L',true);
		
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('KILOMETRAJE'),1,0,'L',true);
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(46,5,utf8_decode($KILOMETRAJE),1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('Nº DE EJES'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(46,5,utf8_decode($NRO_EJES),1,0,'L',true);
		
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(46.25,5,utf8_decode('TARJETA DE PROPIEDAD'),1,0,'L',true);
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(46,5,utf8_decode($TARJETA_PROPIEDAD),1,0,'L',true);
		
		$pdf->Ln();
		
		
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(92,5,utf8_decode('DESCRIPCIÓN'),1,0,'C',true);		
		
		
		
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(92.5,5,utf8_decode('APRECIACIÓN TÉCNICA DEL SISTEMA'),1,0,'C',true);		
		
		$pdf->Ln();
		
		//TITULOS
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(184.5,5,utf8_decode('1. SISTEMA DE MOTOR'),1,0,'L',true);	
		
		$pdf->ln();
				
		
		//DATOS
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Cilindros'),1,0,'L',true);
		
		$pdf->SetFillColor(255,255,255);		
		if ($B_CILINDROS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		//$pdf->Image('check.png',50,70,6);
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($CILINDROS),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Carburador / carter'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_CARBURADOR==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($CARBURADOR),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Distribuidor / bomba de inyección'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_DISTRIBUIDOR==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($DISTRIBUIDOR),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Bomba de gasolina'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_BOMBA_GASOLINA==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($BOMBA_GASOLINA),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Purificador de aire'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_PURIFICADOR_AIRE==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($PURIFICADOR_AIRE),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		
		//TITULOS
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(184.5,5,utf8_decode('2. SISTEMA DE FRENOS'),1,0,'L',true);	

				
		
		$pdf->ln();
		
		//DATOS
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Bomba de frenos'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_BOMBA_FRENOS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($BOMBA_FRENOS),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Zapatas y tambores'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_ZAPATAS_TAMBORES==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($ZAPATAS_TAMBORES),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Discos y pastillas'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_DISCOS_PASTILLAS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($DISCOS_PASTILLAS),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		
		//TITULOS
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(184.5,5,utf8_decode('3. SISTEMA DE REFRIGERACIÓN'),1,0,'L',true);	
		
		$pdf->ln();
		
		//DATOS
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Radiador'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_RADIOADOR==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($RADIOADOR),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Ventilador'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_VENTILADOR==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($VENTILADOR),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Bomba de Agua'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_BOMBA_AGUA==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($BOMBA_AGUA),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		
		//TITULOS
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(184.5,5,utf8_decode('4. SISTEMA ELÉCTRICO'),1,0,'L',true);	
		
		$pdf->ln();
		
		
		//DATOS
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Motor de arranque'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_MOTOR_ARRANQUE==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($MOTOR_ARRANQUE),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Bateria'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_BATERIA==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($BATERIA),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Alternador'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_ALTERNADOR==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($ALTERNADOR),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Bobina'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_BOBINA==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($BOBINA),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Relay de alternador'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_RELAY_ALTERNADOR==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($RELAY_ALTERNADOR),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Faros delanteros'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_FAROS_DELANTEROS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($FAROS_DELANTEROS),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Direccionales delanteras'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_DIRECCIONALES_DELANTERAS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($DIRECCIONALES_DELANTERAS),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Luces posteriores'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_LUCES_POSTERIORES==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($LUCES_POSTERIORES),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Direccionales posteriores'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_DIRECCIONALES_POSTERIORES==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($DIRECCIONALES_POSTERIORES),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Auto radio'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_AUTO_RADIO==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($AUTO_RADIO),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Parlantes'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_PARLANTES==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($PARLANTES),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Claxon'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_CLAXON==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($CLAXON),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Circuito de luces (faros, cableados)'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_CIRCUITO_LUCES==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($CIRCUITO_LUCES),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		
		//TITULOS
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(184.5,5,utf8_decode('5. SISTEMA DE TRANSMISIÓN'),1,0,'L',true);	
		
		$pdf->ln();
		
		//DATOS
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Caja de cambios'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_CAJA_CAMBIOS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($CAJA_CAMBIOS),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Bomba de embrague'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_BOMBA_EMBRAGUE==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($BOMBA_EMBRAGUE),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Caja de transferencia'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_CAJA_TRANSFERENCIA==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($CAJA_TRANSFERENCIA),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Diferencial trasero'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		if ($B_DIFERENCIAL_TRASERO==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}		
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($DIFERENCIAL_TRASERO),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Diferencial delantero (4X4)'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_DIFERENCIAL_DELANTERO==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}		
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($DIFERENCIAL_DELANTERO),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		//TITULOS
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(184.5,5,utf8_decode('6. SISTEMA DE DIRECCIÓN'),1,0,'L',true);	
		
		$pdf->ln();
		
		//DATOS
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Volante'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_VOLANTE==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}		
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($VOLANTE),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Caña de dirección'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_CANA_DIRECCION==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}		
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($CANA_DIRECCION),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Cremallera'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_CREMALLERA==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}	
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($CREMALLERA),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Rotulas'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_ROTULAS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}	
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($ROTULAS),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		//TITULOS
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(184.5,5,utf8_decode('7. SISTEMA DE SUSPENSIÓN'),1,0,'L',true);	
		
		$pdf->ln();
		
		//DATOS
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Amortiguadores / muelles'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_AMORTIGUADORES==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($AMORTIGUADORES),1,0,'L',true);	
		$pdf->Ln();
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Barra de torsión'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_BARRA_TORSION==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($BARRA_TORSION),1,0,'L',true);	
		$pdf->Ln();
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Barra estabilizadora'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		if ($B_BARRA_ESTABILIZADOR==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($BARRA_ESTABILIZADOR),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Llantas'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_LLANTAS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($LLANTAS),1,0,'L',true);	
		$pdf->Ln();
		
		
		//TITULOS
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(184.5,5,utf8_decode('8. CARROCERÍA'),1,0,'L',true);	

		
		$pdf->ln();
		
		//DATOS
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Capot del motor'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		if ($B_CAPOT_MOTOR==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		

		$pdf->Cell(118.2,5,utf8_decode($CAPOT_MOTOR),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Capot de maletera'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		if ($B_CAPOT_MALETERA==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}		
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($CAPOT_MALETERA),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Parachoques delanteros'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		if ($B_PARACHOQUES_DELANTEROS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}		
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($PARACHOQUES_DELANTEROS),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Parachoques posterior'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		if ($B_PARACHOQUES_POSTERIORES==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}		
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($PARACHOQUES_POSTERIORES),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Lunas laterales'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		if ($B_LUNAS_LATERALES==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($LUNAS_LATERALES),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Lunas cortaviento'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		if ($B_LUNAS_CORTAVIENTO==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($LUNAS_CORTAVIENTO),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Parabrisas delanteros'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_PARABRISAS_DELANTERO==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($PARABRISAS_DELANTERO),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Parabrisas posterior'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_PARABRISAS_POSTERIOR==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($PARABRISAS_POSTERIOR),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Tanque de combustible'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		if ($B_TANQUE_COMBUSTIBLE==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($TANQUE_COMBUSTIBLE),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Puertas'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_PUERTAS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($PUERTAS),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Asientos'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_ASIENTOS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($ASIENTOS),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		//TITULOS
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(184.5,5,utf8_decode('9. ACCESORIOS'),1,0,'L',true);	
		
		$pdf->ln();
		
		//DATOS
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Aire acondicionado'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		if ($B_AIRE_ACONDICIONADO==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}		
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($AIRE_ACONDICIONADO),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Alarma'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_ALARMA==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($ALARMA),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Plumillas'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_PLUMILLAS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($PLUMILLAS),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Espejos'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_ESPEJOS==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($ESPEJOS),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Cinturones de seguridad'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		if ($B_CINTURONES_SEGURIDAD==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}		
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($CINTURONES_SEGURIDAD),1,0,'L',true);	
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Antena'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		if ($B_ANTENA==1)
		{
			$pdf->Cell(20,5,utf8_decode("SI"),1,0,'L',true);
			
		}else{
			$pdf->Cell(20,5,utf8_decode("NO"),1,0,'L',true);
		}	
		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(118.2,5,utf8_decode($ANTENA),1,0,'L',true);	
		$pdf->Ln();
		
		
		
		//TITULOS
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(184.5,5,utf8_decode('10. OTRAS CARACTERÍSTICAS RELEVANTES'),1,0,'L',true);	

		
		$pdf->ln();
		
		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(184.5,5,utf8_decode($OTRAS_CARACTERISTICAS_RELEVANTES),1,0,'L',true);	
		
		$pdf->ln();
		
		
		//TITULOS
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(184.5,5,utf8_decode('APRECIACIÓN TÉCNICA GENERAL(*)'),1,0,'L',true);	
		
		$pdf->ln();
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(46.25,5,utf8_decode('Valor de tasación (S/.)'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(138.3,5,utf8_decode($VALOR_TASACION),1,0,'L',true);
		
		
		$pdf->Ln();
		
		
		$pdf->Ln();
		$pdf->Ln(3);


		/*  PIE DE INFORME */
			$pdf->SetY(-60);
			$pdf->SetFont('Arial','',7);
			
			$pdf->Ln();
			$pdf->Ln(8);
			
			
			$pdf->Cell(30);
			$pdf->Cell(1,1,'_______________________________________________',0,0,'L');	
			
			$pdf->Cell(70);
			$pdf->Cell(1,1,'_______________________________________________',0,0,'L');
			
			$pdf->Ln(5);
			
			$pdf->Cell(50);
			$pdf->Cell(1,1,utf8_decode('Responsable de la UCP'),0,0,'L');
			
			$pdf->Cell(70);
			$pdf->Cell(1,1,utf8_decode('Mecánico o Especialista'),0,0,'L');
			
			$pdf->Ln(10);
			
			$pdf->Cell(10);
			$pdf->Cell(1,1,utf8_decode('(*) Si la Apreciación Técnica General indica ser un vehículo en calidad de chatarra, se dispondrá conforme a los estipulado en el numeral 6.5.6 de la Directiva Nº'),0,0,'L');
			
			$pdf->Ln(5);
			$pdf->SetFont('Arial','',6);
			$pdf->Cell(10);
			$pdf->Cell(1,1,'__________/SBN.',0,0,'L');
		

		$pdf->Output();

}else{
	echo "No es la direccion Web Correcta";
}

?>


