<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="../utils/css/stylesPaginacion.css"/>

<?

require_once ('../../Controller/C_Interconexion_SQL.php');
require_once('../../Controller/C_simi_formato_ficha_tecnica_vehicular_Controller.php');
require_once ('../../Model/M_simi_formato_ficha_tecnica_vehicular_Model.php');
require_once('../../../utils/funciones/funciones.php');


$objController	=	new C_simi_formato_ficha_tecnica_vehicular_Controller;

$opt = $_GET['opt'];


if($opt == 'buscar_bien'){	

	$COD_PATRIMONIAL 		= convertir_a_utf8($_REQUEST['busc_txt_codigo_bien']);
	$DENOMINACION 			= convertir_a_utf8($_REQUEST['busc_txt_denominacion']);
	$COD_ENTIDAD 			= $_REQUEST['busc_txt_entidad'];
	
	
}else{
	$COD_PATRIMONIAL 		= '';
	$DENOMINACION 			= '';
	$COD_ENTIDAD			= $_SESSION['th_SIMI_COD_ENTIDAD'];
}

?>


<BR />
<table width="949" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" id="tbl_Resultado_Lista_Familia" >

  <tr>
    <th width="40" height="20" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:center">Item</th>
    <th width="100" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:center">Código Patrimonial</th>
    <th bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:left">Denominación</th>
    <th width="100" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:left">Fecha Adquisición</th>
    <th width="90" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:left">Valor Adquisición</th>
    <th bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:left">Valor Tasación</th>
    <th bgcolor="#f4f4f4" colspan="2" class="Titulo_01_10px" style="text-align:center">Acción</th>
  </tr>

  <?

	$pageNum = $_GET['page'];
	
	$pageNum = ($pageNum == '') ? 1: $pageNum;
	
	$LIMITE = 20;
	$TOTAL_LINK = 6;
	
	$RS = $objController->F_Total_Buscar_BienesVehiculos_Parametros_Gral($COD_ENTIDAD, $COD_PATRIMONIAL, $DENOMINACION);
	
	$TOTAL_REGISTRO = odbc_result($RS,"TOT_REG");
	
	$TOTAL_PAGINAS = ceil($TOTAL_REGISTRO / $LIMITE);
	
	$INI = ($pageNum == '1')? ($pageNum * $LIMITE - ($LIMITE)) : ($pageNum * $LIMITE - ($LIMITE))+1 ;
	$FIN = ($pageNum == '1')? ($INI + $LIMITE) : ($INI + $LIMITE)-1 ;
	

    $Result_Catalogo = $objController->F_Lista_Buscar_Bienes_Vehiculos_x_Entidad_Parametros_Gral($INI, $FIN, $COD_ENTIDAD, $COD_PATRIMONIAL, $DENOMINACION);
					if($Result_Catalogo){
						while (odbc_fetch_row($Result_Catalogo)){
							
							$ROW_NUMBER_ID	 	 = odbc_result($Result_Catalogo,"ROW_NUMBER_ID");
							$CODIGO_PATRIMONIAL	 = odbc_result($Result_Catalogo,"CODIGO_PATRIMONIAL");
							$DENOMINACION_BIEN	 = utf8_encode(odbc_result($Result_Catalogo,"DENOMINACION_BIEN"));
							$VALOR_TASACION	 	 = utf8_encode(odbc_result($Result_Catalogo,"VALOR_TASACION"));
							$COD_UE_BIEN_PATRI	 = utf8_encode(odbc_result($Result_Catalogo,"COD_UE_BIEN_PATRI"));
							$COD_UE_FT_VEHICULAR  = utf8_encode(odbc_result($Result_Catalogo,"COD_UE_FT_VEHICULAR"));
							$FECHA_DOCUMENTO_ADQUIS  = cambiaf_a_normal(utf8_encode(odbc_result($Result_Catalogo,"FECHA_DOCUMENTO_ADQUIS")));
							$VALOR_ADQUIS  = utf8_encode(odbc_result($Result_Catalogo,"VALOR_ADQUIS"));
	?>

  <tr>
    <td height="22" class="separador_borde" style="color:rgb(0, 156, 213); text-align:center"><?=$ROW_NUMBER_ID?></td>
    <td height="22" class="separador_borde" style="color:rgb(0, 156, 213); text-align:center"><?=$CODIGO_PATRIMONIAL?></td>
    <td style="color:rgb(0, 156, 213); text-align:left" class="separador_borde" ><?=$DENOMINACION_BIEN?></td>
    <td style="color:rgb(0, 156, 213); text-align:left" class="separador_borde" ><?=$FECHA_DOCUMENTO_ADQUIS?></td>
    <td style="color:rgb(0, 156, 213); text-align:right" class="separador_borde" ><?=$VALOR_ADQUIS?></td>
    <td width="100" style="color:rgb(0, 156, 213); text-align:right" class="separador_borde" ><?=$VALOR_TASACION?></td>
    <td width="20" style="color:rgb(0, 156, 213); text-align:left" class="separador_borde" ><a href="#" onclick="F_ModificarFichaVehiculos('<?=$COD_UE_BIEN_PATRI?>', <?=$COD_UE_FT_VEHICULAR?>)">
    <img src="../webimages/iconos/editar_texto.png" width="15" height="12" border="0" /></a></td>
    <td width="20" style="color:rgb(0, 156, 213); text-align:left" class="separador_borde" ><a href="#" onclick="F_EliminarFichaVehiculos('<?=$COD_UE_BIEN_PATRI?>', <?=$COD_UE_FT_VEHICULAR?>)" ><img src="../webimages/iconos/eliminar.png" width="15" height="12" border="0" /></a></td>
    
  </tr>
  <?
						}
					}
					
	?>
</table>
<table width="949" border="0" cellspacing="1" cellpadding="0" bgcolor="#f4f4f4">
  <tr>
    <td width="816"><div id="div_paginacion" >
          <?	
        if ($TOTAL_PAGINAS > 1) {
            echo '<div class="pagination">';
            echo '<ul>';
                echo '<li><a href = "javascript: Paginar_Bien('.chr(39).'1'.chr(39).')" class="paginate" >Inicial</a></li>';
                if ($pageNum != 1)
                    echo '<li><a href = "javascript: Paginar_Bien('.chr(39).($pageNum-1).chr(39).')" class="paginate" ><<</a></li>';
                    
                    $page_actual_ini = (($pageNum - $TOTAL_LINK) <= 0) ?  1 : ($pageNum - $TOTAL_LINK);
                    $page_actual_fin = (($pageNum + $TOTAL_LINK) > $TOTAL_PAGINAS) ? $TOTAL_PAGINAS : ($pageNum + $TOTAL_LINK);
                    
                        for ($i=$page_actual_ini;$i<=$page_actual_fin;$i++) {
                            if ($pageNum == $i)
                                //si muestro el índice de la página actual, no coloco enlace
                                echo '<li class="active"><a>'.$i.'</a></li>';
                            else
                                //si el índice no corresponde con la página mostrada actualmente,
                                //coloco el enlace para ir a esa página
                                echo '<li><a href = "javascript: Paginar_Bien('.chr(39).$i.chr(39).')" class="paginate" ">'.$i.'</a></li>';
                        }
                if ($pageNum != $TOTAL_PAGINAS)
                     echo '<li><a href = "javascript: Paginar_Bien('.chr(39).($pageNum+1).chr(39).')" class="paginate" >> ></a></li>';
                echo '<li><a href = "javascript: Paginar_Bien('.chr(39).$TOTAL_PAGINAS.chr(39).')" class="paginate" >Final</a></li>';
           echo '</ul>';
           echo '</div>';
        }
    
    
    ?>
    </div></td>
    <td width="130" class="texto_arial_azul_n_10">Total : 
    <?=$TOTAL_REGISTRO?></td>
  </tr>
</table>