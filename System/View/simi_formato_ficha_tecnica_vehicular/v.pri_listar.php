<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="../utils/css/stylesPaginacion.css"/>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">



<?php
$personales = $data['personales'];
$pager = $data['pager'];

?>


<table width="100%" border="0" cellpadding="0" cellspacing="0" class="TABLE_border4" >
  <tr>
    <th width="37" height="30" bgcolor="#DDE4EC" style="text-align:center">ITEM</th>
    <th width="80" bgcolor="#DDE4EC">CÓDIGO PATRIMONIAL</th>
    <th width="200" bgcolor="#DDE4EC">DENOMINACIÓN DEL BIEN</th>
    <th width="100" bgcolor="#DDE4EC">VALOR ADQUISICIÓN</th>
    <th width="45" bgcolor="#DDE4EC">CON FICHA?</th>
    <th width="45" bgcolor="#DDE4EC">REGISTRAR FICHA</th>
    <th width="45" bgcolor="#DDE4EC">IMPRIMIR FICHA</th>
  </tr>


<?php if($personales) foreach ($personales as $personal): ?>

  <tr  onmouseover="mOvr2(this)" onmouseout="mOut2(this)">
    <td style="text-align:center"><?php print $personal['ROW_NUMBER_ID']?></td>
    <td><?php print $personal['CODIGO_PATRIMONIAL'] ?></td>
    <td><?php print $personal['DENOMINACION_BIEN'] ?></td>
    <td><?php print $personal['VALOR_ADQUIS'] ?></td>
    <td><?php print ($personal['COD_UE_FT_VEHICULAR'] == 0) ? 'NO':'SI'; ?></td>
    <td>
      <button type="button" class="btn btn-default btn-lg" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false" onclick="handle_Mostrar_Form_personal(<?php print $personal['COD_UE_BIEN_PATRI'];?>, <?php print $personal['CODIGO_PATRIMONIAL'];?>, '<?php print $personal['DENOMINACION_BIEN'];?>', <?php print $personal['COD_UE_FT_VEHICULAR'];?> )">
        <span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span>
      </button>
    </td>
    <td>
      <button type="button" class="btn btn-default btn-lg" data-toggle="" data-target="#myModal" data-backdrop="static" data-keyboard="false" onclick="handle_MostrarPDF(<?php print $personal['COD_UE_FT_VEHICULAR'];?>)">
        <span class="glyphicon glyphicon-print" aria-hidden="true"></span>
      </button>
    </td>
  </tr>

<?php endforeach; ?>

<tr  onmouseover="mOvr2(this)" onmouseout="mOut2(this)">
    <td colspan="7" bgcolor="#DDE4EC">
<div id="div_paginacion" >
<?php
if ($pager['cantidadPaginas'] > 1): ?>
    <div class="pagination">
      <ul>
        <?php if ($pager['indicePagina'] != 1): ?>
        	<li><a href="javascript: handle_paginar_bienes_vehiculos(1)" class="paginate">Inicial</a></li>
            <li><a href="javascript: handle_paginar_bienes_vehiculos(<?php print $pager['indicePagina'] - 1; ?>)" class="paginate"><<</a></li>
        <?php endif; ?>
        <?php for ($index = $pager['limiteInferior']; $index <= $pager['limiteSuperior']; $index++): ?>
        	<?php if ($index == $pager['indicePagina']): ?>
                <li class="active"><a><?php print $index; ?></a></li>
            <?php else: ?>
            	<li><a href="javascript: handle_paginar_bienes_vehiculos(<?php print $index; ?>)" class="paginate"><?php print $index; ?></a></li>
            <?php endif; ?>
		<?php endfor; ?>
        <?php if ($pager['indicePagina'] != $pager['cantidadPaginas']): ?>
        	<li><a href="javascript: handle_paginar_bienes_vehiculos(<?php print $pager['indicePagina'] + 1; ?>)" class="paginate">>></a></li>
            <li><a href="javascript: handle_paginar_bienes_vehiculos(<?php print $pager['cantidadPaginas']; ?>)" class="paginate">Final</a></li>
        <?php endif; ?>
      </ul>
    </div>
<?php endif; ?>
</div>
</td>
  </tr>

</table>


<div class="container">
  <!-- Trigger the modal with a button -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ficha Técnica Vehicular</h4>
        </div>
        <div class="modal-body">
          <?php

            include_once("v.form_personal.php");

          ?>
        </div>

      </div>

    </div>
  </div>

</div>
