<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../utils/css/SGI_Estilos.css" rel="stylesheet">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">


<style>
body {font-family: "Lato", sans-serif;}

/* Style the tab */
div.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
div.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.5s;
    font-size: 12px;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
div.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
</style>

<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script type="text/javascript">

function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();






</script>


<?php
$DatosPersonal = $data['DatPersonal'];


?>

<div id="div_1" class="modal-content">
<div id="detail-formulario2">



  <div id="div_2" class="modal-header">


<div class="container">
    <h4>Codigo del bien</h4>
    <?php
      echo($DatosPersonal['CODIGO_PATRIMONIAL']);

    ?>
    <h4>Denominación del bien</h4>
    <?php
      echo($DatosPersonal['DENOMINACION']);
    ?>
</div>

  </div>

  <div class="modal-body">

<!-- PESTAÑAS -->
<div class="tab">
  <button class="tablinks" onclick="openCity(event, 'Generales')" id="defaultOpen">Datos Generales</button>
  <button class="tablinks" onclick="openCity(event, 'Motor')">Sist. Motor y Frenos</button>
  <button class="tablinks" onclick="openCity(event, 'Refrigeracion')">Sist. Refrigeracion y Eléctrico</button>
	<button class="tablinks" onclick="openCity(event, 'Transmision')">Sist. Transmisión y Dirección</button>
	<button class="tablinks" onclick="openCity(event, 'Suspension')">Sist. Suspensión y Carrocería</button>
	<button class="tablinks" onclick="openCity(event, 'Accesorios')">Accesorios</button>
	<button class="tablinks" onclick="openCity(event, 'Caracteristicas')">Otras Características</button>
</div>



<!-- DATOS DEL FORMULARIO -->

<div id="Generales" class="tabcontent">

<input type="hidden" class="form-control" id = "txt_COD_UE_BIEN_PATRI" value="<?php print $DatosPersonal['COD_UE_BIEN_PATRI']?>"></input>
<input type="hidden" class="form-control" id = "txt_CODIGO_PATRIMONIAL" value="<?php print $DatosPersonal['CODIGO_PATRIMONIAL']?>"></input>
<input type="hidden" class="form-control" id = "txt_DENOMINACION" value="<?php print $DatosPersonal['DENOMINACION']?>"></input>
<input type="hidden" class="form-control" id = "txt_COD_UE_FT_VEHICULAR" value="<?php print $DatosPersonal['COD_UE_FT_VEHICULAR']?>"></input>
  <table>
    <div class="panel panel-primary">
      <div class="panel-heading">Datos Generales</div>
    </div>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Placa:</lable></td>
      <td width="300px"><input type="text" class="form-control" id = "txt_placa" value="<?php print $DatosPersonal['PLACA']?>" disabled></input></td>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Carrocería:</lable></td>
      <td width="300px"><input type="text" class="form-control" id = "txt_carroceria" value="<?php print $DatosPersonal['CARROCERIA']?>"></input></td>
      <td width="20px">&nbsp;</td>
    </tr>

    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Marca:</lable></td>
      <td><input type="text" class="form-control" id = "txt_marca" value="<?php print $DatosPersonal['MARCA']?>" width="200px" disabled></input></td>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Modelo:</lable></td>
      <td><input type="text" class="form-control" id = "txt_modelo" value="<?php print $DatosPersonal['MODELO']?>" width="200px" width="200px" disabled></input></td>
      <td width="20px">&nbsp;</td>
    </tr>

    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Categoría:</lable></td>
      <td><input type="text" class="form-control" id = "txt_categoria" value="<?php print $DatosPersonal['CATEGORIA']?>" width="200px"></input></td>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Nro Chasis:</lable></td>
      <td><input type="text" class="form-control" id = "txt_nrochasis" value="<?php print $DatosPersonal['NRO_CHASIS']?>" width="200px" disabled></input></td>
      <td width="20px">&nbsp;</td>
    </tr>

    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Nro Ejes:</lable></td>
      <td><input type="text" class="form-control" id = "txt_nroejes" value="<?php print $DatosPersonal['NRO_EJES']?>" width="200px"></input></td>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Nro Motor:</lable></td>
      <td><input type="text" class="form-control" id = "txt_nromotor" value="<?php print $DatosPersonal['NRO_MOTOR']?>" width="200px" disabled></input></td>
      <td width="20px">&nbsp;</td>
    </tr>

    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Nro Serie:</lable></td>
      <td><input type="text" class="form-control" id = "txt_nroserie" value="<?php print $DatosPersonal['NRO_SERIE']?>" width="200px" disabled></input></td>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Año Fabricación:</lable></td>
      <td><input type="text" class="form-control" id = "txt_aniofabricacion" value="<?php print $DatosPersonal['ANIO_FABRICACION']?>" width="200px" disabled></input></td>
      <td width="20px">&nbsp;</td>
    </tr>

    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Color:</lable></td>
      <td><input type="text" class="form-control" id = "txt_color" value="<?php print $DatosPersonal['COLOR']?>" width="200px" disabled></input></td>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Combustible:</lable></td>
      <td><input type="text" class="form-control" id = "txt_combustible" value="<?php print $DatosPersonal['COMBUSTIBLE']?>" width="200px"></input></td>
      <td width="20px">&nbsp;</td>
    </tr>

    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Transmisión:</lable></td>
      <td><input type="text" class="form-control" id = "txt_transmision" value="<?php print $DatosPersonal['TRANSMISION']?>" width="200px"></input></td>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Cilindrada:</lable></td>
      <td><input type="text" class="form-control" id = "txt_cilindrada" value="<?php print $DatosPersonal['CILINDRADA']?>" width="200px"></input></td>
      <td width="20px">&nbsp;</td>
    </tr>

    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Kilometraje:</lable></td>
      <td><input type="text" class="form-control" id = "txt_kilometraje" value="<?php print $DatosPersonal['KILOMETRAJE']?>" width="200px"></input></td>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Tarjeta Propiedad:</lable></td>
      <td><input type="text" class="form-control" id = "txt_tarjetapropiedad" value="<?php print $DatosPersonal['TARJETA_PROPIEDAD']?>" width="200px"></input></td>
      <td width="20px">&nbsp;</td>
    </tr>
  </table>
</div>


<div id="Motor" class="tabcontent">

  <table width="100%">
    <div class="panel panel-primary">
      <div class="panel-heading">Sistema de Motor y Frenos</div>
    </div>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Cilindros:</lable></td>
  		<td><input type="checkbox" id = "chk_cilindros" <?php print ($DatosPersonal['B_CILINDROS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_cilindros', 'txt_cilindros')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_cilindros" value="<?php print $DatosPersonal['CILINDROS']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Carburador/Carter:</lable></td>
  		<td><input type="checkbox" id = "chk_carburador" <?php print ($DatosPersonal['B_CARBURADOR']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_carburador', 'txt_carburador')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_carburador" value="<?php print $DatosPersonal['CARBURADOR']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Distribuidor/Bomba de inyección:</lable></td>
  		<td><input type="checkbox" id = "chk_distribuidor" <?php print ($DatosPersonal['B_DISTRIBUIDOR']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_distribuidor', 'txt_distribuidor')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_distribuidor" value="<?php print $DatosPersonal['DISTRIBUIDOR']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Bomba de gasolina:</lable></td>
  		<td><input type="checkbox" id = "chk_bombagasolina" <?php print ($DatosPersonal['B_BOMBA_GASOLINA']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_bombagasolina', 'txt_bombagasolina')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_bombagasolina" value="<?php print $DatosPersonal['BOMBA_GASOLINA']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Purificador de aire:</lable></td>
  		<td><input type="checkbox" id = "chk_purificadoraire" <?php print ($DatosPersonal['B_PURIFICADOR_AIRE']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_purificadoraire', 'txt_purificadoraire')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_purificadoraire" value="<?php print $DatosPersonal['PURIFICADOR_AIRE']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Bomba de frenos:</lable></td>
  		<td><input type="checkbox" id = "chk_bombafrenos" <?php print ($DatosPersonal['B_BOMBA_FRENOS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_bombafrenos', 'txt_bombafrenos')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_bombafrenos" value="<?php print $DatosPersonal['BOMBA_FRENOS']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Zapatas y tambores:</lable></td>
  		<td><input type="checkbox" id = "chk_zapatas" <?php print ($DatosPersonal['B_ZAPATAS_TAMBORES']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_zapatas', 'txt_zapatas')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_zapatas" value="<?php print $DatosPersonal['ZAPATAS_TAMBORES']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Discos y pastillas:</lable></td>
  		<td><input type="checkbox" id = "chk_discos" <?php print ($DatosPersonal['B_DISCOS_PASTILLAS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_discos', 'txt_discos')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_discos" value="<?php print $DatosPersonal['DISCOS_PASTILLAS']?>"></input></td>
		</tr>
	</table>

</div>

<div id="Refrigeracion" class="tabcontent">
  <div class="panel panel-primary">
    <div class="panel-heading">Sistema de Refrigeración y Sistema Eléctrico</div>
  </div>

	<table width="100%">
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Radiador:</lable></td>
  		<td><input type="checkbox" id = "chk_radiador" <?php print ($DatosPersonal['B_RADIOADOR']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_radiador', 'txt_radiador')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_radiador" value="<?php print $DatosPersonal['RADIOADOR']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Ventilador:</lable></td>
  		<td><input type="checkbox" id = "chk_ventilador" <?php print ($DatosPersonal['B_VENTILADOR']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_ventilador', 'txt_ventilador')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_ventilador" value="<?php print $DatosPersonal['VENTILADOR']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Bomba de Agua:</lable></td>
  		<td><input type="checkbox" id = "chk_bomba_agua" <?php print ($DatosPersonal['B_BOMBA_AGUA']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_bomba_agua', 'txt_bomba_agua')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_bomba_agua" value="<?php print $DatosPersonal['BOMBA_AGUA']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Motor de Arranque:</lable></td>
  		<td><input type="checkbox" id = "chk_motor_arranque" <?php print ($DatosPersonal['B_MOTOR_ARRANQUE']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_motor_arranque', 'txt_motor_arranque')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_motor_arranque" value="<?php print $DatosPersonal['MOTOR_ARRANQUE']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Bateria:</lable></td>
  		<td><input type="checkbox" id = "chk_bateria" <?php print ($DatosPersonal['B_BATERIA']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_bateria', 'txt_bateria')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_bateria" value="<?php print $DatosPersonal['BATERIA']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Alternador:</lable></td>
  		<td><input type="checkbox" id = "chk_alternador" <?php print ($DatosPersonal['B_ALTERNADOR']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_alternador', 'txt_alternador')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_alternador" value="<?php print $DatosPersonal['ALTERNADOR']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Bobina:</lable></td>
  		<td><input type="checkbox" id = "chk_bobina" <?php print ($DatosPersonal['B_BOBINA']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_bobina', 'txt_bobina')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_bobina" value="<?php print $DatosPersonal['BOBINA']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Relay Alternador:</lable></td>
  		<td><input type="checkbox" id = "chk_relay_alternador" <?php print ($DatosPersonal['B_RELAY_ALTERNADOR']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_relay_alternador', 'txt_relay_alternador')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_relay_alternador" value="<?php print $DatosPersonal['RELAY_ALTERNADOR']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Faros Delanteros:</lable></td>
  		<td><input type="checkbox" id = "chk_farosdelanteros" <?php print ($DatosPersonal['B_FAROS_DELANTEROS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_farosdelanteros', 'txt_farosdelanteros')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_farosdelanteros" value="<?php print $DatosPersonal['FAROS_DELANTEROS']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Direccionales delanteros:</lable></td>
  		<td><input type="checkbox" id = "chk_direccionalesdelanteros" <?php print ($DatosPersonal['B_DIRECCIONALES_DELANTERAS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_direccionalesdelanteros', 'txt_direccionalesdelanteros')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_direccionalesdelanteros" value="<?php print $DatosPersonal['DIRECCIONALES_DELANTERAS']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Luces posteriores:</lable></td>
  		<td><input type="checkbox" id = "chk_lucesposteriores" <?php print ($DatosPersonal['B_LUCES_POSTERIORES']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_lucesposteriores', 'txt_lucesposteriores')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_lucesposteriores" value="<?php print $DatosPersonal['LUCES_POSTERIORES']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Direccionales posteriores:</lable></td>
  		<td><input type="checkbox" id = "chk_direccionalesposteriores" <?php print ($DatosPersonal['B_DIRECCIONALES_POSTERIORES']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_direccionalesposteriores', 'txt_direccionalesposteriores')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_direccionalesposteriores" value="<?php print $DatosPersonal['DIRECCIONALES_POSTERIORES']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Auto radio:</lable></td>
  		<td><input type="checkbox" id = "chk_autoradio" <?php print ($DatosPersonal['B_AUTO_RADIO']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_autoradio', 'txt_autoradio')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_autoradio" value="<?php print $DatosPersonal['AUTO_RADIO']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Parlantes:</lable></td>
  		<td><input type="checkbox" id = "chk_parlantes" <?php print ($DatosPersonal['B_PARLANTES']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_parlantes', 'txt_parlantes')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_parlantes" value="<?php print $DatosPersonal['PARLANTES']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Claxon:</lable></td>
  		<td><input type="checkbox" id = "chk_claxon" <?php print ($DatosPersonal['B_CLAXON']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_claxon', 'txt_claxon')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_claxon" value="<?php print $DatosPersonal['CLAXON']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Circuito de Luces(faros, cableados):</lable></td>
  		<td><input type="checkbox" id = "chk_circuito_luces" <?php print ($DatosPersonal['B_CIRCUITO_LUCES']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_circuito_luces', 'txt_circuito_luces')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_circuito_luces" value="<?php print $DatosPersonal['CIRCUITO_LUCES']?>"></input></td>
		</tr>
	</table>
</div>

<div id="Transmision" class="tabcontent">
  <div class="panel panel-primary">
    <div class="panel-heading">Sistema de Transmisión y Dirección</div>
  </div>

	<table width="100%">
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Caja de cambios:</lable></td>
  		<td><input type="checkbox" id = "chk_caja_cambios" <?php print ($DatosPersonal['B_CAJA_CAMBIOS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_caja_cambios', 'txt_caja_cambios')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_caja_cambios" value="<?php print $DatosPersonal['CAJA_CAMBIOS']?>"></input></td>
		</tr>
    <tr>
  		<td width="20px">&nbsp;</td>
  		<td><label class="control-label">Bomba de embrague:</lable></td>
  		<td><input type="checkbox" id = "chk_bomba_embrague" <?php print ($DatosPersonal['B_BOMBA_EMBRAGUE']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_bomba_embrague', 'txt_bomba_embrague')"></input></td>
  		<td width="500px"><input type="text" class="form-control" id = "txt_bomba_embrague" value="<?php print $DatosPersonal['BOMBA_EMBRAGUE']?>"></input></td>
		</tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Caja de Transferencia:</lable></td>
      <td><input type="checkbox" id = "chk_caja_transferencia" <?php print ($DatosPersonal['B_CAJA_TRANSFERENCIA']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_caja_transferencia', 'txt_caja_transferencia')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_caja_transferencia" value="<?php print $DatosPersonal['CAJA_TRANSFERENCIA']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Diferencial trasero:</lable></td>
      <td><input type="checkbox" id = "chk_diferencialtrasero" <?php print ($DatosPersonal['B_DIFERENCIAL_TRASERO']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_diferencialtrasero', 'txt_diferencialtrasero')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_diferencialtrasero" value="<?php print $DatosPersonal['DIFERENCIAL_TRASERO']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Diferencial delantero (4x4):</lable></td>
      <td><input type="checkbox" id = "chk_diferencialdelantero" <?php print ($DatosPersonal['B_DIFERENCIAL_DELANTERO']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_diferencialdelantero', 'txt_diferencialdelantero')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_diferencialdelantero" value="<?php print $DatosPersonal['DIFERENCIAL_DELANTERO']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Volante:</lable></td>
      <td><input type="checkbox" id = "chk_volante" <?php print ($DatosPersonal['B_VOLANTE']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_volante', 'txt_volante')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_volante" value="<?php print $DatosPersonal['VOLANTE']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Caña de Dirección:</lable></td>
      <td><input type="checkbox" id = "chk_cania_direccion" <?php print ($DatosPersonal['B_CANA_DIRECCION']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_cania_direccion', 'txt_cania_direccion')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_cania_direccion" value="<?php print $DatosPersonal['CANA_DIRECCION']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Cremallera:</lable></td>
      <td><input type="checkbox" id = "chk_cremallera" <?php print ($DatosPersonal['B_CREMALLERA']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_cremallera', 'txt_cremallera')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_cremallera" value="<?php print $DatosPersonal['CREMALLERA']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Rótulas:</lable></td>
      <td><input type="checkbox" id = "chk_rotulas" <?php print ($DatosPersonal['B_ROTULAS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_rotulas', 'txt_rotulas')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_rotulas" value="<?php print $DatosPersonal['ROTULAS']?>"></input></td>
    </tr>
	</table>

</div>

<div id="Suspension" class="tabcontent">
  <div class="panel panel-primary">
    <div class="panel-heading">Sistema de Suspensión y Carrocería</div>
  </div>

  <table width="100%">
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Amortiguadores/Muelles:</lable></td>
      <td><input type="checkbox" id = "chk_amortiguadores_muelles" <?php print ($DatosPersonal['B_AMORTIGUADORES']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_amortiguadores_muelles', 'txt_amortiguadores_muelles')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_amortiguadores_muelles" value="<?php print $DatosPersonal['AMORTIGUADORES']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Barra de Torsión:</lable></td>
      <td><input type="checkbox" id = "chk_barra_torsion" <?php print ($DatosPersonal['B_BARRA_TORSION']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_barra_torsion', 'txt_barra_torsion')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_barra_torsion" value="<?php print $DatosPersonal['BARRA_TORSION']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Barra estabilizadora:</lable></td>
      <td><input type="checkbox" id = "chk_barraestabilizadora" <?php print ($DatosPersonal['B_BARRA_ESTABILIZADOR']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_barraestabilizadora', 'txt_barraestabilizadora')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_barraestabilizadora" value="<?php print $DatosPersonal['BARRA_ESTABILIZADOR']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Llantas:</lable></td>
      <td><input type="checkbox" id = "chk_llantas" <?php print ($DatosPersonal['B_LLANTAS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_llantas', 'txt_llantas')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_llantas" value="<?php print $DatosPersonal['LLANTAS']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Capot del motor:</lable></td>
      <td><input type="checkbox" id = "chk_capot_motor" <?php print ($DatosPersonal['B_CAPOT_MOTOR']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_capot_motor', 'txt_capot_motor')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_capot_motor" value="<?php print $DatosPersonal['CAPOT_MOTOR']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Capot de maletera:</lable></td>
      <td><input type="checkbox" id = "chk_capot_maletera" <?php print ($DatosPersonal['B_CAPOT_MALETERA']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_capot_maletera', 'txt_capot_maletera')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_capot_maletera" value="<?php print $DatosPersonal['CAPOT_MALETERA']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Parachoques delanteros:</lable></td>
      <td><input type="checkbox" id = "chk_parachoquesdelanteros" <?php print ($DatosPersonal['B_PARACHOQUES_DELANTEROS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_parachoquesdelanteros', 'txt_parachoquesdelanteros')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_parachoquesdelanteros" value="<?php print $DatosPersonal['PARACHOQUES_DELANTEROS']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Parachoques posteriores:</lable></td>
      <td><input type="checkbox" id = "chk_parachoquesposteriores" <?php print ($DatosPersonal['B_PARACHOQUES_POSTERIORES']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_parachoquesposteriores', 'txt_parachoquesposteriores')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_parachoquesposteriores" value="<?php print $DatosPersonal['PARACHOQUES_POSTERIORES']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Lunas laterales:</lable></td>
      <td><input type="checkbox" id = "chk_lunaslaterales" <?php print ($DatosPersonal['B_LUNAS_LATERALES']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_lunaslaterales', 'txt_lunaslaterales')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_lunaslaterales" value="<?php print $DatosPersonal['LUNAS_LATERALES']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Lunas cortavientos:</lable></td>
      <td><input type="checkbox" id = "chk_lunascortavientos" <?php print ($DatosPersonal['B_LUNAS_CORTAVIENTO']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_lunascortavientos', 'txt_lunascortavientos')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_lunascortavientos" value="<?php print $DatosPersonal['LUNAS_CORTAVIENTO']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Parabrisas delanteras:</lable></td>
      <td><input type="checkbox" id = "chk_parabrisasdelanteras" <?php print ($DatosPersonal['B_PARABRISAS_DELANTERO']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_parabrisasdelanteras', 'txt_parabrisasdelanteras')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_parabrisasdelanteras" value="<?php print $DatosPersonal['PARABRISAS_DELANTERO']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Parabrisas posteriores:</lable></td>
      <td><input type="checkbox" id = "chk_parabrisasposteriores" <?php print ($DatosPersonal['B_PARABRISAS_POSTERIOR']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_parabrisasposteriores', 'txt_parabrisasposteriores')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_parabrisasposteriores" value="<?php print $DatosPersonal['PARABRISAS_POSTERIOR']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Tanque de combustible:</lable></td>
      <td><input type="checkbox" id = "chk_tanque_combustible" <?php print ($DatosPersonal['B_TANQUE_COMBUSTIBLE']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_tanque_combustible', 'txt_tanque_combustible')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_tanque_combustible" value="<?php print $DatosPersonal['TANQUE_COMBUSTIBLE']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Puertas:</lable></td>
      <td><input type="checkbox" id = "chk_puertas" <?php print ($DatosPersonal['B_PUERTAS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_puertas', 'txt_puertas')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_puertas" value="<?php print $DatosPersonal['PUERTAS']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Asientos:</lable></td>
      <td><input type="checkbox" id = "chk_asientos" <?php print ($DatosPersonal['B_ASIENTOS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_asientos', 'txt_asientos')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_asientos" value="<?php print $DatosPersonal['ASIENTOS']?>"></input></td>
    </tr>

	</table>
</div>

<div id="Accesorios" class="tabcontent">
  <div class="panel panel-primary">
    <div class="panel-heading">Accesorios</div>
  </div>

  <table width="100%">
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Aire Acondicionado:</lable></td>
      <td><input type="checkbox" id = "chk_aireacondicionado" <?php print ($DatosPersonal['B_AIRE_ACONDICIONADO']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_aireacondicionado', 'txt_aireacondicionado')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_aireacondicionado" value="<?php print $DatosPersonal['AIRE_ACONDICIONADO']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Alarma:</lable></td>
      <td><input type="checkbox" id = "chk_alarma" <?php print ($DatosPersonal['B_ALARMA']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_alarma', 'txt_alarma')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_alarma" value="<?php print $DatosPersonal['ALARMA']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Plumillas:</lable></td>
      <td><input type="checkbox" id = "chk_plumillas" <?php print ($DatosPersonal['B_PLUMILLAS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_plumillas', 'txt_plumillas')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_plumillas" value="<?php print $DatosPersonal['PLUMILLAS']?>"></input></td>
    </tr>
    <tr>
      <td width="20px">&nbsp;</td>
      <td><label class="control-label">Espejos:</lable></td>
      <td><input type="checkbox" id = "chk_espejos" <?php print ($DatosPersonal['B_ESPEJOS']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_espejos', 'txt_espejos')"></input></td>
      <td width="500px"><input type="text" class="form-control" id = "txt_espejos" value="<?php print $DatosPersonal['ESPEJOS']?>"></input></td>
    </tr>
    <tr>
			<td>&nbsp;</td>
			<td><label class="control-label">Cinturones de seguridad:</lable></td>
			<td><input type="checkbox" id = "chk_cinturones" <?php print ($DatosPersonal['B_CINTURONES_SEGURIDAD']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_cinturones', 'txt_cinturones')"></input></td>
			<td><input type="text" class="form-control" id = "txt_cinturones" width="120px" value="<?php print $DatosPersonal['CINTURONES_SEGURIDAD']?>"></input></td>
			<td>&nbsp;</td>
		</tr>
    <tr>
			<td>&nbsp;</td>
			<td><label class="control-label">Antena:</lable></td>
			<td><input type="checkbox" id = "chk_antena" <?php print ($DatosPersonal['B_ANTENA']==1) ? 'checked="checked"' : '' ?> onclick="handle_Bloquear_controles_con_check('chk_antena', 'txt_antena')"></input></td>
			<td><input type="text" class="form-control" id = "txt_antena" width="120px" value="<?php print $DatosPersonal['ANTENA']?>"></input></td>
			<td>&nbsp;</td>
		</tr>
  </table>
</div>

<div id="Caracteristicas" class="tabcontent">
  <div class="panel panel-primary">
    <div class="panel-heading">Otras Caracteristicas</div>
  </div>

  <table width="100%">
    <tr>
			<td>&nbsp;</td>
			<td><label class="control-label">Caracteristicas relevantes:</lable></td>
			<td><textarea class="form-control" rows="3" id = "txa_caracteristicasrelevantes" width="200px"><?php print $DatosPersonal['OTRAS_CARACTERISTICAS_RELEVANTES']?></textarea></td>
			<td>&nbsp;</td>
		</tr>
    <tr>
			<td>&nbsp;</td>
			<td><label class="control-label">Valor de tasación(S/.):</lable></td>
			<td><input type="text" class="form-control" id = "txt_tasacion" width="120px" value="<?php print $DatosPersonal['VALOR_TASACION']?>"></input></td>
			<td>&nbsp;</td>
		</tr>
  </table>

</div>

<div class="modal-footer">
  <button type="button" class="btn btn-primary" onclick="handle_Guardar_Popup_Registro_FichaVehicular(); ">Guardar</button>
  <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
</div>

</div>



</div>

</div>
