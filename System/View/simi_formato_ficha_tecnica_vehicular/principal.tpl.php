<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../utils/css/SGI_Estilos.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../../utils/css/stylesPaginacion.css" />

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<table width="1000" border="0" cellspacing="3" cellpadding="0">
<tr>
  <td width="710" valign="top">
    <table width="90%" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="82%" >
          <div class="Titulo_02_19px">
            <img src="../webimages/iconos/btn_admin_55.png" width="58" height="58" align="absmiddle" />
              Registro de Ficha Técnica Vehicular
          </div>
        </td>
        <td width="18%" >
        </td>
      </tr>
      <tr>
        <td colspan="2" >
          <hr />
        </td>
      </tr>
      <tr>
        <td colspan="2" >&nbsp;</td>
      </tr>
    </table>
    <div id="div_tabs" >
      <ul>
        <li>
          <a href="#tabs-1">Busqueda de Bienes Patrimoniales</a>
        </li>
      </ul>

      <div id="tabs-1">
        <table width="100%" border="0" cellpadding="0" cellspacing="2" bgcolor="#f4f4f4" class="table-container TABLE_border4">
          <tr>
            <td colspan="8">&nbsp;</td>
          </tr>
          <tr>
            <td width="20px">&nbsp;</td>
            <td width="100px">Código Patrimonial</td>
            <td width="300px"><input name="txt_codigo" type="text" id="txt_codigo" style="width:90%"/></td>
            <td width="110px">Denominación del Bien</td>
            <td width="350px"><input name="txt_denominacion" type="text" id="txt_denominacion" style="width:90%"/></td>
            <td width="30px">
              <a href="#" onclick='handle_paginar_bienes_vehiculos(1);'>
                <img src="../webimages/iconos/ver_icono_b.png" width="20" height="22" border="0" />
              </a> &nbsp;&nbsp;
            </td>
            <td width="30px">
              <a href="#" onclick="handle_clear_busqueda()">
                <img src="../webimages/iconos/quitar_flitros.png" width="22" height="22" border="0" />
              </a>
            </td>
            <td width="20px">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="8">&nbsp;</td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="2">
          <tr>
            <td width="90%">&nbsp;</td>
            <td style="text-align:right;">
              <div id="dv_Excel" onclick="handle_Exportar_Excel()" class="TABLE_border4" style="background-color:#f4f4f4;
                                      padding:2px 8px; height:25px; width:120px; margin:8px; margin-left:8px;
                                      cursor:pointer;">
                  <img src="../webimages/iconos/excel.png" height="18" align="absmiddle" border="0" width="18"> Exportar a Excel
              </div>
            </td>
          </tr>
        </table>


        <div id="detail-busqueda" style="padding:10px 0px 10px 0px"></div>

      </div>
    </div>
  </td>
  <td width="710" valign="top">
    <div id="detail-formulario" style="padding:10px 0px 10px 10px;"></div>
  </td>
</tr>
</table>

<script type="text/javascript" >
    <?php include_once('js/script.js') ?>
</script>
