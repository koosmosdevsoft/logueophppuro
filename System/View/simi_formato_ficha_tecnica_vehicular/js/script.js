$(function() {
    $("#div_tabs").tabs();
	handle_paginar_bienes_vehiculos(1);
    /*
	$("#txt_busc_fecha_asignacion").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true
    });*/
});

function handle_clear_busqueda(){
  $("#txt_codigo").val('');
  $("#txt_denominacion").val('');
  handle_paginar_bienes_vehiculos(1);
}


function handle_paginar_bienes_vehiculos(numeroPagina) {

  var txt_codigo = $("#txt_codigo").val();
  var txt_denominacion = $("#txt_denominacion").val();


  carga_loading('detail-busqueda');
  var url = "View/simi_formato_ficha_tecnica_vehicular/principal_view.php?operacion=Filtrar_Lista_BienPatrimonial";
  url += (txt_codigo == "") ? "" : "&txt_codigo=" + txt_codigo;
  url += (txt_denominacion == "") ? "" : "&txt_denominacion=" + txt_denominacion;
  url += (numeroPagina == "") ? "1" : "&numeroPagina=" + numeroPagina;
  $.ajax(url).done(function (response) {
    $("#detail-busqueda").html(response);
  });
}


function handle_Mostrar_Form_personal(CodBien, codPatrimonial, Denominacion, codFichaVeh) {

      carga_loading('detail-formulario2');
      var url = "View/simi_formato_ficha_tecnica_vehicular/principal_view.php?operacion=Mostrar_Form_Registro_Ficha_Vehicular";
      url += (CodBien == "") ? "" : "&CodBien=" + CodBien;
      url += (codPatrimonial == "") ? "" : "&codPatrimonial=" + codPatrimonial;
      url += (Denominacion == "") ? "" : "&Denominacion=" + Denominacion;

      //$("#txt_cilindros").val(info[22]);

      $.ajax(url).done(function (response) {
        $("#detail-formulario2").html(response);
      });


}


function handle_Guardar_Popup_Registro_FichaVehicular(){

  var TXH_SIMI_COD_ENTIDAD = $("#TXH_SIMI_COD_ENTIDAD").val();
  var TXH_ID_USUARIO 		= $("#TXH_SIMI_COD_USUARIO").val();


  var txt_COD_UE_BIEN_PATRI = $("#txt_COD_UE_BIEN_PATRI").val();
  var txt_DENOMINACION = $("#txt_DENOMINACION").val();
  var txt_COD_UE_FT_VEHICULAR 		= $("#txt_COD_UE_FT_VEHICULAR").val();

  var txt_placa = $("#txt_placa").val();
  var txt_carroceria = $("#txt_carroceria").val();
  var txt_marca = $("#txt_marca").val();
  var txt_modelo = $("#txt_modelo").val();
  var txt_categoria = $("#txt_categoria").val();
  var txt_nrochasis = $("#txt_nrochasis").val();
  var txt_nroejes = $("#txt_nroejes").val();
  var txt_nromotor = $("#txt_nromotor").val();
  var txt_nroserie = $("#txt_nroserie").val();
  var txt_aniofabricacion = $("#txt_aniofabricacion").val();
  var txt_color = $("#txt_color").val();
  var txt_combustible = $("#txt_combustible").val();
  var txt_transmision = $("#txt_transmision").val();
  var txt_cilindrada = $("#txt_cilindrada").val();
  var txt_kilometraje = $("#txt_kilometraje").val();
  var txt_tarjetapropiedad = $("#txt_tarjetapropiedad").val();

  var txt_cilindros = $("#txt_cilindros").val();
  var txt_carburador = $("#txt_carburador").val();
  var txt_distribuidor = $("#txt_distribuidor").val();
  var txt_bombagasolina = $("#txt_bombagasolina").val();
  var txt_purificadoraire = $("#txt_purificadoraire").val();
  var txt_bombafrenos = $("#txt_bombafrenos").val();
  var txt_zapatas = $("#txt_zapatas").val();
  var txt_discos = $("#txt_discos").val();

  var txt_radiador = $("#txt_radiador").val();
  var txt_ventilador = $("#txt_ventilador").val();
  var txt_bomba_agua = $("#txt_bomba_agua").val();
  var txt_motor_arranque = $("#txt_motor_arranque").val();
  var txt_bateria = $("#txt_bateria").val();
  var txt_alternador = $("#txt_alternador").val();
  var txt_bobina = $("#txt_bobina").val();
  var txt_relay_alternador = $("#txt_relay_alternador").val();
  var txt_farosdelanteros = $("#txt_farosdelanteros").val();
  var txt_direccionalesdelanteros = $("#txt_direccionalesdelanteros").val();
  var txt_lucesposteriores = $("#txt_lucesposteriores").val();
  var txt_direccionalesposteriores = $("#txt_direccionalesposteriores").val();
  var txt_autoradio = $("#txt_autoradio").val();
  var txt_parlantes = $("#txt_parlantes").val();
  var txt_claxon = $("#txt_claxon").val();
  var txt_circuito_luces = $("#txt_circuito_luces").val();


  var txt_caja_cambios = $("#txt_caja_cambios").val();
  var txt_bomba_embrague = $("#txt_bomba_embrague").val();
  var txt_caja_transferencia = $("#txt_caja_transferencia").val();
  var txt_diferencialtrasero = $("#txt_diferencialtrasero").val();
  var txt_diferencialdelantero = $("#txt_diferencialdelantero").val();
  var txt_volante = $("#txt_volante").val();
  var txt_cania_direccion = $("#txt_cania_direccion").val();
  var txt_cremallera = $("#txt_cremallera").val();
  var txt_rotulas = $("#txt_rotulas").val();

  var txt_amortiguadores_muelles = $("#txt_amortiguadores_muelles").val();
  var txt_barra_torsion = $("#txt_barra_torsion").val();
  var txt_barraestabilizadora = $("#txt_barraestabilizadora").val();
  var txt_llantas = $("#txt_llantas").val();
  var txt_capot_motor = $("#txt_capot_motor").val();
  var txt_capot_maletera = $("#txt_capot_maletera").val();
  var txt_parachoquesdelanteros = $("#txt_parachoquesdelanteros").val();
  var txt_parachoquesposteriores = $("#txt_parachoquesposteriores").val();
  var txt_lunaslaterales = $("#txt_lunaslaterales").val();
  var txt_lunascortavientos = $("#txt_lunascortavientos").val();
  var txt_parabrisasdelanteras = $("#txt_parabrisasdelanteras").val();
  var txt_parabrisasposteriores = $("#txt_parabrisasposteriores").val();
  var txt_tanque_combustible = $("#txt_tanque_combustible").val();
  var txt_puertas = $("#txt_puertas").val();
  var txt_asientos = $("#txt_asientos").val();

  var txt_aireacondicionado = $("#txt_aireacondicionado").val();
  var txt_alarma = $("#txt_alarma").val();
  var txt_plumillas = $("#txt_plumillas").val();
  var txt_espejos = $("#txt_espejos").val();
  var txt_cinturones = $("#txt_cinturones").val();
  var txt_antena = $("#txt_antena").val();

  /**** Controles CheckBox ****/
  var chk_cilindros = ($("#chk_cilindros").is(':checked')==true ? '1':'0');
  var chk_carburador = ($("#chk_carburador").is(':checked')==true ? '1':'0');
  var chk_distribuidor = ($("#chk_distribuidor").is(':checked')==true ? '1':'0');
  var chk_bombagasolina = ($("#chk_bombagasolina").is(':checked')==true ? '1':'0');
  var chk_purificadoraire = ($("#chk_purificadoraire").is(':checked')==true ? '1':'0');
  var chk_bombafrenos = ($("#chk_bombafrenos").is(':checked')==true ? '1':'0');
  var chk_zapatas = ($("#chk_zapatas").is(':checked')==true ? '1':'0');
  var chk_discos = ($("#chk_discos").is(':checked')==true ? '1':'0');


  var chk_radiador = ($("#chk_radiador").is(':checked')==true ? '1':'0');
  var chk_ventilador = ($("#chk_ventilador").is(':checked')==true ? '1':'0');
  var chk_bomba_agua = ($("#chk_bomba_agua").is(':checked')==true ? '1':'0');
  var chk_motor_arranque = ($("#chk_motor_arranque").is(':checked')==true ? '1':'0');
  var chk_bateria = ($("#chk_bateria").is(':checked')==true ? '1':'0');
  var chk_alternador = ($("#chk_alternador").is(':checked')==true ? '1':'0');
  var chk_bobina = ($("#chk_bobina").is(':checked')==true ? '1':'0');
  var chk_relay_alternador = ($("#chk_relay_alternador").is(':checked')==true ? '1':'0');
  var chk_farosdelanteros = ($("#chk_farosdelanteros").is(':checked')==true ? '1':'0');
  var chk_direccionalesdelanteros = ($("#chk_direccionalesdelanteros").is(':checked')==true ? '1':'0');
  var chk_lucesposteriores = ($("#chk_lucesposteriores").is(':checked')==true ? '1':'0');
  var chk_direccionalesposteriores = ($("#chk_direccionalesposteriores").is(':checked')==true ? '1':'0');
  var chk_autoradio = ($("#chk_autoradio").is(':checked')==true ? '1':'0');
  var chk_parlantes = ($("#chk_parlantes").is(':checked')==true ? '1':'0');
  var chk_claxon = ($("#chk_claxon").is(':checked')==true ? '1':'0');
  var chk_circuito_luces = ($("#chk_circuito_luces").is(':checked')==true ? '1':'0');

  var chk_caja_cambios = ($("#chk_caja_cambios").is(':checked')==true ? '1':'0');
  var chk_bomba_embrague = ($("#chk_bomba_embrague").is(':checked')==true ? '1':'0');
  var chk_caja_transferencia = ($("#chk_caja_transferencia").is(':checked')==true ? '1':'0');
  var chk_diferencialtrasero = ($("#chk_diferencialtrasero").is(':checked')==true ? '1':'0');
  var chk_diferencialdelantero = ($("#chk_diferencialdelantero").is(':checked')==true ? '1':'0');
  var chk_volante = ($("#chk_volante").is(':checked')==true ? '1':'0');
  var chk_cania_direccion = ($("#chk_cania_direccion").is(':checked')==true ? '1':'0');
  var chk_cremallera = ($("#chk_cremallera").is(':checked')==true ? '1':'0');
  var chk_rotulas = ($("#chk_rotulas").is(':checked')==true ? '1':'0');

  var chk_amortiguadores_muelles = ($("#chk_amortiguadores_muelles").is(':checked')==true ? '1':'0');
  var chk_barra_torsion = ($("#chk_barra_torsion").is(':checked')==true ? '1':'0');
  var chk_barraestabilizadora = ($("#chk_barraestabilizadora").is(':checked')==true ? '1':'0');
  var chk_llantas = ($("#chk_llantas").is(':checked')==true ? '1':'0');
  var chk_capot_motor = ($("#chk_capot_motor").is(':checked')==true ? '1':'0');
  var chk_capot_maletera = ($("#chk_capot_maletera").is(':checked')==true ? '1':'0');
  var chk_parachoquesdelanteros = ($("#chk_parachoquesdelanteros").is(':checked')==true ? '1':'0');
  var chk_parachoquesposteriores = ($("#chk_parachoquesposteriores").is(':checked')==true ? '1':'0');
  var chk_lunaslaterales = ($("#chk_lunaslaterales").is(':checked')==true ? '1':'0');
  var chk_lunascortavientos = ($("#chk_lunascortavientos").is(':checked')==true ? '1':'0');
  var chk_parabrisasdelanteras = ($("#chk_parabrisasdelanteras").is(':checked')==true ? '1':'0');
  var chk_parabrisasposteriores = ($("#chk_parabrisasposteriores").is(':checked')==true ? '1':'0');
  var chk_tanque_combustible = ($("#chk_tanque_combustible").is(':checked')==true ? '1':'0');
  var chk_puertas = ($("#chk_puertas").is(':checked')==true ? '1':'0');
  var chk_asientos = ($("#chk_asientos").is(':checked')==true ? '1':'0');

  var chk_aireacondicionado = ($("#chk_aireacondicionado").is(':checked')==true ? '1':'0');
  var chk_alarma = ($("#chk_alarma").is(':checked')==true ? '1':'0');
  var chk_plumillas = ($("#chk_plumillas").is(':checked')==true ? '1':'0');
  var chk_espejos = ($("#chk_espejos").is(':checked')==true ? '1':'0');
  var chk_cinturones = ($("#chk_cinturones").is(':checked')==true ? '1':'0');
  var chk_antena = ($("#chk_antena").is(':checked')==true ? '1':'0');

  var txa_caracteristicasrelevantes = $("#txa_caracteristicasrelevantes").val();
  var txt_tasacion = $("#txt_tasacion").val();


  if(txt_placa == ''){
    alert("Ud. no ha ingresado la placa");
    $("#txt_placa").focus();
    return;
  }else{

    if (confirm("¿Está seguro de registrar la ficha vehicular?")){

      var url = "View/simi_formato_ficha_tecnica_vehicular/principal_view.php?operacion=Guardar_Form_Registrar_FichaVehicular";
      url += (TXH_SIMI_COD_ENTIDAD == "") ? "" : "&TXH_SIMI_COD_ENTIDAD=" + TXH_SIMI_COD_ENTIDAD;
      url += (TXH_ID_USUARIO == "") ? "" : "&TXH_ID_USUARIO=" + TXH_ID_USUARIO;
      url += (txt_COD_UE_FT_VEHICULAR == "") ? "" : "&txt_COD_UE_FT_VEHICULAR=" + txt_COD_UE_FT_VEHICULAR;

      url += (txt_COD_UE_BIEN_PATRI == "") ? "" : "&txt_COD_UE_BIEN_PATRI=" + txt_COD_UE_BIEN_PATRI;
      url += (txt_DENOMINACION == "") ? "" : "&txt_DENOMINACION=" + txt_DENOMINACION;
			url += (txt_placa == "") ? "" : "&txt_placa=" + txt_placa;
      url += (txt_carroceria == "") ? "" : "&txt_carroceria=" + txt_carroceria;
      url += (txt_marca == "") ? "" : "&txt_marca=" + txt_marca;
      url += (txt_modelo == "") ? "" : "&txt_modelo=" + txt_modelo;
      url += (txt_categoria == "") ? "" : "&txt_categoria=" + txt_categoria;
      url += (txt_nrochasis == "") ? "" : "&txt_nrochasis=" + txt_nrochasis;
      url += (txt_nroejes == "") ? "" : "&txt_nroejes=" + txt_nroejes;
      url += (txt_nromotor == "") ? "" : "&txt_nromotor=" + txt_nromotor;
      url += (txt_nroserie == "") ? "" : "&txt_nroserie=" + txt_nroserie;
      url += (txt_aniofabricacion == "") ? "" : "&txt_aniofabricacion=" + txt_aniofabricacion;
      url += (txt_color == "") ? "" : "&txt_color=" + txt_color;
      url += (txt_combustible == "") ? "" : "&txt_combustible=" + txt_combustible;
      url += (txt_transmision == "") ? "" : "&txt_transmision=" + txt_transmision;
      url += (txt_cilindrada == "") ? "" : "&txt_cilindrada=" + txt_cilindrada;
      url += (txt_kilometraje == "") ? "" : "&txt_kilometraje=" + txt_kilometraje;
      url += (txt_tarjetapropiedad == "") ? "" : "&txt_tarjetapropiedad=" + txt_tarjetapropiedad;

      url += (txt_cilindros == "") ? "" : "&txt_cilindros=" + txt_cilindros;
      url += (txt_carburador == "") ? "" : "&txt_carburador=" + txt_carburador;
      url += (txt_distribuidor == "") ? "" : "&txt_distribuidor=" + txt_distribuidor;
      url += (txt_bombagasolina == "") ? "" : "&txt_bombagasolina=" + txt_bombagasolina;
      url += (txt_purificadoraire == "") ? "" : "&txt_purificadoraire=" + txt_purificadoraire;
      url += (txt_bombafrenos == "") ? "" : "&txt_bombafrenos=" + txt_bombafrenos;
      url += (txt_zapatas == "") ? "" : "&txt_zapatas=" + txt_zapatas;
      url += (txt_discos == "") ? "" : "&txt_discos=" + txt_discos;


      url += (txt_radiador == "") ? "" : "&txt_radiador=" + txt_radiador;
      url += (txt_ventilador == "") ? "" : "&txt_ventilador=" + txt_ventilador;
      url += (txt_bomba_agua == "") ? "" : "&txt_bomba_agua=" + txt_bomba_agua;
      url += (txt_motor_arranque == "") ? "" : "&txt_motor_arranque=" + txt_motor_arranque;
      url += (txt_bateria == "") ? "" : "&txt_bateria=" + txt_bateria;
      url += (txt_alternador == "") ? "" : "&txt_alternador=" + txt_alternador;
      url += (txt_bobina == "") ? "" : "&txt_bobina=" + txt_bobina;
      url += (txt_relay_alternador == "") ? "" : "&txt_relay_alternador=" + txt_relay_alternador;
      url += (txt_farosdelanteros == "") ? "" : "&txt_farosdelanteros=" + txt_farosdelanteros;
      url += (txt_direccionalesdelanteros == "") ? "" : "&txt_direccionalesdelanteros=" + txt_direccionalesdelanteros;
      url += (txt_lucesposteriores == "") ? "" : "&txt_lucesposteriores=" + txt_lucesposteriores;
      url += (txt_direccionalesposteriores == "") ? "" : "&txt_direccionalesposteriores=" + txt_direccionalesposteriores;
      url += (txt_autoradio == "") ? "" : "&txt_autoradio=" + txt_autoradio;
      url += (txt_parlantes == "") ? "" : "&txt_parlantes=" + txt_parlantes;
      url += (txt_claxon == "") ? "" : "&txt_claxon=" + txt_claxon;
      url += (txt_circuito_luces == "") ? "" : "&txt_circuito_luces=" + txt_circuito_luces;


      url += (txt_caja_cambios == "") ? "" : "&txt_caja_cambios=" + txt_caja_cambios;
      url += (txt_bomba_embrague == "") ? "" : "&txt_bomba_embrague=" + txt_bomba_embrague;
      url += (txt_caja_transferencia == "") ? "" : "&txt_caja_transferencia=" + txt_caja_transferencia;
      url += (txt_diferencialtrasero == "") ? "" : "&txt_diferencialtrasero=" + txt_diferencialtrasero;
      url += (txt_diferencialdelantero == "") ? "" : "&txt_diferencialdelantero=" + txt_diferencialdelantero;
      url += (txt_volante == "") ? "" : "&txt_volante=" + txt_volante;
      url += (txt_cania_direccion == "") ? "" : "&txt_cania_direccion=" + txt_cania_direccion;
      url += (txt_cremallera == "") ? "" : "&txt_cremallera=" + txt_cremallera;
      url += (txt_rotulas == "") ? "" : "&txt_rotulas=" + txt_rotulas;

      url += (txt_amortiguadores_muelles == "") ? "" : "&txt_amortiguadores_muelles=" + txt_amortiguadores_muelles;
      url += (txt_barra_torsion == "") ? "" : "&txt_barra_torsion=" + txt_barra_torsion;
      url += (txt_barraestabilizadora == "") ? "" : "&txt_barraestabilizadora=" + txt_barraestabilizadora;
      url += (txt_llantas == "") ? "" : "&txt_llantas=" + txt_llantas;
      url += (txt_capot_motor == "") ? "" : "&txt_capot_motor=" + txt_capot_motor;
      url += (txt_capot_maletera == "") ? "" : "&txt_capot_maletera=" + txt_capot_maletera;
      url += (txt_parachoquesdelanteros == "") ? "" : "&txt_parachoquesdelanteros=" + txt_parachoquesdelanteros;
      url += (txt_parachoquesposteriores == "") ? "" : "&txt_parachoquesposteriores=" + txt_parachoquesposteriores;
      url += (txt_lunaslaterales == "") ? "" : "&txt_lunaslaterales=" + txt_lunaslaterales;
      url += (txt_lunascortavientos == "") ? "" : "&txt_lunascortavientos=" + txt_lunascortavientos;
      url += (txt_parabrisasdelanteras == "") ? "" : "&txt_parabrisasdelanteras=" + txt_parabrisasdelanteras;
      url += (txt_parabrisasposteriores == "") ? "" : "&txt_parabrisasposteriores=" + txt_parabrisasposteriores;
      url += (txt_tanque_combustible == "") ? "" : "&txt_tanque_combustible=" + txt_tanque_combustible;
      url += (txt_puertas == "") ? "" : "&txt_puertas=" + txt_puertas;
      url += (txt_asientos == "") ? "" : "&txt_asientos=" + txt_asientos;

      url += (txt_aireacondicionado == "") ? "" : "&txt_aireacondicionado=" + txt_aireacondicionado;
      url += (txt_alarma == "") ? "" : "&txt_alarma=" + txt_alarma;
      url += (txt_plumillas == "") ? "" : "&txt_plumillas=" + txt_plumillas;
      url += (txt_espejos == "") ? "" : "&txt_espejos=" + txt_espejos;
      url += (txt_cinturones == "") ? "" : "&txt_cinturones=" + txt_cinturones;
      url += (txt_antena == "") ? "" : "&txt_antena=" + txt_antena;

      url += (txa_caracteristicasrelevantes == "") ? "" : "&txa_caracteristicasrelevantes=" + txa_caracteristicasrelevantes;
      url += (txt_tasacion == "") ? "" : "&txt_tasacion=" + txt_tasacion;

      /**** Controles CheckBox ****/
      url += (chk_cilindros == "") ? "" : "&chk_cilindros=" + chk_cilindros;
      url += (chk_carburador == "") ? "" : "&chk_carburador=" + chk_carburador;
      url += (chk_distribuidor == "") ? "" : "&chk_distribuidor=" + chk_distribuidor;
      url += (chk_bombagasolina == "") ? "" : "&chk_bombagasolina=" + chk_bombagasolina;
      url += (chk_purificadoraire == "") ? "" : "&chk_purificadoraire=" + chk_purificadoraire;
      url += (chk_bombafrenos == "") ? "" : "&chk_bombafrenos=" + chk_bombafrenos;
      url += (chk_zapatas == "") ? "" : "&chk_zapatas=" + chk_zapatas;
      url += (chk_discos == "") ? "" : "&chk_discos=" + chk_discos;


      url += (chk_radiador == "") ? "" : "&chk_radiador=" + chk_radiador;
      url += (chk_ventilador == "") ? "" : "&chk_ventilador=" + chk_ventilador;
      url += (chk_bomba_agua == "") ? "" : "&chk_bomba_agua=" + chk_bomba_agua;
      url += (chk_motor_arranque == "") ? "" : "&chk_motor_arranque=" + chk_motor_arranque;
      url += (chk_bateria == "") ? "" : "&chk_bateria=" + chk_bateria;
      url += (chk_alternador == "") ? "" : "&chk_alternador=" + chk_alternador;
      url += (chk_bobina == "") ? "" : "&chk_bobina=" + chk_bobina;
      url += (chk_relay_alternador == "") ? "" : "&chk_relay_alternador=" + chk_relay_alternador;
      url += (chk_farosdelanteros == "") ? "" : "&chk_farosdelanteros=" + chk_farosdelanteros;
      url += (chk_direccionalesdelanteros == "") ? "" : "&chk_direccionalesdelanteros=" + chk_direccionalesdelanteros;
      url += (chk_lucesposteriores == "") ? "" : "&chk_lucesposteriores=" + chk_lucesposteriores;
      url += (chk_direccionalesposteriores == "") ? "" : "&chk_direccionalesposteriores=" + chk_direccionalesposteriores;
      url += (chk_autoradio == "") ? "" : "&chk_autoradio=" + chk_autoradio;
      url += (chk_parlantes == "") ? "" : "&chk_parlantes=" + chk_parlantes;
      url += (chk_claxon == "") ? "" : "&chk_claxon=" + chk_claxon;
      url += (chk_circuito_luces == "") ? "" : "&chk_circuito_luces=" + chk_circuito_luces;


      url += (chk_caja_cambios == "") ? "" : "&chk_caja_cambios=" + chk_caja_cambios;
      url += (chk_bomba_embrague == "") ? "" : "&chk_bomba_embrague=" + chk_bomba_embrague;
      url += (chk_caja_transferencia == "") ? "" : "&chk_caja_transferencia=" + chk_caja_transferencia;
      url += (chk_diferencialtrasero == "") ? "" : "&chk_diferencialtrasero=" + chk_diferencialtrasero;
      url += (chk_diferencialdelantero == "") ? "" : "&chk_diferencialdelantero=" + chk_diferencialdelantero;
      url += (chk_volante == "") ? "" : "&chk_volante=" + chk_volante;
      url += (chk_cania_direccion == "") ? "" : "&chk_cania_direccion=" + chk_cania_direccion;
      url += (chk_cremallera == "") ? "" : "&chk_cremallera=" + chk_cremallera;
      url += (chk_rotulas == "") ? "" : "&chk_rotulas=" + chk_rotulas;

      url += (chk_amortiguadores_muelles == "") ? "" : "&chk_amortiguadores_muelles=" + chk_amortiguadores_muelles;
      url += (chk_barra_torsion == "") ? "" : "&chk_barra_torsion=" + chk_barra_torsion;
      url += (chk_barraestabilizadora == "") ? "" : "&chk_barraestabilizadora=" + chk_barraestabilizadora;
      url += (chk_llantas == "") ? "" : "&chk_llantas=" + chk_llantas;
      url += (chk_capot_motor == "") ? "" : "&chk_capot_motor=" + chk_capot_motor;
      url += (chk_capot_maletera == "") ? "" : "&chk_capot_maletera=" + chk_capot_maletera;
      url += (chk_parachoquesdelanteros == "") ? "" : "&chk_parachoquesdelanteros=" + chk_parachoquesdelanteros;
      url += (chk_parachoquesposteriores == "") ? "" : "&chk_parachoquesposteriores=" + chk_parachoquesposteriores;
      url += (chk_lunaslaterales == "") ? "" : "&chk_lunaslaterales=" + chk_lunaslaterales;
      url += (chk_lunascortavientos == "") ? "" : "&chk_lunascortavientos=" + chk_lunascortavientos;
      url += (chk_parabrisasdelanteras == "") ? "" : "&chk_parabrisasdelanteras=" + chk_parabrisasdelanteras;
      url += (chk_parabrisasposteriores == "") ? "" : "&chk_parabrisasposteriores=" + chk_parabrisasposteriores;
      url += (chk_tanque_combustible == "") ? "" : "&chk_tanque_combustible=" + chk_tanque_combustible;
      url += (chk_puertas == "") ? "" : "&chk_puertas=" + chk_puertas;
      url += (chk_asientos == "") ? "" : "&chk_asientos=" + chk_asientos;

      url += (chk_aireacondicionado == "") ? "" : "&chk_aireacondicionado=" + chk_aireacondicionado;
      url += (chk_alarma == "") ? "" : "&chk_alarma=" + chk_alarma;
      url += (chk_plumillas == "") ? "" : "&chk_plumillas=" + chk_plumillas;
      url += (chk_espejos == "") ? "" : "&chk_espejos=" + chk_espejos;
      url += (chk_cinturones == "") ? "" : "&chk_cinturones=" + chk_cinturones;
      url += (chk_antena == "") ? "" : "&chk_antena=" + chk_antena;


      $.ajax(url).done(function (response) {
				if(response == '1'){
          alert('Se ha registrado satisfactoriamente');
          $(".modal").modal('hide');
          handle_paginar_bienes_vehiculos(1);


					//$("#detail-formulario").html('');
          //#detail-busqueda
					//handle_paginar_personal(1);
				}else{
					alert(response);
				}

			});




    }

  }





}

/***************************************************************/

function handle_Bloquear_controles_con_check(control_check, control_texto){

  if($("#" + control_check).is(':checked')==true){
      $("#" + control_texto).removeAttr("disabled");
      $("#" + control_texto).focus();
  }else{
      $("#" + control_texto).attr('disabled','disabled');
      $("#" + control_texto).val('');
  }

}


/* Permite Mostrar archivo en PDF */
function handle_MostrarPDF(cod_ficha_vehicular){

	var url_page = 'View/simi_formato_ficha_tecnica_vehicular/formatoficha_tecnica_vehicular_PDF.php?OPT=VistaPDF&codFicha='+cod_ficha_vehicular;
		window.open(url_page,'_blank');

	}

/* Permite Exportar a archivo Excel */
function handle_Exportar_Excel(){

  var TXH_SIMI_COD_ENTIDAD = $("#TXH_SIMI_COD_ENTIDAD").val();
  var url = "View/simi_formato_ficha_tecnica_vehicular/Exportar_Excel_FichaVehicular.php?OPT=VistaXLS";
  url += (TXH_SIMI_COD_ENTIDAD == "") ? "" : "&TXH_SIMI_COD_ENTIDAD=" + TXH_SIMI_COD_ENTIDAD;
  window.open(url,'_blank');

/*
  $.ajax(url).done(function (response) {
    if(response == '1'){


    }else{
      alert(response);
    }

  });
  */

}
