<?php header('Content-type: application/vnd.ms-excel; charset=utf-8');
	header("Content-Disposition: attachment; filename=Bien_x_Local.xls");
	header("Pragma: no-cache");
	header("Expires: 0");
ini_set('memory_limit', '-1');
$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

require_once("../../Controller/C_Interconexion_SQL.php");
require_once('../../Model/M_TBL_MUEBLES_UE_BIE_FT_VEHICULAR_Model.php');
//echo urldecode($url);

if(preg_match("/%20|%21|%22|%25|%27|%28|%29|%2A|%2B|%7B|%7C|%7D|%7E|%58|%5B|%5D|%2D|%3B|union|select|insert|update|delete|drop|alter|ASCII|set|declare|char|chr|create|SYSOBJECTS'/", $url)) {
   //echo "coencide con alguno de estos cadenas";
	header("Location: ../../sinabip.php");
}else {


	$OPT = $_GET['OPT'];

	if($OPT == 'VistaXLS'){

		$COD_ENTIDAD	= trim($_GET['TXH_SIMI_COD_ENTIDAD']);
		$ObjModel = new M_TBL_MUEBLES_UE_BIE_FT_VEHICULAR_Model;

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <table width="13533" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" id="tbl_Resultado_Lista_usuarios" class="table2excel" data-tableName="Test Table 1">

  <tr>
    <th width="35" height="20" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;" >ITEM</th>
    <th width="400" height="20" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;" >NOMBRE DE ENTIDAD</th>
    <th width="400" height="30" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">DENOMINACION</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">PLACA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CARROCERIA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">MARCA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">MODELO</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CATEGORIA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">NRO_CHASIS</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">NRO_EJES</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">NRO_MOTOR</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">NRO_SERIE</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">ANIO_FABRICACION</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">COLOR</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">COMBUSTIBLE</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">TRANSMISION</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CILINDRADA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">KILOMETRAJE</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">TARJETA_PROPIEDAD</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CILINDROS</th>
    <th width="300300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CARBURADOR</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">DISTRIBUIDOR</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">BOMBA_GASOLINA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">PURIFICADOR_AIRE</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">BOMBA_FRENOS</th>

    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">ZAPATAS_TAMBORES</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">DISCOS_PASTILLAS</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">RADIOADOR</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">VENTILADOR</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">BOMBA_AGUA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">MOTOR_ARRANQUE</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">BATERIA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">ALTERNADOR</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">BOBINA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">RELAY_ALTERNADOR</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">FAROS_DELANTEROS</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">DIRECCIONALES_DELANTERAS</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">LUCES_POSTERIORES</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">DIRECCIONALES_POSTERIORES</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">AUTO_RADIO</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">PARLANTES</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CLAXON</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CIRCUITO_LUCES</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CAJA_CAMBIOS</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">BOMBA_EMBRAGUE</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CAJA_TRANSFERENCIA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">DIFERENCIAL_TRASERO</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">DIFERENCIAL_DELANTERO</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">VOLANTE</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CANA_DIRECCION</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CREMALLERA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">ROTULAS</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">AMORTIGUADORES</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">BARRA_TORSION</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">BARRA_ESTABILIZADOR</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">LLANTAS</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CAPOT_MOTOR</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CAPOT_MALETERA</th>

    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">PARACHOQUES_DELANTEROS</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">PARACHOQUES_POSTERIORES</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">LUNAS_LATERALES</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">LUNAS_CORTAVIENTO</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">PARABRISAS_DELANTERO</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">PARABRISAS_POSTERIOR</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">TANQUE_COMBUSTIBLE</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">PUERTAS</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">ASIENTOS</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">AIRE_ACONDICIONADO</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">ALARMA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">PLUMILLAS</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">ESPEJOS</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">CINTURONES_SEGURIDAD</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">ANTENA</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">OTRAS_CARACTERISTICAS_RELEVANTES</th>
    <th width="300" bgcolor="#f4f4f4" style="text-align:left;color: rgb(101, 101, 101); font-family: Arial, Helvetica, sans-serif; font-size: 10px;">VALOR_TASACION</th>

  </tr>
  <?

		$ITEM=0;

		$Resultados	= $ObjModel->F_Exportar_Excel_FichasVehiculares($COD_ENTIDAD);
		while (odbc_fetch_row($Resultados)){

			$ITEM					= trim(utf8_encode(odbc_result($Resultados,"ITEM")));
      $NOM_ENTIDAD		= trim(utf8_encode(odbc_result($Resultados,"NOM_ENTIDAD")));
			$DENOMINACION			= trim(utf8_encode(odbc_result($Resultados,"DENOMINACION")));
			$PLACA			= trim(utf8_encode(odbc_result($Resultados,"PLACA")));
			$CARROCERIA			= trim(utf8_encode(odbc_result($Resultados,"CARROCERIA")));
			$MARCA				= trim(utf8_encode(odbc_result($Resultados,"MARCA")));
			$MODELO				= trim(utf8_encode(odbc_result($Resultados,"MODELO")));
			$CATEGORIA			= trim(utf8_encode(odbc_result($Resultados,"CATEGORIA")));
			$NRO_CHASIS		= trim(utf8_encode(odbc_result($Resultados,"NRO_CHASIS")));
			$NRO_EJES			= trim(utf8_encode(odbc_result($Resultados,"NRO_EJES")));
			$NRO_MOTOR	= trim(utf8_encode(odbc_result($Resultados,"NRO_MOTOR")));
			$NRO_SERIE			= trim(utf8_encode(odbc_result($Resultados,"NRO_SERIE")));
			$ANIO_FABRICACION		= trim(utf8_encode(odbc_result($Resultados,"ANIO_FABRICACION")));
			$COLOR	= trim(utf8_encode(odbc_result($Resultados,"COLOR")));
			$COMBUSTIBLE		= trim(utf8_encode(odbc_result($Resultados,"COMBUSTIBLE")));
			$TRANSMISION		= trim(utf8_encode(odbc_result($Resultados,"TRANSMISION")));
			$CILINDRADA				= trim(utf8_encode(odbc_result($Resultados,"CILINDRADA")));
			$KILOMETRAJE		= trim(utf8_encode(odbc_result($Resultados,"KILOMETRAJE")));
      $TARJETA_PROPIEDAD		= trim(utf8_encode(odbc_result($Resultados,"TARJETA_PROPIEDAD")));
      $CILINDROS		= trim(utf8_encode(odbc_result($Resultados,"CILINDROS")));
      $CARBURADOR		= trim(utf8_encode(odbc_result($Resultados,"CARBURADOR")));
      $DISTRIBUIDOR		= trim(utf8_encode(odbc_result($Resultados,"DISTRIBUIDOR")));
      $BOMBA_GASOLINA		= trim(utf8_encode(odbc_result($Resultados,"BOMBA_GASOLINA")));
      $PURIFICADOR_AIRE		= trim(utf8_encode(odbc_result($Resultados,"PURIFICADOR_AIRE")));
      $BOMBA_FRENOS		= trim(utf8_encode(odbc_result($Resultados,"BOMBA_FRENOS")));

      $ZAPATAS_TAMBORES		= trim(utf8_encode(odbc_result($Resultados,"ZAPATAS_TAMBORES")));
      $DISCOS_PASTILLAS		= trim(utf8_encode(odbc_result($Resultados,"DISCOS_PASTILLAS")));
      $RADIOADOR		= trim(utf8_encode(odbc_result($Resultados,"RADIOADOR")));
      $VENTILADOR		= trim(utf8_encode(odbc_result($Resultados,"VENTILADOR")));
      $BOMBA_AGUA		= trim(utf8_encode(odbc_result($Resultados,"BOMBA_AGUA")));
      $MOTOR_ARRANQUE		= trim(utf8_encode(odbc_result($Resultados,"MOTOR_ARRANQUE")));
      $BATERIA		= trim(utf8_encode(odbc_result($Resultados,"BATERIA")));
      $ALTERNADOR		= trim(utf8_encode(odbc_result($Resultados,"ALTERNADOR")));
      $BOBINA		= trim(utf8_encode(odbc_result($Resultados,"BOBINA")));
      $RELAY_ALTERNADOR		= trim(utf8_encode(odbc_result($Resultados,"RELAY_ALTERNADOR")));
      $FAROS_DELANTEROS		= trim(utf8_encode(odbc_result($Resultados,"FAROS_DELANTEROS")));
      $DIRECCIONALES_DELANTERAS		= trim(utf8_encode(odbc_result($Resultados,"DIRECCIONALES_DELANTERAS")));
      $LUCES_POSTERIORES		= trim(utf8_encode(odbc_result($Resultados,"LUCES_POSTERIORES")));
      $DIRECCIONALES_POSTERIORES		= trim(utf8_encode(odbc_result($Resultados,"DIRECCIONALES_POSTERIORES")));
      $AUTO_RADIO		= trim(utf8_encode(odbc_result($Resultados,"AUTO_RADIO")));
      $PARLANTES		= trim(utf8_encode(odbc_result($Resultados,"PARLANTES")));
      $CLAXON		= trim(utf8_encode(odbc_result($Resultados,"CLAXON")));
      $CIRCUITO_LUCES		= trim(utf8_encode(odbc_result($Resultados,"CIRCUITO_LUCES")));
      $CAJA_CAMBIOS		= trim(utf8_encode(odbc_result($Resultados,"CAJA_CAMBIOS")));
      $BOMBA_EMBRAGUE		= trim(utf8_encode(odbc_result($Resultados,"BOMBA_EMBRAGUE")));
      $CAJA_TRANSFERENCIA		= trim(utf8_encode(odbc_result($Resultados,"CAJA_TRANSFERENCIA")));
      $DIFERENCIAL_TRASERO		= trim(utf8_encode(odbc_result($Resultados,"DIFERENCIAL_TRASERO")));
      $DIFERENCIAL_DELANTERO		= trim(utf8_encode(odbc_result($Resultados,"DIFERENCIAL_DELANTERO")));
      $VOLANTE		= trim(utf8_encode(odbc_result($Resultados,"VOLANTE")));
      $CANA_DIRECCION		= trim(utf8_encode(odbc_result($Resultados,"CANA_DIRECCION")));
      $CREMALLERA		= trim(utf8_encode(odbc_result($Resultados,"CREMALLERA")));
      $ROTULAS		= trim(utf8_encode(odbc_result($Resultados,"ROTULAS")));
      $AMORTIGUADORES		= trim(utf8_encode(odbc_result($Resultados,"AMORTIGUADORES")));
      $BARRA_TORSION		= trim(utf8_encode(odbc_result($Resultados,"BARRA_TORSION")));
      $BARRA_ESTABILIZADOR		= trim(utf8_encode(odbc_result($Resultados,"BARRA_ESTABILIZADOR")));
      $LLANTAS		= trim(utf8_encode(odbc_result($Resultados,"LLANTAS")));
      $CAPOT_MOTOR		= trim(utf8_encode(odbc_result($Resultados,"CAPOT_MOTOR")));
      $CAPOT_MALETERA		= trim(utf8_encode(odbc_result($Resultados,"CAPOT_MALETERA")));

      $PARACHOQUES_DELANTEROS		= trim(utf8_encode(odbc_result($Resultados,"PARACHOQUES_DELANTEROS")));
      $PARACHOQUES_POSTERIORES		= trim(utf8_encode(odbc_result($Resultados,"PARACHOQUES_POSTERIORES")));
      $LUNAS_LATERALES		= trim(utf8_encode(odbc_result($Resultados,"LUNAS_LATERALES")));
      $LUNAS_CORTAVIENTO		= trim(utf8_encode(odbc_result($Resultados,"LUNAS_CORTAVIENTO")));
      $PARABRISAS_DELANTERO		= trim(utf8_encode(odbc_result($Resultados,"PARABRISAS_DELANTERO")));
      $PARABRISAS_POSTERIOR		= trim(utf8_encode(odbc_result($Resultados,"PARABRISAS_POSTERIOR")));
      $TANQUE_COMBUSTIBLE		= trim(utf8_encode(odbc_result($Resultados,"TANQUE_COMBUSTIBLE")));
      $PUERTAS		= trim(utf8_encode(odbc_result($Resultados,"PUERTAS")));
      $ASIENTOS		= trim(utf8_encode(odbc_result($Resultados,"ASIENTOS")));
      $AIRE_ACONDICIONADO		= trim(utf8_encode(odbc_result($Resultados,"AIRE_ACONDICIONADO")));
      $ALARMA		= trim(utf8_encode(odbc_result($Resultados,"ALARMA")));
      $PLUMILLAS		= trim(utf8_encode(odbc_result($Resultados,"PLUMILLAS")));
      $ESPEJOS		= trim(utf8_encode(odbc_result($Resultados,"ESPEJOS")));
      $CINTURONES_SEGURIDAD		= trim(utf8_encode(odbc_result($Resultados,"CINTURONES_SEGURIDAD")));
      $ANTENA		= trim(utf8_encode(odbc_result($Resultados,"ANTENA")));
      $OTRAS_CARACTERISTICAS_RELEVANTES		= trim(utf8_encode(odbc_result($Resultados,"OTRAS_CARACTERISTICAS_RELEVANTES")));
      $VALOR_TASACION		= trim(utf8_encode(odbc_result($Resultados,"VALOR_TASACION")));



			//$CODIGO_PATRIMONIAL		= iconv('UTF-8', 'windows-1252', utf8_encode(odbc_result($RS_BPA,"CODIGO_PATRIMONIAL")));

			$ITEM++;

?>
  <tr >
    <td ><?=$ITEM?></td>
    <td ><?=$NOM_ENTIDAD?></td>
    <td ><?=$DENOMINACION?></td>
    <td ><?=$PLACA?></td>
    <td ><?=$CARROCERIA?></td>
    <td ><?=$MARCA?></td>
    <td ><?=$MODELO?></td>
    <td ><?=$CATEGORIA?></td>
    <td ><?=$NRO_CHASIS?></td>
    <td ><?=$NRO_EJES?></td>
    <td ><?=$NRO_MOTOR?></td>
    <td ><?=$NRO_SERIE?></td>
    <td ><?=$ANIO_FABRICACION?></td>
    <td ><?=$COLOR?></td>
    <td ><?=$COMBUSTIBLE?></td>
    <td ><?=$TRANSMISION?></td>
    <td ><?=$CILINDRADA?></td>
    <td ><?=$KILOMETRAJE?></td>
    <td ><?=$TARJETA_PROPIEDAD?></td>
    <td ><?=$CILINDROS?></td>
    <td ><?=$CARBURADOR?></td>
    <td ><?=$DISTRIBUIDOR?></td>
    <td ><?=$BOMBA_GASOLINA?></td>
    <td ><?=$PURIFICADOR_AIRE?></td>
    <td ><?=$BOMBA_FRENOS?></td>

    <td ><?=$ZAPATAS_TAMBORES?></td>
    <td ><?=$DISCOS_PASTILLAS?></td>
    <td ><?=$RADIOADOR?></td>
    <td ><?=$VENTILADOR?></td>
    <td ><?=$BOMBA_AGUA?></td>
    <td ><?=$MOTOR_ARRANQUE?></td>
    <td ><?=$BATERIA?></td>
    <td ><?=$ALTERNADOR?></td>
    <td ><?=$BOBINA?></td>
    <td ><?=$RELAY_ALTERNADOR?></td>
    <td ><?=$FAROS_DELANTEROS?></td>
    <td ><?=$DIRECCIONALES_DELANTERAS?></td>
    <td ><?=$LUCES_POSTERIORES?></td>
    <td ><?=$DIRECCIONALES_POSTERIORES?></td>
    <td ><?=$AUTO_RADIO?></td>
    <td ><?=$PARLANTES?></td>
    <td ><?=$CLAXON?></td>
    <td ><?=$CIRCUITO_LUCES?></td>
    <td ><?=$CAJA_CAMBIOS?></td>
    <td ><?=$BOMBA_EMBRAGUE?></td>
    <td ><?=$CAJA_TRANSFERENCIA?></td>
    <td ><?=$DIFERENCIAL_TRASERO?></td>
    <td ><?=$DIFERENCIAL_DELANTERO?></td>
    <td ><?=$VOLANTE?></td>
    <td ><?=$CANA_DIRECCION?></td>
    <td ><?=$CREMALLERA?></td>
    <td ><?=$ROTULAS?></td>
    <td ><?=$AMORTIGUADORES?></td>
    <td ><?=$BARRA_TORSION?></td>
    <td ><?=$BARRA_ESTABILIZADOR?></td>
    <td ><?=$LLANTAS?></td>
    <td ><?=$CAPOT_MOTOR?></td>
    <td ><?=$CAPOT_MALETERA?></td>

    <td ><?=$PARACHOQUES_DELANTEROS?></td>
    <td ><?=$PARACHOQUES_POSTERIORES?></td>
    <td ><?=$LUNAS_LATERALES?></td>
    <td ><?=$LUNAS_CORTAVIENTO?></td>
    <td ><?=$PARABRISAS_DELANTERO?></td>
    <td ><?=$PARABRISAS_POSTERIOR?></td>
    <td ><?=$TANQUE_COMBUSTIBLE?></td>
    <td ><?=$PUERTAS?></td>
    <td ><?=$ASIENTOS?></td>
    <td ><?=$AIRE_ACONDICIONADO?></td>
    <td ><?=$ALARMA?></td>
    <td ><?=$PLUMILLAS?></td>
    <td ><?=$ESPEJOS?></td>
    <td ><?=$CINTURONES_SEGURIDAD?></td>
    <td ><?=$ANTENA?></td>
    <td ><?=$OTRAS_CARACTERISTICAS_RELEVANTES?></td>
    <td ><?=$VALOR_TASACION?></td>

  </tr>

  <? } ?>

</table>

<?
	}
}
?>
