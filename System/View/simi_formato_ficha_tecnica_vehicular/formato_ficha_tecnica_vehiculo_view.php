<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 

<style type='text/css'>
	#dv_mostrarPDF:hover{
		background-color:#DF7C13 !important;
		color:#fff;
		font-weight:bold;
	}
	#dv_mostrarPDF{
		background-color:#f4f4f4; 
     	padding:4px 10px; 
		height:25px; 
		width:120px; 
		margin:10px; 
		margin-left:10px; 
		float:right !important; 
		cursor:pointer; 
		display:none;
	}
	#dv_guardarFichaV:hover{
		background-color:#DF7C13 !important;
		color:#fff;
		font-weight:bold;
	}
</style>



<form name="frmFichaTecnica" id= "frmFichaTecnica" method="post" action="">
<table class="TABLE_border4" width="100%" cellpadding="0" cellspacing="2" bgcolor="#f4f4f4" name ="tbl_Ficha_Vehicular">
<tr>
    <td width="20%" height="10">&nbsp;</td>
    <td height="10"></td>
    <td width="20%" height="10"></td>
</tr>
<tr>
	<td>&nbsp;</td>
    <td style="text-align:right !important" height="25">
    <a href="#" onclick="Cancelar_Registro_Vehiculos_TBL_MUEBLES_UE_BIE_FT_VEHICULAR()"><img src="../webimages/iconos/btn_cerrar.png" height="18" width="56" border="0"></a>
    </td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td >
    
        <table class="TABLE_border4" width="920" bgcolor="#ffffff" >
		
		<tr> 
			<td colspan="6">
            	<div align="center" class="texto_arial_azul_n_16">
                	REGISTRO DE FICHA
                </div>
            </td>
		</tr>			 
        <tr> 
			<td colspan="6">
            	<div align="center" class="texto_arial_azul_n_16">
                	TÉCNICA DEL VEHICULO
                    <hr />
                </div>
                <br />
            </td>
		</tr>
        <tr>
        	<td colspan="6" style="text-align:left;" width="15">
            	
            	<div id="dv_mostrarPDF" class="TABLE_border4" onclick="MostrarPDF()">
                        <img src="../webimages/iconos/archivoPDF.png" height="16" 
                        align="absmiddle" width="16" border="0"/>				      					
                        Enviar a PDF
                </div>
                
                <div id="dv_guardarFichaV" class="TABLE_border4" 
                onclick="Guardar_Vehiculos_TBL_MUEBLES_UE_BIE_FT_VEHICULAR()" style="background-color:#f4f4f4; 
                        padding:4px 10px; height:25px; width:120px; margin:10px; margin-left:10px;  float:right;
                        cursor:pointer;">
                        <img src="../webimages/iconos/save.gif" height="16" align="absmiddle" width="16" border="0"/>				      					Guardar
                </div>
            </td>
            
        </tr>	
        
        <tr>
        
            <td colspan="6">
            <br />
           	  <fieldset class="TABLE_border4" style="background-color:#FFF">
					<table id="tbl_seleccionarBien" height="50" width="950" cellspacing="1" cellpadding="0" border="0">
                    	<tr>
                        	<td width="192" class="texto_arial_azul_n_11">
                            	Seleccionar Bienes a Asignar : 
                            </td>
                            <td width="711">
                            	<a href="#" onclick="Listar_ventana_popup()">
                                	<img src="../webimages/iconos/ver_icono_b.png" height="22" 
                                    	width="20" border="0">
                                </a>
                            </td>
                        </tr>
                        <tr>
                        	<td colspan="2" height="20" width="800">
                            	<hr class="hr_01" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            
            
            </td>

        </tr>		 
        
        <tr>
        <td colspan="6"><div align="center" class="texto_arial_azul_n_14"></div></td> 
        </tr>
        
        
         <tr>
            <td width="134" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">ENTIDAD</td>
            <td colspan="3"><input name="txt_entidad" readonly="readonly" id="txt_entidad" type="text" value="" style="width: 100%;"/></td>
            <td>
            	<input name="txt_COD_UE_BIEN_PATRI" id="txt_COD_UE_BIEN_PATRI" type="hidden" value="" />
            </td>
            <td><input name="txt_COD_UE_FT_VEHICULAR_hide" id="txt_COD_UE_FT_VEHICULAR_hide" type="hidden" value="" /></td>
            
            
        </tr>
        <tr>
            <td width="134" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">CODIGO PATRIMONIAL</td>
            <td width="133" ><input name="txt_codigopatrimonial" readonly="readonly" id="txt_codigopatrimonial" type="text" value="" /></td>
            <td width="186"  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">CATEGORIA</td>
            <td width="133"><input name="txt_categoria" id="txt_categoria" type="text" /></td>
            <td width="215"  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">COLOR</td>
            <td width="130"><input name="txt_color" id="txt_color" type="text" /></td>
        </tr>
        <tr>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">DENOMINACION</td> 
        <td><input name="txt_denominacion" id="txt_denominacion" type="text" /></td>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Nº DE CHASIS (VIN)</td>
        <td><input name="txt_nchasis" id="txt_nchasis" type="text" /></td>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">COMBUSTIBLE</td>
        <td><input name="txt_combustible" id="txt_combustible" type="text" /></td>
        </tr>
        <tr>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">PLACA</td>
        <td><input maxlength = "10" name="txt_placa" id="txt_placa" type="text" /></td>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Nº DE EJES</td>
        <td><input name="txt_Nejes" id="txt_Nejes" type="text" /></td>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">TRANSMISION</td>
        <td><input name="txt_transmision" id="txt_transmision" type="text" /></td>
        </tr>
        <tr>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">CARROCERIA</td>
        <td><input name="txt_carroceria" id="txt_carroceria" type="text" /></td>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Nº DE MOTOR</td>
        <td><input name="txt_Nro_motor" id="txt_Nro_motor" type="text" /></td>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">CILINDRADA</td>
        <td><input name="txt_cilindrada" id="txt_cilindrada" type="text" /></td>
        </tr>
        <tr>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">MARCA</td>
        <td><input name="txt_marca" id="txt_marca" type="text" /></td>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Nº DE SERIE</td>
        <td><input name="txt_N_serie" id="txt_N_serie" type="text" /></td>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">KILOMETRAJE</td>
        <td><input name="txt_Kilometraje" id="txt_Kilometraje" type="text" /></td>
        </tr>
        <tr>
        <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">MODELO</td>
        <td><input name="txt_Modelo" id="txt_Modelo" type="text" /></td>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">AÑO DE FABRICACION</td>
        <td><input name="txt_anio_fabricacion" id="txt_anio_fabricacion" type="text" /></td>
        <td  bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">TARJETA DE PROPIEDAD</td>
        <td><input name="txt_tarjeta_propiedad" id="txt_tarjeta_propiedad" type="text" /></td>
        </tr>
        </table>
    
    </td>
    <td></td>
</tr>
<tr>
    <td height="10"></td>
    <td height="10"></td>
    <td height="10"></td>
</tr>
<tr>
    <td></td>
    <td >
    
    <table class="TABLE_border4" width="100%">
    <tr>
    <td width="214" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">DESCRIPCION</td>
    <td style='text-align:center' colspan="2" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">APRECIACION TECNICA DEL SISTEMA</td>
    </tr>
    <tr>
    <td colspan="3" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">1. SISTEMA DE MOTOR</td>
    
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Cilindros</td>
    <td width="44">
    <input type="checkbox" name="chk_cilindros" id="chk_cilindros"> 
    <label for="chk_cilindros">S/N</label>
    </td>
    <td width="auto" ><input name="txt_cilindros" id="txt_cilindros" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Carburador / Carter</td>
    <td>
    <input type="checkbox" name="chk_carburador" id="chk_carburador">
    <label for="chk_carburador">S/N</label>
    </td>
    <td><input name="txt_carburador" id="txt_carburador" type="text" style="width:100%"/></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Distribuidor / bomba de inyeccion</td>
    <td> 
    <input type="checkbox" name="chk_distribuidorbomba" id="chk_distribuidorbomba">
    <label for="chk_distribuidorbomba">S/N</label></td>
    <td><input name="txt_distribuidorbomba" id="txt_distribuidorbomba" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Bomba de gasolina</td>
    <td>
    <input type="checkbox" name="chk_bombagasolina" id="chk_bombagasolina">
    <label for="chk_bombagasolina">S/N</label></td>
    <td><input name="txt_bombagasolina" id="txt_bombagasolina" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Purificador de aire</td>
    <td><input type="checkbox" name="chk_purificador_aire" id="chk_purificador_aire">
    <label for="chk_purificador_aire">S/N</label></td>
    <td><input name="txt_purificador_aire" id="txt_purificador_aire" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td colspan="3" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">2. SISTEMA DE FRENOS</td>
    
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Bomba de frenos</td>
    <td><input type="checkbox" name="chk_bomba_frenos" id="chk_bomba_frenos">
    <label for="chk_bomba_frenos">S/N</label></td>
    <td><input name="txt_bomba_frenos" id="txt_bomba_frenos" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Zapatas y tambores</td>
    <td><input type="checkbox" name="chk_zapatas_tambores" id="chk_zapatas_tambores">
    <label for="chk_zapatas_tambores">S/N</label>;</td>
    <td ><input name="txt_zapatas_tambores" id="txt_zapatas_tambores" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Discos y pastillas</td>
    <td><input type="checkbox" name="chk_discos_pastillas" id="chk_discos_pastillas">
    <label for="chk_discos_pastillas">S/N</label></td>
    <td><input name="txt_discos_pastillas" id="txt_discos_pastillas" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td colspan="3" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">3. SISTEMA DE REFREIGERACION</td>
    
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Radiador</td>
    <td><input type="checkbox" name="chk_radiadores" id="chk_radiadores">
    <label for="chk_radiadores">S/N</label></td>
    <td><input name="txt_radiadores" id="txt_radiadores" type="text" style="width:100%"/></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Ventilador</td>
    <td><input type="checkbox" name="chk_ventilador" id="chk_ventilador">
    <label for="chk_ventilador">S/N</label></td>
    <td><input name="txt_ventilador" id="txt_ventilador" type="text" style="width:100%"/></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Bomba de agua</td>
    <td><input type="checkbox" name="chk_bomba_agua" id="chk_bomba_agua">
    <label for="chk_bomba_agua">S/N</label></td>
    <td><input name="txt_bomba_agua" id="txt_bomba_agua" type="text" style="width:100%"/></td>
    </tr>
    <tr>
    <td colspan="3" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">4- SISTEMA ELECTRICO</td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Motor de arranque</td>
    <td><input type="checkbox" name="chk_motor_arranque" id="chk_motor_arranque">
    <label for="chk_motor_arranque">S/N</label></td>
    <td><input name="txt_motor_arranque" id="txt_motor_arranque" type="text" style="width:100%"/></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Bateria</td>
    <td><input type="checkbox" name="chk_bateria" id="chk_bateria">
    <label for="chk_bateria">S/N</label></td>
    <td><input name="txt_bateria" id="txt_bateria" type="text" style="width:100%"/></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Alternador</td>
    <td><input type="checkbox" name="chk_alternador" id="chk_alternador">
    <label for="chk_alternador">S/N</label></td>
    <td><input name="txt_alternador" id="txt_alternador" type="text" style="width:100%"/></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Bobina</td>
    <td><input type="checkbox" name="chk_bobina" id="chk_bobina">
    <label for="chk_bobina">S/N</label></td>
    <td><input name="txt_bobina" id="txt_bobina" type="text" style="width:100%"/></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Relay de alternador</td>
    <td><input type="checkbox" name="chk_relay_alternador" id="chk_relay_alternador">
    <label for="chk_relay_alternador">S/N</label></td>
    <td><input name="txt_relay_alternador" id="txt_relay_alternador" type="text" style="width:100%"/></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Faros delanteros</td>
    <td><input type="checkbox" name="chk_faros_delanteros" id="chk_faros_delanteros">
    <label for="chk_faros_delanteros">S/N</label></td>
    <td><input name="txt_faros_delanteros" id="txt_faros_delanteros" type="text" style="width:100%"/></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Direccionales delanteras</td>
    <td><input type="checkbox" name="chk_direccionales_delanteras" id="chk_direccionales_delanteras">
    <label for="chk_direccionales_delanteras">S/N</label></td>
    <td><input name="txt_direccionales_delanteras" id="txt_direccionales_delanteras" type="text" style="width:100%"/></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Luces posteriores</td>
    <td><input type="checkbox" name="chk_luces_posteriores" id="chk_luces_posteriores">
    <label for="chk_luces_posteriores">S/N</label></td>
    <td><input name="txt_luces_posteriores" id="txt_luces_posteriores" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Direccionales posteriores</td>
    <td><input type="checkbox" name="chk_direccionales_posteriores" id="chk_direccionales_posteriores">
    <label for="chk_direccionales_posteriores">S/N</label></td>
    <td><input name="txt_direccionales_posteriores" id="txt_direccionales_posteriores" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Auto radio</td>
    <td><input type="checkbox" name="chk_autoradio" id="chk_autoradio">
    <label for="chk_autoradio">S/N</label></td>
    <td><input name="txt_autoradio" id="txt_autoradio" type="text" style="width:100%"/></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Parlantes</td>
    <td><input type="checkbox" name="chk_parlantes" id="chk_parlantes">
    <label for="chk_parlantes">S/N</label></td>
    <td><input name="txt_parlantes" id="txt_parlantes" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Claxon</td>
    <td><input type="checkbox" name="chk_claxon" id="chk_claxon">
    <label for="chk_claxon">S/N</label></td>
    <td><input name="txt_claxon" id="txt_claxon" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Circuito de luces (faros, cableados)</td>
    <td><input type="checkbox" name="chk_circuito_luces" id="chk_circuito_luces">
    <label for="chk_circuito_luces">S/N</label></td>
    <td><input name="txt_circuito_luces" id="txt_circuito_luces" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td colspan="3" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">5. SISTEMA DE TRANSMISION</td>
    
    <td colspan="2" >&nbsp;</td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Caja de cambios</td>
    <td ><input type="checkbox" name="chk_caja_cambios" id="chk_caja_cambios">
    <label for="chk_caja_cambios">S/N</label></td>
    <td><input name="txt_caja_cambios" id="txt_caja_cambios" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Bomba de embrague</td>
    <td><input type="checkbox" name="chk_bomba_embrague" id="chk_bomba_embrague">
    <label for="chk_bomba_embrague">S/N</label></td>
    <td><input name="txt_bomba_embrague" id="txt_bomba_embrague" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Caja de transferencia</td>
    <td><input type="checkbox" name="chk_caja_transferencia" id="chk_caja_transferencia">
    <label for="chk_caja_transferencia">S/N</label></td>
    <td><input name="txt_caja_transferencia" id="txt_caja_transferencia" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Diferencial trasero</td>
    <td><input type="checkbox" name="chk_diferencial_trasero" id="chk_diferencial_trasero">
    <label for="chk_diferencial_trasero">S/N</label></td>
    <td><input name="txt_diferencial_trasero" id="txt_diferencial_trasero" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Diferencial delantero (4x4)</td>
    <td><input type="checkbox" name="chk_diferencial_delantero" id="chk_diferencial_delantero">
    <label for="chk_diferencial_delantero">S/N</label></td>
    <td><input name="txt_diferencial_delantero" id="txt_diferencial_delantero" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td colspan="3" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">6. SISTEMA DE DIRECCION</td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Volante</td>
    <td><input type="checkbox" name="chk_volante" id="chk_volante">
    <label for="chk_volante">S/N</label></td>
    <td><input name="txt_volante" id="txt_volante" type="text"style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Caña de direccion</td>
    <td><input type="checkbox" name="chk_cana_direccion" id="chk_cana_direccion">
    <label for="chk_cana_direccion">S/N</label></td>
    <td><input name="txt_cana_direccion" id="txt_cana_direccion" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Cremallera</td>
    <td><input type="checkbox" name="chk_cremallera" id="chk_cremallera">
    <label for="chk_cremallera">S/N</label></td>
    <td><input name="txt_cremallera" id="txt_cremallera" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Rotulas</td>
    <td><input type="checkbox" name="chk_rotulas" id="chk_rotulas">
    <label for="chk_rotulas">S/N</label>;</td>
    <td><input name="txt_rotulas" id="txt_rotulas" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td colspan="3" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">7. SISTEMA DE SUSPENSION</td>
    <td colspan="2" >&nbsp;</td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Amortiguadores / muelles</td>
    <td><input type="checkbox" name="chk_amortiguadores" id="chk_amortiguadores">
    <label for="chk_amortiguadores">S/N</label></td>
    <td><input name="txt_amortiguadores" id="txt_amortiguadores" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Barra de torsion</td>
    <td><input type="checkbox" name="chk_barra_torsion" id="chk_barra_torsion">
    <label for="chk_barra_torsion">S/N</label>;</td>
    <td><input name="txt_barra_torsion" id="txt_barra_torsion" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Barra estabilizadora</td>
    <td><input type="checkbox" name="chk_barra_estabilizadora" id="chk_barra_estabilizadora">
    <label for="chk_barra_estabilizadora">S/N</label></td>
    <td><input name="txt_barra_estabilizadora" id="txt_barra_estabilizadora" type="text"style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Llantas</td>
    <td><input type="checkbox" name="chk_llantas" id="chk_llantas">
    <label for="chk_llantas">S/N</label></td>
    <td><input name="txt_llantas" id="txt_llantas" type="text" style="width:100%" /></td>
    </tr>
    
    <tr>
    <td colspan="3" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">8. CARROCERIA</td>
    <td colspan="2" >&nbsp;</td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Capot del motor</td>
    <td><input type="checkbox" name="chk_capot_motor" id="chk_capot_motor">
    <label for="chk_capot_motor">S/N</label></td>
    <td><input name="txt_capot_motor" id="txt_capot_motor" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Capot de maletera</td>
    <td><input type="checkbox" name="chk_capot_maletera" id="chk_capot_maletera">
    <label for="chk_capot_maletera">S/N</label>;</td>
    <td><input name="txt_capot_maletera" id="txt_capot_maletera" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Parachoques delanteros</td>
    <td><input type="checkbox" name="chk_Parachoques_delanteros" id="chk_Parachoques_delanteros">
    <label for="chk_Parachoques_delanteros">S/N</label></td>
    <td><input name="txt_Parachoques_delanteros" id="txt_Parachoques_delanteros" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Parachoques posteriores</td>
    <td><input type="checkbox" name="chk_Parachoques_posteriores" id="chk_Parachoques_posteriores">
    <label for="chk_Parachoques_posteriores">S/N</label></td>
    <td><input name="txt_Parachoques_posteriores" id="txt_Parachoques_posteriores" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Lunas Laterales</td>
    <td><input type="checkbox" name="chk_Lunas_laterales" id="chk_Lunas_laterales">
    <label for="chk_Lunas_laterales">S/N</label></td>
    <td><input name="txt_Lunas_laterales" id="txt_Lunas_laterales" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Lunas cortavientos</td>
    <td><input type="checkbox" name="chk_Lunas_cortavientos" id="chk_Lunas_cortavientos">
    <label for="chk_Lunas_cortavientos">S/N</label></td>
    <td><input name="txt_Lunas_cortavientos" id="txt_Lunas_cortavientos" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Parabrisas delanteros</td>
    <td><input type="checkbox" name="chk_Parabrisas_delanteros" id="chk_Parabrisas_delanteros">
    <label for="chk_Parabrisas_delanteros">S/N</label></td>
    <td><input name="txt_Parabrisas_delanteros" id="txt_Parabrisas_delanteros" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Parabrisas posteriores</td>
    <td><input type="checkbox" name="chk_Parabrisas_posteriores" id="chk_Parabrisas_posteriores">
    <label for="chk_Parabrisas_posteriores">S/N</label></td>
    <td><input name="txt_Parabrisas_posteriores" id="txt_Parabrisas_posteriores" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Tanque de combustible</td>
    <td><input type="checkbox" name="chk_Tanque_combustible" id="chk_Tanque_combustible">
    <label for="chk_Tanque_combustible">S/N</label></td>
    <td><input name="txt_Tanque_combustible" id="txt_Tanque_combustible" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Puertas</td>
    <td><input type="checkbox" name="chk_Puertas" id="chk_Puertas">
    <label for="chk_Puertas">S/N</label></td>
    <td><input name="txt_Puertas" id="txt_Puertas" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Asientos</td>
    <td><input type="checkbox" name="chk_Asientos" id="chk_Asientos">
    <label for="chk_Asientos">S/N</label></td>
    <td><input name="txt_Asientos" id="txt_Asientos" type="text" style="width:100%" /></td>
    </tr>
    
    <tr>
    <td colspan="3" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">9. ACCESORIOS</td>
    <td colspan="2" >&nbsp;</td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Aire acondicionado</td>
    <td><input type="checkbox" name="chk_aire_acondicionado" id="chk_aire_acondicionado">
    <label for="chk_aire_acondicionado">S/N</label></td>
    <td><input name="txt_aire_acondicionado" id="txt_aire_acondicionado" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Alarma</td>
    <td><input type="checkbox" name="chk_Alarma" id="chk_Alarma">
    <label for="chk_Alarma">S/N</label>;</td>
    <td><input name="txt_Alarma" id="txt_Alarma" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Plumillas</td>
    <td><input type="checkbox" name="chk_plumillas" id="chk_plumillas">
    <label for="chk_plumillas">S/N</label></td>
    <td><input name="txt_plumillas" id="txt_plumillas" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Espejos</td>
    <td><input type="checkbox" name="chk_espejos" id="chk_espejos">
    <label for="chk_espejos">S/N</label></td>
    <td><input name="txt_espejos" id="txt_espejos" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Cinturones de seguridad</td>
    <td><input type="checkbox" name="chk_cinturones" id="chk_cinturones">
    <label for="chk_cinturones">S/N</label></td>
    <td><input name="txt_cinturones" id="txt_cinturones" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Antena</td>
    <td><input type="checkbox" name="chk_antena" id="chk_antena">
    <label for="chk_antena">S/N</label></td>
    <td><input name="txt_antena" id="txt_antena" type="text" style="width:100%" /></td>
    </tr>
    <tr>
    <td colspan="3" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">10. OTRAS CARACTERISTICAS RELEVANTES</td>
    </tr>
    <tr>
    <td colspan="3"><input name="txt_otras_caracteristicas_relev" id="txt_otras_caracteristicas_relev" type="text" style="width:950px" /></td>
    <td></td>
    </tr>
    <tr>
    <td colspan="3" bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">APRECIACION TECNICA GENERAL(*)</td>
    <td colspan="2" >&nbsp;</td>
    </tr>
    <tr>
    <td bgcolor="#8C8C8C" class="texto_arial_blanco_n_11">Valor de tasación(S/.)</td>
    <td colspan="2"><input name="txt_valor_tasacion" id="txt_valor_tasacion" type="text" style="width:740px" /></td>
    </tr>
    <tr>
    <td colspan="3">&nbsp;</td>
    </tr>
	
	<tr> 
	
		<td  colspan="2">&nbsp;</td>
		<td colspan="6" style="text-align:left;" width="15">
        <div id="dv_guardarFichaV" class="TABLE_border4" 
                onclick="Guardar_Vehiculos_TBL_MUEBLES_UE_BIE_FT_VEHICULAR()" style="background-color:#f4f4f4; 
                        padding:4px 10px; height:25px; width:120px; margin:10px; margin-left:10px;  float:right;
                        cursor:pointer;">
                        <img src="../webimages/iconos/save.gif" height="16" align="absmiddle" width="16" border="0"/>				      					Guardar
        </div>
&nbsp;&nbsp;
			
		</td>		
	
	</tr>
    
    
   
    
	<tr> 
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
	</tr>    
    </tr>
</table>
    
    </td>
    <td width="201"></td>
</tr>
<tr>
    <td height="10"></td>
    <td height="10"></td>
    <td height="10"></td>
</tr>

<tr>
    <td height="10" colspan="3">
    
    <input type="hidden" name="txtCODIGO_PATRIMONIAL" id="txtCODIGO_PATRIMONIAL"  value= "<?php  echo $_POST['DEVOLVER'] ?>"/>
    
    </td>
</tr>
</table>
    
</form>


