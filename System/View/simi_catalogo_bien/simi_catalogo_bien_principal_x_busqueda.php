<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<?
require_once('../../Controller/C_simi_grupo.php');
require_once('../../Controller/C_simi_resol_catalogo.php');
require_once('../../../utils/funciones/funciones.php');
error_reporting(0);
$oSimi_Grupo	=	new Simi_Grupo;
$oSimi_Resolucion_Catalogo	=	new Simi_Resolucion_Catalogo;

?>
<style>
.resaltarTexto{
    color: #FFFFFF;
    background-color: #01A5E3;
}
</style>

<script type="text/javascript">

jQuery.fn.extend({
    resaltar: function(busqueda, claseCSSbusqueda){
        var regex = new RegExp("(<[^>]*>)|("+ busqueda.replace(/([-.*+?^${}()|[\]\/\\])/g,"\\$1") +')', 'ig');

        var nuevoHtml=this.html(this.html().replace(regex, function(a, b, c){
            return (a.charAt(0) == "<") ? a : "<span class=\""+ claseCSSbusqueda +"\">" + c + "</span>";
        }));
        return nuevoHtml;
    }
});


function resaltarTexto(){
    $("#div_lista_bien").resaltar($("#txt_denominacion").val(), "resaltarTexto");
}
</script>

<script>

function mostrar_clases_en_Busqueda(){
	
	var busc_cbo_grupo_generico 		= $("#busc_cbo_grupo_generico").val();
		
	$.post("View/simi_grupo/listbox_clase_x_grupo.php",{
		CBO_GRUPO:busc_cbo_grupo_generico
	},function(data){
		$("#busc_CBO_CLASE").html(data);
	});
	
}

function Paginar_Bien_x_Busqueda(numpage){
	
	var busc_cbo_grupo_generico 		= $("#busc_cbo_grupo_generico").val();
	var busc_CBO_CLASE					= $("#busc_CBO_CLASE").val();
	var busc_txt_codigo_bien 			= $("#busc_txt_codigo_bien").val();
	var busc_txt_denominacion 			= $("#busc_txt_denominacion").val();
	var busc_cbo_condicion				= $("#busc_cbo_condicion").val();
	var BUSC_CBO_RESOLUCION_CATALOGO	= $("#BUSC_CBO_RESOLUCION_CATALOGO").val();

	carga_loading('div_lista_bien');
		
	$.post("View/simi_catalogo_bien/simi_catalogo_bien_principal_x_busqueda_list.php?opt=buscar_bien&page="+numpage,{
		busc_cbo_grupo_generico:busc_cbo_grupo_generico,
		busc_CBO_CLASE:busc_CBO_CLASE,
		busc_txt_codigo_bien:busc_txt_codigo_bien,
		busc_txt_denominacion:busc_txt_denominacion,
		busc_cbo_condicion:busc_cbo_condicion,
		BUSC_CBO_RESOLUCION_CATALOGO:BUSC_CBO_RESOLUCION_CATALOGO
	},function(data){
		$("#div_lista_bien").html(data);
		resaltarTexto();
	})	
}


function Quitar_Filtro(){
	
	$("#busc_cbo_grupo_generico").val('-');
	$("#busc_CBO_CLASE").val('0');
	$("#busc_txt_codigo_bien").val('');
	$("#busc_txt_denominacion").val('');
	$("#busc_cbo_condicion").val('-');
	$("#BUSC_CBO_RESOLUCION_CATALOGO").val('-');
	Paginar_Bien_x_Busqueda('1');
	
}

</script>
<br />
    <table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td valign="top" style="padding:10px 10px;">
      <table width="959" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" bgcolor="#F4F4F4">
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td width="373">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28">Grupo Genérico</td>
              <td><select name="busc_cbo_grupo_generico" id="busc_cbo_grupo_generico" style="width:260px" onchange="mostrar_clases_en_Busqueda()">
                <option value="-">:: Seleccione Grupo ::</option>
                <?
                          $Result_Resol = $oSimi_Grupo->Simi_Listar_Grupo();
      					if($Result_Resol){
      						while (odbc_fetch_row($Result_Resol)){
      							
      							$COD_GRUPO	 	= odbc_result($Result_Resol,"COD_GRUPO");
      							$NRO_GRUPO	 	= odbc_result($Result_Resol,"NRO_GRUPO");
      							$DESC_GRUPO	 	= odbc_result($Result_Resol,"DESC_GRUPO");
      							
      							$DESCRIPCION_GRUPO = $NRO_GRUPO.' '.$DESC_GRUPO;
      				?>
                <option value="<?=$COD_GRUPO?>">
                  <?=$DESCRIPCION_GRUPO?>
                  </option>
                <?
      						}
      					}
      				?>
              </select></td>
              <td width="83">Clase</td>
              <td><select id="busc_CBO_CLASE" name="busc_CBO_CLASE" class="form-control" style="width:200px" <?=$disabled_text?> >
                <option value="0" selected="selected">:: Seleccione Clase ::</option>
              </select></td>
              <td width="130" rowspan="3" valign="bottom"><table width="124" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="85" height="40"><input name="button2" type="button" class="btn btn-primary" id="button2" value="Buscar" onclick="Paginar_Bien_x_Busqueda('1')" /></td>
                  <td width="39"><a href="#" onclick="Quitar_Filtro()"><img src="../webimages/iconos/quitar_flitros.png" alt="Quitar Filtro" width="25" height="25" border="0" /></a></td>
                </tr>
              </table></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td height="28">Código del bien</td>
              <td><input type="text" name="busc_txt_codigo_bien" id="busc_txt_codigo_bien" style="width:150px" /></td>
              <td>Resolución :</td>
              <td width="228"><select id="BUSC_CBO_RESOLUCION_CATALOGO" name="BUSC_CBO_RESOLUCION_CATALOGO" class="form-control" style="width:200px" >
                <option value="-" selected="selected">:: Seleccione Resolución ::</option>
                <?


         		    $Result_RESOL_CATALOGO = $oSimi_Resolucion_Catalogo->Simi_Listar_Resolucion_Catalogo();
          			while (odbc_fetch_row($Result_RESOL_CATALOGO)){
          							
          			$COD_RESOL_CATALOGO		= odbc_result($Result_RESOL_CATALOGO,"COD_RESOL_CATALOGO");
          			$NRO_RESOLUCION	 		= utf8_encode(odbc_result($Result_RESOL_CATALOGO,"NRO_RESOLUCION"));
          			
          			if($COD_RESOL_CATALOGO_E == $COD_RESOL_CATALOGO){
          				$select_resoluc = 'selected="selected"';
          			}else{
          				$select_resoluc = '';
          			}
          			
          	  ?>
                <option value="<?=$COD_RESOL_CATALOGO?>" <?=$select_resoluc?> >
                  <?=$NRO_RESOLUCION?>
                  </option>
                <?
      			}
      			
      	  ?>
          </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td height="28"> Nombre del Bien </td>
          <td><input type="text" name="busc_txt_denominacion" id="busc_txt_denominacion" style="width:350px" /></td>
          <td>Condición :</td>
          <td><select name="busc_cbo_condicion" id="busc_cbo_condicion" style="width:200px">
            <option selected="selected" value="-">:: Seleccione Condición ::</option>
            <option value="A">ACTIVO</option>
            <option value="E">EXCLUIDO</option>
          </select></td>
        </tr>
        <tr>
          <td width="14">&nbsp;</td>
          <td width="115">&nbsp;</td>
          <td colspan="4">&nbsp;</td>
        </tr>
      </table>
        <div id="div_lista_bien">
        <? include_once('simi_catalogo_bien_principal_x_busqueda_list.php')?></div></td>
    </td>
      <td>
        <div id="div_formulario"></div>
      </td>
    </tr>
</table>
