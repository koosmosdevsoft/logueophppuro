<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 

<?
require_once('../../Controller/C_simi_grupo.php');
require_once('../../Controller/C_simi_catalogo.php');
require_once('../../Controller/C_simi_cuenta_contable.php');
require_once('../../Controller/C_simi_resol_catalogo.php');
require_once('../../../utils/funciones/funciones.php');


$oSimi_Grupo	=	new Simi_Grupo;
$oSimi_Catalogo	=	new Simi_Catalogo;
$oSimi_Cuenta_Contable	=	new Simi_Cuenta_Contable;
$oSimi_Resolucion_Catalogo	=	new Simi_Resolucion_Catalogo;

$X_ID_CATALOGO 	= isset($_GET['X_ID_CATALOGO']) ? $_GET['X_ID_CATALOGO'] : '';


if($X_ID_CATALOGO !=''){

	$result_Catalogo_E 		= $oSimi_Catalogo->Simi_Ver_Catalogo_x_Codigo($X_ID_CATALOGO);
	
	$NRO_BIEN_E	 			= utf8_encode(odbc_result($result_Catalogo_E,"NRO_BIEN"));
	
	$COD_GRUPO_E	 		= utf8_encode(odbc_result($result_Catalogo_E,"COD_GRUPO"));
	$NRO_GRUPO_E	 		= utf8_encode(odbc_result($result_Catalogo_E,"NRO_GRUPO"));
	
	$COD_CLASE_E	 		= utf8_encode(odbc_result($result_Catalogo_E,"COD_CLASE"));
	$NRO_CLASE_E	 		= utf8_encode(odbc_result($result_Catalogo_E,"NRO_CLASE"));
	
	$NRO_CORRELATIVO_E		= utf8_encode(odbc_result($result_Catalogo_E,"NRO_CORRELATIVO"));
	$DENOM_BIEN_E	 		= utf8_encode(odbc_result($result_Catalogo_E,"DENOM_BIEN"));
	$DESC_GRUPO_E	 		= utf8_encode(odbc_result($result_Catalogo_E,"DESC_GRUPO"));
	$DESC_CLASE_E	 		= utf8_encode(odbc_result($result_Catalogo_E,"DESC_CLASE"));
	
	$COD_RESOL_CATALOGO_E	= utf8_encode(odbc_result($result_Catalogo_E,"COD_RESOL_CATALOGO"));
	$CONDICION_E			= utf8_encode(odbc_result($result_Catalogo_E,"CONDICION"));
	
	$ANOTACIONES_E			= utf8_encode(odbc_result($result_Catalogo_E,"ANOTACIONES"));
	
	$FECHA_REGISTRO_E			= cambiaf_a_normal(odbc_result($result_Catalogo_E,"FECHA_REGISTRO"));
	$FECHA_MODIFICA_E			= cambiaf_a_normal(odbc_result($result_Catalogo_E,"FECHA_MODIFICA"));
	
	$disabled_text = 'disabled="disabled"';	
	$readonly_text = 'readonly="readonly"';
	
	$valor_validacion = '1';
	
  //AAG INICIO
  $NOM_FOTO_L   = odbc_result($result_Catalogo_E,"NOMB_ARCHIVO");

  if($NOM_FOTO_L !='') {
    $RUTA_FOTO = '../../repositorio_muebles/Inventarios_zip/foto_catalogo/'.$NOM_FOTO_L;      
  }else{
    $RUTA_FOTO = '../../repositorio_muebles/Inventarios_zip/foto_catalogo/image_perfil.jpg';
  }
  //AAG FIN

}
?>
<script src="../utils/javascript/jquery.maskedinput.min.js" type="text/javascript"></script>


<script type="text/javascript">
        jQuery(function($){
         	//$("#txt_num_sol_ing_a_buscar").focus();
			//$("#txt_num_sol_ing_a_buscar").mask("99999-9999");
			$("#TXT_NRO_CORRELATIVO_BIEN").mask("9999");         
        });
</script>


<!-- AAG inicio-->
<script type="text/javascript">
$(document).ready(function(){
  new AjaxUpload('txt_file_foto', {
    //se crea un modelo 
    action: 'View/simi_catalogo_bien/catalogo_bien_principal.php?operacion=Adjuntar_Foto_catalogo',
    name: 'txt_file_foto',
    onSubmit : function(file , ext){
      if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext))){
        alert('Error: Solo se permiten estos archivos .jpg|.png|.jpeg|.gif');
        return false;
      }else{
        this.setData({ 
          txt_cod_catalogo_x1 : <?php echo $X_ID_CATALOGO ?>
          
        }); 
                
      }
    },
    onComplete: function(file, response){
      
      var valor         = response.split("[*]")[0];
      var codigo        = response.split("[*]")[1];
      var nom_archivo     = response.split("[*]")[2];
      
      if(valor==1){     
        // $('#div_label_foto_personal').html('<img class="TABLE_border4" src="../../Repositorio/intranet/foto_catalogo/'+nom_archivo+'" width="85" height="100" />');
        $('#div_label_foto_personal').html('<img class="TABLE_border4" src="../../repositorio_muebles/Inventarios_zip/foto_catalogo/'+nom_archivo+'" width="85" height="100" />');
      }else{
        alert(response);  
      }
    }
  });
});
</script>
<!-- AAG fin-->

<table width="830" border="0" cellpadding="0" cellspacing="2">
  <tr>
    <td height="20" colspan="2" >
    <a style="float:right" href="#" onclick="$('#div_formulario').html('')" ><img src="../webimages/iconos/cerrar_2.png" width="20" height="20" border="0" /></a>
    </td>
  </tr>
  <tr>
    <td width="13" height="20">&nbsp;</td>
    <td width="811"><table border="0" cellpadding="0" cellspacing="0" >
      <tr>
        <td class="Titulo_02_17px">Formulario Registro de Familia de Bienes</td>
      </tr>
      <tr>
        <td><hr /></td>
      </tr>
      <tr>
        <td width="952">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="20">&nbsp;</td>
    <td><table width="784" border="0" cellpadding="0" cellspacing="3" class="TABLE_border4"  bgcolor="#f4f4f4">
      <tr>
        <td width="27">&nbsp;</td>
        <td width="716"><span class="texto_02_11">
          <input name="TXH_COD_CATALOGO" id="TXH_COD_CATALOGO" type="hidden" class="form-control" value="<?=$X_ID_CATALOGO?>" size="10" />
        </span></td>
        <td width="27">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><fieldset class="TABLE_border4">
          <legend class="texto_arial_plomo_n_12">Datos de Registro del Bien</legend>
          <table width="706" border="0" cellspacing="2" cellpadding="0">
            <tr>
              <td width="23">&nbsp;</td>
              <td width="107">&nbsp;</td>
              <td width="227">&nbsp;</td>
              <td width="98">&nbsp;</td>
              <td width="239">&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>Grupo</td>
              <td colspan="2"><select id="CBO_GRUPO" name="CBO_GRUPO" class="form-control" onchange="mostrar_nro_grupo(); mostrar_clases();" style="width:300px" <?=isset($disabled_text) ? $disabled_text : ''?> >
                <option value="0" selected="selected">:: Seleccione Grupo ::</option>
                <?
		   
   		    $Result_Grupo = $oSimi_Grupo->Simi_Listar_Grupo();
			while (odbc_fetch_row($Result_Grupo)){
							
			$COD_GRUPO_X1	 		= odbc_result($Result_Grupo,"COD_GRUPO");
			$NRO_GRUPO_X1	 		= utf8_encode(odbc_result($Result_Grupo,"NRO_GRUPO"));
			$DESC_GRUPO_X1	 		= utf8_encode(odbc_result($Result_Grupo,"DESC_GRUPO"));
			
			if(isset($COD_GRUPO_E)&& $COD_GRUPO_E==$COD_GRUPO_X1){
				$select_2 = 'selected="selected"';
			}else{
				$select_2 = '';
			}
	  ?>
                <option value="<?=$COD_GRUPO_X1?>" <?=$select_2?>>
                  <?=$NRO_GRUPO_X1?>
                  -
                  <?=$DESC_GRUPO_X1?>
                  </option>
                <?
			}
	  ?>
              </select></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>Clase</td>
              <td colspan="2"><select id="CBO_CLASE" name="CBO_CLASE" class="form-control" onchange="mostrar_nro_clase(); generar_correlativo_bien();" style="width:300px" <?=isset($disabled_text)? $disabled_text : ''?> >
                <option value="0" selected="selected">:: Seleccione Clase ::</option>
                <?
if($X_ID_CATALOGO !=''){	
	$RESULT_Clase_x_Grupo_X = $oSimi_Grupo->Simi_Listar_Clase_x_Grupo($COD_GRUPO_E);
	while (odbc_fetch_row($RESULT_Clase_x_Grupo_X)){
		$COD_CLASE_A	 	= odbc_result($RESULT_Clase_x_Grupo_X,"COD_CLASE");
		$NRO_CLASE_A	 	= utf8_encode(odbc_result($RESULT_Clase_x_Grupo_X,"NRO_CLASE"));
		$DESC_CLASE_A	 	= utf8_encode(odbc_result($RESULT_Clase_x_Grupo_X,"DESC_CLASE"));
		
		if($COD_CLASE_E == $COD_CLASE_A){
			$select_clase = 'selected="selected"';
		}else{
			$select_clase = '';
		}
		
?>
                <option value="<?=$COD_CLASE_A?>" <?=$select_clase?> >
                  <?=$NRO_CLASE_A?>
                  -
                  <?=$DESC_CLASE_A?>
                  </option>
                <? 
	} 
}
?>
              </select></td>
              <td><input type="hidden" name="txt_validar_num_bien" id="txt_validar_num_bien" value="<?=$valor_validacion?>" /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>Código Bien</td>
              <td><input name="TXT_NRO_GRUPO" type="text" class="form-control" id="TXT_NRO_GRUPO" style="width:30px"  value="<?=!empty($NRO_GRUPO_E) ? $NRO_GRUPO_E : ''?>" readonly="readonly" />
                <input name="TXT_NRO_CLASE" type="text" class="form-control" id="TXT_NRO_CLASE" style="width:30px"  value="<?= !empty($NRO_CLASE_E) ? $NRO_CLASE_E : ''?>" readonly="readonly" />
                <input name="TXT_NRO_CORRELATIVO_BIEN" type="text" class="form-control" id="TXT_NRO_CORRELATIVO_BIEN" style="width:100px"  value="<?=$NRO_CORRELATIVO_E?>" <?=isset($readonly_text) ? $readonly_text : ''?> /></td>
              <td><a id="btn_validar" href="#" onclick="validar_correlativo()"><img src="../webimages/iconos/boton_validar.png" width="58" height="19" border="0" /></a></td>
              <td><div id="div_disponibilidad_bien"></div></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>Nombre del Bien</td>
              <td colspan="3"><input name="TXT_DESC_TIP_BIEN" type="text" class="form-control" id="TXT_DESC_TIP_BIEN" value="<?= !empty($DENOM_BIEN_E) ? $DENOM_BIEN_E : ''?>" style="width:550px"/></td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td>Resolución</td>
              <td colspan="2"><select id="CBO_RESOLUCION_CATALOGO" name="CBO_RESOLUCION_CATALOGO" class="form-control" style="width:180px" >
                <option value="-" selected="selected">:: Seleccione Resolución ::</option>
                <?


   		    $Result_RESOL_CATALOGO = $oSimi_Resolucion_Catalogo->Simi_Listar_Resolucion_Catalogo();
			while (odbc_fetch_row($Result_RESOL_CATALOGO)){
							
			$COD_RESOL_CATALOGO		= odbc_result($Result_RESOL_CATALOGO,"COD_RESOL_CATALOGO");
			$NRO_RESOLUCION	 		= utf8_encode(odbc_result($Result_RESOL_CATALOGO,"NRO_RESOLUCION"));
			
			if(isset($COD_RESOL_CATALOGO_E) && ($COD_RESOL_CATALOGO_E == $COD_RESOL_CATALOGO)){
				$select_resoluc = 'selected="selected"';
			}else{
				$select_resoluc = '';
			}
			
	  ?>
                <option value="<?=$COD_RESOL_CATALOGO?>" <?=$select_resoluc?> >
                  <?=$NRO_RESOLUCION?>
                  </option>
                <?
			}
			
	  ?>
              </select></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>Condición</td>
              <td colspan="2"><select name="cbo_condicion_reg" id="cbo_condicion_reg" style="width:180px">
                <option selected="selected" value="-">:: SELECCIONE ::</option>
                <?
                if(isset($CONDICION_E) && ($CONDICION_E == 'A')){
					$COND_A = 'selected="selected"';
					$COND_E = '';
				}else{
					$COND_A = '';
					$COND_E = 'selected="selected"';
				}
				?>
                <option value="A" <?=$COND_A?> >ACTIVO</option>
                <option value="E" <?=$COND_E?>>EXCLUIDO</option>
                </select></td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td valign="top">Anotaciones</td>
              <td colspan="3"><textarea name="txt_anotaciones" rows="12" class="form-control" id="txt_anotaciones" style="width:550px"><?= !empty($ANOTACIONES_E) ? $ANOTACIONES_E : ''?></textarea></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>Fecha Registro</td>
              <td><input name="txt_fecha_Reg" type="text" disabled="disabled" class="form-control" id="txt_fecha_Reg" style="width:150px"  value="<?=!empty($FECHA_REGISTRO_E) ? $FECHA_REGISTRO_E : ''?>" readonly="readonly" <?= !empty($readonly_text) ? $readonly_text  : ''?> /></td>
              <td>Fecha Actualizado</td>
              <td><input name="txt_fecha_Act" type="text" disabled="disabled" class="form-control" id="txt_fecha_Act" style="width:150px"  value="<?= !empty($FECHA_MODIFICA_E) ? $FECHA_MODIFICA_E : '' ?>" readonly="readonly" <?= !empty($readonly_text) ? $readonly_text : ''?> /></td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
          </table>
<!-- AAG INICIO -->
          <table width="622" border="0" cellpadding="0" cellspacing="3">
            <tr>
              <td>&nbsp;</td>
              <td width="347">&nbsp;</td>
            </tr>
            <tr>
              <td width="100" rowspan="3" valign="top">&nbsp;</td>
              <td valign="top" bgcolor="#D6D6D6" class="texto_arial_plomito_11_N TABLE_border4" style="padding: 8px">Adjuntar Foto :</td>
              <td rowspan="3" valign="top">
                <div id="div_label_foto_personal">
                  <img src="<?php echo !empty($RUTA_FOTO) ? $RUTA_FOTO : ''?>" width="85" height="100" class="TABLE_border4" />
                </div>
              </td>
            </tr>
            <tr>
              <td valign="top" style="padding: 21px 8px; background-color:#FFFFFF" class="TABLE_border4"><input type="file" name="txt_file_foto" id="txt_file_foto"  /></td>
            </tr>
            <tr>
              <td valign="top" style="padding: 8px">Dimensión (85 x 100) pixeles</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>              
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>              
              <td>&nbsp;</td>
            </tr>
          </table>
<!-- AAG FIN -->
        </fieldset></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><input name="button2" type="button" class="btn btn-primary" id="button2" value="Guardar" onclick="Guardar_Catalogo()" />
         </td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>


  <tr>
    <td height="30" colspan="2"><hr class="linea_separador_01" /></td>
  </tr>
</table>
