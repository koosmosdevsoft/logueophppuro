<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="../utils/css/stylesPaginacion.css"/>

<?
require_once('../../Controller/C_simi_catalogo.php');
require_once('../../../utils/funciones/funciones.php');

require_once("../../Controller/BaseBD_PDO.php");
require_once('../../Model/BaseBD_PDOModel.php');
require_once('../../Controller/BienPatrimonialController.php');
require_once('../../Model/BienPatrimonialModel.php'); 

$oSimi_Catalogo	=	new Simi_Catalogo;
$objModel = new Simi_Catalogo;
$objModel2 = new BienPatrimonialModel();
$opt = $_GET['opt'];

if($opt == 'buscar_bien'){			

	$COD_GRUPO 			= convertir_a_utf8($_REQUEST['busc_cbo_grupo_generico']);
	$COD_CLASE 			= convertir_a_utf8($_REQUEST['busc_CBO_CLASE']);
	
	$NRO_BIEN 			= convertir_a_utf8($_REQUEST['busc_txt_codigo_bien']);
	$DENOM_BIEN 		= convertir_a_utf8($_REQUEST['busc_txt_denominacion']);
	$CONDICION 			= $_REQUEST['busc_cbo_condicion'];
	$COD_RESOL_CATALOGO = $_REQUEST['BUSC_CBO_RESOLUCION_CATALOGO'];
	
}else{
	$COD_GRUPO 			= '-';
	$COD_CLASE 			= '0';
	$NRO_BIEN 			= '';
	$DENOM_BIEN 		= '';
	$CONDICION			= '-';
	$COD_RESOL_CATALOGO	= '-';
}
?>
<table width="962" border="0" cellspacing="1" cellpadding="0">
  <tr>
    <td width="946">&nbsp;</td>
  </tr>
  <tr>
    <td class="texto_arial_azul_n_16">CATALOGO  DE LOS BIENES MUEBLES</td>
  </tr>
</table>

<BR />

<table width="1088" border="0" cellpadding="0" cellspacing="0" id="tbl_resultado_BIEN" >
  <tr class="texto_arial_azul_n_12">
    <th width="39" height="25" bgcolor="#F4F4F4" class="estilo_nom_campo TABLE_border4">ITEM</th>
    <th width="75" bgcolor="#F4F4F4" class="estilo_nom_campo TABLE_border4">CÓDIGO</th>
    <th width="285" bgcolor="#F4F4F4" class="estilo_nom_campo TABLE_border4">DENOMINACIÓN</th>
    <th width="264" bgcolor="#F4F4F4" class="estilo_nom_campo TABLE_border4">GRUPO</th>
    <th width="172" bgcolor="#F4F4F4" class="estilo_nom_campo TABLE_border4">CLASE</th>
    <th width="159" bgcolor="#F4F4F4" class="estilo_nom_campo TABLE_border4">RESOLUCIÓN</th>
    <th width="94" bgcolor="#F4F4F4" class="estilo_nom_campo TABLE_border4">CONDICIÓN</th>
  </tr>
  <?

	$pageNum = $_GET['page'];
	
	$pageNum = ($pageNum == '') ? 1: $pageNum;
	
	$LIMITE = 30;
	$TOTAL_LINK = 8;
  
  /*
	$RS = $oSimi_Catalogo->Total_Catalogo_x_Denominacion_Bien_BUSQUEDA(
    $COD_GRUPO, $COD_CLASE, $NRO_BIEN, $DENOM_BIEN, $CONDICION, $COD_RESOL_CATALOGO);
	$TOTAL_REGISTRO = odbc_result($RS,"TOT_REG");
  */
  
  /* CREACION:        WILLIAMS ARENAS */
  /* FECHA CREACION:  25-05-2020 */
  /* DESCRIPCION:     OBTENER TOTAL DE REGISTROS DE CATALOGO */
  $data = array();      
  $dataModel_01 = [
    'COD_GRUPO'         =>$COD_GRUPO,
    'COD_CLASE'         =>$COD_CLASE,
    'NRO_BIEN'          =>$NRO_BIEN,
    'DENOM_BIEN'        =>$DENOM_BIEN,
    'CONDICION'         =>$CONDICION,
    'COD_RESOL_CATALOGO'=>$COD_RESOL_CATALOGO
  ];
  
  $data['DataTotalBusqueda'] = $objModel2->Total_Catalogo_x_Denominacion_Bien_BUSQUEDA_PDO($dataModel_01);
  $TOTAL_REGISTRO = $data['DataTotalBusqueda'][0]['TOT_REG'];
  /* FIN DE OBTENER TOTAL DE REGISTROS DE CATALOGO */



	$TOTAL_PAGINAS = ceil($TOTAL_REGISTRO / $LIMITE);
	
	$INI = ($pageNum == '1')? ($pageNum * $LIMITE - ($LIMITE)) : ($pageNum * $LIMITE - ($LIMITE))+1 ;
	$FIN = ($pageNum == '1')? ($INI + $LIMITE) : ($INI + $LIMITE)-1 ;
	
/*
    $Result_Catalogo = $oSimi_Catalogo->LISTA_Catalogo_x_Denominacion_Bien_BUSQUEDA(
      $INI, $FIN, $COD_GRUPO, $COD_CLASE, $NRO_BIEN, $DENOM_BIEN, $CONDICION, $COD_RESOL_CATALOGO);
*/          

/* CREACION:        WILLIAMS ARENAS */
/* FECHA CREACION:  25-05-2020 */
/* DESCRIPCION:     OBTENER TOTAL DE REGISTROS DE PREDIOS */
$data = array();      
  $dataModel_01 = [
    'INI'               =>$INI,
    'FIN'               =>$FIN,
    'COD_GRUPO'         =>$COD_GRUPO,
    'COD_CLASE'         =>$COD_CLASE,
    'NRO_BIEN'          =>$NRO_BIEN,
    'DENOM_BIEN'        =>$DENOM_BIEN,
    'CONDICION'         =>$CONDICION,
    'COD_RESOL_CATALOGO'=>$COD_RESOL_CATALOGO
  ];
  
  $data['DataListadoBusqueda'] = $objModel2->LISTA_Catalogo_x_Denominacion_Bien_BUSQUEDA_PDO($dataModel_01);
  $Result_Catalogo = $data['DataListadoBusqueda'];
  /* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */

    if($Result_Catalogo) foreach ($Result_Catalogo as $catalogo):					
						
							$ROW_NUMBER_ID	 	= $catalogo['ROW_NUMBER_ID'];
							$ID_CATALOGO	 	= $catalogo['ID_CATALOGO'];
							$NRO_BIEN	 		= utf8_encode($catalogo['NRO_BIEN']);
							$NRO_GRUPO	 		= utf8_encode($catalogo['NRO_GRUPO']);
							$NRO_CLASE	 		= utf8_encode($catalogo['NRO_CLASE']);
							$NRO_CORRELATIVO	= utf8_encode($catalogo['NRO_CORRELATIVO']);
							$DENOM_BIEN	 		= utf8_encode($catalogo['DENOM_BIEN']);
							$DESC_GRUPO	 		= utf8_encode($catalogo['DESC_GRUPO']);
							$DESC_CLASE	 		= utf8_encode($catalogo['DESC_CLASE']);
							$CONDICION	 		= utf8_encode($catalogo['CONDICION']);
							$NRO_RESOLUCION	 	= utf8_encode($catalogo['NRO_RESOLUCION']);
							
							if($CONDICION == 'A'){								
								$DESC_CONDICON = 'ACTIVO';								
							}else if($CONDICION == 'E'){								
								$DESC_CONDICON = 'EXCLUIDO';								
							}
	?>
  <tr class="texto_arial_plomo_n_12" id="TR_<?=$ID_CATALOGO?>" style="cursor: default;" onmouseover="mOvr2(this)" onmouseout="mOut2(this)" onclick="Mostrar_Formulario_catalogo('<?=$ID_CATALOGO?>')" >
    <td height="22" class="estilo_nom_registros"><?=$ROW_NUMBER_ID?></td>
    <td height="22" class="estilo_nom_registros"><?=$NRO_BIEN?></td>
    <td class="estilo_nom_registros"><?=$DENOM_BIEN?></td>
    <td class="estilo_nom_registros"><?=$DESC_GRUPO?></td>
    <td class="estilo_nom_registros"><?=$DESC_CLASE?></td>
    <td class="estilo_nom_registros"><?=$NRO_RESOLUCION?></td>
    <td class="estilo_nom_registros"><?=$DESC_CONDICON?></td>
  </tr>
  <?php
  
  endforeach;
					
	?>
</table>


<script type="text/javascript">
  function Mostrar_Formulario_catalogo(ID_CATALOGO){
  // alert('prueba')
  var ID_TR = 'TR_'+ID_CATALOGO;
  
  ajaxpage("View/simi_catalogo_bien/catalogo_bien_form_img.php?X_ID_CATALOGO="+ID_CATALOGO,"div_formulario") ;
  
}

</script>
<div id="TBL_PAGINACION">
    <table width="1125" border="0" cellspacing="1" cellpadding="0">
      <tr>
        <td height="20" colspan="2"><HR /></td>
      </tr>
      <tr>
        <td width="954"><div id="div_paginacion" >
          <?	
        if ($TOTAL_PAGINAS > 1) {
            echo '<div class="pagination">';
            echo '<ul>';
                echo '<li><a href = "javascript: Paginar_Bien_x_Busqueda('.chr(39).'1'.chr(39).')" class="paginate" >Inicial</a></li>';
                if ($pageNum != 1)
                    echo '<li><a href = "javascript: Paginar_Bien_x_Busqueda('.chr(39).($pageNum-1).chr(39).')" class="paginate" ><<</a></li>';
                    
                    $page_actual_ini = (($pageNum - $TOTAL_LINK) <= 0) ?  1 : ($pageNum - $TOTAL_LINK);
                    $page_actual_fin = (($pageNum + $TOTAL_LINK) > $TOTAL_PAGINAS) ? $TOTAL_PAGINAS : ($pageNum + $TOTAL_LINK);
                    
                        for ($i=$page_actual_ini;$i<=$page_actual_fin;$i++) {
                            if ($pageNum == $i)
                                //si muestro el índice de la página actual, no coloco enlace
                                echo '<li class="active"><a>'.$i.'</a></li>';
                            else
                                //si el índice no corresponde con la página mostrada actualmente,
                                //coloco el enlace para ir a esa página
                                echo '<li><a href = "javascript: Paginar_Bien_x_Busqueda('.chr(39).$i.chr(39).')" class="paginate" ">'.$i.'</a></li>';
                        }
                if ($pageNum != $TOTAL_PAGINAS)
                     echo '<li><a href = "javascript: Paginar_Bien_x_Busqueda('.chr(39).($pageNum+1).chr(39).')" class="paginate" >> ></a></li>';
                echo '<li><a href = "javascript: Paginar_Bien_x_Busqueda('.chr(39).$TOTAL_PAGINAS.chr(39).')" class="paginate" >Final</a></li>';
           echo '</ul>';
           echo '</div>';
        }
    
    
    ?>
        </div></td>
        <td width="168">Total Registros:
          <?=$TOTAL_REGISTRO?></td>
      </tr>
  </table>
</div>