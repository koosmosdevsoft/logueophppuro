<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<?
require_once('../../Controller/C_simi_grupo.php');
require_once('../../Controller/C_simi_resol_catalogo.php');
require_once('../../../utils/funciones/funciones.php');

$oSimi_Grupo	=	new Simi_Grupo;
$oSimi_Resolucion_Catalogo	=	new Simi_Resolucion_Catalogo;

?>
<style>
.resaltarTexto{
    color: #FFFFFF;
    background-color: #01A5E3;
}
</style>

<script type="text/javascript">
jQuery.fn.extend({
    resaltar: function(busqueda, claseCSSbusqueda){
        var regex = new RegExp("(<[^>]*>)|("+ busqueda.replace(/([-.*+?^${}()|[\]\/\\])/g,"\\$1") +')', 'ig');

        var nuevoHtml=this.html(this.html().replace(regex, function(a, b, c){
            return (a.charAt(0) == "<") ? a : "<span class=\""+ claseCSSbusqueda +"\">" + c + "</span>";
        }));
        return nuevoHtml;
    }
});


function resaltarTexto(){
    $("#div_lista_bien").resaltar($("#txt_denominacion").val(), "resaltarTexto");
}
</script>

<script>

function mostrar_clases_en_Busqueda(){
	
	var busc_cbo_grupo_generico 		= $("#busc_cbo_grupo_generico").val();
		
	$.post("View/simi_grupo/listbox_clase_x_grupo.php",{
		CBO_GRUPO:busc_cbo_grupo_generico
	},function(data){
		$("#busc_CBO_CLASE").html(data);
	});
	
}

function Paginar_Bien(numpage){
	
	var busc_cbo_grupo_generico 		= $("#busc_cbo_grupo_generico").val();
	var busc_CBO_CLASE					= $("#busc_CBO_CLASE").val();
	var busc_txt_codigo_bien 			= $("#busc_txt_codigo_bien").val();
	var busc_txt_denominacion 			= $("#busc_txt_denominacion").val();
	var busc_cbo_condicion				= $("#busc_cbo_condicion").val();
	var BUSC_CBO_RESOLUCION_CATALOGO	= $("#BUSC_CBO_RESOLUCION_CATALOGO").val();

	carga_loading('div_lista_bien');
		
	$.post("View/simi_catalogo_bien/simi_catalogo_bien_principal_list.php?opt=buscar_bien&page="+numpage,{
		busc_cbo_grupo_generico:busc_cbo_grupo_generico,
		busc_CBO_CLASE:busc_CBO_CLASE,
		busc_txt_codigo_bien:busc_txt_codigo_bien,
		busc_txt_denominacion:busc_txt_denominacion,
		busc_cbo_condicion:busc_cbo_condicion,
		BUSC_CBO_RESOLUCION_CATALOGO:BUSC_CBO_RESOLUCION_CATALOGO
	},function(data){
		$("#div_lista_bien").html(data);
		resaltarTexto();
	})	
}



function mostrar_nro_grupo(){
	var CBO_GRUPO 		= $("#CBO_GRUPO").val();
	$.post("Model/M_simi_grupo.php?operacion=Ver_Datos_Grupo",{ 
		CBO_GRUPO:CBO_GRUPO
	},function(response){
		
		var NRO_GRUPO 	= response.split("[*]")[0];
		var DESC_GRUPO	= response.split("[*]")[1];
			
		$("#TXT_NRO_GRUPO").val(NRO_GRUPO);
	})
}



function mostrar_clases(){
	var CBO_GRUPO 		= $("#CBO_GRUPO").val();
	
	$.post("View/simi_grupo/listbox_clase_x_grupo.php",{
		CBO_GRUPO:CBO_GRUPO
	},function(data){
		$("#CBO_CLASE").html(data);
	})		
}


function mostrar_nro_clase(){
	var CBO_CLASE 		= $("#CBO_CLASE").val();
	$.post("Model/M_simi_clases.php?operacion=Ver_Datos_Clase",{ 
		CBO_CLASE:CBO_CLASE
	},function(response){
		
		var NRO_CLASE 	= response.split("[*]")[0];
		var DESC_CLASE	= response.split("[*]")[1];
			
		$("#TXT_NRO_CLASE").val(NRO_CLASE);
	})
}



function generar_correlativo_bien(){
	
	var TXH_COD_CATALOGO	= $("#TXH_COD_CATALOGO").val();
	var CBO_GRUPO			= $("#CBO_GRUPO").val();
	var CBO_CLASE			= $("#CBO_CLASE").val();
	
	$.post("Model/M_simi_catalogo.php?operacion=Generar_Correlativo_Bien",{ 
		TXH_COD_CATALOGO:TXH_COD_CATALOGO,
		CBO_GRUPO:CBO_GRUPO,
		CBO_CLASE:CBO_CLASE
	},function(response){
					
		var valor 				= response.split("[*]")[0];
		var codigo 				= response.split("[*]")[1];
						
		if(valor==1){			
			$("#TXT_NRO_CORRELATIVO_BIEN").val(codigo);
		}else{
			alert(response);
		}
		
	})
	
}


function Guardar_Catalogo(){
	
	var TXH_ID_USUARIO 		= $("#TXH_ID_USUARIO").val();
	var TXH_COD_CATALOGO 	= $("#TXH_COD_CATALOGO").val();
	
	var CBO_GRUPO	= $("#CBO_GRUPO").val();
	var CBO_CLASE	= $("#CBO_CLASE").val();
	
	var TXT_NRO_GRUPO				= $("#TXT_NRO_GRUPO").val();
	var TXT_NRO_CLASE				= $("#TXT_NRO_CLASE").val();
	var TXT_NRO_CORRELATIVO_BIEN	= $("#TXT_NRO_CORRELATIVO_BIEN").val();
	
	var TXT_DESC_TIP_BIEN		= $("#TXT_DESC_TIP_BIEN").val();
	var CBO_RESOLUCION_CATALOGO	= $("#CBO_RESOLUCION_CATALOGO").val();
	
	var txt_anotaciones	= $("#txt_anotaciones").val();
	
	var cbo_condicion_reg	= $("#cbo_condicion_reg").val();
	var txt_validar_num_bien	= $("#txt_validar_num_bien").val();
		
	if(CBO_GRUPO == '0'){
		alert('Seleccione Grupo');
		 $("#CBO_GRUPO").focus();
		 
	}else if(CBO_CLASE == '0'){
		alert('Seleccione Clase');
		 $("#CBO_CLASE").focus();
		 
	}else if(TXT_NRO_CORRELATIVO_BIEN == ''){
		alert('Ingrese número correlativo del bien');
		 $("#TXT_NRO_CORRELATIVO_BIEN").focus();
		 
	}else if(TXT_DESC_TIP_BIEN == ''){
		alert('Ingrese Nombre del Tipo del Bien');
		 $("#TXT_DESC_TIP_BIEN").focus();
		 
	}else if(CBO_RESOLUCION_CATALOGO == '-'){
		alert('Seleccione Resolución del Catálogo');
		$("#CBO_RESOLUCION_CATALOGO").focus();
		 
	}else if(cbo_condicion_reg == '-'){
		alert('Seleccione condición del Bien');
		$("#cbo_condicion_reg").focus();
		 
	}else if(txt_anotaciones == ''){
		alert('Ingrese Anotaciones');
		$("#txt_anotaciones").focus();
		 
	}else if(txt_validar_num_bien == ''){
		alert('Aún no ha validado el Código del Bien');
		$("#btn_validar").focus();
		 
	}else if(!(txt_validar_num_bien == '1' || txt_validar_num_bien == '6' || txt_validar_num_bien == '7')){//DISPONIBLE
		alert('Ya existe este código del bien, verifique nuevamente');
		$("#btn_validar").focus();
		 
	}else{

		carga_loading('div_lista_bien');
		
		$.post("Model/M_simi_catalogo.php?operacion=Guardar_Catalogo",{ 
			TXH_ID_USUARIO:TXH_ID_USUARIO,
			TXH_COD_CATALOGO:TXH_COD_CATALOGO,
			CBO_GRUPO:CBO_GRUPO,
			CBO_CLASE:CBO_CLASE,
			TXT_NRO_GRUPO:TXT_NRO_GRUPO,
			TXT_NRO_CLASE:TXT_NRO_CLASE,
			TXT_NRO_CORRELATIVO_BIEN:TXT_NRO_CORRELATIVO_BIEN,
			TXT_DESC_TIP_BIEN:TXT_DESC_TIP_BIEN,
			CBO_RESOLUCION_CATALOGO:CBO_RESOLUCION_CATALOGO,
			cbo_condicion_reg:cbo_condicion_reg,
			txt_anotaciones:txt_anotaciones
		},function(data){
			
			if(data==1){
				mostrar_lista_Catalogo();				
				$("#div_formulario").html('');
			}else{
				alert(data);
			}
			
		})

	}
	
}


function mostrar_lista_Catalogo(){
	$.post("View/simi_catalogo_bien/simi_catalogo_bien_principal_list.php",{
	},function(data){
		$("#div_lista_bien").html(data);
	})
}


function Mostrar_Formulario(ID_CATALOGO){
	
	var ID_TR = 'TR_'+ID_CATALOGO;
	
	ajaxpage("View/simi_catalogo_bien/simi_catalogo_bien_principal_form.php?X_ID_CATALOGO="+ID_CATALOGO,"div_formulario") ;
	
	var ulA = document.getElementById("tbl_Resultado_Lista_Familia"); 
	var liNodesA = ulA.getElementsByTagName("TR"); 
	var sTot =  liNodesA.length;
	
	var color = "";
	var obj = "";
	
	for( var s = 1; s <= sTot; s++ ){ 
		obj = liNodesA[s].id;
		
		if(ID_TR == obj){
			color = "#e2f2ff";
		}else{
			color = "";
		}
				
		$("#"+obj+"").css("background-color",color);
		
	}
}



function Eliminar_Catalogo(ID_CATALOGO){
	
	var TXH_ID_USUARIO 		= $("#TXH_ID_USUARIO").val();
	
	if(confirm('¿Confirma eliminar este tipo de Bien?')){
		
		carga_loading('div_lista_bien');
		
		$.post("Model/M_simi_catalogo.php?operacion=Eliminar_Catalogo",{ 
			ID_CATALOGO:ID_CATALOGO,
			TXH_ID_USUARIO:TXH_ID_USUARIO
		},function(data){
			
			if(data==1){
				mostrar_lista_Catalogo();				
				$("#div_formulario").html('');
			}else{
				alert(data);
			}
			
		});
	}
		
}



function Quitar_Filtro(){
	
	$("#busc_cbo_grupo_generico").val('-');
	$("#busc_CBO_CLASE").val('0');
	$("#busc_txt_codigo_bien").val('');
	$("#busc_txt_denominacion").val('');
	$("#busc_cbo_condicion").val('-');
	$("#BUSC_CBO_RESOLUCION_CATALOGO").val('-');
	Paginar_Bien('1');	
}



function validar_correlativo(){
	
	var TXH_COD_CATALOGO	=  $("#TXH_COD_CATALOGO").val();
		
	var TXT_NRO_GRUPO 		=  $("#TXT_NRO_GRUPO").val();
	var TXT_NRO_CLASE 		=  $("#TXT_NRO_CLASE").val();
	var TXT_NRO_CORRELATIVO_BIEN =  $("#TXT_NRO_CORRELATIVO_BIEN").val();
	
	//var txt_validar_num_bien 	=  $("#txt_validar_num_bien").val();
	
	/*******************************************************************/
	
	if(TXT_NRO_GRUPO == ''){
		
		alert('Seleccione Grupo');
		$("#CBO_GRUPO").focus();
		
	}else if(TXT_NRO_CLASE == ''){
		
		alert('Seleccione Clase');
		$("#CBO_CLASE").focus();
		
	}else{
		
		carga_loading('div_disponibilidad_bien');
			
		$.post("Model/M_simi_catalogo.php?operacion=Validar_Nro_Bien",{ 
			TXH_COD_CATALOGO:TXH_COD_CATALOGO,
			TXT_NRO_GRUPO:TXT_NRO_GRUPO,
			TXT_NRO_CLASE:TXT_NRO_CLASE,
			TXT_NRO_CORRELATIVO_BIEN:TXT_NRO_CORRELATIVO_BIEN
		},function(response){
			
			var valor		= response.split("[*]")[0];
			var texto	= response.split("[*]")[1];
			
			$("#txt_validar_num_bien").val(valor);
			$("#div_disponibilidad_bien").html(texto);
			
			
			if(valor == 1 || valor == 6 || valor == 7){//DISPONIBLE
				$("#div_disponibilidad_bien").css({"color": "#106a9e", "padding": "5px", "font-weight" : "bold", "font-size" : "12px", "border": "1px solid #999999", "width": "220PX" });
			}else{ //no disponible
				$("#div_disponibilidad_bien").css({"color": "red", "padding": "5px", "font-weight" : "bold", "font-size" : "12px", "border": "1px solid #999999", "width": "220PX" });
			}
				
			
		});

	}

}

</script>

    <table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td style="width:960px" valign="top">
      <table width="953" border="0" cellspacing="3" cellpadding="0">
        <tr>
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td width="767"  class="Titulo_plomo_25px"> Familia de Bienes</td>
          <td width="177"  class="Titulo_plomo_25px"><input name="button" type="button" class="btn btn-primary" id="button" value="Agregar bien al Catálogo" onclick='ajaxpage("View/simi_catalogo_bien/simi_catalogo_bien_principal_form.php","div_formulario")' /></td>
        </tr>
        <tr>
          <td colspan="2"  class="Titulo_02_17px"><hr /></td>
        </tr>
        <tr>
          <td colspan="2"  class="Titulo_02_17px">&nbsp;</td>
        </tr>
      </table>
      <table width="954" border="0" cellpadding="0" cellspacing="2" class="FrmBuscadorFlotante TABLE_border4">
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td width="369">&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td height="28">Grupo Genérico</td>
          <td>
          <select name="busc_cbo_grupo_generico" id="busc_cbo_grupo_generico" style="width:260px" onchange="mostrar_clases_en_Busqueda()">
                <option value="-">:: Seleccione Grupo ::</option>
                <?
                    $Result_Resol = $oSimi_Grupo->Simi_Listar_Grupo();
					if($Result_Resol){
						while (odbc_fetch_row($Result_Resol)){
							
							$COD_GRUPO	 	= odbc_result($Result_Resol,"COD_GRUPO");
							$NRO_GRUPO	 	= odbc_result($Result_Resol,"NRO_GRUPO");
							$DESC_GRUPO	 	= odbc_result($Result_Resol,"DESC_GRUPO");
							
							$DESCRIPCION_GRUPO = $NRO_GRUPO.' '.$DESC_GRUPO;
				?>
                <option value="<?=$COD_GRUPO?>"><?=$DESCRIPCION_GRUPO?></option>
                <?
						}
					}
				?>
                </select>
          </td>
          <td width="85">Clase</td>
          <td><select id="busc_CBO_CLASE" name="busc_CBO_CLASE" class="form-control" style="width:200px" <?= isset($disabled_text) ?$disabled_text : ''?> >
            <option value="0" selected="selected">:: Seleccione Clase ::</option>
          </select></td>
          <td width="136" rowspan="3" valign="bottom"><table width="124" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td width="85" height="40"><input name="button2" type="button" class="btn btn-primary" id="button2" value="Buscar" onclick="Paginar_Bien('1')" /></td>
              <td width="39"><a href="#" onclick="Quitar_Filtro()"><img src="../webimages/iconos/quitar_flitros.png" alt="Quitar Filtro" width="25" height="25" border="0" /></a></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td height="28">Código del bien</td>
          <td><input type="text" name="busc_txt_codigo_bien" id="busc_txt_codigo_bien" style="width:150px" /></td>
          <td>Resolución</td>
          <td width="219"><select id="BUSC_CBO_RESOLUCION_CATALOGO" name="BUSC_CBO_RESOLUCION_CATALOGO" class="form-control" style="width:200px" >
            <option value="-" selected="selected">:: Seleccione Resolución ::</option>
            <?


   		    $Result_RESOL_CATALOGO = $oSimi_Resolucion_Catalogo->Simi_Listar_Resolucion_Catalogo();
			while (odbc_fetch_row($Result_RESOL_CATALOGO)){
							
			$COD_RESOL_CATALOGO		= odbc_result($Result_RESOL_CATALOGO,"COD_RESOL_CATALOGO");
			$NRO_RESOLUCION	 		= utf8_encode(odbc_result($Result_RESOL_CATALOGO,"NRO_RESOLUCION"));
			
			if($COD_RESOL_CATALOGO_E == $COD_RESOL_CATALOGO){
				$select_resoluc = 'selected="selected"';
			}else{
				$select_resoluc = '';
			}
			
	  ?>
            <option value="<?=$COD_RESOL_CATALOGO?>" <?=$select_resoluc?> > <?=$NRO_RESOLUCION?></option>
            <?
			}
			
	  ?>
          </select></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td height="28"> Nombre del Bien </td>
          <td><input type="text" name="busc_txt_denominacion" id="busc_txt_denominacion" style="width:350px" /></td>
          <td>Condición</td>
          <td><select name="busc_cbo_condicion" id="busc_cbo_condicion" style="width:200px">
            <option selected="selected" value="-">:: Seleccione Condición ::</option>
            <option value="A">ACTIVO</option>
            <option value="E">EXCLUIDO</option>
          </select></td>
        </tr>
        <tr>
          <td width="14">&nbsp;</td>
          <td width="115">&nbsp;</td>
          <td colspan="4">&nbsp;</td>
        </tr>
      </table>
      <div id="div_lista_bien">
      <? include_once('simi_catalogo_bien_principal_list.php')?></div></td>
    <td valign="top"><div id="div_formulario" ></div>
    </td>
  </tr>
</table>
