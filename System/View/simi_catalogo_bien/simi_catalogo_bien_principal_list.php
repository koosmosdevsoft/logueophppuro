<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="../utils/css/stylesPaginacion.css"/>

<?
require_once('../../Controller/C_simi_catalogo.php');
require_once('../../../utils/funciones/funciones.php');

$oSimi_Catalogo	=	new Simi_Catalogo;

$opt = isset($_GET['opt']) ?$_GET['opt']  : '';

if($opt == 'buscar_bien'){	

				
	$COD_GRUPO 			= convertir_a_utf8($_REQUEST['busc_cbo_grupo_generico']);
	$COD_CLASE 			= convertir_a_utf8($_REQUEST['busc_CBO_CLASE']);
	
	$NRO_BIEN 			= convertir_a_utf8($_REQUEST['busc_txt_codigo_bien']);
	$DENOM_BIEN 		= convertir_a_utf8($_REQUEST['busc_txt_denominacion']);
	$CONDICION 			= $_REQUEST['busc_cbo_condicion'];
	$COD_RESOL_CATALOGO = $_REQUEST['BUSC_CBO_RESOLUCION_CATALOGO'];
	
}else{
	$COD_GRUPO 			= '-';
	$COD_CLASE 			= '0';
	
	$NRO_BIEN 			= '';
	$DENOM_BIEN 		= '';
	$CONDICION			= '-';
	$COD_RESOL_CATALOGO	= '-';
}

?>


<BR />
<table width="949" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" id="tbl_Resultado_Lista_Familia" >

  <tr>
    <th width="34" height="20" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:center">Item</th>
    <th width="70" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:center">Código</th>
    <th bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:left">Denominación</th>
    <th bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:left">Grupo</th>
    <th width="141" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:left">Clase</th>
    <th width="118" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:left">Resolución A</th>
    <th width="66" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:center">Condición</th>
  </tr>

  <?

	$pageNum = isset($_GET['page']) ? $_GET['page'] : '';
	
	$pageNum = ($pageNum == '') ? 1: $pageNum;
	
	$LIMITE = 30;
	$TOTAL_LINK = 6;
	
	$RS = $oSimi_Catalogo->Total_Catalogo_x_Denominacion_Bien_BUSQUEDA($COD_GRUPO, $COD_CLASE, $NRO_BIEN, $DENOM_BIEN, $CONDICION, $COD_RESOL_CATALOGO);
	$TOTAL_REGISTRO = odbc_result($RS,"TOT_REG");
	
	$TOTAL_PAGINAS = ceil($TOTAL_REGISTRO / $LIMITE);
	
	$INI = ($pageNum == '1')? ($pageNum * $LIMITE - ($LIMITE)) : ($pageNum * $LIMITE - ($LIMITE))+1 ;
	$FIN = ($pageNum == '1')? ($INI + $LIMITE) : ($INI + $LIMITE)-1 ;
	

    $Result_Catalogo = $oSimi_Catalogo->LISTA_Catalogo_x_Denominacion_Bien_BUSQUEDA($INI, $FIN, $COD_GRUPO, $COD_CLASE, $NRO_BIEN, $DENOM_BIEN, $CONDICION, $COD_RESOL_CATALOGO);
					if($Result_Catalogo){
						while (odbc_fetch_row($Result_Catalogo)){
							
							$ROW_NUMBER_ID	 	= odbc_result($Result_Catalogo,"ROW_NUMBER_ID");
							$ID_CATALOGO	 	= odbc_result($Result_Catalogo,"ID_CATALOGO");
							$NRO_BIEN	 		= utf8_encode(odbc_result($Result_Catalogo,"NRO_BIEN"));
							$NRO_GRUPO	 		= utf8_encode(odbc_result($Result_Catalogo,"NRO_GRUPO"));
							$NRO_CLASE	 		= utf8_encode(odbc_result($Result_Catalogo,"NRO_CLASE"));
							$NRO_CORRELATIVO	= utf8_encode(odbc_result($Result_Catalogo,"NRO_CORRELATIVO"));
							$DENOM_BIEN	 		= utf8_encode(odbc_result($Result_Catalogo,"DENOM_BIEN"));
							$DESC_GRUPO	 		= utf8_encode(odbc_result($Result_Catalogo,"DESC_GRUPO"));
							$DESC_CLASE	 		= utf8_encode(odbc_result($Result_Catalogo,"DESC_CLASE"));
							$CONDICION	 		= utf8_encode(odbc_result($Result_Catalogo,"CONDICION"));
							$NRO_RESOLUCION	 	= utf8_encode(odbc_result($Result_Catalogo,"NRO_RESOLUCION"));
							$NOMB_ARCHIVO	 	= utf8_encode(odbc_result($Result_Catalogo,"NOMB_ARCHIVO"));
							
							if($CONDICION == 'A'){								
								$DESC_CONDICON = 'ACTIVO';	
								$CLASE = 'class="texto_arial_azul_n_11"';
								
							}else if($CONDICION == 'E'){								
								$DESC_CONDICON = 'EXCLUIDO';
								$CLASE = 'class="texto_arial_rojo_n_11"';								
							}
							
	?>

  <tr id="TR_<?=$ID_CATALOGO?>"  onmouseover="mOvr2(this)" onmouseout="mOut2(this)" onclick="Mostrar_Formulario('<?=$ID_CATALOGO?>')" >
    <td height="22" class="separador_borde" style="color:rgb(0, 156, 213); text-align:center"><?=$ROW_NUMBER_ID?></td>
    <td height="22" class="separador_borde" style="color:rgb(0, 156, 213); text-align:center"><?=$NRO_BIEN?></td>
    <td width="307" style="color:rgb(0, 156, 213); text-align:left" class="separador_borde" ><?=$DENOM_BIEN?></td>
    <td width="213" style="color:rgb(0, 156, 213); text-align:left" class="separador_borde" ><?=$DESC_GRUPO?></td>
    <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left" ><?=$DESC_CLASE?></td>
    <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left" ><?=$NRO_RESOLUCION?></td>
    <td <?=$CLASE?> style="text-align:center" ><?=$DESC_CONDICON?></td>
  </tr>
  <?
						}
					}
					
	?>
</table>


<table width="949" border="0" cellspacing="1" cellpadding="0" bgcolor="#f4f4f4">
  <tr>
    <td width="816"><div id="div_paginacion" >
          <?	
        if ($TOTAL_PAGINAS > 1) {
            echo '<div class="pagination">';
            echo '<ul>';
                echo '<li><a href = "javascript: Paginar_Bien('.chr(39).'1'.chr(39).')" class="paginate" >Inicial</a></li>';
                if ($pageNum != 1)
                    echo '<li><a href = "javascript: Paginar_Bien('.chr(39).($pageNum-1).chr(39).')" class="paginate" ><<</a></li>';
                    
                    $page_actual_ini = (($pageNum - $TOTAL_LINK) <= 0) ?  1 : ($pageNum - $TOTAL_LINK);
                    $page_actual_fin = (($pageNum + $TOTAL_LINK) > $TOTAL_PAGINAS) ? $TOTAL_PAGINAS : ($pageNum + $TOTAL_LINK);
                    
                        for ($i=$page_actual_ini;$i<=$page_actual_fin;$i++) {
                            if ($pageNum == $i)
                                //si muestro el índice de la página actual, no coloco enlace
                                echo '<li class="active"><a>'.$i.'</a></li>';
                            else
                                //si el índice no corresponde con la página mostrada actualmente,
                                //coloco el enlace para ir a esa página
                                echo '<li><a href = "javascript: Paginar_Bien('.chr(39).$i.chr(39).')" class="paginate" ">'.$i.'</a></li>';
                        }
                if ($pageNum != $TOTAL_PAGINAS)
                     echo '<li><a href = "javascript: Paginar_Bien('.chr(39).($pageNum+1).chr(39).')" class="paginate" >> ></a></li>';
                echo '<li><a href = "javascript: Paginar_Bien('.chr(39).$TOTAL_PAGINAS.chr(39).')" class="paginate" >Final</a></li>';
           echo '</ul>';
           echo '</div>';
        }
    
    
    ?>
    </div></td>
    <td width="130" class="texto_arial_azul_n_10">Total : 
    <?=$TOTAL_REGISTRO?></td>
  </tr>
</table>