<? session_start();
if($_SESSION['th_SIMI_COD_ENTIDAD']=='') echo '<script language="javascript">parent.location.href="../muebles.php";</script>';
$COD_ENTIDAD_X1			= $_SESSION['th_SIMI_COD_ENTIDAD'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<? 
require_once('../../Controller/C_simi_forma_adquisicion.php');
require_once('../../Controller/C_simi_m_ue_responsable_patrimonial.php');

$oSimi_Forma_Adquisicion			=	new Simi_Forma_Adquisicion;
$oSimi_UE_Responsable_Patrimonial	=	new Simi_UE_Responsable_Patrimonial;

$RS_CTRLPATRIM 		= $oSimi_UE_Responsable_Patrimonial->Verifica_Existe_Registra_Responsable_Control_Patrimonial($COD_ENTIDAD_X1);
$TOTAL_RESP_CTRL	= utf8_encode(odbc_result($RS_CTRLPATRIM,"TOTAL"));
$TOT_MIGRO_INV		= utf8_encode(odbc_result($RS_CTRLPATRIM,"TOT_MIGRO_INV"));


if($TOTAL_RESP_CTRL == '' || $TOTAL_RESP_CTRL == '0' || $TOT_MIGRO_INV == '0'){
?>
	<script> Actualiza_Menu_Direccionar_URL("View/simi_web_importar_add_resp/simi_web_importar_add_resp_principal.php","Cuerpo", "1");</script>
<?
}
?>

<script>


function Mostrar_Formulario(X_COD_UE_RESP_PATRI){
	
	var TXH_SIMI_COD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();
	var txh_tot_registro 		= $("#txh_tot_registro").val();
	
	
	ajaxpage("View/simi_m_responsable_patrimonial/simi_m_responsable_patrimonial_principal_form.php?X_COD_UE_RESP_PATRI="+X_COD_UE_RESP_PATRI+"&TXH_SIMI_COD_ENTIDAD="+TXH_SIMI_COD_ENTIDAD,"div_formulario") ;
	//$('html, body').animate({scrollTop:$('#div_List_Oficinas').position().top}, 'slow');

}

function Mostrar_Area(){
	
	var ID_PREDIO_SBN 		= $("#cbo_Local_Predio").val();
	
	$.post("View/simi_m_areas/listbox_area.php",{ 
		ID_PREDIO_SBN:ID_PREDIO_SBN
	},function(data){
		$("#cbo_Area").html(data);
	})
}


function Mostrar_Oficina(){
	
	var COD_UE_AREA 		= $("#cbo_Area").val();
	
	$.post("View/simi_m_oficinas/listbox_oficinas.php",{ 
		COD_UE_AREA:COD_UE_AREA
	},function(data){
		$("#cbo_Oficina").html(data);
	})
}

function Mostrar_Personal(){
	
	var COD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();
	var cbo_Local_Predio 	= $("#cbo_Local_Predio").val();
	var cbo_Area 			= $("#cbo_Area").val();
	var cbo_Oficina 		= $("#cbo_Oficina").val();
	
	$.post("View/simi_m_personal/listbox_m_personal.php",{ 
		COD_ENTIDAD:COD_ENTIDAD,
		cbo_Local_Predio:cbo_Local_Predio,
		cbo_Area:cbo_Area,
		cbo_Oficina:cbo_Oficina
	},function(data){
		$("#cbo_Personal").html(data);
	})
}

/* modificado por: 		Williams Arenas Saldaña */
/* fecha modificacion: 	08/03/2019 */
function Mostrar_Personal_Actualizado(){
	
	var COD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val(); 
	var cbo_Local_Predio 	= $("#cbo_Local_Predio").val();
	var cbo_Area 			= $("#cbo_Area").val();
	//var cbo_Oficina 		= $("#cbo_Oficina").val();
	
	$.post("View/simi_m_personal/listbox_m_personal_Actualizado.php",{ 
		COD_ENTIDAD:COD_ENTIDAD,
		cbo_Local_Predio:cbo_Local_Predio,
		cbo_Area:cbo_Area
		//cbo_Oficina:cbo_Oficina
	},function(data){
		$("#cbo_Personal").html(data);
	})
}

/*************************************************************/


function Mostrar_Area_Almacen(){
	
	var cbo_Local_Predio_Almacen 		= $("#cbo_Local_Predio_Almacen").val();
	
	$.post("View/simi_m_areas/listbox_area.php",{ 
		ID_PREDIO_SBN:cbo_Local_Predio_Almacen
	},function(data){
		$("#cbo_Area_Almacen").html(data);
	})
}


function Mostrar_Oficina_Almacen(){
	
	var cbo_Area_Almacen = $("#cbo_Area_Almacen").val();
	
	$.post("View/simi_m_oficinas/listbox_oficinas.php",{ 
		COD_UE_AREA:cbo_Area_Almacen
	},function(data){
		$("#cbo_Oficina_Almacen").html(data);
	})
}


/*************************************************************/

function Guardar_Responsable_Bien_patrimonial(){
	
		
	var TXH_SIMI_COD_USUARIO 		= $("#TXH_SIMI_COD_USUARIO").val();
	var TXH_SIMI_COD_ENTIDAD 		= $("#TXH_SIMI_COD_ENTIDAD").val();
	var txt_COD_UE_RESP_PATRI 		= $("#txt_COD_UE_RESP_PATRI").val();
	
	var cbo_Local_Predio 	= $("#cbo_Local_Predio").val();
	var cbo_Area 			= $("#cbo_Area").val();
	var cbo_Oficina 		= $("#cbo_Oficina").val();
	var cbo_Personal 		= $("#cbo_Personal").val();
	
	if(cbo_Local_Predio == '-'){
		alert('Seleccione Local');
		 $("#cbo_Local_Predio").focus();
		 
	}else if(cbo_Area == '-'){
		alert('Seleccione Area');
		 $("#cbo_Area").focus();
		 
	}else if(cbo_Oficina == '-'){
		alert('Seleccione Oficina');
		 $("#cbo_Oficina").focus();
		 
	}else if(cbo_Personal == '-'){
		alert('Seleccione Personal');
		 $("#cbo_Personal").focus();
		 
	}else{
		
		carga_loading('div_lista');
		
		$.post("Model/M_simi_m_ue_responsable_patrimonial.php?operacion=Guardar_UE_Responsable_Patrimonial",{ 
			TXH_SIMI_COD_USUARIO:TXH_SIMI_COD_USUARIO,
			TXH_SIMI_COD_ENTIDAD:TXH_SIMI_COD_ENTIDAD,
			txt_COD_UE_RESP_PATRI:txt_COD_UE_RESP_PATRI,
			cbo_Local_Predio:cbo_Local_Predio,
			cbo_Area:cbo_Area,
			cbo_Oficina:cbo_Oficina,
			cbo_Personal:cbo_Personal
		},function(data){
			
			if(data==1){
				Mostrar_Lista_Responsable_Patrimonial(TXH_SIMI_COD_ENTIDAD);				
				$("#div_formulario").html('');
			}else{
				alert(data);
			}
			
		})
		
		
	}
	
}


function Mostrar_Lista_Responsable_Patrimonial(Z_COD_ENTIDAD){
	$.post("View/simi_m_responsable_patrimonial/simi_m_responsable_patrimonial_principal_list.php?OPT=List",{
		Z_COD_ENTIDAD:Z_COD_ENTIDAD
	},function(data){
		$("#div_lista").html(data);
	})
}


/*

function Eliminar_Motivo_Traslado(COD_MOT_TRAS_X){
	
	var TXH_ID_USUARIO 		= $("#TXH_ID_USUARIO").val();
	
	if(confirm('¿Confirma eliminar este motivo de traslado?')){
		
		carga_loading('div_lista');
		
		$.post("Model/M_simi_mot_traslado.php?operacion=Eliminar_Motivo_Traslado",{ 
			COD_MOT_TRAS_X:COD_MOT_TRAS_X,
			TXH_ID_USUARIO:TXH_ID_USUARIO
		},function(data){
			
			if(data==1){
				Mostrar_Lista_Motivo_Traslado();				
				$("#div_formulario").html('');
			}else{
				alert(data);
			}
			
		})
	}
	
		
		
}*/
</script>

<table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td style="width:850px" valign="top"><div id="div_lista"><? include_once('simi_m_responsable_patrimonial_principal_list.php')?></div></td>
    <td  valign="top"><div id="div_formulario" >&nbsp;</div>
    </td>
  </tr>
</table>
