<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 

<?
require_once('../../Controller/C_simi_m_ue_responsable_patrimonial.php');
require_once('../../Controller/C_padron_predios.php');
require_once('../../Controller/C_simi_m_area.php');
require_once('../../Controller/C_simi_m_oficina.php');
require_once('../../Controller/C_simi_m_personal.php');

$oSimi_UE_Responsable_Patrimonial	=	new Simi_UE_Responsable_Patrimonial;
$oPadron_Predios					=	new Padron_Predios;
$oSimi_UE_Area						=	new Simi_UE_Area;
$oSimi_UE_Oficina					=	new Simi_UE_Oficina;
$oSimi_UE_Personal					=	new Simi_UE_Personal;


$TXH_SIMI_COD_ENTIDAD 	= $_GET['TXH_SIMI_COD_ENTIDAD'];
$X_COD_UE_RESP_PATRI 	= $_REQUEST['X_COD_UE_RESP_PATRI'];

if($X_COD_UE_RESP_PATRI !=''){
	
	$Result_Resp_Patri_E	= $oSimi_UE_Responsable_Patrimonial->Ver_Datos_UE_Responsable_Patrimonial_x_CODIGO($X_COD_UE_RESP_PATRI);
	
	$ID_PREDIO_SBN_RESP_E	= odbc_result($Result_Resp_Patri_E,"ID_PREDIO_SBN_RESP");
	$COD_UE_AREA_RESP_E		= odbc_result($Result_Resp_Patri_E,"COD_UE_AREA_RESP");
	$COD_UE_OFIC_RESP_E		= odbc_result($Result_Resp_Patri_E,"COD_UE_OFIC_RESP");
	$COD_UE_PERS_RESP_E		= odbc_result($Result_Resp_Patri_E,"COD_UE_PERS_RESP");
	
}

?>


<table width="824" border="0" cellpadding="0" cellspacing="3">
  <tr>
    <td height="20" ><a style="float:right" href="#" onclick="$('#div_formulario').html('')" ><img src="../webimages/iconos/cerrar_2.png" width="20" height="20" border="0" /></a></td>
  </tr>
  <tr>
    <td height="20" class="Titulo_plomo_25px">Formulario responsable de bienes patrimoniales</td>
  </tr>
  <tr>
    <td width="818" height="20">&nbsp;</td>
  </tr>
  <tr>
    <td height="20"><table width="812" border="0" cellpadding="0" cellspacing="2" class="FrmBuscadorFlotante TABLE_border4" bgcolor="#f4f4f4">
      <tr>
        <td width="19">&nbsp;</td>
        <td colspan="2" align="center">&nbsp;</td>
        <td width="18" align="center">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2"><fieldset class="TABLE_border4">
          <legend class="texto_arial_plomo_n_12">Responsable de Control Patrimonial</legend>
          <table width="750" border="0" cellspacing="2" cellpadding="0">
            <tr>
              <td>&nbsp;</td>
              <td colspan="2"><input type="hidden" name="txt_COD_UE_RESP_PATRI" id="txt_COD_UE_RESP_PATRI" value="<?=$X_COD_UE_RESP_PATRI?>" /></td>
            </tr>

            <tr>
              <td>&nbsp;</td>
              <td>Personal Resp. Ctrl. Patrim.</td>
              <td><select name="cbo_Personal" id="cbo_Personal" style="width:500px" >
                <option value="-" > :: Seleccione Personal :: </option>
                
                <?
        if($X_COD_UE_RESP_PATRI !='')
        {

          $RESULT_Personal_R  = $oSimi_UE_Personal->Listar_Simi_UE_Personal_x_Entidad_Local_area_oficina_NEWSINABIP($TXH_SIMI_COD_ENTIDAD);
          while (odbc_fetch_row($RESULT_Personal_R))
          {
            $COD_UE_PERS_R    = odbc_result($RESULT_Personal_R,"COD_UE_PERS");
            $UE_APEPAT_PERS_R = utf8_encode(odbc_result($RESULT_Personal_R,"UE_APEPAT_PERS"));
            $UE_APEMAT_PERS_R = utf8_encode(odbc_result($RESULT_Personal_R,"UE_APEMAT_PERS"));
            $UE_NOMBRES_PERS_R  = utf8_encode(odbc_result($RESULT_Personal_R,"UE_NOMBRES_PERS"));
            
            $UE_NOM_COMPLETO_PERS = $UE_NOMBRES_PERS_R.','.$UE_APEPAT_PERS_R.' '.$UE_APEMAT_PERS_R;
            
            if($COD_UE_PERS_RESP_E == $COD_UE_PERS_R){
              $selected_personal_R = 'selected="selected"';
            }else{
              $selected_personal_R = '';
            }
        ?>
          <option value="<?=$COD_UE_PERS_R?>" <?=$selected_personal_R?> ><?=$UE_NOM_COMPLETO_PERS?></option>
        <? 
          } 

        }else{


          $RESULT_Personal_R  = $oSimi_UE_Personal->Listar_Simi_UE_Personal_x_Entidad_Local_area_oficina_NEWSINABIP($TXH_SIMI_COD_ENTIDAD);
          while (odbc_fetch_row($RESULT_Personal_R))
          {
            $COD_UE_PERS_R    = odbc_result($RESULT_Personal_R,"COD_UE_PERS");
            $UE_APEPAT_PERS_R = utf8_encode(odbc_result($RESULT_Personal_R,"UE_APEPAT_PERS"));
            $UE_APEMAT_PERS_R = utf8_encode(odbc_result($RESULT_Personal_R,"UE_APEMAT_PERS"));
            $UE_NOMBRES_PERS_R  = utf8_encode(odbc_result($RESULT_Personal_R,"UE_NOMBRES_PERS"));
            
            $UE_NOM_COMPLETO_PERS = $UE_NOMBRES_PERS_R.','.$UE_APEPAT_PERS_R.' '.$UE_APEMAT_PERS_R;
            
            
        ?>
          <option value="<?=$COD_UE_PERS_R?>" ><?=$UE_NOM_COMPLETO_PERS?></option>

        <? 

          }
        }
        ?>                
                </select></td>
              </tr>

            <tr>
              <td>&nbsp;</td>
              <td>Local Resp. Ctrl. Patrim.</td>
              <td><select name="cbo_Local_Predio" id="cbo_Local_Predio" style="width:500px" onchange="Mostrar_Area()" >
                <option value="-" > :: Seleccione Local :: </option>
                <?
  			$Result_Local = $oPadron_Predios->Listar_Local_o_Predios_x_COD_ENTIDAD($TXH_SIMI_COD_ENTIDAD);
			while (odbc_fetch_row($Result_Local)){
				$ID_PREDIO_SBN_RESP_R	= utf8_encode(odbc_result($Result_Local,"ID_PREDIO_SBN"));
				$DENOMINACION_PREDIO_RESP	= utf8_encode(odbc_result($Result_Local,"DENOMINACION_PREDIO"));
				
					if($ID_PREDIO_SBN_RESP_E == $ID_PREDIO_SBN_RESP_R){
						$selected_Local_R = 'selected="selected"';
					}else{
						$selected_Local_R = '';
					}
		?>
                <option value="<?=$ID_PREDIO_SBN_RESP_R?>" <?=$selected_Local_R?> > <?=$DENOMINACION_PREDIO_RESP?> </option>
                <? } ?>
                </select></td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td width="178">Área Resp. Ctrl. Patrim.</td>
              <td width="545"><select name="cbo_Area" id="cbo_Area" style="width:500px" >
                <option value="-" > :: Seleccione Área :: </option>
                
                <?
				if($X_COD_UE_RESP_PATRI !=''){
					
					$RESULT_AREAS_REP= $oSimi_UE_Area->Listar_Simi_UE_Area($ID_PREDIO_SBN_RESP_E);
					while (odbc_fetch_row($RESULT_AREAS_REP)){
						$COD_UE_AREA_R	= odbc_result($RESULT_AREAS_REP,"COD_UE_AREA");
						$DESC_AREA_R	 	= utf8_encode(odbc_result($RESULT_AREAS_REP,"DESC_AREA"));
						
						if($COD_UE_AREA_RESP_E == $COD_UE_AREA_R){
							$selected_Area_R = 'selected="selected"';
						}else{
							$selected_Area_R = '';
						}
				?>
                <option value="<?=$COD_UE_AREA_R?>" <?=$selected_Area_R?> ><?=$DESC_AREA_R?></option>
                <? } 
				}
				?>
                </select></td>
              </tr>
            <!-- <tr>
              <td>&nbsp;</td>
              <td>Oficina Resp. Ctrl. Patrim.</td>
              <td><select name="cbo_Oficina" id="cbo_Oficina" style="width:500px" onchange="Mostrar_Personal()" >
                <option value="-" > :: Seleccione Oficina :: </option>
                
                <?
				if($X_COD_UE_RESP_PATRI !=''){
					
						$RESULT_Oficinas_R	= $oSimi_UE_Oficina->Listar_Simi_UE_Oficina_x_area($COD_UE_AREA_RESP_E);
						while (odbc_fetch_row($RESULT_Oficinas_R)){
							$COD_UE_OFIC_R	= odbc_result($RESULT_Oficinas_R,"COD_UE_OFIC");
							$DESC_OFIC_R	 	= utf8_encode(odbc_result($RESULT_Oficinas_R,"DESC_OFIC"));
						
						if($COD_UE_OFIC_RESP_E == $COD_UE_OFIC_R){
							$selected_Oficina_R = 'selected="selected"';
						}else{
							$selected_Oficina_R = '';
						}
				?>
                <option value="<?=$COD_UE_OFIC_R?>" <?=$selected_Oficina_R?> ><?=$DESC_OFIC_R?></option>
                <? } 
				}
				?>
                
                </select></td>
              </tr> -->
            
            <tr>
              <td width="19">&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              </tr>
            </table>
          </fieldset></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td width="586">&nbsp;</td>
        <td width="177"><input name="button2" type="button" class="btn btn-primary" id="button2" value="Guardar Registro" onclick="Guardar_Responsable_Bien_patrimonial()" /></td>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="30"><hr class="linea_separador_01" /></td>
  </tr>
</table>
