<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="../utils/css/stylesPaginacion.css"/>
<?

require_once("../../Controller/BaseBD_PDO.php");
require_once('../../Model/BaseBD_PDOModel.php');

require_once('../../Controller/BienPatrimonialController.php');
require_once('../../Model/BienPatrimonialModel.php');

require_once('../../Controller/C_padron_predios.php');
require_once('../../../utils/funciones/funciones.php');

error_reporting(0);
$oPadron_Predios	=	new Padron_Predios;
$objModel = new BienPatrimonialModel();

$opt = $_GET['opt'];

if($opt == 'busqueda'){
			
	$COD_ENTIDAD 			= convertir_a_utf8($_REQUEST['TXH_SIMI_COD_ENTIDAD']);
	$DENOMINACION_PREDIO 	= convertir_a_utf8($_REQUEST['txtbusc_nom_local']);
	
}else{
	$COD_ENTIDAD			= $_SESSION['th_SIMI_COD_ENTIDAD'];
	$DENOMINACION_PREDIO 	= '';
}

?>



<BR />
<table width="845" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" id="tbl_Resultado_Lista_Locales" class="ReportA2C">

  <tr>
    <th width="48" height="20" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:center">Item</th>
    <th bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:left">Nombre del predio o local</th>
  </tr>

<?php

$pageNum = $_GET['page'];

$pageNum = ($pageNum == '') ? 1: $pageNum;

$LIMITE = 20;
$TOTAL_LINK = 8;

/*
$RS = $oPadron_Predios->TOTAL_PADRON_PREDIOS_X_PARAMETROS($COD_ENTIDAD, $DENOMINACION_PREDIO);
$TOTAL_REGISTRO = odbc_result($RS,"TOT_REG");
*/

/* CREACION:        WILLIAMS ARENAS */
/* FECHA CREACION:  08-06-2020 */
/* DESCRIPCION:     OBTENER TOTAL DE REGISTROS DE PREDIOS */ 
$dataModel_01 = [
  'COD_ENTIDAD'=>$COD_ENTIDAD,
  'DENOMINACION_PREDIO'=>$DENOMINACION_PREDIO,
];

$data['DataTotalPredios'] = $objModel->TOTAL_PADRON_PREDIOS_X_PARAMETROS_PDO($dataModel_01);
$TOTAL_REGISTRO = $data['DataTotalPredios'][0]['TOT_REG'];
/* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */


$TOTAL_PAGINAS = ceil($TOTAL_REGISTRO / $LIMITE);

$INI = ($pageNum == '1')? ($pageNum * $LIMITE - ($LIMITE)) : ($pageNum * $LIMITE - ($LIMITE))+1 ;
$FIN = ($pageNum == '1')? ($INI + $LIMITE) : ($INI + $LIMITE)-1 ;

/*
$RSTDatosPredio_L = $oPadron_Predios->LISTA_PADRON_PREDIOS_X_PARAMETROS($INI,$FIN, $COD_ENTIDAD, $DENOMINACION_PREDIO);
*/

/* CREACION:        WILLIAMS ARENAS */
/* FECHA CREACION:  08-06-2020 */
/* DESCRIPCION:     OBTENER LISTADO DE PREDIOS */
$data = array();   

$dataModel_01 = [
    'COD_ENTIDAD'         =>$COD_ENTIDAD,
    'DENOMINACION_PREDIO' =>$DENOMINACION_PREDIO,
    'INI'                 =>$INI,
    'FIN'                 =>$FIN
];

$data['DataListadoPredios'] = $objModel->LISTA_PADRON_PREDIOS_X_PARAMETROS_PDO($dataModel_01);
$Predios = $data['DataListadoPredios'];
/**** FIN DE OBTENER LISTADO DE PREDIOS ****/

if($Predios) foreach ($Predios as $Predio):
  $ROW_NUMBER_ID_L		  = utf8_encode($Predio['ROW_NUMBER_ID']);
  $ID_PREDIO_SBN			  = utf8_encode($Predio['ID_PREDIO_SBN']); 
  $DENOMINACION_PREDIO	= utf8_encode($Predio['DENOMINACION_PREDIO']); 
?>

  <tr id="<?=$ID_PREDIO_SBN?>"  onmouseover="mOvr2(this)" onmouseout="mOut2(this)">
    <td height="22" style="color:rgb(0, 156, 213); text-align:center" class="separador_borde"><?=$ROW_NUMBER_ID_L?></td>
    <td width="797" style="color:rgb(0, 156, 213); text-align:left" class="separador_borde" onclick="fShowFichaPredio('<?=$ID_PREDIO_SBN?>')" ><?=$DENOMINACION_PREDIO?></td>
  </tr>
  

<?php endforeach; ?>


</table>
<table width="845" border="0" cellspacing="1" cellpadding="0" bgcolor="#f4f4f4">
  <tr>
    <td width="709"><div id="div_paginacion" >
          <?	
        if ($TOTAL_PAGINAS > 1) {
            echo '<div class="pagination">';
            echo '<ul>';
                echo '<li><a href = "javascript: paginar_Lista_Gral('.chr(39).'1'.chr(39).')" class="paginate" >Inicial</a></li>';
                if ($pageNum != 1)
                    echo '<li><a href = "javascript: paginar_Lista_Gral('.chr(39).($pageNum-1).chr(39).')" class="paginate" ><<</a></li>';
                    
                    $page_actual_ini = (($pageNum - $TOTAL_LINK) <= 0) ?  1 : ($pageNum - $TOTAL_LINK);
                    $page_actual_fin = (($pageNum + $TOTAL_LINK) > $TOTAL_PAGINAS) ? $TOTAL_PAGINAS : ($pageNum + $TOTAL_LINK);
                    
                        for ($i=$page_actual_ini;$i<=$page_actual_fin;$i++) {
                            if ($pageNum == $i)
                                //si muestro el índice de la página actual, no coloco enlace
                                echo '<li class="active"><a>'.$i.'</a></li>';
                            else
                                //si el índice no corresponde con la página mostrada actualmente,
                                //coloco el enlace para ir a esa página
                                echo '<li><a href = "javascript: paginar_Lista_Gral('.chr(39).$i.chr(39).')" class="paginate" ">'.$i.'</a></li>';
                        }
                if ($pageNum != $TOTAL_PAGINAS)
                     echo '<li><a href = "javascript: paginar_Lista_Gral('.chr(39).($pageNum+1).chr(39).')" class="paginate" >>></a></li>';
                echo '<li><a href = "javascript: paginar_Lista_Gral('.chr(39).$TOTAL_PAGINAS.chr(39).')" class="paginate" >Final</a></li>';
           echo '</ul>';
           echo '</div>';
        }
    
    
    ?>
    </div></td>
    <td width="133" class="texto_arial_azul_n_10">Total : 
    <?=$TOTAL_REGISTRO?></td>
  </tr>
</table>
