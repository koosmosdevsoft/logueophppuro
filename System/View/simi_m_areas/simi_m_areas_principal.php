<? session_start();
if($_SESSION['th_SIMI_COD_ENTIDAD']=='') echo '<script language="javascript">parent.location.href="../muebles.php";</script>';
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 

<script>

function paginar_Lista_Gral(numpage){
	
	var txtbusc_nom_local 		= $("#txtbusc_nom_local").val();
	var TXH_SIMI_COD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();

	carga_loading('div_lista_Locales');
		
	$.post("View/simi_m_areas/simi_m_areas_principal_locales.php?opt=busqueda&page="+numpage,{
		TXH_SIMI_COD_ENTIDAD:TXH_SIMI_COD_ENTIDAD,
		txtbusc_nom_local:txtbusc_nom_local
	},function(data){
		$("#div_lista_Locales").html(data);
	})	
}



function fShowFichaPredio(sId){
	
	ajaxpage("View/simi_m_areas/simi_m_areas_principal_form.php?F_COD_UE_LOCAL="+sId,"div_formulario") ;
	
	var ulA = document.getElementById("tbl_Resultado_Lista_Locales"); 
	var liNodesA = ulA.getElementsByTagName("TR"); 
	var sTot =  liNodesA.length;
	
	var color = "";
	var obj = "";
	
	for( var s = 1; s <= sTot; s++ ){ 
		obj = liNodesA[s].id;
		
		if(sId == obj){
			color = "#e2f2ff";
		}else{
			color = "";
		}
				
		$("#"+obj+"").css("background-color",color);
		
	}
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



function paginar_areas(numpage){
	
	var TXH_ID_PREDIO_SBN		= $("#TXH_ID_PREDIO_SBN").val();
	var txt_busc_nom_organizacion	= $("#txt_busc_nom_organizacion").val();

	carga_loading('div_UE_areas');
		
	$.post("View/simi_m_areas/simi_m_areas_principal_area_list.php?opt=busqueda_organizacion&page="+numpage,{
		TXH_ID_PREDIO_SBN:TXH_ID_PREDIO_SBN,
		txt_busc_nom_organizacion:txt_busc_nom_organizacion
	},function(data){
		$("#div_UE_areas").html(data);
	})	
}

/*

function Mostrar_Formulario_Area(COD_UE_LOCAL_L1){
	
	ajaxpage("View/simi_m_areas/simi_m_areas_principal_form.php?x_COD_UE_LOCAL="+COD_UE_LOCAL_L1,"div_formulario") ;
	
	//$("html, body").animate({ scrollTop: 0 }, 600);
	$('html, body').animate({scrollTop:$('#div_formulario').position().top}, 'slow');
	
	var ulA      = document.getElementById("tbl_Resultado_Lista_Gral"); 
	var liNodesA = ulA.getElementsByTagName("TR"); 
	var sTot     = liNodesA.length;
	
	var color  = "";
	var obj    = "";
	
	for( var s = 1; s <= sTot; s++ ){ 
		obj = liNodesA[s].id;
		
		if(COD_UE_LOCAL == obj){
			color = "#e2f2ff";
		}else{
			color = "";
		}
				
		$("#"+obj+"").css("background-color",color);
		
	}
	
}
*/

function Mostrar_Ventana_Reg_Area(x_COD_UE_AREA_A01){
	
	$("#div_formulario_Reg_Area").html('');	
	
	$.post("View/simi_m_areas/simi_m_areas_principal_area_form.php",{ 
		x_COD_UE_AREA_A01:x_COD_UE_AREA_A01
	},function(data){
		
		$("#div_formulario_Reg_Area").dialog({ 
				title: 'Registro de organización',
				width: 590,
				height: 190,
				resizable: "false",
				modal: true
			}).html(data);
		})
}



function Guardar_Area(){
	
	var TXH_SIMI_COD_USUARIO 	= $("#TXH_SIMI_COD_USUARIO").val();
	var TXH_ID_PREDIO_SBN 		= $("#TXH_ID_PREDIO_SBN").val();
	var txt_cod_area_x1 		= $("#txt_cod_area_x1").val();
	var txt_nom_area_x1 		= $("#txt_nom_area_x1").val();
	var txt_siglas_x1 			= $("#txt_siglas_x1").val();
	
	if(txt_nom_area_x1 == ''){
		alert('Ingrese nombre del área');
		
	}else{
		var url = "Model/M_simi_m_area.php?operacion=Guardar_Area";
			url += (TXH_SIMI_COD_USUARIO == "") ? "" : "&TXH_SIMI_COD_USUARIO=" + TXH_SIMI_COD_USUARIO;
			url += (TXH_ID_PREDIO_SBN == "") ? "" : "&TXH_ID_PREDIO_SBN=" + TXH_ID_PREDIO_SBN;
			url += (txt_cod_area_x1 == "") ? "" : "&txt_cod_area_x1=" + txt_cod_area_x1;
			url += (txt_nom_area_x1 == "") ? "" : "&txt_nom_area_x1=" + txt_nom_area_x1;
			url += (txt_siglas_x1 == "") ? "" : "&txt_siglas_x1=" + txt_siglas_x1;

		$.ajax({ 
				type: 'GET', 
				url: url, 
				dataType: 'json',
				success: function (response) { 
					if(response.valor == '1'){
						$("#div_formulario_Reg_Area").dialog("close").html('');				
						paginar_areas('1');
						$('#div_errores').hide();
					}else if(response.valor == '0'){
						$('#div_errores').hide();
						alert(response);
					}else {
						let vhtml ='';
						vhtml += '<ul>';
						$.each(response.errores, function(index, element) {
							//console.log(element);
							if(element !== undefined){
								vhtml += '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> <span class="sr-only">Error:</span>';
								vhtml += element + '<br>';
								
							}

						});	
						
						vhtml += '</ul>';
						$('#div_errores').html(vhtml);
						$('#div_errores').show();
					}
				}
			});
	}
}


function Eliminar_Area(x_COD_AREA_A01){
	
	var TXH_ID_PREDIO_SBN 	= $("#TXH_ID_PREDIO_SBN").val();
	var TXH_SIMI_COD_USUARIO 	= $("#TXH_SIMI_COD_USUARIO").val();
	var TXH_SIMI_COD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();
	
	if(confirm('¿Está Ud. seguro(a) de eliminar el área?')){

		$.post("Model/M_simi_m_area.php?operacion=Eliminar_Area",{
			x_COD_AREA_A01:x_COD_AREA_A01,
			TXH_SIMI_COD_USUARIO:TXH_SIMI_COD_USUARIO,
			TXH_SIMI_COD_ENTIDAD:TXH_SIMI_COD_ENTIDAD
		},function(data){
			if(data == 1){
				//$("#div_formulario_Reg_Area").dialog("close").html('');				
				paginar_areas('1');				
			}else if(data == 2){
				alert('No es posible eliminar el Área, tiene asignado Bienes');
			}else{
				alert(data);
			}
			
			
		})
	}
}





	
</script>



<table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td style="width:20px" valign="top">&nbsp;</td>
    <td style="width:870px" valign="top">
    
    <table width="844" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td  class="Titulo_plomo_25px">Registro de áreas</td>
      </tr>
      <tr>
        <td  class="Titulo_02_17px"><hr /></td>
      </tr>
      <tr>
        <td  class="Titulo_02_17px">&nbsp;</td>
      </tr>
      <tr>
        <td width="838"  class="Titulo_02_17px">Seleccione Local</td>
      </tr>
    </table>
    <table width="844" border="0" cellpadding="0" cellspacing="2" class="FrmBuscadorFlotante TABLE_border4" bgcolor="#f4f4f4">
      <tr>
        <td width="35" height="40">&nbsp;</td>
        <td width="133">Nombre del Local </td>
        <td width="527"><input name="txtbusc_nom_local" type="text" id="txtbusc_nom_local" style="width:500px" /></td>
        <td width="137"><a href="#" onclick="paginar_Lista_Gral('1')"><img src="../webimages/iconos/ver_icono_b.png" width="20" height="22" border="0" /></a>&nbsp;&nbsp; <a href="#" onclick="$('#txtbusc_nom_local').val(''); paginar_Lista_Gral('1');"><img src="../webimages/iconos/quitar_flitros.png" width="22" height="22" border="0" /></a></td>
      </tr>
    </table>
    <div id="div_lista_Locales"><? include_once('simi_m_areas_principal_locales.php')?></div>
    
    </td>
    <td valign="top"><div id="div_formulario" >&nbsp;</div>    </td>
  </tr>
</table>

<div id="div_formulario_Reg_Area"></div>