<?
require_once('../../Controller/C_padron_entidad.php');
require_once('../../Controller/C_simi_m_ue_inventario.php');
require_once('../../Controller/C_simi_m_personal.php');
require_once('../../../utils/funciones/funciones.php');
require_once('../../../utils/fpdf181/fpdf.php');

error_reporting(0);
$oSimi_UE_Bien_Inventario	=	new Simi_UE_Bien_Inventario;
$oPadron_Entidad			=	new Padron_Entidad;
$oSimi_UE_Personal			=	new Simi_UE_Personal;

$OPT = $_GET['OPT'];

if($OPT == 'RptInvPDF'){
	
	/*****************************************************************************************************************/
	/*****************************************************************************************************************/
	/*****************************************************************************************************************/
	
	$COD_ENTIDAD = $_GET['sEN'];
	$Xanio 	 	 = $_GET['sAnio'];
	
	$RS_Ent = $oPadron_Entidad->Ver_Datos_Padron_Entidad_x_COD_ENTIDAD($COD_ENTIDAD);
	$NOM_ENTIDAD			= utf8_encode(odbc_result($RS_Ent,"NOM_ENTIDAD"));
	
	
	
	$RS_Inv = $oSimi_UE_Bien_Inventario->UE_Listar_Inventario_Habilitado_x_Entidad_X_ANIO($COD_ENTIDAD, $Xanio);

	$COD_INVENTARIO			= utf8_encode(odbc_result($RS_Inv,"COD_INVENTARIO"));
	$NOM_INVENTARIO			= utf8_encode(odbc_result($RS_Inv,"NOM_INVENTARIO"));
	$PERIODO				= odbc_result($RS_Inv,"PERIODO");
	$FECHA_INICIO			= cambiaf_a_normal(odbc_result($RS_Inv,"FECHA_INICIO"));
	$FECHA_TERMINO			= cambiaf_a_normal(odbc_result($RS_Inv,"FECHA_TERMINO"));
	$HABILITADO				= utf8_encode(odbc_result($RS_Inv,"HABILITADO"));
	$COD_INVENTARIO_ENT		= utf8_encode(odbc_result($RS_Inv,"COD_INVENTARIO_ENT"));
	$TOTAL_LOCAL_GRAL		= number_format((odbc_result($RS_Inv,"TOTAL_LOCAL_GRAL")),0);
	$TOTAL_LOCAL_SI_INV		= number_format(utf8_encode(odbc_result($RS_Inv,"TOTAL_LOCAL_SI_INV")),0);
	$TOTAL_LOCAL_NO_INV		= number_format(utf8_encode(odbc_result($RS_Inv,"TOTAL_LOCAL_NO_INV")),0);
	$TOTAL_BIENES_GRAL		= number_format(utf8_encode(odbc_result($RS_Inv,"TOTAL_BIENES_GRAL")),0);
	$TOTAL_BIENES_ACTIVOS	= number_format(utf8_encode(odbc_result($RS_Inv,"TOTAL_BIENES_ACTIVOS")),0);
	$TOTAL_BIENES_BAJAS		= number_format(utf8_encode(odbc_result($RS_Inv,"TOTAL_BIENES_BAJAS")),0);
	$FECHA_FINALIZA_INV		= cambiaf_a_normal(odbc_result($RS_Inv,"FECHA_FINALIZA_INV"));
	$ID_ESTADO_INV			= odbc_result($RS_Inv,"ID_ESTADO_INV");
	$COD_UE_PERS_CP			= odbc_result($RS_Inv,"COD_UE_PERS_CP");	
	$COD_VERIFICACION		= odbc_result($RS_Inv,"COD_VERIFICACION");
	
	
	$NOM_ARCHIVO_INF_FINAL_INV		= odbc_result($RS_Inv,"NOM_ARCHIVO_INF_FINAL_INV");
	$ADJ_ARCHIVO_INF_FINAL_INV 		= ($NOM_ARCHIVO_INF_FINAL_INV != '') ? "X" : "" ;
	$FECHA_ARCHIVO_INF_FINAL_INV	= cambiaf_a_normal(odbc_result($RS_Inv,"FECHA_ARCHIVO_INF_FINAL_INV"));
	
	
	$NOM_ARCHIVO_ACTA_CONCILI_INV		= odbc_result($RS_Inv,"NOM_ARCHIVO_ACTA_CONCILI_INV");
	$ADJ_ARCHIVO_ACTA_CONCILI_INV 		= ($NOM_ARCHIVO_ACTA_CONCILI_INV != '') ? "X" : "" ;
	$FECHA_ARCHIVO_ACTA_CONCILI_INV		= cambiaf_a_normal(odbc_result($RS_Inv,"FECHA_ARCHIVO_ACTA_CONCILI_INV"));


	$RS_CPATRIM 			= $oSimi_UE_Personal->Ver_Datos_Simi_UE_Personal_x_CODIGO($COD_UE_PERS_CP);
	$NRO_DOCUMENTO			= htmlspecialchars_decode(odbc_result($RS_CPATRIM,"NRO_DOCUMENTO"));
	$RESP_UE_NOMBRES_PERS	= htmlspecialchars_decode(odbc_result($RS_CPATRIM,"UE_NOMBRES_PERS"));
	$RESP_UE_APEPAT_PERS	= htmlspecialchars_decode(odbc_result($RS_CPATRIM,"UE_APEPAT_PERS"));
	$RESP_UE_APEMAT_PERS	= htmlspecialchars_decode(odbc_result($RS_CPATRIM,"UE_APEMAT_PERS"));
	
	//$CONTINV++;		

	if($ID_ESTADO_INV == '1'){
		$Desc_Estado 	= 'FINALIZADO';
	}else{
		$Desc_Estado 	= 'SIN REGISTRAR';
	}
	
	
	/****************************************************************************************************************/
	/****************************************************************************************************************/
	/****************************************************************************************************************/
	
	

	
	
	/*****************************************************************************************************************/
	/*****************************************************************************************************************/
	/*****************************************************************************************************************/
	
	
	class PDF extends FPDF{
		
		
		
		var $B;
var $I;
var $U;
var $HREF;

/*
function PDF($orientation='P', $unit='mm', $size='A4')
{
    // Llama al constructor de la clase padre
    $this->FPDF($orientation,$unit,$size);
    // Iniciación de variables
    $this->B = 0;
    $this->I = 0;
    $this->U = 0;
    $this->HREF = '';
}*/

//Función para escribir en código HTML y que se detecten las etiquetas
function WriteHTML($html)
{
    // Intérprete de HTML
    $html = str_replace("\n",' ',$html);
    $a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            // Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(5,$e);
        }
        else
        {
            // Etiqueta
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                // Extraer atributos
                $a2 = explode(' ',$e);
                $tag = strtoupper(array_shift($a2));
                $attr = array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])] = $a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag, $attr)
{
    // Etiqueta de apertura
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF = $attr['HREF'];
    if($tag=='BR')
        $this->Ln(5);
}

function CloseTag($tag)
{
    // Etiqueta de cierre
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF = '';
}

function SetStyle($tag, $enable)
{
    // Modificar estilo y escoger la fuente correspondiente
    $this->$tag += ($enable ? 1 : -1);
    $style = '';
    foreach(array('B', 'I', 'U') as $s)
    {
        if($this->$s>0)
            $style .= $s;
    }
    $this->SetFont('',$style);
}

function PutLink($URL, $txt)
{
    // Escribir un hiper-enlace
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(5,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}
		
		
		
		
		function Header(){
			
			// Logo	
			$this->Image('../../../webimages/iconos/sbn_11.png',10,5,18);//ancho,alto
	
			//$this->MultiCell(42, 5, 'Superintendencia Nacional de Bienes Estatales', 0, 1);
			$this->SetFont('Arial','',9);
			
			$this->Ln(0);
			$this->Cell(19);
			$this->Cell(1,1,'Superintendencia Nacional',0,0,'L');
			
			$this->Cell(294,1,utf8_decode('Página : ').$this->PageNo().'/{nb}',0,0,'C');
			$this->Ln(4);
			$this->Cell(19);
			$this->Cell(1,1,'de Bienes Estatales',0,0,'L');
			$this->Cell(300,1,utf8_decode('Reporte : '.date('d/m/Y')),0,0,'C');
			
			//$this->MultiCell(42, 5, 'Superintendencia Nacional de Bienes Estatales', 0, 1);
			
			$this->Cell(150);
			
			
			//$this->Ln(-1);
			$this->SetFont('Arial','B',15);		
			$this->Ln(-5);
			//$this->Cell(18);
			$this->Cell(0,1,utf8_decode('Sustento del Inventario'),0,0,'C');
			
			$this->Ln(6);
			$this->SetFont('Arial','',10);
			$this->Cell(0,1,'de Bienes Muebles',0,0,'C');
			
	
			//Dibujamos una linea
			$this->Line(10, 20, 196, 20);
			// Salto de línea
			$this->Ln(5);
				
		}
		
		// Pie de página
		/*function Footer(){
			// Posición: a 1,5 cm del final
			$this->SetY(245);
				
			$this->Ln(1);	
			$this->SetFont('Arial','',9);
			$this->Cell(1,1,utf8_decode('El presente documento tiene la calidad de declaración jurada de acuerdo al artículo 42 de la Ley 27444, Ley del'),0,0,'L');
			
			$this->Ln(1);
			$this->Cell(1,9,'Procedimiento Administrativo General.',0,0,'L');
			
			$this->Ln(4);
			$this->SetFont('Arial','',8);
			$this->Cell(0,10,utf8_decode('Página : ').$this->PageNo().'/{nb}',0,0,'R');
			
			$this->Ln();
			$this->Ln(8);
			
			$this->Cell(5);
			$this->Cell(1,1,'________________________________________',0,0,'L');	
			
			$this->Ln(5);
			
			$this->Cell(10);
			$this->Cell(1,1,'  Firma y sello del Titular de la Entidad',0,0,'L');
			
		}*/
		
			
	}
	
	
	
		// Creación del objeto de la clase heredada
		//$oFPDF = new FPDF();
		//$oFPDF = new FPDF('P', 'mm', 'A4'); //-> oficio
		//$oFPDF = new FPDF('P', 'mm', 'A4'); //-> oficio
		//$oFPDF = new FPDF('P','mm',array(600,150));
	
	
		// Creación del objeto de la clase heredada
		$pdf = new PDF();
		//$pdf = new FPDF('P', 'mm', 'A4'); //-> oficio
		$pdf->AliasNbPages();

		$pdf->SetFont('Arial','',10);
		$pdf->AddPage();
		$pdf->Ln(4);

		//****************************************
		//****************************************
		
		$pdf->SetFillColor(205,205,205);//color de fondo tabla
		$pdf->SetTextColor(10);
		$pdf->SetDrawColor(153,153,153);
		$pdf->SetLineWidth(.3);
		$pdf->SetFont('Arial','B',9);
		
		$pdf->SetFillColor(205,205,205);
		$pdf->Cell(45,8,'ENTIDAD',1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(140,8,$NOM_ENTIDAD,1,0,'L',true);
				
		$pdf->Ln();
		$pdf->Ln(3);
		
		//****************************************
		// DATOS DEL PERSONAL Y CONTROL PATRIMONIAL
		//****************************************
		
		//--------------------		
		//DATOS DEL INVENTARIO
		//--------------------
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(185,7,utf8_decode('DATOS DEL INVENTARIO '. $PERIODO),1,0,'L',true);		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(52,6,utf8_decode('Período'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(40,6,$PERIODO,1,0,'L',true);	
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(52,6,utf8_decode('Estado del Inventario'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(41,6,$Desc_Estado,1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(52,6,utf8_decode('Fecha Inicio del Reg. Inventario'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(40,6,$FECHA_INICIO,1,0,'L',true);
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(52,6,utf8_decode('Fecha Vence el Reg. Inventario'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(41,6,$FECHA_TERMINO,1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(185,3,'',1,0,'L',true);	
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(52,6,utf8_decode('Fecha Finaliza Inventario'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(40,6,$FECHA_FINALIZA_INV,1,0,'L',true);
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(52,6,utf8_decode('Código de Finalización'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(41,6,$COD_VERIFICACION,1,0,'L',true);
		
				
		$pdf->Ln();
		$pdf->Ln(3);
		
		//--------------------		
		//DATOS DEL RESPONSABLE DE CONTROL PATRIMONIAL 
		//--------------------
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(185,7,utf8_decode('DATOS DEL RESPONSABLE DE CONTROL PATRIMONIAL'),1,0,'L',true);		
				
		$pdf->Ln();
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(32,6,utf8_decode('Nro de DNI'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(60,6,$NRO_DOCUMENTO,1,0,'L',true);	
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(32,6,utf8_decode('Nombres'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(61,6,$RESP_UE_NOMBRES_PERS,1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(32,6,utf8_decode('Apellido Paterno'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(60,6,$RESP_UE_APEPAT_PERS,1,0,'L',true);
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(32,6,utf8_decode('Apellido Materno'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(61,6,$RESP_UE_APEMAT_PERS,1,0,'L',true);
		
						
		$pdf->Ln();
		$pdf->Ln(3);
		
		//--------------------
		//TOTALIDAD
		//--------------------
		
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(185,7,utf8_decode('TOTALIDAD DEL INVENTARIO ANUAL'),1,0,'L',true);		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Total locales y/o predios'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(25,6,$TOTAL_LOCAL_GRAL,1,0,'L',true);	
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(68,6,utf8_decode('Total Bienes en General'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(24,6,$TOTAL_BIENES_GRAL,1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Total locales y/o predios con Inventario'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(25,6,$TOTAL_LOCAL_SI_INV,1,0,'L',true);
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(68,6,utf8_decode('Total Bienes Muebles Activos'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(24,6,$TOTAL_BIENES_ACTIVOS,1,0,'L',true);
		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Total locales y/o predios sin Inventario'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(25,6,$TOTAL_LOCAL_NO_INV,1,0,'L',true);
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(68,6,utf8_decode('Total Bienes Muebles Bajas'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(24,6,$TOTAL_BIENES_BAJAS,1,0,'L',true);
		
		
		//--------------------
		//INFORME FINAL
		//--------------------
		
		$pdf->Ln();
		$pdf->Ln(3);

		
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(185,7,utf8_decode('INFORME FINAL DEL INVENTARIO'),1,0,'L',true);		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Nombre del Archivo Adjunto'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(117,6,$NOM_ARCHIVO_INF_FINAL_INV,1,0,'L',true);	
		//$pdf->WriteHTML('<a href="http://www.sbn.gob.pe/repositorio_muebles/inventario/doc_informe_final_inv/'.utf8_decode($NOM_ARCHIVO_INF_FINAL_INV).'" target="_blank">'.utf8_decode($NOM_ARCHIVO_INF_FINAL_INV).'</a>');
		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Adjunto Archivo'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(25,6,$ADJ_ARCHIVO_INF_FINAL_INV,1,0,'L',true);	
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(68,6,utf8_decode('Fecha del Adjunto'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(24,6,$FECHA_ARCHIVO_INF_FINAL_INV,1,0,'L',true);
		
		
		//--------------------
		//ACTA DE CONCILIACIÓN
		//--------------------
		
		$pdf->Ln();
		$pdf->Ln(3);

		
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(185,7,utf8_decode('ACTA DE CONCILIACIÓN'),1,0,'L',true);		
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Nombre del Archivo Adjunto'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(117,6,$NOM_ARCHIVO_ACTA_CONCILI_INV,1,0,'L',true);	
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(68,6,utf8_decode('Adjunto Archivo'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(25,6,$ADJ_ARCHIVO_ACTA_CONCILI_INV,1,0,'L',true);	
		//$pdf->WriteHTML('<a href="http://www.sbn.gob.pe/repositorio_muebles/inventario/doc_informe_final_inv/'.utf8_decode($ADJ_ARCHIVO_ACTA_CONCILI_INV).'" target="_blank">'.utf8_decode($ADJ_ARCHIVO_ACTA_CONCILI_INV).'</a>');
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(68,6,utf8_decode('Fecha del Adjunto'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(24,6,$FECHA_ARCHIVO_ACTA_CONCILI_INV,1,0,'L',true);
				
		/*******************************************************/
		//DETALLE DEL BIEN
		/*******************************************************/
		
		$pdf->Ln();
		$pdf->Ln(5);

		
		//DETALLE DEL BIEN
		$pdf->SetFont('Arial','B',9);
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(146,5,utf8_decode('DETALLE DEL INVENTARIO ANUAL'),1,0,'L',true);
		
		$pdf->SetFillColor(205,205,205);
		$pdf->Cell(39,5,utf8_decode('Total Bienes'),1,0,'C',true);		
	
		
		$pdf->Ln();
		
		$pdf->SetFont('Arial','B',8);
		$w = array(8, 138, 13, 13, 13);
		$pdf->Cell($w[0],5,utf8_decode('Item'),1,0,'L',true);
		$pdf->Cell($w[1],5,utf8_decode('Local y/o Predio'),1,0,'L',true);
		$pdf->Cell($w[2],5,utf8_decode('General'),1,0,'C',true);
		$pdf->Cell($w[3],5,utf8_decode('Activos'),1,0,'C',true);
		$pdf->Cell($w[4],5,utf8_decode('Bajas'),1,0,'C',true);
		$pdf->Ln();
		
		// Restauración de colores y fuentes
		$pdf->SetFillColor(224,235,255);
		$pdf->SetTextColor(0);
		$pdf->SetFont('Arial','',7);
		// Datos
		$fill = false;
		
		
		$contador = 0;
		
		$RS_Inv_2 = $oSimi_UE_Bien_Inventario->UE_Lista_Entidades_Inventario_CON_COD_INVENTARIO_ENT($COD_INVENTARIO_ENT);
		
		while (odbc_fetch_row($RS_Inv_2)){

			$DENOMINACION_PREDIO	= htmlspecialchars_decode(odbc_result($RS_Inv_2,"DENOMINACION_PREDIO"));
			$TOT_BIEN_GRAL			= number_format(odbc_result($RS_Inv_2,"TOT_BIEN_GRAL"),0);
			$TOT_BIEN_ACTIVO		= number_format(odbc_result($RS_Inv_2,"TOT_BIEN_ACTIVO"),0);
			$TOT_BIEN_BAJA			= number_format(odbc_result($RS_Inv_2,"TOT_BIEN_BAJA"),0);
			
			$contador++;
			
			if($contador == 21){
				$pdf->Cell(array_sum($w),0,'','T');
				$pdf->AddPage();
				$linea = 0;
			}else{
				$linea++;
			}
						
			if($linea!= 0 && $linea%58==0 ){
				$pdf->Cell(array_sum($w),0,'','T');
				$pdf->AddPage();
			 }
	
			$pdf->Cell($w[0],4,$contador,'LR',0,'L');
			$pdf->Cell($w[1],4,$DENOMINACION_PREDIO,'LR',0,'L');
			$pdf->Cell($w[2],4,$TOT_BIEN_GRAL,'LR',0,'R');
			$pdf->Cell($w[3],4,$TOT_BIEN_ACTIVO	,'LR',0,'R');
			$pdf->Cell($w[4],4,$TOT_BIEN_BAJA,'LR',0,'R');
			$pdf->Ln();
			$fill = !$fill;
		}
		
		// Línea de cierre
		$pdf->Cell(array_sum($w),0,'','T');
		
		$pdf->Ln();

		
		//$pdf->SetY(165);
			
		$pdf->Ln(15);	
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(1,1,utf8_decode('El presente documento tiene la calidad de declaración jurada de acuerdo al artículo 42 de la Ley 27444, Ley del'),0,0,'L');
		
		$pdf->Ln(1);
		$pdf->Cell(1,9,'Procedimiento Administrativo General.',0,0,'L');
		
		$pdf->Ln();
		$pdf->Ln(8);
		
		$pdf->Cell(5);
		$pdf->Cell(1,1,'________________________________________',0,0,'L');	
		
		$pdf->Ln(5);
		
		$pdf->Cell(10);
		$pdf->Cell(1,1,'  Firma y sello del Titular de la Entidad',0,0,'L');
			


		$pdf->Output();

}else{
	echo "No es la direccion Web Correcta";
}
?>


