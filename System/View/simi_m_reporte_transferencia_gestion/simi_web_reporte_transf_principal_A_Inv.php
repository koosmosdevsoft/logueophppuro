<? error_reporting(0);
// session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 


<style type="text/css">
  * {margin: 0; padding: 0;}
  #container {height: 100%; width:100%; font-size: 0;}
  #left, #middle, #right {display: inline-block; *display: inline; zoom: 1; vertical-align: top; font-size: 12px;}
  #left {width: 87%;}
  #middle {width: 60%;    margin-top: 10%;}
  #right {width: 12%;float: right;}
</style>


<?
require_once('../../Controller/C_simi_m_ue_inventario.php');
require_once('../../../utils/funciones/funciones.php');

$oSimi_UE_Bien_Inventario	=	new Simi_UE_Bien_Inventario;

if($opt == 'Listar_Reporte_inv'){
	$COD_ENTIDAD 			= $_REQUEST['sCOD_ENTIDAD'];	
}else{
	$COD_ENTIDAD			= $_SESSION['th_SIMI_COD_ENTIDAD'];
}
?>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

<script>
function subir_informe_sustento1(xCOD_INVENTARIO_ENT,identificador,xCOD_ENTIDAD){
  //console.log($("#informe_sustento1").get(0).files[0].type);
  //alert('.. '+ xestado);
   if(['application/pdf'].indexOf($("#informe_sustento1"+identificador).get(0).files[0].type) == -1) {
        alert('Error: Solo se permiten estos archivos .PDF');
        return;
    } else {
      var file_data = $('#informe_sustento1'+identificador).prop('files')[0];
      var form_data = new FormData();
      form_data.append('filex', file_data);
      form_data.append('txh_COD_INVENTARIO', xCOD_INVENTARIO_ENT);
      form_data.append('TXH_SIMI_COD_ENTIDAD', xCOD_ENTIDAD );
      console.log(form_data);      

    $.ajax({
      url: 'Model/M_simi_m_ue_inventario.php?operacion=Adjuntar_Informe_Inv_Transf',
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      beforeSend: function () {
        if (confirm('Esta seguro de subir el archivo?')) {
          cargar_loading('div_nom_file_informe');
        }else{
          alert("Has cancelado subir el archivo : "+file);
          return false;
        }
      },
      success: function(data){
        console.log(data); 
        if(data.valor == 1){
          alert('se subio el archivo');
          //console.log(data.descarga_inv);
          var descarga_inv1 = data.descarga_inv;
          if (data.cantidad >= 1){
            //console.log('$H_NOM_ARCHIVO_INF_SUST_INV'+identificador);
            $('#imag01'+identificador).html('');
            $('#imag01'+identificador).html('<a href="../../../repositorio_muebles/inventario/doc_informe_muebles_inv_transf/'+descarga_inv1+'" target="_blank" class="texto_arial_plomo_n_12"><img src="../webimages/iconos/archivoPDF.png" width="20" height="20" border="0"></a>');
            $('#informe_sustento1'+identificador).attr('disabled','disabled');
            // $('#imagen1_sub').hide();
          }
        }else{
          alert('ERROR');
        }
      }
     });
    }
}

function subir_informe_sustento2(xCOD_INVENTARIO_ENT,identificador,xCOD_ENTIDAD){
  //console.log($("#informe_sustento1").get(0).files[0].type);
  //alert('.. '+ xCOD_INVENTARIO_ENT);
   if(['application/pdf'].indexOf($("#informe_sustento2"+identificador).get(0).files[0].type) == -1) {
        alert('Error: Solo se permiten estos archivos .PDF');
        return;
    } else {
      var file_data = $('#informe_sustento2'+identificador).prop('files')[0];
      var form_data = new FormData();
      form_data.append('filex', file_data);
      form_data.append('txh_COD_INVENTARIO', xCOD_INVENTARIO_ENT);
      form_data.append('TXH_SIMI_COD_ENTIDAD', xCOD_ENTIDAD );
      console.log(form_data);      

    $.ajax({
      url: 'Model/M_simi_m_ue_inventario.php?operacion=Adjuntar_Informe_Inv_Transf_Firmado',
      dataType: 'json',
      cache: false,
      contentType: false,
      processData: false,
      data: form_data,
      type: 'post',
      beforeSend: function () {
        if (confirm('Esta seguro de subir el archivo?')) {
          cargar_loading('div_nom_file_informe');
        }else{
          alert("Has cancelado subir el archivo : "+file);
          return false;
        }
      },
      success: function(data){
        console.log(data); 
        if(data.valor == 1){
          alert('se subio el archivo');
          var descarga_firma1 = data.descarga_firma;
          if (data.cantidad >= 1){
            $('#imag02'+identificador).html('');
            $('#imag02'+identificador).html('<a href="../../../repositorio_muebles/inventario/doc_informe_muebles_inv_transf/'+descarga_firma1+'" target="_blank" class="texto_arial_plomo_n_12"><img src="../webimages/iconos/archivoPDF.png" width="20" height="20" border="0"></a>')
            $('#informe_sustento2'+identificador).attr('disabled','disabled');
          }
        }else{
          alert('ERROR');
        }
      }
     });
    }
}

</script>

<table width="1551" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td height="15" colspan="5" valign="top"><hr class="hr_01" /></td>
  </tr>
  <tr>
    <td colspan="5">
      <div id="container" style="height:100%; width:100%">
        <div id="left" class="alert alert-danger" role="alert">
          <p style="text-align: justify;" class="Titulo_01_12px">
            El Informe de complemento de sustento de Inventario, debe de ser firmado por la autoridad saliente, escaneado en pdf y vinculado a este Módulo de Bienes Muebles/Transferencia de Gestión. Para el Informe de complemento, se debe de usar el formato del informe final del inventario, con las compras, altas. Bajas y otros actos relevantes del presente ejercicio (desde 01 de enero 2018 a la fecha de corte para la transferencia), que no se encuentren en el inventario, informe final y el acta de conciliación patrimonio contable del año 2017, remitido por su entidad mediante este módulo.
            </p>
        </div>
        <div id="right" style="padding-top: 15px;">
          <a id="img_nota" href="#">
            <img src="../webimages/iconos/nota.jpg" width="45" height="46" border="0">
            <div id="middle">
              <p class="Titulo_01_12px" style="color:#41A5E3; font-weight: 900;">Nota informativa</p>
            </div>
          </a>
        </div>
      </div>
    </td>
  </tr>
  <tr>
    <td height="15" valign="top">&nbsp;</td>
    <td height="15" valign="top">&nbsp;</td>
    <td height="15" valign="top">&nbsp;</td>
    <td height="15" valign="top">&nbsp;</td>
    <td height="15" valign="top">&nbsp;</td>
  </tr>
</table>

 <table width="1556" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" id="tbl_Resultado_Lista_ITL" class="ReportA2C">
   <tr>
     <th width="30" height="25" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:center">Item</th>
     <th width="137" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:left">Periodo</th>
     <th width="90" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:center">Fecha  de<br />
       Termino<br /></th>
     <th width="73" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:center">Total       Locales <br />
       y/o       Predios</th>
     <th width="86" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:center">Total       Locales <br />
       y/o       Predios<br />
       Sin Inventario</th>
     <th width="98" bgcolor="#f4f4f4" class="Titulo_01_10px" style="text-align:center">Total       Locales <br />
       y/o       Predios<br />
       Con Inventario</th>
     <th width="66" bgcolor="#f4f4f4" class="Titulo_01_10px separador_borde" style="text-align:center">Total de
Bienes <br />
General</th>
     <th width="109" bgcolor="#f4f4f4" class="Titulo_01_10px separador_borde" style="text-align:center">Estado  <br />
     Cumplimiento  <br />
     Inventario</th>
     <th width="100" bgcolor="#f4f4f4" class="Titulo_01_10px separador_borde" style="text-align:center">Descargar<br />
       Sustento<br />
       Inventario</th>
             <!-- Descarga de bienes commentado-->
     <!-- <th width="93" bgcolor="#f4f4f4" class="Titulo_01_10px separador_borde" style="text-align:center">Descargar<br />
       Inventario</th> -->
     <th width="57" bgcolor="#f4f4f4" class="Titulo_01_10px separador_borde" style="text-align:center">Informe<br />
     Final</th>
     <th width="61" bgcolor="#f4f4f4" class="Titulo_01_10px separador_borde" style="text-align:center">Acta de<br />
       Conciliación</th>
     <th width="57" bgcolor="#f4f4f4" class="Titulo_01_10px separador_borde" style="text-align:center">Subir Informe de <br />
     complemento de <br />
     sustento de <br />
     Inventario / Firmado</th>  
     <th width="23" bgcolor="#f4f4f4" class="Titulo_01_10px separador_borde" style="text-align:center">Descargar Informe de<br />
     Complemento <br />de sustento <br />de Inventario / Firmado</th>
     <th width="61" bgcolor="#f4f4f4" class="Titulo_01_10px separador_borde" style="text-align:center">Subir Sustento de<br />
       Inventario/Firmado</th>
         <th width="23" bgcolor="#f4f4f4" class="Titulo_01_10px separador_borde" style="text-align:center">Descargar Sustento de<br />
       Inventario/Firmado</th>
   </tr>
   <?
  $H_count_inv = 0;
	$Result_List_Inve_Hist = $oSimi_UE_Bien_Inventario->UE_Lista_Historial_Inventarios_x_Entidad($COD_ENTIDAD);
	while (odbc_fetch_row($Result_List_Inve_Hist)){
	
		
    $H_COD_ENTIDAD     = utf8_encode(odbc_result($Result_List_Inve_Hist,"COD_ENTIDAD"));
		$H_COD_INVENTARIO			= utf8_encode(odbc_result($Result_List_Inve_Hist,"COD_INVENTARIO"));
		$H_NOM_INVENTARIO			= utf8_encode(odbc_result($Result_List_Inve_Hist,"NOM_INVENTARIO"));
		$H_PERIODO					= odbc_result($Result_List_Inve_Hist,"PERIODO");
		$H_FECHA_INICIO				= cambiaf_a_normal(odbc_result($Result_List_Inve_Hist,"FECHA_INICIO"));
		$H_FECHA_TERMINO			= cambiaf_a_normal(odbc_result($Result_List_Inve_Hist,"FECHA_TERMINO"));
		$H_COD_INVENTARIO_ENT		= utf8_encode(odbc_result($Result_List_Inve_Hist,"COD_INVENTARIO_ENT"));
		$H_TOTAL_LOCAL_GRAL			= utf8_encode(odbc_result($Result_List_Inve_Hist,"TOTAL_LOCAL_GRAL"));
		$H_TOTAL_LOCAL_SI_INV		= utf8_encode(odbc_result($Result_List_Inve_Hist,"TOTAL_LOCAL_SI_INV"));
		$H_TOTAL_LOCAL_NO_INV		= utf8_encode(odbc_result($Result_List_Inve_Hist,"TOTAL_LOCAL_NO_INV"));
		$H_TOTAL_BIENES_GRAL		= utf8_encode(odbc_result($Result_List_Inve_Hist,"TOTAL_BIENES_GRAL"));
		$H_TOTAL_BIENES_ACTIVOS		= utf8_encode(odbc_result($Result_List_Inve_Hist,"TOTAL_BIENES_ACTIVOS"));
		$H_TOTAL_BIENES_BAJAS		= utf8_encode(odbc_result($Result_List_Inve_Hist,"TOTAL_BIENES_BAJAS"));
		$H_FECHA_FINALIZA_INV		= cambiaf_a_normal(odbc_result($Result_List_Inve_Hist,"FECHA_FINALIZA_INV"));
		
		$H_NOM_ARCHIVO_INF_FINAL_INV		= odbc_result($Result_List_Inve_Hist,"NOM_ARCHIVO_INF_FINAL_INV");
		$H_FECHA_ARCHIVO_INF_FINAL_INV		= cambiaf_a_normal(odbc_result($Result_List_Inve_Hist,"FECHA_ARCHIVO_INF_FINAL_INV"));
		$H_NOM_ARCHIVO_ACTA_CONCILI_INV		= odbc_result($Result_List_Inve_Hist,"NOM_ARCHIVO_ACTA_CONCILI_INV");
		$H_FECHA_ARCHIVO_ACTA_CONCILI_INV	= cambiaf_a_normal(odbc_result($Result_List_Inve_Hist,"FECHA_ARCHIVO_ACTA_CONCILI_INV"));		
    
    $H_CANT_SUST      = odbc_result($Result_List_Inve_Hist,"CANT_SUST");
    $H_CANT_FIRMA      = odbc_result($Result_List_Inve_Hist,"CANT_FIRMA");

		$H_FINALIZO							= odbc_result($Result_List_Inve_Hist,"FINALIZO");
		$H_FECHA_FINALIZO					= cambiaf_a_normal(odbc_result($Result_List_Inve_Hist,"FECHA_FINALIZO"));
		
		$H_EST_CUMPLI_INV					= odbc_result($Result_List_Inve_Hist,"EST_CUMPLI_INV");
				
		$H_ID_ESTADO_INV	= odbc_result($Result_List_Inve_Hist,"ID_ESTADO_INV");
		$H_APERTURA			= odbc_result($Result_List_Inve_Hist,"APERTURA");
		$H_Desc_Estado		= odbc_result($Result_List_Inve_Hist,"ESTADO");
    $H_NOM_ARCHIVO_INF_SUST_INV2         = odbc_result($Result_List_Inve_Hist,"NOM_ARCHIVO_INF_SUST_INV");
    $H_NOM_ARCHIVO_INF_SUST_INV_FIRMA2   = odbc_result($Result_List_Inve_Hist,"NOM_ARCHIVO_INF_SUST_INV_FIRMA");
		
    $Result_List_Descarga2 = $oSimi_UE_Bien_Inventario->UE_Lista_muebles_Transf_Descarga($COD_ENTIDAD,$H_COD_INVENTARIO_ENT);
    $H_NOM_ARCHIVO_INF_SUST_INV         = odbc_result($Result_List_Descarga2,"NOM_ARCHIVO_INF_SUST_INV");
    $H_NOM_ARCHIVO_INF_SUST_INV_FIRMA   = odbc_result($Result_List_Descarga2,"NOM_ARCHIVO_INF_SUST_INV_FIRMA");
    //echo $H_NOM_ARCHIVO_INF_SUST_INV;
		$H_count_inv++;
				
		if($H_Desc_Estado == 'ABIERTO'){
			$H_COLOR_ESTADO 	= 'class="texto_arial_verde_n_11"';
		}else{
			$H_COLOR_ESTADO 	= 'class="texto_arial_rojo_n_11"';
		}
		if($H_EST_CUMPLI_INV == 'CUMPLIÓ'){
			$H_COLOR_CUMPLI_INV 	= 'class="texto_arial_verde_n_11"';
		}elseif($H_EST_CUMPLI_INV == 'CUMPLIÓ PARCIALMENTE'){
			$H_COLOR_CUMPLI_INV 	= 'class="texto_arial_azul_n_11"';
		}else{
			$H_COLOR_CUMPLI_INV 	= 'class="texto_arial_rojo_n_11"';
		}
		
		$VBtn = "Inventario ".$H_PERIODO;
		
		//$valor_Adjunto_INF_FINAL_INV 	= '1';	
		//$url_archivo_INF_FINAL_INV		= '../../repositorio_muebles/inventario/doc_informe_final_inv/111.pdf';	
				
	?>

   <tr  onmouseover="mOvr2(this)" onmouseout="mOut2(this)">
     <td height="22" style="color:rgb(0, 156, 213); text-align:center"><?=$H_count_inv?></td>
     <td height="22" style="color:rgb(0, 156, 213); text-align:left"><?=$H_NOM_INVENTARIO?></td>
     <td height="22" style="color:rgb(0, 156, 213); text-align:center"><?=$H_FECHA_FINALIZA_INV?></td>
     <td style="color:rgb(0, 156, 213); text-align:center"><?=$H_TOTAL_LOCAL_GRAL?></td>
     <td style="color:rgb(0, 156, 213); text-align:center"><?=$H_TOTAL_LOCAL_NO_INV?></td>
     <td style="color:rgb(0, 156, 213); text-align:center" ><?=$H_TOTAL_LOCAL_SI_INV?></td>
     <td style="color:rgb(0, 156, 213); text-align:center" class="separador_borde"><?=$H_TOTAL_BIENES_GRAL?></td>
     <td style="color:rgb(0, 156, 213); text-align:center" class="separador_borde">
	<label <?=$H_COLOR_CUMPLI_INV?>><?=$H_EST_CUMPLI_INV?></label>   
     </td>
     <td height="50" class="separador_borde" style="color:rgb(0, 156, 213); text-align:center" >
     <? if($H_EST_CUMPLI_INV == 'CUMPLIÓ' ){ ?>
     <a href="#" title="Descargar Sustento" onclick="Ver_Reporte_Inv('<?=$H_PERIODO?>')"><img src="../webimages/iconos/IMG_Sustento.png" width="45" height="46" border="0" /></a>
     <? }else{ ?>
     <!--<a href="#" title="El sustento esta pendiente por adjuntar Informe Final y Acta de Conciliación"> Pendiente</a>-->
     <? } ?>
     </td>

           <!-- Descarga de bienes commentado-->
     <!-- <td style="color:rgb(0, 156, 213); text-align:center" class="separador_borde">
       <? if($H_ID_ESTADO_INV == '1'){ ?>
       <a href="#" onclick="Mostrar_Form_Descargas_Locales('<?=$H_COD_INVENTARIO_ENT?>')">Descargar <br /> Bienes</a>
       <? } ?>
     </td>      -->
     <td style="color:rgb(0, 156, 213); text-align:center" class="separador_borde">
     <? if($H_EST_CUMPLI_INV == 'CUMPLIÓ' && $H_NOM_ARCHIVO_INF_FINAL_INV != ''  && $H_PERIODO <='2017'){ ?>
     <a href="../../repositorio_muebles/inventario/doc_informe_final_inv/<?php echo $H_NOM_ARCHIVO_INF_FINAL_INV?>" target="_blank">
     <img src="../webimages/iconos/archivoPDF.png" width="20" height="20" border="0" /></a>
     <? }elseif($H_EST_CUMPLI_INV == 'CUMPLIÓ' && $H_NOM_ARCHIVO_INF_FINAL_INV != '' && $H_PERIODO >='2018'){ ?>
     <a href="../../repositorio_muebles/Inventarios_zip/doc_informe_final_inv/<?php echo $H_NOM_ARCHIVO_INF_FINAL_INV?>" target="_blank">
     <img src="../webimages/iconos/archivoPDF.png" width="20" height="20" border="0" /></a>
     <? }else{ echo "---"; }?>
     </td>
     <td style="color:rgb(0, 156, 213); text-align:center" class="separador_borde">
     <? if($H_EST_CUMPLI_INV == 'CUMPLIÓ' && $H_NOM_ARCHIVO_ACTA_CONCILI_INV != '' && $H_PERIODO <='2017'){ ?>
     <a href="../../repositorio_muebles/inventario/doc_acta_conciliacion/<?php echo $H_NOM_ARCHIVO_ACTA_CONCILI_INV?>" target="_blank">
     <img src="../webimages/iconos/archivoPDF.png" width="20" height="20" border="0" /></a>
     <? }elseif($H_EST_CUMPLI_INV == 'CUMPLIÓ' && $H_NOM_ARCHIVO_ACTA_CONCILI_INV != ''  && $H_PERIODO >='2018'){ ?>
     <a href="../../repositorio_muebles/Inventarios_zip/doc_acta_conciliacion/<?php echo $H_NOM_ARCHIVO_ACTA_CONCILI_INV?>" target="_blank">
     <img src="../webimages/iconos/archivoPDF.png" width="20" height="20" border="0" /></a>
      <? }else{ echo "---"; }?>
     </td>
      <td style="color:rgb(0, 156, 213); text-align:center" class="separador_borde">
     <? if($H_EST_CUMPLI_INV == 'CUMPLIÓ' ){ ?>
     

     <!-- CARGA DE SUSTENTO 1 -->
     	<div class="image-upload" >
  			<label for="informe_sustento1<?php echo $H_count_inv; ?>" title="Subir Sustento">
    			<img src="../webimages/iconos/agregar_archivo.png" width="52" height="40"/>
  			</label>
      <? if($H_CANT_SUST == 0 ){  ?>
			<input id="informe_sustento1<?php echo $H_count_inv; ?>" disabled name="informe_sustento1" type="file" style="width:3px" onchange="subir_informe_sustento1('<?=$H_COD_INVENTARIO_ENT?>',<?php echo $H_count_inv; ?>,'<?=$H_COD_ENTIDAD?>')"  /> 
      <? }else{ echo ""; }?>
		</div>

	<style>
    .image-upload > input {
	  visibility:hidden;
	  width:0;
	  height:0
	}
    </style>
     <? }else{ echo "---"; }?>
     </td>
      <td style="color:rgb(0, 156, 213); text-align:center" class="separador_borde">
        <div id="imag01<?php echo $H_count_inv;?>" class="fila_pdf"></div>
        <? if($H_CANT_SUST > 0 ){  ?>
        <a href="../../../repositorio_muebles/inventario/doc_informe_muebles_inv_transf/<?php echo $H_NOM_ARCHIVO_INF_SUST_INV2; ?>" target="_blank" class="texto_arial_plomo_n_12"><img src="../webimages/iconos/archivoPDF.png" width="20" height="20" border="0" id="imagen1_sub"/></a>
        <? }else{ echo ""; }?>
     </td>
     <td style="color:rgb(0, 156, 213); text-align:center" class="separador_borde">
     <? if($H_EST_CUMPLI_INV == 'CUMPLIÓ' ){  ?>

     <!-- CARGA DE SUSTENTO 2 -->
     	<div class="image-upload">
  			<label for="informe_sustento2<?php echo $H_count_inv; ?>" title="Subir Sustento">
    			<img src="../webimages/iconos/agregar_archivo.png" width="52" height="40"/>
  			</label>
      <? if($H_CANT_FIRMA == 0 ){  ?>  
			<input id="informe_sustento2<?php echo $H_count_inv; ?>" disabled name="informe_sustento2" type="file" style="width:3px" onchange="subir_informe_sustento2('<?=$H_COD_INVENTARIO_ENT?>',<?php echo $H_count_inv; ?>,'<?=$H_COD_ENTIDAD?>')"  /> 
      <? }else{ echo ""; }?>
		</div>

	<style>
    .image-upload > input {
	  visibility:hidden;
	  width:0;
	  height:0
	}
    </style>
     <? }else{ echo "---"; }?>
     </td>
      <td style="color:rgb(0, 156, 213); text-align:center" class="separador_borde">
        <div id ="imag02<?php echo $H_count_inv;?>" class="fila2_pdf"></div>
          
        <? if($H_CANT_FIRMA > 0 ){  ?>
        <a href="../../../repositorio_muebles/inventario/doc_informe_muebles_inv_transf/<?php echo $H_NOM_ARCHIVO_INF_SUST_INV_FIRMA2; ?>" target="_blank" class="texto_arial_plomo_n_12"><img src="../webimages/iconos/archivoPDF.png" width="20" height="20" border="0" id="imagen2_sub"/></a>
        <? }else{ echo ""; }?>
     </td>
   </tr>
   <? } ?>
 </table>

<script type="text/javascript">
  $(document).ready(function(){
     $("#left").hide();
    // $("#img_nota").on( "hover", function() {
    //   //$("#p_nota").show();
    //    $('#left').toggle();
    // });

    $( "#img_nota" ).hover(
      function() {
        $('#left').toggle();
      }, function() {
        $('#left').toggle();
      }
    );

    
  });
</script>