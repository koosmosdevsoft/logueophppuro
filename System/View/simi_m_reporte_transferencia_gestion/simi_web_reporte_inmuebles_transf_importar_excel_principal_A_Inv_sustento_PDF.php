<?
require_once('../../Controller/C_padron_entidad.php');
require_once('../../Controller/C_simi_m_ue_inventario.php');
require_once('../../Controller/C_simi_m_personal.php');
require_once('../../../utils/funciones/funciones.php');
require_once('../../../utils/fpdf181/fpdf.php');
error_reporting(0);
$oSimi_UE_Bien_Inventario	=	new Simi_UE_Bien_Inventario;
$oPadron_Entidad			=	new Padron_Entidad;
$oSimi_UE_Personal			=	new Simi_UE_Personal;

$OPT = $_GET['OPT'];

if($OPT == 'RptInvPDF'){
	
	/*****************************************************************************************************************/
	/*****************************************************************************************************************/
	/*****************************************************************************************************************/
	
	$COD_ENTIDAD = $_GET['sEN'];
	//$IdPredio 	 = $_GET['sIdPredio'];
	
	$RS_Ent = $oPadron_Entidad->Ver_Datos_Padron_Entidad_x_COD_ENTIDAD($COD_ENTIDAD);
	$NOM_ENTIDAD			= utf8_encode(odbc_result($RS_Ent,"NOM_ENTIDAD"));
	$COD_INVENTARIO_ENT		= utf8_encode(odbc_result($RS_Ent,"COD_ENTIDAD"));

	$RS_Inv = $oSimi_UE_Bien_Inventario->UE_Listar_responsable_patrimonial_muebles($COD_ENTIDAD);
	$RESP_UE_APEMAT_PERS	= odbc_result($RS_Inv,"RESP_UE_APEMAT_PERS");
	$RESP_UE_APEPAT_PERS	= odbc_result($RS_Inv,"RESP_UE_APEPAT_PERS");
	$RESP_UE_NOMBRES_PERS	= odbc_result($RS_Inv,"RESP_UE_NOMBRES_PERS");
	$NRO_DOCUMENTO			= odbc_result($RS_Inv,"NRO_DOCUMENTO");


	$RS_Inv3 = $oSimi_UE_Bien_Inventario->UE_total_area_registral_muebles($COD_ENTIDAD);
	$AREA_REGISTRAL_TOTAL	= utf8_encode(odbc_result($RS_Inv3,"AREA_REGISTRAL_TOTAL"));
/*
	$RS_CPATRIM 			= $oSimi_UE_Personal->Ver_Datos_Simi_UE_Personal_x_CODIGO($COD_UE_PERS_CP);
	$NRO_DOCUMENTO			= htmlspecialchars_decode(odbc_result($RS_CPATRIM,"NRO_DOCUMENTO"));
	$RESP_UE_NOMBRES_PERS	= htmlspecialchars_decode(odbc_result($RS_CPATRIM,"UE_NOMBRES_PERS"));
	$RESP_UE_APEPAT_PERS	= htmlspecialchars_decode(odbc_result($RS_CPATRIM,"UE_APEPAT_PERS"));
	$RESP_UE_APEMAT_PERS	= htmlspecialchars_decode(odbc_result($RS_CPATRIM,"UE_APEMAT_PERS"));
	*/
	//$CONTINV++;		
		
	/****************************************************************************************************************/
	/****************************************************************************************************************/
	/****************************************************************************************************************/
	
	

	
	
	/*****************************************************************************************************************/
	/*****************************************************************************************************************/
	/*****************************************************************************************************************/
	
	
	class PDF extends FPDF{
		
		
		
		var $B;
var $I;
var $U;
var $HREF;

//Función para escribir en código HTML y que se detecten las etiquetas
function WriteHTML($html)
{
    // Intérprete de HTML
    $html = str_replace("\n",' ',$html);
    $a = preg_split('/<(.*)>/U',$html,-1,PREG_SPLIT_DELIM_CAPTURE);
    foreach($a as $i=>$e)
    {
        if($i%2==0)
        {
            // Text
            if($this->HREF)
                $this->PutLink($this->HREF,$e);
            else
                $this->Write(5,$e);
        }
        else
        {
            // Etiqueta
            if($e[0]=='/')
                $this->CloseTag(strtoupper(substr($e,1)));
            else
            {
                // Extraer atributos
                $a2 = explode(' ',$e);
                $tag = strtoupper(array_shift($a2));
                $attr = array();
                foreach($a2 as $v)
                {
                    if(preg_match('/([^=]*)=["\']?([^"\']*)/',$v,$a3))
                        $attr[strtoupper($a3[1])] = $a3[2];
                }
                $this->OpenTag($tag,$attr);
            }
        }
    }
}

function OpenTag($tag, $attr)
{
    // Etiqueta de apertura
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,true);
    if($tag=='A')
        $this->HREF = $attr['HREF'];
    if($tag=='BR')
        $this->Ln(5);
}

function CloseTag($tag)
{
    // Etiqueta de cierre
    if($tag=='B' || $tag=='I' || $tag=='U')
        $this->SetStyle($tag,false);
    if($tag=='A')
        $this->HREF = '';
}

function SetStyle($tag, $enable)
{
    // Modificar estilo y escoger la fuente correspondiente
    $this->$tag += ($enable ? 1 : -1);
    $style = '';
    foreach(array('B', 'I', 'U') as $s)
    {
        if($this->$s>0)
            $style .= $s;
    }
    $this->SetFont('',$style);
}

function PutLink($URL, $txt)
{
    // Escribir un hiper-enlace
    $this->SetTextColor(0,0,255);
    $this->SetStyle('U',true);
    $this->Write(5,$txt,$URL);
    $this->SetStyle('U',false);
    $this->SetTextColor(0);
}
		
		
		
		
		function Header(){
			
			// Logo	
			$this->Image('../../../webimages/iconos/sbn_11.png',10,5,18);//ancho,alto
	
			//$this->MultiCell(42, 5, 'Superintendencia Nacional de Bienes Estatales', 0, 1);
			$this->SetFont('Arial','',9);
			
			$this->Ln(0);
			$this->Cell(19);
			$this->Cell(1,1,'Superintendencia Nacional',0,0,'L');
			
			$this->Cell(481,1,utf8_decode('Página : ').$this->PageNo().'/{nb}',0,0,'C');
			$this->Ln(4);
			$this->Cell(19);
			$this->Cell(1,1,'de Bienes Estatales',0,0,'L');
			$this->Cell(487,1,utf8_decode('Reporte : '.date('d/m/Y')),0,0,'C');
			
			//$this->MultiCell(42, 5, 'Superintendencia Nacional de Bienes Estatales', 0, 1);
			
			$this->Cell(150);
			
			
			//$this->Ln(-1);
			$this->SetFont('Arial','B',10);		
			$this->Ln(-5);
			//$this->Cell(18);
			$this->Cell(0,1,utf8_decode('SUSTENTO DE BIENES'),0,0,'C');
			
			$this->Ln(6);
			$this->SetFont('Arial','B',10);
			$this->Cell(0,1,'INMUEBLES',0,0,'C');
			
	
			//Dibujamos una linea
			$this->Line(10, 20, 290, 20);
			// Salto de línea
			$this->Ln(5);
				
		}
		
		// Pie de página
		/*function Footer(){
			// Posición: a 1,5 cm del final
			$this->SetY(165);
				
			$this->Ln(1);	
			$this->SetFont('Arial','',9);
			$this->Cell(1,1,utf8_decode('El presente documento tiene la calidad de declaración jurada de acuerdo al artículo 42 de la Ley 27444, Ley del'),0,0,'L');
			
			$this->Ln(1);
			$this->Cell(1,9,'Procedimiento Administrativo General.',0,0,'L');
			
			$this->Ln(4);
			$this->SetFont('Arial','',8);
			$this->Cell(0,10,utf8_decode('Página : ').$this->PageNo().'/{nb}',0,0,'R');
			
			$this->Ln();
			$this->Ln(8);
			
			$this->Cell(5);
			$this->Cell(1,1,'________________________________________',0,0,'L');	
			
			$this->Ln(5);
			
			$this->Cell(10);
			$this->Cell(1,1,'  Firma y sello del Titular de la Entidad',0,0,'L');
			
		}
		*/
			
	}
	
	
	
		// Creación del objeto de la clase heredada
		//$oFPDF = new FPDF();
		//$oFPDF = new FPDF('P', 'mm', 'A4'); //-> oficio
		//$oFPDF = new FPDF('P', 'mm', 'A4'); //-> oficio
		//$oFPDF = new FPDF('P','mm',array(600,150));
	
	
		// Creación del objeto de la clase heredada
		$pdf = new PDF('L', 'mm', 'A4');
		//$pdf = new FPDF('P', 'mm', 'A4'); //-> oficio
		$pdf->AliasNbPages();
		
		//$pdf->set_paper(paper_size);
		$pdf->SetFont('Arial','',10);
		$pdf->AddPage();
		$pdf->Ln(4);

		//****************************************
		//****************************************
		
		$pdf->SetFillColor(205,205,205);//color de fondo tabla
		$pdf->SetTextColor(10);
		$pdf->SetDrawColor(153,153,153);
		$pdf->SetLineWidth(.3);
		$pdf->SetFont('Arial','B',9);
		
		$pdf->SetFillColor(205,205,205);
		$pdf->Cell(50,8,'ENTIDAD',1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);	
		$pdf->Cell(230,8,$NOM_ENTIDAD,1,0,'L',true);
				
		$pdf->Ln();
		$pdf->Ln(2);
		
		
		//--------------------		
		//DATOS DEL RESPONSABLE DE CONTROL PATRIMONIAL 
		//--------------------
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(280,7,utf8_decode('DATOS DEL RESPONSABLE DE CONTROL PATRIMONIAL'),1,0,'L',true);		
				
		$pdf->Ln();
		
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(40,6,utf8_decode('Nro de DNI'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(100,6,$NRO_DOCUMENTO,1,0,'L',true);	
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(40,6,utf8_decode('Nombres'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(100,6,$RESP_UE_NOMBRES_PERS,1,0,'L',true);
		
		$pdf->Ln();
		
		$pdf->SetFillColor(230,230,230);	
		$pdf->Cell(40,6,utf8_decode('Apellido Paterno'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);		
		$pdf->Cell(100,6,$RESP_UE_APEPAT_PERS,1,0,'L',true);
		
		$pdf->SetFillColor(230,230,230);
		$pdf->Cell(40,6,utf8_decode('Apellido Materno'),1,0,'L',true);		
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(100,6,$RESP_UE_APEMAT_PERS,1,0,'L',true);
		
						
		$pdf->Ln();
		$pdf->Ln(2);
				
		/*******************************************************/
		//DETALLE DEL BIEN
		/*******************************************************/
		
		
		//DETALLE DEL BIEN
		$pdf->SetFont('Arial','B',9);
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(280,7,utf8_decode('Detalle de Predios'),1,0,'L',true);		
		//$pdf->Cell(146,5,utf8_decode('DETALLE DEL INVENTARIO ANUAL'),1,0,'L',true);
		
		//$pdf->SetFillColor(205,205,205);
		//$pdf->Cell(39,5,utf8_decode('Total Bienes'),1,0,'C',true);		
	
		
		$pdf->Ln();
		$pdf->Ln(2);
		
		$pdf->SetFont('Arial','B',7);
      
		$w = array(7, 48, 48, 20, 20, 20, 24, 21, 16, 28, 28);
		          
		$pdf->Cell($w[0],10,utf8_decode('Item'),1,0,'L',true);
		$pdf->Cell($w[1],10,utf8_decode('Nombre del Local'),1,0,'C',true);
		$pdf->Cell($w[2],10,utf8_decode('Dirección'),1,0,'C',true);
		$pdf->Cell($w[3],10,utf8_decode('Distrito'),1,0,'C',true);
		$pdf->Cell($w[4],10,utf8_decode('Provincia'),1,0,'C',true);
		$pdf->Cell($w[5],10,utf8_decode('Departamento'),1,0,'C',true);
		$pdf->Cell($w[6],10,utf8_decode('Área Registral (m²)'),1,0,'C',true);
		$pdf->Cell($w[7],10,utf8_decode('Partida Registral'),1,0,'C',true);
		$pdf->Cell($w[8],10,utf8_decode('CUS'),1,0,'C',true);
		$pdf->Cell($w[9],10,utf8_decode('Situación de Propiedad'),1,0,'C',true);
		$pdf->Cell($w[10],10,utf8_decode('Estado de saneamiento'),1,0,'C',true);

		/*
		$x = $pdf->GetX();
        $y = $pdf->GetY();  
        $x += 13;

		$pdf->SetXY($x, $y);
		*/

		$pdf->Ln();
		// Restauración de colores y fuentes
		$pdf->SetFillColor(224,235,255);
		$pdf->SetTextColor(0);
		$pdf->SetFont('Arial','',7);
		// Datos
		$fill = false;
		$flag = 0;
		$contador = 0;

		$RS_Inv_2 = $oSimi_UE_Bien_Inventario->UE_Lista_Inmuebles_Transf_Inventarios_x_Entidad($COD_INVENTARIO_ENT);
		
		while (odbc_fetch_row($RS_Inv_2)){

			$H_DENOMINACION_PREDIO		= utf8_encode(odbc_result($RS_Inv_2,"DENOMINACION_PREDIO"));
			$H_DIRECCION				= utf8_encode(odbc_result($RS_Inv_2,"DIRECCION"));
			$H_DISTRITO					= utf8_encode(odbc_result($RS_Inv_2,"DISTRITO"));
			$H_PROVINCIA				= utf8_encode(odbc_result($RS_Inv_2,"PROVINCIA"));
			$H_DEPARTAMENTO				= utf8_encode(odbc_result($RS_Inv_2,"DEPARTAMENTO"));
			$H_AREA_REGISTRAL			= utf8_encode(odbc_result($RS_Inv_2,"AREA_REGISTRAL"));
			$H_PARTIDA_REGISTRAL		= utf8_encode(odbc_result($RS_Inv_2,"PARTIDA_REGISTRAL"));
			$H_CUS						= utf8_encode(odbc_result($RS_Inv_2,"CUS"));
			$H_DESC_TIP_PROPIEDAD		= utf8_encode(odbc_result($RS_Inv_2,"DESC_TIP_PROPIEDAD"));
			$H_TXT_ESANEAMIENTO			= utf8_encode(odbc_result($RS_Inv_2,"TXT_ESANEAMIENTO"));
			
			$contador++;
			if($contador == 29){
				$flag = 1;
			}else{
				$flag = 2;
			}
			
		
			if($contador == 29){
				$pdf->Cell(array_sum($w),0,'','T');
				$pdf->AddPage();
				$linea = 0;
			}else{
				$linea++;
			}
			
			if($flag == 1){			
				if($linea!= 0 && $linea%29==0 ){
					$pdf->Cell(array_sum($w),0,'','T');
					$pdf->AddPage();
				 }
			}else{				
				if($linea!= 0 && $linea%40==0 ){
					$pdf->Cell(array_sum($w),0,'','T');
					$pdf->AddPage();
				 }
			 }
				
			
			$pdf->Cell($w[0],4,$contador,'LR',0,'C');
			$pdf->Cell($w[1],4,$H_DENOMINACION_PREDIO,'LR',0,'L');
			$pdf->Cell($w[2],4,$H_DIRECCION,'LR',0,'L');
			$pdf->Cell($w[3],4,$H_DISTRITO,'LR',0,'L');
			$pdf->Cell($w[4],4,$H_PROVINCIA,'LR',0,'L');
			$pdf->Cell($w[5],4,$H_DEPARTAMENTO,'LR',0,'L');
			$pdf->Cell($w[6],4,$H_AREA_REGISTRAL,'LR',0,'C');
			$pdf->Cell($w[7],4,$H_PARTIDA_REGISTRAL,'LR',0,'C');
			$pdf->Cell($w[8],4,$H_CUS,'LR',0,'C');
			$pdf->Cell($w[9],4,$H_DESC_TIP_PROPIEDAD,'LR',0,'L');
			$pdf->Cell($w[10],4,$H_TXT_ESANEAMIENTO,'LR',0,'L');
			
			$pdf->Ln();
			$fill = !$fill;
		}
		
		// Línea de cierre
		$pdf->Cell(array_sum($w),0,'','T');
		
		$pdf->Ln();

		$pdf->SetFont('Arial','B',9);
		$pdf->SetFillColor(205,205,205);	
		$pdf->Cell(163,7,utf8_decode('TOTAL'),1,0,'C',true);	
		$pdf->SetFont('Arial','B',8);
		$pdf->SetFillColor(255,255,255);
		$pdf->Cell(24,7,$AREA_REGISTRAL_TOTAL,1,0,'C',true);


		//$pdf->SetY(165);
			
		$pdf->Ln(15);
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(1,1,utf8_decode('El presente documento tiene la calidad de declaración jurada de acuerdo al artículo 42 de la Ley 27444, Ley del'),0,0,'L');
		
		$pdf->Ln(1);
		$pdf->Cell(1,9,'Procedimiento Administrativo General.',0,0,'L');
		
		$pdf->Ln();
		$pdf->Ln(8);
		
		$pdf->Cell(5);
		$pdf->Cell(1,1,'________________________________________',0,0,'L');	
		
		$pdf->Ln(5);
		
		$pdf->Cell(10);
		$pdf->Cell(1,1,'  Firma y sello del Titular de la Entidad',0,0,'L');
			
		$pdf->Output();

}else{
	echo "No es la direccion Web Correcta";
}
?>


