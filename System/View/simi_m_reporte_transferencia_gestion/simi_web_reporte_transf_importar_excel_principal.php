<? session_start();
require_once('../../../utils/funciones/funciones.php');
if($_SESSION['th_SIMI_COD_ENTIDAD']=='') echo '<script language="javascript">parent.location.href="../sinabip.php";</script>';
$COD_ENTIDAD_X1			= $_SESSION['th_SIMI_COD_ENTIDAD'];
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet">


<script>

//window.onload = function() {
//visor=document.getElementById("reloj"); //localizar pantalla del reloj
//}
//empezar();

function empezar() {
	emp=new Date() //fecha inicial (en el momento de pulsar)
    elcrono=setInterval(tiempo,10); //función del temporizador.
    marcha=1 //indicamos que se ha puesto en marcha.
}

function parar() {
     clearInterval(elcrono); //parar el crono
     marcha=0; //indicar que está parado.
}

function tiempo() {
     actual=new Date(); //fecha a cada instante

        //tiempo del crono (cro) = fecha instante (actual) - fecha inicial (emp)
     cro=actual-emp; //milisegundos transcurridos.
     cr=new Date(); //pasamos el num. de milisegundos a objeto fecha.
     cr.setTime(cro);
        //obtener los distintos formatos de la fecha:
     cs=cr.getMilliseconds(); //milisegundos
     cs=cs/10; //paso a centsimas de segundo.
     cs=Math.round(cs); //redondear las centésimas
     sg=cr.getSeconds(); //segundos
     mn=cr.getMinutes(); //minutos
     ho=cr.getHours(); //horas
	// ho=actual.getHours(); //horas
        //poner siempre 2 cifras en los números
     if (cs<10) {cs="0"+cs;}
     if (sg<10) {sg="0"+sg;}
     if (mn<10) {mn="0"+mn;}
        //llevar resultado al visor.
     visor.innerHTML= "Tiempo => "+ho+" : "+mn+" : "+sg+" : "+cs;

	// visor.innerHTML= "Tiempo => "+"H_actual : "+actual+" ==> "+ " H_emp : "+emp + " ------> "+cro +">>>>" +ho+" : "+mn+" : "+sg+" : "+cs;
}



function prueba_aaa(){
	var f=new Date();
	cad=f.getHours()+":"+f.getMinutes()+":"+f.getSeconds();
	alert(cad);
	//window.status =cad;
	//setTimeout("prueba()",1000);
}

</script>

<script>

function Ver_Reporte_Inv(sAnio){

	var sCOD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();
	var url_page 		= 'View/simi_m_reporte_transferencia_gestion/simi_web_reporte_transf_importar_excel_principal_A_Inv_sustento_PDF.php?OPT=RptInvPDF&sEN='+sCOD_ENTIDAD+'&sAnio='+sAnio;
	window.open(url_page,'_blank');

}

function Descargar_Data_Inventario(sCOD_INVENTARIO_ENT, sCOD_INVENTARIO_ENT_DET){

	var sCOD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();
	var url_page = 'View/simi_web_importar_excel/simi_web_importar_excel_principal_A_Inv_predios_Excel.php?OPT=RptBienInvXLS&sEN='+sCOD_ENTIDAD+'&sIE='+sCOD_INVENTARIO_ENT+'&sIED='+sCOD_INVENTARIO_ENT_DET;

	window.open(url_page,'_blank');

}



function Mostrar_Form_Descargas_Locales(H_COD_INVENTARIO_ENT){

	$("#Div_Popup_Descargar_Local").dialog({
			//dialogClass: "no-close",
			title: 'Descargar Inventario de los Locales y/o Predios',
			width: 900,
			height: 800,
			resizable: "false",
			modal: true,
			open: function() {
				Mostrar_Inventario_x_Locales('1', H_COD_INVENTARIO_ENT);
			},
			close: function() {
				$(this).dialog("destroy");
				document.getElementById('Div_Popup_Descargar_Bienes').innerHTML = '';
			}
	});
}


function Mostrar_Inventario_x_Locales(page, H_COD_INVENTARIO_ENT){

	var TXH_SIMI_COD_ENTIDAD 		= $("#TXH_SIMI_COD_ENTIDAD").val();

	carga_loading('Div_Popup_Descargar_Bienes');
	$.post("View/simi_web_importar_excel/simi_web_importar_excel_principal_A_Inv_predios.php?page_x="+page,{
		TXH_SIMI_COD_ENTIDAD:TXH_SIMI_COD_ENTIDAD,
		H_COD_INVENTARIO_ENT:H_COD_INVENTARIO_ENT
	},function(data){
		$("#Div_Popup_Descargar_Bienes").html(data);
	});

}

/***************************************************************/
//-- PARA REGISTRAR INVENTARIO
/***************************************************************/


function Regresar_Inventario(){
	Actualiza_Menu_Direccionar_URL("View/simi_web_importar_excel/simi_web_importar_excel_principal.php","Cuerpo", "1");
}


function Form_Reg_Inventario(sCOD_INVENTARIO){

	var sCOD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();

	carga_loading('div_area_Inventario');

	$.post("View/simi_web_importar_excel/simi_web_importar_excel_principal_migracion.php?opt=Mostrar_Locales",{
		sCOD_ENTIDAD:sCOD_ENTIDAD,
		sCOD_INVENTARIO:sCOD_INVENTARIO
	},function(data){
		$("#div_area_Inventario").html(data);
	});

}


function paginar_Lista_Gral(numpage){

	var sCOD_ENTIDAD 		= $("#txh_migra_cod_entidad").val();
	var txt_busc_local		= $("#txt_busc_local").val();
	var sCOD_INVENTARIO 	= $("#txh_migra_cod_inventario").val();

	carga_loading('div_lista_locales_inventario');

	$.post("View/simi_web_importar_excel/simi_web_importar_excel_principal_migracion_local.php?page="+numpage,{
		sCOD_ENTIDAD:sCOD_ENTIDAD,
		txt_busc_local:txt_busc_local,
		sCOD_INVENTARIO:sCOD_INVENTARIO
	},function(data){
		$("#div_lista_locales_inventario").html(data);
	});
}

function Quitar_Filtro_Busc_Local(){
	$("#txt_busc_local").val('');
	paginar_Lista_Gral('1');
}



function fShowFormulario_Agregar_Excel(sCOD_LOCAL){

	var cod_tr 								= 'TR_'+sCOD_LOCAL;
	var txh_migra_cod_entidad 				= $("#txh_migra_cod_entidad").val();
	var txh_migra_cod_inventario			= $("#txh_migra_cod_inventario").val();
	var txh_migra_anio						= $("#txh_migra_anio").val();
	var txh_migra_cod_resp_cont_patrim_excel = $("#txh_migra_cod_resp_cont_patrim_excel").val();


	ajaxpage("View/simi_web_importar_excel/simi_web_importar_excel_principal_migracion_form.php?xOPT=VerLocal&sCodRespMuebles="+txh_migra_cod_resp_cont_patrim_excel+"&sCODENTIDAD="+txh_migra_cod_entidad+"&sCOD_LOCAL="+sCOD_LOCAL+'&sInv='+txh_migra_cod_inventario+'&sInvEntidad='+txh_migra_cod_entidad+'&sAnio='+txh_migra_anio,"div_formulario") ;

	var ulA = document.getElementById("tbl_Resultado_Lista_Locales");
	var liNodesA = ulA.getElementsByTagName("TR");
	var sTot =  liNodesA.length;

	var color = "";
	var obj = "";

	for( var s = 1; s <= sTot; s++ ){
		obj = liNodesA[s].id;

		if(cod_tr == obj){
			color = "#e2f2ff";
		}else{
			color = "";
		}

		$("#"+obj+"").css("background-color",color);

	}
}


function agregar_archivo_Excel(){

	var txt_val_adjuntar_archivo 	= $("#txt_val_adjuntar_archivo").val();
	var txh_COD_IMPORT_EXCEL_FILE 	= $("#txh_COD_IMPORT_EXCEL_FILE").val();

	var txh_cod_local_file 			= $("#txh_cod_local_file").val();
	var txh_migra_cod_inventario 	= $("#txh_migra_cod_inventario").val();


	if(txh_COD_IMPORT_EXCEL_FILE == '' && txt_val_adjuntar_archivo != '1'){
		alert('Aún no adjuntó archivo TXT');

	}else{

		carga_loading('div_lista_excel_Cargado');

		$.post("Model/M_simi_m_importar_excel_muebles.php?operacion=Agregar_Archivo_a_Lista",{
			txh_COD_IMPORT_EXCEL_FILE:txh_COD_IMPORT_EXCEL_FILE,
			txh_cod_local_file:txh_cod_local_file,
			txh_migra_cod_inventario:txh_migra_cod_inventario
		},function(data){

			if(data==1){
				fShowFormulario_Agregar_Excel(txh_cod_local_file);
			}else{
				alert(data);
			}
		});
	}

}


function Boton_Cargar_Archivo(sCOD_IMPORT_EXCEL_FILE_XLS, sID_PREDIO_SBN_LOCAL){

	if(confirm('Confirma Cargar la Información del archivo TXT?')){

		$("#Div_Popup").dialog({
				dialogClass: "no-close",
				title: 'Cargar Información del Archivo TXT',
				width: 1000,
				height: 700,
				resizable: "false",
				modal: true,
				open: function() {
					Ejecuta_Cargar_Archivo(sCOD_IMPORT_EXCEL_FILE_XLS, sID_PREDIO_SBN_LOCAL);
				},
				close: function() {
					$(this).dialog("destroy");
					document.getElementById('div_ejecuta_proceso').innerHTML = '';
					document.getElementById('reloj').innerHTML = '';
				}
		});

	}
}

function Boton_Cerrar(){
	$("#Div_Popup").dialog( "close" );
	Listar_Excel_Cargados();
}

function Eliminar_Archivo(sCOD_IMPORT_EXCEL_FILE){

	var TXH_SIMI_COD_USUARIO 	= $("#TXH_SIMI_COD_USUARIO").val();
	var sCOD_LOCAL 				= $("#txh_cod_local_file").val();

	if(confirm('Confirma eliminar este registro')){

		carga_loading('div_lista_excel_Cargado');
		$.post("Model/M_simi_m_importar_excel_muebles.php?operacion=Eliminar_Cuadro_Excel",{
			sCOD_IMPORT_EXCEL_FILE:sCOD_IMPORT_EXCEL_FILE,
			TXH_SIMI_COD_USUARIO:TXH_SIMI_COD_USUARIO
		},function(data){

			if(data == 1){
				paginar_Lista_Gral('1');
				fShowFormulario_Agregar_Excel(sCOD_LOCAL);
			}else{
				alert(data);
			}

		});
	}

}



//----------------------------------------------------------------------------------
//************************** EJECUTA BOTON CARGAR
//----------------------------------------------------------------------------------

function Ejecuta_Cargar_Archivo(sCOD_IMPORT_EXCEL_FILE_XLS, sID_PREDIO_SBN_LOCAL){

	var TXH_SIMI_COD_ENTIDAD 		= $("#TXH_SIMI_COD_ENTIDAD").val();
	var TXH_SIMI_COD_USUARIO 		= $("#TXH_SIMI_COD_USUARIO").val();

	mostrarCargaContenido('div_ejecuta_proceso','SE ESTA CARGANDO LA INFORMACION. <br>EL PROCESO DE CARGA DE INFORMACION PUEDE TARDAR VARIOS MINUTOS ');

	$("#reloj").addClass("css_reloj");
	visor=document.getElementById("reloj"); //localizar pantalla del reloj
	empezar();


	$.post("Model/M_simi_m_importar_excel_muebles.php?operacion=Ejecuta_Cargar_TXT",{
			sCOD_IMPORT_EXCEL_FILE_XLS:sCOD_IMPORT_EXCEL_FILE_XLS,
			TXH_SIMI_COD_ENTIDAD:TXH_SIMI_COD_ENTIDAD,
			TXH_SIMI_COD_USUARIO:TXH_SIMI_COD_USUARIO,
			sID_PREDIO_SBN_LOCAL:sID_PREDIO_SBN_LOCAL
		},function(data){
			parar();
			$("#div_ejecuta_proceso").html(data);
			//$("#reloj").removeClass("css_reloj");
		});

}

function Listar_Excel_Cargados(){

	var xxx_cod_local_file 			= $("#txh_cod_local_file").val();
	var xxx_COD_IMPORT_EXCEL_FILE 	= $("#txh_COD_IMPORT_EXCEL_FILE").val();

	carga_loading('div_lista_excel_Cargado');

	$.post("View/simi_web_importar_excel/simi_web_importar_excel_principal_form_list_inv.php?opt=listar_fILE",{
		xxx_cod_local_file:xxx_cod_local_file,
		xxx_COD_IMPORT_EXCEL_FILE:xxx_COD_IMPORT_EXCEL_FILE
	},function(data){
		$("#div_lista_excel_Cargado").html(data);
	})
}


//----------------------------------------------------------------------------------
//************************** EJECUTA BOTON VALIDAR
//----------------------------------------------------------------------------------

function Boton_Validar_Carga(sCOD_IMPORT_EXCEL_FILE_XLS, sID_PREDIO_SBN_Local){

	if(confirm('Confirma validar el proceso de migración de este local?')){
		$("#Div_Popup").dialog({
				dialogClass: "no-close",
				title: 'Validando Información',
				width: 1000,
				height: 700,
				resizable: "false",
				modal: true,
				open: function() { Ejecuta_Validar_Carga(sCOD_IMPORT_EXCEL_FILE_XLS, sID_PREDIO_SBN_Local);
				},
				close: function() {
					$(this).dialog("destroy");
					document.getElementById('div_ejecuta_proceso').innerHTML = '';
					document.getElementById('reloj').innerHTML = '';
					//document.getElementById('tbl_Eliminar').style.display = "block";
					//$("#tbl_Eliminar").hide();
				}
		}) 
	}
}


function Ejecuta_Validar_Carga(sCOD_IMPORT_EXCEL_FILE_XLS, sID_PREDIO_SBN_Local){

	var TXH_COD_IMPORT_EXCEL_RESP 	= $("#TXH_COD_IMPORT_EXCEL_RESP").val();
	var TXH_SIMI_COD_ENTIDAD 		= $("#TXH_SIMI_COD_ENTIDAD").val();
	var txh_migra_anio 		= $("#txh_migra_anio").val();


	//carga_loading('div_ejecuta_proceso');
	mostrarCargaContenido('div_ejecuta_proceso','SE ESTA PROCESANDO LA INFORMACIÓN. <br>EL PROCESO DE INFORMACIÓN PUEDE TARDAR VARIOS MINUTOS ');

	$("#reloj").addClass("css_reloj");
	visor=document.getElementById("reloj"); //localizar pantalla del reloj
	empezar();


	$.post("Model/M_simi_m_importar_excel_muebles.php?operacion=Ejecuta_Procesar_Carga",{
			sCOD_IMPORT_EXCEL_FILE_XLS:sCOD_IMPORT_EXCEL_FILE_XLS,
			TXH_SIMI_COD_ENTIDAD:TXH_SIMI_COD_ENTIDAD,
			txh_migra_anio:txh_migra_anio
		},function(response){
			parar();
			var valor 					= response.split("[*]")[0];
			var RESULTADO 				= response.split("[*]")[1];
			var COD_IMPORT_EXCEL_FILE 	= response.split("[*]")[2];
			var TOTAL_REG 				= response.split("[*]")[3];
			var TOTAL_BIEN_NO_CATAL		= response.split("[*]")[4];
			var ERRORLINE 				= response.split("[*]")[5];
			var ERRORMESSAGE			= response.split("[*]")[6];

			if(valor == '1'){

				if(RESULTADO == 'OK'){
					
					//$("#div_ejecuta_proceso").html("Se Ejecuto Correctamente los registros del excel");
					Listar_Proceso_CORRECTO(COD_IMPORT_EXCEL_FILE, TOTAL_BIEN_NO_CATAL);
					//$("#tbl_Eliminar").hide();
					//alert("hola")
					//$("#tbl_Eliminar").style.display="none";
				}else if(RESULTADO == 'ERROR'){
					Listar_Proceso_ERROR(COD_IMPORT_EXCEL_FILE, TOTAL_REG, TOTAL_BIEN_NO_CATAL);

				}else if(RESULTADO == 'DUPLICADOS'){
					Listar_Proceso_DUPLICADOS(COD_IMPORT_EXCEL_FILE, TOTAL_REG, sID_PREDIO_SBN_Local);


				}else if(RESULTADO == 'FALLA'){
					Listar_Proceso_FALLA(COD_IMPORT_EXCEL_FILE, ERRORLINE, ERRORMESSAGE);
				}

			}else{
				$("#div_ejecuta_proceso").html(response);
			}
		});
}



function Listar_Proceso_CORRECTO(sCOD_IMPORT_EXCEL_FILE_XLS, sTOTAL_BIEN_NO_CATAL){

	mostrarCargaContenido('div_ejecuta_proceso','MOSTRANDO RESULTADO');
	$.post("View/simi_web_importar_excel/simi_web_importar_excel_principal_form_list_correcto.php?opt=Mostrar_Correcto_Excel",{
		sCOD_IMPORT_EXCEL_FILE_XLS:sCOD_IMPORT_EXCEL_FILE_XLS,
		sTOTAL_BIEN_NO_CATAL:sTOTAL_BIEN_NO_CATAL
	},function(DATA){
		$("#div_ejecuta_proceso").html(DATA);
	});

}

function Listar_Proceso_ERROR(sCOD_IMPORT_EXCEL_FILE_XLS, sTOTAL_REG){

	mostrarCargaContenido('div_ejecuta_proceso','MOSTRANDO LISTA DE ERRORES');
	$.post("View/simi_web_importar_excel/simi_web_importar_excel_principal_form_list_error.php?opt=Mostrar_Error_Excel",{
		sCOD_IMPORT_EXCEL_FILE_XLS:sCOD_IMPORT_EXCEL_FILE_XLS,
		sTOTAL_REG:sTOTAL_REG
	},function(DATA){
		$("#div_ejecuta_proceso").html(DATA);
	});

}


function Listar_Proceso_DUPLICADOS(sCOD_IMPORT_EXCEL_FILE_XLS, sTOTAL_REG, sID_PREDIO_SBN_Local){

	mostrarCargaContenido('div_ejecuta_proceso','MOSTRANDO LISTA DE DUPLICADOS');
	$.post("View/simi_web_importar_excel/simi_web_importar_excel_principal_form_list_duplicados.php?opt=Mostrar_Duplicados_Excel",{
		sCOD_IMPORT_EXCEL_FILE_XLS:sCOD_IMPORT_EXCEL_FILE_XLS,
		sTOTAL_REG:sTOTAL_REG,
		sID_PREDIO_SBN_Local:sID_PREDIO_SBN_Local
	},function(DATA){
		$("#div_ejecuta_proceso").html(DATA);
	});

}

function Listar_Proceso_FALLA(sCOD_IMPORT_EXCEL_FILE_XLS, ERRORLINE, ERRORMESSAGE){

	mostrarCargaContenido('div_ejecuta_proceso','MOSTRANDO ERRORES');
	$.post("View/simi_web_importar_excel/simi_web_importar_excel_principal_form_list_falla.php?opt=Mostrar_Falla_Excel",{
		sCOD_IMPORT_EXCEL_FILE_XLS:sCOD_IMPORT_EXCEL_FILE_XLS,
		ERRORLINE:ERRORLINE,
		ERRORMESSAGE:ERRORMESSAGE
	},function(DATA){
		$("#div_ejecuta_proceso").html(DATA);
	});

}

//----------------------------------------------------------------------------------
//************************** EJECUTA BOTON FINALIZAR
//----------------------------------------------------------------------------------

function Finalizar_Inventario(sCOD_IMPORT_EXCEL_FILE_XLS, sID_PREDIO_SBN_Local){

	if(confirm('Confirma finalizar el proceso de migracion del inventario de este local?')){

		$("#Div_Popup").dialog({
				dialogClass: "no-close",
				title: 'Finalizando Inventario',
				width: 1000,
				height: 500,
				resizable: "false",
				modal: true,
				open: function() { Ejecuta_Finalizar_Inventario(sCOD_IMPORT_EXCEL_FILE_XLS, sID_PREDIO_SBN_Local);
				},
				close: function() {
					$(this).dialog("destroy");
					document.getElementById('div_ejecuta_proceso').innerHTML = '';
					document.getElementById('reloj').innerHTML = '';
				}
		});
	}
}


function Ejecuta_Finalizar_Inventario(sCOD_IMPORT_EXCEL_FILE_XLS, sID_PREDIO_SBN_Local){

	var TXH_COD_IMPORT_EXCEL_RESP 	= $("#TXH_COD_IMPORT_EXCEL_RESP").val();
	var TXH_SIMI_COD_ENTIDAD 		= $("#TXH_SIMI_COD_ENTIDAD").val();

	mostrarCargaContenido('div_ejecuta_proceso','SE ESTA FINALIZANDO EL PROCESO DE LA MIGRACION DEL INVENTARIO. <br>EL PROCESO DE MIGRACIÓN PUEDE TARDAR VARIOS MINUTOS ');

	$("#reloj").addClass("css_reloj");
	visor=document.getElementById("reloj"); //localizar pantalla del reloj
	empezar();


	$.post("Model/M_simi_m_importar_excel_muebles.php?operacion=Ejecuta_Finalizar_Inventario",{
		sCOD_IMPORT_EXCEL_FILE_XLS:sCOD_IMPORT_EXCEL_FILE_XLS,
		sID_PREDIO_SBN_Local:sID_PREDIO_SBN_Local,
		TXH_SIMI_COD_ENTIDAD:TXH_SIMI_COD_ENTIDAD
	},function(response){

		var valor 			= response.split("[*]")[0];
		var CONDICION 		= response.split("[*]")[1];
		var TOT_EXCEL 		= response.split("[*]")[2];
		var TOT_MIGRADO 	= response.split("[*]")[3];

		if(valor == '1' && CONDICION == 'x100'){

			parar();
			Mostrar_Mensaje_Inventario_Finalizado();

		}else if(valor == '0' && CONDICION == 'x0'){

			$("#div_ejecuta_proceso").html('<br>Error. El Total de Bienes del Excel '+TOT_EXCEL+', no coencide con total de bienes Migrados '+TOT_MIGRADO+'.<br>');

		}else{
			$("#div_ejecuta_proceso").html(response);
		}


	});
}


function Mostrar_Mensaje_Inventario_Finalizado(){

	mostrarCargaContenido('div_ejecuta_proceso','MOSTRANDO FORMULARIO');
	$.post("View/simi_web_importar_excel/simi_web_importar_excel_principal_form_list_fin.php",{
	},function(DATA){
		$("#div_ejecuta_proceso").html(DATA);
	});

}

//----------------------------------------------------------------------------------
//************************** EJECUTA BOTON ENVIAR
//----------------------------------------------------------------------------------

function Enviar_FILE_SBN(sCOD_IMPORT_EXCEL_FILE){

	var TXH_SIMI_COD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();
	var TXH_SIMI_COD_USUARIO 	= $("#TXH_SIMI_COD_USUARIO").val();
	var txh_cod_local_file 		= $("#txh_cod_local_file").val();

	var txh_page = $("#txh_page").val();

	if(confirm('Confirma enviar el proceso de inventario a la SBN?')){

		carga_loading('div_lista_excel_Cargado');

		$.post("Model/M_simi_m_importar_excel_muebles.php?operacion=Enviar_FILE_EXCEL_a_SBN",{
			sCOD_IMPORT_EXCEL_FILE:sCOD_IMPORT_EXCEL_FILE,
			TXH_SIMI_COD_USUARIO:TXH_SIMI_COD_USUARIO,
			TXH_SIMI_COD_ENTIDAD:TXH_SIMI_COD_ENTIDAD
		},function(data){

			if(data == 1){
				paginar_Lista_Gral(txh_page);
				$("#div_formulario").html('');
			}else{
				alert(data);
			}
		});
	}
}


//----------------------------------------------------------------------------------
//************************** EJECUTA BOTON REGISTRAR SIN BIENES
//----------------------------------------------------------------------------------

function Registrar_Sin_Bienes(sID_PREDIO_SBN_LOCAL){

	var txh_page = $("#txh_page").val();

	var TXH_SIMI_COD_ENTIDAD 		= $("#TXH_SIMI_COD_ENTIDAD").val();
	var TXH_SIMI_COD_USUARIO 		= $("#TXH_SIMI_COD_USUARIO").val();
	var TXH_COD_IMPORT_EXCEL_RESP 	= $("#txh_migra_cod_resp_cont_patrim_excel").val();
	var txh_migra_cod_inventario	= $("#txh_migra_cod_inventario").val();

	if(confirm('Confirma Registrar este Local y/o Predio sin bienes muebles?')){

		$.post("Model/M_simi_m_importar_excel_muebles.php?operacion=Reg_Sin_Bienes",{
			TXH_SIMI_COD_ENTIDAD:TXH_SIMI_COD_ENTIDAD,
			TXH_SIMI_COD_USUARIO:TXH_SIMI_COD_USUARIO,
			sID_PREDIO_SBN_LOCAL:sID_PREDIO_SBN_LOCAL,
			TXH_COD_IMPORT_EXCEL_RESP:TXH_COD_IMPORT_EXCEL_RESP,
			txh_migra_cod_inventario:txh_migra_cod_inventario
		},function(data){
			if(data=='1'){
				paginar_Lista_Gral(txh_page);
				$('#div_formulario').html('');
			}else{
				alert(data);
			}
		});
	}
}


//----------------------------------------------------------------------------------
//************************** EJECUTA BOTON REGISTRAR SIN BIENES
//----------------------------------------------------------------------------------


function Mostrar_ventana_Finalizar_Inventario_Final(xCOD_ENTIDAD, sCOD_INVENTARIO){

	var txh_tot_predios 		= $("#txh_tot_predios").val();
	var txh_tot_enviado 		= $("#txh_tot_enviado").val();
	var txh_tot_pendiente 		= $("#txh_tot_pendiente").val();
	var sAnio 					= $("#txh_migra_anio").val();

	var txh_migra_cod_inv_ent 	= $("#txh_migra_cod_inv_ent").val();
	var txh_migra_finalizo		= $("#txh_migra_finalizo").val();

	if(txh_tot_pendiente > 0){
		alert('Debe terminar de migrar todo los locales y/o predios.');

	}else if(txh_tot_predios == txh_tot_enviado){

		if(txh_migra_cod_inv_ent == ''){

			if(confirm('Confirma Finalizar el Inventario '+sAnio)){

				$("#Div_Popup_Ventana_Finalizar_Inv").dialog({
					//dialogClass: "no-close",
					title: 'Formulario para Finalizar Inventario',
					width: 900,
					height: 500,
					resizable: "false",
					modal: true,
					open: function() {
						Finalizar_Inventario_Final_x_Entidad(xCOD_ENTIDAD, sCOD_INVENTARIO);
					},
					close: function() {
						$(this).dialog("destroy");
						document.getElementById('Div_Popup_Ventana_Finalizar_Inv_Contenido').innerHTML = '';
					}
				});
			}


		}else if(txh_migra_cod_inv_ent != '' && txh_migra_finalizo == '' ){

			$("#Div_Popup_Ventana_Finalizar_Inv").dialog({
				//dialogClass: "no-close",
				title: 'Formulario para Finalizar Inventario',
				width: 900,
				height: 500,
				resizable: "false",
				modal: true,
				open: function() {
					Mostrar_Reporte_Inventario_Final_Fase_01(xCOD_ENTIDAD, sCOD_INVENTARIO, sAnio);
				},
				close: function() {
					$(this).dialog("destroy");
					document.getElementById('Div_Popup_Ventana_Finalizar_Inv_Contenido').innerHTML = '';
				}
			});

		}
	}

}




function Finalizar_Inventario_Final_x_Entidad(xCOD_ENTIDAD, sCOD_INVENTARIO){

	var txh_migra_cod_inventario	= $("#txh_migra_cod_inventario").val();
	var sAnio 						= $("#txh_migra_anio").val();
	var txh_migra_cod_inv_ent 		= $("#txh_migra_cod_inv_ent").val();
	var TXH_SIMI_COD_USUARIO 		= $("#TXH_SIMI_COD_USUARIO").val();

	carga_loading('Div_Popup_Ventana_Finalizar_Inv_Contenido');
	$.post("Model/M_simi_m_ue_inventario.php?operacion=Finalizar_Todo_Inventario_General_x_Entidad",{
		xCOD_ENTIDAD:xCOD_ENTIDAD,
		sCOD_INVENTARIO:sCOD_INVENTARIO,
		txh_migra_cod_inv_ent:txh_migra_cod_inv_ent,
		sAnio:sAnio,
		TXH_SIMI_COD_USUARIO:TXH_SIMI_COD_USUARIO
	},function(data){
		if(data == 1){
			//Regresar_Inventario();
			Mostrar_Reporte_Inventario_Final_Fase_01(xCOD_ENTIDAD, sCOD_INVENTARIO, sAnio);
			Form_Reg_Inventario(txh_migra_cod_inventario);
		}else{
			alert(data);
		}
	});

}



function Mostrar_Reporte_Inventario_Final_Fase_01(xCOD_ENTIDAD, sCOD_INVENTARIO, sAnio){

	carga_loading('Div_Popup_Ventana_Finalizar_Inv_Contenido');
	$.post("View/simi_web_importar_excel/simi_web_importar_excel_principal_migracion_final.php?xOPT=FinInv",{
		xCOD_ENTIDAD:xCOD_ENTIDAD,
		sCOD_INVENTARIO:sCOD_INVENTARIO,
		sAnio:sAnio
	},function(data){
		$("#Div_Popup_Ventana_Finalizar_Inv_Contenido").html(data);
	});
}

//----------------------------------------------------------------------------------
//************************** DESCARGAR RELACION ACTUAL
//----------------------------------------------------------------------------------


function Mostrar_Ventana_Relaccion_Actual_Bienes_Descargar(){

	$("#Div_Popup_Descargar_Local").dialog({
			//dialogClass: "no-close",
			title: 'Descargar Inventario de los Locales y/o Predios de los Bienes Actuales',
			width: 900,
			height: 800,
			resizable: "false",
			modal: true,
			open: function() {
				Mostrar_Locales_Para_Descarga_Bienes('1');
			},
			close: function() {
				$(this).dialog("destroy");
				document.getElementById('Div_Popup_Descargar_Bienes').innerHTML = '';
			}
	});
}


function Mostrar_Locales_Para_Descarga_Bienes(page){

	var TXH_SIMI_COD_ENTIDAD 		= $("#TXH_SIMI_COD_ENTIDAD").val();

	carga_loading('Div_Popup_Descargar_Bienes');
	$.post("View/simi_web_importar_excel/simi_web_importar_excel_principal_B_Bien_Local.php?page_x="+page,{
		TXH_SIMI_COD_ENTIDAD:TXH_SIMI_COD_ENTIDAD
	},function(data){
		$("#Div_Popup_Descargar_Bienes").html(data);
	});
}


function Exportar_Excel_Relacion_Bien(ID_PREDIO_SBN){
	var COD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();
	var url_page = 'View/simi_web_importar_excel/simi_web_importar_excel_principal_A_Inv_XLS.php?OPT=RptBienXLS&sE='+COD_ENTIDAD+'&sL='+ID_PREDIO_SBN;
	window.open(url_page,'_blank');

}


function Finalizar_Inv_FINAL(xCOD_ENTIDAD, wCOD_INVENTARIO_ENT){

	var txt_val_file_informe 	= $("#txt_val_file_informe").val();
	var txt_val_file_acta 	= $("#txt_val_file_acta").val();

	if(txt_val_file_informe == 1 && txt_val_file_acta == 1){

		carga_loading('div_boton_finalizar');
		$.post("Model/M_simi_m_ue_inventario.php?operacion=Finalizar_Inventario_Todo_Inf_Final_Acta_Conciliacion",{
			xCOD_ENTIDAD:xCOD_ENTIDAD,
			wCOD_INVENTARIO_ENT:wCOD_INVENTARIO_ENT
		},function(data){
			if(data == 1){
				Regresar_Inventario();
				$("#Div_Popup_Ventana_Finalizar_Inv").dialog("destroy");
				document.getElementById('Div_Popup_Ventana_Finalizar_Inv_Contenido').innerHTML = '';
			}else{
				alert(data);
			}
		});

	}else if(txt_val_file_informe == "" && txt_val_file_acta == 1){

		alert("Debe adjuntar el Informe Final del Inventario en Formato PDF");

	}else if(txt_val_file_informe == 1 && txt_val_file_acta == ""){

		alert("Debe adjuntar el Acta de Conciliación del Inventario en Formato PDF");

	}else{
		alert("No ha Adjuntado ningun archivo.");
	}



}


function Limpiar_Bienes(xCOD_ENTIDAD, ID_PREDIO_SBN, sCOD_INVENTARIO){

	var txh_anio 				= $("#txh_migra_anio").val();
	var TXH_SIMI_COD_USUARIO 	= $("#TXH_SIMI_COD_USUARIO").val();

	carga_loading('div_lista_locales_inventario');
	$.post("Model/M_simi_m_ue_inventario.php?operacion=Limpiar_Bienes",{
		xCOD_ENTIDAD:xCOD_ENTIDAD,
		ID_PREDIO_SBN:ID_PREDIO_SBN,
		sCOD_INVENTARIO:sCOD_INVENTARIO,
		txh_anio:txh_anio
	},function(response){

		var valor 					= response.split("[*]")[0];
		var sCOD_IMPORT_EXCEL_FILE 	= response.split("[*]")[1];

		if(valor == 1){

			$.post("Model/M_simi_m_importar_excel_muebles.php?operacion=Eliminar_Cuadro_Excel",{
				sCOD_IMPORT_EXCEL_FILE:sCOD_IMPORT_EXCEL_FILE,
				TXH_SIMI_COD_USUARIO:TXH_SIMI_COD_USUARIO
			},function(data){

				if(data == 1){
					paginar_Lista_Gral('1');
				}else{
					alert(data);
				}

			});

		}else{
			alert(response);
		}
	});

}


function Regresar_a_Pendiente(xCOD_ENTIDAD, sCOD_INVENTARIO, wCOD_INVENTARIO_ENT){

	if(confirm('Confirma regresar a estado Pendiente este inventario?')){

		$("#Div_Popup_Ventana_Finalizar_Inv").dialog("destroy");
		document.getElementById('Div_Popup_Ventana_Finalizar_Inv_Contenido').innerHTML = '';

		carga_loading('div_lista_locales_inventario');

		$.post("Model/M_simi_m_ue_inventario.php?operacion=Regresar_a_Estado_Pendiente",{
			xCOD_ENTIDAD:xCOD_ENTIDAD,
			sCOD_INVENTARIO:sCOD_INVENTARIO,
			wCOD_INVENTARIO_ENT:wCOD_INVENTARIO_ENT
		},function(valor){
			if(valor == 1){
				Regresar_Inventario();
			}else{
				alert(valor);
			}
		});

	}

}
</script>

<div id="div_area_Inventario"><? include_once('simi_web_reporte_transf_principal_A_Inv.php')?></div>
<div id="Div_Popup_Descargar_Local"><div id="Div_Popup_Descargar_Bienes"></div></div>
<div id="Div_Popup_Ventana_Finalizar_Inv"><div id="Div_Popup_Ventana_Finalizar_Inv_Contenido"></div></div>

<div id="Div_Popup">
	<div id="div_ejecuta_proceso"></div>
    <div id="reloj"></div>
</div>
