

<?php session_start();

error_reporting(0);


require_once('../../../utils/funciones/funciones.php'); 
require_once('../../../utils/fpdf181/fpdf.php');
require_once("../../Controller/BaseBD_PDO.php");
require_once('../../Model/BaseBD_PDOModel.php');
require_once("../../Controller/C_Interconexion_SQL.php");

require_once('../../Controller/BienPatrimonialController.php');
require_once('../../Model/BienPatrimonialModel.php');


require_once('../../Controller/PrediosController.php');
require_once('../../Model/PrediosModel.php');

require_once('../../Controller/UbigeoController.php');
require_once('../../Model/UbigeoModel.php');
require_once('../../../utils/funciones/clases.php');

$oPrediosController	=	new PrediosController;

$aa = ''; 
if(isset($_GET['operacion']))
{
  $aa = $_GET['operacion'];
}

if( $aa == 'Filtrar_Lista_Predios' ){ 
  $oPrediosController->Buscar_Predios_x_Parametros();
  
}else if( $aa == 'Mostrar_Form_Datos_Predio' ){
  $oPrediosController->Mostrar_Datos_Predio_Local();
  
}else if( $aa == 'Guardar_Datos_Predio' ){
   $oPrediosController->Guardar_Datos_Local_Predio();
   
}else if( $aa == 'Guardar_valorizacion' ){
   $oPrediosController->Guardar_Datos_Valorizacion();
   
}else if( $aa == 'Listar_Cab_Valorizacion' ){
   $oPrediosController->Ver_Datos_Valorizacion_x_Codigo();

}else if( $aa == 'Listar_Det_Valorizacion' ){
   $oPrediosController->Listar_Valorizaciones();

}else if( $aa == 'Ver_Datos_Valorizacion_x_Codigo' ){
   $oPrediosController->Ver_Datos_Valorizacion_x_Codigo();
   
}else if( $aa == 'Mostrar_Form_tab1_Datos_Registrales' ){
   $oPrediosController->Mostrar_Form_tab1_Datos_Registrales();
   
}else if( $aa == 'Mostrar_Form_tab2_Datos_Tecnicos' ){
   $oPrediosController->Mostrar_Form_tab2_Datos_Tecnicos();

}else if( $aa == 'Guardar_Datos_Unid_Inmobiliaria' ){
   $oPrediosController->Guardar_Datos_Unid_Inmobiliaria();
   
}else if( $aa == 'Lista_Registros_Unid_Inmobiliaria' ){
   $oPrediosController->Lista_Registros_Unid_Inmobiliaria();

}else if( $aa == 'Ver_Datos_Unidad_Inmobiliaria_x_Codigo' ){
   $oPrediosController->Ver_Datos_Unidad_Inmobiliaria_x_Codigo();

}else if( $aa == 'Mostrar_Form_Documento' ){
   $oPrediosController->Mostrar_Form_Documento();

}else if( $aa == 'Registrar_Predio_Documentos' ){
   $oPrediosController->Registrar_Predio_Documentos();

}else if( $aa == 'Listar_Registro_Documentos_registrados' ){
   $oPrediosController->Listar_Registro_Documentos_registrados();

}else if( $aa == 'Eliminar_Documento_Tecnico' ){
   $oPrediosController->Eliminar_Documento_Tecnico();
   
}else if( $aa == 'Eliminar_Valorizacion_x_Codigo' ){
   $oPrediosController->Eliminar_Valorizacion_x_Codigo();
   
}else if( $aa == 'Dar_Baja_predio' ){
   $oPrediosController->Dar_Baja_predio();

}else if( $aa == 'Eliminar_Reg_Inmobiliaria' ){
   $oPrediosController->Eliminar_Reg_Inmobiliaria_x_Codigo();

}else if( $aa == 'Eliminacion_Baja_Predios' ){
   $oPrediosController->Eliminacion_Baja_Predios();
// INICIO JMCR
}else if( $aa == 'Validar_Eliminar_Predio' ){
   $oPrediosController->Validar_Eliminar_Predio();

}else if( $aa == 'Eliminar_Predio' ){
   $oPrediosController->Eliminar_Predio();

}else if( $aa == 'Habilitar_Predios' ){
   $oPrediosController->Habilitar_Predios();

}else if( $aa == 'DarDeBaja_Predios' ){
   $oPrediosController->DarDeBaja_Predios();

}else if($aa == 'Migrar_Predio'){
  $oPrediosController->Migrar_Predios();
// FIN JMCR
}else{
  $oPrediosController->index();  
}
?>
