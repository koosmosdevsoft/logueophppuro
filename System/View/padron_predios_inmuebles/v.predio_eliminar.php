<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

<script type="text/javascript">
	$(document).ready(function(){
		$("#div_tabs_form").tabs();
	});
</script>
<?php
	$Datos = $data;
	$ArrayPrediosMigrar = $data['PREDIOS_MIGRAR'];
?>
<table width="797" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="763" height="30" class="Titulo_02_19px">&nbsp;&nbsp;<img src="../webimages/iconos/reporte_03.png" width="48" height="48" align="absmiddle" /> Eliminar predio</td>
    	<td width="34" height="20"><a style="text-align:center" href="#" onclick='$(&quot;#detail-formulario&quot;).html(&quot;&quot;);' ><img src="../webimages/iconos/cerrar.png" width="22" height="22" border="0" /></a></td>
	</tr>
	<tr>
    	<td height="20" colspan="2"><hr /></td>
    </tr>
	<tr>
    	<td colspan="2" >
    		<div id="div_tabs_form">
				<ul>
					<li><a href="#tabs-1_form">Datos Generales</a></li>
				</ul>

				<div id="tabs-1_form" style="background-color:#f4f4f4">
					<table width="788" border="0" cellpadding="0" cellspacing="3" class="TABLE_border4" style="background-color:#f4f4f4" >
						<tr>
							<td>&nbsp;</td>
							<td width="29" class="texto_arial_plomito_11_N"><i class="fa fa-check-circle fa-fw" aria-hidden="true" style="font-size:18px"></i></td>
							<td width="200" class="texto_arial_plomito_11_N">Nombre del local/predio</td>
							<td colspan="2"><span class="texto_02_11"><?php print $Datos['DENOMINACION_PREDIO']?></span></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td width="29" class="texto_arial_plomito_11_N"><i class="fa fa-check-circle fa-fw" aria-hidden="true" style="font-size:18px"></i></td>
							<td width="200" class="texto_arial_plomito_11_N">Personas asignadas</td>
							<td colspan="2"><span class="texto_02_11"><?php print $Datos['CANTIDAD_PERSONAL']?></span></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td width="29" class="texto_arial_plomito_11_N"><i class="fa fa-check-circle fa-fw" aria-hidden="true" style="font-size:18px"></i></td>
							<td width="200" class="texto_arial_plomito_11_N">Áreas asignadas</td>
							<td colspan="2"><span class="texto_02_11"><?php print $Datos['CANTIDAD_AREAS']?></span></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td width="29" class="texto_arial_plomito_11_N"><i class="fa fa-check-circle fa-fw" aria-hidden="true" style="font-size:18px"></i></td>
							<td width="200" class="texto_arial_plomito_11_N">Muebles asignados</td>
							<td colspan="2"><span><?php print $Datos['CANTIDAD_PREDIOS']?></span></td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
						<?php 
							$bloqueo_migrar = 'required';
							$bloqueo_eliminar = 'disabled';
							if ($Datos['CANTIDAD_PERSONAL'] == '0' &&  $Datos['CANTIDAD_AREAS'] == '0' && $Datos['CANTIDAD_PREDIOS'] == '0') {
								$bloqueo_migrar = 'disabled';
								$bloqueo_eliminar = '';
							}
						?>
						<tr>
							<td colspan="3">&nbsp;</td>
							<td>
								<form>
									<select name="cbo_predio" id="cbo_predio" <?php print $bloqueo_migrar ?>>
										<option value="">:: Seleccione ::</option>
										<?php
											if($ArrayPrediosMigrar):
												while (odbc_fetch_row($ArrayPrediosMigrar)){
										?>
													<option value="<?php print odbc_result($ArrayPrediosMigrar,"ID_PREDIO_SBN") ?>"><?php print odbc_result($ArrayPrediosMigrar,"DENOMINACION_PREDIO") ?></option>
										<?php
												}
											endif; 
										?>
									</select>
									<input name="btn_migrar" type="button" class="btn btn-success" id="btn_migrar" value="Migrar" onclick="handle_Migrar_Predio('<?php print $Datos['ID_PREDIO_SBN']?>', '<?php print $Datos['COD_TIP_PROPIEDAD']?>')" <?php print $bloqueo_migrar ?>/>
								</form>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
						<tr>
							<td colspan="3">&nbsp;</td>
							<td>
								<input name="btn_eliminar" type="button" class="btn btn-primary" id="btn_eliminar" value="Eliminar predio" onclick="handle_Eliminar_Predio('<?php print $Datos['ID_PREDIO_SBN']?>', '<?php print $Datos['COD_TIP_PROPIEDAD']?>')" <?php print $bloqueo_eliminar ?>/>
								<input name="btn_cancelar" type="button" class="btn btn-danger" id="btn_cancelar" value="Cancelar"/>
							</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="5">&nbsp;</td>
						</tr>
					</table>
				</div>
    		</div>
    	</td>
    </tr>
</table>