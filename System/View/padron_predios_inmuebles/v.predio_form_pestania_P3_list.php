<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<?php 
$ArrayValorizacion = $data['DatValorizacion'];
//$this->dump($ArrayValorizacion);
?>
<table width="681" border="0" cellpadding="0" cellspacing="0" class="TABLE_border4">
  <tr>
    <td width="27" bgcolor="#D9D9D9" style="text-align:center">Item</td>
    <td width="42" bgcolor="#D9D9D9" style="text-align:center">Editar</td>
    <td width="56" bgcolor="#D9D9D9" style="text-align:center">Fecha<br />
      Tasación</td>
    <td width="90" bgcolor="#D9D9D9" style="text-align:center">Tipo<br />
      Valorización</td>
    <td width="67" bgcolor="#D9D9D9" style="text-align:center">Descargar</td>
    <td bgcolor="#D9D9D9" style="text-align:center">Valorización</td>
    <td bgcolor="#D9D9D9" style="text-align:center">Estado</td>
  </tr>

<?php if($ArrayValorizacion) foreach ($ArrayValorizacion as $ListValorizacion): 

	$color = ($ListValorizacion['VAL_ITEM'] % 2 == '0') ? 'bgcolor="#F4F4F4"' : 'bgcolor="#ffffff"';
	
?>
  <tr <?php echo $color; ?> >
    <td style="text-align:center;"><?php print $ListValorizacion['VAL_ITEM'] ?></td>
    <td style="text-align:center">
      <a href="#" onclick="Mostrar_Form_Valorizacion('<?php print $ListValorizacion['COD_VAL_PREDIO'] ?>')">
      <img src="../webimages/iconos/editar_texto.png" width="24" height="20" border="0" />
      </a>
    </td>
    <td style="text-align:center"><?php print $ListValorizacion['FECHA_VALORIZACION'] ?></td>
    <td style="text-align:center"><?php print $ListValorizacion['NOM_TIP_VALORIZACION'] ?></td>
    <td style="text-align:center">

<? if( $ListValorizacion['COD_PREDIO_DOC_TECN'] !='' && $ListValorizacion['COD_PREDIO_DOC_TECN'] !='0'){ ?>
      <a href="../../Repositorio/predios_docs_tecnicos/tasacion/<?php print $ListValorizacion['NOM_ARCHIVO'] ?>" target="_blank"><img src="../webimages/iconos/archivoPDF.png" width="20" height="20" border="0" /></a>
<? }else{?>
---
<? }?>
      </td>
    <td width="330" style="text-align:left">
    
    <table width="324" border="0" cellspacing="2" cellpadding="0">
              <tr>
                <td width="12">&nbsp;</td>
                <td width="135" height="18" bgcolor="#FFFFFF" class="TABLE_border4" style="color:#333333; font-weight:bold; font-size:10px">MONEDA</td>
                <td width="26" bgcolor="#FFFFFF" style="color:#666666; font-weight:bold; font-size:10px; text-align:center"><img src="../webimages/iconos/vineta_1.gif" width="5" height="7" /></td>
                <td width="24" bgcolor="#FFFFFF" style="color:#666666; font-weight:bold; font-size:10px; text-align:center"><?php print $ListValorizacion['TXT_MONEDA_ABREV'] ?></td>
                <td width="115" bgcolor="#FFFFFF" class="TABLE_border4" style="color:#333333; font-weight:bold; font-size:10px"><?php print $ListValorizacion['TXT_MONEDA'] ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td bgcolor="#E6E6E6" style="color:#333333; font-weight:bold; font-size:10px">Valor del Terreno</td>
                <td style="color:#666666; font-weight:bold; font-size:10px; text-align:center">=</td>
                <td style="color:#666666; font-weight:bold; font-size:10px; text-align:center"><?php print $ListValorizacion['TXT_MONEDA_ABREV'] ?></td>
                <td bgcolor="#E6E6E6" style="color:#333333; font-weight:bold; font-size:10px"><?php print $ListValorizacion['MONTO_VALOR_TERRENO'] ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td bgcolor="#E6E6E6" style="color:#333333; font-weight:bold; font-size:10px">Valor de Construcción</td>
                <td style="color:#666666; font-weight:bold; font-size:10px; text-align:center">=</td>
                <td style="color:#666666; font-weight:bold; font-size:10px; text-align:center"><?php print $ListValorizacion['TXT_MONEDA_ABREV'] ?></td>
                <td bgcolor="#E6E6E6" style="color:#333333; font-weight:bold; font-size:10px"><?php print $ListValorizacion['MONTO_VALOR_CONTRUCCION'] ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td bgcolor="#E6E6E6" style="color:#333333; font-weight:bold; font-size:10px">Valor de la Obra</td>
                <td style="color:#666666; font-weight:bold; font-size:10px; text-align:center">=</td>
                <td style="color:#666666; font-weight:bold; font-size:10px; text-align:center"><?php print $ListValorizacion['TXT_MONEDA_ABREV'] ?></td>
                <td bgcolor="#E6E6E6" style="color:#333333; font-weight:bold; font-size:10px"><?php print $ListValorizacion['MONTO_VALOR_OBRA'] ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td colspan="4"><hr class="hr_01" /></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td bgcolor="#E6E6E6" style="color:#333333; font-weight:bold; font-size:10px">Valor Total de Inmuebles</td>
                <td style="color:#666666; font-weight:bold; font-size:10px; text-align:center">=</td>
                <td style="color:#666666; font-weight:bold; font-size:10px; text-align:center"><?php print $ListValorizacion['TXT_MONEDA_ABREV'] ?></td>
                <td bgcolor="#E6E6E6" style="color:#333333; font-weight:bold; font-size:10px"><?php print $ListValorizacion['MONTO_VALOR_TOTAL_INMUEBLE'] ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td bgcolor="#E6E6E6" style="color:#333333; font-weight:bold; font-size:10px">Tipo de Cambio</td>
                <td style="color:#666666; font-weight:bold; font-size:10px; text-align:center">=</td>
                <td style="color:#666666; font-weight:bold; font-size:10px; text-align:center">S/.</td>
                <td bgcolor="#E6E6E6" style="color:#333333; font-weight:bold; font-size:10px"><?php print $ListValorizacion['TIPO_CAMBIO_TOTAL_INMUEBLE'] ?></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td bgcolor="#E6E6E6" style="color:#333333; font-weight:bold; font-size:10px">Valor Total del Inmueble en Soles</td>
                <td style="color:#666666; font-weight:bold; font-size:10px; text-align:center"> =</td>
                <td style="color:#666666; font-weight:bold; font-size:10px; text-align:center">S/.</td>
                <td bgcolor="#E6E6E6" style="color:#333333; font-weight:bold; font-size:10px"><?php print $ListValorizacion['MONTO_VALOR_TOTAL_INMUEBLE_SOL'] ?></td>
              </tr>
            </table>
    
    </td>
    <td width="67" style="text-align:center"><?php print $ListValorizacion['VAL_DESC_ESTADO'] ?></td>
  </tr>

<?php endforeach; ?>

  <tr>
    <td colspan="7" style="text-align:center"><hr class="hr_01" /></td>
  </tr>


</table>
