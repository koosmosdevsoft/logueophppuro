<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<script src="../../../utils/javascript/funcion.js"></script>
<?
$DatosPredLocal = $data['DatPredioLocal'];
$ArrayOficRegistral = $data['DatOficRegistral'];
$ArrayUnidMedida = $data['DatUnidMedida'];

//$this->dump($DatosPredLocal);
?>
<table width="667" border="0" cellpadding="0" cellspacing="2" style="background-color:#f4f4f4">
  <tr>
    <td valign="top">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">CUS</td>
    <td><span class="texto_02_11">
    <?
    $Cus = ($DatosPredLocal['DR_CUS'] == '0')? '':$DatosPredLocal['DR_CUS'];
	?>
      <input name="txt_reg_cus" type="text" id="txt_reg_cus" style="width:150px;" value="<?php print $Cus?>"/>
    </span></td>
    <td class="texto_arial_plomito_11_N">Reg. SINABIP</td>
    <td><span class="texto_02_11">
      
      <?
    $DR_RSINABIP = ($DatosPredLocal['DR_RSINABIP'] == '0')? '':$DatosPredLocal['DR_RSINABIP'];
	?>
      <input name="txt_reg_sinabip" type="text" id="txt_reg_sinabip" style="width:150px;" value="<?php print $DR_RSINABIP?>" onkeypress="return solo_numeros(event)" />
    </span></td>
  </tr>
  <tr>
    <td width="28" valign="top">&nbsp;</td>
    <td width="137" class="texto_arial_plomito_11_N">Propietario Registral</td>
    <td colspan="3"><span class="texto_02_11">
      <input name="txt_propietario_registral" type="text" id="txt_propietario_registral" style="width:470px;" value="<?php print $DatosPredLocal['DR_NOM_PROP_REGISTRAL']?>" />
    </span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Oficina   Registral</td>
    <td width="190"><select style="width:150px" name="cbo_oficina_registral" id="cbo_oficina_registral" class="form-control">
      <option value="0">[.Seleccione.]</option>      
                <?php if($ArrayOficRegistral) foreach ($ArrayOficRegistral as $ListOficRegistral): 
						$select_OfiReg = ($DatosPredLocal['DR_COD_OFIC_REGISTRAL'] == $ListOficRegistral['COD_OFIC_REGISTRAL']) ? 'selected="selected"' : '';
                ?>
        <option value="<?php print $ListOficRegistral['COD_OFIC_REGISTRAL'] ?>" <?php print $select_OfiReg ?> ><?php print $ListOficRegistral['NOM_OFICINA_REGISTRAL'] ?></option>
                <?php endforeach; ?>
    </select></td>
    <td width="126" class="texto_arial_plomito_11_N">Partida Registral</td>
    <td width="172"><span class="texto_02_11">
      <input name="txt_partida_registral" type="text" id="txt_partida_registral" style="width:150px;" value="<?php print $DatosPredLocal['DR_PARTIDA_ELECTRONICA']?>"/>
    </span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Código del Predio</td>
    <td><span class="texto_02_11">
      <input name="txt_cod_predio" type="text" id="txt_cod_predio" style="width:150px;" value="<?php print $DatosPredLocal['DR_CODIGO_PREDIO']?>"/>
    </span></td>
    <td class="texto_arial_plomito_11_N">Area Registral</td>
    <td><span class="texto_02_11">
        <?
    $DR_AREA_REGISTRAL = ($DatosPredLocal['DR_AREA_REGISTRAL'] == '0')? '':$DatosPredLocal['DR_AREA_REGISTRAL'];
	?>
      <input name="txt_area_registral" type="text" id="txt_area_registral" style="width:100px;" value="<?php print $DR_AREA_REGISTRAL?>" onkeyup="formatodecimales(this)"/>
      <select name="cbo_unid_medida" class="form-control" id="cbo_unid_medida" style="width:50px;" >
        <option value="0"> :: UM ::</option>
                <?php if($ArrayUnidMedida) foreach ($ArrayUnidMedida as $ListUnidMedida): 
						$select_UnidMed = ($DatosPredLocal['DR_COD_TIP_UNID_MED'] == $ListUnidMedida['COD_TIP_UNID_MED']) ? 'selected="selected"' : '';
                ?>
                <option value="<?php print $ListUnidMedida['COD_TIP_UNID_MED'] ?>" <?php print $select_UnidMed ?> ><?php print $ListUnidMedida['ABREV_TIP_UNID_MED'] ?></option>
                <?php endforeach; ?>
      </select>
    </span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Tomo</td>
    <td><span class="texto_02_11">
      <input name="txt_tomo" type="text" id="txt_tomo" style="width:150px;" value="<?php print $DatosPredLocal['DR_TOMO']?>"/>
    </span></td>
    <td class="texto_arial_plomito_11_N">Asiento</td>
    <td><span class="texto_02_11">

      <input name="txt_asiento" type="text" id="txt_asiento" style="width:150px;" value="<?php print $DatosPredLocal['DR_ASIENTO']?>"/>
    </span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Fojas</td>
    <td><span class="texto_02_11">
      <input name="txt_fojas" type="text" id="txt_fojas" style="width:150px;" value="<?php print $DatosPredLocal['DR_FOJAS']?>" onkeypress="return solo_numeros(event)" />
    </span></td>
    <td class="texto_arial_plomito_11_N">Ficha</td>
    <td><span class="texto_02_11">
      <input name="txt_ficha" type="text" id="txt_ficha" style="width:150px;" value="<?php print $DatosPredLocal['DR_FICHA']?>"/>
    </span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Cuenta con <br />
    Notación Preventiva</td>
    <td><span class="texto_02_11">
      <select style="width:60px" name="txt_anotacion_preventiva" class="form-control" id="txt_anotacion_preventiva" >
        <option value="-"> :: --- ::</option>
        <? 		
			if($DatosPredLocal['DR_ANOTACION_PREVENTIVA'] == 'SI'){
				$variable_01 = 'selected="selected"';
				$variable_02 = '';
			}else if($DatosPredLocal['DR_ANOTACION_PREVENTIVA'] == 'NO'){
				$variable_01 = '';
				$variable_02 = 'selected="selected"';
			}
			?>
        <option value="SI" <?=$variable_01?> >SI</option>
        <option value="NO" <?=$variable_02?> >NO</option>
      </select>
    </span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
