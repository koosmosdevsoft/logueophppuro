<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="../utils/css/stylesPaginacion.css"/>
<?php
$PrediosLocal = $data['Predio_Locales'];
$pager = $data['pager'];
//$this->dump($data['pager']);
//$this->dump($pager);
?>

<table width="593" border="0" cellpadding="0" cellspacing="0" class="TABLE_border4" >
  <tr>
    <th width="40" height="30" bgcolor="#DDE4EC" style="text-align:center">Item</th>
    <th width="328" bgcolor="#DDE4EC" style="text-align:center">Denominación del Predio y/o Local</th>
    <th width="109" bgcolor="#DDE4EC">Tipo de <br />
    Propiedad</th>
    <th width="83" bgcolor="#DDE4EC">Proceso de<br />
Validación</th>
    <th width="60" bgcolor="#DDE4EC">Estado</th>
    <th width="49" bgcolor="#DDE4EC">Editar</th>
    <!-- <th width="49" bgcolor="#DDE4EC">Eliminar / <br> Dar de Baja</th> -->
    <th width="49" bgcolor="#DDE4EC">Eliminar</th>
    <th width="49" bgcolor="#DDE4EC">Dar Baja</th>
  </tr>
  
<?php if($PrediosLocal) foreach ($PrediosLocal as $ListaLocales): ?>

  <tr  onmouseover="mOvr2(this)" onmouseout="mOut2(this)"  style="<?= print $ListaLocales['stylo_estado'] ?>">
    <td style="text-align:center"><?php print $ListaLocales['ROW_NUMBER_ID']?></td>
    <td style="text-align:left"><?php print $ListaLocales['DENOMINACION_PREDIO'] ?></td>
    <td><?php print $ListaLocales['DESC_TIP_PROPIEDAD'] ?></td>
    <td><?php print $ListaLocales['Desc_TIPO_VALIDACION'] ?></td>
    <td><?php print $ListaLocales['DESC_EST_PROPIEDAD'] ?></td>
    <td><a href="#" onclick="handle_Mostrar_Form_Predio_Local('<?php print $ListaLocales['ID_PREDIO_SBN']?>')"><img src="../webimages/iconos/editar_texto.png" width="24" height="20" border="0" /></a></td>
    <!-- <td>
        <a href="#" onclick="handle_Eliminar_Predio_Local('<?php print $ListaLocales['ID_PREDIO_SBN']?>', 
            '<?php print $ListaLocales['COD_TIP_PROPIEDAD']?>')">
            <?php  

                if ($ListaLocales['COD_TIP_PROPIEDAD'] == '1') 
                {
            ?>
                <img src="../webimages/iconos/eliminar-predio.png" width="24" height="20" border="0" />
            <?php

                }else{
            ?>

            
                    <img src="../webimages/iconos/dar-baja.png" width="24" height="20" border="0" />
            <?php

                }

            ?>
            
        </a>
    </td> -->
    <td>
        <?php if($ListaLocales['ID_ESTADO'] == '3'): ?>
            <a href="#" onclick="handle_Habilitar_Predio('<?php print $ListaLocales['ID_PREDIO_SBN']?>', '<?php print $ListaLocales['COD_TIP_PROPIEDAD']?>')" title="Habilitar">
                <i class="fa fa-check" aria-hidden="true" style="font-size:18px"></i>
            </a>
        <?php  else: ?>
            <a href="#" onclick="handle_Validar_Eliminar_Predio('<?php print $ListaLocales['ID_PREDIO_SBN']?>', '<?php print $ListaLocales['COD_TIP_PROPIEDAD']?>')" title="Eliminar">
                <i class="fa fa-trash fa-fw" aria-hidden="true" style="font-size:18px"></i>
            </a>
        <?php  endif; ?>
    </td>
    <td>
        <?php if($ListaLocales['ID_ESTADO'] == '1'): ?>
            <a href="#" onclick="handle_Bajar_Predio('<?php print $ListaLocales['ID_PREDIO_SBN']?>', '<?php print $ListaLocales['COD_TIP_PROPIEDAD']?>')" title="Dar de baja">
                <i class="fa fa-ban fa-fw" aria-hidden="true" style="font-size:18px"></i>
            </a>
        <?php  else: ?>
            &nbsp;
        <?php  endif; ?>
    </td>

  </tr>
  
<?php endforeach; ?>

<tr  onmouseover="mOvr2(this)" onmouseout="mOut2(this)">
    <td colspan="6" bgcolor="#DDE4EC">
<div id="div_paginacion" >
<?php 
if ($pager['cantidadPaginas'] > 1): ?>
    <div class="pagination">
      <ul>
        <?php if ($pager['indicePagina'] != 1): ?>
        	<li><a href="javascript: handle_paginar_predios(1)" class="paginate">Inicial</a></li>
            <li><a href="javascript: handle_paginar_predios(<?php print $pager['indicePagina'] - 1; ?>)" class="paginate"><<</a></li>
        <?php endif; ?>
        <?php for ($index = $pager['limiteInferior']; $index <= $pager['limiteSuperior']; $index++): ?>
        	<?php if ($index == $pager['indicePagina']): ?>
                <li class="active"><a><?php print $index; ?></a></li>
            <?php else: ?>
            	<li><a href="javascript: handle_paginar_predios(<?php print $index; ?>)" class="paginate"><?php print $index; ?></a></li>
            <?php endif; ?>
		<?php endfor; ?>
        <?php if ($pager['indicePagina'] != $pager['cantidadPaginas']): ?>
        	<li><a href="javascript: handle_paginar_predios(<?php print $pager['indicePagina'] + 1; ?>)" class="paginate">>></a></li>
            <li><a href="javascript: handle_paginar_predios(<?php print $pager['cantidadPaginas']; ?>)" class="paginate">Final</a></li>
        <?php endif; ?>
      </ul>
    </div>
<?php endif; ?>
</div>
</td>
  </tr>
  
</table>