<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../utils/css/stylesPaginacion.css" />
<table width="100%" border="0" cellspacing="3" cellpadding="0">
    <tr>
      <td width="636" valign="top">


<table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td width="70%" >
      <div class="Titulo_02_19px">
        <img src="../webimages/iconos/btn_admin_55.png" width="58" height="58" align="absmiddle" /> Administración de Predios y/o Locales</div></td>
    <td width="18%" >
      <div style="text-align:right">
        <button class="btn btn-primary" onclick="handle_Mostrar_Form_Predio_Local('')">
        <i class="fa fa-plus-square" aria-hidden="true" style="font-size:14px" ></i> Agregar Predio</button>
        </div>
      </td>
  </tr>
  <tr>
        <td width="39"> &nbsp; &nbsp; &nbsp;<img src="../webimages/iconos/pdf.png" width="27" height="27" /> &nbsp;&nbsp; <a href="../manuales/SINABIP_INMUEBLES_REGISTRO_PREDIOS.pdf" target="_blank" class="texto_arial_plomo_n_12">Descargar Guía de Usuario : Predios</a></td>
      </tr>
  <tr>
    <td colspan="2" ><hr /></td>
    </tr>
  <tr>
    <td style="color:red; font-size: 14px" colspan="2" >Los Predios eliminados en el módulo de Decreto de Urgencia se visualizarán en rojo</td>
  </tr>
  </table>
<div id="div_tabs" >
  <ul>

    <li><a href="#tabs-1">Busqueda de Predios y/ o Local</a></li>


  </ul>

  <div id="tabs-1">
    <table width="100%" border="0" cellpadding="0" cellspacing="2" bgcolor="#f4f4f4" class="table-container TABLE_border4">
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <tr>
    <td width="3%">&nbsp;</td>
    <td width="21%" height="30">Buscar por denominación</td>
    <td width="64%"><input name="txb_busc_nom_predios" type="text" id="txb_busc_nom_predios" style="width:95%"/></td>
    <td width="12%">
  <a href="#" onclick='handle_paginar_predios(1); $("#detail-formulario").html("");'><img src="../webimages/iconos/ver_icono_b.png" width="20" height="22" border="0" /></a> &nbsp;&nbsp;
  <a href="#" onclick="handle_clear_busqueda()">
  <img src="../webimages/iconos/quitar_flitros.png" width="22" height="22" border="0" /></a>
      </td>
  </tr>
  <tr>
    <td colspan="4">&nbsp;</td>
    </tr>
</table>


<div id="detail-busqueda" style="padding:10px 0px 10px 0px"></div>

  </div>
    </div>
      </td>
      <td width="877" valign="top">
<div id="detail-formulario" style="padding:10px 0px 10px 10px;"></div>
      </td>
    </tr>
</table>
<script type="text/javascript" > 
    <?php include_once('js/script.js') ?>
</script>