<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 
<style type="text/css">
    .messages{
        float: left;
        font-family: sans-serif;
        display: none;
    }
    .info{
        padding: 5px;
        border-radius: 5px;
        background: #5693B4;/*fondo azul MARINO AL SELECCIONAR EL ARCHIVO*/
        color: #FFFFFF;
        font-size: 12px;
        text-align: center;
    }
    .before{
        padding: 8px;
        border-radius: 8px;
        background: #1ABC9C;/*VERDE - CUANDO VA CARGANDO EL FILE*/
        color: #fff;
        font-size: 13px;
        text-align: center;
    }
    .success{
        padding: 8px;
        border-radius: 8px;
        background: #E2F2FF;/*cuando finaliza*/
        color: #A12A2A;
        font-size: 15px;
        text-align: center;
		font-weight:bold;
    }
    .error{
        padding: 8px;
        border-radius: 8px;
        background: red;
        color: #fff;
        font-size: 13px;
        text-align: center;
    }
</style>

<script>

$(document).ready(function(){	
    $("#txt_fecha_valorizacion").datepicker({
		changeMonth: true,
		changeYear: true
    });	
});	
</script>

<script>
$(document).ready(function(){

	$(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_archivo_tasacion")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		
		
		if(fileExtension == 'PDF' || fileExtension == 'pdf'){
			$(".div_nom_archivo").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");			
			$("#txt_indicador_reg_tasac").val('1');
		}else{
			$("#txf_archivo_tasacion").val('');
			$(".div_nom_archivo").html("");
			alert('Error: Solo se permiten estos archivos .pdf');
			return false;
		}
		
    });

    //al enviar el formulario
    $("#link_guardar").click(function(){
        //información del formulario
		
		var txh_cod_val_predio		= $("#txh_cod_val_predio").val();
		var TXH_SIMI_COD_USUARIO 	= $("#TXH_SIMI_COD_USUARIO").val();
		var TXH_SIMI_COD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();
		var txh_COD_UE_LOCAL 	= $("#txh_COD_UE_LOCAL").val();
		
		var txt_indicador_reg_tasac 	= $("#txt_indicador_reg_tasac").val();
		var txt_w_nom_archivo_reg_tasac 	= $("#txt_w_nom_archivo_reg_tasac").val();
		
		$("#txh_cod_val_cod_user").val(TXH_SIMI_COD_USUARIO);
		$("#txh_cod_val_cod_ent").val(TXH_SIMI_COD_ENTIDAD);
		$("#txh_cod_val_cod_predio").val(txh_COD_UE_LOCAL);
		

		if(txh_cod_val_predio == '' && txt_indicador_reg_tasac == ''){
			alert('Adjuntar el archivo de la tasacion');
			
		}else{
			
			
			var formData = new FormData($(".formulario")[0]);
			var message = ""; 
			//hacemos la petición ajax  
			$.ajax({  
				url: "View/padron_predios_inmuebles/principal.php?operacion=Guardar_valorizacion",
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,			
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
					message = $("<span class='before'>Subiendo el archivo, por favor espere...</span>");
					showMessage(message)        
				},
				//una vez finalizado correctamente
				success: function(data){
					//alert(data);
					
					//if(isFile(fileExtension)){
						
						var valor 				= data.split("[*]")[0];
						var wwwID_PREDIO_SBN		= data.split("[*]")[1];
						
						if(valor==1){
							
							//var url_aweb = "http://archivos.sbn.gob.pe/Asientos/"+nro_rsinabip+"/"+nom_archivo;
							carga_loading('div_list_valorizacion'); 			
							$("#txh_COD_UE_LOCAL").val(wwwID_PREDIO_SBN);
							Listar_Documentos_Valorizacion(wwwID_PREDIO_SBN);
							
							$("#div_dialog_form_reg_valorizacion").dialog("destroy");
							document.getElementById('div_cab_valorizacion').innerHTML = '';
							
						}else{
							message = $("<span class='error'>Ha ocurrido un error: "+data+"</span>");
							showMessage(message);
						}
					//}
				},
				//si ha ocurrido un error
				error: function(){
					message = $("<span class='error'>Ha ocurrido un error.</span>");
					showMessage(message);
				}
			});
			
		
			
		}
		
		
		
    });
})

//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}

//comprobamos si el archivo a subir es una imagen
//para visualizarla una vez haya subido
function isFile(extension)
{
    switch(extension.toLowerCase()) 
    {
        case 'pdf':  case 'PDF': case 'jpg': case 'JPG':
            return true;
        break;
        default:
            return false;
        break;
    }
}
</script>
<?php
$DatosPredLocal = $data['DatPredioLocal'];
$ArrayTipoDocTasa = $data['DatTipoDocTasa'];
$ArrayTipoValorizacion = $data['DatTipoValorizacion'];
$ArrayTipoMoneda = $data['DatTipoMoneda'];

$DatosIDValor = $data['DatIDValor'];

$DatosE_Valorizacion = $data['Dat_E_Valorizacion'];


//$this->dump($DatosE_Valorizacion);
?>
<form enctype="multipart/form-data" class="formulario" >
<table width="753" border="0" cellpadding="0" cellspacing="2" style="background-color:#f4f4f4" class="TABLE_border4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="6" class="texto_arial_plomito_11_N">
      <input type="hidden" name="txh_cod_val_predio" id="txh_cod_val_predio" value="<?php print $DatosE_Valorizacion['E1_COD_VAL_PREDIO'] ?>" readonly="readonly" style="width:50px" />
      <input type="hidden" name="txh_cod_val_cod_user" id="txh_cod_val_cod_user" style="width:50px" />
      <input type="hidden" name="txh_cod_val_cod_ent" id="txh_cod_val_cod_ent"  style="width:50px" />
      <input type="hidden" name="txh_cod_val_cod_predio" id="txh_cod_val_cod_predio"  style="width:50px" />
      <input type="hidden" name="TXT_COD_PREDIO_DOC_TECN_X2"  id="TXT_COD_PREDIO_DOC_TECN_X2" size="5" value="<?php print $DatosE_Valorizacion['E1_COD_PREDIO_DOC_TECN'] ?>"/>
    </td>
    </tr>
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Documento de valorización</td>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="170"><select name="CBO_tip_doc_tecnico_val" id="CBO_tip_doc_tecnico_val" style="width:150px" >
                <?php if($ArrayTipoDocTasa) foreach ($ArrayTipoDocTasa as $ListTipoDocTasa): ?>
        <option value="<?php print $ListTipoDocTasa['T1_COD_TIP_DOC_PRED'] ?>" selected="selected" ><?php print $ListTipoDocTasa['T1_NOM_TIP_DOC'] ?></option>
                <?php endforeach; ?>
    </select></td>
    <td colspan="2" class="texto_arial_plomito_11_N">Tipo de Valorización</td>
    <td width="171"><select name="cbo_tipo_valorizacion" id="cbo_tipo_valorizacion" class="form-control" style="width:150px">
      <option value="-">[.Seleccione.]</option>
                <?php if($ArrayTipoValorizacion) foreach ($ArrayTipoValorizacion as $ListTipoValorizacion): 
						$select_TipVal = ($DatosE_Valorizacion['E1_COD_TIP_VALORIZACION'] == $ListTipoValorizacion['COD_TIP_VALORIZACION']) ? 'selected="selected"' : '';
                ?>
        <option value="<?php print $ListTipoValorizacion['COD_TIP_VALORIZACION'] ?>" <?php print $select_TipVal ?> ><?php print $ListTipoValorizacion['NOM_TIP_VALORIZACION'] ?></option>
                <?php endforeach; ?>
    </select></td>
  </tr>
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Fecha de Valorización</td>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td><input name="txt_fecha_valorizacion" type="text" id="txt_fecha_valorizacion" style="width:150px" value="<?php print $DatosE_Valorizacion['E1_FECHA_VALORIZACION'] ?>" /></td>
    <td colspan="2" class="texto_arial_plomito_11_N">Tipo de moneda</td>
    <td><select name="cbo_tipo_moneda" id="cbo_tipo_moneda" class="form-control" style="width:150px" onchange="mostrar_tipo_moneda()">
      <option value="-">[.Seleccione.]</option>
                <?php if($ArrayTipoMoneda) foreach ($ArrayTipoMoneda as $ListTipoMoneda): 
						$select_TipMoneda = ($DatosE_Valorizacion['E1_COD_MONEDA_VALORIZACION'] == $ListTipoMoneda['COD_MONEDA']) ? 'selected="selected"' : '';
                ?>
        <option value="<?php print $ListTipoMoneda['COD_MONEDA'] ?>" <?php print $select_TipMoneda ?> ><?php print $ListTipoMoneda['TXT_MONEDA'] ?></option>
                <?php endforeach; ?>
    </select></td>
  </tr>
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Valor del Terreno</td>
    <td class="texto_arial_plomito_11_N"><div id="div_mon_val_terreno"><?php print $DatosE_Valorizacion['E1_DES_MONEDA_VALORIZACION'] ?></div></td>
    <td><input name="txt_valor_terreno" type="text" id="txt_valor_terreno" style="width:150px" onkeyup="mostrar_tipo_moneda()" value="<?php print $DatosE_Valorizacion['E1_MONTO_VALOR_TERRENO'] ?>" /></td>
    <td class="texto_arial_plomito_11_N">Valor de la Construcción</td>
    <td class="texto_arial_plomito_11_N"><div id="div_mon_val_construccion"><?php print $DatosE_Valorizacion['E1_DES_MONEDA_VALORIZACION'] ?></div></td>
    <td><input name="txt_valor_construccion" type="text" id="txt_valor_construccion" style="width:150px" onkeyup="mostrar_tipo_moneda()" value="<?php print $DatosE_Valorizacion['E1_MONTO_VALOR_CONTRUCCION'] ?>" /></td>
  </tr>
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Valor de la Obra</td>
    <td class="texto_arial_plomito_11_N"><div id="div_mon_val_obra"><?php print $DatosE_Valorizacion['E1_DES_MONEDA_VALORIZACION'] ?></div></td>
    <td><input name="txt_valor_obra" type="text" id="txt_valor_obra" style="width:150px" onkeyup="mostrar_tipo_moneda()" value="<?php print $DatosE_Valorizacion['E1_MONTO_VALOR_OBRA'] ?>" /></td>
    <td class="texto_arial_plomito_11_N">Valor Total del Inmuebles</td>
    <td class="texto_arial_plomito_11_N"><div id="div_mon_val_tot_inm"><?php print $DatosE_Valorizacion['E1_DES_MONEDA_VALORIZACION'] ?></div></td>
    <td><input name="txt_valor_total_inmueble" type="text" id="txt_valor_total_inmueble" style="width:150px" value="<?php print $DatosE_Valorizacion['E1_MONTO_VALOR_TOTAL_INMUEBLE'] ?>" readonly="readonly" /></td>
  </tr>
  <tr>
    <td colspan="7" class="texto_arial_plomito_11_N"><hr class="hr_01" /></td>
  </tr>
  <tr>
    <td width="18" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="163" class="texto_arial_plomito_11_N">Tipo de cambio del dolar </td>
    <td width="26" class="texto_arial_plomito_11_N">S/.</td>
    <td><input name="txt_tipo_cambio" type="text" id="txt_tipo_cambio" style="width:150px" onkeyup="mostrar_tipo_moneda()" value="<?php print $DatosE_Valorizacion['E1_TIPO_CAMBIO_TOTAL_INMUEBLE'] ?>" /></td>
    <td width="155" class="texto_arial_plomito_11_N">Valor Total del Inmueble</td>
    <td width="34" class="texto_arial_plomito_11_N">S/.</td>
    <td><input name="txt_valor_total_inmueble_soles" type="text" id="txt_valor_total_inmueble_soles" style="width:150px" value="<?php print $DatosE_Valorizacion['E1_MONTO_VALOR_TOTAL_INMUEBLE_SOL'] ?>" readonly="readonly" /></td>
  </tr>
  <tr>
    <td colspan="3" class="texto_arial_plomito_11_N"></td>
    <td colspan="4"></td>
  </tr>
  <tr>
    <td colspan="7"><table width="735" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="19">&nbsp;</td>
        <td width="262" height="35"><span class="texto_arial_plomito_11_N">Adjuntar archivo de tasación en formato PDF</span></td>
        <td width="442"><input type="file" name="txf_archivo_tasacion" id="txf_archivo_tasacion" /></td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">
        
        <div class="div_nom_archivo"></div>
        <a href="../../Repositorio/predios_docs_tecnicos/tasacion/<?php print $DatosE_Valorizacion['E1_NOM_ARCHIVO_Valorizacion'] ?>" target="_blank"><?php print $DatosE_Valorizacion['E1_NOM_ARCHIVO_Valorizacion'] ?></a>
        <input name="txt_indicador_reg_tasac" type="hidden" id="txt_indicador_reg_tasac" style="width:120px" />
      <input name="txt_w_nom_archivo_reg_tasac" type="hidden" id="txt_w_nom_archivo_reg_tasac" style="width:450px" value="<?php print $DatosE_Valorizacion['E1_NOM_ARCHIVO_Valorizacion'] ?>" />
      <input name="txt_w_peso_archivo_reg_tasac" type="hidden" id="txt_w_peso_archivo_reg_tasac" style="width:200px" value="<?php print $DatosE_Valorizacion['E1_PESO_ARCHIVO_Valorizacion'] ?>" />
        
        </td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="6"><hr class="hr_01" /></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="6">
    <input name="link_guardar" type="button" class="btn btn-primary" id="link_guardar" value="Guardar" />&nbsp;&nbsp;&nbsp;
    
    <input name="btn_guardar_partida2" type="button" class="btn btn-Rojo" id="btn_guardar_partida2" value="Eliminar" onclick="Eliminar_Valorizacion('<?php print $DatosE_Valorizacion['E1_COD_VAL_PREDIO'] ?>','<?php print $DatosE_Valorizacion['E1_COD_PREDIO_DOC_TECN'] ?>')" />
    </td>
    </tr>
  <tr>
    <td colspan="7"><br /><div class="messages"></div></td>
    </tr>
</table>
</form>