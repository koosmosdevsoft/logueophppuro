<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="./View/padron_predios_inmuebles/css/style.css"/>


<script type="text/javascript">
$(document).ready(function(){
	
	$("#div_tabs_form").tabs();
	
});
</script>

<?




$DatosPredLocal = $data['DatPredioLocal'];
$ArrayDepartamento = $data['Departamento'];
$ArrayProvincia = $data['Provincia'];
$ArrayDistrito = $data['Distrito'];
$ArrayTipoZona = $data['TipoZona'];
$ArrayTipoVia = $data['TipoVia'];
$ArrayTipoHabilitacion = $data['TipoHabilitacion'];
$ArrayTipoPropiedad = $data['TipoPropiedad'];
//$ArrayErrores = $Errores['Errores'];


//$this->dump($ArrayErrores);
?>
<table width="797" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="763" height="30" class="Titulo_02_19px">&nbsp;&nbsp;<img src="../webimages/iconos/reporte_03.png" width="48" height="48" align="absmiddle" /> Registro de Predios y/o Locales</td>
    <td width="34" height="20"><a style="text-align:center" href="#" onclick='$(&quot;#detail-formulario&quot;).html(&quot;&quot;);' ><img src="../webimages/iconos/cerrar.png" width="22" height="22" border="0" /></a></td>
  </tr>

  <tr>
    <td height="20" colspan="2"><hr /></td>
  </tr>
  <tr>
    <td colspan="2" >
 
   
    <div id="div_errores" class="alert alert-danger texto_arial_plomito_11_N" role="alert" style = "display:none">
    </div>
   
 <div id="div_tabs_form" >
  <ul>

    <li><a href="#tabs-1_form">Datos Generales</a></li>


  </ul>

  <div id="tabs-1_form" style="background-color:#f4f4f4">
    
    
    <table width="788" border="0" cellpadding="0" cellspacing="3" class="TABLE_border4" style="background-color:#f4f4f4" >
      <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="3"><input name="txh_COD_UE_LOCAL" type="hidden" id="txh_COD_UE_LOCAL" size="20" value="<?php print $DatosPredLocal['E_ID_PREDIO_SBN']?>" /></td>
      </tr>
      <tr>
        <td width="29" class="texto_arial_plomito_11_N">&nbsp;</td>
        <td width="124" class="texto_arial_plomito_11_N">Nombre del local</td>
        <td colspan="3"><span class="texto_02_11">
          <input name="txt_nom_inmueble" type="text" id="txt_nom_inmueble" style="width:600px;" value="<?php print $DatosPredLocal['DENOMINACION_PREDIO']?>" />
          </span></td>
      </tr>
      <tr>
        <td class="texto_arial_plomito_11_N">&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Departamento</td>
        <td width="235">
<select name="cbo_departamento" id="cbo_departamento" onchange="mostrar_provincia()" class="form-control" style="width:180px">
                <option value="-">[.Seleccione.]</option>
                <?php if($ArrayDepartamento) foreach ($ArrayDepartamento as $ListDepartamento): 
						$select_Depa = ($DatosPredLocal['E_COD_DEPA'] == $ListDepartamento['COD_DPTO']) ? 'selected="selected"' : '';
                ?>
                <option value="<?php print $ListDepartamento['COD_DPTO'] ?>" <?php print $select_Depa ?> ><?php print $ListDepartamento['DEPARTAMENTO'] ?></option>
                <?php endforeach; ?>
              </select></td>
        <td width="145" class="texto_arial_plomito_11_N">Provincia</td>
        <td>
          <select name="cbo_provincia" class="form-control" id="cbo_provincia" onchange="mostrar_distrito()" style="width:180px" >
                  <option value="-">[.Seleccione.]</option>
                <?php if($ArrayProvincia) foreach ($ArrayProvincia as $ListProvincia): 
						$select_prov = ($DatosPredLocal['E_COD_PROV'] == $ListProvincia['COD_PROV']) ? 'selected="selected"' : '';
                ?>
                <option value="<?php print $ListProvincia['COD_PROV'] ?>" <?php print $select_prov ?> ><?php print $ListProvincia['PROVINCIA'] ?></option>
                <?php endforeach; ?>
              </select></td>
        </tr>
      <tr>
        <td class="texto_arial_plomito_11_N">&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Distrito</td>
        <td>
				<select name="cbo_distrito" class="form-control" id="cbo_distrito" style="width:180px" >
                  <option value="-">[.Seleccione.]</option>
                <?php if($ArrayDistrito) foreach ($ArrayDistrito as $ListDistrito): 
						$select_dist = ($DatosPredLocal['E_COD_DIST'] == $ListDistrito['COD_DIST']) ? 'selected="selected"' : '';
                ?>
                <option value="<?php print $ListDistrito['COD_DIST'] ?>" <?php print $select_dist ?> ><?php print $ListDistrito['DISTRITO'] ?></option>
                <?php endforeach; ?>
              </select>
        </td>
        <td class="texto_arial_plomito_11_N">Zona</td>
        <td><select name="cbo_zona" id="cbo_zona" class="form-control" style="width:180px">
                  <option value="-">[.Seleccione.]</option>
                <?php if($ArrayTipoZona) foreach ($ArrayTipoZona as $ListTipoZona): 
						$select_TipZona = ($DatosPredLocal['E_COD_ZONA'] == $ListTipoZona['COD_ZONA']) ? 'selected="selected"' : '';
                ?>
                <option value="<?php print $ListTipoZona['COD_ZONA'] ?>" <?php print $select_TipZona ?> ><?php print $ListTipoZona['NOM_ZONA'] ?></option>
                <?php endforeach; ?>
        </select></td>
        </tr>
      <tr>
        <td class="texto_arial_plomito_11_N">&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Tipo de Vía</td>
        <td><select name="cbo_via" id="cbo_via" class="form-control" style="width:180px">
          <option value="-">:: Seleccione ::</option>
                <?php if($ArrayTipoVia) foreach ($ArrayTipoVia as $ListTipoVia): 
						$select_TipVia = ($DatosPredLocal['E_COD_TVIA'] == $ListTipoVia['COD_TVIA']) ? 'selected="selected"' : '';
                ?>
                <option value="<?php print $ListTipoVia['COD_TVIA'] ?>" <?php print $select_TipVia ?> ><?php print $ListTipoVia['DESC_VIA'] ?></option>
                <?php endforeach; ?>
        </select></td>
        <td class="texto_arial_plomito_11_N">Nombre de la vía</td>
        <td><span class="texto_02_11">
          <input name="txt_nom_via" type="text" id="txt_nom_via" style="width:180px" value="<?php print $DatosPredLocal['NOMBRE_VIA']?>"/>
        </span></td>
        </tr>
      <tr>
        <td class="texto_arial_plomito_11_N">&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Número</td>
        <td><span class="texto_02_11">
          <input name="txt_nro_inmueble" type="text" id="txt_nro_inmueble" style="width:100px;"  value="<?php print $DatosPredLocal['NRO_PRED']?>"/>
        </span></td>
        <td class="texto_arial_plomito_11_N">Manzana</td>
        <td width="235"><span class="texto_02_11">
          <input name="txt_manzana" type="text" id="txt_manzana" style="width:60px;" value="<?php print $DatosPredLocal['MZA_PRED']?>"/>
        </span></td>
        </tr>
      <tr>
        <td class="texto_arial_plomito_11_N">&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Lote</td>
        <td><span class="texto_02_11">
          <input name="txt_lote" type="text" id="txt_lote" style="width:60px;" value="<?php print $DatosPredLocal['LTE_PRED']?>"/>
        </span></td>
        <td class="texto_arial_plomito_11_N"> Nro de Pisos</td>
        <td><span class="texto_02_11">
          <input name="txt_ubicado_piso_nro" type="text" id="txt_ubicado_piso_nro" style="width:55px;"  value="<?php print $DatosPredLocal['UBICADO_PISO']?>" />
        </span></td>
        </tr>
      <tr>
        <td class="texto_arial_plomito_11_N">&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Tipo Ubicación</td>
        <td><select name="txt_detalle" id="txt_detalle" class="form-control" style="width:180px">
          <option value="-" selected="selected">:: Seleccione ::</option>
          <?
		  
		  
				if($DatosPredLocal['E_ID_PREDIO_SBN'] != ''){
					
					$DET_PRED_ED = $DatosPredLocal['E_UBIC_DET_PRED'];
					
					if($DET_PRED_ED == 'INTERIOR'){
						$select_det_1 = 'selected="selected"';
						$select_det_2 = '';
						$select_det_3 = '';
						$select_det_4 = '';
						
					}else if($DET_PRED_ED == 'DEPARTAMENTO'){
						$select_det_1 = '';
						$select_det_2 = 'selected="selected"';
						$select_det_3 = '';
						$select_det_4 = '';
					
					}else if($DET_PRED_ED == 'OFICINA'){
						$select_det_1 = '';
						$select_det_2 = '';
						$select_det_3 = 'selected="selected"';
						$select_det_4 = '';
						
					}else if($DET_PRED_ED == 'ESTACIONAMIENTO'){
						$select_det_1 = '';
						$select_det_2 = '';
						$select_det_3 = '';
						$select_det_4 = 'selected="selected"';
					
					}else{
						$select_det_1 = '';
						$select_det_2 = '';
						$select_det_3 = '';
						$select_det_4 = '';
					}
					
				}
		 ?>
          <option value="INTERIOR" <?=$select_det_1?> >INTERIOR</option>
          <option value="DEPARTAMENTO" <?=$select_det_2?>>DEPARTAMENTO</option>
          <option value="OFICINA" <?=$select_det_3?>>OFICINA</option>
          <option value="ESTACIONAMIENTO" <?=$select_det_4?>>ESTACIONAMIENTO</option>
        </select></td>
        <td class="texto_arial_plomito_11_N">Nro de la Ubicación</td>
        <td><span class="texto_02_11">
          <input name="txt_nro_detalle" type="text" id="txt_nro_detalle" style="width:55px;" value="<?php print $DatosPredLocal['E_UBIC_DET_PRED_NRO']?>"/>
        </span></td>
        </tr>
      <tr>
        <td class="texto_arial_plomito_11_N">&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Habilitación</td>
        <td><select name="cbo_habilitacion" id="cbo_habilitacion" class="form-control" style="width:180px">
          <option value="-">:: Seleccione ::</option>
                <?php if($ArrayTipoHabilitacion) foreach ($ArrayTipoHabilitacion as $ListTipoHabilitacion): 
						$select_TipHabilitacion = ($DatosPredLocal['E_COD_THABILITACION'] == $ListTipoHabilitacion['COD_THABILITACION']) ? 'selected="selected"' : '';
                ?>
                <option value="<?php print $ListTipoHabilitacion['COD_THABILITACION'] ?>" <?php print $select_TipHabilitacion ?> ><?php print $ListTipoHabilitacion['TXT_THABILITACION'] ?></option>
                <?php endforeach; ?>
        </select></td>
        <td class="texto_arial_plomito_11_N">Nombre de Habilitación</td>
        <td><span class="texto_02_11">
          <input name="txt_nom_habilitacion" type="text" id="txt_nom_habilitacion" style="width:180px" value="<?php print $DatosPredLocal['NOM_THABILITACION']?>"/>
        </span></td>
        </tr>
      <tr>
        <td class="texto_arial_plomito_11_N">&nbsp;</td>
        <td class="texto_arial_plomito_11_N">Sector</td>
        <td><span class="texto_02_11">
          <input name="txt_nom_sector" type="text" id="txt_nom_sector" style="width:180px" value="<?php print $DatosPredLocal['NOM_SECTOR']?>"/>
          </span></td>
        <td class="texto_arial_plomito_11_N">Propiedad</td>
        <td><select name="cbo_propiedad" class="form-control" id="cbo_propiedad"  style="width:180px"  >
          <option value="-" selected="selected"> :: Seleccione :: </option>
          <?php if($ArrayTipoPropiedad) foreach ($ArrayTipoPropiedad as $ListTipoPropiedad): 
						$select_TipPropiedad = ($DatosPredLocal['E_COD_TIP_PROPIEDAD'] == $ListTipoPropiedad['COD_TIP_PROPIEDAD']) ? 'selected="selected"' : '';
                ?>
          <option value="<?php print $ListTipoPropiedad['COD_TIP_PROPIEDAD'] ?>" <?php print $select_TipPropiedad ?> ><?php print $ListTipoPropiedad['DESC_TIP_PROPIEDAD'] ?></option>
          <?php endforeach; ?>
          </select></td>
      </tr>
      <tr>
        <td colspan="5">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="5"><div id="div-detail-area_tipo_propiedad" style="padding: 0px 0px 0px 10px">
          <? if(($DatosPredLocal['E_COD_TIP_PROPIEDAD'] != '') && ($DatosPredLocal['E_COD_TIP_PROPIEDAD'] == '2' || $DatosPredLocal['E_COD_TIP_PROPIEDAD'] == '3' )){ ?>
          <? include_once('v.predio_form_pestania.php')?>
          <? } ?>
          </div></td>
      </tr>
      </table>



  </div>
    </div>
 
 

</td>
  </tr>
  <tr>
    <td height="30" colspan="2"><hr class="linea_separador_01" /></td>
  </tr>
  <tr>
    <td colspan="2">
    <div id="div_guardar"></div>
<? if($DatosPredLocal['DAR_BAJA'] == 'X'){?>    
<span class="texto_arial_rojo_n_11">* Los Datos de este local ya fueron dados de Baja</span>
<? }if($DatosPredLocal['DAR_BAJA'] == ''){?>
<input name="button2" type="button" class="btn btn-primary" id="button2" value="Guardar" onclick="Guardar_Datos_Local_Predio()" />
<!--<input name="button2" type="button" class="btn btn-Rojo" id="button2" value="Dar de Baja" onclick="Dar_Baja()" /> -->
<? }?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>
<div id="div_dialog_form_reg_documentos"><div id="Div_contenido_dialog"></div></div>