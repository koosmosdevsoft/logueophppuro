$(function() {
    $("#div_tabs").tabs();
	handle_paginar_predios(1);
    /*
	$("#txt_busc_fecha_asignacion").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true
    });*/
});


function handle_clear_busqueda(){	
  $("#txb_busc_nom_predios").val('');
  handle_paginar_predios(1);
}


function handle_paginar_predios(numeroPagina) {
  
  var TXH_SIMI_COD_ENTIDAD = $("#TXH_SIMI_COD_ENTIDAD").val();
  var txb_busc_nom_predios = $("#txb_busc_nom_predios").val();
  
  carga_loading('detail-busqueda');  
  var url = "View/padron_predios_inmuebles/principal.php?operacion=Filtrar_Lista_Predios";
  url += (TXH_SIMI_COD_ENTIDAD == "") ? "" : "&TXH_SIMI_COD_ENTIDAD=" + TXH_SIMI_COD_ENTIDAD;
  url += (txb_busc_nom_predios == "") ? "" : "&txb_busc_nom_predios=" + txb_busc_nom_predios;
  url += (numeroPagina == "") ? "1" : "&numeroPagina=" + numeroPagina;
  $.ajax(url).done(function (response) {
    $("#detail-busqueda").html(response);
  });
  
}

function handle_Mostrar_Form_Predio_Local(xID_PREDIO_SBN) {

  carga_loading('detail-formulario');	
  var url = "View/padron_predios_inmuebles/principal.php?operacion=Mostrar_Form_Datos_Predio";
  url += (xID_PREDIO_SBN == "") ? "" : "&xID_PREDIO_SBN=" + xID_PREDIO_SBN;
  $.ajax(url).done(function (response) {
    $("#detail-formulario").html(response);	
  });
}


function handle_Validar_Eliminar_Predio(xID_PREDIO_SBN, xCOD_TIP_PROPIEDAD){
	var xTXH_SIMI_COD_ENTIDAD = $("#TXH_SIMI_COD_ENTIDAD").val();
	var url = "View/padron_predios_inmuebles/principal.php?operacion=Validar_Eliminar_Predio";
	url += (xID_PREDIO_SBN == "") ? "" : "&xID_PREDIO_SBN=" + xID_PREDIO_SBN;
	url += (xCOD_TIP_PROPIEDAD == "") ? "" : "&xCOD_TIP_PROPIEDAD=" + xCOD_TIP_PROPIEDAD;
	url += (xTXH_SIMI_COD_ENTIDAD == "") ? "" : "&xTXH_SIMI_COD_ENTIDAD=" + xTXH_SIMI_COD_ENTIDAD;
	$.ajax(url).done(function (response) {
    	$("#detail-formulario").html(response);	
  });
}

function handle_Eliminar_Predio(xID_PREDIO_SBN, xCOD_TIP_PROPIEDAD){
	var xTXH_SIMI_COD_ENTIDAD = $("#TXH_SIMI_COD_ENTIDAD").val();
	var url = "View/padron_predios_inmuebles/principal.php?operacion=Eliminar_Predio";
	url += (xID_PREDIO_SBN == "") ? "" : "&xID_PREDIO_SBN=" + xID_PREDIO_SBN;
	url += (xCOD_TIP_PROPIEDAD == "") ? "" : "&xCOD_TIP_PROPIEDAD=" + xCOD_TIP_PROPIEDAD;
	url += (xTXH_SIMI_COD_ENTIDAD == "") ? "" : "&xTXH_SIMI_COD_ENTIDAD=" + xTXH_SIMI_COD_ENTIDAD;
	$.ajax(url).done(function (response) {
    	$('#detail-formulario').html('');
    	handle_paginar_predios(1);
	});
}

function handle_Migrar_Predio(xID_PREDIO_SBN, xCOD_TIP_PROPIEDAD){
	var xTXH_SIMI_COD_ENTIDAD = $("#TXH_SIMI_COD_ENTIDAD").val();
	var cbo_predio = $("#cbo_predio").val();
	if (cbo_predio=='') {
		alert('Debe escoger un predio');
	}else{
		mensaje = '¿Está Ud. seguro(a) de migrar la información del predio?';
		if (confirm(mensaje)) {
			var url = "View/padron_predios_inmuebles/principal.php?operacion=Migrar_Predio";
				url += (xID_PREDIO_SBN == "") ? "" : "&xID_PREDIO_SBN=" + xID_PREDIO_SBN;
				url += (xCOD_TIP_PROPIEDAD == "") ? "" : "&xCOD_TIP_PROPIEDAD=" + xCOD_TIP_PROPIEDAD;
				url += (xTXH_SIMI_COD_ENTIDAD == "") ? "" : "&xTXH_SIMI_COD_ENTIDAD=" + xTXH_SIMI_COD_ENTIDAD;
				url += (cbo_predio == "") ? "" : "&cbo_predio=" + cbo_predio;
			$.ajax(url).done(function (response) {
		    	$('#detail-formulario').html('');
		    	handle_paginar_predios(1);
			});
		}
	}
}

function handle_Habilitar_Predio(xID_PREDIO_SBN, xCOD_TIP_PROPIEDAD){
	var xTXH_SIMI_COD_ENTIDAD = $("#TXH_SIMI_COD_ENTIDAD").val();
	mensaje = '¿Está Ud. seguro(a) de habilitar el predio?';
	if (confirm(mensaje)) {
		var url = "View/padron_predios_inmuebles/principal.php?operacion=Habilitar_Predios";
		url += (xID_PREDIO_SBN == "") ? "" : "&xID_PREDIO_SBN=" + xID_PREDIO_SBN;
		url += (xCOD_TIP_PROPIEDAD == "") ? "" : "&xCOD_TIP_PROPIEDAD=" + xCOD_TIP_PROPIEDAD;
		url += (xTXH_SIMI_COD_ENTIDAD == "") ? "" : "&xTXH_SIMI_COD_ENTIDAD=" + xTXH_SIMI_COD_ENTIDAD;

		$.ajax(url).done(function (response) {
			if (response == '1'){
				handle_paginar_predios(1);
				$("#detail-formulario").html("");
			}else if(response == '2'){
				alert('No es posible llevar a cabo la operación, El Predio tiene Bienes asignados');
			}else{
				alert(response);
			}
	  	});
	}
}

function handle_Bajar_Predio(xID_PREDIO_SBN, xCOD_TIP_PROPIEDAD){
	var xTXH_SIMI_COD_ENTIDAD = $("#TXH_SIMI_COD_ENTIDAD").val();
	mensaje = '¿Está Ud. seguro(a) de dar de baja el predio?';
	if (confirm(mensaje)) {
		var url = "View/padron_predios_inmuebles/principal.php?operacion=DarDeBaja_Predios";
		url += (xID_PREDIO_SBN == "") ? "" : "&xID_PREDIO_SBN=" + xID_PREDIO_SBN;
		url += (xCOD_TIP_PROPIEDAD == "") ? "" : "&xCOD_TIP_PROPIEDAD=" + xCOD_TIP_PROPIEDAD;
		url += (xTXH_SIMI_COD_ENTIDAD == "") ? "" : "&xTXH_SIMI_COD_ENTIDAD=" + xTXH_SIMI_COD_ENTIDAD;

		$.ajax(url).done(function (response) {
			//alert(response.trim());
			if (response.trim() == '1'){
				handle_paginar_predios(1);
				$("#detail-formulario").html("");
			}else if(response.trim() == '2'){
				alert('No es posible llevar a cabo la operación, El Predio tiene Bienes asignados');
			}else{
				alert(response.trim());
			}
	  	});
	}
}

// FIN JMCR


function mostrar_provincia(){
	var cbo_depa	= $("#cbo_departamento").val();		
	$.post("View/padron_ubigeo/listbox_provincia.php",{ 
		cbo_depa:cbo_depa
	},function(data){
		$("#cbo_provincia").html(data);
	});
}


function mostrar_distrito(){
	var cbo_depa	= $("#cbo_departamento").val();
	var cbo_prov	= $("#cbo_provincia").val();	
	$.post("View/padron_ubigeo/listbox_distrito.php",{ 
		cbo_depa:cbo_depa,
		cbo_prov:cbo_prov
	},function(data){
		$("#cbo_distrito").html(data);
	})
}


function Mostrar_Datos_Registro_Datos_Tecnicos(wID_PREDIO_SBN){

	var cbo_propiedad 		= $("#cbo_propiedad").val();
	if((cbo_propiedad != '') && (cbo_propiedad == '2' || cbo_propiedad == '3' )){		
		Mostrar_Datos_Registro_Datos_Tecnicos_parametros(wID_PREDIO_SBN);
	}else{
		
		$("#div-detail-area_tipo_propiedad").html('');
	}
}


function Mostrar_Datos_Registro_Datos_Tecnicos_parametros(wID_PREDIO_SBN){
	$.post("View/padron_predios_inmuebles/v.predio_form_pestania.php",{
	},function(data){
		$("#div-detail-area_tipo_propiedad").html(data);
		Mostrar_tab1_datos_registrales(wID_PREDIO_SBN);
		Mostrar_tab2_datos_tecnicos(wID_PREDIO_SBN);
		Mostrar_tab3_datos_valorizacion(wID_PREDIO_SBN);
	});
}


function Mostrar_tab1_datos_registrales(wID_PREDIO_SBN){
 
  carga_loading('div_tab1_contenido');  
  var url = "View/padron_predios_inmuebles/principal.php?operacion=Mostrar_Form_tab1_Datos_Registrales";
  url += (wID_PREDIO_SBN == "") ? "" : "&wID_PREDIO_SBN=" + wID_PREDIO_SBN;

  $.ajax(url).done(function (response) {
    $("#div_tab1_contenido").html(response);
  });
}


function Mostrar_tab2_datos_tecnicos(wID_PREDIO_SBN){
 
  carga_loading('div_tab2_contenido');  
  var url = "View/padron_predios_inmuebles/principal.php?operacion=Mostrar_Form_tab2_Datos_Tecnicos";
  url += (wID_PREDIO_SBN == "") ? "" : "&wID_PREDIO_SBN=" + wID_PREDIO_SBN;

  $.ajax(url).done(function (response) {
    $("#div_tab2_contenido").html(response);
  });
}


function Mostrar_tab3_datos_valorizacion(wID_PREDIO_SBN){
	
  Listar_Documentos_Valorizacion_Cabecera(wID_PREDIO_SBN);   
  Listar_Documentos_Valorizacion(wID_PREDIO_SBN);

}

function mostrar_det_saneamiento(){
	var cbo_tipo_saneamiento	= $("#cbo_estado_saneamiento").val();
	
	$.post("View/padron_predios/cbo_estado_saneamiento_detalle.php",{ 
		cbo_tipo_saneamiento:cbo_tipo_saneamiento
	},function(data){
		$("#cbo_det_saneamiento").html(data);
	})
}

	/**************************************************************/
	/**************************************************************/
	/**************************************************************/
	
	separadorDecimalesInicial="."; //Modifique este dato para poder obtener la nomenclatura que utilizamos en mi pais
	separadorDecimales="."; //Modifique este dato para poder obtener la nomenclatura que utilizamos en mi pais
	separadorMiles=","; //Modifique este dato para poder obtener la nomenclatura que utilizamos en mi pais
	
	function arreglar(numero){ 
		var numeroo=""; 
		numero=""+numero; 
		partes=numero.split(separadorDecimalesInicial);
		entero=partes[0];
		
		if(partes.length>1){ 
			decimal=partes[1]; 
		} 
		
		cifras=entero.length; 
		cifras2=cifras 
		
		for(a=0;a<cifras;a++){
			cifras2-=1;
			numeroo+=entero.charAt(a);
			
			if(cifras2%3==0 &&cifras2!=0){
				numeroo+=separadorMiles;
			}
		} 
		
		if(partes.length>1){
			numeroo+=separadorDecimales+decimal;
		}
		
		return numeroo 
	}

	function formatodecimales(input){
		var num = input.value.replace(/\,/g,'');		
		input.value = arreglar(num);		
		if(!isNaN(num)){
			num = num.toString().split('').reverse().join('').replace(/(?=\d*\,?)(\d{3})/g,'$1,');
			num = num.split('').reverse().join('').replace(/^[\,]/,'');
			input.value = num;
		}else{ alert('Solo se permiten numeros');
			input.value = input.value.replace(/[^\d\.]*/g,'');
		}
	}

/*******************************************************/
/*******************************************************/
/*******************************************************/




function Guardar_Datos_Local_Predio() {

	
	
	//$.post("Model/M_padron_predios.php?operacion=Guardar_Local_Predio_en_INMUEBLES",{ 
	
	//********************************************************
	//************* DATOS GENERALES ************************
	//********************************************************
	
	var TXH_SIMI_COD_USUARIO 	= $("#TXH_SIMI_COD_USUARIO").val();
	var TXH_SIMI_COD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();
	
	var txh_COD_UE_LOCAL 	= $("#txh_COD_UE_LOCAL").val();
	var txt_nom_inmueble 	= $("#txt_nom_inmueble").val();
	var cbo_departamento 	= $("#cbo_departamento").val();
	var cbo_provincia 		= $("#cbo_provincia").val();
	var cbo_distrito 		= $("#cbo_distrito").val();
	var cbo_zona 			= $("#cbo_zona").val();
	var cbo_via 			= $("#cbo_via").val();
	var txt_nom_via 		= $("#txt_nom_via").val();
	var txt_nro_inmueble 	= $("#txt_nro_inmueble").val();
	var txt_manzana 		= $("#txt_manzana").val();
	var txt_lote 			= $("#txt_lote").val();
	var txt_detalle 		= $("#txt_detalle").val();	
	var txt_nro_detalle 	= $("#txt_nro_detalle").val();
	var cbo_habilitacion 	= $("#cbo_habilitacion").val();
	var txt_nom_habilitacion 	= $("#txt_nom_habilitacion").val();
	var txt_nom_sector 			= $("#txt_nom_sector").val();
	var cbo_propiedad 			= $("#cbo_propiedad").val();
	var txt_ubicado_piso_nro	= $("#txt_ubicado_piso_nro").val();
	
	if(cbo_propiedad == '2' || cbo_propiedad == '3'){
		
	//********************************************************
	//************* DATOS REGISTRALES ************************
	//********************************************************
	
	var txt_reg_cus 	= $("#txt_reg_cus").val();
	var txt_reg_sinabip 	= $("#txt_reg_sinabip").val();
	var txt_propietario_registral 	= $("#txt_propietario_registral").val();
	var cbo_oficina_registral 	= $("#cbo_oficina_registral").val();
	var txt_partida_registral 	= $("#txt_partida_registral").val();
	var txt_cod_predio 	= $("#txt_cod_predio").val();	
	var txt_area_registral_0 	= 0 //$("#txt_area_registral_0").val();
	//var txt_area_registral 	= (txt_area_registral_0.replace(/[,]/gi, "")*1);
	var txt_area_registral 	= $("#txt_area_registral").val();
	var cbo_unid_medida 	= $("#cbo_unid_medida").val();	
	var txt_tomo 	= $("#txt_tomo").val();
	var txt_asiento 	= $("#txt_asiento").val();
	var txt_fojas 	= $("#txt_fojas").val();
	var txt_ficha 	= $("#txt_ficha").val();
	var txt_anotacion_preventiva 	= $("#txt_anotacion_preventiva").val(); 
	
	
	//********************************************************
	//************* DATOS TECNICOS ************************
	//********************************************************
	
	var txt_zonficacion 		= $("#txt_zonficacion").val();
	var cbo_tipo_terreno 		= $("#cbo_tipo_terreno").val();
	var txt_perimetro_0 		= $("#txt_perimetro").val();
	var txt_perimetro 			= 0 //(txt_perimetro_0.replace(/[,]/gi, "")*1);
	var txt_area_terreno_0 		= $("#txt_area_terreno").val();
	var txt_area_terreno 		= 0 //(txt_area_terreno_0.replace(/[,]/gi, "")*1);
	var txt_area_construida_0 	= $("#txt_area_construida").val();
	var txt_area_construida 	= 0 //(txt_area_construida_0.replace(/[,]/gi, "")*1);
	var cbo_est_conservacion 	= $("#cbo_est_conservacion").val();
	var txt_nro_pisos 			= $("#txt_nro_pisos").val();
	var cbo_estado_saneamiento 	= $("#cbo_estado_saneamiento").val();
	var cbo_det_saneamiento 	= $("#cbo_det_saneamiento").val();
	var cbo_uso_predio 			= $("#cbo_uso_predio").val();
	var txt_otros_usos 			= $("#txt_otros_usos").val();
	
	}
	
	if(txt_nom_inmueble == ''){
		alert('Ingrese nombre del Local o Predio');
		$("#txt_nom_inmueble").focus();
		
	}else if(cbo_departamento == '-'){
		alert('Seleccione Departamento');
		$("#cbo_departamento").focus();
		
	}else if(cbo_provincia == '-'){
		alert('Seleccione Provincia');
		$("#cbo_provincia").focus();
		
	}else if(cbo_distrito == '-'){
		alert('Seleccione Distrito');
		$("#cbo_distrito").focus();
		
	}else if(cbo_zona == '-'){
		alert('Seleccione Zona');
		$("#cbo_zona").focus();
		
	}else if(cbo_via == '-'){
		alert('Seleccione Vía');
		$("#cbo_via").focus();
		
	}else if(txt_nom_via == ''){
		alert('Ingrese Nombre de la Vía');
		$("#txt_nom_via").focus();
		
	}else if(cbo_propiedad == '-'){
		alert('Seleccione el Tipo de Propiedad'); 
		$("#cbo_propiedad").focus();
		
	}else{
			
		if(confirm('Confirma Registrar los datos del predio y/o local?')){
			
			carga_loading('div_guardar');
			
			var url = "View/padron_predios_inmuebles/principal.php?operacion=Guardar_Datos_Predio";
			url += (TXH_SIMI_COD_USUARIO == "") ? "" : "&TXH_SIMI_COD_USUARIO=" + TXH_SIMI_COD_USUARIO;
			url += (TXH_SIMI_COD_ENTIDAD == "") ? "" : "&TXH_SIMI_COD_ENTIDAD=" + TXH_SIMI_COD_ENTIDAD;
			url += (txh_COD_UE_LOCAL == "") ? "" : "&txh_COD_UE_LOCAL=" + txh_COD_UE_LOCAL;
			url += (txt_nom_inmueble == "") ? "" : "&txt_nom_inmueble=" + txt_nom_inmueble;
			url += (cbo_departamento == "") ? "" : "&cbo_departamento=" + cbo_departamento;
			url += (cbo_provincia == "") ? "" : "&cbo_provincia=" + cbo_provincia;
			url += (cbo_distrito == "") ? "" : "&cbo_distrito=" + cbo_distrito;
			url += (cbo_zona == "") ? "" : "&cbo_zona=" + cbo_zona;
			url += (cbo_via == "") ? "" : "&cbo_via=" + cbo_via;
			url += (txt_nom_via == "") ? "" : "&txt_nom_via=" + txt_nom_via;
			url += (txt_nro_inmueble == "") ? "" : "&txt_nro_inmueble=" + txt_nro_inmueble;
			url += (txt_manzana == "") ? "" : "&txt_manzana=" + txt_manzana;
			url += (txt_lote == "") ? "" : "&txt_lote=" + txt_lote;
			url += (txt_detalle == "") ? "" : "&txt_detalle=" + txt_detalle;
			url += (txt_nro_detalle == "") ? "" : "&txt_nro_detalle=" + txt_nro_detalle;
			url += (cbo_habilitacion == "") ? "" : "&cbo_habilitacion=" + cbo_habilitacion;
			url += (txt_nom_habilitacion == "") ? "" : "&txt_nom_habilitacion=" + txt_nom_habilitacion;
			url += (txt_nom_sector == "") ? "" : "&txt_nom_sector=" + txt_nom_sector;
			url += (cbo_propiedad == "") ? "" : "&cbo_propiedad=" + cbo_propiedad;
			url += (txt_ubicado_piso_nro == "") ? "" : "&txt_ubicado_piso_nro=" + txt_ubicado_piso_nro;
			
			if(cbo_propiedad == '2' || cbo_propiedad == '3'){
				
			url += (txt_reg_cus == "") ? "" : "&txt_reg_cus=" + txt_reg_cus;
			url += (txt_reg_sinabip == "") ? "" : "&txt_reg_sinabip=" + txt_reg_sinabip;
			url += (txt_propietario_registral == "") ? "" : "&txt_propietario_registral=" + txt_propietario_registral;
			url += (cbo_oficina_registral == "") ? "" : "&cbo_oficina_registral=" + cbo_oficina_registral;
			url += (txt_partida_registral == "") ? "" : "&txt_partida_registral=" + txt_partida_registral;
			url += (txt_cod_predio == "") ? "" : "&txt_cod_predio=" + txt_cod_predio;
			url += (txt_area_registral == "") ? "" : "&txt_area_registral=" + txt_area_registral;
			url += (cbo_unid_medida == "") ? "" : "&cbo_unid_medida=" + cbo_unid_medida;
			url += (txt_tomo == "") ? "" : "&txt_tomo=" + txt_tomo;
			url += (txt_asiento == "") ? "" : "&txt_asiento=" + txt_asiento;
			url += (txt_fojas == "") ? "" : "&txt_fojas=" + txt_fojas;
			url += (txt_ficha == "") ? "" : "&txt_ficha=" + txt_ficha;
			url += (txt_anotacion_preventiva == "") ? "" : "&txt_anotacion_preventiva=" + txt_anotacion_preventiva;
			
			url += (txt_zonficacion == "") ? "" : "&txt_zonficacion=" + txt_zonficacion;
			url += (cbo_tipo_terreno == "") ? "" : "&cbo_tipo_terreno=" + cbo_tipo_terreno;
			url += (txt_perimetro == "") ? "" : "&txt_perimetro=" + txt_perimetro;
			url += (txt_area_terreno == "") ? "" : "&txt_area_terreno=" + txt_area_terreno;
			url += (txt_area_construida == "") ? "" : "&txt_area_construida=" + txt_area_construida;
			url += (cbo_est_conservacion == "") ? "" : "&cbo_est_conservacion=" + cbo_est_conservacion;
			url += (txt_nro_pisos == "") ? "" : "&txt_nro_pisos=" + txt_nro_pisos;
			url += (cbo_estado_saneamiento == "") ? "" : "&cbo_estado_saneamiento=" + cbo_estado_saneamiento;
			url += (cbo_det_saneamiento == "") ? "" : "&cbo_det_saneamiento=" + cbo_det_saneamiento;
			url += (cbo_uso_predio == "") ? "" : "&cbo_uso_predio=" + cbo_uso_predio;
			url += (txt_otros_usos == "") ? "" : "&txt_otros_usos=" + txt_otros_usos;
			
			}
			
			$.ajax({ 
				type: 'GET', 
				url: url, 
				//data: { get_param: 'value' }, 
				dataType: 'json',
				success: function (response) { 
					if(response.valor == '1'){
						$("#div_guardar").html('');
						$("#detail-formulario").html('');
						$("#div_errores").html('');
						handle_paginar_predios(1);
						$('#div_errores').hide();
					}else if(response.valor == '0'){
						$('#div_errores').hide();
						alert(response);
					}else {
						let vhtml ='';
						vhtml += '<ul>';
						$.each(response.errores, function(index, element) {
							//console.log(element);
							if(element !== undefined){
								vhtml += '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> <span class="sr-only">Error:</span>';
								vhtml += element + '<br>';
							}
						});	
						
						vhtml += '</ul>';
						$('#div_errores').html(vhtml);
						$('#div_errores').show();
					}
				}
			});
		}

	}
}


function mostrar_tipo_moneda(){
	var cbo_tipo_moneda	= $("#cbo_tipo_moneda").val();
	
	if(cbo_tipo_moneda == '-'){
		$("#div_mon_val_terreno").html('-');
		$("#div_mon_val_construccion").html('-');
		$("#div_mon_val_obra").html('-');
		$("#div_mon_val_tot_inm").html('-');
		
	}else if(cbo_tipo_moneda == '1'){	//------soles
		$("#div_mon_val_terreno").html('S/.');
		$("#div_mon_val_construccion").html('S/.');
		$("#div_mon_val_obra").html('S/.');
		$("#div_mon_val_tot_inm").html('S/.');
		
		$("#txt_tipo_cambio").prop('disabled', true).val(0);
		
	}else if(cbo_tipo_moneda == '2'){	//------dolar
		$("#div_mon_val_terreno").html('$.');
		$("#div_mon_val_construccion").html('$.');
		$("#div_mon_val_obra").html('$.');
		$("#div_mon_val_tot_inm").html('$.');
		
		$("#txt_tipo_cambio").prop('disabled', false);
	}
	
	calcular_monto_total();
	
}

function calcular_monto_total(){
	
	var txt_valor_terreno = parseFloat(1*$("#txt_valor_terreno").val());
	var txt_valor_construccion = parseFloat(1*$("#txt_valor_construccion").val());
	var txt_valor_obra = parseFloat(1*$("#txt_valor_obra").val());

	var sumatoria_val_total = parseFloat((txt_valor_terreno+txt_valor_construccion+txt_valor_obra)*1);
	$("#txt_valor_total_inmueble").val(sumatoria_val_total);
	
	var cbo_tipo_moneda = $("#cbo_tipo_moneda").val();

	if(cbo_tipo_moneda == '2'){ // dolar
		var txt_tipo_cambio 		= parseFloat(1*$("#txt_tipo_cambio").val());
		var val_tot_mueble_soles	= parseFloat(sumatoria_val_total*txt_tipo_cambio);		
	}else{
		var txt_tipo_cambio 		= parseFloat(0);
		var val_tot_mueble_soles	= parseFloat(sumatoria_val_total);
	}
	
	$("#txt_valor_total_inmueble_soles").val(val_tot_mueble_soles);
}


function Listar_Documentos_Valorizacion_Cabecera(sID_PREDIO_SBN){
	
	carga_loading('div_cab_valorizacion');
	
	var url = "View/padron_predios_inmuebles/principal.php?operacion=Listar_Cab_Valorizacion";
	url += (sID_PREDIO_SBN == "") ? "" : "&sID_PREDIO_SBN=" + sID_PREDIO_SBN;
		
	$.ajax(url).done(function (data) {
		$("#div_cab_valorizacion").html(data);		
	});
	
}

function Listar_Documentos_Valorizacion(sID_PREDIO_SBN){
	
	carga_loading('div_list_valorizacion'); 
	 
	var url = "View/padron_predios_inmuebles/principal.php?operacion=Listar_Det_Valorizacion";
	url += (sID_PREDIO_SBN == "") ? "" : "&sID_PREDIO_SBN=" + sID_PREDIO_SBN;
		
	$.ajax(url).done(function (data) {
		$("#div_list_valorizacion").html(data);		
	});
	
}





function Eliminar_Valorizacion(E1_COD_VAL_PREDIO, E1_COD_PREDIO_DOC_TECN){
	
	var sID_PREDIO_SBN 	= $("#txh_COD_UE_LOCAL").val();
	
	if(confirm('Confirma Eliminar esta Valorización')){
	
		var url = "View/padron_predios_inmuebles/principal.php?operacion=Eliminar_Valorizacion_x_Codigo";
		
		url += (sID_PREDIO_SBN == "") ? "" : "&sID_PREDIO_SBN=" + sID_PREDIO_SBN;
		url += (E1_COD_VAL_PREDIO == "") ? "" : "&E1_COD_VAL_PREDIO=" + E1_COD_VAL_PREDIO;
		url += (E1_COD_PREDIO_DOC_TECN == "") ? "" : "&E1_COD_PREDIO_DOC_TECN=" + E1_COD_PREDIO_DOC_TECN;
			
		$.ajax(url).done(function (data) {
			if(data == '1'){
				$("#div_dialog_form_reg_valorizacion").dialog("destroy");
				document.getElementById('div_cab_valorizacion').innerHTML = '';
				Listar_Documentos_Valorizacion(sID_PREDIO_SBN);	
			}
		});
		
	}
	
}


/*************************************************************/


function Agregar_Unid_Inmobiliaria(){
	
	var txh_COD_UE_LOCAL = $("#txh_COD_UE_LOCAL").val();
	var TXT_COD_UNID_INMOBIL = $("#TXT_COD_UNID_INMOBIL").val();
	var TXH_SIMI_COD_USUARIO = $("#TXH_SIMI_COD_USUARIO").val();
	
	var TXT_NOM_UNID_INMOBIL = $("#TXT_NOM_UNID_INMOBIL").val();
	var TXT_AREA_UNID_INMOBIL = $("#TXT_AREA_UNID_INMOBIL").val();
	var TXT_AUTOVALUO_UNID_INMOBIL = $("#TXT_AUTOVALUO_UNID_INMOBIL").val();
	var TXT_OBS_UNID_INMOBIL = $("#TXT_OBS_UNID_INMOBIL").val();
	
	if(TXT_NOM_UNID_INMOBIL == ''){
		alert('Ingrese Nombre de la unidad del inmueble');
		$("#TXT_NOM_UNID_INMOBIL").focus();
	
	}else if(TXT_AREA_UNID_INMOBIL == ''){
		alert('Ingrese Área de la unidad del inmueble');
		$("#TXT_AREA_UNID_INMOBIL").focus();
		
	}else if(TXT_AUTOVALUO_UNID_INMOBIL == ''){
		alert('Ingrese Valor de autovalúo');
		$("#TXT_AUTOVALUO_UNID_INMOBIL").focus();
		
	}else{
	
		var url = "View/padron_predios_inmuebles/principal.php?operacion=Guardar_Datos_Unid_Inmobiliaria";
		url += (txh_COD_UE_LOCAL == "") ? "" : "&txh_COD_UE_LOCAL=" + txh_COD_UE_LOCAL;
		url += (TXT_COD_UNID_INMOBIL == "") ? "" : "&TXT_COD_UNID_INMOBIL=" + TXT_COD_UNID_INMOBIL;
		url += (TXH_SIMI_COD_USUARIO == "") ? "" : "&TXH_SIMI_COD_USUARIO=" + TXH_SIMI_COD_USUARIO;
		
		url += (TXT_NOM_UNID_INMOBIL == "") ? "" : "&TXT_NOM_UNID_INMOBIL=" + TXT_NOM_UNID_INMOBIL;
		url += (TXT_AREA_UNID_INMOBIL == "") ? "" : "&TXT_AREA_UNID_INMOBIL=" + TXT_AREA_UNID_INMOBIL;
		url += (TXT_AUTOVALUO_UNID_INMOBIL == "") ? "" : "&TXT_AUTOVALUO_UNID_INMOBIL=" + TXT_AUTOVALUO_UNID_INMOBIL;
		url += (TXT_OBS_UNID_INMOBIL == "") ? "" : "&TXT_OBS_UNID_INMOBIL=" + TXT_OBS_UNID_INMOBIL;
			
		$.ajax(url).done(function (data) {
			
			var valor 				= data.split("[*]")[0];
			var wwwID_PREDIO_SBN 	= data.split("[*]")[1];
			
			if(valor == '1'){
				$("#div_dialog_form_reg_unidinmobiliaria").dialog("destroy");
				document.getElementById('div_cab_unidinmobiliaria').innerHTML = '';
				Lista_Registros_Unid_Inmobiliaria(wwwID_PREDIO_SBN);
			}else{
				alert(data);
			}
					
		});
	}
}


function Lista_Registros_Unid_Inmobiliaria(sID_PREDIO_SBN){
	
	carga_loading('div_det_unidinmobiliaria'); 
	
	var url = "View/padron_predios_inmuebles/principal.php?operacion=Lista_Registros_Unid_Inmobiliaria";
	url += (sID_PREDIO_SBN == "") ? "" : "&sID_PREDIO_SBN=" + sID_PREDIO_SBN;
	$.ajax(url).done(function (data) {
		$("#div_det_unidinmobiliaria").html(data);		
	});
}




/***********************************************************************************************/
/***********************************************************************************************/
/***********************************************************************************************/


function Mostrar_Dialog_Reg_Documentos(sCOD_TIP_DOC_PRED, sCOD_PREDIO_DOC_TECN){
	$("#div_dialog_form_reg_documentos").dialog({
			//dialogClass: "no-close",
			title: 'Registro de la Documentación',
			width: 550,
			height: 500,
			resizable: "false",
			modal: true,
			open: function() {
				Mostrar_Contenido_Dialog(sCOD_TIP_DOC_PRED, sCOD_PREDIO_DOC_TECN);
			},
			close: function() {
				$(this).dialog("destroy");
				document.getElementById('Div_contenido_dialog').innerHTML = '';
			}
	});
}


function Mostrar_Contenido_Dialog(sCOD_TIP_DOC_PRED, sCOD_PREDIO_DOC_TECN){
	
	var txh_COD_UE_LOCAL = $("#txh_COD_UE_LOCAL").val();
	
	var url = "View/padron_predios_inmuebles/principal.php?operacion=Mostrar_Form_Documento";
	url += (txh_COD_UE_LOCAL == "") ? "" : "&txh_COD_UE_LOCAL=" + txh_COD_UE_LOCAL;
	url += (sCOD_TIP_DOC_PRED == "") ? "" : "&sCOD_TIP_DOC_PRED=" + sCOD_TIP_DOC_PRED;
	url += (sCOD_PREDIO_DOC_TECN == "") ? "" : "&sCOD_PREDIO_DOC_TECN=" + sCOD_PREDIO_DOC_TECN;
		
	$.ajax(url).done(function (data) {
		$("#Div_contenido_dialog").html(data);		
	});
}

function Listar_Registro_Documentacion(sID_PREDIO_SBN){
	
	carga_loading('div_det_reg_documentacion'); 
	var url = "View/padron_predios_inmuebles/principal.php?operacion=Listar_Registro_Documentos_registrados";
	url += (sID_PREDIO_SBN == "") ? "" : "&sID_PREDIO_SBN=" + sID_PREDIO_SBN;
		
	$.ajax(url).done(function (data) {
		$("#div_det_reg_documentacion").html(data);		
	});
	
}

function Eliminar_Documento(){
	
	var txh_COD_UE_LOCAL 			= $("#txh_COD_UE_LOCAL").val();
	var txtH_COD_PREDIO_DOC_TECN 	= $("#txtH_COD_PREDIO_DOC_TECN").val();
	var TXH_SIMI_COD_USUARIO 		= $("#TXH_SIMI_COD_USUARIO").val();
	
	var url = "View/padron_predios_inmuebles/principal.php?operacion=Eliminar_Documento_Tecnico";
	url += (txtH_COD_PREDIO_DOC_TECN == "") ? "" : "&txtH_COD_PREDIO_DOC_TECN=" + txtH_COD_PREDIO_DOC_TECN;
	url += (TXH_SIMI_COD_USUARIO == "") ? "" : "&TXH_SIMI_COD_USUARIO=" + TXH_SIMI_COD_USUARIO;
		
	$.ajax(url).done(function (data) {
		if(data == '1'){
			
			$("#div_dialog_form_reg_documentos").dialog("destroy");
			document.getElementById('Div_contenido_dialog').innerHTML = '';
			Listar_Registro_Documentacion(txh_COD_UE_LOCAL);
				
		}else{
			alert(data)
		}
	});
	
}


function Eliminar_REG_INMOBILI(sCOD_UNID_INMOBIL){
	
	var sID_PREDIO_SBN 				= $("#txh_COD_UE_LOCAL").val();
	
	if(confirm('Confirma Eliminar este Registro Inmobiliario')){
			
		var url = "View/padron_predios_inmuebles/principal.php?operacion=Eliminar_Reg_Inmobiliaria";
		url += (sCOD_UNID_INMOBIL == "") ? "" : "&sCOD_UNID_INMOBIL=" + sCOD_UNID_INMOBIL;
			
		$.ajax(url).done(function (data) {
			if(data == '1'){			
				$("#div_dialog_form_reg_unidinmobiliaria").dialog("destroy");
				document.getElementById('div_cab_unidinmobiliaria').innerHTML = '';
				Lista_Registros_Unid_Inmobiliaria(sID_PREDIO_SBN);
					
			}else{
				alert(data)
			}
		});
	}
	
}

/***********************************************************************************************/
/***********************************************************************************************/
/***********************************************************************************************/


function Mostrar_Form_Reg_Inmobiliario(sCOD_UNID_INMOBIL){
		
	$("#div_dialog_form_reg_unidinmobiliaria").dialog({
			//dialogClass: "no-close",
			title: 'Registro de Unidades Inmobiliarias del Predio',
			width: 670,
			height: 260,
			resizable: "false",
			modal: true,
			open: function() {
				Mostrar_Contenido_Dialog_Reg_Inmobiliario(sCOD_UNID_INMOBIL);
			},
			close: function() {
				$(this).dialog("destroy");
				document.getElementById('div_cab_unidinmobiliaria').innerHTML = '';
			}
	});
}



function Mostrar_Contenido_Dialog_Reg_Inmobiliario(sCOD_UNID_INMOBIL){
	
	carga_loading('div_cab_unidinmobiliaria'); 
	var url = "View/padron_predios_inmuebles/principal.php?operacion=Ver_Datos_Unidad_Inmobiliaria_x_Codigo";
	url += (sCOD_UNID_INMOBIL == "") ? "" : "&sCOD_UNID_INMOBIL=" + sCOD_UNID_INMOBIL;
		
	$.ajax(url).done(function (data) {
		$("#div_cab_unidinmobiliaria").html(data);		
	});
}




/**************************************************************************************/


function Mostrar_Form_Valorizacion(eCOD_VAL_PREDIO){
		
	$("#div_dialog_form_reg_valorizacion").dialog({
			//dialogClass: "no-close",
			title: 'Registro de Valorizacion del Predio',
			width: 790,
			height: 350,
			resizable: "false",
			modal: true,
			open: function() {
				Mostrar_Contenido_Dialog_Valorizacion(eCOD_VAL_PREDIO);
			},
			close: function() {
				$(this).dialog("destroy");
				document.getElementById('div_cab_valorizacion').innerHTML = '';
			}
	});
}


function Mostrar_Contenido_Dialog_Valorizacion(eCOD_VAL_PREDIO){
	
	carga_loading('div_cab_valorizacion'); 
	var url = "View/padron_predios_inmuebles/principal.php?operacion=Ver_Datos_Valorizacion_x_Codigo";
	url += (eCOD_VAL_PREDIO == "") ? "" : "&eCOD_VAL_PREDIO=" + eCOD_VAL_PREDIO;
		
	$.ajax(url).done(function (data) {
		$("#div_cab_valorizacion").html(data);		
	});
	
}

/**************************************************************************/


function Dar_Baja(){
	
	var TXH_SIMI_COD_USUARIO 	= $("#TXH_SIMI_COD_USUARIO").val();
	var TXH_SIMI_COD_ENTIDAD 	= $("#TXH_SIMI_COD_ENTIDAD").val();	
	var txh_COD_PREDIO_LOCAL 	= $("#txh_COD_UE_LOCAL").val();
		
	if(confirm('Confirma dar de baja el predio y/o local')){		
		
		var url = "View/padron_predios_inmuebles/principal.php?operacion=Dar_Baja_predio";
		url += (TXH_SIMI_COD_USUARIO == "") ? "" : "&TXH_SIMI_COD_USUARIO=" + TXH_SIMI_COD_USUARIO;
		url += (TXH_SIMI_COD_ENTIDAD == "") ? "" : "&TXH_SIMI_COD_ENTIDAD=" + TXH_SIMI_COD_ENTIDAD;
		url += (txh_COD_PREDIO_LOCAL == "") ? "" : "&txh_COD_PREDIO_LOCAL=" + txh_COD_PREDIO_LOCAL;
		
		carga_loading('detail-formulario');
		
		$.ajax(url).done(function (valor) {
			if(valor == 1){
				$("#detail-formulario").html('');	
				handle_paginar_predios(1);							
			}else{
				alert(valor);
			}
		});		
	}
	
	
}