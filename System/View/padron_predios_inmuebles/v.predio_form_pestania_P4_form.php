<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 

<style type="text/css">
    .messages{
        float: left;
        font-family: sans-serif;
        display: none;
    }
    .info{
        padding: 5px;
        border-radius: 5px;
        background: #5693B4;/*fondo azul MARINO AL SELECCIONAR EL ARCHIVO*/
        color: #FFFFFF;
        font-size: 11px;
        text-align: center;
    }
    .before{
        padding: 8px;
        border-radius: 8px;
        background: #1ABC9C;/*VERDE - CUANDO VA CARGANDO EL FILE*/
        color: #fff;
        font-size: 13px;
        text-align: center;
    }
    .success{
        padding: 8px;
        border-radius: 8px;
        background: #E2F2FF;/*cuando finaliza*/
        color: #A12A2A;
        font-size: 15px;
        text-align: center;
		font-weight:bold;
    }
    .error{
        padding: 8px;
        border-radius: 8px;
        background: red;
        color: #fff;
        font-size: 13px;
        text-align: center;
    }
</style>

<?php

$Datos_eDoc = $data['Dat_eDoc'];
$wID_PREDIO_SBN = $Datos_eDoc['D_ID_PREDIO'];
$wCOD_TIP_DOC = $Datos_eDoc['D_COD_TIP_DOC_PRED'];
$wCOD_PREDIO_DOC_TECN = $Datos_eDoc['E_COD_PREDIO_DOC_TECN'];

$List_DisposicionLegal = $data['DatTipDisposicionLegal'];
//$this->dump($List_DisposicionLegal);

?>


<form enctype="multipart/form-data" class="formulario_doc_reg" >

<input name="txtH_wID_PREDIO_SBN" type="hidden" id="txtH_wID_PREDIO_SBN" style="width:100px" value="<?php print $wID_PREDIO_SBN ?>" />
<input name="txtH_COD_TIP_DOC" type="hidden" id="txtH_COD_TIP_DOC" style="width:100px" value="<?php print $wCOD_TIP_DOC ?>" />
<input name="txtH_COD_PREDIO_DOC_TECN" type="hidden" id="txtH_COD_PREDIO_DOC_TECN" style="width:100px" value="<?php print $wCOD_PREDIO_DOC_TECN ?>" />
<input name="WWW_SIMI_COD_USUARIO" type="hidden" id="WWW_SIMI_COD_USUARIO" style="width:100px" />
<input name="WWW_SIMI_COD_ENTIDAD" type="hidden" id="WWW_SIMI_COD_ENTIDAD" style="width:100px" />



<?php 

//========================================================================
//========================================================================
// INICIO - Partida registral
//========================================================================
//========================================================================

if($wCOD_TIP_DOC == '5'){
?>

<script>
$(document).ready(function(){

	$(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_reg")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		
		
		if(fileExtension == 'PDF' || fileExtension == 'pdf'){
			$(".div_nom_archivo_part_reg").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_part_reg").val('1');
		}else{
			$("#txf_partida_reg").val('');				
			$(".div_nom_archivo_part_reg").html('');
			alert('Error: Solo se permiten estos archivos .pdf');
			return false;
		}
		
    });

    //al enviar el formulario
    $("#btn_guardar_partida").click(function(){
		
		var txtH_COD_TIP_DOC			= $("#txtH_COD_TIP_DOC").val();
		var txtH_COD_PREDIO_DOC_TECN	= $("#txtH_COD_PREDIO_DOC_TECN").val();
		var txtH_wID_PREDIO_SBN			= $("#txtH_wID_PREDIO_SBN").val();
		
		var txt_NUM_PART_REG 			= $("#txt_NUM_PART_REG").val();
		var txt_indicador_part_reg		= $("#txt_indicador_part_reg").val();
		
		var TXH_SIMI_COD_USUARIO		= $("#TXH_SIMI_COD_USUARIO").val();
		$("#WWW_SIMI_COD_USUARIO").val(TXH_SIMI_COD_USUARIO);
		
		var TXH_SIMI_COD_ENTIDAD		= $("#TXH_SIMI_COD_ENTIDAD").val();
		$("#WWW_SIMI_COD_ENTIDAD").val(TXH_SIMI_COD_ENTIDAD);
		
		if(txt_NUM_PART_REG == ''){
			alert('Ingrese Numero de la Partida Registral');
		 	$("#txt_NUM_PART_REG").focus();
		 
		}else if(txt_NUM_PART_REG == '' && txt_indicador_part_reg == '' ){
			alert('Adjunte el Archivo de la partida Registral PDF');
		 
		}else{
				
			var formData = new FormData($(".formulario_doc_reg")[0]);
			var message = ""; 
			//hacemos la petición ajax  
			$.ajax({
				url: 'View/padron_predios_inmuebles/principal.php?operacion=Registrar_Predio_Documentos',
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,			
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
					message = $("<span class='before'>Subiendo el archivo, por favor espere...</span>");
					showMessage(message)        
				},
				//una vez finalizado correctamente
				success: function(data){
					//alert(data);
					
			
						
						var valor 				= data.split("[*]")[0];
						var codigo 				= data.split("[*]")[1];
						var nom_archivo 		= data.split("[*]")[2];			
						var nombre_carpeta 		= data.split("[*]")[3];
						var gID_PREDIO_SBN 		= data.split("[*]")[4];
						
						if(valor==1){
							
							
							$("#txtH_COD_PREDIO_DOC_TECN").val(codigo);
							$("#txh_COD_UE_LOCAL").val(gID_PREDIO_SBN);
							
							alert('Se Guardo Correctamente');
							
							$("#div_dialog_form_reg_documentos").dialog("destroy");
							document.getElementById('Div_contenido_dialog').innerHTML = '';
							
							Listar_Registro_Documentacion(gID_PREDIO_SBN);
							
						}else{
							message = $("<span class='error'>Ha ocurrido un error: "+data+"</span>");
							showMessage(message);
						}
				
				},
				//si ha ocurrido un error
				error: function(){
					message = $("<span class='error'>Ha ocurrido un error.</span>");
					showMessage(message);
				}
			});
			
		}
		
    });
})

//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}


</script>


<table width="519" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" style="background-color:#f4f4f4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><span class="texto_arial_plomito_11_N">

    </span></td>
    </tr>
  <tr id="tr_partida_registral_x1">
    <td width="13" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Tipo de Documento</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td><input name="txt_t5_NOM_TIP_DOC" type="text" id="txt_t5_NOM_TIP_DOC" style="width:300px" value="<?php print $Datos_eDoc['D_NOM_TIP_DOC'] ?>" readonly="readonly" /></td>
  </tr>
  <tr id="tr_descripcion_foto">
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Número de Partida</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td width="323"><input name="txt_NUM_PART_REG" type="text" id="txt_NUM_PART_REG" style="width:120px" value="<?php print $Datos_eDoc['E_NRO_PART_REG'] ?>" /></td>
  </tr>
  <tr id="tr_descripcion_gral">
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="152" height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar Archivo (PDF)   </td>
    <td width="19" valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_reg" type="file" id="txf_partida_reg"  /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N">
    <div class="div_nom_archivo_part_reg"></div>
      <input name="txt_indicador_part_reg" type="hidden" id="txt_indicador_part_reg" style="width:120px" />
      <input name="txt_w_nom_archivo_part_reg" type="hidden" id="txt_w_nom_archivo_part_reg" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO'] ?>" />
      <input name="txt_w_peso_archivo_part_reg" type="hidden" id="txt_w_peso_archivo_part_reg" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO'] ?>" /></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><hr class="hr_01"></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N"><table width="482" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="89"><input name="btn_guardar_partida" type="button" class="btn btn-primary" id="btn_guardar_partida" value="Guardar" /></td>
        <td width="91"><input name="btn_guardar_partida2" type="button" class="btn btn-Rojo" id="btn_guardar_partida2" value="Eliminar" onclick="Eliminar_Documento()" /></td>
        <td width="290"><div class="messages"></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
  </tr>
</table>

<? } ?>




<?php 

//========================================================================
//========================================================================
// INICIO - Plano Perimetrico
//========================================================================
//========================================================================

if($wCOD_TIP_DOC == '6'){
?>



<script>
$(document).ready(function(){

	$(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
	
	
    //$(':file').change(function(){
	$("#txf_partida_pperimetrico_pdf").change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_pperimetrico_pdf")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		
		
		if(fileExtension == 'PDF' || fileExtension == 'pdf'){
			$(".div_nom_archivo_pperimetrico_pdf").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_pperimetrico_pdf").val('1');
		}else{
			$("#txf_partida_pperimetrico_pdf").val('');				
			$(".div_nom_archivo_pperimetrico_pdf").html('');
			alert('Error: Solo se permiten estos archivos .pdf');
			return false;
		}
		
    });
	
	
	
	
	$("#txf_partida_pperimetrico_cad").change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_pperimetrico_cad")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		
		
		if(fileExtension == 'dwg' || fileExtension == 'DWG' || fileExtension == 'dxf' || fileExtension == 'DXF'){
			$(".div_nom_archivo_pperimetrico_cad").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_pperimetrico_cad").val('1');
		}else{
			$("#txf_partida_pperimetrico_cad").val('');				
			$(".div_nom_archivo_pperimetrico_cad").html('');
			alert('Error: Solo se permiten estos archivos .dwg | .dxf');
			return false;
		}
		
    });


    //al enviar el formulario
    $("#btn_guardar_plano_perimetrico").click(function(){
		
		var txtH_COD_TIP_DOC			= $("#txtH_COD_TIP_DOC").val();
		var txtH_COD_PREDIO_DOC_TECN	= $("#txtH_COD_PREDIO_DOC_TECN").val();
		var txtH_wID_PREDIO_SBN			= $("#txtH_wID_PREDIO_SBN").val();
		
		var txt_NUM_DESCRIP_PPERMETRICO 		= $("#txt_NUM_DESCRIP_PPERMETRICO").val();
		var txt_indicador_pperimetrico_pdf		= $("#txt_indicador_pperimetrico_pdf").val();
		var txt_indicador_pperimetrico_cad		= $("#txt_indicador_pperimetrico_cad").val();
		
		//var txt_w_nom_archivo_pperimetric_pdf		= $("#txt_w_nom_archivo_pperimetric_pdf").val();
		//var txt_w_nom_archivo_pperimetric_cad		= $("#txt_w_nom_archivo_pperimetric_cad").val();
		
		var TXH_SIMI_COD_USUARIO		= $("#TXH_SIMI_COD_USUARIO").val();
		$("#WWW_SIMI_COD_USUARIO").val(TXH_SIMI_COD_USUARIO);
		
		var TXH_SIMI_COD_ENTIDAD		= $("#TXH_SIMI_COD_ENTIDAD").val();
		$("#WWW_SIMI_COD_ENTIDAD").val(TXH_SIMI_COD_ENTIDAD);
		
		if(txt_NUM_DESCRIP_PPERMETRICO == ''){
			alert('Ingrese Numero y/o Descripción de Plano Perimetrico');
		 	$("#txt_NUM_DESCRIP_PPERMETRICO").focus();
		 
		}else if(txtH_COD_PREDIO_DOC_TECN == '' && txt_NUM_DESCRIP_PPERMETRICO != '' && txt_indicador_pperimetrico_pdf == '' && txt_indicador_pperimetrico_cad == '' ){
			alert('Adjunte por lo menos un archivo del plano perimetrico');
		 
		}else{
				
			var formData = new FormData($(".formulario_doc_reg")[0]);
			var message = ""; 
			//hacemos la petición ajax  
			$.ajax({
				url: 'View/padron_predios_inmuebles/principal.php?operacion=Registrar_Predio_Documentos',
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,			
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
					message = $("<span class='before'>Subiendo el archivo, por favor espere...</span>");
					showMessage(message)        
				},
				//una vez finalizado correctamente
				success: function(data){

						
						var valor 				= data.split("[*]")[0];
						var codigo 				= data.split("[*]")[1];
						var nom_archivo 		= data.split("[*]")[2];			
						var nombre_carpeta 		= data.split("[*]")[3];
						var gID_PREDIO_SBN 		= data.split("[*]")[4];
						
						if(valor==1){
							
							$("#txtH_COD_PREDIO_DOC_TECN").val(codigo);
							$("#txh_COD_UE_LOCAL").val(gID_PREDIO_SBN);
							
							alert('Se Guardo Correctamente');
							
							$("#div_dialog_form_reg_documentos").dialog("destroy");
							document.getElementById('Div_contenido_dialog').innerHTML = '';
							
							Listar_Registro_Documentacion(gID_PREDIO_SBN);
							
						}else{
							message = $("<span class='error'>Ha ocurrido un error: "+data+"</span>");
							showMessage(message);
						}
			
				},
				//si ha ocurrido un error
				error: function(){
					message = $("<span class='error'>Ha ocurrido un error.</span>");
					showMessage(message);
				}
			});
			
		}
		
    });
})

//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}


</script>


<table width="519" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" style="background-color:#f4f4f4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><span class="texto_arial_plomito_11_N">

    </span></td>
    </tr>
  <tr id="tr_partida_registral_x1">
    <td width="13" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Tipo de Documento</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td><input name="txt_t5_NOM_TIP_DOC" type="text" id="txt_t5_NOM_TIP_DOC" style="width:300px" value="<?php print $Datos_eDoc['D_NOM_TIP_DOC'] ?>" readonly="readonly" /></td>
  </tr>
  <tr id="tr_descripcion_foto">
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N"> Número del Plano</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td width="323"><input name="txt_NUM_DESCRIP_PPERMETRICO" type="text" id="txt_NUM_DESCRIP_PPERMETRICO" style="width:220px" value="<?php print $Datos_eDoc['E_DESCRIP_ARCHIVO'] ?>" /></td>
  </tr>
  <tr id="tr_descripcion_gral">
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="152" height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar Archivo (PDF)   </td>
    <td width="19" valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_pperimetrico_pdf" type="file" id="txf_partida_pperimetrico_pdf"  /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top"><div class="div_nom_archivo_pperimetrico_pdf"></div>
      <input name="txt_indicador_pperimetrico_pdf" type="hidden" id="txt_indicador_pperimetrico_pdf" style="width:120px" />
      <input name="txt_w_nom_archivo_pperimetric_pdf" type="hidden" id="txt_w_nom_archivo_pperimetric_pdf" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO'] ?>" />
      <input name="txt_w_peso_archivo_pperimetric_pdf" type="hidden" id="txt_w_peso_archivo_pperimetric_pdf" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO'] ?>" />
      </td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar (.dwg | .dxf)</td>
    <td valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_pperimetrico_cad" type="file" id="txf_partida_pperimetrico_cad"  /></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3">
    <div class="div_nom_archivo_pperimetrico_cad"></div>
    <input name="txt_indicador_pperimetrico_cad" type="hidden" id="txt_indicador_pperimetrico_cad" style="width:120px" />
      <input name="txt_w_nom_archivo_pperimetric_cad" type="hidden" id="txt_w_nom_archivo_pperimetric_cad" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO_CAD'] ?>" />
      <input name="txt_w_peso_archivo_pperimetric_cad" type="hidden" id="txt_w_peso_archivo_pperimetric_cad" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO_CAD'] ?>" />
    </td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><hr class="hr_01"></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N"><table width="482" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="89"><input name="btn_guardar_plano_perimetrico" type="button" class="btn btn-primary" id="btn_guardar_plano_perimetrico" value="Guardar" /></td>
        <td width="91"><input name="btn_guardar_partida2" type="button" class="btn btn-Rojo" id="btn_guardar_partida2" value="Eliminar" onclick="Eliminar_Documento()" /></td>
        <td width="290"><div class="messages"></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
  </tr>
</table>

<?php
}
?>




<?php 

//========================================================================
//========================================================================
// INICIO - Plano de Ubicacion
//========================================================================
//========================================================================

if($wCOD_TIP_DOC == '7'){
?>



<script>
$(document).ready(function(){

	$(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
	
	
    //$(':file').change(function(){
	$("#txf_partida_pperimetrico_pdf").change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_pperimetrico_pdf")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		
		
		if(fileExtension == 'PDF' || fileExtension == 'pdf'){
			$(".div_nom_archivo_pperimetrico_pdf").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_pperimetrico_pdf").val('1');
		}else{
			$("#txf_partida_pperimetrico_pdf").val('');				
			$(".div_nom_archivo_pperimetrico_pdf").html('');
			alert('Error: Solo se permiten estos archivos .pdf');
			return false;
		}
		
    });
	
	
	
	
	$("#txf_partida_pperimetrico_cad").change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_pperimetrico_cad")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		
		
		if(fileExtension == 'dwg' || fileExtension == 'DWG' || fileExtension == 'dxf' || fileExtension == 'DXF'){
			$(".div_nom_archivo_pperimetrico_cad").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_pperimetrico_cad").val('1');
		}else{
			$("#txf_partida_pperimetrico_cad").val('');				
			$(".div_nom_archivo_pperimetrico_cad").html('');
			alert('Error: Solo se permiten estos archivos .dwg | .dxf');
			return false;
		}
		
    });


    //al enviar el formulario
    $("#btn_guardar_plano_perimetrico").click(function(){
		
		var txtH_COD_TIP_DOC			= $("#txtH_COD_TIP_DOC").val();
		var txtH_COD_PREDIO_DOC_TECN	= $("#txtH_COD_PREDIO_DOC_TECN").val();
		var txtH_wID_PREDIO_SBN			= $("#txtH_wID_PREDIO_SBN").val();
		
		var txt_NUM_DESCRIP_PPERMETRICO 		= $("#txt_NUM_DESCRIP_PPERMETRICO").val();
		var txt_indicador_pperimetrico_pdf		= $("#txt_indicador_pperimetrico_pdf").val();
		var txt_indicador_pperimetrico_cad		= $("#txt_indicador_pperimetrico_cad").val();
		
		//var txt_w_nom_archivo_pperimetric_pdf		= $("#txt_w_nom_archivo_pperimetric_pdf").val();
		//var txt_w_nom_archivo_pperimetric_cad		= $("#txt_w_nom_archivo_pperimetric_cad").val();
		
		var TXH_SIMI_COD_USUARIO		= $("#TXH_SIMI_COD_USUARIO").val();
		$("#WWW_SIMI_COD_USUARIO").val(TXH_SIMI_COD_USUARIO);
		
		var TXH_SIMI_COD_ENTIDAD		= $("#TXH_SIMI_COD_ENTIDAD").val();
		$("#WWW_SIMI_COD_ENTIDAD").val(TXH_SIMI_COD_ENTIDAD);
		
		if(txt_NUM_DESCRIP_PPERMETRICO == ''){
			alert('Ingrese Numero y/o Descripción de Plano de Ubicación');
		 	$("#txt_NUM_DESCRIP_PPERMETRICO").focus();
		 
		}else if(txtH_COD_PREDIO_DOC_TECN == '' && txt_NUM_DESCRIP_PPERMETRICO != '' && txt_indicador_pperimetrico_pdf == '' && txt_indicador_pperimetrico_cad == '' ){
			alert('Adjunte por lo menos un archivo del plano de Ubicacións');
		 
		}else{
				
			var formData = new FormData($(".formulario_doc_reg")[0]);
			var message = ""; 
			//hacemos la petición ajax  
			$.ajax({
				url: 'View/padron_predios_inmuebles/principal.php?operacion=Registrar_Predio_Documentos',
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,			
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
					message = $("<span class='before'>Subiendo el archivo, por favor espere...</span>");
					showMessage(message)        
				},
				//una vez finalizado correctamente
				success: function(data){

						//alert(data);
						
						var valor 				= data.split("[*]")[0];
						var codigo 				= data.split("[*]")[1];
						var nom_archivo 		= data.split("[*]")[2];			
						var nombre_carpeta 		= data.split("[*]")[3];
						var gID_PREDIO_SBN 		= data.split("[*]")[4];
						
						if(valor==1){
							
							$("#txtH_COD_PREDIO_DOC_TECN").val(codigo);
							$("#txh_COD_UE_LOCAL").val(gID_PREDIO_SBN);
							
							alert('Se Guardo Correctamente');
							
							$("#div_dialog_form_reg_documentos").dialog("destroy");
							document.getElementById('Div_contenido_dialog').innerHTML = '';
							
							Listar_Registro_Documentacion(gID_PREDIO_SBN);
							
						}else{
							message = $("<span class='error'>Ha ocurrido un error: "+data+"</span>");
							showMessage(message);
						}
			
				},
				//si ha ocurrido un error
				error: function(){
					message = $("<span class='error'>Ha ocurrido un error.</span>");
					showMessage(message);
				}
			});
			
		}
		
    });
})

//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}


</script>


<table width="519" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" style="background-color:#f4f4f4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><span class="texto_arial_plomito_11_N">

    </span></td>
    </tr>
  <tr id="tr_partida_registral_x1">
    <td width="13" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Tipo de Documento</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td><input name="txt_t5_NOM_TIP_DOC" type="text" id="txt_t5_NOM_TIP_DOC" style="width:300px" value="<?php print $Datos_eDoc['D_NOM_TIP_DOC'] ?>" readonly="readonly" /></td>
  </tr>
  <tr id="tr_descripcion_foto">
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N"> Número del Plano</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td width="323"><input name="txt_NUM_DESCRIP_PPERMETRICO" type="text" id="txt_NUM_DESCRIP_PPERMETRICO" style="width:220px" value="<?php print $Datos_eDoc['E_DESCRIP_ARCHIVO'] ?>" /></td>
  </tr>
  <tr id="tr_descripcion_gral">
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="152" height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar Archivo (PDF)   </td>
    <td width="19" valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_pperimetrico_pdf" type="file" id="txf_partida_pperimetrico_pdf"  /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top"><div class="div_nom_archivo_pperimetrico_pdf"></div>
      <input name="txt_indicador_pperimetrico_pdf" type="hidden" id="txt_indicador_pperimetrico_pdf" style="width:120px" />
      <input name="txt_w_nom_archivo_pperimetric_pdf" type="hidden" id="txt_w_nom_archivo_pperimetric_pdf" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO'] ?>" />
      <input name="txt_w_peso_archivo_pperimetric_pdf" type="hidden" id="txt_w_peso_archivo_pperimetric_pdf" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO'] ?>" />
      </td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar (.dwg | .dxf)</td>
    <td valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_pperimetrico_cad" type="file" id="txf_partida_pperimetrico_cad"  /></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3">
    <div class="div_nom_archivo_pperimetrico_cad"></div>
    <input name="txt_indicador_pperimetrico_cad" type="hidden" id="txt_indicador_pperimetrico_cad" style="width:120px" />
      <input name="txt_w_nom_archivo_pperimetric_cad" type="hidden" id="txt_w_nom_archivo_pperimetric_cad" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO_CAD'] ?>" />
      <input name="txt_w_peso_archivo_pperimetric_cad" type="hidden" id="txt_w_peso_archivo_pperimetric_cad" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO_CAD'] ?>" />
    </td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><hr class="hr_01"></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N"><table width="482" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="89"><input name="btn_guardar_plano_perimetrico" type="button" class="btn btn-primary" id="btn_guardar_plano_perimetrico" value="Guardar" /></td>
        <td width="91"><input name="btn_guardar_partida2" type="button" class="btn btn-Rojo" id="btn_guardar_partida2" value="Eliminar" onclick="Eliminar_Documento()" /></td>
        <td width="290"><div class="messages"></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
  </tr>
</table>

<?php
}
?>







<?php 

//========================================================================
//========================================================================
// INICIO - Plano de habilitacion
//========================================================================
//========================================================================

if($wCOD_TIP_DOC == '8'){
?>



<script>
$(document).ready(function(){

	$(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
	
	
    //$(':file').change(function(){
	$("#txf_partida_pperimetrico_pdf").change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_pperimetrico_pdf")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		
		
		if(fileExtension == 'PDF' || fileExtension == 'pdf'){
			$(".div_nom_archivo_pperimetrico_pdf").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_pperimetrico_pdf").val('1');
		}else{
			$("#txf_partida_pperimetrico_pdf").val('');				
			$(".div_nom_archivo_pperimetrico_pdf").html('');
			alert('Error: Solo se permiten estos archivos .pdf');
			return false;
		}
		
    });
	
	
	
	
	$("#txf_partida_pperimetrico_cad").change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_pperimetrico_cad")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		
		
		if(fileExtension == 'dwg' || fileExtension == 'DWG' || fileExtension == 'dxf' || fileExtension == 'DXF'){
			$(".div_nom_archivo_pperimetrico_cad").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_pperimetrico_cad").val('1');
		}else{
			$("#txf_partida_pperimetrico_cad").val('');				
			$(".div_nom_archivo_pperimetrico_cad").html('');
			alert('Error: Solo se permiten estos archivos .dwg | .dxf');
			return false;
		}
		
    });


    //al enviar el formulario
    $("#btn_guardar_plano_perimetrico").click(function(){
		
		var txtH_COD_TIP_DOC			= $("#txtH_COD_TIP_DOC").val();
		var txtH_COD_PREDIO_DOC_TECN	= $("#txtH_COD_PREDIO_DOC_TECN").val();
		var txtH_wID_PREDIO_SBN			= $("#txtH_wID_PREDIO_SBN").val();
		
		var txt_NUM_DESCRIP_PPERMETRICO 		= $("#txt_NUM_DESCRIP_PPERMETRICO").val();
		var txt_indicador_pperimetrico_pdf		= $("#txt_indicador_pperimetrico_pdf").val();
		var txt_indicador_pperimetrico_cad		= $("#txt_indicador_pperimetrico_cad").val();
		
		//var txt_w_nom_archivo_pperimetric_pdf		= $("#txt_w_nom_archivo_pperimetric_pdf").val();
		//var txt_w_nom_archivo_pperimetric_cad		= $("#txt_w_nom_archivo_pperimetric_cad").val();
		
		var TXH_SIMI_COD_USUARIO		= $("#TXH_SIMI_COD_USUARIO").val();
		$("#WWW_SIMI_COD_USUARIO").val(TXH_SIMI_COD_USUARIO);
		
		var TXH_SIMI_COD_ENTIDAD		= $("#TXH_SIMI_COD_ENTIDAD").val();
		$("#WWW_SIMI_COD_ENTIDAD").val(TXH_SIMI_COD_ENTIDAD);
		
		if(txt_NUM_DESCRIP_PPERMETRICO == ''){
			alert('Ingrese Numero y/o Descripción de Plano de Habilitación');
		 	$("#txt_NUM_DESCRIP_PPERMETRICO").focus();
		 
		}else if(txtH_COD_PREDIO_DOC_TECN == '' && txt_NUM_DESCRIP_PPERMETRICO != '' && txt_indicador_pperimetrico_pdf == '' && txt_indicador_pperimetrico_cad == '' ){
			alert('Adjunte por lo menos un archivo del plano de Habilitación');
		 
		}else{
				
			var formData = new FormData($(".formulario_doc_reg")[0]);
			var message = ""; 
			//hacemos la petición ajax  
			$.ajax({
				url: 'View/padron_predios_inmuebles/principal.php?operacion=Registrar_Predio_Documentos',
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,			
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
					message = $("<span class='before'>Subiendo el archivo, por favor espere...</span>");
					showMessage(message)        
				},
				//una vez finalizado correctamente
				success: function(data){

						//alert(data);
						
						var valor 				= data.split("[*]")[0];
						var codigo 				= data.split("[*]")[1];
						var nom_archivo 		= data.split("[*]")[2];			
						var nombre_carpeta 		= data.split("[*]")[3];
						var gID_PREDIO_SBN 		= data.split("[*]")[4];
						
						if(valor==1){
							
							$("#txtH_COD_PREDIO_DOC_TECN").val(codigo);
							$("#txh_COD_UE_LOCAL").val(gID_PREDIO_SBN);
							
							alert('Se Guardo Correctamente');
							
							$("#div_dialog_form_reg_documentos").dialog("destroy");
							document.getElementById('Div_contenido_dialog').innerHTML = '';
							
							Listar_Registro_Documentacion(gID_PREDIO_SBN);
							
						}else{
							message = $("<span class='error'>Ha ocurrido un error: "+data+"</span>");
							showMessage(message);
						}
			
				},
				//si ha ocurrido un error
				error: function(){
					message = $("<span class='error'>Ha ocurrido un error.</span>");
					showMessage(message);
				}
			});
			
		}
		
    });
})

//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}


</script>


<table width="519" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" style="background-color:#f4f4f4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><span class="texto_arial_plomito_11_N">

    </span></td>
    </tr>
  <tr id="tr_partida_registral_x1">
    <td width="13" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Tipo de Documento</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td><input name="txt_t5_NOM_TIP_DOC" type="text" id="txt_t5_NOM_TIP_DOC" style="width:300px" value="<?php print $Datos_eDoc['D_NOM_TIP_DOC'] ?>" readonly="readonly" /></td>
  </tr>
  <tr id="tr_descripcion_foto">
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N"> Número del Plano</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td width="323"><input name="txt_NUM_DESCRIP_PPERMETRICO" type="text" id="txt_NUM_DESCRIP_PPERMETRICO" style="width:220px" value="<?php print $Datos_eDoc['E_DESCRIP_ARCHIVO'] ?>" /></td>
  </tr>
  <tr id="tr_descripcion_gral">
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="152" height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar Archivo (PDF)   </td>
    <td width="19" valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_pperimetrico_pdf" type="file" id="txf_partida_pperimetrico_pdf"  /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top"><div class="div_nom_archivo_pperimetrico_pdf"></div>
      <input name="txt_indicador_pperimetrico_pdf" type="hidden" id="txt_indicador_pperimetrico_pdf" style="width:120px" />
      <input name="txt_w_nom_archivo_pperimetric_pdf" type="hidden" id="txt_w_nom_archivo_pperimetric_pdf" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO'] ?>" />
      <input name="txt_w_peso_archivo_pperimetric_pdf" type="hidden" id="txt_w_peso_archivo_pperimetric_pdf" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO'] ?>" />
      </td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar (.dwg | .dxf)</td>
    <td valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_pperimetrico_cad" type="file" id="txf_partida_pperimetrico_cad"  /></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3">
    <div class="div_nom_archivo_pperimetrico_cad"></div>
    <input name="txt_indicador_pperimetrico_cad" type="hidden" id="txt_indicador_pperimetrico_cad" style="width:120px" />
      <input name="txt_w_nom_archivo_pperimetric_cad" type="hidden" id="txt_w_nom_archivo_pperimetric_cad" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO_CAD'] ?>" />
      <input name="txt_w_peso_archivo_pperimetric_cad" type="hidden" id="txt_w_peso_archivo_pperimetric_cad" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO_CAD'] ?>" />
    </td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><hr class="hr_01"></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N"><table width="482" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="89"><input name="btn_guardar_plano_perimetrico" type="button" class="btn btn-primary" id="btn_guardar_plano_perimetrico" value="Guardar" /></td>
        <td width="91"><input name="btn_guardar_partida2" type="button" class="btn btn-Rojo" id="btn_guardar_partida2" value="Eliminar" onclick="Eliminar_Documento()" /></td>
        <td width="290"><div class="messages"></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
  </tr>
</table>

<?php
}
?>







<?php 

//========================================================================
//========================================================================
// INICIO - FOTOGRAFIAS
//========================================================================
//========================================================================

if($wCOD_TIP_DOC == '10'){
?>

<script>
$(document).ready(function(){

	$(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_reg")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		

		if(fileExtension == 'jpg' || fileExtension == 'JPG' || fileExtension == 'gif' || fileExtension == 'GIF' || fileExtension == 'png' || fileExtension == 'PNG'){
			$(".div_nom_archivo_part_reg").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_part_reg").val('1');
		}else{
			$("#txf_partida_reg").val('');				
			$(".div_nom_archivo_part_reg").html('');
			alert('Error: Solo se permiten estos archivos .gif | .png | .jpg');
			return false;
		}
		
    });

    //al enviar el formulario
    $("#btn_guardar_partida").click(function(){
		
		var txtH_COD_TIP_DOC			= $("#txtH_COD_TIP_DOC").val();
		var txtH_COD_PREDIO_DOC_TECN	= $("#txtH_COD_PREDIO_DOC_TECN").val();
		var txtH_wID_PREDIO_SBN			= $("#txtH_wID_PREDIO_SBN").val();
		
		var txt_NUM_PART_REG 			= $("#txt_NUM_PART_REG").val();
		var txt_indicador_part_reg		= $("#txt_indicador_part_reg").val();
		
		var TXH_SIMI_COD_USUARIO		= $("#TXH_SIMI_COD_USUARIO").val();
		$("#WWW_SIMI_COD_USUARIO").val(TXH_SIMI_COD_USUARIO);
		
		var TXH_SIMI_COD_ENTIDAD		= $("#TXH_SIMI_COD_ENTIDAD").val();
		$("#WWW_SIMI_COD_ENTIDAD").val(TXH_SIMI_COD_ENTIDAD);
		
		if(txt_NUM_PART_REG == ''){
		alert('Ingrese Descripción');
		 	$("#txt_NUM_PART_REG").focus();
		 
		}else if(txt_NUM_PART_REG == '' && txt_indicador_part_reg == '' ){
			alert('Adjunte el Archivo');
		 
		}else{
				
			var formData = new FormData($(".formulario_doc_reg")[0]);
			var message = ""; 
			//hacemos la petición ajax  
			$.ajax({
				url: 'View/padron_predios_inmuebles/principal.php?operacion=Registrar_Predio_Documentos',
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,			
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
					message = $("<span class='before'>Subiendo el archivo, por favor espere...</span>");
					showMessage(message)        
				},
				//una vez finalizado correctamente
				success: function(data){
					//alert(data);
					
						var valor 				= data.split("[*]")[0];
						var codigo 				= data.split("[*]")[1];
						var nom_archivo 		= data.split("[*]")[2];			
						var nombre_carpeta 		= data.split("[*]")[3];
						var gID_PREDIO_SBN 		= data.split("[*]")[4];
						
						if(valor==1){
							
							
							$("#txtH_COD_PREDIO_DOC_TECN").val(codigo);
							$("#txh_COD_UE_LOCAL").val(gID_PREDIO_SBN);
							
							alert('Se Guardo Correctamente');
							
							$("#div_dialog_form_reg_documentos").dialog("destroy");
							document.getElementById('Div_contenido_dialog').innerHTML = '';
							
							Listar_Registro_Documentacion(gID_PREDIO_SBN);
							
						}else{
							message = $("<span class='error'>Ha ocurrido un error: "+data+"</span>");
							showMessage(message);
						}
				
				},
				//si ha ocurrido un error
				error: function(){
					message = $("<span class='error'>Ha ocurrido un error.</span>");
					showMessage(message);
				}
			});
			
		}
		
    });
})

//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}


</script>


<table width="519" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" style="background-color:#f4f4f4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><span class="texto_arial_plomito_11_N">

    </span></td>
    </tr>
  <tr id="tr_partida_registral_x1">
    <td width="13" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Tipo de Documento</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td><input name="txt_t5_NOM_TIP_DOC" type="text" id="txt_t5_NOM_TIP_DOC" style="width:300px" value="<?php print $Datos_eDoc['D_NOM_TIP_DOC'] ?>" readonly="readonly" /></td>
  </tr>
  <tr id="tr_descripcion_foto">
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Descripción de la Foto</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td width="323"><input name="txt_NUM_PART_REG" type="text" id="txt_NUM_PART_REG" style="width:220px" value="<?php print $Datos_eDoc['E_DESCRIP_ARCHIVO'] ?>" /></td>
  </tr>
  <tr id="tr_descripcion_gral">
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="152" height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar  ( jpg | png | gif )   </td>
    <td width="19" valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_reg" type="file" id="txf_partida_reg"  /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N">
    <div class="div_nom_archivo_part_reg"></div>
      <input name="txt_indicador_part_reg" type="hidden" id="txt_indicador_part_reg" style="width:120px" />
      <input name="txt_w_nom_archivo_part_reg" type="hidden" id="txt_w_nom_archivo_part_reg" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO'] ?>" />
      <input name="txt_w_peso_archivo_part_reg" type="hidden" id="txt_w_peso_archivo_part_reg" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO'] ?>" /></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><hr class="hr_01"></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N"><table width="482" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="89"><input name="btn_guardar_partida" type="button" class="btn btn-primary" id="btn_guardar_partida" value="Guardar" /></td>
        <td width="91"><input name="btn_guardar_partida2" type="button" class="btn btn-Rojo" id="btn_guardar_partida2" value="Eliminar" onclick="Eliminar_Documento()" /></td>
        <td width="290"><div class="messages"></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
  </tr>
</table>

<? } ?>






<?php 

//========================================================================
//========================================================================
// INICIO - ESCRITURA PÚBLICA
//========================================================================
//========================================================================

if($wCOD_TIP_DOC == '11'){
?>

<script>
$(document).ready(function(){

	$(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_reg")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		

		if( fileExtension == 'pdf' || fileExtension == 'PDF'){
			$(".div_nom_archivo_part_reg").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_part_reg").val('1');
		}else{
			$("#txf_partida_reg").val('');				
			$(".div_nom_archivo_part_reg").html('');
			alert('Error: Solo se permiten estos archivos .pdf');
			return false;
		}
		
    });

    //al enviar el formulario
    $("#btn_guardar_partida").click(function(){
		
		var txtH_COD_TIP_DOC			= $("#txtH_COD_TIP_DOC").val();
		var txtH_COD_PREDIO_DOC_TECN	= $("#txtH_COD_PREDIO_DOC_TECN").val();
		var txtH_wID_PREDIO_SBN			= $("#txtH_wID_PREDIO_SBN").val();
		
		var txt_NUM_PART_REG 			= $("#txt_NUM_PART_REG").val();
		var txt_indicador_part_reg		= $("#txt_indicador_part_reg").val();
		
		var TXH_SIMI_COD_USUARIO		= $("#TXH_SIMI_COD_USUARIO").val();
		$("#WWW_SIMI_COD_USUARIO").val(TXH_SIMI_COD_USUARIO);
		
		var TXH_SIMI_COD_ENTIDAD		= $("#TXH_SIMI_COD_ENTIDAD").val();
		$("#WWW_SIMI_COD_ENTIDAD").val(TXH_SIMI_COD_ENTIDAD);
		
		if(txt_NUM_PART_REG == ''){
			alert('Ingrese Número / Descripción');
		 	$("#txt_NUM_PART_REG").focus();
		 
		}else if(txt_NUM_PART_REG == '' && txt_indicador_part_reg == '' ){
			alert('Adjunte el Archivo');
		 
		}else{
				
			var formData = new FormData($(".formulario_doc_reg")[0]);
			var message = ""; 
			//hacemos la petición ajax  
			$.ajax({
				url: 'View/padron_predios_inmuebles/principal.php?operacion=Registrar_Predio_Documentos',
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,			
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
					message = $("<span class='before'>Subiendo el archivo, por favor espere...</span>");
					showMessage(message)        
				},
				//una vez finalizado correctamente
				success: function(data){
					//alert(data);
					
						var valor 				= data.split("[*]")[0];
						var codigo 				= data.split("[*]")[1];
						var nom_archivo 		= data.split("[*]")[2];			
						var nombre_carpeta 		= data.split("[*]")[3];
						var gID_PREDIO_SBN 		= data.split("[*]")[4];
						
						if(valor==1){
							
							
							$("#txtH_COD_PREDIO_DOC_TECN").val(codigo);
							$("#txh_COD_UE_LOCAL").val(gID_PREDIO_SBN);
							
							alert('Se Guardo Correctamente');
							
							$("#div_dialog_form_reg_documentos").dialog("destroy");
							document.getElementById('Div_contenido_dialog').innerHTML = '';
							
							Listar_Registro_Documentacion(gID_PREDIO_SBN);
							
						}else{
							message = $("<span class='error'>Ha ocurrido un error: "+data+"</span>");
							showMessage(message);
						}
				
				},
				//si ha ocurrido un error
				error: function(){
					message = $("<span class='error'>Ha ocurrido un error.</span>");
					showMessage(message);
				}
			});
			
		}
		
    });
})

//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}


</script>


<table width="519" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" style="background-color:#f4f4f4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><span class="texto_arial_plomito_11_N">

    </span></td>
    </tr>
  <tr id="tr_partida_registral_x1">
    <td width="13" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Tipo de Documento</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td><input name="txt_t5_NOM_TIP_DOC" type="text" id="txt_t5_NOM_TIP_DOC" style="width:300px" value="<?php print $Datos_eDoc['D_NOM_TIP_DOC'] ?>" readonly="readonly" /></td>
  </tr>
  <tr id="tr_descripcion_foto">
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Descripción Escritura</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td width="323"><input name="txt_NUM_PART_REG" type="text" id="txt_NUM_PART_REG" style="width:220px" value="<?php print $Datos_eDoc['E_DESCRIP_ARCHIVO'] ?>" /></td>
  </tr>
  <tr id="tr_descripcion_gral">
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="152" height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar Archivo ( PDF )   </td>
    <td width="19" valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_reg" type="file" id="txf_partida_reg"  /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N">
    <div class="div_nom_archivo_part_reg"></div>
      <input name="txt_indicador_part_reg" type="hidden" id="txt_indicador_part_reg" style="width:120px" />
      <input name="txt_w_nom_archivo_part_reg" type="hidden" id="txt_w_nom_archivo_part_reg" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO'] ?>" />
      <input name="txt_w_peso_archivo_part_reg" type="hidden" id="txt_w_peso_archivo_part_reg" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO'] ?>" /></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><hr class="hr_01"></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N"><table width="482" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="89"><input name="btn_guardar_partida" type="button" class="btn btn-primary" id="btn_guardar_partida" value="Guardar" /></td>
        <td width="91"><input name="btn_guardar_partida2" type="button" class="btn btn-Rojo" id="btn_guardar_partida2" value="Eliminar" onclick="Eliminar_Documento()" /></td>
        <td width="290"><div class="messages"></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
  </tr>
</table>

<? } ?>






<?php 

//========================================================================
//========================================================================
// INICIO - MEMORIA DESCRIPTIVA
//========================================================================
//========================================================================

if($wCOD_TIP_DOC == '12'){
?>

<script>
$(document).ready(function(){

	$(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_reg")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		

		if( fileExtension == 'pdf' || fileExtension == 'PDF'){
			$(".div_nom_archivo_part_reg").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_part_reg").val('1');
		}else{
			$("#txf_partida_reg").val('');				
			$(".div_nom_archivo_part_reg").html('');
			alert('Error: Solo se permiten estos archivos .pdf');
			return false;
		}
		
    });

    //al enviar el formulario
    $("#btn_guardar_partida").click(function(){
		
		var txtH_COD_TIP_DOC			= $("#txtH_COD_TIP_DOC").val();
		var txtH_COD_PREDIO_DOC_TECN	= $("#txtH_COD_PREDIO_DOC_TECN").val();
		var txtH_wID_PREDIO_SBN			= $("#txtH_wID_PREDIO_SBN").val();
		
		var txt_NUM_PART_REG 			= $("#txt_NUM_PART_REG").val();
		var txt_indicador_part_reg		= $("#txt_indicador_part_reg").val();
		
		var TXH_SIMI_COD_USUARIO		= $("#TXH_SIMI_COD_USUARIO").val();
		$("#WWW_SIMI_COD_USUARIO").val(TXH_SIMI_COD_USUARIO);
		
		var TXH_SIMI_COD_ENTIDAD		= $("#TXH_SIMI_COD_ENTIDAD").val();
		$("#WWW_SIMI_COD_ENTIDAD").val(TXH_SIMI_COD_ENTIDAD);
		
		if(txt_NUM_PART_REG == ''){
			alert('Ingrese Número / Descripción');
		 	$("#txt_NUM_PART_REG").focus();
		 
		}else if(txt_NUM_PART_REG == '' && txt_indicador_part_reg == '' ){
			alert('Adjunte el Archivo');
		 
		}else{
				
			var formData = new FormData($(".formulario_doc_reg")[0]);
			var message = ""; 
			//hacemos la petición ajax  
			$.ajax({
				url: 'View/padron_predios_inmuebles/principal.php?operacion=Registrar_Predio_Documentos',
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,			
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
					message = $("<span class='before'>Subiendo el archivo, por favor espere...</span>");
					showMessage(message)        
				},
				//una vez finalizado correctamente
				success: function(data){
					//alert(data);
					
						var valor 				= data.split("[*]")[0];
						var codigo 				= data.split("[*]")[1];
						var nom_archivo 		= data.split("[*]")[2];			
						var nombre_carpeta 		= data.split("[*]")[3];
						var gID_PREDIO_SBN 		= data.split("[*]")[4];
						
						if(valor==1){
							
							
							$("#txtH_COD_PREDIO_DOC_TECN").val(codigo);
							$("#txh_COD_UE_LOCAL").val(gID_PREDIO_SBN);
							
							alert('Se Guardo Correctamente');
							
							$("#div_dialog_form_reg_documentos").dialog("destroy");
							document.getElementById('Div_contenido_dialog').innerHTML = '';
							
							Listar_Registro_Documentacion(gID_PREDIO_SBN);
							
						}else{
							message = $("<span class='error'>Ha ocurrido un error: "+data+"</span>");
							showMessage(message);
						}
				
				},
				//si ha ocurrido un error
				error: function(){
					message = $("<span class='error'>Ha ocurrido un error.</span>");
					showMessage(message);
				}
			});
			
		}
		
    });
})

//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}


</script>


<table width="519" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" style="background-color:#f4f4f4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><span class="texto_arial_plomito_11_N">

    </span></td>
    </tr>
  <tr id="tr_partida_registral_x1">
    <td width="13" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Tipo de Documento</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td><input name="txt_t5_NOM_TIP_DOC" type="text" id="txt_t5_NOM_TIP_DOC" style="width:300px" value="<?php print $Datos_eDoc['D_NOM_TIP_DOC'] ?>" readonly="readonly" /></td>
  </tr>
  <tr id="tr_descripcion_foto">
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Descripción Memoria</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td width="323"><input name="txt_NUM_PART_REG" type="text" id="txt_NUM_PART_REG" style="width:220px" value="<?php print $Datos_eDoc['E_DESCRIP_ARCHIVO'] ?>" /></td>
  </tr>
  <tr id="tr_descripcion_gral">
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="152" height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar Archivo ( PDF )   </td>
    <td width="19" valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_reg" type="file" id="txf_partida_reg"  /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N">
    <div class="div_nom_archivo_part_reg"></div>
      <input name="txt_indicador_part_reg" type="hidden" id="txt_indicador_part_reg" style="width:120px" />
      <input name="txt_w_nom_archivo_part_reg" type="hidden" id="txt_w_nom_archivo_part_reg" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO'] ?>" />
      <input name="txt_w_peso_archivo_part_reg" type="hidden" id="txt_w_peso_archivo_part_reg" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO'] ?>" /></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><hr class="hr_01"></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N"><table width="482" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="89"><input name="btn_guardar_partida" type="button" class="btn btn-primary" id="btn_guardar_partida" value="Guardar" /></td>
        <td width="91"><input name="btn_guardar_partida2" type="button" class="btn btn-Rojo" id="btn_guardar_partida2" value="Eliminar" onclick="Eliminar_Documento()" /></td>
        <td width="290"><div class="messages"></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
  </tr>
</table>

<? } ?>





<?php 

//========================================================================
//========================================================================
// INICIO - TITULO DE PROPIEDAD
//========================================================================
//========================================================================

if($wCOD_TIP_DOC == '13'){
?>

<script>
$(document).ready(function(){

	$(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_reg")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		

		if( fileExtension == 'pdf' || fileExtension == 'PDF'){
			$(".div_nom_archivo_part_reg").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_part_reg").val('1');
		}else{
			$("#txf_partida_reg").val('');				
			$(".div_nom_archivo_part_reg").html('');
			alert('Error: Solo se permiten estos archivos .pdf');
			return false;
		}
		
    });

    //al enviar el formulario
    $("#btn_guardar_partida").click(function(){
		
		var txtH_COD_TIP_DOC			= $("#txtH_COD_TIP_DOC").val();
		var txtH_COD_PREDIO_DOC_TECN	= $("#txtH_COD_PREDIO_DOC_TECN").val();
		var txtH_wID_PREDIO_SBN			= $("#txtH_wID_PREDIO_SBN").val();
		
		var txt_NUM_PART_REG 			= $("#txt_NUM_PART_REG").val();
		var txt_indicador_part_reg		= $("#txt_indicador_part_reg").val();
		
		var TXH_SIMI_COD_USUARIO		= $("#TXH_SIMI_COD_USUARIO").val();
		$("#WWW_SIMI_COD_USUARIO").val(TXH_SIMI_COD_USUARIO);
		
		var TXH_SIMI_COD_ENTIDAD		= $("#TXH_SIMI_COD_ENTIDAD").val();
		$("#WWW_SIMI_COD_ENTIDAD").val(TXH_SIMI_COD_ENTIDAD);
		
		if(txt_NUM_PART_REG == ''){
			alert('Ingrese Número / Descripción');
		 	$("#txt_NUM_PART_REG").focus();
		 
		}else if(txt_NUM_PART_REG == '' && txt_indicador_part_reg == '' ){
			alert('Adjunte el Archivo');
		 
		}else{
				
			var formData = new FormData($(".formulario_doc_reg")[0]);
			var message = ""; 
			//hacemos la petición ajax  
			$.ajax({
				url: 'View/padron_predios_inmuebles/principal.php?operacion=Registrar_Predio_Documentos',
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,			
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
					message = $("<span class='before'>Subiendo el archivo, por favor espere...</span>");
					showMessage(message)        
				},
				//una vez finalizado correctamente
				success: function(data){
					//alert(data);
					
						var valor 				= data.split("[*]")[0];
						var codigo 				= data.split("[*]")[1];
						var nom_archivo 		= data.split("[*]")[2];			
						var nombre_carpeta 		= data.split("[*]")[3];
						var gID_PREDIO_SBN 		= data.split("[*]")[4];
						
						if(valor==1){
							
							
							$("#txtH_COD_PREDIO_DOC_TECN").val(codigo);
							$("#txh_COD_UE_LOCAL").val(gID_PREDIO_SBN);
							
							alert('Se Guardo Correctamente');
							
							$("#div_dialog_form_reg_documentos").dialog("destroy");
							document.getElementById('Div_contenido_dialog').innerHTML = '';
							
							Listar_Registro_Documentacion(gID_PREDIO_SBN);
							
						}else{
							message = $("<span class='error'>Ha ocurrido un error: "+data+"</span>");
							showMessage(message);
						}
				
				},
				//si ha ocurrido un error
				error: function(){
					message = $("<span class='error'>Ha ocurrido un error.</span>");
					showMessage(message);
				}
			});
			
		}
		
    });
})

//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}


</script>


<table width="519" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" style="background-color:#f4f4f4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><span class="texto_arial_plomito_11_N">

    </span></td>
    </tr>
  <tr id="tr_partida_registral_x1">
    <td width="13" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Tipo de Documento</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td><input name="txt_t5_NOM_TIP_DOC" type="text" id="txt_t5_NOM_TIP_DOC" style="width:300px" value="<?php print $Datos_eDoc['D_NOM_TIP_DOC'] ?>" readonly="readonly" /></td>
  </tr>
  <tr id="tr_descripcion_foto">
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Descripción Memoria</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td width="323"><input name="txt_NUM_PART_REG" type="text" id="txt_NUM_PART_REG" style="width:220px" value="<?php print $Datos_eDoc['E_DESCRIP_ARCHIVO'] ?>" /></td>
  </tr>
  <tr id="tr_descripcion_gral">
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="152" height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar Archivo ( PDF )   </td>
    <td width="19" valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_reg" type="file" id="txf_partida_reg"  /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N">
    <div class="div_nom_archivo_part_reg"></div>
      <input name="txt_indicador_part_reg" type="hidden" id="txt_indicador_part_reg" style="width:120px" />
      <input name="txt_w_nom_archivo_part_reg" type="hidden" id="txt_w_nom_archivo_part_reg" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO'] ?>" />
      <input name="txt_w_peso_archivo_part_reg" type="hidden" id="txt_w_peso_archivo_part_reg" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO'] ?>" /></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><hr class="hr_01"></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N"><table width="482" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="89"><input name="btn_guardar_partida" type="button" class="btn btn-primary" id="btn_guardar_partida" value="Guardar" /></td>
        <td width="91"><input name="btn_guardar_partida2" type="button" class="btn btn-Rojo" id="btn_guardar_partida2" value="Eliminar" onclick="Eliminar_Documento()" /></td>
        <td width="290"><div class="messages"></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
  </tr>
</table>

<? } ?>









<?php 

//========================================================================
//========================================================================
// INICIO - INFORME TECNICO LEGAL
//========================================================================
//========================================================================

if($wCOD_TIP_DOC == '14'){
?>

<script>
$(document).ready(function(){

	$(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_reg")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		

		if( fileExtension == 'pdf' || fileExtension == 'PDF'){
			$(".div_nom_archivo_part_reg").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_part_reg").val('1');
		}else{
			$("#txf_partida_reg").val('');				
			$(".div_nom_archivo_part_reg").html('');
			alert('Error: Solo se permiten estos archivos .pdf');
			return false;
		}
		
    });

    //al enviar el formulario
    $("#btn_guardar_partida").click(function(){
		
		var txtH_COD_TIP_DOC			= $("#txtH_COD_TIP_DOC").val();
		var txtH_COD_PREDIO_DOC_TECN	= $("#txtH_COD_PREDIO_DOC_TECN").val();
		var txtH_wID_PREDIO_SBN			= $("#txtH_wID_PREDIO_SBN").val();
		
		var txt_NUM_PART_REG 			= $("#txt_NUM_PART_REG").val();
		var txt_indicador_part_reg		= $("#txt_indicador_part_reg").val();
		
		var TXH_SIMI_COD_USUARIO		= $("#TXH_SIMI_COD_USUARIO").val();
		$("#WWW_SIMI_COD_USUARIO").val(TXH_SIMI_COD_USUARIO);
		
		var TXH_SIMI_COD_ENTIDAD		= $("#TXH_SIMI_COD_ENTIDAD").val();
		$("#WWW_SIMI_COD_ENTIDAD").val(TXH_SIMI_COD_ENTIDAD);
		
		if(txt_NUM_PART_REG == ''){
			alert('Ingrese Número / Descripción');
		 	$("#txt_NUM_PART_REG").focus();
		 
		}else if(txt_NUM_PART_REG == '' && txt_indicador_part_reg == '' ){
			alert('Adjunte el Archivo');
		 
		}else{
				
			var formData = new FormData($(".formulario_doc_reg")[0]);
			var message = ""; 
			//hacemos la petición ajax  
			$.ajax({
				url: 'View/padron_predios_inmuebles/principal.php?operacion=Registrar_Predio_Documentos',
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,			
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
					message = $("<span class='before'>Subiendo el archivo, por favor espere...</span>");
					showMessage(message)        
				},
				//una vez finalizado correctamente
				success: function(data){
					//alert(data);
					
						var valor 				= data.split("[*]")[0];
						var codigo 				= data.split("[*]")[1];
						var nom_archivo 		= data.split("[*]")[2];			
						var nombre_carpeta 		= data.split("[*]")[3];
						var gID_PREDIO_SBN 		= data.split("[*]")[4];
						
						if(valor==1){
							
							
							$("#txtH_COD_PREDIO_DOC_TECN").val(codigo);
							$("#txh_COD_UE_LOCAL").val(gID_PREDIO_SBN);
							
							alert('Se Guardo Correctamente');
							
							$("#div_dialog_form_reg_documentos").dialog("destroy");
							document.getElementById('Div_contenido_dialog').innerHTML = '';
							
							Listar_Registro_Documentacion(gID_PREDIO_SBN);
							
						}else{
							message = $("<span class='error'>Ha ocurrido un error: "+data+"</span>");
							showMessage(message);
						}
				
				},
				//si ha ocurrido un error
				error: function(){
					message = $("<span class='error'>Ha ocurrido un error.</span>");
					showMessage(message);
				}
			});
			
		}
		
    });
})

//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}


</script>


<table width="519" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" style="background-color:#f4f4f4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><span class="texto_arial_plomito_11_N">

    </span></td>
    </tr>
  <tr id="tr_partida_registral_x1">
    <td width="13" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Tipo de Documento</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td><input name="txt_t5_NOM_TIP_DOC" type="text" id="txt_t5_NOM_TIP_DOC" style="width:300px" value="<?php print $Datos_eDoc['D_NOM_TIP_DOC'] ?>" readonly="readonly" /></td>
  </tr>
  <tr id="tr_descripcion_foto">
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Descripción Memoria</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td width="323"><input name="txt_NUM_PART_REG" type="text" id="txt_NUM_PART_REG" style="width:220px" value="<?php print $Datos_eDoc['E_DESCRIP_ARCHIVO'] ?>" /></td>
  </tr>
  <tr id="tr_descripcion_gral">
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="152" height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar Archivo ( PDF )   </td>
    <td width="19" valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_reg" type="file" id="txf_partida_reg"  /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N">
    <div class="div_nom_archivo_part_reg"></div>
      <input name="txt_indicador_part_reg" type="hidden" id="txt_indicador_part_reg" style="width:120px" />
      <input name="txt_w_nom_archivo_part_reg" type="hidden" id="txt_w_nom_archivo_part_reg" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO'] ?>" />
      <input name="txt_w_peso_archivo_part_reg" type="hidden" id="txt_w_peso_archivo_part_reg" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO'] ?>" /></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><hr class="hr_01"></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N"><table width="482" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="89"><input name="btn_guardar_partida" type="button" class="btn btn-primary" id="btn_guardar_partida" value="Guardar" /></td>
        <td width="91"><input name="btn_guardar_partida2" type="button" class="btn btn-Rojo" id="btn_guardar_partida2" value="Eliminar" onclick="Eliminar_Documento()" /></td>
        <td width="290"><div class="messages"></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
  </tr>
</table>

<? } ?>





<?php 

//========================================================================
//========================================================================
// INICIO -  	Tipo Dispostivo Legal
//========================================================================
//========================================================================

if($wCOD_TIP_DOC == '15'){
?>

<script>
$(document).ready(function(){

	$(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_reg")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		

		if( fileExtension == 'pdf' || fileExtension == 'PDF'){
			$(".div_nom_archivo_part_reg").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_part_reg").val('1');
		}else{
			$("#txf_partida_reg").val('');				
			$(".div_nom_archivo_part_reg").html('');
			alert('Error: Solo se permiten estos archivos .pdf');
			return false;
		}
		
    });

    //al enviar el formulario
    $("#btn_guardar_partida").click(function(){
		
		var txtH_COD_TIP_DOC			= $("#txtH_COD_TIP_DOC").val();
		var txtH_COD_PREDIO_DOC_TECN	= $("#txtH_COD_PREDIO_DOC_TECN").val();
		var txtH_wID_PREDIO_SBN			= $("#txtH_wID_PREDIO_SBN").val();
		
		var txt_NUM_PART_REG 			= $("#txt_NUM_PART_REG").val();
		var txt_indicador_part_reg		= $("#txt_indicador_part_reg").val();
		
		var TXH_SIMI_COD_USUARIO		= $("#TXH_SIMI_COD_USUARIO").val();
		$("#WWW_SIMI_COD_USUARIO").val(TXH_SIMI_COD_USUARIO);
		
		var TXH_SIMI_COD_ENTIDAD		= $("#TXH_SIMI_COD_ENTIDAD").val();
		$("#WWW_SIMI_COD_ENTIDAD").val(TXH_SIMI_COD_ENTIDAD);
		
		var CBO_tip_dispos_legal = $("#CBO_tip_dispos_legal").val();
		
		if(txt_NUM_PART_REG == ''){
			alert('Ingrese Número / Descripción');
		 	$("#txt_NUM_PART_REG").focus();
		 
		}else if(CBO_tip_dispos_legal == ''){
			alert('Seleccione Tipo Dispostivo Legal');
		 	$("#CBO_tip_dispos_legal").focus();
		 
		}else if(txt_NUM_PART_REG == '' && txt_indicador_part_reg == '' ){
			alert('Adjunte el Archivo');
		 
		}else{
				
			var formData = new FormData($(".formulario_doc_reg")[0]);
			var message = ""; 
			//hacemos la petición ajax  
			$.ajax({
				url: 'View/padron_predios_inmuebles/principal.php?operacion=Registrar_Predio_Documentos',
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,			
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
					message = $("<span class='before'>Subiendo el archivo, por favor espere...</span>");
					showMessage(message)        
				},
				//una vez finalizado correctamente
				success: function(data){
					//alert(data);
					
						var valor 				= data.split("[*]")[0];
						var codigo 				= data.split("[*]")[1];
						var nom_archivo 		= data.split("[*]")[2];			
						var nombre_carpeta 		= data.split("[*]")[3];
						var gID_PREDIO_SBN 		= data.split("[*]")[4];
						
						if(valor==1){
							
							
							$("#txtH_COD_PREDIO_DOC_TECN").val(codigo);
							$("#txh_COD_UE_LOCAL").val(gID_PREDIO_SBN);
							
							alert('Se Guardo Correctamente');
							
							$("#div_dialog_form_reg_documentos").dialog("destroy");
							document.getElementById('Div_contenido_dialog').innerHTML = '';
							
							Listar_Registro_Documentacion(gID_PREDIO_SBN);
							
						}else{
							message = $("<span class='error'>Ha ocurrido un error: "+data+"</span>");
							showMessage(message);
						}
				
				},
				//si ha ocurrido un error
				error: function(){
					message = $("<span class='error'>Ha ocurrido un error.</span>");
					showMessage(message);
				}
			});
			
		}
		
    });
})

//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}


</script>


<table width="519" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" style="background-color:#f4f4f4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><span class="texto_arial_plomito_11_N">

    </span></td>
    </tr>
  <tr id="tr_partida_registral_x1">
    <td width="13" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Tipo de Documento</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td width="323"><input name="txt_t5_NOM_TIP_DOC" type="text" id="txt_t5_NOM_TIP_DOC" style="width:300px" value="<?php print $Datos_eDoc['D_NOM_TIP_DOC'] ?>" readonly="readonly" /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td height="30" valign="top" class="texto_arial_plomito_11_N"> 	Tipo Dispostivo Legal</td>
    <td valign="top" class="texto_arial_plomito_11_N">:</td>
    <td>
      
  <select name="CBO_tip_dispos_legal" id="CBO_tip_dispos_legal" style="width:220px">
    <option value="-" >:: Seleccione Tipo Dispostivo Legal ::</option>
  <?php if($List_DisposicionLegal) foreach ($List_DisposicionLegal as $DisposicionLegal): 
			$select_Disposiclegal = ($Datos_eDoc['E_COD_TIP_RESOL_DL'] == $DisposicionLegal['COD_TIP_RESOL_DL']) ? 'selected="selected"' : '';
?>
    
    <option value="<?php print $DisposicionLegal['COD_TIP_RESOL_DL'] ?>" <?=$select_Disposiclegal?> ><?php print $DisposicionLegal['DES_TIP_RESOL_DL'] ?></option>
    <?php endforeach; ?>
    </select>
      
      </td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Descripción Dispositivo L.</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td><input name="txt_NUM_PART_REG" type="text" id="txt_NUM_PART_REG" style="width:220px" value="<?php print $Datos_eDoc['E_DESCRIP_ARCHIVO'] ?>" /></td>
  </tr>
  <tr id="tr_descripcion_gral">
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="152" height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar Archivo ( PDF )   </td>
    <td width="19" valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_reg" type="file" id="txf_partida_reg"  /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N">
    <div class="div_nom_archivo_part_reg"></div>
      <input name="txt_indicador_part_reg" type="hidden" id="txt_indicador_part_reg" style="width:120px" />
      <input name="txt_w_nom_archivo_part_reg" type="hidden" id="txt_w_nom_archivo_part_reg" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO'] ?>" />
      <input name="txt_w_peso_archivo_part_reg" type="hidden" id="txt_w_peso_archivo_part_reg" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO'] ?>" /></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><hr class="hr_01"></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N"><table width="482" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="89"><input name="btn_guardar_partida" type="button" class="btn btn-primary" id="btn_guardar_partida" value="Guardar" /></td>
        <td width="91"><input name="btn_guardar_partida2" type="button" class="btn btn-Rojo" id="btn_guardar_partida2" value="Eliminar" onclick="Eliminar_Documento()" /></td>
        <td width="290"><div class="messages"></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
  </tr>
</table>

<? } ?>






<?php 

//========================================================================
//========================================================================
// INICIO -   Plano Perimétrico - AutoCAD DX
//========================================================================
//========================================================================

if($wCOD_TIP_DOC == '16'){
?>

<script>
$(document).ready(function(){

	$(".messages").hide();
    //queremos que esta variable sea global
    var fileExtension = "";
    //función que observa los cambios del campo file y obtiene información
    $(':file').change(function(){
        //obtenemos un array con los datos del archivo
        var file = $("#txf_partida_reg")[0].files[0];
        //obtenemos el nombre del archivo
        var fileName = file.name;
        //obtenemos la extensión del archivo
        fileExtension = fileName.substring(fileName.lastIndexOf('.') + 1);
        //obtenemos el tamaño del archivo
        var fileSize = file.size;
        //obtenemos el tipo de archivo image/png ejemplo
        var fileType = file.type;
        //mensaje con la información del archivo		

		if( fileExtension == 'dxf' || fileExtension == 'DXF'){
			$(".div_nom_archivo_part_reg").html("<span class='info'>Archivo para subir: "+fileName+", peso total: "+fileSize+" bytes.</span>");
			$("#txt_indicador_part_reg").val('1');
		}else{
			$("#txf_partida_reg").val('');				
			$(".div_nom_archivo_part_reg").html('');
			alert('Error: Solo se permiten estos archivos .dxf');
			return false;
		}
		
    });

    //al enviar el formulario
    $("#btn_guardar_partida").click(function(){
		
		var txtH_COD_TIP_DOC			= $("#txtH_COD_TIP_DOC").val();
		var txtH_COD_PREDIO_DOC_TECN	= $("#txtH_COD_PREDIO_DOC_TECN").val();
		var txtH_wID_PREDIO_SBN			= $("#txtH_wID_PREDIO_SBN").val();

		var txt_indicador_part_reg		= $("#txt_indicador_part_reg").val();
		
		var TXH_SIMI_COD_USUARIO		= $("#TXH_SIMI_COD_USUARIO").val();
		$("#WWW_SIMI_COD_USUARIO").val(TXH_SIMI_COD_USUARIO);
		
		var TXH_SIMI_COD_ENTIDAD		= $("#TXH_SIMI_COD_ENTIDAD").val();
		$("#WWW_SIMI_COD_ENTIDAD").val(TXH_SIMI_COD_ENTIDAD);
		
		var cbo_sist_coordenadas = $("#cbo_sist_coordenadas").val();
		
		if(cbo_sist_coordenadas == ''){
			alert('Seleccione Tipo Sistema de Coordenadas');
		 	$("#cbo_sist_coordenadas").focus();
		 
		}else if(txt_indicador_part_reg == '' ){
			alert('Adjunte el Archivo .dxf');
		 
		}else{
				
			var formData = new FormData($(".formulario_doc_reg")[0]);
			var message = ""; 
			//hacemos la petición ajax  
			$.ajax({
				url: 'View/padron_predios_inmuebles/principal.php?operacion=Registrar_Predio_Documentos',
				type: 'POST',
				// Form data
				//datos del formulario
				data: formData,			
				//necesario para subir archivos via ajax
				cache: false,
				contentType: false,
				processData: false,
				//mientras enviamos el archivo
				beforeSend: function(){
					message = $("<span class='before'>Subiendo el archivo, por favor espere...</span>");
					showMessage(message)        
				},
				//una vez finalizado correctamente
				success: function(data){
					//alert(data);
					
						var valor 				= data.split("[*]")[0];
						var codigo 				= data.split("[*]")[1];
						var nom_archivo 		= data.split("[*]")[2];			
						var nombre_carpeta 		= data.split("[*]")[3];
						var gID_PREDIO_SBN 		= data.split("[*]")[4];
						
						if(valor==1){
							
							
							$("#txtH_COD_PREDIO_DOC_TECN").val(codigo);
							$("#txh_COD_UE_LOCAL").val(gID_PREDIO_SBN);
							
							alert('Se Guardo Correctamente');
							
							$("#div_dialog_form_reg_documentos").dialog("destroy");
							document.getElementById('Div_contenido_dialog').innerHTML = '';
							
							Listar_Registro_Documentacion(gID_PREDIO_SBN);
							
						}else{
							message = $("<span class='error'>Ha ocurrido un error: "+data+"</span>");
							showMessage(message);
						}
				
				},
				//si ha ocurrido un error
				error: function(){
					message = $("<span class='error'>Ha ocurrido un error.</span>");
					showMessage(message);
				}
			});
			
		}
		
    });
})

//como la utilizamos demasiadas veces, creamos una función para 
//evitar repetición de código
function showMessage(message){
    $(".messages").html("").show();
    $(".messages").html(message);
}


</script>


<table width="519" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" style="background-color:#f4f4f4">
  <tr>
    <td class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><span class="texto_arial_plomito_11_N">

    </span></td>
    </tr>
  <tr id="tr_partida_registral_x1">
    <td width="13" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td class="texto_arial_plomito_11_N">Tipo de Documento</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td width="323"><input name="txt_t5_NOM_TIP_DOC" type="text" id="txt_t5_NOM_TIP_DOC" style="width:300px" value="<?php print $Datos_eDoc['D_NOM_TIP_DOC'] ?>" readonly="readonly" /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td height="30" class="texto_arial_plomito_11_N">Sistema de Coordenadas</td>
    <td class="texto_arial_plomito_11_N">:</td>
    <td>
      
  <select name="cbo_sist_coordenadas" id="cbo_sist_coordenadas" style="width:220px" >
    <option value="0" selected="selected">- Seleccione -</option>
    
    <?	  
	$e_COD_SISTEMA_COORDENADA = $Datos_eDoc['E_COD_SISTEMA_COORDENADA'];
	  
    if($e_COD_SISTEMA_COORDENADA == '32717'){
		$select_sist_coord_01 = 'selected="selected"';
		$select_sist_coord_02 = '';
		$select_sist_coord_03 = '';
		$select_sist_coord_04 = '';
		$select_sist_coord_05 = '';
		$select_sist_coord_06 = '';
		
	}else if($e_COD_SISTEMA_COORDENADA == '32718'){
		$select_sist_coord_01 = '';
		$select_sist_coord_02 = 'selected="selected"';
		$select_sist_coord_03 = '';
		$select_sist_coord_04 = '';
		$select_sist_coord_05 = '';
		$select_sist_coord_06 = '';
		
	}else if($e_COD_SISTEMA_COORDENADA == '32719'){
		$select_sist_coord_01 = '';
		$select_sist_coord_02 = '';
		$select_sist_coord_03 = 'selected="selected"';
		$select_sist_coord_04 = '';
		$select_sist_coord_05 = '';
		$select_sist_coord_06 = '';
		
	}else if($e_COD_SISTEMA_COORDENADA == '24877'){
		$select_sist_coord_01 = '';
		$select_sist_coord_02 = '';
		$select_sist_coord_03 = '';
		$select_sist_coord_04 = 'selected="selected"';
		$select_sist_coord_05 = '';
		$select_sist_coord_06 = '';
		
	}else if($e_COD_SISTEMA_COORDENADA == '24878'){
		$select_sist_coord_01 = '';
		$select_sist_coord_02 = '';
		$select_sist_coord_03 = '';
		$select_sist_coord_04 = '';
		$select_sist_coord_05 = 'selected="selected"';
		$select_sist_coord_06 = '';
		
	}else if($e_COD_SISTEMA_COORDENADA == '24879'){
		$select_sist_coord_01 = '';
		$select_sist_coord_02 = '';
		$select_sist_coord_03 = '';
		$select_sist_coord_04 = '';
		$select_sist_coord_05 = '';
		$select_sist_coord_06 = 'selected="selected"';
	}else{
		$select_sist_coord_01 = '';
		$select_sist_coord_02 = '';
		$select_sist_coord_03 = '';
		$select_sist_coord_04 = '';
		$select_sist_coord_05 = '';
		$select_sist_coord_06 = '';
	}
	  ?>
    
    <option value="32717" <?=$select_sist_coord_01?> >UTM W6S84 ZONA 17</option>
    <option value="32718" <?=$select_sist_coord_02?> >UTM W6S84 ZONA 18</option>
    <option value="32719" <?=$select_sist_coord_03?> >UTM W6S84 ZONA 19</option>
    <option value="24877" <?=$select_sist_coord_04?> >UTM PSAD56 ZONA 17</option>
    <option value="24878" <?=$select_sist_coord_05?> >UTM PSAD56 ZONA 18</option>
    <option value="24879" <?=$select_sist_coord_06?> >UTM PSAD56 ZONA 19</option>
    </select>
      
      </td>
  </tr>
  <tr id="tr_descripcion_gral">
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td width="152" height="30" valign="top" class="texto_arial_plomito_11_N">Adjuntar Archivo ( PDF )   </td>
    <td width="19" valign="top" class="texto_arial_plomito_11_N">:</td>
    <td><input name="txf_partida_reg" type="file" id="txf_partida_reg"  /></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N">
    <div class="div_nom_archivo_part_reg"></div>
      <input name="txt_indicador_part_reg" type="hidden" id="txt_indicador_part_reg" style="width:120px" />
      <input name="txt_w_nom_archivo_part_reg" type="hidden" id="txt_w_nom_archivo_part_reg" style="width:450px" value="<?php print $Datos_eDoc['E_NOM_ARCHIVO'] ?>" />
      <input name="txt_w_peso_archivo_part_reg" type="hidden" id="txt_w_peso_archivo_part_reg" style="width:200px" value="<?php print $Datos_eDoc['E_PESO_ARCHIVO'] ?>" /></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" class="texto_arial_plomito_11_N"><hr class="hr_01"></td>
    </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td colspan="3" valign="top" class="texto_arial_plomito_11_N"><table width="482" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="89"><input name="btn_guardar_partida" type="button" class="btn btn-primary" id="btn_guardar_partida" value="Guardar" /></td>
        <td width="91"><input name="btn_guardar_partida2" type="button" class="btn btn-Rojo" id="btn_guardar_partida2" value="Eliminar" onclick="Eliminar_Documento()" /></td>
        <td width="290"><div class="messages"></div></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
    <td valign="top" class="texto_arial_plomito_11_N">&nbsp;</td>
  </tr>
</table>

<? } ?>


</form>
