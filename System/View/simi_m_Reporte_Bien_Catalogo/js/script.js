$(function() {
    $("#tabs").tabs();
});

function handle_load_clases_by_group() {
  elemento = $("#busc_cbo_grupo_generico").val();

  $.post("View/simi_grupo/listbox_clase_x_grupo.php",{
		CBO_GRUPO:elemento
	},function(data){
		$("#busc_CBO_CLASE").html(data);
	});
}

function handle_filtrar_grid() {
  var url = "View/simi_m_Reporte_Bien_Catalogo/principal.php?operacion=filtrar-grid";
  var codigo = $('#busc_txt_codigo_bien').val();
  var nombre = $('#busc_txt_denominacion').val();
  var grupo = $('#busc_cbo_grupo_generico').val();
  var clase = $('#busc_CBO_CLASE').val();
  var resolucion = $('#BUSC_CBO_RESOLUCION_CATALOGO').val();
  var condicion = $('#busc_cbo_condicion').val();

  clase = (clase == "" || clase == "-" || clase == "0") ? "" : clase;
  console.log(clase);
  url += (codigo == "") ? "" : "&codigo=" + codigo;
  url += (nombre == "") ? "" : "&nombre=" + nombre;
  url += (grupo == "") ? "" : "&grupo=" + grupo;
  url += (clase == "" || clase == "-" || clase == "0") ? "" : "&clase=" + clase;
  url += (resolucion == "") ? "" : "&resolucion=" + resolucion;
  url += (condicion == "") ? "" : "&condicion=" + condicion;

  $.ajax(url).done(function (response) {
    $('#detail-container').html(response);
    $('#detail-container').data('codigo', codigo);
    $('#detail-container').data('nombre', nombre);
    $('#detail-container').data('grupo', grupo);
    $('#detail-container').data('clase', clase);
    $('#detail-container').data('resolucion', resolucion);
    $('#detail-container').data('condicion', condicion);
  });
}

function handle_paginar_grid(pageNumber) {
  var url = "View/simi_m_Reporte_Bien_Catalogo/principal.php?operacion=filtrar-grid";
  var codigo = $('#detail-container').data('codigo');
  var nombre = $('#detail-container').data('nombre');
  var grupo = $('#detail-container').data('grupo');
  var clase = $('#detail-container').data('clase');
  var resolucion = $('#detail-container').data('resolucion');
  var condicion = $('#detail-container').data('condicion');

  url += (codigo == "" || typeof(codigo) == "undefined") ? "" : "&codigo=" + codigo;
  url += (nombre == "" || typeof(nombre) == "undefined") ? "" : "&nombre=" + nombre;
  url += (pageNumber == "" || typeof(pageNumber) == "undefined") ? "" : "&numeroPagina=" + pageNumber;
  url += (grupo == "" || typeof(grupo) == "undefined") ? "" : "&grupo=" + grupo;
  url += (clase == "" || typeof(clase) == "undefined" || clase == "-") ? "" : "&clase=" + clase;
  url += (resolucion == "" || typeof(resolucion) == "undefined") ? "" : "&resolucion=" + resolucion;
  url += (condicion == "" || typeof(condicion) == "undefined") ? "" : "&condicion=" + condicion;

  $.ajax(url).done(function (response) {
    $('#detail-container').html(response);
    $('#detail-container').data('codigo', codigo);
    $('#detail-container').data('nombre', nombre);
    $('#detail-container').data('grupo', grupo);
    $('#detail-container').data('clase', clase);
    $('#detail-container').data('resolucion', resolucion);
    $('#detail-container').data('condicion', condicion);
  });
}

function handle_limpiar_filtro() {
  $("#busc_cbo_grupo_generico").val("");
  $("#busc_CBO_CLASE").val("");
  $("#BUSC_CBO_RESOLUCION_CATALOGO").val("");
  $("#busc_cbo_condicion").val("");
  $('#busc_txt_codigo_bien').val("");
  $('#busc_txt_denominacion').val("");
}









































function cargarReporteGrafico(url) {

  $.ajax((url != "")? url : 'View/simi_m_Reporte_Bien_x_Parametros/principal.php?operacion=graficos-estadisticos').done(function (response) {
    console.log(response.estadoBien[0].nombre);
    Highcharts.chart('graph-container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Estadisticas de Estados de Bienes.'
        },
        subtitle: {
            text: 'SINABIP - SBN'
        },
        xAxis: {
            categories: [
              response.estadoBien[0].nombre,
              response.estadoBien[1].nombre,
              response.estadoBien[2].nombre,
              response.estadoBien[3].nombre
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Cantidades en Unidades (und)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            // column: {
            //     pointPadding: 0.2,
            //     borderWidth: 0
            // }
            bar: {
                dataLabels: {
                    enabled: true
                }
            },
            series: {
                cursor: 'pointer',
                point: {
                    events: {
                        click: function(e) {
                            handle_load_report_detail(this.category, this.color);
                        }
                    }
                },
                marker: {
                    lineWidth: 1
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Activos',
            color: "green",
            // data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0]
            data: [
              parseInt(response.cantidadesAlta[1]),
              parseInt(response.cantidadesAlta[2]),
              parseInt(response.cantidadesAlta[3]),
              parseInt(response.cantidadesAlta[4])
            ]
        }, {
            name: 'Bajas',
            color: "red",
            // data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5]
            data: [
              parseInt(response.cantidadesBaja[1]),
              parseInt(response.cantidadesBaja[2]),
              parseInt(response.cantidadesBaja[3]),
              parseInt(response.cantidadesBaja[4])
            ]
        }]
    });
  });
}


function handle_filtrar_graficos_parametros() {

}

function handle_load_report_detail(category, type) {
  console.log(category);
  console.log(type);


  var url = "View/simi_m_Reporte_Bien_x_Parametros/principal.php?operacion=graficos-estadisticos-detalle";
  url += (category == "") ? "" : "&category=" + category;
  url += (type != "" && type.toLowerCase() == "green") ? "&type=" + "alta" : "";
  url += (type != "" && type.toLowerCase() == "red") ? "&type=" + "baja" : "";
  $.ajax(url).done(function (data) {
    $("#detail-container").html(data);
    $("#detail-container").data('category', category);
    $("#detail-container").data('type', type);
  });
}

/**
 * Pagina el grid de la presentacion.
 */
function handle_paginar_listado_bienes(numeroPagina) {
  var tab = $(".ui-state-active a").attr("href");
  if (tab == "#tabs-2") {

    var url = 'View/simi_m_Reporte_Bien_Disposicion/principal.php?operacion=filtrar-grid';
    url += (numeroPagina == "") ? "" : "&numeroPagina=" + numeroPagina;
    var type = $("#listado-disposicion-type").find(":selected").val();
    var resolucion = $("#listado-disposicion-resolucion").val();
    var beneficiario = $("#listado-disposicion-beneficiario").val();
    var beneficiarioNombre = $("#listado-disposicion-beneficiario-nombre").val();
    url += (type == "") ? "" : "&type=" + type;
    url += (resolucion == "") ? "" : "&resolucion=" + resolucion;
    url += (beneficiario == "") ? "" : "&beneficiario=" + beneficiario;
    url += (beneficiarioNombre == "") ? "" : "&beneficiarioNombre=" + beneficiarioNombre;
    $.ajax(url).done(function (data) {
      $("#div_lista_resultado_busqueda").html("");
      $("#div_lista_resultado_busqueda").html(data);
    });
  }
  if (tab == "#tabs-1") {
    var url = "View/simi_m_Reporte_Bien_x_Parametros/principal.php?operacion=graficos-estadisticos-detalle";
    var type = $("#detail-container").data('type');
    var category = $("#detail-container").data('category');
    var codigoEntidad = $("#TXH_SIMI_COD_ENTIDAD").val();

    url += (category == "") ? "" : "&category=" + category;
    url += (type != "" && type.toLowerCase() == "green") ? "&type=" + "alta" : "";
    url += (type != "" && type.toLowerCase() == "red") ? "&type=" + "baja" : "";
    url += (numeroPagina == "") ? "" : "&numeroPagina=" + numeroPagina;
    url += (codigoEntidad == "") ? "" : "&entity=" + parseInt(codigoEntidad);

    $.ajax(url).done(function (data) {
      $("#detail-container").html(data);
    });
  }

}

/**
 * Muestra el detalle de bienes para cada predio de la entidad.
 */
function handle_listar_bienes_predio(codigoPredio) {
  var url = "View/simi_m_Reporte_Bien_x_Parametros/principal.php?operacion=bienes-local";
  url += (codigoPredio == "") ? "": "&codigoPredio=" + codigoPredio;
  $.ajax(url).done(function (response) {
    console.log("hola mundo");
    $("#detalle_bienes_local").html(response);
    $("#detalle_bienes_local").data("codigoPredio", codigoPredio);
  });
}
/**
* Pagina el grid de la presentacion reporte por locales.
*/
function handle_paginar_detalle_bienes_predio(numeroPagina) {
  var url = "View/simi_m_Reporte_Bien_x_Parametros/principal.php?operacion=bienes-local";
  var type = $("#detail-container").data('type');
  var category = $("#detail-container").data('category');
  var codigoEntidad = $("#TXH_SIMI_COD_ENTIDAD").val();

  var codigoPredio = $('#detalle_bienes_local').data('codigoPredio');
  url += (codigoPredio == "") ? "": "&codigoPredio=" + codigoPredio;
  url += (numeroPagina == "") ? "" : "&numeroPagina=" + numeroPagina;
  url += (codigoEntidad == "") ? "" : "&entity=" + parseInt(codigoEntidad);

  $.ajax(url).done(function (response) {
    $("#detalle_bienes_local").html(response);
    $("#detalle_bienes_local").data("codigoPredio", codigoPredio);
  });
}

/**
 * Muestra el detalle de los bienes filtrados por grupo y clase.
 */
function handle_detalle_grupo_clase(codigoGrupo, codigoClase) {
  var url = "View/simi_m_Reporte_Bien_x_Parametros/principal.php?operacion=bienes-grupo-clase";
  url += (codigoGrupo == "") ? "" : "&codigoGrupo=" + codigoGrupo;
  url += (codigoClase == "") ? "" : "&codigoClase=" + codigoClase;
  $.ajax(url).done(function (response) {
    console.log("hola mundo");
    $("#detalle_bienes_grupo_clase").html(response);
    $("#detalle_bienes_grupo_clase").data("codigoGrupo", codigoGrupo);
    $("#detalle_bienes_grupo_clase").data("codigoClase", codigoClase);
  });

}

/**
 * pagina el grid de la presentacion reporte grupo y clase.
 */
function handle_paginar_listado_bienes_grupo_clase(numeroPagina) {
  var url = "View/simi_m_Reporte_Bien_x_Parametros/principal.php?operacion=bienes-grupo-clase";
  var codigoGrupo = $("#detalle_bienes_grupo_clase").data("codigoGrupo");
  var codigoClase = $("#detalle_bienes_grupo_clase").data("codigoClase");

  url += (codigoGrupo == "") ? "" : "&codigoGrupo=" + codigoGrupo;
  url += (codigoClase == "") ? "" : "&codigoClase=" + codigoClase;
  url += (numeroPagina == "") ? "" : "&numeroPagina=" + numeroPagina;
  $.ajax(url).done(function (response) {
    console.log("hola mundo");
    $("#detalle_bienes_grupo_clase").html(response);
    $("#detalle_bienes_grupo_clase").data("codigoGrupo", codigoGrupo);
    $("#detalle_bienes_grupo_clase").data("codigoClase", codigoClase);
  });

}
