
  <table class="ReportA2C TABLE_border4" width="100%">
    <thead>
      <tr>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">ITEM</th>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4" style="width: 150px;">CODIGO PATRIMONIAL</th>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">DENOMINACION</th>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4"><?php if ($type == 'ALTA') print 'NRO. ALTA'; elseif ( $type == 'BAJA') print 'NRO. BAJA'; else print "NRO. ALTA/BAJA"; ?></th>
        <!-- <th class="Titulo_01_10px" bgcolor="#f4f4f4">NRO. BAJA</th> -->
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">MARCA</th>
        <!-- <th class="Titulo_01_10px" bgcolor="#f4f4f4">MODELO</th> -->
        <!-- <th class="Titulo_01_10px" bgcolor="#f4f4f4">COLOR</th> -->
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">MODELO</th>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">COLOR</th>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">TIPO</th>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">SERIE</th>
        <!-- <th class="Titulo_01_10px" bgcolor="#f4f4f4">CONCICION BIEN</th> -->
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">ESTADO BIEN</th>
      </tr>
    </thead>
    <tbody>
      <?php if($bienes) foreach ($bienes as $bien): ?>
        <tr>
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:right"><?php print $bien['orderId'] ?></td>
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['codigoPatrimonial'] ?></td>
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['denominacion'] ?></td>

          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left; background-color: <?php print $bien['condicionBien'] != A ? '#ffdada' : '#daffda' ?>"><?php print $bien['codigoAlta'] ?></td>
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['marca'] ?></td>
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['modelo'] ?></td>

          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:right"><?php print $bien['color'] ?></td>
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:right"><?php print $bien['tipo'] ?></td>
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['serie'] ?></td>

          <!-- <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['condicionBien'] ?></td> -->
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['estadoBien'] ?></td>
          <!-- <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['serie'] ?></td>
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['serie'] ?></td> -->
          <!-- <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><a href="javascript:">DET</a></td> -->
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

  <table width="881" border="0" cellspacing="1" cellpadding="0" bgcolor="#f4f4f4">
      <tr>
          <td width="725">
            <div id="div_paginacion">
              <?php if ($pager['cantidadPaginas'] > 1): ?>
              <div class="pagination">
                  <ul>
                      <?php if ($pager['indicePagina'] != 1): ?>
                        <li><a href="javascript: <?php print $pager['handlerFunction'];?>(1)" class="paginate">Inicial</a></li>
                        <li><a href="javascript: <?php print $pager['handlerFunction'];?>(<?php print $pager['indicePagina'] - 1; ?>)" class="paginate"><<</a></li>
                      <?php endif; ?>
                      <?php for ($index = $pager['limiteInferior']; $index <= $pager['limiteSuperior']; $index++): ?>
                          <?php if ($index == $pager['indicePagina']): ?>
                              <li class="active"><a><?php print $index; ?></a></li>
                          <?php else: ?>
                            <li><a href="javascript: <?php print $pager['handlerFunction'];?>(<?php print $index; ?>)" class="paginate"><?php print $index; ?></a></li>
                          <?php endif; ?>
                      <?php endfor; ?>
                      <?php if ($pager['indicePagina'] != $pager['cantidadPaginas']): ?>
                        <li><a href="javascript: <?php print $pager['handlerFunction'];?>(<?php print $pager['indicePagina'] + 1; ?>)" class="paginate">>></a></li>
                        <li><a href="javascript: <?php print $pager['handlerFunction'];?>(<?php print $pager['cantidadPaginas']; ?>)" class="paginate">Final</a></li>
                      <?php endif; ?>
                  </ul>
              </div>
              <?php endif; ?>
            </div>
          </td>
          <td width="150" class="texto_arial_azul_n_10">Total Documentos:<?php print $pager['cantidadRegistros'] ?></td>
      </tr>
  </table>
