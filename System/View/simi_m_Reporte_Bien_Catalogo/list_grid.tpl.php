
  <table class="ReportA2C TABLE_border4" width="100%">
    <thead>
      <tr>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">ITEM</th>
        <!-- <th class="Titulo_01_10px" bgcolor="#f4f4f4" style="width: 150px;">CANTIDAD DE BIENES</th> -->
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">CODIGO CATALOGO</th>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">NOMBRE CATALOGO</th>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">CODIGO GRUPO</th>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">NOMBRE GRUPO</th>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">CODIGO CLASE</th>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">NOMBRE CLASE</th>
        <th class="Titulo_01_10px" bgcolor="#f4f4f4">CANTIDAD DE BIENES</th>
        <!-- <th class="Titulo_01_10px" bgcolor="#f4f4f4">CANTIDAD</th> -->
      </tr>
    </thead>
    <tbody>
      <?php if($bienes) foreach ($bienes as $bien): ?>
        <tr>

          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['orderId'] ?></td>
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['codigoCatalogo'] ?></td>
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['nombreCatalogo'] ?></td>

          <!-- <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['cantidad'] ?></td> -->
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['codigoGrupo'] ?></td>
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['nombreGrupo'] ?></td>

          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['codigoClase'] ?></td>
          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:right"><?php print $bien['nombreClase'] ?></td>

          <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['cantidad'] ?></td>
          <!-- <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:right"><?php print $bien['cantidad'] ?></td> -->
          <!-- <td class="separador_borde" style="color:rgb(0, 156, 213); text-align:left"><?php print $bien['serie'] ?></td> -->

        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>

  <table width="881" border="0" cellspacing="1" cellpadding="0" bgcolor="#f4f4f4">
      <tr>
          <td width="725">
            <div id="div_paginacion">
              <?php if ($pager['cantidadPaginas'] > 1): ?>
              <div class="pagination">
                  <ul>
                      <?php if ($pager['indicePagina'] != 1): ?>
                        <li><a href="javascript: <?php print $pager['handlerFunction'];?>(1)" class="paginate">Inicial</a></li>
                        <li><a href="javascript: <?php print $pager['handlerFunction'];?>(<?php print $pager['indicePagina'] - 1; ?>)" class="paginate"><<</a></li>
                      <?php endif; ?>
                      <?php for ($index = $pager['limiteInferior']; $index <= $pager['limiteSuperior']; $index++): ?>
                          <?php if ($index == $pager['indicePagina']): ?>
                              <li class="active"><a><?php print $index; ?></a></li>
                          <?php else: ?>
                            <li><a href="javascript: <?php print $pager['handlerFunction'];?>(<?php print $index; ?>)" class="paginate"><?php print $index; ?></a></li>
                          <?php endif; ?>
                      <?php endfor; ?>
                      <?php if ($pager['indicePagina'] != $pager['cantidadPaginas']): ?>
                        <li><a href="javascript: <?php print $pager['handlerFunction'];?>(<?php print $pager['indicePagina'] + 1; ?>)" class="paginate">>></a></li>
                        <li><a href="javascript: <?php print $pager['handlerFunction'];?>(<?php print $pager['cantidadPaginas']; ?>)" class="paginate">Final</a></li>
                      <?php endif; ?>
                  </ul>
              </div>
              <?php endif; ?>
            </div>
          </td>
          <td width="150" class="texto_arial_azul_n_10">Total Documentos:<?php print $pager['cantidadRegistros'] ?></td>
      </tr>
  </table>
