<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../utils/css/stylesPaginacion.css" />

<table width="941" border="0" cellspacing="3" cellpadding="0">
    <tr>
        <td width="24">&nbsp;</td>
        <td width="710">&nbsp;</td>
        <td width="195" style="text-align:right">
          <!-- <button class="btn btn-primary" onclick="fShowFormulario_Asignacion('')"><i class="fa fa-plus-square" aria-hidden="true" style="font-size:14px" ></i> Agregar Asignación de Bienes</button> -->
        </td>
    </tr>

    <tr>
        <td>&nbsp;</td>
        <td colspan="2">

            <div id="tabs">
                <ul>
                    <li><a href="#tabs-1" >Por estado del Bien</a></li>
                </ul>
                <div id="tabs-1">
                    <table width="100%" border="0" cellpadding="0" cellspacing="2" class="TABLE_border4" bgcolor="#F4F4F4">
                      <tr>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td width="373">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td height="28">Grupo Genérico</td>
                        <td><select name="busc_cbo_grupo_generico" id="busc_cbo_grupo_generico" style="width:260px" onchange="handle_load_clases_by_group()">
                          <option value="">.:: Seleccione Grupo ::.</option>
                          <?php if($grupos) foreach ($grupos as $grupo): ?>
                            <option value="<?php print $grupo['codigo']?>">
                              <?php print $grupo['nombre']?>
                            </option>
                          <?php endforeach; ?>

                        </select></td>
                        <td width="83">Clase</td>
                        <td><select id="busc_CBO_CLASE" name="busc_CBO_CLASE" class="form-control" style="width:200px" <?=$disabled_text?> >
                          <option value="" selected="selected">:: Seleccione Clase ::</option>
                        </select></td>
                        <td width="130" rowspan="3" valign="bottom"><table width="124" border="0" cellpadding="0" cellspacing="0">
                          <tr>
                            <td width="85" height="40"><input name="button2" type="button" class="btn btn-primary" id="button2" value="Buscar" onclick="handle_filtrar_grid()" /></td>
                            <td width="39"><a href="#" onclick="handle_limpiar_filtro()"><img src="../webimages/iconos/quitar_flitros.png" alt="Quitar Filtro" width="25" height="25" border="0" /></a></td>
                          </tr>
                        </table></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td height="28">Código del bien</td>
                        <td><input type="text" name="busc_txt_codigo_bien" id="busc_txt_codigo_bien" style="width:150px" /></td>
                        <td>Resolución :</td>
                        <td width="228"><select id="BUSC_CBO_RESOLUCION_CATALOGO" name="BUSC_CBO_RESOLUCION_CATALOGO" class="form-control" style="width:200px" >
                          <option value="" selected="selected">:: Seleccione Resolución ::</option>
                          <?php if($resoluciones) foreach ($resoluciones as $resolucion): ?>
                            <option value="<?php print $resolucion['codigo']; ?>" <?=$select_resoluc?> >
                              <?php print $resolucion['numero'] ?>
                            </option>
                          <?php endforeach; ?>
                        </select></td>
                      </tr>
                      <tr>
                        <td>&nbsp;</td>
                        <td height="28"> Nombre del Bien </td>
                        <td><input type="text" name="busc_txt_denominacion" id="busc_txt_denominacion" style="width:350px" /></td>
                        <td>Condición :</td>
                        <td><select name="busc_cbo_condicion" id="busc_cbo_condicion" style="width:200px">
                          <option selected="selected" value="">:: Seleccione Condición ::</option>
                          <option value="A">ACTIVO</option>
                          <option value="E">EXCLUIDO</option>
                        </select></td>
                      </tr>
                      <tr>
                        <td width="14">&nbsp;</td>
                        <td width="115">&nbsp;</td>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                    </table>
                    <!-- <div id="graph-container" style="min-width: 310px; height: 400px; margin: 0 auto; text-align: center">
                      <br><br>
                      <button type="button" class="btn btn-primary" onclick="cargarReporteGrafico('')" style="margin: 0 auto">Cargar Reporte</button>
                    </div><hr><br>
                    <div style="text-align: right;">
                      <button type="button" class="btn btn-primary" onclick="handle_imprimir()" style="margin: 0 auto">Descargar Como PDF</button>
                    </div> -->
                    <br><br><hr>
                    <div id="detail-container" style="padding:0px 0px 10px 15px;">
                      <?php render('list_grid.tpl.php',['bienes' => $bienes, 'pager' => $pager]); ?>
                    </div>
                </div>
            </div>
        </td>
    </tr>
</table>
