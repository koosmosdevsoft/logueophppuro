<?
$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
//echo urldecode($url);

/* CREACION:        WILLIAMS ARENAS */
/* FECHA CREACION:  26-05-2020 */
/* DESCRIPCION:     IMPORTACION DE LIBRERIAS PDO */
require_once("../../Controller/BaseBD_PDO.php");
require_once('../../Model/BaseBD_PDOModel.php');
require_once("../../Model/BienPatrimonialModel.php");
/* FIN DE IMPORTACION DE LIBRERIAS PDO */

$objModel = new BienPatrimonialModel();

if(preg_match("/or|%20|%21|%22|%25|%27|%28|%29|%2A|%2B|%7B|%7C|%7D|%7E|%58|%5B|%5D|%2D|%3B|union|select|insert|update|delete|drop|alter|ASCII|set|declare|char|chr|create|SYSOBJECTS|-|'/", $url)) {
   //echo "coencide con alguno de estos cadenas";
	header("Location: ../../index.php");
}else {


//if (is_file('../../Controlador/C_sbn_ubigueo.php')){
	require_once("../../Controller/C_padron_ubigueo.php");
//}

$oPadron_Ubigueo	=	new Padron_Ubigueo;

$cod_dpto = $_REQUEST['cbo_depa'];

?>
<option value="-" selected="selected">[.Seleccione.]</option>
<?
/*
	$RESULT_PROV = $oPadron_Ubigueo->Lista_Provincia($cod_dpto);
*/

	/* CREACION:        WILLIAMS ARENAS */
    /* FECHA CREACION:  27-05-2020 */
    /* DESCRIPCION:     LISTADO DE PROVINCIAS */
    $data = array();    
    
    $dataModel_01 = [
      'cod_dpto'	=>$cod_dpto
    ];
    
    $data['DataListadoProvincia'] = $objModel->Lista_ProvinciaPDO($dataModel_01);
    $ArrayListadoProvincia = $data['DataListadoProvincia'];
    /* FIN DE LISTADO DE PROVINCIAS */

    if($ArrayListadoProvincia) foreach ($ArrayListadoProvincia as $ListadoProvincia):
    	$cod_prov = $ListadoProvincia['COD_PROV'];
    	$provincia = utf8_encode($ListadoProvincia['PROVINCIA']);

?>
<option value="<?=$cod_prov?>"><?=$provincia?></option>
<? 
	endforeach;
?>
<? }

?>