<? 
$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];
//echo urldecode($url);

/* CREACION:        WILLIAMS ARENAS */
/* FECHA CREACION:  26-05-2020 */
/* DESCRIPCION:     IMPORTACION DE LIBRERIAS PDO */
require_once("../../Controller/BaseBD_PDO.php");
require_once('../../Model/BaseBD_PDOModel.php');
require_once("../../Model/BienPatrimonialModel.php");
/* FIN DE IMPORTACION DE LIBRERIAS PDO */

$objModel = new BienPatrimonialModel();

if(preg_match("/or|%20|%21|%22|%25|%27|%28|%29|%2A|%2B|%7B|%7C|%7D|%7E|%58|%5B|%5D|%2D|%3B|union|select|insert|update|delete|drop|alter|ASCII|set|declare|char|chr|create|SYSOBJECTS|-|'/", $url)) {
   //echo "coencide con alguno de estos cadenas";
	header("Location: ../../index.php");
}else {

	require_once("../../Controller/C_padron_ubigueo.php");

	$oPadron_Ubigueo	=	new Padron_Ubigueo;
	
	$cod_dpto = $_REQUEST['cbo_depa'];
	$cod_prov = $_REQUEST['cbo_prov'];

?>
<option value="-" selected="selected">[.Seleccione.]</option>
<?
/*
	$RESULT_DISTRITO = $oPadron_Ubigueo->Lista_Distrito($cod_dpto, $cod_prov);
*/

	/* CREACION:        WILLIAMS ARENAS */
    /* FECHA CREACION:  26-05-2020 */
    /* DESCRIPCION:     LISTADO DE DISTRITO */
    $data = array();    
    
    $dataModel_01 = [
      'cod_dpto'	=>$cod_dpto,
      'cod_prov'	=>$cod_prov
    ];
    
    $data['DataListadoDistrito'] = $objModel->Lista_DistritoPDO($dataModel_01);
    $ArrayListadoDistrito = $data['DataListadoDistrito'];
    /* FIN DE LISTADO DE DISTRITO */

    if($ArrayListadoDistrito) foreach ($ArrayListadoDistrito as $ListadoDistrito):
    	$cod_dist = $ListadoDistrito['COD_DIST'];
    	$distrito = utf8_encode($ListadoDistrito['DISTRITO']);

	
?>
<option value="<?=$cod_dist?>"><?=$distrito?></option>
<?
	endforeach;
?>
<? }

?>
