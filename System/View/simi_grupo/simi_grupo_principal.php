<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 

<script>

function Guardar_Grupo(){
	
	var TXH_ID_USUARIO 		= $("#TXH_ID_USUARIO").val();
	var TXH_COD_GRUPO 		= $("#TXH_COD_GRUPO").val();
	var TXT_NRO_GRUPO		= $("#TXT_NRO_GRUPO").val();
	var TXT_NOM_GRUPO 		= $("#TXT_NOM_GRUPO").val();
	
	
	
	if(TXT_NRO_GRUPO == ''){
		alert('Ingrese Código del Grupo');
		 $("#TXT_NRO_GRUPO").focus();
		 
	}else if(TXT_NOM_GRUPO == ''){
		alert('Ingrese Nombre del Grupo');
		 $("#TXT_NOM_GRUPO").focus();
		 
	}else{

		carga_loading('div_lista');
		
		$.post("Model/M_simi_grupo.php?operacion=Guardar_Grupo",{ 
			TXH_ID_USUARIO:TXH_ID_USUARIO,
			TXH_COD_GRUPO:TXH_COD_GRUPO,
			TXT_NRO_GRUPO:TXT_NRO_GRUPO,
			TXT_NOM_GRUPO:TXT_NOM_GRUPO
		},function(data){
			
			if(data==1){
				mostrar_lista_Grupo();				
				$("#div_formulario").html('');
			}else{
				alert(data);
			}
			
		})

	}
	
}

function mostrar_lista_Grupo(){
	$.post("View/simi_grupo/simi_grupo_principal_list.php",{
	},function(data){
		$("#div_lista").html(data);
	})
}

function Eliminar_Grupo(COD_GRUPO_X){
	
	var TXH_ID_USUARIO 		= $("#TXH_ID_USUARIO").val();
	
	if(confirm('¿Confirma eliminar este Grupo?')){
		
		carga_loading('div_lista');
		
		$.post("Model/M_simi_grupo.php?operacion=Eliminar_Grupo",{ 
			COD_GRUPO_X:COD_GRUPO_X,
			TXH_ID_USUARIO:TXH_ID_USUARIO
		},function(data){
			
			if(data==1){
				mostrar_lista_Grupo();				
				$("#div_formulario").html('');
			}else{
				alert(data);
			}
			
		})
	}
	
}


function Mostrar_Ventana_Reg_Clase_x_Grupo(TXH_COD_DET_GRUPO_CLASE){
	
	var TXH_COD_GRUPO 		= $("#TXH_COD_GRUPO").val();	
	$("#div_formulario_grupo_clase").html('');
		
	if(TXH_COD_GRUPO == ''){
		alert('No ha registrado el Grupo');
		
	}else{
		
		$.post("View/simi_grupo/simi_grupo_principal_x_clase_form.php",{ 
			TXH_COD_GRUPO:TXH_COD_GRUPO,
			TXH_COD_DET_GRUPO_CLASE:TXH_COD_DET_GRUPO_CLASE
		},function(data){
			$("#div_formulario_grupo_clase").dialog({ 
					title: 'Registro de Clase x Grupo',
					width: 610,
					height: 280,
					resizable: "false",
					modal: true					
				}).html(data);
			});
	}
	
}

function Guardar_Clases_Por_Grupo(){
	
	var TXH_ID_USUARIO 				= $("#TXH_ID_USUARIO").val();
	var TXH_COD_DET_GRUPO_CLASE 	= $("#TXH_COD_DET_GRUPO_CLASE").val();
	var TXH_COD_GRUPO 				= $("#TXH_COD_GRUPO").val();
	var CBO_CLASE 					= $("#CBO_CLASE").val();
	var cbo_cta_cble 				= $("#cbo_cta_cble").val();
	var cbo_cta_depreciacion		= $("#cbo_cta_depreciacion").val();
	
	$.post("Model/M_simi_grupo.php?operacion=Guardar_Clase_x_Grupo",{ 
		TXH_ID_USUARIO:TXH_ID_USUARIO,
		TXH_COD_DET_GRUPO_CLASE:TXH_COD_DET_GRUPO_CLASE,
		TXH_COD_GRUPO:TXH_COD_GRUPO,
		CBO_CLASE:CBO_CLASE,
		cbo_cta_cble:cbo_cta_cble,
		cbo_cta_depreciacion:cbo_cta_depreciacion
	},function(data){
		
		if(data==1){
			$("#div_formulario_grupo_clase").dialog("close").html("");
			mostrar_lista_Clase_x_Grupo(TXH_COD_GRUPO);
			mostrar_lista_Grupo();			
		}else if(data==5){
			alert('Esta clase ya se encuentra registrada, seleccione otra clase.')
		}else{
			alert(data);
		}
		
	})
}



function mostrar_lista_Clase_x_Grupo(COD_GRUPO_X){
	carga_loading('div_lista_clase_x_grupo');
	$.post("View/simi_grupo/simi_grupo_principal_x_clase_list.php?x_COD_GRUPO="+COD_GRUPO_X,{ 
	},function(data){
		$("#div_lista_clase_x_grupo").html(data);
	})
}



function Eliminar_Clase_x_Grupo_ID(TXH_COD_DET_GRUPO_CLASE){
	
	var TXH_ID_USUARIO 				= $("#TXH_ID_USUARIO").val();
	var TXH_COD_GRUPO 				= $("#TXH_COD_GRUPO").val();
	
	if(confirm('Confirma eliminar esta clase?')){
		
		$.post("Model/M_simi_grupo.php?operacion=Eliminar_Clase_x_Grupo_ID",{ 
			TXH_ID_USUARIO:TXH_ID_USUARIO,
			TXH_COD_DET_GRUPO_CLASE:TXH_COD_DET_GRUPO_CLASE
		},function(data){
			
			if(data==1){
				$("#div_formulario_grupo_clase").dialog("close").html('');
				mostrar_lista_Clase_x_Grupo(TXH_COD_GRUPO);
				mostrar_lista_Grupo();				
			}else{
				alert(data);
			}
			
		})
	}		
}


/************************************************************************************/
/************************************************************************************/
/************************************************************************************/

/*
function Mostrar_Ventana_Reg_Clase_x_Grupo(TXH_COD_DET_GRUPO_CLASE){
	


	var TXH_COD_GRUPO 		= $("#TXH_COD_GRUPO").val();
	var TXT_NRO_GRUPO 		= $("#TXT_NRO_GRUPO").val();
	var TXT_NOM_GRUPO 		= $("#TXT_NOM_GRUPO").val();
	
	$.post("View/simi_grupo/simi_grupo_principal_x_clase_form.php",{ 
		TXH_COD_GRUPO:TXH_COD_GRUPO,
		TXT_NRO_GRUPO:TXT_NRO_GRUPO,
		TXT_NOM_GRUPO:TXT_NOM_GRUPO,
		TXH_COD_DET_GRUPO_CLASE:TXH_COD_DET_GRUPO_CLASE
	},function(data){
		
		var buttons = {
			"Guardar": function () { Guardar_Clase_x_Grupo() }
		};
		
		if(TXH_COD_DET_GRUPO_CLASE != '') {
			buttons['Eliminar'] = function() { Eliminar_Clase_x_Grupo_ID( TXH_COD_DET_GRUPO_CLASE) };
		}
		
		buttons['Cerrar'] = function() { $(this).dialog("close") };

		$("#div_formulario_grupo_clase").dialog({ 
				title: 'Registro de Clase x Grupo',
				width: 590,
				height: 290,
				resizable: "false",
				modal: true,
				buttons: buttons
				
			}).html(data);
		})
}
*/











</script>

<table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td style="width:780px" valign="top"><table width="764" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
      <tr>
        <td width="639"  class="Titulo_plomo_25px">Registro de Grupos</td>
        <td width="116"  class="Titulo_plomo_25px"><input name="button" type="button" class="btn btn-primary" id="button" value="Agregar Grupo" onclick='ajaxpage("View/simi_grupo/simi_grupo_principal_form.php","div_formulario")' /></td>
      </tr>
      <tr>
        <td colspan="2"  class="Titulo_02_17px"><hr /></td>
      </tr>
      <tr>
        <td colspan="2"  class="Titulo_02_17px">&nbsp;</td>
      </tr>
      </table>
      <div id="div_lista"><? include_once('simi_grupo_principal_list.php')?></div></td>
    <td valign="top"><div id="div_formulario" ></div>
    </td>
  </tr>
</table>
<div id="div_formulario_grupo_clase"></div>
