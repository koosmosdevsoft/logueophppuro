<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<LINK href="../../../utils/css/SGI_Estilos.css" rel="stylesheet"> 

<?
require_once('../../Controller/C_simi_clases.php');
require_once('../../Controller/C_simi_grupo.php');
require_once('../../Controller/C_simi_cuenta_contable.php');

$oSimi_Clase			=	new Simi_Clase;
$oSimi_Grupo			=	new Simi_Grupo;
$oSimi_Cuenta_Contable	=	new Simi_Cuenta_Contable;

$TXH_COD_GRUPO 				= $_REQUEST['TXH_COD_GRUPO'];
$TXH_COD_DET_GRUPO_CLASE 	= $_REQUEST['TXH_COD_DET_GRUPO_CLASE'];


if($TXH_COD_GRUPO !=''){
	
	$Result_Ver_Grupo	= $oSimi_Grupo->Simi_Ver_Grupo_x_Codigo($TXH_COD_GRUPO);
	$NRO_GRUPO_G	 	= (odbc_result($Result_Ver_Grupo,"NRO_GRUPO"));
	$DESC_GRUPO_G	 	= (odbc_result($Result_Ver_Grupo,"DESC_GRUPO"));
	
	if($TXH_COD_DET_GRUPO_CLASE !=''){
		
		$Result_List_Clase_x_Grupo_01 = $oSimi_Grupo->Simi_Ver_Clase_x_Grupo_x_Codigo_DET($TXH_COD_DET_GRUPO_CLASE);
		
		$COD_CLASE_E	 	= odbc_result($Result_List_Clase_x_Grupo_01,"COD_CLASE");
		$COD_GRUPO_E	 	= odbc_result($Result_List_Clase_x_Grupo_01,"COD_GRUPO");
		$COD_CTA_CONTABLE_E	 	= odbc_result($Result_List_Clase_x_Grupo_01,"COD_CTA_CONTABLE");
		$COD_CTA_CONTABLE_DEPREC_E	 	= odbc_result($Result_List_Clase_x_Grupo_01,"COD_CTA_CONTABLE_DEPREC");
	}

}
    





?>




<table width="583" border="0" cellpadding="0" cellspacing="1" class="FrmBuscadorFlotante TABLE_border4" bgcolor="#f4f4f4">
  <tr>
    <td width="10">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
    <td align="center">&nbsp;</td>
  </tr>
  <tr>
    <td height="25">&nbsp;</td>
    <td width="152"> Grupo
    <input name="TXH_COD_DET_GRUPO_CLASE" type="hidden" id="TXH_COD_DET_GRUPO_CLASE" value="<?=$TXH_COD_DET_GRUPO_CLASE?>" style="width:60px" /></td>
    <td width="14">:</td>
    <td colspan="4"><input name="TXT_X1_NRO_GRUPO" type="text" id="TXT_X1_NRO_GRUPO" style="width:40px" value="<?=$NRO_GRUPO_G?>" readonly="readonly" />
    <input name="TXT_X1_NOMBRE_GRUPO" type="text" id="TXT_X1_NOMBRE_GRUPO" style="width:300px" value="<?=$DESC_GRUPO_G?>" readonly="readonly" /></td>
  </tr>
  <tr>
    <td height="28">&nbsp;</td>
    <td>Clase</td>
    <td>:</td>
    <td colspan="4"><select id="CBO_CLASE" name="CBO_CLASE" class="form-control" style="width:370px">
      <option value="0" selected="selected">:: Seleccione Clase ::</option>
      <?
		    $Result_Clase = $oSimi_Clase->Simi_Listar_Clase();
			while (odbc_fetch_row($Result_Clase)){
							
			$COD_CLASE_X1	 		= odbc_result($Result_Clase,"COD_CLASE");
			$NRO_CLASE_X1	 		= utf8_encode(odbc_result($Result_Clase,"NRO_CLASE"));
			$DESC_CLASE_X1	 		= utf8_encode(odbc_result($Result_Clase,"DESC_CLASE"));
			
			if($COD_CLASE_X1==$COD_CLASE_E){
				$select = 'selected="selected"';
			}else{
				$select = '';
			}
							
		?>
      <option value="<?=$COD_CLASE_X1?>" <?=$select?>><?=$NRO_CLASE_X1?> - <?=$DESC_CLASE_X1?>
        </option>
      <?
			}
		?>
    </select></td>
  </tr>
  <tr>
    <td height="28">&nbsp;</td>
    <td>Uso de la Cuenta</td>
    <td>:</td>
    <td width="144"><input name="textfield" type="text" disabled="disabled" id="textfield" value="Uso Estatal" style="width:90px" /></td>
    <td width="97">Tipo de Cuenta:</td>
    <td width="143"><input name="textfield2" type="text" disabled="disabled" id="textfield2" value="Cuenta de Orden" style="width:120px" /></td>
    <td width="13">&nbsp;</td>
  </tr>
  <tr>
    <td height="28">&nbsp;</td>
    <td>Nro Cta Contable</td>
    <td>:</td>
    <td colspan="4"><select id="cbo_cta_cble" name="cbo_cta_cble" class="form-control" style="width:370px">
      <option value="0" selected="selected">:: Seleccione Nro Cuenta Contable ::</option>
      <?
		    $Result_Cta_Contable = $oSimi_Cuenta_Contable->Simi_Listar_Cuenta_Contable_Parametros('', '', 'A', 'E', '-');
			while (odbc_fetch_row($Result_Cta_Contable)){
							
							$COD_CTA_CONTABLE		 	= odbc_result($Result_Cta_Contable,"COD_CTA_CONTABLE");
							$NRO_CUENTA_X1	 			= utf8_encode(odbc_result($Result_Cta_Contable,"NRO_CUENTA"));
							$NRO_SUBCUENTA_X1	 		= utf8_encode(odbc_result($Result_Cta_Contable,"NRO_SUBCUENTA"));
							$NRO_DIVISIONARIA_X1	 	= utf8_encode(odbc_result($Result_Cta_Contable,"NRO_DIVISIONARIA"));
							$NRO_CTA_CONTABLE_X1	 	= utf8_encode(odbc_result($Result_Cta_Contable,"NRO_CTA_CONTABLE"));
							$NOM_CTA_CONTABLE_X1	 	= utf8_encode(odbc_result($Result_Cta_Contable,"NOM_CTA_CONTABLE"));
							$DES_USO_CUENTA_X1	 		= utf8_encode(odbc_result($Result_Cta_Contable,"DES_USO_CUENTA"));
							$DES_TIP_CUENTA_X1	 		= utf8_encode(odbc_result($Result_Cta_Contable,"DES_TIP_CUENTA"));
							$DES_ESTADO_CUENTA_X1	 	= utf8_encode(odbc_result($Result_Cta_Contable,"DES_ESTADO_CUENTA"));
							//$TEXT_NRO_CTA_CONTABLE_X1	 	= utf8_encode(odbc_result($Result_Cta_Contable,"TEXT_NRO_CTA_CONTABLE"));
							
							$TEXT_NRO_CTA_CONTABLE_X1_OK =  $NRO_CTA_CONTABLE_X1." _ _ _ _ ".$NOM_CTA_CONTABLE_X1;
							
			
			if($COD_CTA_CONTABLE_E==$COD_CTA_CONTABLE){
				$select_Cta_cble = 'selected="selected"';
			}else{
				$select_Cta_cble = '';
			}
							
		?>
      <option value="<?=$COD_CTA_CONTABLE?>" <?=$select_Cta_cble?>><?=$TEXT_NRO_CTA_CONTABLE_X1_OK?></option>
      <?
			}
		?>
    </select></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>Nro. Cta Depreciación</td>
    <td>:</td>
    <td colspan="4"><select id="cbo_cta_depreciacion" name="cbo_cta_depreciacion" class="form-control" style="width:370px">
      <option value="0" selected="selected">:: Seleccione Nro Cuenta Depreciación::</option>
      <?
		    $Result_Cta_Depreciacion = $oSimi_Cuenta_Contable->Simi_Listar_Cuenta_Contable_Parametros('', '', 'A', 'E', '-');
			while (odbc_fetch_row($Result_Cta_Depreciacion)){
							
							$COD_CTA_CONTABLE_DEPREC		 	= odbc_result($Result_Cta_Depreciacion,"COD_CTA_CONTABLE");
							$NRO_CUENTA_X1_DEPREC	 			= utf8_encode(odbc_result($Result_Cta_Depreciacion,"NRO_CUENTA"));
							$NRO_SUBCUENTA_X1_DEPREC	 		= utf8_encode(odbc_result($Result_Cta_Depreciacion,"NRO_SUBCUENTA"));
							$NRO_DIVISIONARIA_X1_DEPREC	 	= utf8_encode(odbc_result($Result_Cta_Depreciacion,"NRO_DIVISIONARIA"));
							$NRO_CTA_CONTABLE_X1_DEPREC	 	= utf8_encode(odbc_result($Result_Cta_Depreciacion,"NRO_CTA_CONTABLE"));
							$NOM_CTA_CONTABLE_X1_DEPREC	 	= utf8_encode(odbc_result($Result_Cta_Depreciacion,"NOM_CTA_CONTABLE"));
							$DES_USO_CUENTA_X1_DEPREC	 		= utf8_encode(odbc_result($Result_Cta_Depreciacion,"DES_USO_CUENTA"));
							$DES_TIP_CUENTA_X1_DEPREC	 		= utf8_encode(odbc_result($Result_Cta_Depreciacion,"DES_TIP_CUENTA"));
							$DES_ESTADO_CUENTA_X1_DEPREC	 	= utf8_encode(odbc_result($Result_Cta_Depreciacion,"DES_ESTADO_CUENTA"));
							//$TEXT_NRO_CTA_CONTABLE_X1	 	= utf8_encode(odbc_result($Result_Cta_Contable,"TEXT_NRO_CTA_CONTABLE"));
							
							$TEXT_NRO_CTA_CONTABLE_X1_DEPREC =  $NRO_CTA_CONTABLE_X1_DEPREC." _ _ _ _ ".$NOM_CTA_CONTABLE_X1_DEPREC;
							
			
			if($COD_CTA_CONTABLE_DEPREC_E==$COD_CTA_CONTABLE_DEPREC){
				$select_Cta_Deprec = 'selected="selected"';
			}else{
				$select_Cta_Deprec = '';
			}
							
		?>
      <option value="<?=$COD_CTA_CONTABLE_DEPREC?>" <?=$select_Cta_Deprec?>> <?=$TEXT_NRO_CTA_CONTABLE_X1_DEPREC?> </option>
      <?
			}
		?>
    </select></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="5"><hr /></td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="6"><table width="404" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="151">&nbsp;</td>
        <td width="96"><input name="button2" type="button" class="btn btn-primary" id="button2" value="Guardar" onclick="Guardar_Clases_Por_Grupo()" /></td>
        <td width="145"><?
	  if($TXH_COD_DET_GRUPO_CLASE != ''){
      ?>
          <input name="button3" type="button" class="btn btn-primary" id="button3" value="Eliminar" onclick="Eliminar_Clase_x_Grupo_ID('<?=$TXH_COD_DET_GRUPO_CLASE?>')" />
         <? } ?>
          </td>
      </tr>
      <tr>
        <td colspan="3">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
