<?php

class CatalogoModel extends BaseModel{
	
    	
	public function __construct(){
		$this->connect();
	}

		
	function Listar_Grupos_Catalogo(){
		$params = [];		
		$sqlQuery = "SELECT * FROM  TBL_MUEBLES_GRUPO  WHERE ID_ESTADO = 1  ORDER BY COD_GRUPO ";
		return $this->Consultar($sqlQuery, $params);
	}
	
	
	function Ver_Datos_Catalogo_x_ID($filtros=[]){
		$params = [];	
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['ID_CATALOGO'], "tipo"=>"int"];	
		$sqlQuery = "SELECT * FROM  TBL_MUEBLES_CATALOGO  WHERE ID_CATALOGO = (?)";
		return $this->Consultar($sqlQuery, $params);
	}
	
		
}
?>