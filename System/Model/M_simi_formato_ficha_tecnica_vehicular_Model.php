<?php

	//require_once $_SERVER['DOCUMENT_ROOT'].'SGISBN/System/Controller/C_Interconexion_SQL.php';

class M_simi_formato_ficha_tecnica_vehicular_Model extends Database
{



		//Metodo Contructor
		public function __construct(){

			parent::__construct();

		}

		//Metodo Total Filas
	function F_Total_Buscar_BienesVehiculos_Parametros_Gral($COD_ENTIDAD, $COD_PATRIMONIAL, $DENOMINACION)
	{
		if($this->conectar()==true){


			if($COD_PATRIMONIAL != ''){
				$COND1 = " AND MP.CODIGO_PATRIMONIAL LIKE '%$COD_PATRIMONIAL%' ";
			}else{
				$COND1 = "";
			}

			if($DENOMINACION != ''){
				$COND2 = " AND MP.DENOMINACION_BIEN LIKE '%$DENOMINACION%' ";
			}else{
				$COND2 = "";
			}


			$SQL_ORIGEN="
			SELECT COUNT(*) AS TOT_REG
			FROM TBL_MUEBLES_UE_BIE_FT_VEHICULAR B
			LEFT JOIN TBL_MUEBLES_UE_BIEN_PATRIMONIAL MP ON
				B.COD_ENTIDAD		= MP.COD_ENTIDAD AND
				B.COD_UE_BIEN_PATRI	= MP.COD_UE_BIEN_PATRI
			WHERE B.ID_ESTADO = '1'
			AND B.COD_ENTIDAD = '$COD_ENTIDAD'
			$COND1 $COND2
			";

			$resultado = $this->execute($SQL_ORIGEN);
			return $resultado;
		}

	}

	//Metodo Listar
	function F_Lista_Buscar_Bienes_Vehiculos_x_Entidad_Parametros_Gral($INI, $FIN, $COD_ENTIDAD, $COD_PATRIMONIAL, $DENOMINACION){
		if($this->conectar()==true){


			if($COD_PATRIMONIAL != ''){
				$COND1 = " AND MP.CODIGO_PATRIMONIAL LIKE '%$COD_PATRIMONIAL%' ";
			}else{
				$COND1 = "";
			}

			if($DENOMINACION != ''){
				$COND2 = " AND MP.DENOMINACION_BIEN LIKE '%$DENOMINACION%' ";
			}else{
				$COND2 = "";
			}


			$SQL_ORIGEN="SELECT
			ROW_NUMBER() OVER (order by COD_UE_FT_VEHICULAR) AS ROW_NUMBER_ID, MP.CODIGO_PATRIMONIAL, MP.DENOMINACION_BIEN,
			B.*, BA.FECHA_DOCUMENTO_ADQUIS, MP.VALOR_ADQUIS
			FROM TBL_MUEBLES_UE_BIE_FT_VEHICULAR B
			LEFT JOIN TBL_MUEBLES_UE_BIEN_PATRIMONIAL MP ON
				B.COD_ENTIDAD		= MP.COD_ENTIDAD AND
				B.COD_UE_BIEN_PATRI	= MP.COD_UE_BIEN_PATRI
			LEFT JOIN TBL_MUEBLES_UE_BIEN_ALTA BA ON
				MP.COD_ENTIDAD  = BA.COD_ENTIDAD AND
				MP.COD_UE_BIEN_ALTA = BA.COD_UE_BIEN_ALTA
			WHERE B.ID_ESTADO = '1'
			AND B.COD_ENTIDAD = '$COD_ENTIDAD'
			$COND1 $COND2
			";

			$sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ORDER BY 1";

			$resultado = $this->execute($sql);



			return $resultado;
		}

	}




	//Metodo Total Filas-Muebles Bienes Patrimonial-Vehiculos
	function F_Total_Muebles_BienesPatrimonialVehiculos_x_Entidad($COD_ENTIDAD, $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN){
		if($this->conectar()==true){


			$SQL_ORIGEN="
			SELECT COUNT(*) AS TOT_REG
			FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL B
			WHERE B.ID_ESTADO = '1'
			AND B.COD_ENTIDAD = '$COD_ENTIDAD' AND CODIGO_PATRIMONIAL LIKE '%$CODIGO_PATRIMONIAL%' AND DENOMINACION_BIEN LIKE '%$DENOMINACION_BIEN%'
			AND ID_CATALOGO IN (SELECT ID_CATALOGO FROM TBL_MUEBLES_CATALOGO WHERE NRO_GRUPO = '67' AND NRO_CLASE = '82')
			";
			$resultado = $this->execute($SQL_ORIGEN);
			return $resultado;
		}

	}

	//Metodo Listar-Muebles Bienes Patrimonial-Vehiculos
	function F_Lista_Muebles_BienesPatrimonialVehiculos_x_Entidad($INI, $FIN, $COD_ENTIDAD, $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN){
		if($this->conectar()==true){


			$SQL_ORIGEN="SELECT
			ROW_NUMBER() OVER (order by B.COD_UE_BIEN_PATRI) AS ROW_NUMBER_ID, CODIGO_PATRIMONIAL, B.COD_UE_BIEN_PATRI,		DENOMINACION_BIEN, ISNULL(COD_UE_FT_VEHICULAR, '')[COD_UE_FT_VEHICULAR]
			FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL B
			LEFT JOIN TBL_MUEBLES_UE_BIE_FT_VEHICULAR C ON
				B.COD_ENTIDAD = C.COD_ENTIDAD AND
				B.COD_UE_BIEN_PATRI = C.COD_UE_BIEN_PATRI AND C.ID_ESTADO = 1
			WHERE B.ID_ESTADO = '1'
			AND B.COD_ENTIDAD = '$COD_ENTIDAD' AND CODIGO_PATRIMONIAL LIKE '%$CODIGO_PATRIMONIAL%' AND DENOMINACION_BIEN LIKE '%$DENOMINACION_BIEN%'
			AND ID_CATALOGO IN (SELECT ID_CATALOGO FROM TBL_MUEBLES_CATALOGO WHERE NRO_GRUPO = '67' AND NRO_CLASE = '82')
			";

			$sql = "SELECT * FROM ( ".$SQL_ORIGEN." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ORDER BY 1";



			$resultado = $this->execute($sql);



			return $resultado;
		}

	}


	/* Metodo  Insertar TBL_MUEBLES_UE_BIE_FT_VEHICULAR */
	function F_Insertar_TBL_MUEBLES_UE_BIE_FT_VEHICULAR
		(
			$COD_UE_FT_VEHICULAR_hide,
			$COD_USUARIO_hide,
			$COD_ENTIDAD,
			$COD_UE_BIEN_PATRI,
			$DENOMINACION,
			$PLACA,
			$CARROCERIA,
			$MARCA,
			$MODELO,
			$CATEGORIA,
			$NRO_CHASIS,
			$NRO_EJES,
			$NRO_MOTOR,
			$NRO_SERIE,
			$ANIO_FABRICACION,
			$COLOR,
			$COMBUSTIBLE,
			$TRANSMISION,
			$CILINDRADA,
			$KILOMETRAJE,
			$TARJETA_PROPIEDAD,
			$CILINDROS,
			$B_CILINDROS,
			$CARBURADOR,
			$B_CARBURADOR,
			$DISTRIBUIDOR,
			$B_DISTRIBUIDOR,
			$BOMBA_GASOLINA,
			$B_BOMBA_GASOLINA,
			$PURIFICADOR_AIRE,
			$B_PURIFICADOR_AIRE,
			$BOMBA_FRENOS,
			$B_BOMBA_FRENOS,
			$ZAPATAS_TAMBORES,
			$B_ZAPATAS_TAMBORES,
			$DISCOS_PASTILLAS,
			$B_DISCOS_PASTILLAS,
			$RADIOADOR,
			$B_RADIOADOR,
			$VENTILADOR,
			$B_VENTILADOR,
			$BOMBA_AGUA,
			$B_BOMBA_AGUA,
			$MOTOR_ARRANQUE,
			$B_MOTOR_ARRANQUE,
			$BATERIA,
			$B_BATERIA,
			$ALTERNADOR,
			$B_ALTERNADOR,
			$BOBINA,
			$B_BOBINA,
			$RELAY_ALTERNADOR,
			$B_RELAY_ALTERNADOR,
			$FAROS_DELANTEROS,
			$B_FAROS_DELANTEROS,
			$DIRECCIONALES_DELANTERAS,
			$B_DIRECCIONALES_DELANTERAS,
			$LUCES_POSTERIORES,
			$B_LUCES_POSTERIORES,
			$DIRECCIONALES_POSTERIORES,
			$B_DIRECCIONALES_POSTERIORES,
			$AUTO_RADIO,
			$B_AUTO_RADIO,
			$PARLANTES,
			$B_PARLANTES,
			$CLAXON,
			$B_CLAXON,
			$CIRCUITO_LUCES,
			$B_CIRCUITO_LUCES,
			$CAJA_CAMBIOS,
			$B_CAJA_CAMBIOS,
			$BOMBA_EMBRAGUE,
			$B_BOMBA_EMBRAGUE,
			$CAJA_TRANSFERENCIA,
			$B_CAJA_TRANSFERENCIA,
			$DIFERENCIAL_TRASERO,
			$B_DIFERENCIAL_TRASERO,
			$DIFERENCIAL_DELANTERO,
			$B_DIFERENCIAL_DELANTERO,
			$VOLANTE,
			$B_VOLANTE,
			$CANA_DIRECCION,
			$B_CANA_DIRECCION,
			$CREMALLERA,
			$B_CREMALLERA,
			$ROTULAS,
			$B_ROTULAS,
			$AMORTIGUADORES,
			$B_AMORTIGUADORES,
			$BARRA_TORSION,
			$B_BARRA_TORSION,
			$BARRA_ESTABILIZADOR,
			$B_BARRA_ESTABILIZADOR,
			$LLANTAS,
			$B_LLANTAS,
			$CAPOT_MOTOR,
			$B_CAPOT_MOTOR,
			$CAPOT_MALETERA,
			$B_CAPOT_MALETERA,
			$PARACHOQUES_DELANTEROS,
			$B_PARACHOQUES_DELANTEROS,
			$PARACHOQUES_POSTERIORES,
			$B_PARACHOQUES_POSTERIORES,
			$LUNAS_LATERALES,
			$B_LUNAS_LATERALES,
			$LUNAS_CORTAVIENTO,
			$B_LUNAS_CORTAVIENTO,
			$PARABRISAS_DELANTERO,
			$B_PARABRISAS_DELANTERO,
			$PARABRISAS_POSTERIOR,
			$B_PARABRISAS_POSTERIOR,
			$TANQUE_COMBUSTIBLE,
			$B_TANQUE_COMBUSTIBLE,
			$PUERTAS,
			$B_PUERTAS,
			$ASIENTOS,
			$B_ASIENTOS,
			$AIRE_ACONDICIONADO,
			$B_AIRE_ACONDICIONADO,
			$ALARMA,
			$B_ALARMA,
			$PLUMILLAS,
			$B_PLUMILLAS,
			$ESPEJOS,
			$B_ESPEJOS,
			$CINTURONES_SEGURIDAD,
			$B_CINTURONES_SEGURIDAD,
			$ANTENA,
			$B_ANTENA,
			$OTRAS_CARACTERISTICAS_RELEVANTES,
			$VALOR_TASACION,
			$ID_ESTADO,
			$SIMI_USUARIO_CREACION,
			$SIMI_FECHA_CREACION,
			$SIMI_USUARIO_MODIFICA,
			$SIMI_FECHA_MODIFICA,
			$SIMI_USUARIO_ELIMINA,
			$SIMI_FECHA_ELIMINA,
			$FECHA_REGISTRO

		){
		if($this->conectar()==true){
			if ($COD_UE_FT_VEHICULAR_hide == 0 or $COD_UE_FT_VEHICULAR_hide == 0)
			{

			/* REGISTRA EN TABLA DE CARACTERISTICAS DE BIENES VEHICULOS */
				$consulta = " INSERT INTO TBL_MUEBLES_UE_BIE_FT_VEHICULAR ( ";
				$consulta .= "COD_ENTIDAD, ";
				$consulta .= "COD_UE_BIEN_PATRI, ";
				$consulta .= "DENOMINACION, ";
				$consulta .= "PLACA, ";
				$consulta .= "CARROCERIA, ";
				$consulta .= "MARCA, ";
				$consulta .= "MODELO, ";
				$consulta .= "CATEGORIA, ";
				$consulta .= "NRO_CHASIS, ";
				$consulta .= "NRO_EJES, ";
				$consulta .= "NRO_MOTOR, ";
				$consulta .= "NRO_SERIE, ";
				$consulta .= "ANIO_FABRICACION, ";
				$consulta .= "COLOR, ";
				$consulta .= "COMBUSTIBLE, ";
				$consulta .= "TRANSMISION, ";
				$consulta .= "CILINDRADA, ";
				$consulta .= "KILOMETRAJE, ";
				$consulta .= "TARJETA_PROPIEDAD, ";
				$consulta .= "CILINDROS, ";
				$consulta .= "B_CILINDROS, ";
				$consulta .= "CARBURADOR, ";
				$consulta .= "B_CARBURADOR, ";
				$consulta .= "DISTRIBUIDOR, ";
				$consulta .= "B_DISTRIBUIDOR, ";
				$consulta .= "BOMBA_GASOLINA, ";
				$consulta .= "B_BOMBA_GASOLINA, ";
				$consulta .= "PURIFICADOR_AIRE, ";
				$consulta .= "B_PURIFICADOR_AIRE, ";
				$consulta .= "BOMBA_FRENOS, ";
				$consulta .= "B_BOMBA_FRENOS, ";
				$consulta .= "ZAPATAS_TAMBORES, ";
				$consulta .= "B_ZAPATAS_TAMBORES, ";
				$consulta .= "DISCOS_PASTILLAS, ";
				$consulta .= "B_DISCOS_PASTILLAS, ";
				$consulta .= "RADIOADOR, ";
				$consulta .= "B_RADIOADOR, ";
				$consulta .= "VENTILADOR, ";
				$consulta .= "B_VENTILADOR, ";
				$consulta .= "BOMBA_AGUA, ";
				$consulta .= "B_BOMBA_AGUA, ";
				$consulta .= "MOTOR_ARRANQUE, ";
				$consulta .= "B_MOTOR_ARRANQUE, ";
				$consulta .= "BATERIA, ";
				$consulta .= "B_BATERIA, ";
				$consulta .= "ALTERNADOR, ";
				$consulta .= "B_ALTERNADOR, ";
				$consulta .= "BOBINA, ";
				$consulta .= "B_BOBINA, ";
				$consulta .= "RELAY_ALTERNADOR, ";
				$consulta .= "B_RELAY_ALTERNADOR, ";
				$consulta .= "FAROS_DELANTEROS, ";
				$consulta .= "B_FAROS_DELANTEROS, ";
				$consulta .= "DIRECCIONALES_DELANTERAS, ";
				$consulta .= "B_DIRECCIONALES_DELANTERAS, ";
				$consulta .= "LUCES_POSTERIORES, ";
				$consulta .= "B_LUCES_POSTERIORES, ";
				$consulta .= "DIRECCIONALES_POSTERIORES, ";
				$consulta .= "B_DIRECCIONALES_POSTERIORES, ";
				$consulta .= "AUTO_RADIO, ";
				$consulta .= "B_AUTO_RADIO, ";
				$consulta .= "PARLANTES, ";
				$consulta .= "B_PARLANTES, ";
				$consulta .= "CLAXON, ";
				$consulta .= "B_CLAXON, ";
				$consulta .= "CIRCUITO_LUCES, ";
				$consulta .= "B_CIRCUITO_LUCES, ";
				$consulta .= "CAJA_CAMBIOS, ";
				$consulta .= "B_CAJA_CAMBIOS, ";
				$consulta .= "BOMBA_EMBRAGUE, ";
				$consulta .= "B_BOMBA_EMBRAGUE, ";
				$consulta .= "CAJA_TRANSFERENCIA, ";
				$consulta .= "B_CAJA_TRANSFERENCIA, ";
				$consulta .= "DIFERENCIAL_TRASERO, ";
				$consulta .= "B_DIFERENCIAL_TRASERO, ";
				$consulta .= "DIFERENCIAL_DELANTERO, ";
				$consulta .= "B_DIFERENCIAL_DELANTERO, ";
				$consulta .= "VOLANTE, ";
				$consulta .= "B_VOLANTE, ";
				$consulta .= "CANA_DIRECCION, ";
				$consulta .= "B_CANA_DIRECCION, ";
				$consulta .= "CREMALLERA, ";
				$consulta .= "B_CREMALLERA, ";
				$consulta .= "ROTULAS, ";
				$consulta .= "B_ROTULAS, ";
				$consulta .= "AMORTIGUADORES, ";
				$consulta .= "B_AMORTIGUADORES, ";
				$consulta .= "BARRA_TORSION, ";
				$consulta .= "B_BARRA_TORSION, ";
				$consulta .= "BARRA_ESTABILIZADOR, ";
				$consulta .= "B_BARRA_ESTABILIZADOR, ";
				$consulta .= "LLANTAS, ";
				$consulta .= "B_LLANTAS, ";
				$consulta .= "CAPOT_MOTOR, ";
				$consulta .= "B_CAPOT_MOTOR, ";
				$consulta .= "CAPOT_MALETERA, ";
				$consulta .= "B_CAPOT_MALETERA, ";
				$consulta .= "PARACHOQUES_DELANTEROS, ";
				$consulta .= "B_PARACHOQUES_DELANTEROS, ";
				$consulta .= "PARACHOQUES_POSTERIORES, ";
				$consulta .= "B_PARACHOQUES_POSTERIORES, ";
				$consulta .= "LUNAS_LATERALES, ";
				$consulta .= "B_LUNAS_LATERALES, ";
				$consulta .= "LUNAS_CORTAVIENTO, ";
				$consulta .= "B_LUNAS_CORTAVIENTO, ";
				$consulta .= "PARABRISAS_DELANTERO, ";
				$consulta .= "B_PARABRISAS_DELANTERO, ";
				$consulta .= "PARABRISAS_POSTERIOR, ";
				$consulta .= "B_PARABRISAS_POSTERIOR, ";
				$consulta .= "TANQUE_COMBUSTIBLE, ";
				$consulta .= "B_TANQUE_COMBUSTIBLE, ";
				$consulta .= "PUERTAS, ";
				$consulta .= "B_PUERTAS, ";
				$consulta .= "ASIENTOS, ";
				$consulta .= "B_ASIENTOS, ";
				$consulta .= "AIRE_ACONDICIONADO, ";
				$consulta .= "B_AIRE_ACONDICIONADO, ";
				$consulta .= "ALARMA, ";
				$consulta .= "B_ALARMA, ";
				$consulta .= "PLUMILLAS, ";
				$consulta .= "B_PLUMILLAS, ";
				$consulta .= "ESPEJOS, ";
				$consulta .= "B_ESPEJOS, ";
				$consulta .= "CINTURONES_SEGURIDAD, ";
				$consulta .= "B_CINTURONES_SEGURIDAD, ";
				$consulta .= "ANTENA, ";
				$consulta .= "B_ANTENA, ";
				$consulta .= "OTRAS_CARACTERISTICAS_RELEVANTES, ";
				$consulta .= "VALOR_TASACION, ";
				$consulta .= "ID_ESTADO, ";
				$consulta .= "SIMI_USUARIO_CREACION, ";
				$consulta .= "SIMI_FECHA_CREACION, ";
				$consulta .= "SIMI_USUARIO_MODIFICA, ";
				$consulta .= "SIMI_FECHA_MODIFICA, ";
				$consulta .= "FECHA_REGISTRO ";
				$consulta .= ") VALUES (";
				$consulta .= "'$COD_ENTIDAD', ";
				$consulta .= "'$COD_UE_BIEN_PATRI', ";
				$consulta .= "'$DENOMINACION', ";
				$consulta .= "'$PLACA', ";
				$consulta .= "'$CARROCERIA', ";
				$consulta .= "'$MARCA', ";
				$consulta .= "'$MODELO', ";
				$consulta .= "'$CATEGORIA', ";
				$consulta .= "'$NRO_CHASIS', ";
				$consulta .= "'$NRO_EJES', ";
				$consulta .= "'$NRO_MOTOR', ";
				$consulta .= "'$NRO_SERIE', ";
				$consulta .= "'$ANIO_FABRICACION', ";
				$consulta .= "'$COLOR', ";
				$consulta .= "'$COMBUSTIBLE', ";
				$consulta .= "'$TRANSMISION', ";
				$consulta .= "'$CILINDRADA', ";
				$consulta .= "'$KILOMETRAJE', ";
				$consulta .= "'$TARJETA_PROPIEDAD', ";
				$consulta .= "'$CILINDROS', ";
				$consulta .= "'$B_CILINDROS', ";
				$consulta .= "'$CARBURADOR', ";
				$consulta .= "'$B_CARBURADOR', ";
				$consulta .= "'$DISTRIBUIDOR', ";
				$consulta .= "'$B_DISTRIBUIDOR', ";
				$consulta .= "'$BOMBA_GASOLINA', ";
				$consulta .= "'$B_BOMBA_GASOLINA', ";
				$consulta .= "'$PURIFICADOR_AIRE', ";
				$consulta .= "'$B_PURIFICADOR_AIRE', ";
				$consulta .= "'$BOMBA_FRENOS', ";
				$consulta .= "'$B_BOMBA_FRENOS', ";
				$consulta .= "'$ZAPATAS_TAMBORES', ";
				$consulta .= "'$B_ZAPATAS_TAMBORES', ";
				$consulta .= "'$DISCOS_PASTILLAS', ";
				$consulta .= "'$B_DISCOS_PASTILLAS', ";
				$consulta .= "'$RADIOADOR', ";
				$consulta .= "'$B_RADIOADOR', ";
				$consulta .= "'$VENTILADOR', ";
				$consulta .= "'$B_VENTILADOR', ";
				$consulta .= "'$BOMBA_AGUA', ";
				$consulta .= "'$B_BOMBA_AGUA', ";
				$consulta .= "'$MOTOR_ARRANQUE', ";
				$consulta .= "'$B_MOTOR_ARRANQUE', ";
				$consulta .= "'$BATERIA', ";
				$consulta .= "'$B_BATERIA', ";
				$consulta .= "'$ALTERNADOR', ";
				$consulta .= "'$B_ALTERNADOR', ";
				$consulta .= "'$BOBINA', ";
				$consulta .= "'$B_BOBINA', ";
				$consulta .= "'$RELAY_ALTERNADOR', ";
				$consulta .= "'$B_RELAY_ALTERNADOR', ";
				$consulta .= "'$FAROS_DELANTEROS', ";
				$consulta .= "'$B_FAROS_DELANTEROS', ";
				$consulta .= "'$DIRECCIONALES_DELANTERAS', ";
				$consulta .= "'$B_DIRECCIONALES_DELANTERAS', ";
				$consulta .= "'$LUCES_POSTERIORES', ";
				$consulta .= "'$B_LUCES_POSTERIORES', ";
				$consulta .= "'$DIRECCIONALES_POSTERIORES', ";
				$consulta .= "'$B_DIRECCIONALES_POSTERIORES', ";
				$consulta .= "'$AUTO_RADIO', ";
				$consulta .= "'$B_AUTO_RADIO', ";
				$consulta .= "'$PARLANTES', ";
				$consulta .= "'$B_PARLANTES', ";
				$consulta .= "'$CLAXON', ";
				$consulta .= "'$B_CLAXON', ";
				$consulta .= "'$CIRCUITO_LUCES', ";
				$consulta .= "'$B_CIRCUITO_LUCES', ";
				$consulta .= "'$CAJA_CAMBIOS', ";
				$consulta .= "'$B_CAJA_CAMBIOS', ";
				$consulta .= "'$BOMBA_EMBRAGUE', ";
				$consulta .= "'$B_BOMBA_EMBRAGUE', ";
				$consulta .= "'$CAJA_TRANSFERENCIA', ";
				$consulta .= "'$B_CAJA_TRANSFERENCIA', ";
				$consulta .= "'$DIFERENCIAL_TRASERO', ";
				$consulta .= "'$B_DIFERENCIAL_TRASERO', ";
				$consulta .= "'$DIFERENCIAL_DELANTERO', ";
				$consulta .= "'$B_DIFERENCIAL_DELANTERO', ";
				$consulta .= "'$VOLANTE', ";
				$consulta .= "'$B_VOLANTE', ";
				$consulta .= "'$CANA_DIRECCION', ";
				$consulta .= "'$B_CANA_DIRECCION', ";
				$consulta .= "'$CREMALLERA', ";
				$consulta .= "'$B_CREMALLERA', ";
				$consulta .= "'$ROTULAS', ";
				$consulta .= "'$B_ROTULAS', ";
				$consulta .= "'$AMORTIGUADORES', ";
				$consulta .= "'$B_AMORTIGUADORES', ";
				$consulta .= "'$BARRA_TORSION', ";
				$consulta .= "'$B_BARRA_TORSION', ";
				$consulta .= "'$BARRA_ESTABILIZADOR', ";
				$consulta .= "'$B_BARRA_ESTABILIZADOR', ";
				$consulta .= "'$LLANTAS', ";
				$consulta .= "'$B_LLANTAS', ";
				$consulta .= "'$CAPOT_MOTOR', ";
				$consulta .= "'$B_CAPOT_MOTOR', ";
				$consulta .= "'$CAPOT_MALETERA', ";
				$consulta .= "'$B_CAPOT_MALETERA', ";
				$consulta .= "'$PARACHOQUES_DELANTEROS', ";
				$consulta .= "'$B_PARACHOQUES_DELANTEROS', ";
				$consulta .= "'$PARACHOQUES_POSTERIORES', ";
				$consulta .= "'$B_PARACHOQUES_POSTERIORES', ";
				$consulta .= "'$LUNAS_LATERALES', ";
				$consulta .= "'$B_LUNAS_LATERALES', ";
				$consulta .= "'$LUNAS_CORTAVIENTO', ";
				$consulta .= "'$B_LUNAS_CORTAVIENTO', ";
				$consulta .= "'$PARABRISAS_DELANTERO', ";
				$consulta .= "'$B_PARABRISAS_DELANTERO', ";
				$consulta .= "'$PARABRISAS_POSTERIOR', ";
				$consulta .= "'$B_PARABRISAS_POSTERIOR', ";
				$consulta .= "'$TANQUE_COMBUSTIBLE', ";
				$consulta .= "'$B_TANQUE_COMBUSTIBLE', ";
				$consulta .= "'$PUERTAS', ";
				$consulta .= "'$B_PUERTAS', ";
				$consulta .= "'$ASIENTOS', ";
				$consulta .= "'$B_ASIENTOS', ";
				$consulta .= "'$AIRE_ACONDICIONADO', ";
				$consulta .= "'$B_AIRE_ACONDICIONADO', ";
				$consulta .= "'$ALARMA', ";
				$consulta .= "'$B_ALARMA', ";
				$consulta .= "'$PLUMILLAS', ";
				$consulta .= "'$B_PLUMILLAS', ";
				$consulta .= "'$ESPEJOS', ";
				$consulta .= "'$B_ESPEJOS', ";
				$consulta .= "'$CINTURONES_SEGURIDAD', ";
				$consulta .= "'$B_CINTURONES_SEGURIDAD', ";
				$consulta .= "'$ANTENA', ";
				$consulta .= "'$B_ANTENA', ";
				$consulta .= "'$OTRAS_CARACTERISTICAS_RELEVANTES', ";
				$consulta .= "'$VALOR_TASACION', ";
				$consulta .= "'$ID_ESTADO', ";
				$consulta .= "'$COD_USUARIO_hide', ";
				$consulta .= " GETDATE(), ";
				$consulta .= "'$COD_USUARIO_hide', ";
				$consulta .= " GETDATE(), ";
				$consulta .= "GETDATE()) ";
			}else
			{

				$consulta = "UPDATE TBL_MUEBLES_UE_BIE_FT_VEHICULAR SET ";
				$consulta .= "DENOMINACION = '$DENOMINACION', ";
				$consulta .= "PLACA = '$PLACA', ";
				$consulta .= "CARROCERIA = '$CARROCERIA', ";
				$consulta .= "MARCA = '$MARCA', ";
				$consulta .= "MODELO = '$MODELO', ";
				$consulta .= "CATEGORIA ='$CATEGORIA', ";
				$consulta .= "NRO_CHASIS = '$NRO_CHASIS', ";
				$consulta .= "NRO_EJES = '$NRO_EJES', ";
				$consulta .= "NRO_MOTOR = '$NRO_MOTOR', ";
				$consulta .= "NRO_SERIE = '$NRO_SERIE', ";
				$consulta .= "ANIO_FABRICACION = '$ANIO_FABRICACION', ";
				$consulta .= "COLOR = '$COLOR', ";
				$consulta .= "COMBUSTIBLE = '$COMBUSTIBLE', ";
				$consulta .= "TRANSMISION = '$TRANSMISION', ";
				$consulta .= "CILINDRADA = '$CILINDRADA', ";
				$consulta .= "KILOMETRAJE = '$KILOMETRAJE', ";
				$consulta .= "TARJETA_PROPIEDAD = '$TARJETA_PROPIEDAD', ";
				$consulta .= "CILINDROS = '$CILINDROS', ";
				$consulta .= "B_CILINDROS = '$B_CILINDROS', ";
				$consulta .= "CARBURADOR = '$CARBURADOR', ";
				$consulta .= "B_CARBURADOR ='$B_CARBURADOR', ";
				$consulta .= "DISTRIBUIDOR = '$DISTRIBUIDOR', ";
				$consulta .= "B_DISTRIBUIDOR ='$B_DISTRIBUIDOR', ";
				$consulta .= "BOMBA_GASOLINA ='$BOMBA_GASOLINA', ";
				$consulta .= "B_BOMBA_GASOLINA = '$B_BOMBA_GASOLINA', ";
				$consulta .= "PURIFICADOR_AIRE = '$PURIFICADOR_AIRE', ";
				$consulta .= "B_PURIFICADOR_AIRE = '$B_PURIFICADOR_AIRE', ";
				$consulta .= "BOMBA_FRENOS ='$BOMBA_FRENOS', ";
				$consulta .= "B_BOMBA_FRENOS = '$B_BOMBA_FRENOS', ";
				$consulta .= "ZAPATAS_TAMBORES = '$ZAPATAS_TAMBORES', ";
				$consulta .= "B_ZAPATAS_TAMBORES = '$B_ZAPATAS_TAMBORES', ";
				$consulta .= "DISCOS_PASTILLAS = '$DISCOS_PASTILLAS', ";
				$consulta .= "B_DISCOS_PASTILLAS = '$B_DISCOS_PASTILLAS', ";
				$consulta .= "RADIOADOR = '$RADIOADOR', ";
				$consulta .= "B_RADIOADOR ='$B_RADIOADOR', ";
				$consulta .= "VENTILADOR = '$VENTILADOR', ";
				$consulta .= "B_VENTILADOR = '$B_VENTILADOR', ";
				$consulta .= "BOMBA_AGUA = '$BOMBA_AGUA', ";
				$consulta .= "B_BOMBA_AGUA = '$B_BOMBA_AGUA', ";
				$consulta .= "MOTOR_ARRANQUE = '$MOTOR_ARRANQUE', ";
				$consulta .= "B_MOTOR_ARRANQUE = '$B_MOTOR_ARRANQUE', ";
				$consulta .= "BATERIA = '$BATERIA', ";
				$consulta .= "B_BATERIA = '$B_BATERIA', ";
				$consulta .= "ALTERNADOR = '$ALTERNADOR', ";
				$consulta .= "B_ALTERNADOR = '$B_ALTERNADOR', ";
				$consulta .= "BOBINA = '$BOBINA', ";
				$consulta .= "B_BOBINA = '$B_BOBINA', ";
				$consulta .= "RELAY_ALTERNADOR = '$RELAY_ALTERNADOR', ";
				$consulta .= "B_RELAY_ALTERNADOR = '$B_RELAY_ALTERNADOR', ";
				$consulta .= "FAROS_DELANTEROS = '$FAROS_DELANTEROS', ";
				$consulta .= "B_FAROS_DELANTEROS = '$B_FAROS_DELANTEROS', ";
				$consulta .= "DIRECCIONALES_DELANTERAS = '$DIRECCIONALES_DELANTERAS', ";
				$consulta .= "B_DIRECCIONALES_DELANTERAS = '$B_DIRECCIONALES_DELANTERAS', ";
				$consulta .= "LUCES_POSTERIORES = '$LUCES_POSTERIORES', ";
				$consulta .= "B_LUCES_POSTERIORES = '$B_LUCES_POSTERIORES', ";
				$consulta .= "DIRECCIONALES_POSTERIORES = '$DIRECCIONALES_POSTERIORES', ";
				$consulta .= "B_DIRECCIONALES_POSTERIORES = '$B_DIRECCIONALES_POSTERIORES', ";
				$consulta .= "AUTO_RADIO = '$AUTO_RADIO', ";
				$consulta .= "B_AUTO_RADIO = '$B_AUTO_RADIO', ";
				$consulta .= "PARLANTES = '$PARLANTES', ";
				$consulta .= "B_PARLANTES = '$B_PARLANTES', ";
				$consulta .= "CLAXON = '$CLAXON', ";
				$consulta .= "B_CLAXON = '$B_CLAXON', ";
				$consulta .= "CIRCUITO_LUCES = '$CIRCUITO_LUCES', ";
				$consulta .= "B_CIRCUITO_LUCES = '$B_CIRCUITO_LUCES', ";
				$consulta .= "CAJA_CAMBIOS = '$CAJA_CAMBIOS', ";
				$consulta .= "B_CAJA_CAMBIOS = '$B_CAJA_CAMBIOS', ";
				$consulta .= "BOMBA_EMBRAGUE = '$BOMBA_EMBRAGUE', ";
				$consulta .= "B_BOMBA_EMBRAGUE = '$B_BOMBA_EMBRAGUE', ";
				$consulta .= "CAJA_TRANSFERENCIA = '$CAJA_TRANSFERENCIA', ";
				$consulta .= "B_CAJA_TRANSFERENCIA = '$B_CAJA_TRANSFERENCIA', ";
				$consulta .= "DIFERENCIAL_TRASERO = '$DIFERENCIAL_TRASERO', ";
				$consulta .= "B_DIFERENCIAL_TRASERO = '$B_DIFERENCIAL_TRASERO', ";
				$consulta .= "DIFERENCIAL_DELANTERO = '$DIFERENCIAL_DELANTERO', ";
				$consulta .= "B_DIFERENCIAL_DELANTERO = '$B_DIFERENCIAL_DELANTERO', ";
				$consulta .= "VOLANTE = '$VOLANTE', ";
				$consulta .= "B_VOLANTE = '$B_VOLANTE', ";
				$consulta .= "CANA_DIRECCION = '$CANA_DIRECCION', ";
				$consulta .= "B_CANA_DIRECCION = '$B_CANA_DIRECCION', ";
				$consulta .= "CREMALLERA = '$CREMALLERA', ";
				$consulta .= "B_CREMALLERA = '$B_CREMALLERA', ";
				$consulta .= "ROTULAS = '$ROTULAS', ";
				$consulta .= "B_ROTULAS = '$B_ROTULAS', ";
				$consulta .= "AMORTIGUADORES = '$AMORTIGUADORES', ";
				$consulta .= "B_AMORTIGUADORES = '$B_AMORTIGUADORES', ";
				$consulta .= "BARRA_TORSION = '$BARRA_TORSION', ";
				$consulta .= "B_BARRA_TORSION = '$B_BARRA_TORSION', ";
				$consulta .= "BARRA_ESTABILIZADOR = '$BARRA_ESTABILIZADOR', ";
				$consulta .= "B_BARRA_ESTABILIZADOR = '$B_BARRA_ESTABILIZADOR', ";
				$consulta .= "LLANTAS = '$LLANTAS', ";
				$consulta .= "B_LLANTAS = '$B_LLANTAS', ";
				$consulta .= "CAPOT_MOTOR = '$CAPOT_MOTOR', ";
				$consulta .= "B_CAPOT_MOTOR = '$B_CAPOT_MOTOR', ";
				$consulta .= "CAPOT_MALETERA = '$CAPOT_MALETERA', ";
				$consulta .= "B_CAPOT_MALETERA = '$B_CAPOT_MALETERA', ";
				$consulta .= "PARACHOQUES_DELANTEROS = '$PARACHOQUES_DELANTEROS', ";
				$consulta .= "B_PARACHOQUES_DELANTEROS = '$B_PARACHOQUES_DELANTEROS', ";
				$consulta .= "PARACHOQUES_POSTERIORES = '$PARACHOQUES_POSTERIORES', ";
				$consulta .= "B_PARACHOQUES_POSTERIORES = '$B_PARACHOQUES_POSTERIORES', ";
				$consulta .= "LUNAS_LATERALES = '$LUNAS_LATERALES', ";
				$consulta .= "B_LUNAS_LATERALES = '$B_LUNAS_LATERALES', ";
				$consulta .= "LUNAS_CORTAVIENTO = '$LUNAS_CORTAVIENTO', ";
				$consulta .= "B_LUNAS_CORTAVIENTO = '$B_LUNAS_CORTAVIENTO', ";
				$consulta .= "PARABRISAS_DELANTERO = '$PARABRISAS_DELANTERO', ";
				$consulta .= "B_PARABRISAS_DELANTERO = '$B_PARABRISAS_DELANTERO', ";
				$consulta .= "PARABRISAS_POSTERIOR = '$PARABRISAS_POSTERIOR', ";
				$consulta .= "B_PARABRISAS_POSTERIOR = '$B_PARABRISAS_POSTERIOR', ";
				$consulta .= "TANQUE_COMBUSTIBLE = '$TANQUE_COMBUSTIBLE', ";
				$consulta .= "B_TANQUE_COMBUSTIBLE = '$B_TANQUE_COMBUSTIBLE', ";
				$consulta .= "PUERTAS = '$PUERTAS', ";
				$consulta .= "B_PUERTAS = '$B_PUERTAS', ";
				$consulta .= "ASIENTOS = '$ASIENTOS', ";
				$consulta .= "B_ASIENTOS = '$B_ASIENTOS', ";
				$consulta .= "AIRE_ACONDICIONADO = '$AIRE_ACONDICIONADO', ";
				$consulta .= "B_AIRE_ACONDICIONADO = '$B_AIRE_ACONDICIONADO', ";
				$consulta .= "ALARMA = '$ALARMA', ";
				$consulta .= "B_ALARMA = '$B_ALARMA', ";
				$consulta .= "PLUMILLAS = '$PLUMILLAS', ";
				$consulta .= "B_PLUMILLAS = '$B_PLUMILLAS', ";
				$consulta .= "ESPEJOS = '$ESPEJOS', ";
				$consulta .= "B_ESPEJOS = '$B_ESPEJOS', ";
				$consulta .= "CINTURONES_SEGURIDAD = '$CINTURONES_SEGURIDAD', ";
				$consulta .= "B_CINTURONES_SEGURIDAD = '$B_CINTURONES_SEGURIDAD', ";
				$consulta .= "ANTENA = '$ANTENA', ";
				$consulta .= "B_ANTENA = '$B_ANTENA', ";
				$consulta .= "OTRAS_CARACTERISTICAS_RELEVANTES = '$OTRAS_CARACTERISTICAS_RELEVANTES', ";
				$consulta .= "VALOR_TASACION = '$VALOR_TASACION', ";
				$consulta .= "SIMI_FECHA_MODIFICA = GETDATE(), ";
				$consulta .= "SIMI_USUARIO_MODIFICA = '$COD_USUARIO_hide' ";
				$consulta .= "WHERE COD_UE_FT_VEHICULAR = $COD_UE_FT_VEHICULAR_hide";

			}


			$result = $this->execute($consulta);


			if(!is_string($result)){

			 /* ACTUALIZA REGISTRO DE BIENES MUEBLES PARA VEHICULOS */
			$consulta= "UPDATE TBL_MUEBLES_UE_BIEN_PATRIMONIAL SET ";
			$consulta .= "DENOMINACION_BIEN 	= '$DENOMINACION', ";
			$consulta .= "SIMI_USUARIO_MODIFICA	= '$COD_USUARIO_hide', ";
			$consulta .= "SIMI_FECHA_MODIFICA	= GETDATE(), ";
			$consulta .= "MARCA					= '$MARCA', ";
			$consulta .= "MODELO				= '$MODELO', ";
			$consulta .= "COLOR					= '$COLOR', ";
			$consulta .= "SERIE					= '$NRO_SERIE', ";
			$consulta .= "PLACA					= '$PLACA', ";
			$consulta .= "NRO_MOTOR				= '$NRO_MOTOR', ";
			$consulta .= "NRO_CHASIS			= '$NRO_CHASIS', ";
			$consulta .= "ANIO_FABRICACION		= '$ANIO_FABRICACION', ";
			$consulta .= "OTRAS_CARACT			= '$OTRAS_CARACTERISTICAS_RELEVANTES' ";
			$consulta .= "WHERE COD_ENTIDAD = '$COD_ENTIDAD' AND COD_UE_BIEN_PATRI = '$COD_UE_BIEN_PATRI'";



			$result = $this->execute($consulta);

				}



			$this->close();
			return $result;
		}




	}


	/* Metodo  Actualizar TBL_MUEBLES_UE_BIEN_PATRIMONIAL */
	function F_Actualizar_TBL_MUEBLES_UE_BIEN_PATRIMONIAL
		(
			$COD_ENTIDAD,			$COD_UE_BIEN_PATRI,		$CODIGO_PATRIMONIAL,	$DENOMINACION_BIEN,
			$NRO_BIEN,				$NRO_BIEN_CORRELATIVO,	$COD_CORRELATIVO,		$COD_UE_BIEN_ALTA,
			$USO_CUENTA,			$TIP_CUENTA,			$COD_CTA_CONTABLE,		$VALOR_ADQUIS,
			$VALOR_NETO,			$COD_ESTADO_BIEN,		$OPC_ASEGURADO,			$COND_BIEN,
			$OBSERVACION,			$COD_UE_BIEN_DEPREC,	$ALMACEN,				$ENVIADO_SBN,
			$FECHA_ENVIADO_SBN,		$FECHA_DEVUELTO,		$ID_PREDIO_SBN,			$COD_UE_AREA,
			$COD_UE_OFIC,			$COD_UE_PERS,			$COD_UE_BIEN_ASIG,		$COD_UE_BIEN_BAJA,
			$COD_UE_BIEN_DISPOSIC,	$COD_UE_BIEN_ADMINIS,	$COD_UE_BIEN_DEVOL,		$FECHA_REGISTRO,
			$SIMI_USUARIO_CREACION,	$SIMI_FECHA_CREACION,	$SIMI_USUARIO_MODIFICA,	$SIMI_FECHA_MODIFICA,
			$SIMI_USUARIO_ELIMINA,	$SIMI_FECHA_ELIMINA,	$ID_ESTADO,				$COD_IMPORT_EXCEL_FILE,
			$DOC_ALTA_SBN,			$DOC_BAJA_SBN,			$ID_PREDIO_EXCEL,		$MARCA,
			$MODELO,				$TIPO,					$COLOR,					$SERIE,
			$DIMENSION,				$PLACA,					$NRO_MOTOR,				$NRO_CHASIS,
			$MATRICULA,				$ANIO_FABRICACION,		$LONGITUD,				$ALTURA,
			$ANCHO,					$RAZA,					$ESPECIE,				$EDAD,
			$PAIS,					$OTRAS_CARACT,			$PORC_DEPREC


		){
		if($this->conectar()==true){
			$consulta="UPDATE INTO TBL_MUEBLES_UE_BIEN_PATRIMONIAL SET
			DENOMINACION_BIEN 		= $DENOMINACION_BIEN,
			NRO_BIEN 				= $NRO_BIEN,
			NRO_BIEN_CORRELATIVO 	= $NRO_BIEN_CORRELATIVO,
			COD_CORRELATIVO		= $COD_CORRELATIVO,
			COD_UE_BIEN_ALTA		= $COD_UE_BIEN_ALTA,
			USO_CUENTA				= $USO_CUENTA,
			TIP_CUENTA				= $TIP_CUENTA,
			COD_CTA_CONTABLE		= $COD_CTA_CONTABLE,
			VALOR_ADQUIS			= $VALOR_ADQUIS,
			VALOR_NETO				= $VALOR_NETO,
			COD_ESTADO_BIEN			= $COD_ESTADO_BIEN,
			OPC_ASEGURADO			= $OPC_ASEGURADO,
			COND_BIEN				= $COND_BIEN,
			OBSERVACION				= $OBSERVACION,
			COD_UE_BIEN_DEPREC		= $COD_UE_BIEN_DEPREC,
			ALMACEN					= $ALMACEN,
			ENVIADO_SBN				= $ENVIADO_SBN,
			FECHA_ENVIADO_SBN		= $FECHA_ENVIADO_SBN,
			FECHA_DEVUELTO			= $FECHA_DEVUELTO,
			ID_PREDIO_SBN			= $ID_PREDIO_SBN,
			COD_UE_AREA				= $COD_UE_AREA,
			COD_UE_OFIC				= $COD_UE_OFIC,
			COD_UE_PERS				= $COD_UE_PERS,
			COD_UE_BIEN_ASIG		= $COD_UE_BIEN_ASIG,
			COD_UE_BIEN_BAJA		= $COD_UE_BIEN_BAJA,
			COD_UE_BIEN_DISPOSIC,	= $COD_UE_BIEN_DISPOSIC,
			COD_UE_BIEN_ADMINIS		= $COD_UE_BIEN_ADMINIS,
			COD_UE_BIEN_DEVOL		= $COD_UE_BIEN_DEVOL,
			FECHA_REGISTRO			= $FECHA_REGISTRO,
			SIMI_USUARIO_CREACION	= $SIMI_USUARIO_CREACION,
			SIMI_FECHA_CREACION		= $SIMI_FECHA_CREACION,
			SIMI_USUARIO_MODIFICA	= $SIMI_USUARIO_MODIFICA,
			SIMI_FECHA_MODIFICA		= $SIMI_FECHA_MODIFICA,
			SIMI_USUARIO_ELIMINA	= $SIMI_USUARIO_ELIMINA,
			SIMI_FECHA_ELIMINA		= $SIMI_FECHA_ELIMINA,
			ID_ESTADO				= $ID_ESTADO,
			COD_IMPORT_EXCEL_FILE	= $COD_IMPORT_EXCEL_FILE,
			DOC_ALTA_SBN			= $DOC_ALTA_SBN,
			DOC_BAJA_SBN			= $DOC_BAJA_SBN,
			ID_PREDIO_EXCEL			= $ID_PREDIO_EXCEL,
			MARCA					= $MARCA,
			MODELO					= $MODELO,
			TIPO					= $TIPO,
			COLOR					= $COLOR,
			SERIE					= $SERIE,
			DIMENSION				= $DIMENSION,
			PLACA					= $PLACA,
			NRO_MOTOR				= $NRO_MOTOR,
			NRO_CHASIS				= $NRO_CHASIS,
			MATRICULA				= $MATRICULA,
			ANIO_FABRICACION		= $ANIO_FABRICACION,
			LONGITUD				= $LONGITUD,
			ALTURA					= $ALTURA,
			ANCHO					= $ANCHO,
			RAZA					= $RAZA,
			ESPECIE					= $ESPECIE,
			EDAD					= $EDAD,
			PAIS					= $PAIS,
			OTRAS_CARACT			= $OTRAS_CARACT,
			PORC_DEPREC				= $PORC_DEPREC
			WHERE COD_ENTIDAD = $COD_ENTIDAD AND COD_UE_BIEN_PATRI = $COD_UE_BIEN_PATRI)";



			$result = $this->execute($consulta);
			$this->close();
			return $result;
		}




	}



	/* Busqueda de Bien Vehicular */
	function F_Lista_BienVehicularXCodigo($COD_ENTIDAD, $COD_UE_BIEN_PATRI){
		if($this->conectar()==true){


			$SQL_ORIGEN="SELECT
			B.COD_UE_BIEN_PATRI, B.CODIGO_PATRIMONIAL, B.DENOMINACION_BIEN, B.PLACA,
			B.MARCA, B.MODELO, B.NRO_CHASIS, B.NRO_MOTOR, B.SERIE [NRO_SERIE], B.ANIO_FABRICACION,
			B.COLOR, B.OTRAS_CARACT [OTRAS_CARACTERISTICAS_RELEVANTES], P.NOM_ENTIDAD
			FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL B
			LEFT JOIN TBL_PADRON_ENTIDAD P ON B.COD_ENTIDAD = P.COD_ENTIDAD
			WHERE B.ID_ESTADO = '1'
			AND B.COD_ENTIDAD = '$COD_ENTIDAD' AND B.COD_UE_BIEN_PATRI = '$COD_UE_BIEN_PATRI'
			AND ID_CATALOGO IN (SELECT ID_CATALOGO FROM TBL_MUEBLES_CATALOGO WHERE NRO_GRUPO = '67' AND NRO_CLASE = '82')
			";

			$resultado = $this->execute($SQL_ORIGEN);



			return $resultado;
		}

	}


	//Metodo Buscar Ficha Tecnica Vehicular de un Bien
	function F_Lista_Buscar_FichaTecnicaVehicular($COD_ENTIDAD, $COD_UE_BIEN_PATRI, $COD_UE_FT_VEHICULAR){
		if($this->conectar()==true){

			$SQL_ORIGEN="SELECT
			MP.CODIGO_PATRIMONIAL, MP.DENOMINACION_BIEN,
			B.*, P.NOM_ENTIDAD
			FROM TBL_MUEBLES_UE_BIE_FT_VEHICULAR B
			LEFT JOIN TBL_MUEBLES_UE_BIEN_PATRIMONIAL MP ON
				B.COD_ENTIDAD		= MP.COD_ENTIDAD AND
				B.COD_UE_BIEN_PATRI	= MP.COD_UE_BIEN_PATRI
			LEFT JOIN TBL_PADRON_ENTIDAD P ON B.COD_ENTIDAD = P.COD_ENTIDAD
			WHERE B.ID_ESTADO = '1'
			AND B.COD_ENTIDAD = '$COD_ENTIDAD' AND B.COD_UE_BIEN_PATRI = '$COD_UE_BIEN_PATRI' AND B.COD_UE_FT_VEHICULAR = '$COD_UE_FT_VEHICULAR'
			";


			$resultado = $this->execute($SQL_ORIGEN);

			return $resultado;
		}

	}


	/* Metodo  Eliminar TBL_MUEBLES_UE_BIE_FT_VEHICULAR */
	function F_Eliminar_TBL_MUEBLES_UE_BIE_FT_VEHICULAR
		(
			$COD_ENTIDAD, $COD_UE_BIEN_PATRI, $COD_UE_FT_VEHICULAR

		)
	{
		if($this->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_UE_BIE_FT_VEHICULAR SET
			ID_ESTADO = 2
			WHERE COD_ENTIDAD =  $COD_ENTIDAD AND COD_UE_BIEN_PATRI = $COD_UE_BIEN_PATRI AND COD_UE_FT_VEHICULAR = $COD_UE_FT_VEHICULAR";


			$result = $this->execute($consulta);
			$this->close();
			return $result;
		}


	}//F_Eliminar_TBL_MUEBLES_UE_BIE_FT_VEHICULAR

	function F_Listar_Ficha_Tecnica_Vehicular($COD_UE_FT_VEHICULAR)
	{

		if($this->conectar()==true)
		{
			$consulta="SELECT FT.*, PD.NOM_ENTIDAD FROM TBL_MUEBLES_UE_BIE_FT_VEHICULAR FT LEFT JOIN TBL_PADRON_ENTIDAD PD ON FT.COD_ENTIDAD = PD.COD_ENTIDAD WHERE FT.COD_UE_FT_VEHICULAR = $COD_UE_FT_VEHICULAR";


			$result = $this->execute($consulta);
			//$this->close();
			return $result;
		}

	}


	function F_Obtener_Nombre_Entidad($COD_ENTIDAD)
	{

		if($this->conectar()==true)
		{
			$consulta="SELECT NOM_ENTIDAD FROM TBL_PADRON_ENTIDAD WHERE COD_ENTIDAD = $COD_ENTIDAD";

			$result = $this->execute($consulta);
			return $result;
		}

	}

	function F_Listado_Fichas_Tecnica_Vehiculares( $ACCION, $INI, $FIN, $COD_ENTIDAD,  $COD_GRUPO, $COD_CLASE,
			$CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $ANIO_ADQUISICION,$MES_ADQUISICION)
	{

		if($this->conectar()==true)
		{
			$consulta = "EXEC SP_SIMI_LISTADO_REPORTE_FICHA_VEHICULAR ";
			$consulta .= "@ACCION = '$ACCION', ";
			$consulta .= "@INI = $INI, ";
			$consulta .= "@FIN = $FIN, ";
			$consulta .= "@COD_ENTIDAD 	= $COD_ENTIDAD, ";
			$consulta .= "@COD_GRUPO	= '$COD_GRUPO', ";
			$consulta .= "@COD_CLASE	= '$COD_CLASE', ";
			$consulta .= "@CODIGO_PATRIMONIAL = '$CODIGO_PATRIMONIAL', ";
			$consulta .= "@DENOMINACION_BIEN = '$DENOMINACION_BIEN', ";
			$consulta .= "@ANIO_ADQUISICION = '$ANIO_ADQUISICION',";
			$consulta .= "@MES_ADQUISICION = '$MES_ADQUISICION'";

			$result = $this->execute($consulta);
			return $result;
		}

	}

	function F_Total_Fichas_Tecnica_Vehiculares( $ACCION, $COD_ENTIDAD,  $COD_GRUPO, $COD_CLASE,
			$CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $ANIO_ADQUISICION,$MES_ADQUISICION)
	{

		if($this->conectar()==true)
		{
			$consulta = "EXEC SP_SIMI_TOTAL_REGISTROS_REPORTE_FICHA_VEHICULAR ";
			$consulta .= "@ACCION = '$ACCION', ";
			$consulta .= "@COD_ENTIDAD 	= $COD_ENTIDAD, ";
			$consulta .= "@COD_GRUPO	= '$COD_GRUPO', ";
			$consulta .= "@COD_CLASE	= '$COD_CLASE', ";
			$consulta .= "@CODIGO_PATRIMONIAL = '$CODIGO_PATRIMONIAL', ";
			$consulta .= "@DENOMINACION_BIEN = '$DENOMINACION_BIEN', ";
			$consulta .= "@ANIO_ADQUISICION = '$ANIO_ADQUISICION',";
                        $consulta .= "@MES_ADQUISICION = '$MES_ADQUISICION'";


                        $result = $this->execute($consulta);
			return $result;
		}

	}

	function F_Listado_Anios_Fecha_Adquisicion($COD_ENTIDAD)
	{

		if($this->conectar()==true)
		{
			$consulta = "EXEC SP_SIMI_LISTADO_DE_ANIOS_X_FECHA_ADQUISICION_BIENES ";
			$consulta .= "@COD_ENTIDAD 	= $COD_ENTIDAD";

			$result = $this->execute($consulta);
			return $result;
		}

	}

	function F_Listado_Fichas_Tecnica_Vehiculares_en_Grafica( $ACCION, $COD_ENTIDAD,  $ANIO_ADQUISICION)
	{

		if($this->conectar()==true)
		{
			$consulta = "EXEC SP_SIMI_LISTADO_REPORTE_FICHA_VEHICULAR_GRAFICA ";
			$consulta .= "@ACCION = '$ACCION', ";
			$consulta .= "@COD_ENTIDAD 	= $COD_ENTIDAD, ";
			$consulta .= "@ANIO_ADQUISICION = '$ANIO_ADQUISICION'";
			$result = $this->execute($consulta);
			return $result;
		}

	}

	function F_Listado_Fichas_Vehicular_Formato_PDF( $ACCION, $COD_ENTIDAD,  $COD_GRUPO, $COD_CLASE,
			$CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $ANIO_ADQUISICION, $MES_ADQUISICION )
	{

		if($this->conectar()==true)
		{
			$consulta = "EXEC SP_SIMI_LISTADO_FICHA_VEHICULAR_FORMATO_PDF ";
			$consulta .= "@ACCION = '$ACCION', ";
			$consulta .= "@COD_ENTIDAD 	= $COD_ENTIDAD, ";
			$consulta .= "@COD_GRUPO 	= '$COD_GRUPO', ";
			$consulta .= "@COD_CLASE 	= '$COD_CLASE', ";
			$consulta .= "@CODIGO_PATRIMONIAL 	= '$CODIGO_PATRIMONIAL', ";
			$consulta .= "@DENOMINACION_BIEN 	= '$DENOMINACION_BIEN', ";
			$consulta .= "@ANIO_ADQUISICION 	= '$ANIO_ADQUISICION', ";
			$consulta .= "@MES_ADQUISICION 		= '$MES_ADQUISICION' ";

			$result = $this->execute($consulta);
			return $result;
		}

	}

	function F_Listado_Fichas_Tecnica_Vehiculares_X_Entidad( $ACCION, $COD_ENTIDAD,
			$CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $INI, $FIN)
	{

		if($this->conectar()==true)
		{
			$consulta = "EXEC SP_SIMI_BUSQUEDA_FICHASVEHICULARES_X_ENTIDAD ";
			$consulta .= "@ACCION = '$ACCION', ";
			$consulta .= "@COD_ENTIDAD 	= '$COD_ENTIDAD', ";
			$consulta .= "@CODIGO_PATRIMONIAL = '$CODIGO_PATRIMONIAL', ";
			$consulta .= "@DENOMINACION_BIEN = '$DENOMINACION_BIEN', ";
			$consulta .= "@INI = '$INI', ";
			$consulta .= "@FIN = '$FIN'";

			if ($ACCION == '1')
			{

				$result = $this->execute($consulta);
				$total = odbc_result($result,"TOT_REG");
				$this->close();
				return $total;
			}
			else
			{

				$result = $this->execute($consulta);
				return $result;
			}


		}

	}



}

?>
