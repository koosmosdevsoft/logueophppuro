<?php

class M_TBL_MUEBLES_UE_BIE_FT_VEHICULAR_Model extends Database
{

  //Metodo Constructor
  public function __construct()
  {
    parent::__construct();
  }

  function F_Consulta_Ficha_Vehicular($COD_BIEN)
  {
    if ($this->conectar()==true)
    {
      $consulta = "EXEC SP_SIMI_CONSULTA_FICHA_VEHICULAR @COD_UE_BIEN_PATRI = '$COD_BIEN'";

      $rs_Resultado = $this->execute($consulta);
      return $rs_Resultado;
    }
  }

  function F_Guardar_Ficha_Vehicular(
    $txt_COD_UE_FT_VEHICULAR,       $COD_ENTIDAD,                 $COD_USUARIO,
    $COD_UE_BIEN_PATRI,             $DENOMINACION,
    $PLACA,                         $CARROCERIA,                  $MARCA,                     $MODELO,                $CATEGORIA,
    $NRO_CHASIS,                    $NRO_EJES,                    $NRO_MOTOR,                 $NRO_SERIE,             $ANIO_FABRICACION,
    $COLOR,                         $COMBUSTIBLE,                 $TRANSMISION,               $CILINDRADA,            $KILOMETRAJE,
    $TARJETA_PROPIEDAD,             $CILINDROS,                   $B_CILINDROS,               $CARBURADOR,            $B_CARBURADOR,
    $DISTRIBUIDOR,                  $B_DISTRIBUIDOR,              $BOMBA_GASOLINA,            $B_BOMBA_GASOLINA,      $PURIFICADOR_AIRE,
    $B_PURIFICADOR_AIRE,            $BOMBA_FRENOS,                $B_BOMBA_FRENOS,            $ZAPATAS_TAMBORES,      $B_ZAPATAS_TAMBORES,
    $DISCOS_PASTILLAS,              $B_DISCOS_PASTILLAS,          $RADIOADOR,                 $B_RADIOADOR,           $VENTILADOR,
    $B_VENTILADOR,                  $BOMBA_AGUA,                  $B_BOMBA_AGUA,              $MOTOR_ARRANQUE,        $B_MOTOR_ARRANQUE,
    $BATERIA,                       $B_BATERIA,                   $ALTERNADOR,                $B_ALTERNADOR,          $BOBINA,
    $B_BOBINA,                      $RELAY_ALTERNADOR,            $B_RELAY_ALTERNADOR,            $FAROS_DELANTEROS,      $B_FAROS_DELANTEROS,
    $DIRECCIONALES_DELANTERAS,      $B_DIRECCIONALES_DELANTERAS,  $LUCES_POSTERIORES,         $B_LUCES_POSTERIORES,   $DIRECCIONALES_POSTERIORES,
    $B_DIRECCIONALES_POSTERIORES,   $AUTO_RADIO,                  $B_AUTO_RADIO,              $PARLANTES,               $B_PARLANTES,
    $CLAXON,                        $B_CLAXON,                    $CIRCUITO_LUCES,            $B_CIRCUITO_LUCES,        $CAJA_CAMBIOS,
    $B_CAJA_CAMBIOS,                $BOMBA_EMBRAGUE,              $B_BOMBA_EMBRAGUE,          $CAJA_TRANSFERENCIA,      $B_CAJA_TRANSFERENCIA,
    $DIFERENCIAL_TRASERO,           $B_DIFERENCIAL_TRASERO,       $DIFERENCIAL_DELANTERO,     $B_DIFERENCIAL_DELANTERO, $VOLANTE,
    $B_VOLANTE,                     $CANA_DIRECCION,              $B_CANA_DIRECCION,          $CREMALLERA,              $B_CREMALLERA,
    $ROTULAS,                       $B_ROTULAS,                   $AMORTIGUADORES,            $B_AMORTIGUADORES,        $BARRA_TORSION,
    $B_BARRA_TORSION,               $BARRA_ESTABILIZADOR,         $B_BARRA_ESTABILIZADOR,     $LLANTAS,                 $B_LLANTAS,
    $CAPOT_MOTOR,                   $B_CAPOT_MOTOR,               $CAPOT_MALETERA,            $B_CAPOT_MALETERA,        $PARACHOQUES_DELANTEROS,
    $B_PARACHOQUES_DELANTEROS,      $PARACHOQUES_POSTERIORES,     $B_PARACHOQUES_POSTERIORES,     $LUNAS_LATERALES,         $B_LUNAS_LATERALES,
    $LUNAS_CORTAVIENTO,             $B_LUNAS_CORTAVIENTO,         $PARABRISAS_DELANTERO,      $B_PARABRISAS_DELANTERO,  $PARABRISAS_POSTERIOR,
    $B_PARABRISAS_POSTERIOR,        $TANQUE_COMBUSTIBLE,          $B_TANQUE_COMBUSTIBLE,      $PUERTAS,                 $B_PUERTAS,
    $ASIENTOS,                      $B_ASIENTOS,                  $AIRE_ACONDICIONADO,        $B_AIRE_ACONDICIONADO,    $ALARMA,
    $B_ALARMA,                      $PLUMILLAS,                   $B_PLUMILLAS,               $ESPEJOS,                 $B_ESPEJOS,
    $CINTURONES_SEGURIDAD,          $B_CINTURONES_SEGURIDAD,      $ANTENA,                    $B_ANTENA,                $OTRAS_CARACTERISTICAS_RELEVANTES,
    $VALOR_TASACION)
    {
    if ($this->conectar()==true)
    {
      $consulta = "EXEC SP_SIMI_GUARDAR_FICHA_VEHICULAR
      '$txt_COD_UE_FT_VEHICULAR',       '$COD_ENTIDAD',                 '$COD_USUARIO',
      '$COD_UE_BIEN_PATRI',             '$DENOMINACION',
      '$PLACA',                         '$CARROCERIA',                  '$MARCA',                     '$MODELO',                '$CATEGORIA',
      '$NRO_CHASIS',                    '$NRO_EJES',                    '$NRO_MOTOR',                 '$NRO_SERIE',             '$ANIO_FABRICACION',
      '$COLOR',                         '$COMBUSTIBLE',                 '$TRANSMISION',               '$CILINDRADA',            '$KILOMETRAJE',
      '$TARJETA_PROPIEDAD',             '$CILINDROS',                   '$B_CILINDROS',               '$CARBURADOR',            '$B_CARBURADOR',
      '$DISTRIBUIDOR',                  '$B_DISTRIBUIDOR',              '$BOMBA_GASOLINA',            '$B_BOMBA_GASOLINA',      '$PURIFICADOR_AIRE',
      '$B_PURIFICADOR_AIRE',            '$BOMBA_FRENOS',                '$B_BOMBA_FRENOS',            '$ZAPATAS_TAMBORES',      '$B_ZAPATAS_TAMBORES',
      '$DISCOS_PASTILLAS',              '$B_DISCOS_PASTILLAS',          '$RADIOADOR',                 '$B_RADIOADOR',           '$VENTILADOR',
      '$B_VENTILADOR',                  '$BOMBA_AGUA',                  '$B_BOMBA_AGUA',              '$MOTOR_ARRANQUE',        '$B_MOTOR_ARRANQUE',
      '$BATERIA',                       '$B_BATERIA',                   '$ALTERNADOR',                '$B_ALTERNADOR',          '$BOBINA',
      '$B_BOBINA',                      '$RELAY_ALTERNADOR',            '$B_RELAY_ALTERNADOR',            '$FAROS_DELANTEROS',      '$B_FAROS_DELANTEROS',
      '$DIRECCIONALES_DELANTERAS',      '$B_DIRECCIONALES_DELANTERAS',  '$LUCES_POSTERIORES',         '$B_LUCES_POSTERIORES',   '$DIRECCIONALES_POSTERIORES',
      '$B_DIRECCIONALES_POSTERIORES',   '$AUTO_RADIO',                  '$B_AUTO_RADIO',              '$PARLANTES',               '$B_PARLANTES',
      '$CLAXON',                        '$B_CLAXON',                    '$CIRCUITO_LUCES',            '$B_CIRCUITO_LUCES',        '$CAJA_CAMBIOS',
      '$B_CAJA_CAMBIOS',                '$BOMBA_EMBRAGUE',              '$B_BOMBA_EMBRAGUE',          '$CAJA_TRANSFERENCIA',      '$B_CAJA_TRANSFERENCIA',
      '$DIFERENCIAL_TRASERO',           '$B_DIFERENCIAL_TRASERO',       '$DIFERENCIAL_DELANTERO',     '$B_DIFERENCIAL_DELANTERO', '$VOLANTE',
      '$B_VOLANTE',                     '$CANA_DIRECCION',              '$B_CANA_DIRECCION',          '$CREMALLERA',              '$B_CREMALLERA',
      '$ROTULAS',                       '$B_ROTULAS',                   '$AMORTIGUADORES',            '$B_AMORTIGUADORES',        '$BARRA_TORSION',
      '$B_BARRA_TORSION',               '$BARRA_ESTABILIZADOR',         '$B_BARRA_ESTABILIZADOR',     '$LLANTAS',                 '$B_LLANTAS',
      '$CAPOT_MOTOR',                   '$B_CAPOT_MOTOR',               '$CAPOT_MALETERA',            '$B_CAPOT_MALETERA',        '$PARACHOQUES_DELANTEROS',
      '$B_PARACHOQUES_DELANTEROS',      '$PARACHOQUES_POSTERIORES',     '$B_PARACHOQUES_POSTERIORES',     '$LUNAS_LATERALES',         '$B_LUNAS_LATERALES',
      '$LUNAS_CORTAVIENTO',             '$B_LUNAS_CORTAVIENTO',         '$PARABRISAS_DELANTERO',      '$B_PARABRISAS_DELANTERO',  '$PARABRISAS_POSTERIOR',
      '$B_PARABRISAS_POSTERIOR',        '$TANQUE_COMBUSTIBLE',          '$B_TANQUE_COMBUSTIBLE',      '$PUERTAS',                 '$B_PUERTAS',
      '$ASIENTOS',                      '$B_ASIENTOS',                  '$AIRE_ACONDICIONADO',        '$B_AIRE_ACONDICIONADO',    '$ALARMA',
      '$B_ALARMA',                      '$PLUMILLAS',                   '$B_PLUMILLAS',               '$ESPEJOS',                 '$B_ESPEJOS',
      '$CINTURONES_SEGURIDAD',          '$B_CINTURONES_SEGURIDAD',      '$ANTENA',                    '$B_ANTENA',                '$OTRAS_CARACTERISTICAS_RELEVANTES',
      $VALOR_TASACION";


      $rs_Resultado = $this->execute($consulta);
      return $rs_Resultado;
    }
  }

  function F_Exportar_Excel_FichasVehiculares( $COD_ENTIDAD )
    {
    if ($this->conectar()==true)
    {
      $consulta = "EXEC SP_SNB_MUEBLES_EXPORTAR_XLS_LISTADO_FICHASVEHICULARES '$COD_ENTIDAD'";
      
      $rs_Resultado = $this->execute($consulta);
      return $rs_Resultado;
    }

    }
}
 ?>
