<?php
require_once "../../Controller/C_Interconexion_SQL.php";

class PrediosModel{
	
   private $oDBManager;
    	
	public function __construct(){
		$this->oDBManager	=	new Database;
	}
	
    	
	function TOTAL_REGISTRO_PREDIOS_X_PARAMETROS($COD_ENTIDAD, $DENOMINACION_PREDIO){
		if($this->oDBManager->conectar()==true){
						
			$query		= "
SELECT COUNT(*) AS TOT_REG 
FROM TBL_PADRON_PREDIOS P (NOLOCK)
WHERE P.ID_ESTADO  IN ('1', '5') 
AND P.COD_ENTIDAD = '$COD_ENTIDAD' 
AND (('$DENOMINACION_PREDIO' = '') OR (P.DENOMINACION_PREDIO LIKE '%$DENOMINACION_PREDIO%'))
";
			$resultado 	= $this->oDBManager->execute($query);
			
			return $resultado;
		}
	}
	
	
	function LISTA_PREDIOS_X_PARAMETROS($INI, $FIN, $COD_ENTIDAD, $DENOMINACION_PREDIO){
		if($this->oDBManager->conectar()==true){
			try{
				$SQL_TABLA ="
				SELECT P.*,
				
					EP.DESC_EST_PROPIEDAD,
				
				TP.DESC_TIP_PROPIEDAD,
				ROW_NUMBER() OVER (ORDER BY P.ID_PREDIO_SBN) AS ROW_NUMBER_ID
				FROM TBL_PADRON_PREDIOS P (NOLOCK)
				LEFT JOIN TBL_TIPO_PROPIEDAD TP (NOLOCK) ON (P.COD_TIP_PROPIEDAD = TP.COD_TIP_PROPIEDAD)					
				-- INICIO JMCR
					LEFT JOIN TBL_ESTADO_PROPIEDAD EP (NOLOCK) ON (P.ID_ESTADO = EP.COD_EST_PROPIEDAD)
					-- WHERE P.ID_ESTADO IN ('1','5')
					WHERE P.ID_ESTADO IN ('1', '3', '5') AND P.ID_ESTADO_OCULTAR <> 2
				-- FIN JMCR
				AND P.COD_ENTIDAD = '$COD_ENTIDAD' 
				AND (('$DENOMINACION_PREDIO' = '') OR (P.DENOMINACION_PREDIO LIKE '%$DENOMINACION_PREDIO%'))
				";
				$query = "SELECT * FROM ( ".$SQL_TABLA." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ";
				//echo $query;
				$resultado = $this->oDBManager->execute($query);
				return $resultado;
			}catch(Exception $e){
				die($e->getMessage());
			}
			
		}
	}


	/* USO 				: LISTADO DE USO DE PREDIO GENERAL */
	function TOTAL_REGISTRO_PREDIOS_X_PARAMETROS_GRAL($COD_ENTIDAD, $DENOMINACION_PREDIO, $BUSCAR_CUS){
		if($this->oDBManager->conectar()==true){
						
			$query		= "
			SELECT COUNT(*) AS TOT_REG 
			FROM TBL_PADRON_PREDIOS P (NOLOCK)
			WHERE P.ID_ESTADO  IN ('1') 
			AND P.COD_ENTIDAD = '$COD_ENTIDAD' 
			AND (('$DENOMINACION_PREDIO' = '') OR (P.DENOMINACION_PREDIO LIKE '%$DENOMINACION_PREDIO%'))
			AND (('$BUSCAR_CUS' = '') OR (P.CUS LIKE '%$BUSCAR_CUS%')) AND P.ID_ESTADO_OCULTAR <> 2
			";
						$resultado 	= $this->oDBManager->execute($query);
						
						return $resultado;
		}
	}
	
	/* USO 				: LISTADO DE USO DE PREDIO GENERAL */
	function LISTA_PREDIOS_X_PARAMETROS_GRAL($INI, $FIN, $COD_ENTIDAD, $DENOMINACION_PREDIO, $BUSCAR_CUS){
		if($this->oDBManager->conectar()==true){
			
			try{			
							
				$SQL_TABLA ="
				SELECT P.*,  
				TP.DESC_TIP_PROPIEDAD,
				ROW_NUMBER() OVER (ORDER BY P.ID_PREDIO_SBN) AS ROW_NUMBER_ID, 
				DU.PROP_HORIZ[PROP_HORIZ_DU]
				FROM TBL_PADRON_PREDIOS P (NOLOCK)
				LEFT JOIN TBL_TIPO_PROPIEDAD TP (NOLOCK) ON (P.COD_TIP_PROPIEDAD = TP.COD_TIP_PROPIEDAD)						
				LEFT JOIN TBL_DECRETO_URGENCIA DU ON DU.ID_PREDIO = P.ID_PREDIO_SBN
				WHERE P.ID_ESTADO IN ('1')  
				AND P.COD_ENTIDAD = '$COD_ENTIDAD' 
				AND (('$DENOMINACION_PREDIO' = '') OR (P.DENOMINACION_PREDIO LIKE '%$DENOMINACION_PREDIO%'))
				AND (('$BUSCAR_CUS' = '') OR (P.CUS LIKE '%$BUSCAR_CUS%')) AND ID_ESTADO_OCULTAR<>'2'
				";
								$query = "SELECT * FROM ( ".$SQL_TABLA." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ";
								//echo $query;
								$resultado = $this->oDBManager->execute($query);

								return $resultado;
			
			
			}catch(Exception $e){
				die($e->getMessage());
			}
			
		}
	}

	/* USO 				: LISTADO  PREDIO POR ENTIDAD */
	function LISTA_PREDIOS_X_PARAMETROS_GRAL2($COD_PREDIO){
		if($this->oDBManager->conectar()==true){
			
			try{			
							
				$query ="
				SELECT NOM_ENTIDAD, pe.RUC_ENTIDAD, (DENOMINACION_PREDIO)[DENOMINACION_PREDIO], DEPA.nmbubigeo[DEPARTAMENTO], PROV.nmbubigeo[PROVINCIA], DIST.nmbubigeo[DISTRITO], pp.cus[CUS],
					CASE PP.COD_TIP_PROPIEDAD
					    WHEN '1' THEN 'ARRENDAMIENTO'
					    WHEN '2' THEN 'PROPIO'
					    WHEN '3' THEN 'BAJO ADMINISTRACION'
					    WHEN '4' THEN 'USO'
					    WHEN '5' THEN 'OTROS'
					END[MODALIDAD],
					CASE WHEN PP.COD_TIP_PROPIEDAD <> '1' THEN
					    CASE WHEN ISNULL(PP.DESOCUPADO, '') = '' THEN 
					            'PENDIENTE'
					    ELSE
					            'ACTUALIZADO'
					    END
					ELSE
					    CASE WHEN ISNULL((
						SELECT  CONVERT(VARCHAR(10), ID_DECRETO_URGENCIA)          
					   FROM TBL_DECRETO_URGENCIA A                 
					   RIGHT JOIN TBL_PADRON_PREDIOS BB ON A.ID_PREDIO = BB.ID_PREDIO_SBN AND A.COD_ENTIDAD = BB.COD_ENTIDAD                
					   WHERE BB.ID_ESTADO = '1' AND ISNULL(BB.ID_ESTADO_OCULTAR, '') <> '2' AND BB.COD_TIP_PROPIEDAD = '1' AND PP.ID_PREDIO_SBN = A.ID_PREDIO AND A.COD_ENTIDAD = PP.COD_ENTIDAD  ), '') = '' THEN
					            'PENDIENTE'
					    ELSE
					            'ACTUALIZADO'
					    END
					END[ESTADO],
					CASE WHEN ISNULL(DUV2.ID_DECRETO_URGENCIA_V2, '') = '' THEN 'PENDIENTE' ELSE 'ACTUALIZADO' END[ESTADO_NOVIEMBRE]

					FROM TBL_PADRON_PREDIOS PP
					LEFT JOIN TBL_PADRON_ENTIDAD PE ON PP.COD_ENTIDAD = PE.COD_ENTIDAD
					LEFT JOIN TBL_UBIGEO DEPA ON (DEPA.Flag = 'D' AND DEPA.coddep = PP.COD_DEPA)
					LEFT JOIN TBL_UBIGEO PROV ON (PROV.Flag = 'P' AND PROV.coddep = PP.COD_DEPA AND PROV.codprov = PP.COD_PROV )
					LEFT JOIN TBL_UBIGEO DIST ON (DIST.Flag = 'T' AND DIST.coddep = PP.COD_DEPA AND DIST.codprov = PP.COD_PROV AND DIST.coddist = PP.COD_DIST )
					LEFT JOIN TBL_DECRETO_URGENCIA_V2 DUV2 ON DUV2.ID_PREDIO_SBN = PP.ID_PREDIO_SBN AND DUV2.COD_ENTIDAD = PP.COD_ENTIDAD 
					WHERE PP.COD_ENTIDAD = '$COD_PREDIO' AND PP.ID_ESTADO = '1' AND ISNULL(ID_ESTADO_OCULTAR, '') <> '2' ORDER BY 3
				";
								// $query = "SELECT * FROM ( ".$SQL_TABLA." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID ";
								//echo $query; 
								$resultado = $this->oDBManager->execute($query);

								return $resultado;
			
			
			}catch(Exception $e){
				die($e->getMessage());
			}
			
		}
	}


	function VER_DATOS_ENTIDAD_X_CODIGO($COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){			
			$query ="SELECT * FROM TBL_PADRON_ENTIDAD WHERE COD_ENTIDAD = '$COD_ENTIDAD' ";
			$resultado = $this->oDBManager->execute($query);
			return $resultado;			
		}
	}
	
	function VER_DATOS_PREDIO_X_CODIGO($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){			
			$query ="SELECT * FROM TBL_PADRON_PREDIOS (NOLOCK) WHERE ID_PREDIO_SBN = '$ID_PREDIO_SBN' ";
			$resultado = $this->oDBManager->execute($query);
			return $resultado;			
		}
	}



	
	
	function TOTAL_BIENES_MUEBLES_x_PREDIO($COD_ENTIDAD, $ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
						
			$sql="
SELECT 
BP.ID_PREDIO_SBN,
COUNT(BP.COD_UE_BIEN_PATRI) AS TOT_BIEN
FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL BP (NOLOCK)
WHERE BP.ID_ESTADO = '1'
AND BP.COD_ENTIDAD = '$COD_ENTIDAD'
AND BP.ID_PREDIO_SBN = '$ID_PREDIO_SBN'
GROUP BY 
BP.ID_PREDIO_SBN
				";			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	
	function Ver_Datos_Local_x_CODIGO($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * 
			FROM TBL_PADRON_PREDIOS (NOLOCK) 
			WHERE convert(CHAR, ID_PREDIO_SBN) = '$ID_PREDIO_SBN' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}


	/* USO 				: LISTADO DE USO DE PREDIO GENERAL */
	function Ver_Datos_Local_x_CODIGO_GRAL($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$sql="SET DATEFORMAT DMY SELECT P.*,
			CASE WHEN P.PARTIDA_REGISTRAL = 1 THEN P.PARTIDA_ELECTRONICA 
				 WHEN P.PARTIDA_REGISTRAL = 2 THEN P.CODIGO_PREDIO
				 WHEN P.PARTIDA_REGISTRAL = 3 THEN P.FICHA 
				 WHEN P.PARTIDA_REGISTRAL = 4 THEN P.FOJAS ELSE '' END [DU_P_FOJAS],
			P.TOMO[DU_P_TOMO], 
			P.PARTIDA_REGISTRAL[DU_P_PARTIDA_REGISTRAL],
			P.NRO_TRABAJADORES[DU_P_NRO_TRABAJADORES], 
			P.LICENCIA_FUNCIONAMIENTO[DU_P_LICENCIA_FUNCIONAMIENTO],
			P.FECHA_VIGENCIA[DU_P_FECHA_VIGENCIA],
			P.CERTIFICADO_DEFENSA_CIVIL[DU_P_CERTIFICADO_DEFENSA_CIVIL],
			P.ID_USO_PREDIO[DU_P_ID_USO_PREDIO],
			DU.ID_USO_PREDIO[ID_USO_PREDIO_DU],
			DU.AREA_TERRENO[AREA_TERRENO_DU],
			DU.PROP_HORIZ[PROP_HORIZ_DU],
			DU.NRO_PISOS_ARRENDADO[NRO_PISOS_ARRENDADO_DU],
			DU.AREA_TOTAL_ARRENDADO[AREA_TOTAL_ARRENDADO_DU],
			DU.ESPECIFIQUE_USO[ESPECIFIQUE_USO_DU],
			DU.NRO_TRABAJADORES[NRO_TRABAJADORES_DU],
			DU.LICENCIA_FUNCIONAMIENTO[LICENCIA_FUNCIONAMIENTO_DU],
  			DU.FECHA_VIGENCIA[FECHA_VIGENCIA_DU],
  			DU.CERTIFICADO_DEFENSA_CIVIL[CERTIFICADO_DEFENSA_CIVIL_DU],  
			DU.NOMBRE_ARRENDADOR[NOMBRE_ARRENDADOR_DU],
			DU.DOCIDENTIDAD[DOCIDENTIDAD_DU],
			DU.NRODOCUMENTO[NRODOCUMENTO_DU],
			DU.FECHA_FIRMA_CONTRATO[FECHA_FIRMA_CONTRATO_DU],
			DU.RENTA_MENSUAL[RENTA_MENSUAL_DU],
			DU.MONEDA_RENTA_MENSUAL[MONEDA_RENTA_MENSUAL_DU],
			DU.FECHA_VCTO_CONTRATO[FECHA_VCTO_CONTRATO_DU],
			DU.ANIOS_TOTAL_ARRENDAMIENTO[ANIOS_TOTAL_ARRENDAMIENTO_DU],
			DU.MESES_TOTAL_ARRENDAMIENTO[MESES_TOTAL_ARRENDAMIENTO_DU],
			DU.MONTO_TOTAL_PAGADO_ARRENDAMIENTO[MONTO_TOTAL_PAGADO_ARRENDAMIENTO_DU],
			DU.MONEDA_MONTO_TOTAL_PAGADO_ARRENDAMIENTO[MONEDA_MONTO_TOTAL_PAGADO_ARRENDAMIENTO_DU],
			P.ESPECIFIQUE_ACTOS,
			P.ESPECIFIQUE_MODALIDAD,
			DU.FECHA_VIGENCIA_CERTIFICADO[FECHA_VIGENCIA_CERTIFICADO_DU],
			DU.COD_ECONSERVACION_CERTIFICADO[COD_ECONSERVACION_CERTIFICADO_DU],
			DU.VALOR_M_CUADRADO[VALOR_M_CUADRADO_DU],
			DU.MONEDA_VALOR_M_CUADRADO[MONEDA_VALOR_M_CUADRADO_DU]		
			FROM TBL_PADRON_PREDIOS P(NOLOCK) 
			LEFT JOIN TBL_DECRETO_URGENCIA DU (NOLOCK)  ON DU.ID_PREDIO = P.ID_PREDIO_SBN
			WHERE convert(CHAR, P.ID_PREDIO_SBN) = '$ID_PREDIO_SBN' ";
			// echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}


	/* USO 				: LISTADO DE USO DE PREDIO GENERAL */
	function Ver_Datos_Local_x_CODIGO_GRAL_DU_v2($ID_PREDIO_SBN, $COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){
			$sql="EXEC SP_MOSTRAR_DATOS_PREDIO_A_MODIFICAR_DU '$ID_PREDIO_SBN', '$COD_ENTIDAD'";
			// echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado; 
		}
	}

	/* USO 				: LISTADO DE USO DE PREDIO GENERAL */
	function concluir_proceso_GRAL($COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_ENTIDADES_ACCESO_DU SET ID_ESTADO_CONCLUIDO = '2' 
			WHERE convert(CHAR, COD_ENTIDAD) = '$COD_ENTIDAD' ";
			//echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	/* USO 				:ELIMINACION(OCULTAR) DE PREDIO */
	function Eliminar_Predio_Local_GRAL($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO_OCULTAR = '2' 
			WHERE convert(CHAR, ID_PREDIO_SBN) = '$ID_PREDIO_SBN' ";
			//echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	/* USO 				: LISTADO DE USO DE PREDIO GENERAL */
	function Listar_Tipo_Propiedad(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_TIPO_PROPIEDAD WHERE ID_ESTADO ='1' ORDER BY COD_TIP_PROPIEDAD ";
			$resultado=$this->oDBManager->execute($sql);
			return $resultado;
		}
	}


	/* USO 				: LISTADO DE USO DE PREDIO GENERAL */
	function Listar_Tipo_Propiedad_GRAL(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_TIPO_PROPIEDAD WHERE ID_ESTADO ='1' AND COD_TIP_PROPIEDAD NOT IN (4) ORDER BY COD_TIP_PROPIEDAD ";
			//echo $sql;
			$resultado=$this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	/*********************************************************************************/
	/*********************************************************************************/
	
	
	function Padron_Generar_Codigo_Padron_Predios(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(ID_PREDIO_SBN),0)+1) AS CODIGO FROM  TBL_PADRON_PREDIOS ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Padron_Insertar_Local_Predios_Basico($ID_PREDIO_SBN, $COD_ENTIDAD, $MODULO_REGISTRO, $USUARIO){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_PADRON_PREDIOS  (ID_PREDIO_SBN, COD_ENTIDAD, FECHA_REGISTRO, MODULO_REGISTRO, MUEBLES_USUARIO_CREACION, MUEBLES_FECHA_CREACION, ID_ESTADO)
						 VALUES ('$ID_PREDIO_SBN', '$COD_ENTIDAD', GETDATE(), '$MODULO_REGISTRO', '$USUARIO', GETDATE(), '0' ) ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Padron_Insertar_Local_Predios_x_Muebles($ID_PREDIO_SBN, $COD_ENTIDAD, $DENOMINACION_PREDIO, $COD_DEPA, $COD_PROV, $COD_DIST, $COD_ZONA, $COD_TVIA, $NOMBRE_VIA, $NRO_PRED, $MZA_PRED, $LTE_PRED, $UBICADO_PISO, $DET_PRED, $DET_PRED_NRO, $COD_THABILITACION, $NOM_THABILITACION, $NOM_SECTOR, $COD_TIP_PROPIEDAD, $MODULO_REGISTRO, $USUARIO, $ID_ESTADO){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_PADRON_PREDIOS  (ID_PREDIO_SBN, COD_ENTIDAD, DENOMINACION_PREDIO, COD_DEPA, COD_PROV, COD_DIST, COD_ZONA, COD_TVIA, NOMBRE_VIA, NRO_PRED, MZA_PRED, LTE_PRED, UBICADO_PISO, DET_PRED, DET_PRED_NRO, COD_THABILITACION, NOM_THABILITACION, NOM_SECTOR, COD_TIP_PROPIEDAD, FECHA_REGISTRO, MODULO_REGISTRO, MUEBLES_USUARIO_CREACION, MUEBLES_FECHA_CREACION, ID_ESTADO)
						 VALUES ('$ID_PREDIO_SBN', '$COD_ENTIDAD', '$DENOMINACION_PREDIO', '$COD_DEPA', '$COD_PROV', '$COD_DIST', '$COD_ZONA', '$COD_TVIA', '$NOMBRE_VIA', '$NRO_PRED', '$MZA_PRED', '$LTE_PRED', '$UBICADO_PISO', '$DET_PRED', '$DET_PRED_NRO', '$COD_THABILITACION', '$NOM_THABILITACION', '$NOM_SECTOR', '$COD_TIP_PROPIEDAD', GETDATE(), '$MODULO_REGISTRO', '$USUARIO', GETDATE(), '$ID_ESTADO') ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Verificar_Predio_Reg_TBL_CONTROL_PREDIO($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT COUNT(*) AS TOT_REG FROM TBL_CONTROL_PREDIO WHERE ID_PREDIO_SBN = '$ID_PREDIO_SBN' ";
			$resultado = $this->oDBManager->execute($consulta);
			return $resultado;
			
		}
	}
	
	function Insertar_Predio_Reg_TBL_CONTROL_PREDIO($ID_PREDIO_SBN, $ID_USUARIO){
		if($this->oDBManager->conectar()==true){
			$consulta="
			INSERT INTO TBL_CONTROL_PREDIO (ID_PREDIO_SBN, ID_USUARIO, FECHA_REG_USU, ID_ESTADO) 
			VALUES ('$ID_PREDIO_SBN', '$ID_USUARIO', GETDATE(), 1) ";
			$result = $this->oDBManager->execute($consulta);
			//$this->oDBManager->close();
			return $result;
			
		}
	}
	
	
	function Padron_Actualizar_Local_Predios_x_Muebles($ID_PREDIO_SBN, $COD_ENTIDAD, $DENOMINACION_PREDIO, $COD_DEPA, $COD_PROV, $COD_DIST, $COD_ZONA, $COD_TVIA, $NOMBRE_VIA, $NRO_PRED, $MZA_PRED, $LTE_PRED, $UBICADO_PISO, $DET_PRED, $DET_PRED_NRO, $COD_THABILITACION, $NOM_THABILITACION, $NOM_SECTOR, $COD_TIP_PROPIEDAD, $MODULO_REGISTRO, $USUARIO, $ID_ESTADO){
		if($this->oDBManager->conectar()==true){
			$sql="
			UPDATE TBL_PADRON_PREDIOS  SET  
			COD_ENTIDAD = '$COD_ENTIDAD', 
			DENOMINACION_PREDIO = '$DENOMINACION_PREDIO', 
			COD_DEPA = '$COD_DEPA', 
			COD_PROV = '$COD_PROV', 
			COD_DIST = '$COD_DIST', 
			COD_ZONA = '$COD_ZONA', 
			COD_TVIA = '$COD_TVIA', 
			NOMBRE_VIA = '$NOMBRE_VIA', 
			NRO_PRED = '$NRO_PRED', 
			MZA_PRED = '$MZA_PRED', 
			LTE_PRED = '$LTE_PRED', 			
			UBICADO_PISO = '$UBICADO_PISO', 
			DET_PRED = '$DET_PRED', 
			DET_PRED_NRO = '$DET_PRED_NRO', 
			COD_THABILITACION = '$COD_THABILITACION', 
			NOM_THABILITACION = '$NOM_THABILITACION', 			
			NOM_SECTOR = '$NOM_SECTOR', 
			COD_TIP_PROPIEDAD = '$COD_TIP_PROPIEDAD', 
			MODULO_REGISTRO = '$MODULO_REGISTRO',
			MUEBLES_USUARIO_MODIFICA = '$USUARIO', 
			MUEBLES_FECHA_MODIFICA = GETDATE(),
			ID_ESTADO = '$ID_ESTADO',
			FECHA_ENVIO_REG_A_SBN = GETDATE()
			WHERE ID_PREDIO_SBN = '$ID_PREDIO_SBN' 
			";
			$result = $this->oDBManager->execute($sql);
			$this->oDBManager->close();
			return $result;
		}
	}

	/* USO 				: LISTADO DE USO DE PREDIO GENERAL */
	function Padron_Actualizar_Local_Predios_x_Muebles_Gral($ID_PREDIO_SBN, $COD_ENTIDAD, 
				$cbo_departamento, $cbo_provincia,
				$cbo_distrito, $cbo_via, $txt_nom_via, $txt_nro_inmueble, $cbo_prop_horiz, $txt_manzana, $txt_lote, 
				$cbo_habilitacion, $txt_nom_habilitacion, $txt_nom_inmueble, $COD_TIP_PROPIEDAD, $cbo_actos, $cbo_TipoArrendamiento,
				$txt_CUS, $txt_especifique_modalidad, $txt_especifique_actos,
				$txt_area_terreno_pro, $cbo_desocupado_pro, $cbo_uso_inmuebles_pro, $txt_especifique_pro, $txt_area_construida_pro, 
				$cbo_pisos_ocupados_pro, $cbo_est_conservacion_pro, $txt_propietario_registral_pro, $txt_ruc_pro, 
				$cbo_oficina_registral_pro, $cbo_partida_registral_pro, $txt_tomo_pro, $txt_fojas_pro, $USUARIO, $ID_ESTADO,
				$MODULO_REGISTRO,$txt_nro_trabajadores_prop,$cbo_licen_funcion_prop,$txt_fecha_vigen_prop,$cbo_certif_defen_prop,$txt_fecha_vigencia_certificado_pro){


		if($this->oDBManager->conectar()==true){
			$sql="SET DATEFORMAT DMY 
			UPDATE TBL_PADRON_PREDIOS  SET  
			COD_ENTIDAD = '$COD_ENTIDAD', 
			COD_DEPA = '$cbo_departamento', 
			COD_PROV = '$cbo_provincia', 
			COD_DIST = '$cbo_distrito', 
			COD_TVIA = '$cbo_via',
			NOMBRE_VIA = '$txt_nom_via',
			NRO_PRED = '$txt_nro_inmueble',
			PROP_HORIZ = '$cbo_prop_horiz',
			MZA_PRED			= '$txt_manzana',
			LTE_PRED 			= '$txt_lote',
			COD_THABILITACION	= '$cbo_habilitacion',
			NOM_THABILITACION	= '$txt_nom_habilitacion',
			DENOMINACION_PREDIO	= '$txt_nom_inmueble',
			COD_TIP_PROPIEDAD	= '$COD_TIP_PROPIEDAD',
			ACTOS 				= '$cbo_actos',
			TIPO_ARRENDAMIENTO	= '$cbo_TipoArrendamiento',
			CUS 				= '$txt_CUS',
			ESPECIFIQUE_MODALIDAD 	= '$txt_especifique_modalidad',
			ESPECIFIQUE_ACTOS 		= '$txt_especifique_actos',
			AREA_TERRENO 		= CASE WHEN ISNULL('$txt_area_terreno_pro', '') = '' THEN 0 ELSE $txt_area_terreno_pro END,
			DESOCUPADO 			= '$cbo_desocupado_pro',
			ID_USO_PREDIO 		= '$cbo_uso_inmuebles_pro',
			ESPECIFIQUE_USO		= '$txt_especifique_pro',
			AREA_CONSTRUIDA 	= CASE WHEN ISNULL('$txt_area_construida_pro', '') = '' THEN 0 ELSE $txt_area_construida_pro END,
			UBICADO_PISO 		= '$cbo_pisos_ocupados_pro',
			COD_ECONSERVACION 	= '$cbo_est_conservacion_pro',
			NOM_PROP_REGISTRAL 	= '$txt_propietario_registral_pro',
			NRO_RUC				= '$txt_ruc_pro',
			COD_OFIC_REGISTRAL	= '$cbo_oficina_registral_pro',
			PARTIDA_REGISTRAL 	= '$cbo_partida_registral_pro',
			PARTIDA_ELECTRONICA 	= CASE WHEN '$cbo_partida_registral_pro' = 1 THEN '$txt_fojas_pro' ELSE '' END,
			CODIGO_PREDIO = CASE WHEN '$cbo_partida_registral_pro' = 2 THEN '$txt_fojas_pro' ELSE '' END,
			FICHA = CASE WHEN '$cbo_partida_registral_pro' = 3 THEN '$txt_fojas_pro' ELSE '' END,
			TOMO 				= '$txt_tomo_pro',
			FOJAS 				= CASE WHEN '$cbo_partida_registral_pro' = 4 THEN '$txt_fojas_pro' ELSE '' END,
			INMUEBLES_USUARIO_MODIFICA 	= '$USUARIO',
			ID_ESTADO			= '$ID_ESTADO',
			INMUEBLES_FECHA_MODIFICA	= GETDATE(),
			FECHA_REGISTRO 				= GETDATE(),
			MODULO_REGISTRO 			= '$MODULO_REGISTRO',
			COD_ZONA = NULL,
			ESTADO_DECRETO = 1,
			FECHA_ENVIO_REG_A_SBN 		= GETDATE(),
			NRO_TRABAJADORES = '$txt_nro_trabajadores_prop',
			LICENCIA_FUNCIONAMIENTO = '$cbo_licen_funcion_prop',
			FECHA_VIGENCIA = CASE WHEN '$txt_fecha_vigen_prop' = '' THEN NULL ELSE '$txt_fecha_vigen_prop' END,
			CERTIFICADO_DEFENSA_CIVIL = '$cbo_certif_defen_prop',
			FECHA_VIGENCIA_CERTIFICADO = CASE WHEN '$txt_fecha_vigencia_certificado_pro' = '' THEN NULL ELSE '$txt_fecha_vigencia_certificado_pro' END
			WHERE ID_PREDIO_SBN 		= '$ID_PREDIO_SBN'";

			//prop_horiz = case when '$cbo_prop_horiz' = '' then NULL else '$cbo_prop_horiz' end,
			
			//echo $sql;
			$result = $this->oDBManager->execute($sql);
			$this->oDBManager->close();
			return $result;
		}
	}

	/* USO 				: LISTADO DE USO DE PREDIO GENERAL */
	function Padron_Insertar_Local_Predios_x_Muebles_Gral($ID_PREDIO_SBN, $COD_ENTIDAD, $cbo_departamento, $cbo_provincia,
				$cbo_distrito, $cbo_via, $txt_nom_via, $txt_nro_inmueble, $cbo_prop_horiz, $txt_manzana, $txt_lote, 
				$cbo_habilitacion, $txt_nom_habilitacion, $txt_nom_inmueble, $COD_TIP_PROPIEDAD, $cbo_actos, $cbo_TipoArrendamiento,
				$txt_CUS, $txt_especifique_modalidad, $txt_especifique_actos,
				$txt_area_terreno_pro, $cbo_desocupado_pro, $cbo_uso_inmuebles_pro, $txt_especifique_pro, $txt_area_construida_pro, 
				$cbo_pisos_ocupados_pro, $cbo_est_conservacion_pro, $txt_propietario_registral_pro, $txt_ruc_pro, 
				$cbo_oficina_registral_pro, $cbo_partida_registral_pro, $txt_tomo_pro, $txt_fojas_pro, $USUARIO, $ID_ESTADO, 
				$MODULO_REGISTRO,$txt_nro_trabajadores_prop,$cbo_licen_funcion_prop,$txt_fecha_vigen_prop,$cbo_certif_defen_prop,$txt_fecha_vigencia_certificado_pro){
 
		if($this->oDBManager->conectar()==true){
			$consulta="SET DATEFORMAT DMY  INSERT INTO TBL_PADRON_PREDIOS  (ID_PREDIO_SBN, COD_ENTIDAD, COD_DEPA, COD_PROV, 
				COD_DIST, COD_TVIA, NOMBRE_VIA, NRO_PRED, PROP_HORIZ, MZA_PRED, LTE_PRED, 
				COD_THABILITACION, NOM_THABILITACION, DENOMINACION_PREDIO, COD_TIP_PROPIEDAD, ACTOS, TIPO_ARRENDAMIENTO,
				CUS, ESPECIFIQUE_MODALIDAD, ESPECIFIQUE_ACTOS,
				AREA_TERRENO, DESOCUPADO, ID_USO_PREDIO, ESPECIFIQUE_USO, AREA_CONSTRUIDA, 
				UBICADO_PISO, COD_ECONSERVACION, NOM_PROP_REGISTRAL, NRO_RUC, 
				COD_OFIC_REGISTRAL, PARTIDA_REGISTRAL, PARTIDA_ELECTRONICA,CODIGO_PREDIO, FICHA, TOMO, FOJAS, INMUEBLES_USUARIO_CREACION, ID_ESTADO, 
				INMUEBLES_FECHA_CREACION, FECHA_REGISTRO, MODULO_REGISTRO, FECHA_ENVIO_REG_A_SBN, COD_ZONA,NRO_TRABAJADORES, LICENCIA_FUNCIONAMIENTO, FECHA_VIGENCIA, CERTIFICADO_DEFENSA_CIVIL, FECHA_VIGENCIA_CERTIFICADO)

						 VALUES ($ID_PREDIO_SBN, $COD_ENTIDAD, '$cbo_departamento', '$cbo_provincia',
				'$cbo_distrito', '$cbo_via', '$txt_nom_via', '$txt_nro_inmueble', '$cbo_prop_horiz', '$txt_manzana', '$txt_lote', 
				'$cbo_habilitacion', '$txt_nom_habilitacion', '$txt_nom_inmueble', '$COD_TIP_PROPIEDAD', '$cbo_actos', '$cbo_TipoArrendamiento',
				'$txt_CUS', '$txt_especifique_modalidad', '$txt_especifique_actos',
				CASE WHEN ISNULL('$txt_area_terreno_pro', '0') = '0' THEN '0' ELSE '$txt_area_terreno_pro' END,
				'$cbo_desocupado_pro', '$cbo_uso_inmuebles_pro', '$txt_especifique_pro', 
				CASE WHEN ISNULL('$txt_area_construida_pro', '0') = '0' THEN '0' ELSE '$txt_area_construida_pro' END,
				CASE WHEN ISNULL('$cbo_pisos_ocupados_pro', '') = '' THEN 0 ELSE '$cbo_pisos_ocupados_pro' END,
				'$cbo_est_conservacion_pro', '$txt_propietario_registral_pro', '$txt_ruc_pro', 
				'$cbo_oficina_registral_pro', '$cbo_partida_registral_pro', 
				CASE WHEN '$cbo_partida_registral_pro' = 1 THEN '$txt_fojas_pro' ELSE '' END,
				CASE WHEN '$cbo_partida_registral_pro' = 2 THEN '$txt_fojas_pro' ELSE '' END,
				CASE WHEN '$cbo_partida_registral_pro' = 3 THEN '$txt_fojas_pro' ELSE '' END,
				'$txt_tomo_pro',
				CASE WHEN '$cbo_partida_registral_pro' = 4 THEN '$txt_fojas_pro' ELSE '' END,
				'$USUARIO', '$ID_ESTADO',
				GETDATE(), GETDATE(), '$MODULO_REGISTRO', GETDATE(), NULL,$txt_nro_trabajadores_prop,'$cbo_licen_funcion_prop',
				CASE WHEN '$txt_fecha_vigen_prop' = '' THEN NULL ELSE '$txt_fecha_vigen_prop' END, 
				'$cbo_certif_defen_prop',
				CASE WHEN '$txt_fecha_vigencia_certificado_pro' = '' THEN NULL ELSE '$txt_fecha_vigencia_certificado_pro' END)";
			//echo $consulta; 
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}


	/* USO 				: LISTADO DE USO DE PREDIO GENERAL */ 
	function Registrar_Datos_Inmuebles_DU_v2(
				$ACCION,  $ID_PREDIO_SBN, $COD_ENTIDAD, 
				$txt_nom_inmueble, $cbo_departamento, $cbo_provincia, $cbo_distrito, 
				$cbo_via, $txt_nom_via, $txt_numero, $txt_manzana, $txt_lote, 
				$txt_nom_habilitacion, $cbo_uso_inmueble_alq, $txt_area_total, $txt_area_util, $txt_pers_cas, $txt_pers_cap, $txt_pers_fag, 
				$txt_pers_terceros, $txt_pers_practicantes, $txt_pers_vigilantes, $txt_pers_limpieza, $txt_pers_jardineria, $txt_pers_mantenimiento, $cbo_est_conservacion_pro,
				$txt_renta_mensual, $TXH_SIMI_COD_USUARIO,$inmueble_estatal,$es_arendado
			){
 
		if($this->oDBManager->conectar()==true){ 

			$consulta="SET DATEFORMAT DMY EXEC SP_GUARDAR_DATOS_PREDIO_DU 
				'$ACCION',  '$ID_PREDIO_SBN', '$COD_ENTIDAD', 
				'$txt_nom_inmueble', '$cbo_departamento', '$cbo_provincia', '$cbo_distrito', 
				'$cbo_via', '$txt_nom_via', '$txt_numero', '$txt_manzana', '$txt_lote', 
				'$txt_nom_habilitacion', '$cbo_uso_inmueble_alq', '$txt_area_total', '$txt_area_util', '$txt_pers_cas', '$txt_pers_cap', '$txt_pers_fag',
				'$txt_pers_terceros', '$txt_pers_practicantes', '$txt_pers_vigilantes', '$txt_pers_limpieza', '$txt_pers_jardineria', '$txt_pers_mantenimiento', '$cbo_est_conservacion_pro', 
				'$txt_renta_mensual', '$TXH_SIMI_COD_USUARIO', '$inmueble_estatal', '$es_arendado'"; 
			//echo $consulta;
			$result = $this->oDBManager->execute($consulta); 
			$this->oDBManager->close();
			return $result;
		}
	}

	/* USO 				: GUARDAR DATOS DE LOCAL ALQUILADO */
	function Insertar_Datos_Predios_Alquilados_Gral($COD_ENTIDAD,
				$ID_PREDIO_SBN, 
				$txt_area_terreno_alq,
				$cbo_prop_horizontal_alq,
				$cbo_pisos_alq,
				$txt_area_arrendada_alq,
				$cbo_uso_inmueble_alq,
				$txt_especifique_alq,
				$txt_nro_trabajadores_alq,
				$cbo_licencia_funcionamiento_alq,
				$txt_fecha_vigencia_alq,
				$cbo_centificado_defensa_civil_alq,
				$txt_nombre_arrendador_alq,
				$cbo_Doc_Identidad_alq,
				$txt_nro_doc_alq,
				$txt_fecha_firma_contrato_alq,
				$txt_renta_mensual_alq,
				$cbo_tipo_moneda_alq,
				$txt_fecha_vcto_contrato_alq,
				$cbo_Anio_tiempoTotal_alq,
				$cbo_Mes_tiempoTotal_alq,
				$txt_monto_total_pagado_alq,
				$cbo_tipo_moneda_total_alq,
				$TXH_SIMI_COD_USUARIO,
				$txt_fecha_vigencia_certificado_alq,
				$cbo_est_conservacion_certificado_alq,
				$txt_valor_m_cuadrado_alq,
				$cbo_tipo_mon_valor_alq
			){
		//echo $cbo_tipo_mon_valor_alq;
		if($this->oDBManager->conectar()==true){
			$consulta="SET DATEFORMAT DMY EXEC SP_Insertar_Actualizar_Datos_Predios_Alquilados_Gral
			
				'$COD_ENTIDAD', 
				'$ID_PREDIO_SBN', 
				'$txt_area_terreno_alq',
				'$cbo_prop_horizontal_alq',
				'$cbo_pisos_alq',
				'$txt_area_arrendada_alq',
				'$cbo_uso_inmueble_alq',
				'$txt_especifique_alq',
				'$txt_nro_trabajadores_alq',
				'$cbo_licencia_funcionamiento_alq',
				'$txt_fecha_vigencia_alq',
				'$cbo_centificado_defensa_civil_alq',
				'$txt_nombre_arrendador_alq',
				'$cbo_Doc_Identidad_alq',
				'$txt_nro_doc_alq',
				'$txt_fecha_firma_contrato_alq',
				'$txt_renta_mensual_alq',
				'$cbo_tipo_moneda_alq',
				'$txt_fecha_vcto_contrato_alq',
				'$cbo_Anio_tiempoTotal_alq',
				'$cbo_Mes_tiempoTotal_alq',
				'$txt_monto_total_pagado_alq',
				'$cbo_tipo_moneda_total_alq',
				'$TXH_SIMI_COD_USUARIO',
				'$txt_fecha_vigencia_certificado_alq',
				'$cbo_est_conservacion_certificado_alq',
				'$txt_valor_m_cuadrado_alq',
				'$cbo_tipo_mon_valor_alq'
			 ";
			//echo $consulta;
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}	
	
	
	function Padron_Actualizar_Local_Predios_x_Inmuebles_Datos_Registrales($ID_PREDIO_SBN, $CUS, $RSINABIP, $NOM_PROP_REGISTRAL, $COD_OFIC_REGISTRAL, $PARTIDA_ELECTRONICA, $CODIGO_PREDIO, $AREA_REGISTRAL, $COD_TIP_UNID_MED, $TOMO, $ASIENTO, $FOJAS, $FICHA, $ANOTACION_PREVENTIVA, $USUARIO){
		if($this->oDBManager->conectar()==true){
			$sql="
			UPDATE TBL_PADRON_PREDIOS  SET  
			CUS = CAST('$CUS' AS INT), 
			RSINABIP = CAST('$RSINABIP' AS INT), 
			NOM_PROP_REGISTRAL = '$NOM_PROP_REGISTRAL', 
			COD_OFIC_REGISTRAL = '$COD_OFIC_REGISTRAL', 
			PARTIDA_ELECTRONICA = '$PARTIDA_ELECTRONICA', 
			CODIGO_PREDIO = '$CODIGO_PREDIO', 
			AREA_REGISTRAL = CAST('$AREA_REGISTRAL' AS FLOAT), 
			COD_TIP_UNID_MED = '$COD_TIP_UNID_MED', 
			TOMO = '$TOMO', 
			ASIENTO = '$ASIENTO', 
			FOJAS = '$FOJAS', 
			FICHA = '$FICHA', 
			ANOTACION_PREVENTIVA = '$ANOTACION_PREVENTIVA',				
			INMUEBLES_USUARIO_MODIFICA = '$USUARIO', 
			INMUEBLES_FECHA_MODIFICA = GETDATE()
			WHERE ID_PREDIO_SBN = '$ID_PREDIO_SBN' 
			";
			$result = $this->oDBManager->execute($sql);
			//$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function Padron_Actualizar_Local_Predios_x_Inmuebles_Datos_Tecnicos($ID_PREDIO_SBN, $ZONIFICACION, $COD_TIP_TERRENO, $PERIMETRO, $AREA_TERRENO, $AREA_CONSTRUIDA, $COD_ECONSERVACION, $NUMERO_PISOS, $COD_ESANEAMIENTO, $CODIGO_DETALLE_ESANEAMIENTO, $CODIGO_USO_GENERICO, $OTROS_USO_PREDIO, $USUARIO){
		if($this->oDBManager->conectar()==true){
			$sql="
			UPDATE TBL_PADRON_PREDIOS  SET  
			ZONIFICACION = '$ZONIFICACION', 
			COD_TIP_TERRENO = '$COD_TIP_TERRENO', 
			
			PERIMETRO = '$PERIMETRO', 
			AREA_TERRENO = '$AREA_TERRENO', 
			AREA_CONSTRUIDA = '$AREA_CONSTRUIDA', 
			COD_ECONSERVACION = '$COD_ECONSERVACION', 
			NUMERO_PISOS = '$NUMERO_PISOS', 
			COD_ESANEAMIENTO = '$COD_ESANEAMIENTO', 
			CODIGO_DETALLE_ESANEAMIENTO = '$CODIGO_DETALLE_ESANEAMIENTO', 
			CODIGO_USO_GENERICO = '$CODIGO_USO_GENERICO', 
			OTROS_USO_PREDIO = '$OTROS_USO_PREDIO', 
			INMUEBLES_USUARIO_MODIFICA = '$USUARIO', 
			INMUEBLES_FECHA_MODIFICA = GETDATE() 
			WHERE ID_PREDIO_SBN = '$ID_PREDIO_SBN' ";
			//echo $sql;
			$result = $this->oDBManager->execute($sql);
			//$this->oDBManager->close();
			return $result;
		}
	}
	
	/*********************************************************************************/
	/*********************************************************************************/
	
	function Lista_Tipo_Zona_Inmueble(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_TIPO_ZONA WHERE EST_ZONA = 1 AND INDICADOR = 'INMUEBLES'";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Lista_Tipo_Via(){
		if($this->oDBManager->conectar()==true){
			$sql=" SELECT * FROM TBL_TIPO_VIA";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Lista_Tipo_Habilitacion(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_TIPO_HABILITACION ORDER BY REPLACE(TXT_THABILITACION,'NINGUNO','0') ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Listar_Tipo_Terreno(){
		if($this->oDBManager->conectar()==true){
			$sql=" SELECT * FROM TBL_TIPO_TERRENO WHERE ID_ESTADO = 1 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Lista_Estado_Conservacion(){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT * FROM TBL_ESTADO_CONSERVACION ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	
	/**************************************************************************************/
	
	function Lista_Estado_Saneamiento(){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT * FROM TBL_ESTADO_SANEAMIENTO";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Lista_Estado_Saneamiento_Detalle($detalle_filtro){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT codigo_detalle, dsc_detalle  FROM TBL_MAESTRO_SINABIP WHERE codigo_maestro = '005' and detalle_filtro = '$detalle_filtro' ";
			$result = $this->oDBManager->execute($consulta);
			
			return $result;
		}
	}
	
	
	function Lista_Uso_Predio(){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT * FROM TBL_USO_PREDIO ORDER BY NOMBRE_USO_GENERICO"; 
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}


	/* USO 				: LISTADO DE USO DE PREDIO GENERAL */

	function Listado_Uso_Predio_Gral(){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT * FROM TBL_USO_PREDIO_GRAL WHERE ID_ESTADO = 2 ORDER BY ID_USO_PREDIO";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	/*********************************************************************************************/
	
	function Lista_Oficina_registral(){
		if($this->oDBManager->conectar()==true){
			$sql=" SELECT * FROM TBL_PADRON_ZONA_REGISTRAL_OFICINA WHERE ID_ESTADO = 1 ORDER BY NOM_OFICINA_REGISTRAL ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Ver_Oficina_registral_x_Codigo($COD_OFIC_REGISTRAL){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_PADRON_ZONA_REGISTRAL_OFICINA WHERE COD_OFIC_REGISTRAL = '$COD_OFIC_REGISTRAL'";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Listar_Unidad_Medida(){
		if($this->oDBManager->conectar()==true){
			$sql=" SELECT * FROM TBL_TIPO_UNID_MED WHERE ID_ESTADO = 1 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	/*****************************************************************************************************/
	//VALORIZACION
	/*****************************************************************************************************/
	
	
	function LISTAR_VALORIZACIONES_PREDIOS_X_ID_PREDIO_SBN($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			
			$sql="
SELECT
ROW_NUMBER() OVER (ORDER BY PV.COD_VAL_PREDIO) AS ITEM,
PV.*,
TV.NOM_TIP_VALORIZACION,
M.TXT_MONEDA,
M.TXT_MONEDA_ABREV,
ITD.NOM_TIP_DOC,
DTECN.NOM_ARCHIVO
FROM TBL_PADRON_PREDIOS_VALORIZACION PV
LEFT JOIN TBL_TIPO_VALORIZACION TV ON (PV.COD_TIP_VALORIZACION = TV.COD_TIP_VALORIZACION)
LEFT JOIN TBL_MONEDA M ON (M.COD_MONEDA = PV.COD_MONEDA_VALORIZACION)				
LEFT JOIN TBL_INMUEBLES_TIPO_DOC ITD ON (ITD.COD_TIP_DOC_PRED = PV.COD_TIP_DOC_PRED)
LEFT JOIN TBL_PADRON_PREDIOS_DOC_TECN DTECN ON (DTECN.COD_PREDIO_DOC_TECN = PV.COD_PREDIO_DOC_TECN)
WHERE PV.ID_ESTADO IN (0,1) 
AND PV.ID_PREDIO_SBN = '$ID_PREDIO_SBN'

				";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	/***********************************************************************************/
	
	function Lista_Inmuebles_Tipo_Doc(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_INMUEBLES_TIPO_DOC WHERE ID_ESTADO = '1' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Lista_Inmuebles_Tipo_Doc_X_COD_TIP_DOC_PRED($COD_TIP_DOC_PRED){
		if($this->oDBManager->conectar()==true){
			
			if($COD_TIP_DOC_PRED !=''){
				$COND1 = " AND COD_TIP_DOC_PRED IN ($COD_TIP_DOC_PRED)";
			}else{
				$COND1 = "";
			}
			
			$sql="SELECT * FROM TBL_INMUEBLES_TIPO_DOC WHERE ID_ESTADO = '1' $COND1 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Lista_Tipo_Valorizacion(){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT * FROM TBL_TIPO_VALORIZACION WHERE ID_ESTADO = '1' ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Lista_Moneda(){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT * FROM TBL_MONEDA";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	/***********************************************************************************/
	
	
	function GENERAR_CODIGO_PADRON_PREDIOS_VALORIZACION(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(MAX(COD_VAL_PREDIO),0)+1) AS CODIGO FROM  TBL_PADRON_PREDIOS_VALORIZACION ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function INSERTAR_VALORIZACION_PADRON_PREDIOS($COD_VAL_PREDIO, $ID_PREDIO_SBN, $COD_ENTIDAD, $COD_TIP_DOC_PRED, $COD_TIP_VALORIZACION, $FECHA_VALORIZACION, $COD_MONEDA_VALORIZACION, $MONTO_VALOR_TERRENO, $MONTO_VALOR_CONTRUCCION, $MONTO_VALOR_OBRA, $MONTO_VALOR_TOTAL_INMUEBLE, $TIPO_CAMBIO_TOTAL_INMUEBLE, $MONTO_VALOR_TOTAL_INMUEBLE_SOL, $COD_PREDIO_DOC_TECN, $USUARIO, $ID_ESTADO){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_PADRON_PREDIOS_VALORIZACION  (COD_VAL_PREDIO, ID_PREDIO_SBN, COD_ENTIDAD, COD_TIP_DOC_PRED, COD_TIP_VALORIZACION, FECHA_VALORIZACION, COD_MONEDA_VALORIZACION, MONTO_VALOR_TERRENO, MONTO_VALOR_CONTRUCCION, MONTO_VALOR_OBRA, MONTO_VALOR_TOTAL_INMUEBLE, TIPO_CAMBIO_TOTAL_INMUEBLE, MONTO_VALOR_TOTAL_INMUEBLE_SOL, COD_PREDIO_DOC_TECN, FECHA_REGISTRO, INMUEBLES_USUARIO_CREACION, INMUEBLES_FECHA_CREACION, ID_ESTADO)
						 VALUES ('$COD_VAL_PREDIO', '$ID_PREDIO_SBN', '$COD_ENTIDAD', '$COD_TIP_DOC_PRED', '$COD_TIP_VALORIZACION', '$FECHA_VALORIZACION', '$COD_MONEDA_VALORIZACION', '$MONTO_VALOR_TERRENO', '$MONTO_VALOR_CONTRUCCION', '$MONTO_VALOR_OBRA', '$MONTO_VALOR_TOTAL_INMUEBLE', '$TIPO_CAMBIO_TOTAL_INMUEBLE', '$MONTO_VALOR_TOTAL_INMUEBLE_SOL', '$COD_PREDIO_DOC_TECN', GETDATE(), '$USUARIO', GETDATE(), '$ID_ESTADO') ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function ACTUALIZAR_VALORIZACION_PADRON_PREDIOS($COD_VAL_PREDIO, $ID_PREDIO_SBN, $COD_ENTIDAD, $COD_TIP_DOC_PRED, $COD_TIP_VALORIZACION, $FECHA_VALORIZACION, $COD_MONEDA_VALORIZACION, $MONTO_VALOR_TERRENO, $MONTO_VALOR_CONTRUCCION, $MONTO_VALOR_OBRA, $MONTO_VALOR_TOTAL_INMUEBLE, $TIPO_CAMBIO_TOTAL_INMUEBLE, $MONTO_VALOR_TOTAL_INMUEBLE_SOL, $COD_PREDIO_DOC_TECN, $USUARIO){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_PADRON_PREDIOS_VALORIZACION SET 
			ID_PREDIO_SBN = '$ID_PREDIO_SBN', COD_ENTIDAD = '$COD_ENTIDAD', COD_TIP_DOC_PRED = '$COD_TIP_DOC_PRED', COD_TIP_VALORIZACION = '$COD_TIP_VALORIZACION', FECHA_VALORIZACION = '$FECHA_VALORIZACION', COD_MONEDA_VALORIZACION = '$COD_MONEDA_VALORIZACION', MONTO_VALOR_TERRENO = '$MONTO_VALOR_TERRENO', MONTO_VALOR_CONTRUCCION = '$MONTO_VALOR_CONTRUCCION', MONTO_VALOR_OBRA = '$MONTO_VALOR_OBRA', MONTO_VALOR_TOTAL_INMUEBLE = '$MONTO_VALOR_TOTAL_INMUEBLE', TIPO_CAMBIO_TOTAL_INMUEBLE = '$TIPO_CAMBIO_TOTAL_INMUEBLE', MONTO_VALOR_TOTAL_INMUEBLE_SOL = '$MONTO_VALOR_TOTAL_INMUEBLE_SOL', COD_PREDIO_DOC_TECN = '$COD_PREDIO_DOC_TECN', INMUEBLES_USUARIO_MODIFICA = '$USUARIO', INMUEBLES_FECHA_MODIFICA = GETDATE()
			WHERE COD_VAL_PREDIO = '$COD_VAL_PREDIO' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function VER_DATOS_VALORIZACIONES_PREDIOS_X_COD_VAL_PREDIO($COD_VAL_PREDIO){
		if($this->oDBManager->conectar()==true){
			
			$sql="SELECT * FROM TBL_PADRON_PREDIOS_VALORIZACION WHERE COD_VAL_PREDIO = '$COD_VAL_PREDIO' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function ELIMINAR_VALORIZACIONES_PREDIOS_X_CODIGO($COD_VAL_PREDIO){
		if($this->oDBManager->conectar()==true){
			
			$sql="UPDATE TBL_PADRON_PREDIOS_VALORIZACION  SET ID_ESTADO = '2', INMUEBLES_FECHA_ELIMINA = GETDATE()  WHERE COD_VAL_PREDIO = '$COD_VAL_PREDIO' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	//**********************************************************
	
	function GENERAR_CODIGO_PREDIO_DOCUMENTOS_TECNICOS(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(Max(COD_PREDIO_DOC_TECN),0)+1) as CODIGO FROM TBL_PADRON_PREDIOS_DOC_TECN";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	//'5,6,7,8,10,11,12,13,14,15,16'
	function VER_DATOS_DOCUMENTOS_TECNICOS_X_CODIGO($COD_PREDIO_DOC_TECN){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT * FROM TBL_PADRON_PREDIOS_DOC_TECN WHERE COD_PREDIO_DOC_TECN = '$COD_PREDIO_DOC_TECN' ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	
	function VER_TIPO_DOCUMENTO_X_CODIGO($COD_TIP_DOC_PRED){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT * FROM TBL_INMUEBLES_TIPO_DOC WHERE ID_ESTADO = '1' AND COD_TIP_DOC_PRED = '$COD_TIP_DOC_PRED' ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	
	function INSERTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_PADRON_PREDIOS_DOC_TECN (COD_PREDIO_DOC_TECN, ID_PREDIO_SBN, COD_TIP_DOC_PRED, DESCRIP_ARCHIVO, NOM_ARCHIVO, PESO_ARCHIVO, NOM_ARCHIVO_CAD, PESO_ARCHIVO_CAD, FECHA_REGISTRO, NRO_PART_REG, FECHA_TASACION, COD_TIP_RESOL_DL, COD_SISTEMA_COORDENADA, INMUEBLES_USUARIO_CREACION, INMUEBLES_FECHA_CREACION, ID_ESTADO) 
						VALUES ('$COD_PREDIO_DOC_TECN', '$ID_PREDIO_SBN', '$COD_TIP_DOC_PRED', '$DESCRIP_ARCHIVO', '$NOM_ARCHIVO', '$PESO_ARCHIVO', '$NOM_ARCHIVO_CAD', '$PESO_ARCHIVO_CAD', GETDATE(), '$NRO_PART_REG', '$FECHA_TASACION', '$COD_TIP_RESOL_DL', '$COD_SISTEMA_COORDENADA', '$USUARIO', GETDATE(), '$ID_ESTADO' )";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function ADJUNTAR_ARCHIVO_DOC_TECN_GRAL($COD_PREDIO_DOC_TECN, $ID_PREDIO_SBN, $COD_TIP_DOC_PRED, $DESCRIP_ARCHIVO, $NOM_ARCHIVO, $PESO_ARCHIVO, $NOM_ARCHIVO_CAD, $PESO_ARCHIVO_CAD, $NRO_PART_REG, $FECHA_TASACION, $COD_TIP_RESOL_DL, $COD_SISTEMA_COORDENADA, $USUARIO, $ID_ESTADO){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_PADRON_PREDIOS_DOC_TECN SET 
			ID_PREDIO_SBN = '$ID_PREDIO_SBN',
			COD_TIP_DOC_PRED = '$COD_TIP_DOC_PRED',
			DESCRIP_ARCHIVO = '$DESCRIP_ARCHIVO',
			NOM_ARCHIVO = '$NOM_ARCHIVO',
			PESO_ARCHIVO = '$PESO_ARCHIVO',
			NOM_ARCHIVO_CAD = '$NOM_ARCHIVO_CAD',
			PESO_ARCHIVO_CAD = '$PESO_ARCHIVO_CAD',
			NRO_PART_REG = '$NRO_PART_REG',
			FECHA_TASACION = '$FECHA_TASACION',
			COD_TIP_RESOL_DL = '$COD_TIP_RESOL_DL',
			COD_SISTEMA_COORDENADA = '$COD_SISTEMA_COORDENADA',
			INMUEBLES_USUARIO_MODIFICA = '$USUARIO',
			INMUEBLES_FECHA_MODIFICA = GETDATE(),
			ID_ESTADO = '$ID_ESTADO'
			WHERE COD_PREDIO_DOC_TECN = '$COD_PREDIO_DOC_TECN' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	
	/******************************************************************/
	
	function Actualizar_Estado_TBL_CONTROL_PREDIO($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_CONTROL_PREDIO SET ID_ESTADO_REGISTROGRAFICO = 2 WHERE ID_PREDIO_SBN = '$ID_PREDIO_SBN' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	/**********************************************************************/
	
	
	
	function LISTAR_DOCUMENTOS_TECNICOS_X_ENTIDADES_X_COD_TIP_DOC_PRED($ID_PREDIO_SBN, $COD_TIP_DOC_PRED){
		if($this->oDBManager->conectar()==true){
			$consulta="
			SELECT 
			ROW_NUMBER() OVER (ORDER BY COD_PREDIO_DOC_TECN) AS ITEM,
			DOCTEC.*, TD.NOM_TIP_DOC, DL.DES_TIP_RESOL_DL
			FROM TBL_PADRON_PREDIOS_DOC_TECN DOCTEC
			LEFT JOIN TBL_INMUEBLES_TIPO_DOC TD ON (DOCTEC.COD_TIP_DOC_PRED = TD.COD_TIP_DOC_PRED)
			LEFT JOIN TBL_TIPO_RESOL_DL DL ON (DL.COD_TIP_RESOL_DL = DOCTEC.COD_TIP_RESOL_DL)
			WHERE DOCTEC.ID_ESTADO IN (0, 1, 3) AND DOCTEC.ID_PREDIO_SBN = '$ID_PREDIO_SBN' AND DOCTEC.COD_TIP_DOC_PRED = '$COD_TIP_DOC_PRED'
			 ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Eliminar_Documento_Tecnico($COD_PREDIO_DOC_TECN, $USUARIO){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_PADRON_PREDIOS_DOC_TECN SET ID_ESTADO = '2', INMUEBLES_USUARIO_ELIMINA = '$USUARIO', INMUEBLES_FECHA_ELIMINA = GETDATE() WHERE COD_PREDIO_DOC_TECN = '$COD_PREDIO_DOC_TECN' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function Lista_Dispositivo_Resolucion(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_TIPO_RESOL_DL WHERE ID_ESTADO = '1' order by DES_TIP_RESOL_DL ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Lista_Dispositivo_Resolucion_x_CODIGO($COD_TIP_RESOL_DL){
		if($this->oDBManager->conectar()==true){
			
			$sql="SELECT * FROM TBL_TIPO_RESOL_DL WHERE COD_TIP_RESOL_DL = '$COD_TIP_RESOL_DL' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	
	function ACTUALIZA_TBL_PADRON_PREDIOS_DOC_TECN($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_PADRON_PREDIOS_DOC_TECN SET ID_ESTADO ='1', INMUEBLES_FECHA_MODIFICA = GETDATE() WHERE ID_ESTADO IN (0,1,3) AND ID_PREDIO_SBN = '$ID_PREDIO_SBN' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function ACTUALIZA_TBL_PADRON_PREDIOS_VALORIZACION($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_PADRON_PREDIOS_VALORIZACION SET ID_ESTADO ='1', INMUEBLES_FECHA_MODIFICA = GETDATE() WHERE ID_ESTADO IN (0,1,3) AND ID_PREDIO_SBN = '$ID_PREDIO_SBN' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function ACTUALIZA_TBL_PADRON_PREDIOS_UNID_INMOBIL($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_PADRON_PREDIOS_UNID_INMOBIL SET ID_ESTADO ='1', INMUEBLES_FECHA_MODIFICA = GETDATE() WHERE ID_ESTADO IN (0,1,3) AND ID_PREDIO_SBN = '$ID_PREDIO_SBN' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	/***********************************************************************************************/
	
	function LISTAR_DATOS_PADRON_PREDIOS_UNID_INMOBIL($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT ROW_NUMBER() OVER (ORDER BY COD_UNID_INMOBIL) AS ITEM,* FROM TBL_PADRON_PREDIOS_UNID_INMOBIL WHERE ID_PREDIO_SBN = '$ID_PREDIO_SBN' AND ID_ESTADO IN (0,1,3) ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function GENERAR_CODIGO_PADRON_PREDIOS_UNID_INMOBIL(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT (ISNULL(Max(COD_UNID_INMOBIL),0)+1) as CODIGO FROM TBL_PADRON_PREDIOS_UNID_INMOBIL";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function INSERTAR_PADRON_PREDIOS_UNID_INMOBIL($COD_UNID_INMOBIL, $ID_PREDIO_SBN, $NOM_UNID_INMOBIL, $AREA_UNID_INMOBIL, $VALOR_AUTOVALUO, $OBS_UNID_INMOBIL, $USUARIO){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_PADRON_PREDIOS_UNID_INMOBIL (COD_UNID_INMOBIL, ID_PREDIO_SBN, NOM_UNID_INMOBIL, AREA_UNID_INMOBIL, VALOR_AUTOVALUO, OBS_UNID_INMOBIL, INMUEBLES_USUARIO_CREACION, INMUEBLES_FECHA_CREACION, FECHA_REGISTRO, ID_ESTADO) 
						VALUES ('$COD_UNID_INMOBIL', '$ID_PREDIO_SBN', '$NOM_UNID_INMOBIL', '$AREA_UNID_INMOBIL', '$VALOR_AUTOVALUO', '$OBS_UNID_INMOBIL', '$USUARIO', GETDATE(), GETDATE(), '3')";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function ACTUALIZA_PADRON_PREDIOS_UNID_INMOBIL($COD_UNID_INMOBIL, $ID_PREDIO_SBN, $NOM_UNID_INMOBIL, $AREA_UNID_INMOBIL, $VALOR_AUTOVALUO, $OBS_UNID_INMOBIL, $USUARIO){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_PADRON_PREDIOS_UNID_INMOBIL 
			SET ID_PREDIO_SBN = '$ID_PREDIO_SBN', NOM_UNID_INMOBIL='$NOM_UNID_INMOBIL', AREA_UNID_INMOBIL= '$AREA_UNID_INMOBIL', VALOR_AUTOVALUO = '$VALOR_AUTOVALUO', OBS_UNID_INMOBIL = '$OBS_UNID_INMOBIL', INMUEBLES_USUARIO_MODIFICA = '$USUARIO', INMUEBLES_FECHA_MODIFICA = GETDATE()  
			WHERE COD_UNID_INMOBIL = '$COD_UNID_INMOBIL' ";
			//echo $consulta;
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	
	function ELIMINAR_UNID_INMOBIL($COD_UNID_INMOBIL){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_PADRON_PREDIOS_UNID_INMOBIL  SET  ID_ESTADO = '2', INMUEBLES_FECHA_ELIMINA = GETDATE()  WHERE COD_UNID_INMOBIL = '$COD_UNID_INMOBIL' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function VER_DATOS_PADRON_PREDIOS_UNID_INMOBIL($COD_UNID_INMOBIL){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT * FROM TBL_PADRON_PREDIOS_UNID_INMOBIL WHERE COD_UNID_INMOBIL = '$COD_UNID_INMOBIL' ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Dar_Baja_Predio_Local($BAJ_COD_USER, $BAJ_COD_ENTIDAD, $BAJ_ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
						
			$sql="
UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '5', DAR_BAJA = 'X', SBN_USUARIO_MODIFICA = '$BAJ_COD_USER', SBN_FECHA_MODIFICA = GETDATE()  
WHERE COD_ENTIDAD = '$BAJ_COD_ENTIDAD' AND ID_PREDIO_SBN = '$BAJ_ID_PREDIO_SBN'
				";			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Enviar_Predio_SBN($ID_PREDIO_SBN, $USUARIO){
		if($this->oDBManager->conectar()==true){
						
			$sql="
UPDATE TBL_PADRON_PREDIOS SET 
ENVIO_REG_A_SBN = 'X',
FECHA_ENVIO_REG_A_SBN = GETDATE(),
TIPO_VALIDACION = NULL,
FECHA_TIPO_VALIDACION = NULL,
SBN_USUARIO_MODIFICA = '$USUARIO',
SBN_FECHA_MODIFICA = GETDATE()
WHERE ID_PREDIO_SBN = '$ID_PREDIO_SBN'
";			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Obtener_ID_CONTROL_PREDIO_Predio_Hitoria($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
						
			$sql="SELECT ID_CONTROL_PREDIO FROM TBL_CONTROL_PREDIO where ID_PREDIO_SBN = '$ID_PREDIO_SBN' ";			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function TOT_REG_TBL_CONTROL_PREDIO_OBS($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
						
			$sql="
			SELECT COUNT(ID_CONTROL_PREDIO_OBS) AS TOT_REG FROM TBL_CONTROL_PREDIO_OBS 
			WHERE ID_CONTROL_PREDIO IN (SELECT ID_CONTROL_PREDIO FROM TBL_CONTROL_PREDIO where ID_PREDIO_SBN = '$ID_PREDIO_SBN')
			";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function REGISTRAR_TBL_CONTROL_PREDIO_OBS($OBSERVACION, $ID_CONTROL_PREDIO, $TIPO_ESTADO, $ID_USUARIO_EXTERNO, $TIPO_MODULO){
		if($this->oDBManager->conectar()==true){
						
			$sql="
			INSERT INTO TBL_CONTROL_PREDIO_OBS(OBSERVACION, FECHA, ID_USUARIO, ID_CONTROL_PREDIO, TIPO_ESTADO, ID_USUARIO_EXTERNO, TIPO_MODULO)
			VALUES('$OBSERVACION', GETDATE(), NULL, '$ID_CONTROL_PREDIO', '$TIPO_ESTADO', '$ID_USUARIO_EXTERNO', '$TIPO_MODULO')
			";			
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	
	function Lista_Predios_Locales_Detallado_x_entidad($COD_ENTIDAD){
		if($this->oDBManager->conectar()==true){
						
			$consulta="
SELECT 
ROW_NUMBER() OVER (ORDER BY P.ID_PREDIO_SBN) AS ROW_NUMBER_ID,

--DATOS GENERALES

P.ID_PREDIO_SBN,
CODIGO_INTERNO = REPLACE(ISNULL(P.CODIGO_INTERNO,''),'null',''),
P.COD_ENTIDAD,
P.DENOMINACION_PREDIO,
DEPA.nmbubigeo AS DEPARTAMENTO,
PROV.nmbubigeo AS PROVINCIA,
DIST.nmbubigeo AS DISTRITO,
Z.NOM_ZONA,
TXT_TVIA = REPLACE(ISNULL(V.TXT_TVIA,''),'null',''),
P.NOMBRE_VIA,
P.NRO_PRED,
P.MZA_PRED,
P.LTE_PRED,
P.UBICADO_PISO,
TIP_UBICAC = REPLACE(ISNULL(P.DET_PRED,''),'null',''),
NUM_UBIC = REPLACE(ISNULL(P.DET_PRED_NRO,''),'null',''),
H.TXT_THABILITACION,
NOM_THABILITACION = REPLACE(ISNULL(P.NOM_THABILITACION,''),'null',''),
NOM_SECTOR = REPLACE(ISNULL(P.NOM_SECTOR,''),'null',''),
DESC_TIP_PROPIEDAD = REPLACE(ISNULL(TP.DESC_TIP_PROPIEDAD,''),'null',''),

--DATOS REGISTRALES

CUS = REPLACE(ISNULL(P.CUS,''),'null',''),
RSINABIP = REPLACE(ISNULL(P.RSINABIP,''),'null',''),
NOM_PROP_REGISTRAL = REPLACE(ISNULL(P.NOM_PROP_REGISTRAL,''),'null',''),
NOM_OFICINA_REGISTRAL = REPLACE(ISNULL(OREG.NOM_OFICINA_REGISTRAL,''),'null',''),
PARTIDA_ELECTRONICA = REPLACE(ISNULL(P.PARTIDA_ELECTRONICA,''),'null',''),
CODIGO_PREDIO = REPLACE(ISNULL(P.CODIGO_PREDIO,''),'null',''),
P.AREA_REGISTRAL,
ABREV_TIP_UNID_MED = REPLACE(ISNULL(UM.ABREV_TIP_UNID_MED,''),'null',''),
TOMO = REPLACE(ISNULL(P.TOMO,''),'null',''),
ASIENTO = REPLACE(ISNULL(P.ASIENTO,''),'null',''),
FOJAS = REPLACE(ISNULL(P.FOJAS,''),'null',''),
FICHA = REPLACE(ISNULL(P.FICHA,''),'null',''),
ANOTACION_PREVENTIVA = REPLACE(ISNULL(P.ANOTACION_PREVENTIVA,''),'null',''),

--DATOS TECNICOS

ZONIFICACION = REPLACE(ISNULL(P.ZONIFICACION,''),'null',''),
NOM_TIP_TERRENO = REPLACE(ISNULL(TT.NOM_TIP_TERRENO,''),'null',''),
P.PERIMETRO,
P.AREA_TERRENO,
P.AREA_CONSTRUIDA,
TXT_CONSERVACION = REPLACE(ISNULL(EC.TXT_CONSERVACION,''),'null',''),
NUMERO_PISOS = REPLACE(ISNULL(P.NUMERO_PISOS,''),'null',''),
TXT_ESANEAMIENTO = REPLACE(ISNULL(ES.TXT_ESANEAMIENTO,''),'null',''),
CODIGO_DETALLE_ESANEAMIENTO = REPLACE(ISNULL(P.CODIGO_DETALLE_ESANEAMIENTO,''),'null',''),
DSC_DETALLE = REPLACE(ISNULL(TES.DSC_DETALLE,''),'null',''),

NOMBRE_USO_GENERICO = REPLACE(ISNULL(UP.NOMBRE_USO_GENERICO,''),'null',''),
OTROS_USO_PREDIO = REPLACE(ISNULL(P.OTROS_USO_PREDIO,''),'null','')

FROM TBL_PADRON_PREDIOS P
LEFT JOIN TBL_UBIGEO DEPA ON (DEPA.Flag = 'D' AND DEPA.coddep = P.COD_DEPA)
LEFT JOIN TBL_UBIGEO PROV ON (PROV.Flag = 'P' AND PROV.coddep = P.COD_DEPA AND PROV.codprov = P.COD_PROV )
LEFT JOIN TBL_UBIGEO DIST ON (DIST.Flag = 'T' AND DIST.coddep = P.COD_DEPA AND DIST.codprov = P.COD_PROV AND DIST.coddist = P.COD_DIST )
LEFT JOIN TBL_TIPO_ZONA Z ON (P.COD_ZONA = Z.COD_ZONA)
LEFT JOIN TBL_TIPO_VIA V ON (P.COD_TVIA = V.COD_TVIA)
LEFT JOIN TBL_TIPO_HABILITACION H ON (H.COD_THABILITACION = P.COD_THABILITACION)
LEFT JOIN TBL_TIPO_PROPIEDAD TP ON (TP.COD_TIP_PROPIEDAD = P.COD_TIP_PROPIEDAD)
LEFT JOIN TBL_PADRON_ZONA_REGISTRAL_OFICINA OREG ON (OREG.COD_OFIC_REGISTRAL = P.COD_OFIC_REGISTRAL)
LEFT JOIN TBL_TIPO_UNID_MED UM ON (UM.COD_TIP_UNID_MED = P.COD_TIP_UNID_MED)
LEFT JOIN TBL_TIPO_TERRENO TT ON (TT.COD_TIP_TERRENO =P.COD_TIP_TERRENO)
LEFT JOIN TBL_ESTADO_CONSERVACION EC ON (EC.COD_ECONSERVACION = P.COD_ECONSERVACION)
LEFT JOIN TBL_ESTADO_SANEAMIENTO ES ON (ES.COD_ESANEAMIENTO = P.COD_ESANEAMIENTO)
LEFT JOIN TBL_MAESTRO_SINABIP TES ON (TES.CODIGO_MAESTRO = '005' AND TES.detalle_filtro = P.COD_ESANEAMIENTO AND  TES.codigo_detalle = P.CODIGO_DETALLE_ESANEAMIENTO )
LEFT JOIN TBL_USO_PREDIO UP ON (UP.CODIGO_USO_GENERICO = P.CODIGO_USO_GENERICO)
WHERE P.ID_ESTADO = 1  AND P.ID_ESTADO_OCULTAR <> 2
AND P.COD_ENTIDAD = '$COD_ENTIDAD'
				";		

			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	/*************************************************************************************************/
	/*************************************************************************************************/
	/*************************************************************************************************/
	
	
	function Ver_Historial_Tipo_Validacion_x_ID_PREDIO_SBN($ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT
CTRLP.ID_PREDIO_SBN,
OBS.ID_CONTROL_PREDIO,
OBS.ID_CONTROL_PREDIO_OBS,
CONVERT(VARCHAR(10),OBS.FECHA,103) AS FECHA,
SBN_COD_USUARIO = U_SBN.ID_USUARIO,
SBN_NOM_USUARIO = U_SBN.NOM_USUARIO,
SBN_NOMBRES_PERS = (P_SBN.NOMBRES_PERS+', '+P_SBN.APE_PATERNO_PERS+' '+P_SBN.APE_PATERNO_PERS),
TIPO_VALIDACION	 = CASE WHEN OBS.TIPO_ESTADO = 'O' THEN 'OBSERVADO' 
						WHEN OBS.TIPO_ESTADO = 'E' THEN 'ENVIADO SIN PLANO' 
						WHEN OBS.TIPO_ESTADO = 'P' THEN 'ENVIADO CON PLANO' 
						WHEN OBS.TIPO_ESTADO = 'S' THEN 'SUBSANADO' 
						WHEN OBS.TIPO_ESTADO = 'V' THEN 'VALIDADO' 
						WHEN OBS.TIPO_ESTADO = 'N' THEN 'NUEVO' 
						ELSE '---'						
					END,
OBS.OBSERVACION,
ENT_COD_USUARIO = U_ENT.COD_USUARIO,
ENT_COD_USUARIO = U_ENT.NOM_USUARIO,
ENT_NOM_ENTIDAD = ENT.NOM_ENTIDAD,
OBS.TIPO_MODULO
FROM TBL_CONTROL_PREDIO_OBS OBS
LEFT JOIN TBL_CONTROL_PREDIO CTRLP ON (OBS.ID_CONTROL_PREDIO = CTRLP.ID_CONTROL_PREDIO)
LEFT JOIN TBL_INTRANET_USUARIO U_SBN ON (U_SBN.ID_USUARIO = OBS.ID_USUARIO)
LEFT JOIN TBL_SBN_PERSONAL P_SBN ON (U_SBN.ID_PERSONAL = P_SBN.COD_PERSONAL)
LEFT JOIN TBL_MUEBLES_USUARIO U_ENT ON (U_ENT.COD_USUARIO = OBS.ID_USUARIO_EXTERNO)
LEFT JOIN TBL_PADRON_ENTIDAD ENT ON (ENT.COD_ENTIDAD = U_ENT.COD_ENTIDAD)
WHERE CTRLP.ID_PREDIO_SBN ='$ID_PREDIO_SBN' 
AND CTRLP.ID_ESTADO = '1' 
ORDER BY OBS.FECHA DESC
";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}



	function Eliminacion_Baja_Predios_Validacion( $COD_ENTIDAD, $ID_PREDIO_SBN )
	{

		if($this->oDBManager->conectar()==true){
			$sql="SELECT COUNT(COD_UE_BIEN_PATRI)[CANTIDAD] FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL WHERE COD_ENTIDAD = $COD_ENTIDAD AND ID_PREDIO_SBN = $ID_PREDIO_SBN AND ID_ESTADO = '1'";

			//echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function Eliminar_Baja_Predio( $COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD )
	{

		if($this->oDBManager->conectar()==true){
			if( $COD_TIP_PROPIEDAD == 1 ){
				$sql="DELETE TBL_PADRON_PREDIOS WHERE ID_PREDIO_SBN = $ID_PREDIO_SBN ";
			}else{
				$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '5', DAR_BAJA = 'X' WHERE ID_PREDIO_SBN = $ID_PREDIO_SBN";
			}

			//echo $sql;
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;

		}

	}

	// INICIO JMCR
	function Contar_muebles_predio($COD_ENTIDAD, $ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT COUNT(COD_UE_BIEN_PATRI)[CANTIDAD_P] FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL WHERE COD_ENTIDAD = $COD_ENTIDAD AND ID_PREDIO_SBN = $ID_PREDIO_SBN AND ID_ESTADO = '1'";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function Contar_areas_predio($COD_ENTIDAD, $ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT COUNT(COD_UE_AREA)[CANTIDAD_A] FROM TBL_MUEBLES_UE_AREA WHERE ID_PREDIO_SBN = $ID_PREDIO_SBN AND ID_ESTADO = '1'";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function Contar_personal_predio($COD_ENTIDAD, $ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT COUNT(COD_UE_PERS)[CANTIDAD_S] FROM TBL_MUEBLES_UE_PERSONAL WHERE COD_ENTIDAD = $COD_ENTIDAD AND ID_PREDIO_SBN = $ID_PREDIO_SBN AND ID_ESTADO = '1'";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function Devolver_nombre_predio($COD_ENTIDAD, $ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT DENOMINACION_PREDIO FROM TBL_PADRON_PREDIOS P WHERE COD_ENTIDAD = $COD_ENTIDAD AND ID_PREDIO_SBN = $ID_PREDIO_SBN";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function Listar_predios_migrar($COD_ENTIDAD, $ID_PREDIO_SBN){
		if($this->oDBManager->conectar()==true){
			$sql="select ID_PREDIO_SBN, DENOMINACION_PREDIO from TBL_PADRON_PREDIOS P WHERE COD_ENTIDAD = $COD_ENTIDAD AND ID_PREDIO_SBN <> $ID_PREDIO_SBN AND ID_ESTADO IN ('1')";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function Eliminar_Predio($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD){
		if($this->oDBManager->conectar()==true){
			if($COD_TIP_PROPIEDAD == 1){
				$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '2' WHERE ID_PREDIO_SBN = $ID_PREDIO_SBN AND COD_ENTIDAD = $COD_ENTIDAD";
			}elseif ($COD_TIP_PROPIEDAD == 2) {
				$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '3' WHERE ID_PREDIO_SBN = $ID_PREDIO_SBN AND COD_ENTIDAD = $COD_ENTIDAD";
			}elseif ($COD_TIP_PROPIEDAD == 3) {
				$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '4' WHERE ID_PREDIO_SBN = $ID_PREDIO_SBN AND COD_ENTIDAD = $COD_ENTIDAD";
			}elseif ($COD_TIP_PROPIEDAD == 4) {
				$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '4' WHERE ID_PREDIO_SBN = $ID_PREDIO_SBN AND COD_ENTIDAD = $COD_ENTIDAD";
			}else{
				$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '2' WHERE ID_PREDIO_SBN = $ID_PREDIO_SBN AND COD_ENTIDAD = $COD_ENTIDAD";
			}
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function Habilitar_Predio($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '1' WHERE ID_PREDIO_SBN = $ID_PREDIO_SBN AND COD_ENTIDAD = $COD_ENTIDAD AND COD_TIP_PROPIEDAD = $COD_TIP_PROPIEDAD";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function DarDeBaja_Predios($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '5', DAR_BAJA = 'X' WHERE ID_PREDIO_SBN = $ID_PREDIO_SBN AND COD_ENTIDAD = $COD_ENTIDAD AND COD_TIP_PROPIEDAD = $COD_TIP_PROPIEDAD";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function Migrar_predio_personas($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD, $cbo_predio){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_MUEBLES_UE_PERSONAL SET ID_PREDIO_SBN = $cbo_predio WHERE COD_ENTIDAD = $COD_ENTIDAD AND ID_PREDIO_SBN = $ID_PREDIO_SBN AND ID_ESTADO = '1'";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function Migrar_predio_areas($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD, $cbo_predio){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_MUEBLES_UE_AREA SET ID_PREDIO_SBN = $cbo_predio WHERE ID_PREDIO_SBN = $ID_PREDIO_SBN AND ID_ESTADO = '1'";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}

	function Migrar_predio_muebles($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_TIP_PROPIEDAD, $cbo_predio){
		if($this->oDBManager->conectar()==true){
			$sql="UPDATE TBL_MUEBLES_UE_BIEN_PATRIMONIAL SET ID_PREDIO_SBN = $cbo_predio WHERE COD_ENTIDAD = $COD_ENTIDAD AND ID_PREDIO_SBN = $ID_PREDIO_SBN AND ID_ESTADO = '1'";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	// FIN JMCR

	function Descargar_Decreto_Urgencia_Excel_V1($COD_ENTIDAD){
		
		if($this->oDBManager->conectar()==true) {
			$SQL = "
				SELECT 
				ITEM	=	ROW_NUMBER() OVER (ORDER BY DU.ID_PREDIO_SBN), 
				DENOMINACION_INMUEBLE	=	DU.[DENOMINACION_INMUEBLE], 
				DEPARTAMENTO	=	DEPA.nmbubigeo,
				PROVINCIA	=	PROV.nmbubigeo,
				DISTRITO	=	DIST.nmbubigeo,
				TIPO_VIA	=	TV.DESC_VIA,
				NOMBRE_VIA	=	DU.[NOMBRE_VIA], 
				NRO_PRED	=	DU.[NRO_PRED], 
				MZA_PRED	=	DU.[MZA_PRED], 
				LTE_PRED	=	DU.[LTE_PRED], 
				URBANIZACION	=	DU.[URBANIZACION],
				SECTOR_NACIONAL	=	SE.SECTOR_NACIONAL,
				USO_INMUEBLE	=	UPG.DESCRIPCION_USO,
				AREA_TOTAL	=	DU.[AREA_TOTAL], 
				AREA_UTIL	=	DU.[AREA_UTIL], 
				PERS_CAS	=	DU.[PERS_CAS], 
				PERS_CAP	=	DU.[PERS_CAP], 
				PERS_FAG	=	PERS_FAG, 
				PERS_TERCEROS	=	DU.[PERS_TERCEROS],
				PERS_PRACTICANTES	=	DU.[PERS_PRACTICANTES], 
				PERS_VIGI	=	DU.[PERS_TERC_VIGI], 
				PERS_LIMP	=	DU.[PERS_TERC_LIMP], 
				PERS_JARD	=	PERS_TERC_JARD, 
				PERS_MANT	=	PERS_TERC_MANT,
				CONSERVACION	=	EC.TXT_CONSERVACION, 
				CASE WHEN DU.[TIPO_INMUEBLE] = 1 THEN 'SI' WHEN DU.[TIPO_INMUEBLE] = 2 THEN 'NO' END INMUEBLE_ESTATAL,
				CASE WHEN DU.[ES_ARRENDADO] = 1 THEN 'SI' WHEN DU.[ES_ARRENDADO] = 2 THEN 'NO' END INMUEBLE_ARRENDADO,
				RENTA_MENSUAL	=	[RENTA_MENSUAL_SOLES],
				PROPIEDAD	=	TP.DESC_TIP_PROPIEDAD 
				FROM [TBL_DECRETO_URGENCIA_V2] DU
				LEFT JOIN TBL_PADRON_ENTIDAD PE ON DU.COD_ENTIDAD = PE.COD_ENTIDAD
				LEFT JOIN TBL_ENTIDADES_ACCESO_DU SE ON SE.COD_ENTIDAD = PE.COD_ENTIDAD
				LEFT JOIN TBL_UBIGEO DEPA ON (DEPA.Flag = 'D' AND DEPA.coddep = DU.COD_DEPA)
				LEFT JOIN TBL_UBIGEO PROV ON (PROV.Flag = 'P' AND PROV.coddep = DU.COD_DEPA AND PROV.codprov = DU.COD_PROV )
				LEFT JOIN TBL_UBIGEO DIST ON (DIST.Flag = 'T' AND DIST.coddep = DU.COD_DEPA AND DIST.codprov = DU.COD_PROV AND DIST.coddist = DU.COD_DIST )
				LEFT JOIN TBL_TIPO_VIA TV ON TV.COD_TVIA=DU.COD_TVIA
				LEFT JOIN TBL_USO_PREDIO_GRAL UPG ON UPG.ID_USO_PREDIO=DU.ID_USO_PREDIO
				LEFT JOIN TBL_ESTADO_CONSERVACION EC ON EC.COD_ECONSERVACION = DU.COD_ECONSERVACION 
				LEFT JOIN TBL_TIPO_PROPIEDAD TP ON TP.COD_TIP_PROPIEDAD=DU.COD_TIP_PROPIEDAD
				WHERE DU. COD_ENTIDAD = $COD_ENTIDAD 
			";
			//echo $SQL;
			$result= $this->oDBManager->execute($SQL); 
			return $result;	 
					
		}
	}
	


	
}
?>