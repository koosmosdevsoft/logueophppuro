<?
ini_set('max_execution_time', 99999); //600 seconds = 10 minutes
//ini_set('max_execution_time', 600); //600 seconds = 10 minutes
//error_reporting(E_ALL ^ E_NOTICE);
$operacion	=	$_GET['operacion'];
require_once('../Controller/C_simi_m_importar_excel_muebles.php');
require_once('../../utils/funciones/funciones.php');
require_once '../../utils/Classes/PHPExcel.php';

$oSimi_Importar_Excel_Muebles	=	new Simi_Importar_Excel_Muebles;


if($operacion=='Guardar_Datos_Responsable'){
		
	$USUARIO 						=	$_REQUEST['TXH_SIMI_COD_USUARIO'];		
	$COD_ENTIDAD 					= 	$_REQUEST['TXH_COD_ENTIDAD_RESP'];	
	$TXH_COD_IMPORT_EXCEL_RESP		= 	$_REQUEST['TXH_COD_IMPORT_EXCEL_RESP'];
		
	$DNI 							= 	$_REQUEST['TXT_NRO_DNI_RESP'];
	$NOMB_RESP 						= 	convertir_a_utf8(strtoupper($_REQUEST['TXT_NOMB_RESP']));
	$APEPAT_RESP 					= 	convertir_a_utf8(strtoupper($_REQUEST['TXT_APEPAT_RESP']));
	$APEMAT_RESP 					= 	convertir_a_utf8(strtoupper($_REQUEST['TXT_APEMAT_RESP']));
	
	$ID_PREDIO_SBN 					= 	convertir_a_utf8(strtoupper($_REQUEST['cbo_local_Responsable']));	
	$NOMB_AREA 						= 	convertir_a_utf8(strtoupper($_REQUEST['TXT_NOM_AREA_RESP']));	
	$NOMB_OFICINA 					= 	convertir_a_utf8(strtoupper($_REQUEST['TXT_NOM_OFICINA_RESP']));

	
	if($TXH_COD_IMPORT_EXCEL_RESP == ''){
			
		$RESULT_1				=	$oSimi_Importar_Excel_Muebles->Simi_Generar_Codigo_Responsable_Control_Patrimonial(); 
		$COD_IMPORT_EXCEL_RESP 	= 	odbc_result($RESULT_1,"CODIGO"); 

		$RESULTADO				=	$oSimi_Importar_Excel_Muebles->Insertar_Datos_Responsable_Control_Patrimonial($COD_IMPORT_EXCEL_RESP, $COD_ENTIDAD, $DNI, $NOMB_RESP, $APEPAT_RESP, $APEMAT_RESP, $ID_PREDIO_SBN, $NOMB_AREA, $NOMB_OFICINA, $USUARIO);

		$RESULTADO				=	$oSimi_Importar_Excel_Muebles->Insertar_otros_datos_responsable_patrimonio($COD_ENTIDAD, $NOMB_RESP, $APEPAT_RESP, $APEMAT_RESP, $DNI, $USUARIO, $ID_PREDIO_SBN, $NOMB_AREA);

		// $RESULTADO				=	$oSimi_Importar_Excel_Muebles->Insertar_Datos_Personal($COD_ENTIDAD, $NOMB_RESP, $APEPAT_RESP, $APEMAT_RESP, $DNI, $USUARIO);

		// $RESULTADO				=	$oSimi_Importar_Excel_Muebles->Insertar_Datos_Area($ID_PREDIO_SBN, $NOMB_AREA, $USUARIO);		
	}else{
		
		$COD_IMPORT_EXCEL_RESP	= $TXH_COD_IMPORT_EXCEL_RESP;		
		$RESULTADO				= $oSimi_Importar_Excel_Muebles->Actualiza_Datos_Responsable_Control_Patrimonial($COD_IMPORT_EXCEL_RESP, $COD_ENTIDAD, $DNI, $NOMB_RESP, $APEPAT_RESP, $APEMAT_RESP, $ID_PREDIO_SBN, $NOMB_AREA, $NOMB_OFICINA, $USUARIO);
		
	}	
	
	echo $valor = ($RESULTADO) ? '1'.'[*]'.$COD_IMPORT_EXCEL_RESP : 0;
	
}else if($operacion=='Enviar_Datos_Responsable_a_SBN'){
				
	$COD_IMPORT_EXCEL_RESP	=	$_REQUEST['sCodRespMuebles'];		
	$RESULTADO				=	$oSimi_Importar_Excel_Muebles->Enviar_Datos_Responsable_a_SBN($COD_IMPORT_EXCEL_RESP);	
	echo $valor = ($RESULTADO) ? 1 : 0;

}else if($operacion=='Adjuntar_Excel_Muebles'){
	
	$COD_IMPORT_EXCEL_RESP	=	$_REQUEST['TXH_COD_IMPORT_EXCEL_RESP'];
	$COD_USUARIO			=	$_REQUEST['TXH_SIMI_COD_USUARIO'];
	$txh_COD_IMPORT_EXCEL_FILE	=	$_REQUEST['txh_COD_IMPORT_EXCEL_FILE'];
	$COD_ENTIDAD 			=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];
	$ID_PREDIO_SBN			=	$_REQUEST['txh_cod_local_file'];
	$COD_INVENTARIO 		=	$_REQUEST['txh_migra_cod_inventario'];
	
	
	if($txh_COD_IMPORT_EXCEL_FILE == ''){
		
		$RESULT		=	$oSimi_Importar_Excel_Muebles->Simi_Generar_Codigo_Importar_Datos_Excel_Muebles();		
		$COD_IMPORT_EXCEL_FILE	= 	odbc_result($RESULT,"CODIGO");
			
	}else{
		
		$COD_IMPORT_EXCEL_FILE = $txh_COD_IMPORT_EXCEL_FILE;
		
	}
	
	$UPLOADDIR='../../../repositorio_muebles/inventario';
	
	$ARCHIVO					= $_FILES['upload_excel_file']['tmp_name'];
	$ARCHIVO_NOMBRE				= basename($_FILES['upload_excel_file']['name']);
	$ARCHIVO_PESO				= $_FILES['upload_excel_file']['size'];
	$ARCHIVO_TIPO				= $_FILES['upload_excel_file']['type'];
	$ARCHIVO_EXTENSION			= extension_archivo($_FILES['upload_excel_file']['name']);
	
	$NUEVA_CADENA				= substr( md5(microtime()), 1, 4);
	$FECHA_ARCHIVO				= date('Y').''.date('m').''.date('d');
	
	$ARCHIVO_NOMBRE_GENERADO	= 'Inv_'.$COD_INVENTARIO.'_ID_'.$COD_ENTIDAD.'_L_'.$ID_PREDIO_SBN.'_FILE_'.$COD_IMPORT_EXCEL_FILE.'_'.$FECHA_ARCHIVO.'_'.$NUEVA_CADENA.'.'.$ARCHIVO_EXTENSION;
	$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;
	
	if (move_uploaded_file($ARCHIVO, $UPLOADFILE)) {
		
		if($txh_COD_IMPORT_EXCEL_FILE == ''){
			$RESULT2	=	$oSimi_Importar_Excel_Muebles->Adjuntar_Excel_Importar_Datos_Excel_Muebles($COD_IMPORT_EXCEL_FILE, $COD_IMPORT_EXCEL_RESP, $COD_ENTIDAD, $ARCHIVO_NOMBRE_GENERADO, $ARCHIVO_PESO, $COD_USUARIO, $COD_INVENTARIO, $ID_PREDIO_SBN);	
		}else{
			$RESULT2	=	$oSimi_Importar_Excel_Muebles->Actualiza_Excel_Importar_Datos_Excel_Muebles($COD_IMPORT_EXCEL_FILE, $ARCHIVO_NOMBRE_GENERADO, $ARCHIVO_PESO, $COD_USUARIO, $COD_INVENTARIO, $ID_PREDIO_SBN);
		}
		
		$valor = ($RESULT2) ? '1'.'[*]'.$COD_IMPORT_EXCEL_FILE.'[*]'.$ARCHIVO_NOMBRE_GENERADO : 0 ;
		
	}else{
		$valor = 0;	
	}
	
	//=============== Directorio  =================================================
	
	echo $valor;
	

}else if($operacion=='Agregar_Archivo_a_Lista'){
	
	$COD_IMPORT_EXCEL_FILE	=	$_REQUEST['txh_COD_IMPORT_EXCEL_FILE'];
	$ID_PREDIO_SBN	=	$_REQUEST['txh_cod_local_file'];
	//$COD_INVENTARIO	=	$_REQUEST['txh_migra_cod_inventario'];
	
	$RESULT2	=	$oSimi_Importar_Excel_Muebles->Agregar_Excel_Importar_Datos_Excel_Muebles_a_Lista($COD_IMPORT_EXCEL_FILE, $ID_PREDIO_SBN);
	
	
	/*********************************************** INICIO - CONVERTIR EXCEL A CSV ***********************************/
	
	/*
	ini_set('memory_limit', '5256M');
	
	$RESULT2		=	$oSimi_Importar_Excel_Muebles->Ver_Datos_Excel_Muebles_Adjunto_Activo($COD_IMPORT_EXCEL_FILE);
	$NOM_ARCHIVO	= 	odbc_result($RESULT2,"NOM_ARCHIVO");
	$UPLOADFILE='../../../repositorio_muebles/inventario/'.$NOM_ARCHIVO;
	
	require_once '../../utils/Classes/PHPExcel/IOFactory.php';

	$excel = PHPExcel_IOFactory::load($UPLOADFILE);
	$writer = PHPExcel_IOFactory::createWriter($excel, 'CSV');
	$writer->setDelimiter("|");
	$writer->setEnclosure("");
	$writer->save("../../../repositorio_muebles/inventario/".$NOM_ARCHIVO.".csv");
	*/
	
	/*********************************************** FIN - CONVERTIR EXCEL A CSV ***********************************/	
	
	echo $valor = ($RESULT2) ? 1 : 0 ;


}else if($operacion=='Ejecuta_Cargar_TXT'){

	//ini_set('memory_limit', '-1');
	//set_time_limit(6000);
	//ini_set('memory_limit', '1000M');
	
	/*
	ini_set('upload_max_filesize', '1000M');
	ini_set('post_max_size', '1000M');
	ini_set('max_execution_time', 30000);
	*/
	
	$Fecha_Hora_Ini			= 	date("Y-m-d H:i:s");
	
	$COD_IMPORT_EXCEL_FILE	=	$_REQUEST['sCOD_IMPORT_EXCEL_FILE_XLS'];
	$COD_ENTIDAD			=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];
	$USUARIO_PROCESA		=	$_REQUEST['TXH_SIMI_COD_USUARIO'];
	$ID_PREDIO_SBN			=	$_REQUEST['sID_PREDIO_SBN_LOCAL'];
	
	$RESULT_FILE		=	$oSimi_Importar_Excel_Muebles->Ver_Datos_Excel_Muebles_Adjunto_Activo($COD_IMPORT_EXCEL_FILE);
	$NOM_ARCHIVO		= 	ltrim(rtrim(odbc_result($RESULT_FILE,"NOM_ARCHIVO")));
	
	$RESULT_TXT			=	$oSimi_Importar_Excel_Muebles->Cargar_Data_TXT($COD_ENTIDAD, $ID_PREDIO_SBN, $COD_IMPORT_EXCEL_FILE, $USUARIO_PROCESA, $NOM_ARCHIVO);
	$RESULTADO					= 	odbc_result($RESULT_TXT,"RESULTADO");
	//$COD_IMPORT_EXCEL_FILE_RS	= 	odbc_result($RESULT_TXT,"COD_IMPORT_EXCEL_FILE");
	
	if($RESULTADO == 'FALLA'){
		
		echo "<div style='color:#F00'><br><========================================================================>";
		echo "<br><===================== ERROR AL CARGAR TXT ===============================>";
		echo "<========================================================================></DIV>";
		echo "<br>";		
		echo "<br><B><div style='color:#F00'>NO ESTA UTILIZANDO LA VERSIÓN 7 DEL CUADRO EXCEL </div></B>";
		echo "<br><B><div style='color:#069'>DESCARGAR ==> <A href='../../documentos_web/comunicados_sinabip/inventario/Nuevo_Formato_Inventario_2016_v7.xlsm' target='_blank'> <img src='../webimages/iconos/excel.png' border='0' width='20' height='20'></A></div></B>";
		echo "<br>";
		echo "<div style='color:#F00'><br><========================================================================>";
		echo "<br><========================================================================>";
		echo "<========================================================================></DIV>";
		echo '<BR><BR><input type="button" name="BTN_RESULT" id="BTN_RESULT" value="Cerrar Ventana" onclick="Boton_Cerrar()" /><BR><BR>';
		
	}else{

		function mostrar_boton_cerrar(){
			
			echo '<br>=> Fecha y Hora Termino ==> '.date("Y-m-d H:i:s");
			echo '<BR><BR><input type="button" name="BTN_RESULT" id="BTN_RESULT" value="Cerrar Ventana" onclick="Boton_Cerrar()" />';
			echo '<BR>';
			echo '<script>$("#BTN_RESULT").focus();</script>';
		}
		
		
		echo "<br><===================================================================================================================><br>";
		echo '<br>=> Archivo TXT ==> '.$NOM_ARCHIVO;
		echo '<br>=> Fecha y Hora Inicio ==> '.$Fecha_Hora_Ini;
		echo "<br>=> RESULTADO FINAL ==> ".$RESULTADO;	
		mostrar_boton_cerrar();
		echo "<br><===================================================================================================================><BR><BR>";
	
	}
	
}else if($operacion=='Ejecuta_Cargar_Excel'){

	//ini_set('memory_limit', '-1');
	//set_time_limit(6000);
	//ini_set('memory_limit', '1000M');
	
	
	$COD_IMPORT_EXCEL_FILE	=	$_REQUEST['sCOD_IMPORT_EXCEL_FILE_XLS'];
	$COD_ENTIDAD_PROCESA	=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];
	$USUARIO_PROCESA		=	$_REQUEST['TXH_SIMI_COD_USUARIO'];
	$ID_PREDIO_SBN			=	$_REQUEST['sID_PREDIO_SBN_LOCAL'];
		
	$RESULT2		=	$oSimi_Importar_Excel_Muebles->Ver_Datos_Excel_Muebles_Adjunto_Activo($COD_IMPORT_EXCEL_FILE);
	$NOM_ARCHIVO	= 	odbc_result($RESULT2,"NOM_ARCHIVO");
	$UPLOADFILE='../../../repositorio_muebles/inventario/'.$NOM_ARCHIVO;
	
	$oSimi_Importar_Excel_Muebles->Eliminar_Datos_Tbl_Excel_Muebles_x_Codigo($COD_IMPORT_EXCEL_FILE);

	$objPHPExcel = PHPExcel_IOFactory::load($UPLOADFILE);
	
	
	
	
	function mostrar_boton_cerrar(){
		
		echo '<br>=================> Fecha y Hora Termino ==> '.date("Y-m-d H:i:s");
		echo '<BR><BR><input type="button" name="BTN_RESULT" id="BTN_RESULT" value="Cerrar Ventana" onclick="Boton_Cerrar()" />';
		echo '<BR><BR><BR>';
		echo '<script>$("#BTN_RESULT").focus();</script>';
	}

	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
		
		$TituloHojaCalculo  	= $worksheet->getTitle();
		$TotalFilas         	= $worksheet->getHighestRow(); // e.g. 10		
		$TotalFilas_SinCAB      = ($TotalFilas-1);
		$NombreColumna      	= $worksheet->getHighestColumn(); // e.g 'F'
		$TotalColumnasConData 	= PHPExcel_Cell::columnIndexFromString($NombreColumna);
		//$nrColumns = ord($NombreColumna) - 64; //Devuelve el valor ASCII del $TotalColumna - 64:
		$arrayData 				= $worksheet->toArray();
		$data_campos 			= $arrayData[0];
		
		$Fecha_Hora_Ini			= date("Y-m-d H:i:s");
		

		echo "<br><===================================================================================================================>";		
		echo "<br>=> NOMBRE DEL ARCHIVO EXCEL ==> ".$NOM_ARCHIVO;
		echo "<br>=> NOMBRE DE LA HOJA DE CALCULO ==> ".$TituloHojaCalculo;
		echo "<br>=> TOTAL COLUMNAS ==> ".$TotalColumnasConData." COLUMNAS DE LA (A - " . $NombreColumna . ")";
		echo "<br>=> TOTAL REGISTROS ==> " . $TotalFilas_SinCAB ." FILAS";
		echo '<BR>=> Fecha y Hora Inicio ==> '.$Fecha_Hora_Ini;
		echo "<br><===================================================================================================================><BR>";
		
		
		
		
		if ($TotalColumnasConData != '68' ) {
			echo "<br><===================================================================================================================>";
			echo "<br>=> ERROR COLUMNAS: EL FORMATO EXCEL SOLO DEBE CONTENER 68 COLUMNAS.";
			echo "<br><===================================================================================================================><BR>";
			mostrar_boton_cerrar();
			return false;
		}
		
		
		
		if ($TotalColumnasConData == '68' ) {
	
			$rs_campo_01 = (trim(ltrim($data_campos[0])) == 'ITEM')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_02 = (trim(ltrim($data_campos[1])) == 'RUC_ENTIDAD')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_03 = (trim(ltrim($data_campos[2])) == 'NOMBRE_LOCAL')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_04 = (trim(ltrim($data_campos[3])) == 'DEPARTAMENTO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_05 = (trim(ltrim($data_campos[4])) == 'PROVINCIA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_06 = (trim(ltrim($data_campos[5])) == 'DISTRITO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_07 = (trim(ltrim($data_campos[6])) == 'NOMBRE_AREA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_08 = (trim(ltrim($data_campos[7])) == 'ABREVIATURA_AREA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_09 = (trim(ltrim($data_campos[8])) == 'NOMBRE_OFICINA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_10 = (trim(ltrim($data_campos[9])) == 'ABREVIATURA_OFICINA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_11 = (trim(ltrim($data_campos[10])) == 'PISO_OFICINA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_12 = (trim(ltrim($data_campos[11])) == 'TIPO_DOC_IDENTIDAD')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_13 = (trim(ltrim($data_campos[12])) == 'NRO_DOC_IDENT_PERSONAL')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_14 = (trim(ltrim($data_campos[13])) == 'APELLIDO_PATERNO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_15 = (trim(ltrim($data_campos[14])) == 'APELLIDO_MATERNO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_16 = (trim(ltrim($data_campos[15])) == 'NOMBRES')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_17 = (trim(ltrim($data_campos[16])) == 'MODALIDAD_CONTRATO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_18 = (trim(ltrim($data_campos[17])) == 'CODIGO_PATRIMONIAL')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_19 = (trim(ltrim($data_campos[18])) == 'DENOMINACION_BIEN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_20 = (trim(ltrim($data_campos[19])) == 'TIPO_CAUSAL_ALTA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_21 = (trim(ltrim($data_campos[20])) == 'NRO_DOC_ADQUISICION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_22 = (trim(ltrim($data_campos[21])) == 'FECHA_ADQUISICION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_23 = (trim(ltrim($data_campos[22])) == 'VALOR_ADQUISICION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_24 = (trim(ltrim($data_campos[23])) == 'FECHA_DEPRECIACION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_25 = (trim(ltrim($data_campos[24])) == 'VALOR_DEPREC_EJERCICIO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_26 = (trim(ltrim($data_campos[25])) == 'VALOR_DEPREC_ACUMULADO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_27 = (trim(ltrim($data_campos[26])) == 'VALOR_NETO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_28 = (trim(ltrim($data_campos[27])) == 'TIP_USO_CUENTA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_29 = (trim(ltrim($data_campos[28])) == 'TIPO_CUENTA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_30 = (trim(ltrim($data_campos[29])) == 'NRO_CUENTA_CONTABLE')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_31 = (trim(ltrim($data_campos[30])) == 'CTA_CON_SEGURO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_32 = (trim(ltrim($data_campos[31])) == 'ESTADO_BIEN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_33 = (trim(ltrim($data_campos[32])) == 'CONDICION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_34 = (trim(ltrim($data_campos[33])) == 'MARCA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_35 = (trim(ltrim($data_campos[34])) == 'MODELO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_36 = (trim(ltrim($data_campos[35])) == 'TIPO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_37 = (trim(ltrim($data_campos[36])) == 'COLOR')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_38 = (trim(ltrim($data_campos[37])) == 'SERIE')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_39 = (trim(ltrim($data_campos[38])) == 'DIMENSION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_40 = (trim(ltrim($data_campos[39])) == 'PLACA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_41 = (trim(ltrim($data_campos[40])) == 'NRO_MOTOR')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_42 = (trim(ltrim($data_campos[41])) == 'NRO_CHASIS')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_43 = (trim(ltrim($data_campos[42])) == 'MATRICULA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_44 = (trim(ltrim($data_campos[43])) == 'ANIO_FABRICACION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_45 = (trim(ltrim($data_campos[44])) == 'LONGITUD')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_46 = (trim(ltrim($data_campos[45])) == 'ALTURA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_47 = (trim(ltrim($data_campos[46])) == 'ANCHO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_48 = (trim(ltrim($data_campos[47])) == 'RAZA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_49 = (trim(ltrim($data_campos[48])) == 'ESPECIE')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_50 = (trim(ltrim($data_campos[49])) == 'EDAD')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_51 = (trim(ltrim($data_campos[50])) == 'PAIS')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_52 = (trim(ltrim($data_campos[51])) == 'OTRAS_CARACT')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_53 = (trim(ltrim($data_campos[52])) == 'DESCRIPCION_UBICACION_BIEN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_54 = (trim(ltrim($data_campos[53])) == 'CAUSAL_BAJA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_55 = (trim(ltrim($data_campos[54])) == 'NRO_RESOLUCION_BAJA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_56 = (trim(ltrim($data_campos[55])) == 'FECHA_BAJA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_57 = (trim(ltrim($data_campos[56])) == 'ACTO_DISPOSICION_BIEN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_58 = (trim(ltrim($data_campos[57])) == 'NRO_RESOLUCION_DISP')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_59 = (trim(ltrim($data_campos[58])) == 'FECHA_DISPOSICION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_60 = (trim(ltrim($data_campos[59])) == 'ENTIDAD_BENEFICIADA_ACTO_DISPOSICION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_61 = (trim(ltrim($data_campos[60])) == 'MOTIVO_ELIMINACION_BIEN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_62 = (trim(ltrim($data_campos[61])) == 'ACTO_ADMINISTRACION_BIEN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_63 = (trim(ltrim($data_campos[62])) == 'NUM_RESOLUCION_ADMINISTRACION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_64 = (trim(ltrim($data_campos[63])) == 'FECHA_ADMINISTRACION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_65 = (trim(ltrim($data_campos[64])) == 'FECHA_VENC_ACTO_ADMIN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_66 = (trim(ltrim($data_campos[65])) == 'ENTIDAD_BENEFICIADA_ACTO_ADMIN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_67 = (trim(ltrim($data_campos[66])) == 'DOC_ALTA_SBN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_68 = (trim(ltrim($data_campos[67])) == 'DOC_BAJA_SBN')? 'CORRECTO': '==> INCORRECTO';
			
		
		
			echo "
			  <table width='925' border='0' cellspacing='3' cellpadding='0'>
			  <tr>
				<td width='95'><B>Columna</B></td>
				<td width='365'><B>Cabecera Original (SBN)</B></td>
				<td width='365'><B>Cabecera Inventario (Excel)</B></td>
				<td width='100'><B>Estado</B></td>
			  </tr>
			  <tr><td>Columna 01</td> <td>ITEM</td> <td>$data_campos[0]</td> <td>$rs_campo_01</td> </tr>
			  <tr><td>Columna 02</td> <td>RUC_ENTIDAD</td> <td>$data_campos[1]</td> <td>$rs_campo_02</td> </tr>
			  <tr><td>Columna 03</td> <td>NOMBRE_LOCAL</td> <td>$data_campos[2]</td> <td>$rs_campo_03</td> </tr>
			  <tr><td>Columna 04</td> <td>DEPARTAMENTO</td> <td>$data_campos[3]</td> <td>$rs_campo_04</td> </tr>  
			  <tr><td>Columna 05</td> <td>PROVINCIA</td> <td>$data_campos[4]</td> <td>$rs_campo_05</td> </tr>
			  <tr><td>Columna 06</td> <td>DISTRITO</td> <td>$data_campos[5]</td> <td>$rs_campo_06</td> </tr>
			  <tr><td>Columna 07</td> <td>NOMBRE_AREA</td><td>$data_campos[6]</td> <td>$rs_campo_07</td> </tr>
			  <tr><td>Columna 08</td> <td>ABREVIATURA_AREA</td><td>$data_campos[7]</td> <td>$rs_campo_08</td> </tr>
			  <tr><td>Columna 09</td> <td>NOMBRE_OFICINA</td><td>$data_campos[8]</td> <td>$rs_campo_09</td> </tr>
			  <tr><td>Columna 10</td> <td>ABREVIATURA_OFICINA</td><td>$data_campos[9]</td> <td>$rs_campo_10</td> </tr>
			  <tr><td>Columna 11</td> <td>PISO_OFICINA</td><td>$data_campos[10]</td> <td>$rs_campo_11</td> </tr>
			  <tr><td>Columna 12</td> <td>TIPO_DOC_IDENTIDAD</td><td>$data_campos[11]</td> <td>$rs_campo_12</td> </tr>
			  <tr><td>Columna 13</td> <td>NRO_DOC_IDENT_PERSONAL</td><td>$data_campos[12]</td> <td>$rs_campo_13</td> </tr>
			  <tr><td>Columna 14</td> <td>APELLIDO_PATERNO</td><td>$data_campos[13]</td> <td>$rs_campo_14</td> </tr>  
			  <tr><td>Columna 15</td> <td>APELLIDO_MATERNO</td><td>$data_campos[14]</td> <td>$rs_campo_15</td> </tr>
			  <tr><td>Columna 16</td> <td>NOMBRES</td><td>$data_campos[15]</td> <td>$rs_campo_16</td> </tr>
			  <tr><td>Columna 17</td> <td>MODALIDAD_CONTRATO</td><td>$data_campos[16]</td> <td>$rs_campo_17</td> </tr>
			  <tr><td>Columna 18</td> <td>CODIGO_PATRIMONIAL</td><td>$data_campos[17]</td> <td>$rs_campo_18</td> </tr>
			  <tr><td>Columna 19</td> <td>DENOMINACION_BIEN</td><td>$data_campos[18]</td> <td>$rs_campo_19</td> </tr>
			  <tr><td>Columna 20</td> <td>TIPO_CAUSAL_ALTA</td><td>$data_campos[19]</td> <td>$rs_campo_20</td> </tr>
			  <tr><td>Columna 21</td> <td>NRO_DOC_ADQUISICION</td><td>$data_campos[20]</td> <td>$rs_campo_21</td> </tr>
			  <tr><td>Columna 22</td> <td>FECHA_ADQUISICION</td><td>$data_campos[21]</td> <td>$rs_campo_22</td> </tr>
			  <tr><td>Columna 23</td> <td>VALOR_ADQUISICION</td><td>$data_campos[22]</td> <td>$rs_campo_23</td> </tr>
			  <tr><td>Columna 24</td> <td>FECHA_DEPRECIACION</td><td>$data_campos[23]</td> <td>$rs_campo_24</td> </tr>
			  <tr><td>Columna 25</td> <td>VALOR_DEPREC_EJERCICIO</td><td>$data_campos[24]</td> <td>$rs_campo_25</td> </tr>
			  <tr><td>Columna 26</td> <td>VALOR_DEPREC_ACUMULADO</td><td>$data_campos[25]</td> <td>$rs_campo_26</td> </tr>
			  <tr><td>Columna 27</td> <td>VALOR_NETO</td><td>$data_campos[26]</td> <td>$rs_campo_27</td> </tr>
			  <tr><td>Columna 28</td><td>TIP_USO_CUENTA</td><td>$data_campos[27]</td> <td>$rs_campo_28</td> </tr>
			  <tr><td>Columna 29</td><td>TIPO_CUENTA</td><td>$data_campos[28]</td> <td>$rs_campo_29</td> </tr>
			  <tr><td>Columna 30</td><td>NRO_CUENTA_CONTABLE</td><td>$data_campos[29]</td> <td>$rs_campo_30</td> </tr>
			  <tr><td>Columna 31</td><td>CTA_CON_SEGURO</td><td>$data_campos[30]</td> <td>$rs_campo_31</td> </tr>
			  <tr><td>Columna 32</td><td>ESTADO_BIEN</td><td>$data_campos[31]</td> <td>$rs_campo_32</td> </tr>
			  <tr><td>Columna 33</td><td>CONDICION</td><td>$data_campos[32]</td> <td>$rs_campo_33</td> </tr>
			  <tr><td>Columna 34</td><td>MARCA</td><td>$data_campos[33]</td> <td>$rs_campo_34</td> </tr>
			  <tr><td>Columna 35</td><td>MODELO</td><td>$data_campos[34]</td> <td>$rs_campo_35</td> </tr>
			  <tr><td>Columna 36</td><td>TIPO</td><td>$data_campos[35]</td> <td>$rs_campo_36</td> </tr>
			  <tr><td>Columna 37</td><td>COLOR</td><td>$data_campos[36]</td> <td>$rs_campo_37</td> </tr>
			  <tr><td>Columna 38</td><td>SERIE</td><td>$data_campos[37]</td> <td>$rs_campo_38</td> </tr>
			  <tr><td>Columna 39</td><td>DIMENSION</td><td>$data_campos[38]</td> <td>$rs_campo_39</td> </tr>
			  <tr><td>Columna 40</td><td>PLACA</td><td>$data_campos[39]</td> <td>$rs_campo_40</td> </tr>
			  <tr><td>Columna 41</td><td>NRO_MOTOR</td><td>$data_campos[40]</td> <td>$rs_campo_41</td> </tr>
			  <tr><td>Columna 42</td><td>NRO_CHASIS</td><td>$data_campos[41]</td> <td>$rs_campo_42</td> </tr>
			  <tr><td>Columna 43</td><td>MATRICULA</td><td>$data_campos[42]</td> <td>$rs_campo_43</td> </tr>
			  <tr><td>Columna 44</td><td>ANIO_FABRICACION</td><td>$data_campos[43]</td> <td>$rs_campo_44</td> </tr>
			  <tr><td>Columna 45</td><td>LONGITUD</td><td>$data_campos[44]</td> <td>$rs_campo_45</td> </tr>
			  <tr><td>Columna 46</td><td>ALTURA</td><td>$data_campos[45]</td> <td>$rs_campo_46</td> </tr>
			  <tr><td>Columna 47</td><td>ANCHO</td><td>$data_campos[46]</td> <td>$rs_campo_47</td> </tr>
			  <tr><td>Columna 48</td><td>RAZA</td><td>$data_campos[47]</td> <td>$rs_campo_48</td> </tr>
			  <tr><td>Columna 49</td><td>ESPECIE</td><td>$data_campos[48]</td> <td>$rs_campo_49</td> </tr>
			  <tr><td>Columna 50</td><td>EDAD</td><td>$data_campos[49]</td> <td>$rs_campo_50</td> </tr>
			  <tr><td>Columna 51</td><td>PAIS</td><td>$data_campos[50]</td> <td>$rs_campo_51</td> </tr>
			  <tr><td>Columna 52</td><td>OTRAS_CARACT</td><td>$data_campos[51]</td> <td>$rs_campo_52</td> </tr>
			  <tr><td>Columna 53</td><td>DESCRIPCION_UBICACION_BIEN</td><td>$data_campos[52]</td> <td>$rs_campo_53</td> </tr>
			  <tr><td>Columna 54</td><td>CAUSAL_BAJA</td><td>$data_campos[53]</td> <td>$rs_campo_54</td> </tr>
			  <tr><td>Columna 55</td><td>NRO_RESOLUCION_BAJA</td><td>$data_campos[54]</td> <td>$rs_campo_55</td> </tr>
			  <tr><td>Columna 56</td><td>FECHA_BAJA</td><td>$data_campos[55]</td> <td>$rs_campo_56</td> </tr>
			  <tr><td>Columna 57</td><td>ACTO_DISPOSICION_BIEN</td><td>$data_campos[56]</td> <td>$rs_campo_57</td> </tr>
			  <tr><td>Columna 58</td><td>NRO_RESOLUCION_DISP</td><td>$data_campos[57]</td> <td>$rs_campo_58</td> </tr>
			  <tr><td>Columna 59</td><td>FECHA_DISPOSICION</td><td>$data_campos[58]</td> <td>$rs_campo_59</td> </tr>
			  <tr><td>Columna 60</td><td>ENTIDAD_BENEFICIADA_ACTO_DISPOSICION</td><td>$data_campos[59]</td> <td>$rs_campo_60</td> </tr>
			  <tr><td>Columna 61</td><td>MOTIVO_ELIMINACION_BIEN</td><td>$data_campos[60]</td> <td>$rs_campo_61</td> </tr>
			  <tr><td>Columna 62</td><td>ACTO_ADMINISTRACION_BIEN</td><td>$data_campos[61]</td> <td>$rs_campo_62</td> </tr>
			  <tr><td>Columna 63</td><td>NUM_RESOLUCION_ADMINISTRACION</td><td>$data_campos[62]</td> <td>$rs_campo_63</td> </tr>
			  <tr><td>Columna 64</td><td>FECHA_ADMINISTRACION</td><td>$data_campos[63]</td> <td>$rs_campo_64</td> </tr>
			  <tr><td>Columna 65</td><td>FECHA_VENC_ACTO_ADMIN</td><td>$data_campos[64]</td> <td>$rs_campo_65</td> </tr>
			  <tr><td>Columna 66</td><td>ENTIDAD_BENEFICIADA_ACTO_ADMIN</td><td>$data_campos[65]</td> <td>$rs_campo_66</td> </tr>
			  <tr><td>Columna 67</td><td>DOC_ALTA_SBN</td><td>$data_campos[66]</td> <td>$rs_campo_67</td> </tr>
			  <tr><td>Columna 68</td><td>DOC_BAJA_SBN</td><td>$data_campos[67]</td> <td>$rs_campo_68</td> </tr>
			  </table>
			";
			
		
			echo "<===================================================================================================================><br>";
	
				
			if( $rs_campo_01== '==> INCORRECTO' ||
				$rs_campo_02== '==> INCORRECTO' ||
				$rs_campo_03== '==> INCORRECTO' ||
				$rs_campo_04== '==> INCORRECTO' ||
				$rs_campo_05== '==> INCORRECTO' ||
				$rs_campo_06== '==> INCORRECTO' ||
				$rs_campo_07== '==> INCORRECTO' ||
				$rs_campo_08== '==> INCORRECTO' ||
				$rs_campo_09== '==> INCORRECTO' ||
				$rs_campo_10== '==> INCORRECTO' ||
				$rs_campo_11== '==> INCORRECTO' ||
				$rs_campo_12== '==> INCORRECTO' ||
				$rs_campo_13== '==> INCORRECTO' ||
				$rs_campo_14== '==> INCORRECTO' ||
				$rs_campo_15== '==> INCORRECTO' ||
				$rs_campo_16== '==> INCORRECTO' ||
				$rs_campo_17== '==> INCORRECTO' ||
				$rs_campo_18== '==> INCORRECTO' ||
				$rs_campo_19== '==> INCORRECTO' ||
				$rs_campo_20== '==> INCORRECTO' ||
				$rs_campo_21== '==> INCORRECTO' ||
				$rs_campo_22== '==> INCORRECTO' ||
				$rs_campo_23== '==> INCORRECTO' ||
				$rs_campo_24== '==> INCORRECTO' ||
				$rs_campo_25== '==> INCORRECTO' ||
				$rs_campo_26== '==> INCORRECTO' ||
				$rs_campo_27== '==> INCORRECTO' ||
				$rs_campo_28== '==> INCORRECTO' ||
				$rs_campo_29== '==> INCORRECTO' ||
				$rs_campo_30== '==> INCORRECTO' ||
				$rs_campo_31== '==> INCORRECTO' ||
				$rs_campo_32== '==> INCORRECTO' ||
				$rs_campo_33== '==> INCORRECTO' ||
				$rs_campo_34== '==> INCORRECTO' ||
				$rs_campo_35== '==> INCORRECTO' ||
				$rs_campo_36== '==> INCORRECTO' ||
				$rs_campo_37== '==> INCORRECTO' ||
				$rs_campo_38== '==> INCORRECTO' ||
				$rs_campo_39== '==> INCORRECTO' ||
				$rs_campo_40== '==> INCORRECTO' ||
				$rs_campo_41== '==> INCORRECTO' ||
				$rs_campo_42== '==> INCORRECTO' ||
				$rs_campo_43== '==> INCORRECTO' ||
				$rs_campo_44== '==> INCORRECTO' ||
				$rs_campo_45== '==> INCORRECTO' ||
				$rs_campo_46== '==> INCORRECTO' ||
				$rs_campo_47== '==> INCORRECTO' ||
				$rs_campo_48== '==> INCORRECTO' ||
				$rs_campo_49== '==> INCORRECTO' ||
				$rs_campo_50== '==> INCORRECTO' ||
				$rs_campo_51== '==> INCORRECTO' ||
				$rs_campo_52== '==> INCORRECTO' ||
				$rs_campo_53== '==> INCORRECTO' ||
				$rs_campo_54== '==> INCORRECTO' ||
				$rs_campo_55== '==> INCORRECTO' ||
				$rs_campo_56== '==> INCORRECTO' ||
				$rs_campo_57== '==> INCORRECTO' ||
				$rs_campo_58== '==> INCORRECTO' ||
				$rs_campo_59== '==> INCORRECTO' ||
				$rs_campo_60== '==> INCORRECTO' ||
				$rs_campo_61== '==> INCORRECTO' ||
				$rs_campo_62== '==> INCORRECTO' ||
				$rs_campo_63== '==> INCORRECTO' ||
				$rs_campo_64== '==> INCORRECTO' ||
				$rs_campo_65== '==> INCORRECTO' ||
				$rs_campo_66== '==> INCORRECTO' ||
				$rs_campo_67== '==> INCORRECTO' ||
				$rs_campo_68== '==> INCORRECTO'	){
			
				echo "<br><===================================================================================================================>";
				echo "<br>=> ERROR CON LOS NOMBRES DE LA CABECERA DEL FORMATO EXCEL ";
				echo "<br><===================================================================================================================><BR>";
				mostrar_boton_cerrar();
				return false;
					
			}
		}
		
		
		if ($TotalColumnasConData == '68' ) {

			for ($row = 1; $row < $TotalFilas; ++ $row) {

				
				//for ($col = 0; $col < $highestColumnIndex; ++ $col) {
					//$cell = $worksheet->getCellByColumnAndRow($col, $row);
					//echo $val = $cell->getValue();
				//}
				
					$ITEM 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][0])))));
					$RUC_ENTIDAD 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][1])))));
					$NOMBRE_LOCAL 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][2])))));
					$DEPARTAMENTO 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][3])))));
					$PROVINCIA 				= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][4])))));
					$DISTRITO 				= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][5])))));
					$NOMBRE_AREA 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][6])))));
					$ABREVIATURA_AREA 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][7])))));
					$NOMBRE_OFICINA 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][8])))));
					$ABREVIATURA_OFICINA 	= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][9])))));
					$PISO_OFICINA 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][10])))));
					$TIPO_DOC_IDENTIDAD 	= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][11])))));
					$NRO_DOC_IDENT_PERSONAL = convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][12])))));
					$APELLIDO_PATERNO 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][13])))));
					$APELLIDO_MATERNO 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][14])))));
					$NOMBRES 				= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][15])))));
					$MODALIDAD_CONTRATO 	= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][16])))));
					$CODIGO_PATRIMONIAL 	= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][17])))));
					$DENOMINACION_BIEN 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][18])))));
					$TIPO_CAUSAL_ALTA 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][19])))));
					$NRO_DOC_ADQUISICION 	= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][20])))));
					
					$FECHA_ADQUISICION_XLS 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][21])))));
					
					
					if(strtotime($FECHA_ADQUISICION_XLS)){
						$FECHA_ADQUISICION = date('d-m-Y', strtotime($FECHA_ADQUISICION_XLS));
					}else{
						$FECHA_ADQUISICION = $FECHA_ADQUISICION_XLS;
					}
					
					$VALOR_ADQUISICION 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][22])))));
					$VALOR_ADQUISICION  = str_replace(',', '', $VALOR_ADQUISICION); 
					
					$FECHA_DEPRECIACION_XLS 	= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][23])))));
					
					if(strtotime($FECHA_DEPRECIACION_XLS)){
						$FECHA_DEPRECIACION = date('d-m-Y', strtotime($FECHA_DEPRECIACION_XLS));
					}else{
						$FECHA_DEPRECIACION = $FECHA_DEPRECIACION_XLS;
					}
					
					$VALOR_DEPREC_EJERCICIO = convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][24])))));
					$VALOR_DEPREC_EJERCICIO  = str_replace(',', '', $VALOR_DEPREC_EJERCICIO); 
					
					$VALOR_DEPREC_ACUMULADO = convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][25])))));
					$VALOR_DEPREC_ACUMULADO  = str_replace(',', '', $VALOR_DEPREC_ACUMULADO); 
					
					$VALOR_NETO 	= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][26])))));
					$VALOR_NETO 	= str_replace(',', '', $VALOR_NETO); 
					
					$TIP_USO_CUENTA 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][27])))));
					$TIPO_CUENTA 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][28])))));
					$NRO_CUENTA_CONTABLE 	= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][29])))));
					$CTA_CON_SEGURO 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][30])))));
					$ESTADO_BIEN 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][31])))));
					$CONDICION 				= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][32])))));
					$MARCA 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][33])))));
					$MODELO 				= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][34])))));
					$TIPO 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][35])))));
					$COLOR 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][36])))));
					$SERIE 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][37])))));
					$DIMENSION 				= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][38])))));
					$PLACA 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][39])))));
					$NRO_MOTOR 				= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][40])))));
					$NRO_CHASIS 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][41])))));
					$MATRICULA 				= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][42])))));
					$ANIO_FABRICACION 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][43])))));
					$LONGITUD 				= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][44])))));
					$ALTURA 				= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][45])))));
					$ANCHO 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][46])))));
					$RAZA 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][47])))));
					$ESPECIE 				= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][48])))));
					$EDAD 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][49])))));
					$PAIS 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][50])))));
					$OTRAS_CARACT 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][51])))));
					$DESCRIPCION_UBICACION_BIEN 	= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][52])))));
					$CAUSAL_BAJA 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][53])))));
					$NRO_RESOLUCION_BAJA 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][54])))));
					$FECHA_BAJA_XLS 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][55])))));
					
					if(strtotime($FECHA_BAJA_XLS)){
						$FECHA_BAJA = date('d-m-Y', strtotime($FECHA_BAJA_XLS));
					}else{
						$FECHA_BAJA = $FECHA_BAJA_XLS;
					}
					
					$ACTO_DISPOSICION_BIEN 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][56])))));
					$NRO_RESOLUCION_DISP			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][57])))));
					$FECHA_DISPOSICION_XLS 				= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][58])))));
					
					if(strtotime($FECHA_DISPOSICION_XLS)){
						$FECHA_DISPOSICION = date('d-m-Y', strtotime($FECHA_DISPOSICION_XLS));
					}else{
						$FECHA_DISPOSICION = $FECHA_DISPOSICION_XLS;
					}
					
					$ENTIDAD_BENEFICIADA_ACTO_DISPOSICION = convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][59])))));
					$MOTIVO_ELIMINACION_BIEN 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][60])))));
					$ACTO_ADMINISTRACION_BIEN 		= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][61])))));
					$NUM_RESOLUCION_ADMINISTRACION 	= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][62])))));
					$FECHA_ADMINISTRACION_XLS 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][63])))));
					
					if(strtotime($FECHA_ADMINISTRACION_XLS)){
						$FECHA_ADMINISTRACION = date('d-m-Y', strtotime($FECHA_ADMINISTRACION_XLS));
					}else{
						$FECHA_ADMINISTRACION = $FECHA_ADMINISTRACION_XLS;
					}
					
					$FECHA_VENC_ACTO_ADMIN_XLS 			= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][64])))));
					
					if(strtotime($FECHA_VENC_ACTO_ADMIN_XLS)){
						$FECHA_VENC_ACTO_ADMIN = date('d-m-Y', strtotime($FECHA_VENC_ACTO_ADMIN_XLS));
					}else{
						$FECHA_VENC_ACTO_ADMIN = $FECHA_VENC_ACTO_ADMIN_XLS;
					}
					
					$ENTIDAD_BENEFICIADA_ACTO_ADMIN = convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][65])))));
					$DOC_ALTA_SBN 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][66])))));
					$DOC_BAJA_SBN 					= convertir_a_utf8(strtoupper(ltrim(rtrim(filtrar_caracteres($arrayData[$row][67])))));
					
				
				
					
					$Result_Codigo = $oSimi_Importar_Excel_Muebles->Generar_Codigo_TBL_MUEBLES_IMPORTAR_EXCEL_MUEBLE();
					$COD_IMPORT_EXCEL_MUEBLE	= 	odbc_result($Result_Codigo,"CODIGO");
					
					
					$COD_IMPORT_EXCEL_MUEBLE_OK = " '$COD_IMPORT_EXCEL_MUEBLE' ";
					$COD_IMPORT_EXCEL_FILE_OK = " '$COD_IMPORT_EXCEL_FILE' ";
					$COD_ENTIDAD_PROCESA_OK = " '$COD_ENTIDAD_PROCESA' ";
					$USUARIO_PROCESA_OK = " '$USUARIO_PROCESA' ";
					$FECHA_PROCESA_OK = " GETDATE() ";
					$ID_PREDIO_SBN_OK = " '$ID_PREDIO_SBN' ";
					$ITEM_OK = " '$ITEM' ";
					$RUC_ENTIDAD_OK = " '$RUC_ENTIDAD' ";
					$NOMBRE_LOCAL_OK = " '$NOMBRE_LOCAL' ";
					$DEPARTAMENTO_OK = " '$DEPARTAMENTO' ";
					$PROVINCIA_OK = " '$PROVINCIA' ";
					$DISTRITO_OK = " '$DISTRITO' ";
					$NOMBRE_AREA_OK = " '$NOMBRE_AREA' ";
					$ABREVIATURA_AREA_OK = " '$ABREVIATURA_AREA' ";
					$NOMBRE_OFICINA_OK = " '$NOMBRE_OFICINA' ";
					$ABREVIATURA_OFICINA_OK = " '$ABREVIATURA_OFICINA' ";
					$PISO_OFICINA_OK = " '$PISO_OFICINA' ";
					$TIPO_DOC_IDENTIDAD_OK = " '$TIPO_DOC_IDENTIDAD' ";
					$NRO_DOC_IDENT_PERSONAL_OK = " '$NRO_DOC_IDENT_PERSONAL' ";
					$APELLIDO_PATERNO_OK = " '$APELLIDO_PATERNO' ";
					$APELLIDO_MATERNO_OK = " '$APELLIDO_MATERNO' ";
					$NOMBRES_OK = " '$NOMBRES' ";
					$MODALIDAD_CONTRATO_OK = " '$MODALIDAD_CONTRATO' ";
					$CODIGO_PATRIMONIAL_OK = " '$CODIGO_PATRIMONIAL' ";
					$DENOMINACION_BIEN_OK = " '$DENOMINACION_BIEN' ";
					$TIPO_CAUSAL_ALTA_OK = " '$TIPO_CAUSAL_ALTA' ";
					$NRO_DOC_ADQUISICION_OK = " '$NRO_DOC_ADQUISICION' ";
					$FECHA_ADQUISICION_OK = " '$FECHA_ADQUISICION' ";
					$VALOR_ADQUISICION_OK = " '$VALOR_ADQUISICION' ";
					$FECHA_DEPRECIACION_OK = " '$FECHA_DEPRECIACION' ";
					$VALOR_DEPREC_EJERCICIO_OK = " '$VALOR_DEPREC_EJERCICIO' ";
					$VALOR_DEPREC_ACUMULADO_OK = " '$VALOR_DEPREC_ACUMULADO' ";
					$VALOR_NETO_OK = " '$VALOR_NETO' ";
					$TIP_USO_CUENTA_OK = " '$TIP_USO_CUENTA' ";
					$TIPO_CUENTA_OK = " '$TIPO_CUENTA' ";
					$NRO_CUENTA_CONTABLE_OK = " '$NRO_CUENTA_CONTABLE' ";
					$CTA_CON_SEGURO_OK = " '$CTA_CON_SEGURO' ";
					$ESTADO_BIEN_OK = " '$ESTADO_BIEN' ";
					$CONDICION_OK = " '$CONDICION' ";
					$MARCA_OK = " '$MARCA' ";
					$MODELO_OK = " '$MODELO' ";
					$TIPO_OK = " '$TIPO' ";
					$COLOR_OK = " '$COLOR' ";
					$SERIE_OK = " '$SERIE' ";
					$DIMENSION_OK = " '$DIMENSION' ";
					$PLACA_OK = " '$PLACA' ";
					$NRO_MOTOR_OK = " '$NRO_MOTOR' ";
					$NRO_CHASIS_OK = " '$NRO_CHASIS' ";
					$MATRICULA_OK = " '$MATRICULA' ";
					$ANIO_FABRICACION_OK = " '$ANIO_FABRICACION' ";
					$LONGITUD_OK = " '$LONGITUD' ";
					$ALTURA_OK = " '$ALTURA' ";
					$ANCHO_OK = " '$ANCHO' ";
					$RAZA_OK = " '$RAZA' ";
					$ESPECIE_OK = " '$ESPECIE' ";
					$EDAD_OK = " '$EDAD' ";
					$PAIS_OK = " '$PAIS' ";
					$OTRAS_CARACT_OK = " '$OTRAS_CARACT' ";
					$DESCRIPCION_UBICACION_BIEN_OK = " '$DESCRIPCION_UBICACION_BIEN' ";
					$CAUSAL_BAJA_OK = " '$CAUSAL_BAJA' ";
					$NRO_RESOLUCION_BAJA_OK = " '$NRO_RESOLUCION_BAJA' ";
					$FECHA_BAJA_OK = " '$FECHA_BAJA' ";
					$ACTO_DISPOSICION_BIEN_OK = " '$ACTO_DISPOSICION_BIEN' ";
					$NRO_RESOLUCION_DISP_OK = " '$NRO_RESOLUCION_DISP' ";
					$FECHA_DISPOSICION_OK = " '$FECHA_DISPOSICION' ";
					$ENTIDAD_BENEFICIADA_ACTO_DISPOSICION_OK = " '$ENTIDAD_BENEFICIADA_ACTO_DISPOSICION' ";
					$MOTIVO_ELIMINACION_BIEN_OK = " '$MOTIVO_ELIMINACION_BIEN' ";
					$ACTO_ADMINISTRACION_BIEN_OK = " '$ACTO_ADMINISTRACION_BIEN' ";
					$NUM_RESOLUCION_ADMINISTRACION_OK = " '$NUM_RESOLUCION_ADMINISTRACION' ";
					$FECHA_ADMINISTRACION_OK = " '$FECHA_ADMINISTRACION' ";
					$FECHA_VENC_ACTO_ADMIN_OK = " '$FECHA_VENC_ACTO_ADMIN' ";
					$ENTIDAD_BENEFICIADA_ACTO_ADMIN_OK = " '$ENTIDAD_BENEFICIADA_ACTO_ADMIN' ";
					$DOC_ALTA_SBN_OK = " '$DOC_ALTA_SBN' ";
					$DOC_BAJA_SBN_OK = " '$DOC_BAJA_SBN' ";

					
					$Insert_Table_File = $oSimi_Importar_Excel_Muebles->Insertar_Registro_to_Tabla_Excel_Muebles_x_Local_Directo($COD_IMPORT_EXCEL_MUEBLE_OK, $COD_IMPORT_EXCEL_FILE_OK, $COD_ENTIDAD_PROCESA_OK, $USUARIO_PROCESA_OK, $FECHA_PROCESA_OK, $ID_PREDIO_SBN_OK, $ITEM_OK, $RUC_ENTIDAD_OK, $NOMBRE_LOCAL_OK, $DEPARTAMENTO_OK, $PROVINCIA_OK, $DISTRITO_OK, $NOMBRE_AREA_OK, $ABREVIATURA_AREA_OK, $NOMBRE_OFICINA_OK, $ABREVIATURA_OFICINA_OK, $PISO_OFICINA_OK, $TIPO_DOC_IDENTIDAD_OK, $NRO_DOC_IDENT_PERSONAL_OK, $APELLIDO_PATERNO_OK, $APELLIDO_MATERNO_OK, $NOMBRES_OK, $MODALIDAD_CONTRATO_OK, $CODIGO_PATRIMONIAL_OK, $DENOMINACION_BIEN_OK, $TIPO_CAUSAL_ALTA_OK, $NRO_DOC_ADQUISICION_OK, $FECHA_ADQUISICION_OK, $VALOR_ADQUISICION_OK, $FECHA_DEPRECIACION_OK, $VALOR_DEPREC_EJERCICIO_OK, $VALOR_DEPREC_ACUMULADO_OK, $VALOR_NETO_OK, $TIP_USO_CUENTA_OK, $TIPO_CUENTA_OK, $NRO_CUENTA_CONTABLE_OK, $CTA_CON_SEGURO_OK, $ESTADO_BIEN_OK, $CONDICION_OK, $MARCA_OK, $MODELO_OK, $TIPO_OK, $COLOR_OK, $SERIE_OK, $DIMENSION_OK, $PLACA_OK, $NRO_MOTOR_OK, $NRO_CHASIS_OK, $MATRICULA_OK, $ANIO_FABRICACION_OK, $LONGITUD_OK, $ALTURA_OK, $ANCHO_OK, $RAZA_OK, $ESPECIE_OK, $EDAD_OK, $PAIS_OK, $OTRAS_CARACT_OK, $DESCRIPCION_UBICACION_BIEN_OK, $CAUSAL_BAJA_OK, $NRO_RESOLUCION_BAJA_OK, $FECHA_BAJA_OK, $ACTO_DISPOSICION_BIEN_OK, $NRO_RESOLUCION_DISP_OK, $FECHA_DISPOSICION_OK, $ENTIDAD_BENEFICIADA_ACTO_DISPOSICION_OK, $MOTIVO_ELIMINACION_BIEN_OK, $ACTO_ADMINISTRACION_BIEN_OK, $NUM_RESOLUCION_ADMINISTRACION_OK, $FECHA_ADMINISTRACION_OK, $FECHA_VENC_ACTO_ADMIN_OK, $ENTIDAD_BENEFICIADA_ACTO_ADMIN_OK, $DOC_ALTA_SBN_OK, $DOC_BAJA_SBN_OK );
					
					$contador_OK++;
				
					if($Insert_Table_File){
						$RESULTADO_MIGRACION = 'OK';
					}else{
						$RESULTADO_MIGRACION = 'MAL';
						echo "<BR>=================>   ERROR AL INSERTAR LOS REGISTROS, COORDINE CON EL AREA DE SISTEMAS DE LA SBN.";
						mostrar_boton_cerrar();
						return false;
					}
					
			}
			
		
			if($RESULTADO_MIGRACION == 'OK'){
				$RESULT_Proceso	=	$oSimi_Importar_Excel_Muebles->Actualiza_Estado_Ejecucion_Proceso_Excel($COD_IMPORT_EXCEL_FILE, $contador_OK);
				echo "<BR><===================================================================================================================>";
				echo "<BR><===================================================================================================================>";
				echo "<BR><===================================================================================================================>";
				echo "<BR>";
				echo "<BR>=================>   CONFIRMACION DE LOS DATOS DE MIGRACIÓN  ";
				echo "<BR>=================>   SE MIGRO CORRECTAMENTE ";
				echo "<BR>=================>   [ ".$contador_OK." REGISTROS MIGRADOS ]";
				echo "<BR>";
				echo "<BR><===================================================================================================================>";
				echo "<BR><===================================================================================================================>";
				echo "<BR><===================================================================================================================>";
				mostrar_boton_cerrar();
				return FALSE;
			}else{
				echo "<BR>=================>   ERROR AL REALIZAR LA MIGRACION, REVISE SU CUADRO EXCEL.";
				mostrar_boton_cerrar();
				return false;
			}
		
		}
		
		
	}
	
	
	
}else if($operacion=='Ejecuta_Procesar_Carga'){
	
	$COD_IMPORT_EXCEL_FILE	=	$_REQUEST['sCOD_IMPORT_EXCEL_FILE_XLS'];
	$xANIO					=	$_REQUEST['txh_migra_anio'];
	
	
	
	$RESULTADO_PROCESO			=	$oSimi_Importar_Excel_Muebles->Procesar_Validar_DATA_EXCEL_MUEBLE_XLS($COD_IMPORT_EXCEL_FILE, $xANIO);
	$RESULTADO		= 	odbc_result($RESULTADO_PROCESO,"RESULTADO");
	$COD_IMPORT_EXCEL_FILE_RS	= 	odbc_result($RESULTADO_PROCESO,"COD_IMPORT_EXCEL_FILE");
	$BIENES_SIN_MARGESI = 0;
	
	if($RESULTADO == 'ERROR' || $RESULTADO == 'OK'){
		
		$TOTAL_REGISTRO_RS				= 	odbc_result($RESULTADO_PROCESO,"TOTAL_REGISTRO");
		$TOTAL_BIEN_NO_CATAL_RS			= 	odbc_result($RESULTADO_PROCESO,"TOTAL_BIEN_NO_CATAL");
		$BIENES_SIN_MARGESI			= 	odbc_result($RESULTADO_PROCESO,"BIENES_SIN_MARGESI");

	}elseif($RESULTADO == 'FALLA'){
		
		$ERRORLINE				= 	odbc_result($RESULTADO_PROCESO,"ERRORLINE");
		$ERRORMESSAGE			= 	odbc_result($RESULTADO_PROCESO,"ERRORMESSAGE");
		
	}elseif($RESULTADO == 'DUPLICADOS'){
		
		$TOTAL_REGISTRO_RS				= 	odbc_result($RESULTADO_PROCESO,"TOTAL_REGISTRO"); 
		
	}
	
	
	$valor = ($RESULTADO_PROCESO)? '1'.'[*]'.$RESULTADO.'[*]'.$COD_IMPORT_EXCEL_FILE_RS.'[*]'.$TOTAL_REGISTRO_RS.'[*]'.$TOTAL_BIEN_NO_CATAL_RS.'[*]'.$ERRORLINE.'[*]'.$ERRORMESSAGE.'[*]'.$BIENES_SIN_MARGESI  : '0';
	
	echo $valor;

}else if($operacion=='Ejecuta_Cargar_Excel_Version_01'){
	
	//ini_set('memory_limit', '-1');
	//set_time_limit(6000);
	//ini_set('memory_limit', '1000M');
	
	$COD_IMPORT_EXCEL_FILE	=	$_REQUEST['sCOD_IMPORT_EXCEL_FILE_XLS'];
	$COD_ENTIDAD_PROCESA	=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];
	$USUARIO_PROCESA		=	$_REQUEST['TXH_SIMI_COD_USUARIO'];
	$ID_PREDIO_SBN			=	$_REQUEST['sID_PREDIO_SBN_Local'];
	
	
	
	$RESULT2		=	$oSimi_Importar_Excel_Muebles->Ver_Datos_Excel_Muebles_Adjunto_Activo($COD_IMPORT_EXCEL_FILE);
	$NOM_ARCHIVO	= 	odbc_result($RESULT2,"NOM_ARCHIVO");
	$UPLOADFILE='../../../repositorio_muebles/inventario/'.$NOM_ARCHIVO;


	//$objReader = PHPExcel_IOFactory::createReader('Excel2007');
	//$objPHPExcel = $objReader->load($UPLOADFILE);
		
	$objPHPExcel = PHPExcel_IOFactory::load($UPLOADFILE);
	
	
	foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
		
		//$arrayData = $worksheet->toArray();
		//$data_campos = $arrayData[0];		
		//echo $columnas = count($data_campos); //$columnas = sizeof($data_campos);
		
		
		$TituloHojaCalculo  	= $worksheet->getTitle();
		$TotalFilas         	= $worksheet->getHighestRow(); // e.g. 10		
		$TotalFilas_SinCAB      = ($TotalFilas-1);
		$NombreColumna      	= $worksheet->getHighestColumn(); // e.g 'F'
		$TotalColumnasConData 	= PHPExcel_Cell::columnIndexFromString($NombreColumna);
		//$nrColumns = ord($NombreColumna) - 64; //Devuelve el valor ASCII del $TotalColumna - 64:
		$arrayData 				= $worksheet->toArray();
		$data_campos 			= $arrayData[0];
		

		echo "<br><===================================================================================================================>";		
		echo "<br>=> NOMBRE DEL ARCHIVO EXCEL ==> ".$NOM_ARCHIVO;
		echo "<br>=> NOMBRE DE LA HOJA DE CALCULO ==> ".$TituloHojaCalculo;
		echo "<br>=> TOTAL COLUMNAS ==> ".$TotalColumnasConData." COLUMNAS DE LA (A - " . $NombreColumna . ")";
		echo "<br>=> TOTAL REGISTROS ==> " . $TotalFilas_SinCAB ." FILAS";
		echo "<br><===================================================================================================================><BR>";



		echo "<br><===================================================================================================================>";
		echo "<br>=> VALIDACION DE CAMPOS DEL CUADRO EXCEL ";
		echo "<br><===================================================================================================================><BR>";

		if ($TotalColumnasConData != '68') {
				echo "<br><===================================================================================================================>";
				echo "<br>=> ERROR COLUMNAS: LA CANTIDAD DE COLUMNAS DEL CUADRO EXCEL SOBREPASAN A LOS 68 COLUMNAS DEL LA PLANTILLA.";
				echo "<br><===================================================================================================================><BR>";
				echo '<BR><input type="button" name="BTN_RESULT" id="BTN_RESULT" value="Cerrar Ventana" onclick="Boton_Cerrar()" />';
				return false;
		}
		
		/*		
		if ($TotalFilas_SinCAB  >  3000) {
				echo "<br><===================================================================================================================>";
				echo "<br>=> ERROR FILAS: LA CANTIDAD DE REGISTROS DEL CUADRO EXCEL SOBREPASAN A LOS 1500 REGISTROS DEL LA PLANTILLA.";
				echo "<br><===================================================================================================================><BR>";
				echo '<BR><input type="button" name="BTN_RESULT" id="BTN_RESULT" value="Cerrar Ventana" onclick="Boton_Cerrar()" />';
				return false;
		}
		*/
		
		
		if ($TotalColumnasConData == '68' ) {
	
			$rs_campo_01 = (trim(ltrim($data_campos[0])) == 'ITEM')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_02 = (trim(ltrim($data_campos[1])) == 'RUC_ENTIDAD')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_03 = (trim(ltrim($data_campos[2])) == 'NOMBRE_LOCAL')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_04 = (trim(ltrim($data_campos[3])) == 'DEPARTAMENTO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_05 = (trim(ltrim($data_campos[4])) == 'PROVINCIA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_06 = (trim(ltrim($data_campos[5])) == 'DISTRITO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_07 = (trim(ltrim($data_campos[6])) == 'NOMBRE_AREA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_08 = (trim(ltrim($data_campos[7])) == 'ABREVIATURA_AREA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_09 = (trim(ltrim($data_campos[8])) == 'NOMBRE_OFICINA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_10 = (trim(ltrim($data_campos[9])) == 'ABREVIATURA_OFICINA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_11 = (trim(ltrim($data_campos[10])) == 'PISO_OFICINA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_12 = (trim(ltrim($data_campos[11])) == 'TIPO_DOC_IDENTIDAD')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_13 = (trim(ltrim($data_campos[12])) == 'NRO_DOC_IDENT_PERSONAL')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_14 = (trim(ltrim($data_campos[13])) == 'APELLIDO_PATERNO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_15 = (trim(ltrim($data_campos[14])) == 'APELLIDO_MATERNO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_16 = (trim(ltrim($data_campos[15])) == 'NOMBRES')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_17 = (trim(ltrim($data_campos[16])) == 'MODALIDAD_CONTRATO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_18 = (trim(ltrim($data_campos[17])) == 'CODIGO_PATRIMONIAL')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_19 = (trim(ltrim($data_campos[18])) == 'DENOMINACION_BIEN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_20 = (trim(ltrim($data_campos[19])) == 'TIPO_CAUSAL_ALTA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_21 = (trim(ltrim($data_campos[20])) == 'NRO_DOC_ADQUISICION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_22 = (trim(ltrim($data_campos[21])) == 'FECHA_ADQUISICION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_23 = (trim(ltrim($data_campos[22])) == 'VALOR_ADQUISICION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_24 = (trim(ltrim($data_campos[23])) == 'FECHA_DEPRECIACION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_25 = (trim(ltrim($data_campos[24])) == 'VALOR_DEPREC_EJERCICIO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_26 = (trim(ltrim($data_campos[25])) == 'VALOR_DEPREC_ACUMULADO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_27 = (trim(ltrim($data_campos[26])) == 'VALOR_NETO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_28 = (trim(ltrim($data_campos[27])) == 'TIP_USO_CUENTA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_29 = (trim(ltrim($data_campos[28])) == 'TIPO_CUENTA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_30 = (trim(ltrim($data_campos[29])) == 'NRO_CUENTA_CONTABLE')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_31 = (trim(ltrim($data_campos[30])) == 'CTA_CON_SEGURO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_32 = (trim(ltrim($data_campos[31])) == 'ESTADO_BIEN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_33 = (trim(ltrim($data_campos[32])) == 'CONDICION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_34 = (trim(ltrim($data_campos[33])) == 'MARCA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_35 = (trim(ltrim($data_campos[34])) == 'MODELO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_36 = (trim(ltrim($data_campos[35])) == 'TIPO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_37 = (trim(ltrim($data_campos[36])) == 'COLOR')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_38 = (trim(ltrim($data_campos[37])) == 'SERIE')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_39 = (trim(ltrim($data_campos[38])) == 'DIMENSION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_40 = (trim(ltrim($data_campos[39])) == 'PLACA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_41 = (trim(ltrim($data_campos[40])) == 'NRO_MOTOR')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_42 = (trim(ltrim($data_campos[41])) == 'NRO_CHASIS')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_43 = (trim(ltrim($data_campos[42])) == 'MATRICULA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_44 = (trim(ltrim($data_campos[43])) == 'ANIO_FABRICACION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_45 = (trim(ltrim($data_campos[44])) == 'LONGITUD')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_46 = (trim(ltrim($data_campos[45])) == 'ALTURA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_47 = (trim(ltrim($data_campos[46])) == 'ANCHO')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_48 = (trim(ltrim($data_campos[47])) == 'RAZA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_49 = (trim(ltrim($data_campos[48])) == 'ESPECIE')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_50 = (trim(ltrim($data_campos[49])) == 'EDAD')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_51 = (trim(ltrim($data_campos[50])) == 'PAIS')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_52 = (trim(ltrim($data_campos[51])) == 'OTRAS_CARACT')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_53 = (trim(ltrim($data_campos[52])) == 'DESCRIPCION_UBICACION_BIEN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_54 = (trim(ltrim($data_campos[53])) == 'CAUSAL_BAJA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_55 = (trim(ltrim($data_campos[54])) == 'NRO_RESOLUCION_BAJA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_56 = (trim(ltrim($data_campos[55])) == 'FECHA_BAJA')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_57 = (trim(ltrim($data_campos[56])) == 'ACTO_DISPOSICION_BIEN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_58 = (trim(ltrim($data_campos[57])) == 'NRO_RESOLUCION_DISP')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_59 = (trim(ltrim($data_campos[58])) == 'FECHA_DISPOSICION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_60 = (trim(ltrim($data_campos[59])) == 'ENTIDAD_BENEFICIADA_ACTO_DISPOSICION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_61 = (trim(ltrim($data_campos[60])) == 'MOTIVO_ELIMINACION_BIEN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_62 = (trim(ltrim($data_campos[61])) == 'ACTO_ADMINISTRACION_BIEN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_63 = (trim(ltrim($data_campos[62])) == 'NUM_RESOLUCION_ADMINISTRACION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_64 = (trim(ltrim($data_campos[63])) == 'FECHA_ADMINISTRACION')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_65 = (trim(ltrim($data_campos[64])) == 'FECHA_VENC_ACTO_ADMIN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_66 = (trim(ltrim($data_campos[65])) == 'ENTIDAD_BENEFICIADA_ACTO_ADMIN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_67 = (trim(ltrim($data_campos[66])) == 'DOC_ALTA_SBN')? 'CORRECTO': '==> INCORRECTO';
			$rs_campo_68 = (trim(ltrim($data_campos[67])) == 'DOC_BAJA_SBN')? 'CORRECTO': '==> INCORRECTO';
			
		
		
			echo "
			  <table width='925' border='0' cellspacing='3' cellpadding='0'>
			  <tr>
				<td width='95'><B>Columna</B></td>
				<td width='365'><B>Cabecera Original (SBN)</B></td>
				<td width='365'><B>Cabecera Inventario (Excel)</B></td>
				<td width='100'><B>Estado</B></td>
			  </tr>
			  <tr><td>Columna 01</td> <td>ITEM</td> <td>$data_campos[0]</td> <td>$rs_campo_01</td> </tr>
			  <tr><td>Columna 02</td> <td>RUC_ENTIDAD</td> <td>$data_campos[1]</td> <td>$rs_campo_02</td> </tr>
			  <tr><td>Columna 03</td> <td>NOMBRE_LOCAL</td> <td>$data_campos[2]</td> <td>$rs_campo_03</td> </tr>
			  <tr><td>Columna 04</td> <td>DEPARTAMENTO</td> <td>$data_campos[3]</td> <td>$rs_campo_04</td> </tr>  
			  <tr><td>Columna 05</td> <td>PROVINCIA</td> <td>$data_campos[4]</td> <td>$rs_campo_05</td> </tr>
			  <tr><td>Columna 06</td> <td>DISTRITO</td> <td>$data_campos[5]</td> <td>$rs_campo_06</td> </tr>
			  <tr><td>Columna 07</td> <td>NOMBRE_AREA</td><td>$data_campos[6]</td> <td>$rs_campo_07</td> </tr>
			  <tr><td>Columna 08</td> <td>ABREVIATURA_AREA</td><td>$data_campos[7]</td> <td>$rs_campo_08</td> </tr>
			  <tr><td>Columna 09</td> <td>NOMBRE_OFICINA</td><td>$data_campos[8]</td> <td>$rs_campo_09</td> </tr>
			  <tr><td>Columna 10</td> <td>ABREVIATURA_OFICINA</td><td>$data_campos[9]</td> <td>$rs_campo_10</td> </tr>
			  <tr><td>Columna 11</td> <td>PISO_OFICINA</td><td>$data_campos[10]</td> <td>$rs_campo_11</td> </tr>
			  <tr><td>Columna 12</td> <td>TIPO_DOC_IDENTIDAD</td><td>$data_campos[11]</td> <td>$rs_campo_12</td> </tr>
			  <tr><td>Columna 13</td> <td>NRO_DOC_IDENT_PERSONAL</td><td>$data_campos[12]</td> <td>$rs_campo_13</td> </tr>
			  <tr><td>Columna 14</td> <td>APELLIDO_PATERNO</td><td>$data_campos[13]</td> <td>$rs_campo_14</td> </tr>  
			  <tr><td>Columna 15</td> <td>APELLIDO_MATERNO</td><td>$data_campos[14]</td> <td>$rs_campo_15</td> </tr>
			  <tr><td>Columna 16</td> <td>NOMBRES</td><td>$data_campos[15]</td> <td>$rs_campo_16</td> </tr>
			  <tr><td>Columna 17</td> <td>MODALIDAD_CONTRATO</td><td>$data_campos[16]</td> <td>$rs_campo_17</td> </tr>
			  <tr><td>Columna 18</td> <td>CODIGO_PATRIMONIAL</td><td>$data_campos[17]</td> <td>$rs_campo_18</td> </tr>
			  <tr><td>Columna 19</td> <td>DENOMINACION_BIEN</td><td>$data_campos[18]</td> <td>$rs_campo_19</td> </tr>
			  <tr><td>Columna 20</td> <td>TIPO_CAUSAL_ALTA</td><td>$data_campos[19]</td> <td>$rs_campo_20</td> </tr>
			  <tr><td>Columna 21</td> <td>NRO_DOC_ADQUISICION</td><td>$data_campos[20]</td> <td>$rs_campo_21</td> </tr>
			  <tr><td>Columna 22</td> <td>FECHA_ADQUISICION</td><td>$data_campos[21]</td> <td>$rs_campo_22</td> </tr>
			  <tr><td>Columna 23</td> <td>VALOR_ADQUISICION</td><td>$data_campos[22]</td> <td>$rs_campo_23</td> </tr>
			  <tr><td>Columna 24</td> <td>FECHA_DEPRECIACION</td><td>$data_campos[23]</td> <td>$rs_campo_24</td> </tr>
			  <tr><td>Columna 25</td> <td>VALOR_DEPREC_EJERCICIO</td><td>$data_campos[24]</td> <td>$rs_campo_25</td> </tr>

			  <tr><td>Columna 26</td> <td>VALOR_DEPREC_ACUMULADO</td><td>$data_campos[25]</td> <td>$rs_campo_26</td> </tr>
			  <tr><td>Columna 27</td> <td>VALOR_NETO</td><td>$data_campos[26]</td> <td>$rs_campo_27</td> </tr>
			  <tr><td>Columna 28</td><td>TIP_USO_CUENTA</td><td>$data_campos[27]</td> <td>$rs_campo_28</td> </tr>
			  <tr><td>Columna 29</td><td>TIPO_CUENTA</td><td>$data_campos[28]</td> <td>$rs_campo_29</td> </tr>
			  <tr><td>Columna 30</td><td>NRO_CUENTA_CONTABLE</td><td>$data_campos[29]</td> <td>$rs_campo_30</td> </tr>
			  <tr><td>Columna 31</td><td>CTA_CON_SEGURO</td><td>$data_campos[30]</td> <td>$rs_campo_31</td> </tr>
			  <tr><td>Columna 32</td><td>ESTADO_BIEN</td><td>$data_campos[31]</td> <td>$rs_campo_32</td> </tr>
			  <tr><td>Columna 33</td><td>CONDICION</td><td>$data_campos[32]</td> <td>$rs_campo_33</td> </tr>
			  <tr><td>Columna 34</td><td>MARCA</td><td>$data_campos[33]</td> <td>$rs_campo_34</td> </tr>
			  <tr><td>Columna 35</td><td>MODELO</td><td>$data_campos[34]</td> <td>$rs_campo_35</td> </tr>
			  <tr><td>Columna 36</td><td>TIPO</td><td>$data_campos[35]</td> <td>$rs_campo_36</td> </tr>
			  <tr><td>Columna 37</td><td>COLOR</td><td>$data_campos[36]</td> <td>$rs_campo_37</td> </tr>
			  <tr><td>Columna 38</td><td>SERIE</td><td>$data_campos[37]</td> <td>$rs_campo_38</td> </tr>
			  <tr><td>Columna 39</td><td>DIMENSION</td><td>$data_campos[38]</td> <td>$rs_campo_39</td> </tr>
			  <tr><td>Columna 40</td><td>PLACA</td><td>$data_campos[39]</td> <td>$rs_campo_40</td> </tr>
			  <tr><td>Columna 41</td><td>NRO_MOTOR</td><td>$data_campos[40]</td> <td>$rs_campo_41</td> </tr>
			  <tr><td>Columna 42</td><td>NRO_CHASIS</td><td>$data_campos[41]</td> <td>$rs_campo_42</td> </tr>
			  <tr><td>Columna 43</td><td>MATRICULA</td><td>$data_campos[42]</td> <td>$rs_campo_43</td> </tr>
			  <tr><td>Columna 44</td><td>ANIO_FABRICACION</td><td>$data_campos[43]</td> <td>$rs_campo_44</td> </tr>
			  <tr><td>Columna 45</td><td>LONGITUD</td><td>$data_campos[44]</td> <td>$rs_campo_45</td> </tr>
			  <tr><td>Columna 46</td><td>ALTURA</td><td>$data_campos[45]</td> <td>$rs_campo_46</td> </tr>
			  <tr><td>Columna 47</td><td>ANCHO</td><td>$data_campos[46]</td> <td>$rs_campo_47</td> </tr>
			  <tr><td>Columna 48</td><td>RAZA</td><td>$data_campos[47]</td> <td>$rs_campo_48</td> </tr>
			  <tr><td>Columna 49</td><td>ESPECIE</td><td>$data_campos[48]</td> <td>$rs_campo_49</td> </tr>
			  <tr><td>Columna 50</td><td>EDAD</td><td>$data_campos[49]</td> <td>$rs_campo_50</td> </tr>
			  <tr><td>Columna 51</td><td>PAIS</td><td>$data_campos[50]</td> <td>$rs_campo_51</td> </tr>
			  <tr><td>Columna 52</td><td>OTRAS_CARACT</td><td>$data_campos[51]</td> <td>$rs_campo_52</td> </tr>
			  <tr><td>Columna 53</td><td>DESCRIPCION_UBICACION_BIEN</td><td>$data_campos[52]</td> <td>$rs_campo_53</td> </tr>
			  <tr><td>Columna 54</td><td>CAUSAL_BAJA</td><td>$data_campos[53]</td> <td>$rs_campo_54</td> </tr>
			  <tr><td>Columna 55</td><td>NRO_RESOLUCION_BAJA</td><td>$data_campos[54]</td> <td>$rs_campo_55</td> </tr>
			  <tr><td>Columna 56</td><td>FECHA_BAJA</td><td>$data_campos[55]</td> <td>$rs_campo_56</td> </tr>
			  <tr><td>Columna 57</td><td>ACTO_DISPOSICION_BIEN</td><td>$data_campos[56]</td> <td>$rs_campo_57</td> </tr>
			  <tr><td>Columna 58</td><td>NRO_RESOLUCION_DISP</td><td>$data_campos[57]</td> <td>$rs_campo_58</td> </tr>
			  <tr><td>Columna 59</td><td>FECHA_DISPOSICION</td><td>$data_campos[58]</td> <td>$rs_campo_59</td> </tr>
			  <tr><td>Columna 60</td><td>ENTIDAD_BENEFICIADA_ACTO_DISPOSICION</td><td>$data_campos[59]</td> <td>$rs_campo_60</td> </tr>
			  <tr><td>Columna 61</td><td>MOTIVO_ELIMINACION_BIEN</td><td>$data_campos[60]</td> <td>$rs_campo_61</td> </tr>
			  <tr><td>Columna 62</td><td>ACTO_ADMINISTRACION_BIEN</td><td>$data_campos[61]</td> <td>$rs_campo_62</td> </tr>
			  <tr><td>Columna 63</td><td>NUM_RESOLUCION_ADMINISTRACION</td><td>$data_campos[62]</td> <td>$rs_campo_63</td> </tr>
			  <tr><td>Columna 64</td><td>FECHA_ADMINISTRACION</td><td>$data_campos[63]</td> <td>$rs_campo_64</td> </tr>
			  <tr><td>Columna 65</td><td>FECHA_VENC_ACTO_ADMIN</td><td>$data_campos[64]</td> <td>$rs_campo_65</td> </tr>
			  <tr><td>Columna 66</td><td>ENTIDAD_BENEFICIADA_ACTO_ADMIN</td><td>$data_campos[65]</td> <td>$rs_campo_66</td> </tr>
			  <tr><td>Columna 67</td><td>DOC_ALTA_SBN</td><td>$data_campos[66]</td> <td>$rs_campo_67</td> </tr>
			  <tr><td>Columna 68</td><td>DOC_BAJA_SBN</td><td>$data_campos[67]</td> <td>$rs_campo_68</td> </tr>
			  </table>
			";
			
		
			echo "<===================================================================================================================><br><br>";
	
				
			if( $rs_campo_01== '==> INCORRECTO' ||
				$rs_campo_02== '==> INCORRECTO' ||
				$rs_campo_03== '==> INCORRECTO' ||
				$rs_campo_04== '==> INCORRECTO' ||
				$rs_campo_05== '==> INCORRECTO' ||
				$rs_campo_06== '==> INCORRECTO' ||
				$rs_campo_07== '==> INCORRECTO' ||
				$rs_campo_08== '==> INCORRECTO' ||
				$rs_campo_09== '==> INCORRECTO' ||
				$rs_campo_10== '==> INCORRECTO' ||
				$rs_campo_11== '==> INCORRECTO' ||
				$rs_campo_12== '==> INCORRECTO' ||
				$rs_campo_13== '==> INCORRECTO' ||
				$rs_campo_14== '==> INCORRECTO' ||
				$rs_campo_15== '==> INCORRECTO' ||
				$rs_campo_16== '==> INCORRECTO' ||
				$rs_campo_17== '==> INCORRECTO' ||
				$rs_campo_18== '==> INCORRECTO' ||
				$rs_campo_19== '==> INCORRECTO' ||
				$rs_campo_20== '==> INCORRECTO' ||
				$rs_campo_21== '==> INCORRECTO' ||
				$rs_campo_22== '==> INCORRECTO' ||
				$rs_campo_23== '==> INCORRECTO' ||
				$rs_campo_24== '==> INCORRECTO' ||
				$rs_campo_25== '==> INCORRECTO' ||
				$rs_campo_26== '==> INCORRECTO' ||
				$rs_campo_27== '==> INCORRECTO' ||
				$rs_campo_28== '==> INCORRECTO' ||
				$rs_campo_29== '==> INCORRECTO' ||
				$rs_campo_30== '==> INCORRECTO' ||
				$rs_campo_31== '==> INCORRECTO' ||
				$rs_campo_32== '==> INCORRECTO' ||
				$rs_campo_33== '==> INCORRECTO' ||
				$rs_campo_34== '==> INCORRECTO' ||
				$rs_campo_35== '==> INCORRECTO' ||
				$rs_campo_36== '==> INCORRECTO' ||
				$rs_campo_37== '==> INCORRECTO' ||
				$rs_campo_38== '==> INCORRECTO' ||
				$rs_campo_39== '==> INCORRECTO' ||
				$rs_campo_40== '==> INCORRECTO' ||
				$rs_campo_41== '==> INCORRECTO' ||
				$rs_campo_42== '==> INCORRECTO' ||
				$rs_campo_43== '==> INCORRECTO' ||
				$rs_campo_44== '==> INCORRECTO' ||
				$rs_campo_45== '==> INCORRECTO' ||
				$rs_campo_46== '==> INCORRECTO' ||
				$rs_campo_47== '==> INCORRECTO' ||
				$rs_campo_48== '==> INCORRECTO' ||
				$rs_campo_49== '==> INCORRECTO' ||
				$rs_campo_50== '==> INCORRECTO' ||
				$rs_campo_51== '==> INCORRECTO' ||
				$rs_campo_52== '==> INCORRECTO' ||
				$rs_campo_53== '==> INCORRECTO' ||
				$rs_campo_54== '==> INCORRECTO' ||
				$rs_campo_55== '==> INCORRECTO' ||
				$rs_campo_56== '==> INCORRECTO' ||
				$rs_campo_57== '==> INCORRECTO' ||
				$rs_campo_58== '==> INCORRECTO' ||
				$rs_campo_59== '==> INCORRECTO' ||
				$rs_campo_60== '==> INCORRECTO' ||
				$rs_campo_61== '==> INCORRECTO' ||
				$rs_campo_62== '==> INCORRECTO' ||
				$rs_campo_63== '==> INCORRECTO' ||
				$rs_campo_64== '==> INCORRECTO' ||
				$rs_campo_65== '==> INCORRECTO' ||
				$rs_campo_66== '==> INCORRECTO' ||
				$rs_campo_67== '==> INCORRECTO' ||
				$rs_campo_68== '==> INCORRECTO'	){
			
				echo "<br><===================================================================================================================>";
				echo "<br>=> ERROR CON LOS CAMPOS DE LA CABECERA DEL EXCEL ";
				echo "<br><===================================================================================================================><BR>";
				
				return false;
					
			}

		}else{
				echo "<br><===================================================================================================================>";
				echo "<br>=> ERROR: LA CANTIDAD DE COLUMNAS DEL CUADRO EXCEL SOBREPASAN A LOS 68 COLUMNAS DEL LA PLANTILLA.";
				echo "<br><===================================================================================================================><BR>";
				echo '<BR><input type="button" name="BTN_RESULT" id="BTN_RESULT" value="Cerrar Ventana" onclick="Boton_Cerrar()" />';
				return false;
		}


		//if ($TotalColumnasConData == '68') {
			
			//================================================
			//=======  Crear tabla si no existe
			//================================================
			
			/*
			$data_cabecera_xls = $arrayData[0];
			$Total_columna_xls = sizeof($data_cabecera_xls);
			
			foreach ($data_cabecera_xls as $key => $campo) {
				$campos .=  $campo . ' varchar(255) NULL, ';
			}
			
			$campos = substr($campos,0,-2);			
			$create_table_repons = $oSimi_Importar_Excel_Muebles->create_excel_table($campos);
			
			echo $valor_Table = ($create_table_repons) ? 'SE CREO LA TABLA CORRECTAMENTE' : 'ERROR AL CREAR LA TABLA' ;
			*/
		//}
		
		/*
		if ($TotalColumnasConData == '68') {
			
			//=============== NOMBRE DE LA CABECERA
			$data_cabecera_xls = $arrayData[0];
			foreach ($data_cabecera_xls as $key => $campo) {
				$campos_excel .=  $campo . ', ';
			}
			
			$campos_excel = "COD_IMPORT_EXCEL_MUEBLE, COD_IMPORT_EXCEL_FILE, COD_ENTIDAD_PROCESA, USUARIO_PROCESA, FECHA_PROCESA, ID_PREDIO_SBN, ".substr($campos_excel,0,-2);
			
			
			
			//=============== VALOR DE LOS ITEMS
			
			unset($arrayData[0]);//ELIMINAMOS LA CABECERA
			foreach ($arrayData as $key2 => $rows_items) {
				
				foreach ($rows_items as $key2 => $value) {
					$values = $values."'".$value . "', ";
				}
				
				
				$Result_Codigo = $oSimi_Importar_Excel_Muebles->Generar_Codigo_TBL_MUEBLES_IMPORTAR_EXCEL_MUEBLE();
				$COD_IMPORT_EXCEL_MUEBLE	= 	odbc_result($Result_Codigo,"CODIGO");
				
				$values = " '$COD_IMPORT_EXCEL_MUEBLE', '$COD_IMPORT_EXCEL_FILE', '$COD_ENTIDAD_PROCESA', '$USUARIO_PROCESA', GETDATE(), '$ID_PREDIO_SBN', ".substr($values,0,-2);
								
				$insert_table_repons = $oSimi_Importar_Excel_Muebles->Insertar_Registro_to_Tabla_Excel_Muebles_x_Local($campos_excel, $values);
				
				if($insert_table_repons){
					unset($values);
				}else{
					return false;
				}
				
				
			}
			
		}
		
		*/


		if ($TotalColumnasConData == '68') {

			//$arrayData 				= $worksheet->toArray();
			//$data_campos 			= $arrayData[0];
			
			
			
			//unset($arrayData[0]);
			
			
			for ($row = 1; $row < $TotalFilas; ++ $row) {

				//for ($col = 0; $col < $TotalColumnasConData; ++ $col) {

					$ITEM 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][0]))));
					$RUC_ENTIDAD 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][1]))));
					$NOMBRE_LOCAL 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][2]))));
					$DEPARTAMENTO 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][3]))));
					$PROVINCIA 				= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][4]))));
					$DISTRITO 				= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][5]))));
					$NOMBRE_AREA 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][6]))));
					$ABREVIATURA_AREA 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][7]))));
					$NOMBRE_OFICINA 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][8]))));
					$ABREVIATURA_OFICINA 	= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][9]))));
					$PISO_OFICINA 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][10]))));
					$TIPO_DOC_IDENTIDAD 	= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][11]))));
					$NRO_DOC_IDENT_PERSONAL = convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][12]))));
					$APELLIDO_PATERNO 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][13]))));
					$APELLIDO_MATERNO 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][14]))));
					$NOMBRES 				= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][15]))));
					$MODALIDAD_CONTRATO 	= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][16]))));
					$CODIGO_PATRIMONIAL 	= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][17]))));
					$DENOMINACION_BIEN 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][18]))));
					$TIPO_CAUSAL_ALTA 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][19]))));
					$NRO_DOC_ADQUISICION 	= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][20]))));
					$FECHA_ADQUISICION 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][21]))));
					$VALOR_ADQUISICION 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][22]))));
					$FECHA_DEPRECIACION 	= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][23]))));
					$VALOR_DEPREC_EJERCICIO = convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][24]))));
					$VALOR_DEPREC_ACUMULADO = convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][25]))));
					$VALOR_NETO 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][26]))));
					$TIP_USO_CUENTA 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][27]))));
					$TIPO_CUENTA 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][28]))));
					$NRO_CUENTA_CONTABLE 	= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][29]))));
					$CTA_CON_SEGURO 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][30]))));
					$ESTADO_BIEN 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][31]))));
					$CONDICION 				= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][32]))));
					$MARCA 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][33]))));
					$MODELO 				= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][34]))));
					$TIPO 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][35]))));
					$COLOR 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][36]))));
					$SERIE 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][37]))));
					$DIMENSION 				= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][38]))));
					$PLACA 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][39]))));
					$NRO_MOTOR 				= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][40]))));
					$NRO_CHASIS 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][41]))));
					$MATRICULA 				= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][42]))));
					$ANIO_FABRICACION 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][43]))));
					$LONGITUD 				= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][44]))));
					$ALTURA 				= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][45]))));
					$ANCHO 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][46]))));
					$RAZA 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][47]))));
					$ESPECIE 				= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][48]))));
					$EDAD 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][49]))));
					$PAIS 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][50]))));
					$OTRAS_CARACT 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][51]))));
					$DESCRIPCION_UBICACION_BIEN 	= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][52]))));
					$CAUSAL_BAJA 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][53]))));
					$NRO_RESOLUCION_BAJA 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][54]))));
					$FECHA_BAJA 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][55]))));
					$ACTO_DISPOSICION_BIEN 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][56]))));
					$NRO_RESOLUCION_DISP			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][57]))));
					$FECHA_DISPOSICION 				= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][58]))));
					$ENTIDAD_BENEFICIADA_ACTO_DISPOSICION = convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][59]))));
					$MOTIVO_ELIMINACION_BIEN 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][60]))));
					$ACTO_ADMINISTRACION_BIEN 		= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][61]))));
					$NUM_RESOLUCION_ADMINISTRACION 	= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][62]))));
					$FECHA_ADMINISTRACION 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][63]))));
					$FECHA_VENC_ACTO_ADMIN 			= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][64]))));
					$ENTIDAD_BENEFICIADA_ACTO_ADMIN = convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][65]))));
					$DOC_ALTA_SBN 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][66]))));
					$DOC_BAJA_SBN 					= convertir_a_utf8(strtoupper(ltrim(rtrim($arrayData[$row][67]))));
					
					//========== Validacion Campo ITEM
					
					if($ITEM!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo ITEM esta vacio.";
						return false;
					}
					
					if(is_numeric($ITEM)){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo ITEM no es numerico.";
						return false;
					}
					
					//========== Validacion Campo RUC_ENTIDAD
					
					if($RUC_ENTIDAD!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo RUC_ENTIDAD esta vacio.";
						return false;
					}
					
					if(is_numeric($RUC_ENTIDAD)){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo RUC_ENTIDAD no es numérico.";
						return false;
					}
					
					if(strlen($RUC_ENTIDAD)=='11'){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo RUC_ENTIDAD no tiene 11 digitos.";
						return false;
					}
					
					
					//========== Validacion Campo NOMBRE_LOCAL
					
					if($NOMBRE_LOCAL!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo NOMBRE_LOCAL esta vacio.";
						return false;
					}
					
					//========== Validacion Campo DEPARTAMENTO
					
					if($DEPARTAMENTO!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo DEPARTAMENTO esta vacio.";
						return false;
					}
					
					//========== Validacion Campo PROVINCIA
					
					if($PROVINCIA!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo PROVINCIA esta vacio.";
						return false;
					}
					
					//========== Validacion Campo DISTRITO
					
					if($DISTRITO!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo DISTRITO esta vacio.";
						return false;
					}
					
					//========== Validacion Campo NOMBRE_AREA
					
					if($NOMBRE_AREA!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo NOMBRE_AREA esta vacio.";
						return false;
					}
					
					//========== Validacion Campo NOMBRE_OFICINA
					
					if($NOMBRE_OFICINA!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo NOMBRE_OFICINA esta vacio.";
						return false;
					}
					
					
					//========== Validacion Campo PISO_OFICINA
					
					if($PISO_OFICINA!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo PISO_OFICINA esta vacio.";
						return false;
					}
					
					//========== Validacion Campo TIPO_DOC_IDENTIDAD
					
					if($TIPO_DOC_IDENTIDAD!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo TIPO_DOC_IDENTIDAD esta vacio.";
						return false;
					}
					
					if($TIPO_DOC_IDENTIDAD=='1' || $TIPO_DOC_IDENTIDAD=='2'){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> El valor del campo TIPO_DOC_IDENTIDAD no corresponde al tipo del documento DNI o Carnet de Extranjería.";
						return false;
					}
					
					//========== Validacion Campo NRO_DOC_IDENT_PERSONAL
					
					if($NRO_DOC_IDENT_PERSONAL!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo NRO_DOC_IDENT_PERSONAL esta vacio.";
						return false;
					}
					
					
					if($TIPO_DOC_IDENTIDAD=='1' && strlen($NRO_DOC_IDENT_PERSONAL)!=8 ){					
						echo "<br>Error: Item Nro $row ==> Campo strlen($NRO_DOC_IDENT_PERSONAL) no corresponde al tipo del documento DNI.";
						return false;
					}
					
					if($TIPO_DOC_IDENTIDAD=='2' && strlen($NRO_DOC_IDENT_PERSONAL)<12 ){
						echo "<br>Error: Item Nro $row ==> Campo NRO_DOC_IDENT_PERSONAL no corresponde al tipo del documento Carnet de Extranjería.";
						return false;
					}
					
					
					//========== Validacion Campo APELLIDO_PATERNO
					
					if($APELLIDO_PATERNO!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo APELLIDO_PATERNO esta vacio.";
						return false;
					}
					
					
					//========== Validacion Campo APELLIDO_MATERNO
					
					if($APELLIDO_MATERNO!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo APELLIDO_MATERNO esta vacio.";
						return false;
					}
					
					
					//========== Validacion Campo NOMBRES
					
					if($NOMBRES!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo NOMBRES esta vacio.";
						return false;
					}
					
					//========== Validacion Campo MODALIDAD_CONTRATO
					
					if($MODALIDAD_CONTRATO!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo MODALIDAD_CONTRATO esta vacio.";
						return false;
					}
					
					if($MODALIDAD_CONTRATO=="C" || $MODALIDAD_CONTRATO=="F" || $MODALIDAD_CONTRATO=="P" || $MODALIDAD_CONTRATO=="S" || $MODALIDAD_CONTRATO=="E"){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> El valor del campo MODALIDAD_CONTRATO no corresponde al tipo de modalidad del contrato.";
						return false;
					}
					

					//========== Validacion Campo CODIGO_PATRIMONIAL
					
					if($CODIGO_PATRIMONIAL!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo CODIGO_PATRIMONIAL esta vacio.";
						return false;
					}
					
					
					if(strlen($CODIGO_PATRIMONIAL)=='12' || strlen($CODIGO_PATRIMONIAL)=='11'){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo CODIGO_PATRIMONIAL no tiene 12 digitos.";
						return false;
					}
					
					//========== Validacion Campo TIPO_CAUSAL_ALTA
					
					if($TIPO_CAUSAL_ALTA!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo TIPO_CAUSAL_ALTA esta vacio.";
						return false;
					}
					
					if($TIPO_CAUSAL_ALTA=="C" || $TIPO_CAUSAL_ALTA=="D" || $TIPO_CAUSAL_ALTA=="F" || $TIPO_CAUSAL_ALTA=="P" || $TIPO_CAUSAL_ALTA=="R" || $TIPO_CAUSAL_ALTA=="E" || $TIPO_CAUSAL_ALTA=="S" || $TIPO_CAUSAL_ALTA=="O"){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> El valor del campo $TIPO_CAUSAL_ALTA no corresponde al tipo de causal de alta.";
						return false;
					}
					
					//========== Validacion Campo NRO_DOC_ADQUISICION
					
					if($NRO_DOC_ADQUISICION!=''){ 					
					}else{
						echo "<br>Error: Item Nro $row ==> Campo NRO_DOC_ADQUISICION esta vacio.";
						return false;
					}
					
					//========== Validacion Campo FECHA_ADQUISICION
					
					/*
					if($FECHA_ADQUISICION!=''){ 	
						
						echo "<br>".$FECHA_ADQUISICION;

						$trozos = explode ("/", trim($FECHA_ADQUISICION));						
						$dia=$trozos[1]; 
						$mes=$trozos[0]; 
						$año=$trozos[2]; 
						
						if(checkdate ($mes,$dia,$año)){ 
							//return true; 
						}else{ 
							echo "<br>Error: Item Nro $row ==> Campo $FECHA_ADQUISICION no tiene el formato fecha dd/mm/aaaa.";
							return false; 
						}
									
					}else{
						echo "<br>Error: Item Nro $row ==> Campo FECHA_ADQUISICION esta vacio.";
						return false;
					}
					*/
					
					
                     
                 
					
					
					
					$Result_Codigo = $oSimi_Importar_Excel_Muebles->Generar_Codigo_TBL_MUEBLES_IMPORTAR_EXCEL_MUEBLE();
					$COD_IMPORT_EXCEL_MUEBLE	= 	odbc_result($Result_Codigo,"CODIGO");
					
					
					$COD_IMPORT_EXCEL_MUEBLE_OK = " '$COD_IMPORT_EXCEL_MUEBLE' ";
					$COD_IMPORT_EXCEL_FILE_OK = " '$COD_IMPORT_EXCEL_FILE' ";
					$COD_ENTIDAD_PROCESA_OK = " '$COD_ENTIDAD_PROCESA' ";
					$USUARIO_PROCESA_OK = " '$USUARIO_PROCESA' ";
					$FECHA_PROCESA_OK = " GETDATE() ";
					$ID_PREDIO_SBN_OK = " '$ID_PREDIO_SBN' ";
					$ITEM_OK = " '$ITEM' ";
					$RUC_ENTIDAD_OK = " '$RUC_ENTIDAD' ";
					$NOMBRE_LOCAL_OK = " '$NOMBRE_LOCAL' ";
					$DEPARTAMENTO_OK = " '$DEPARTAMENTO' ";
					$PROVINCIA_OK = " '$PROVINCIA' ";
					$DISTRITO_OK = " '$DISTRITO' ";
					$NOMBRE_AREA_OK = " '$NOMBRE_AREA' ";
					$ABREVIATURA_AREA_OK = " '$ABREVIATURA_AREA' ";
					$NOMBRE_OFICINA_OK = " '$NOMBRE_OFICINA' ";
					$ABREVIATURA_OFICINA_OK = " '$ABREVIATURA_OFICINA' ";
					$PISO_OFICINA_OK = " '$PISO_OFICINA' ";
					$TIPO_DOC_IDENTIDAD_OK = " '$TIPO_DOC_IDENTIDAD' ";
					$NRO_DOC_IDENT_PERSONAL_OK = " '$NRO_DOC_IDENT_PERSONAL' ";
					$APELLIDO_PATERNO_OK = " '$APELLIDO_PATERNO' ";
					$APELLIDO_MATERNO_OK = " '$APELLIDO_MATERNO' ";
					$NOMBRES_OK = " '$NOMBRES' ";
					$MODALIDAD_CONTRATO_OK = " '$MODALIDAD_CONTRATO' ";
					$CODIGO_PATRIMONIAL_OK = " '$CODIGO_PATRIMONIAL' ";
					$DENOMINACION_BIEN_OK = " '$DENOMINACION_BIEN' ";
					$TIPO_CAUSAL_ALTA_OK = " '$TIPO_CAUSAL_ALTA' ";
					$NRO_DOC_ADQUISICION_OK = " '$NRO_DOC_ADQUISICION' ";
					$FECHA_ADQUISICION_OK = " '$FECHA_ADQUISICION' ";
					$VALOR_ADQUISICION_OK = " '$VALOR_ADQUISICION' ";
					$FECHA_DEPRECIACION_OK = " '$FECHA_DEPRECIACION' ";
					$VALOR_DEPREC_EJERCICIO_OK = " '$VALOR_DEPREC_EJERCICIO' ";
					$VALOR_DEPREC_ACUMULADO_OK = " '$VALOR_DEPREC_ACUMULADO' ";
					$VALOR_NETO_OK = " '$VALOR_NETO' ";
					$TIP_USO_CUENTA_OK = " '$TIP_USO_CUENTA' ";
					$TIPO_CUENTA_OK = " '$TIPO_CUENTA' ";
					$NRO_CUENTA_CONTABLE_OK = " '$NRO_CUENTA_CONTABLE' ";
					$CTA_CON_SEGURO_OK = " '$CTA_CON_SEGURO' ";
					$ESTADO_BIEN_OK = " '$ESTADO_BIEN' ";
					$CONDICION_OK = " '$CONDICION' ";
					$MARCA_OK = " '$MARCA' ";
					$MODELO_OK = " '$MODELO' ";
					$TIPO_OK = " '$TIPO' ";
					$COLOR_OK = " '$COLOR' ";
					$SERIE_OK = " '$SERIE' ";
					$DIMENSION_OK = " '$DIMENSION' ";
					$PLACA_OK = " '$PLACA' ";
					$NRO_MOTOR_OK = " '$NRO_MOTOR' ";
					$NRO_CHASIS_OK = " '$NRO_CHASIS' ";
					$MATRICULA_OK = " '$MATRICULA' ";
					$ANIO_FABRICACION_OK = " '$ANIO_FABRICACION' ";
					$LONGITUD_OK = " '$LONGITUD' ";
					$ALTURA_OK = " '$ALTURA' ";
					$ANCHO_OK = " '$ANCHO' ";
					$RAZA_OK = " '$RAZA' ";
					$ESPECIE_OK = " '$ESPECIE' ";
					$EDAD_OK = " '$EDAD' ";
					$PAIS_OK = " '$PAIS' ";
					$OTRAS_CARACT_OK = " '$OTRAS_CARACT' ";
					$DESCRIPCION_UBICACION_BIEN_OK = " '$DESCRIPCION_UBICACION_BIEN' ";
					$CAUSAL_BAJA_OK = " '$CAUSAL_BAJA' ";
					$NRO_RESOLUCION_BAJA_OK = " '$NRO_RESOLUCION_BAJA' ";
					$FECHA_BAJA_OK = " '$FECHA_BAJA' ";
					$ACTO_DISPOSICION_BIEN_OK = " '$ACTO_DISPOSICION_BIEN' ";
					$NRO_RESOLUCION_DISP_OK = " '$NRO_RESOLUCION_DISP' ";
					$FECHA_DISPOSICION_OK = " '$FECHA_DISPOSICION' ";
					$ENTIDAD_BENEFICIADA_ACTO_DISPOSICION_OK = " '$ENTIDAD_BENEFICIADA_ACTO_DISPOSICION' ";
					$MOTIVO_ELIMINACION_BIEN_OK = " '$MOTIVO_ELIMINACION_BIEN' ";
					$ACTO_ADMINISTRACION_BIEN_OK = " '$ACTO_ADMINISTRACION_BIEN' ";
					$NUM_RESOLUCION_ADMINISTRACION_OK = " '$NUM_RESOLUCION_ADMINISTRACION' ";
					$FECHA_ADMINISTRACION_OK = " '$FECHA_ADMINISTRACION' ";
					$FECHA_VENC_ACTO_ADMIN_OK = " '$FECHA_VENC_ACTO_ADMIN' ";
					$ENTIDAD_BENEFICIADA_ACTO_ADMIN_OK = " '$ENTIDAD_BENEFICIADA_ACTO_ADMIN' ";
					$DOC_ALTA_SBN_OK = " '$DOC_ALTA_SBN' ";
					$DOC_BAJA_SBN_OK = " '$DOC_BAJA_SBN' ";

					
					$Insert_Table_File = $oSimi_Importar_Excel_Muebles->Insertar_Registro_to_Tabla_Excel_Muebles_x_Local_Directo($COD_IMPORT_EXCEL_MUEBLE_OK, $COD_IMPORT_EXCEL_FILE_OK, $COD_ENTIDAD_PROCESA_OK, $USUARIO_PROCESA_OK, $FECHA_PROCESA_OK, $ID_PREDIO_SBN_OK, $ITEM_OK, $RUC_ENTIDAD_OK, $NOMBRE_LOCAL_OK, $DEPARTAMENTO_OK, $PROVINCIA_OK, $DISTRITO_OK, $NOMBRE_AREA_OK, $ABREVIATURA_AREA_OK, $NOMBRE_OFICINA_OK, $ABREVIATURA_OFICINA_OK, $PISO_OFICINA_OK, $TIPO_DOC_IDENTIDAD_OK, $NRO_DOC_IDENT_PERSONAL_OK, $APELLIDO_PATERNO_OK, $APELLIDO_MATERNO_OK, $NOMBRES_OK, $MODALIDAD_CONTRATO_OK, $CODIGO_PATRIMONIAL_OK, $DENOMINACION_BIEN_OK, $TIPO_CAUSAL_ALTA_OK, $NRO_DOC_ADQUISICION_OK, $FECHA_ADQUISICION_OK, $VALOR_ADQUISICION_OK, $FECHA_DEPRECIACION_OK, $VALOR_DEPREC_EJERCICIO_OK, $VALOR_DEPREC_ACUMULADO_OK, $VALOR_NETO_OK, $TIP_USO_CUENTA_OK, $TIPO_CUENTA_OK, $NRO_CUENTA_CONTABLE_OK, $CTA_CON_SEGURO_OK, $ESTADO_BIEN_OK, $CONDICION_OK, $MARCA_OK, $MODELO_OK, $TIPO_OK, $COLOR_OK, $SERIE_OK, $DIMENSION_OK, $PLACA_OK, $NRO_MOTOR_OK, $NRO_CHASIS_OK, $MATRICULA_OK, $ANIO_FABRICACION_OK, $LONGITUD_OK, $ALTURA_OK, $ANCHO_OK, $RAZA_OK, $ESPECIE_OK, $EDAD_OK, $PAIS_OK, $OTRAS_CARACT_OK, $DESCRIPCION_UBICACION_BIEN_OK, $CAUSAL_BAJA_OK, $NRO_RESOLUCION_BAJA_OK, $FECHA_BAJA_OK, $ACTO_DISPOSICION_BIEN_OK, $NRO_RESOLUCION_DISP_OK, $FECHA_DISPOSICION_OK, $ENTIDAD_BENEFICIADA_ACTO_DISPOSICION_OK, $MOTIVO_ELIMINACION_BIEN_OK, $ACTO_ADMINISTRACION_BIEN_OK, $NUM_RESOLUCION_ADMINISTRACION_OK, $FECHA_ADMINISTRACION_OK, $FECHA_VENC_ACTO_ADMIN_OK, $ENTIDAD_BENEFICIADA_ACTO_ADMIN_OK, $DOC_ALTA_SBN_OK, $DOC_BAJA_SBN_OK );
					
					$contador_OK++;
				
					if($Insert_Table_File){
						
						$RESULTADO_MIGRACION = 'OK';
						//unset($values);
					}else{
						$RESULTADO_MIGRACION = 'MAL';
						return false;
					}
					
					/*
					$cell = $worksheet->getCellByColumnAndRow($col, $row);
					$val = $cell->getValue();
					$dataType = PHPExcel_Cell_DataType::dataTypeForValue($val);
					//echo '<td>' . $val . '<br>(Typ ' . $dataType . ')</td>';
					echo '<td>' . htmlentities($val) . '</td>';
					*/
					
				//}
			}
			
			if($RESULTADO_MIGRACION == 'OK'){
				echo "<BR>=================>   SE MIGRO CORRECTAMENTE ";
				echo "<BR>=================>   [ ".$contador_OK." REGISTROS ] MIGRADOS";
			}

		}
		
		
		if($TotalFilas_SinCAB == $contador_OK){
			$RESULT_Proceso	=	$oSimi_Importar_Excel_Muebles->Actualiza_Estado_Ejecucion_Proceso_Excel($COD_IMPORT_EXCEL_FILE, $contador_OK);
		}
		
		echo '<BR><input type="button" name="BTN_RESULT" id="BTN_RESULT" value="Cerrar Ventana" onclick="Boton_Cerrar()" />';
		
	}



}else if( $operacion == 'Ejecuta_Finalizar_Inventario'){

	$COD_IMPORT_EXCEL_FILE	=	$_REQUEST['sCOD_IMPORT_EXCEL_FILE_XLS'];
	$COD_PREDIO	=	$_REQUEST['sID_PREDIO_SBN_Local'];
	$COD_ENTIDAD	=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];

	$RESULTADO_FINALIZAR	=	$oSimi_Importar_Excel_Muebles->Procesar_Finalizar_Inventario_MUEBLE_XLS($COD_ENTIDAD, $COD_PREDIO, $COD_IMPORT_EXCEL_FILE);
	$CONDICION		= 	odbc_result($RESULTADO_FINALIZAR,"CONDICION");
	$TOT_EXCEL		= 	odbc_result($RESULTADO_FINALIZAR,"TOT_EXCEL");
	$TOT_MIGRADO	= 	odbc_result($RESULTADO_FINALIZAR,"TOT_MIGRADO");
	
	if($RESULTADO_FINALIZAR){
		
		if($CONDICION == 'x100'){		
		
			//$RESULTADO_FINALIZAR	=	$oSimi_Importar_Excel_Muebles->Actualiza_Estado_Finalizar_Inventario_Excel($COD_IMPORT_EXCEL_FILE, $TOT_MIGRADO);		
			$valor = '1'.'[*]'.$CONDICION.'[*]'.$TOT_EXCEL.'[*]'.$TOT_MIGRADO;
		
		}elseif($CONDICION == 'x0'){
			$valor = '0'.'[*]'.$CONDICION.'[*]'.$TOT_EXCEL.'[*]'.$TOT_MIGRADO;
			
		}else{
			$valor = '0'.'[*]'.'---'.'[*]'.$TOT_EXCEL.'[*]'.$TOT_MIGRADO;
		}
	
	}
		
	echo $valor;
	
}else if($operacion == 'Eliminar_Cuadro_Excel'){
			
	$COD_IMPORT_EXCEL_FILE 		= 	$_REQUEST['sCOD_IMPORT_EXCEL_FILE'];
	$USUARIO_ELIMINA 			= 	$_REQUEST['TXH_SIMI_COD_USUARIO'];
	$COD_ENTIDAD				=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];

	//echo 'aaaa' . $COD_ENTIDAD;
		
	$RESULT_1	=	$oSimi_Importar_Excel_Muebles->Eliminar_Archivo_Excel_x_Codigo($COD_IMPORT_EXCEL_FILE, $USUARIO_ELIMINA, $COD_ENTIDAD);
	$valor = ($RESULT_1)? '1':'0';
	$RESULT_2	=	$oSimi_Importar_Excel_Muebles->Eliminar_Bienes_Cargados($COD_ENTIDAD, $COD_IMPORT_EXCEL_FILE);
	$valor = ($RESULT_2)? '1':'0'; 
 
	echo $valor;
	
}else if($operacion=='Enviar_FILE_EXCEL_a_SBN'){
		
	$COD_IMPORT_EXCEL_FILE 	=	$_REQUEST['sCOD_IMPORT_EXCEL_FILE'];
	$COD_ENTIDAD 			=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];
	$SIMI_USUARIO_CREACION 	=	$_REQUEST['TXH_SIMI_COD_USUARIO'];
	
		
	$RESULT_1	=	$oSimi_Importar_Excel_Muebles->Enviar_Excel_a_SBN($COD_IMPORT_EXCEL_FILE);

	if($RESULT_1){
		$oSimi_Importar_Excel_Muebles->Enviar_Bienes_Muebles_a_SBN_EXCEL($COD_IMPORT_EXCEL_FILE, $COD_ENTIDAD, $SIMI_USUARIO_CREACION);
	}
	
	$valor = ($RESULT_1)? '1':'0';
	
	echo $valor;
	
}else if($operacion=='Finalizar_Todo_Proceso_Inventario'){
		
	$TXH_SIMI_COD_ENTIDAD 	=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];
	$TXH_SIMI_COD_USUARIO 	=	$_REQUEST['TXH_SIMI_COD_USUARIO'];
		
	$RESULT_1	=	$oSimi_Importar_Excel_Muebles->Enviar_Excel_a_SBN($sCOD_IMPORT_EXCEL_FILE);
	$valor 		=	($RESULT_1)? '1':'0';
	
	echo $valor;
	
}else if($operacion=='Reg_Sin_Bienes'){
		
	$COD_IMPORT_EXCEL_RESP 	=	$_REQUEST['TXH_COD_IMPORT_EXCEL_RESP'];
	$COD_ENTIDAD 			=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];
	$COD_USUARIO 			=	$_REQUEST['TXH_SIMI_COD_USUARIO'];
	$ID_PREDIO_SBN 			=	$_REQUEST['sID_PREDIO_SBN_LOCAL'];
	$COD_INVENTARIO 		=	$_REQUEST['txh_migra_cod_inventario'];
	
	$ARCHIVO_NOMBRE_GENERADO = 'El local y/o predio no cuenta con bienes muebles.';
	$ARCHIVO_PESO			 = '0';
	
	$RESULT		=	$oSimi_Importar_Excel_Muebles->Simi_Generar_Codigo_Importar_Datos_Excel_Muebles();		
	$COD_IMPORT_EXCEL_FILE	= 	odbc_result($RESULT,"CODIGO");
			
		
	$RESULT_1	=	$oSimi_Importar_Excel_Muebles->Adjuntar_Excel_Importar_Datos_Excel_Muebles($COD_IMPORT_EXCEL_FILE, $COD_IMPORT_EXCEL_RESP, $COD_ENTIDAD, $ARCHIVO_NOMBRE_GENERADO, $ARCHIVO_PESO, $COD_USUARIO, $COD_INVENTARIO, $ID_PREDIO_SBN);
	
	$oSimi_Importar_Excel_Muebles->Simi_Registrar_Local_Sin_Bienes($COD_IMPORT_EXCEL_FILE, $COD_ENTIDAD, $ID_PREDIO_SBN);
	
	$valor 		=	($RESULT_1)? '1':'0';
	
	echo $valor;
	
}





?>