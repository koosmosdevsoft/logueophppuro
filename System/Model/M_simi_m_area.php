<?
$operacion	=	$_GET['operacion'];
require_once('../Controller/C_simi_m_area.php');
require_once('../../utils/funciones/funciones.php');
require_once('../../utils/funciones/clases.php');
require_once("../Controller/BaseBD_PDO.php");
require_once('../Model/BaseBD_PDOModel.php');
require_once('../Controller/BienPatrimonialController.php');
require_once('../Model/BienPatrimonialModel.php');

$validaciones	=	new Clases;
$oSimi_UE_Area = new Simi_UE_Area;
$objModel = new BienPatrimonialModel();

if($operacion=='Guardar_Area'){
	
	if (isset($_REQUEST['txt_cod_area_x1'])) 
	{
    	$txt_cod_area_x1 = $_REQUEST['txt_cod_area_x1'];
	}else
	{
		$txt_cod_area_x1 = "";
	}

	if (isset($_REQUEST['txt_siglas_x1'])) 
	{
    	$SIGLAS_AREA = $_REQUEST['txt_siglas_x1'];
	}else
	{
		$SIGLAS_AREA = "";
	}
	
	$ID_PREDIO_SBN 		=	$_REQUEST['TXH_ID_PREDIO_SBN'];
	$DESC_AREA 			= 	convertir_a_utf8($_REQUEST['txt_nom_area_x1']);
	$COD_USUARIO 		=	$_REQUEST['TXH_SIMI_COD_USUARIO'];
	
	/* VALIDACIONES */
	/*
	if ($this->validar_caracteresEspeciales($DENOMINACION_PREDIO)) {
		$error[] = 'El campo Nombre de Local contiene caracteres no admitidos.';
		$valor = 2;
	}
	*/
	if (!$validaciones->validar_entero($DESC_AREA)) {
		$error[] = 'El campo de Numero debe ser un número.';
		$valor = 2;
	}
	/**** FIN DE VALIDACIONES ****/
	
	if (isset($error)) {
		$valor = 2;
	}else{

		if($txt_cod_area_x1 == ''){

			/* CREACION:        WILLIAMS ARENAS */
			/* FECHA CREACION:  09-06-2020 */
			/* DESCRIPCION:     REGISTRO DE AREA */  
			$dataModel_01 = [
				'ID_PREDIO_SBN'	=>$ID_PREDIO_SBN,
				'DESC_AREA'		=>$DESC_AREA,
				'SIGLAS_AREA'	=>$SIGLAS_AREA,
				'COD_USUARIO'	=>$COD_USUARIO,
			];
			
			$RESULTADO = $objModel->Insertar_Simi_UE_Area_PDO($dataModel_01);
			$valor = ($RESULTADO>=0) ? '1':'0';
			/**** FIN DE REGISTRO DE AREA ****/


		}else{
			$COD_UE_AREA		=	$txt_cod_area_x1;

			/* CREACION:        WILLIAMS ARENAS */
			/* FECHA CREACION:  09-06-2020 */
			/* DESCRIPCION:     ACTUALIZA AREA */  
			$dataModel_01 = [
				'COD_UE_AREA'	=>$COD_UE_AREA,
				'ID_PREDIO_SBN'	=>$ID_PREDIO_SBN,
				'DESC_AREA'		=>$DESC_AREA,
				'SIGLAS_AREA'	=>$SIGLAS_AREA,
				'COD_USUARIO'	=>$COD_USUARIO,
			];
			
			$RESULTADO = $objModel->Actualizar_Simi_UE_Area_PDO($dataModel_01);
			$valor = ($RESULTADO>=0) ? '1':'0';
			/**** FIN DE ACTUALIZA AREA ****/

		}	
		$error[] = '';
	}
	
	echo json_encode(array('valor'=>$valor,'errores'=>$error));
	
}else if($operacion=='Eliminar_Area'){
	
			
	$COD_UE_AREA 		=	$_REQUEST['x_COD_AREA_A01'];
	$USUARIO_ELIMINA 	=	$_REQUEST['TXH_SIMI_COD_USUARIO'];
	$COD_ENTIDAD 	=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];
/*
	$RESULTADO			=	$oSimi_UE_Area->Eliminar_Area_Validacion( $COD_ENTIDAD, $COD_UE_AREA );
	
	$CANTIDAD			= 	odbc_result($RESULTADO,"CANTIDAD");

	$valor = ($CANTIDAD == '0') ? '1' : '2';
*/

	/* CREACION:        WILLIAMS ARENAS */
	/* FECHA CREACION:  09-06-2020 */
	/* DESCRIPCION:     VALIDACION PARA ELIMINAR AREA */  
	$dataModel_01 = [
		'COD_ENTIDAD'		=>$COD_ENTIDAD,
		'COD_UE_AREA'		=>$COD_UE_AREA
	];
	
	$data['DataValidacion'] = $objModel->Eliminar_Area_Validacion_PDO($dataModel_01);
	$CANTIDAD = $data['DataValidacion'][0]['CANTIDAD'];
	$valor = ($CANTIDAD == '0') ? '1' : '2';
	/**** FIN DE VALIDACION PARA ELIMINAR AREA ****/

	if( $valor == 1 ){
	/*
		$RESULTADO				=	$oSimi_UE_Area->Eliminar_Area($COD_UE_AREA, $USUARIO_ELIMINA);
		$valor = ($RESULTADO) ? 1 : 0;
	*/

	/* CREACION:        WILLIAMS ARENAS */
	/* FECHA CREACION:  09-06-2020 */
	/* DESCRIPCION:     ELIMINACION DE AREA */  
	$dataModel_01 = [
		'COD_UE_AREA'		=>$COD_UE_AREA,
		'USUARIO_ELIMINA'	=>$USUARIO_ELIMINA	
	];
	
	$RESULTADO = $objModel->Eliminar_Area_PDO($dataModel_01);
	$valor = ($RESULTADO>=0) ? '1':'0';
	/**** FIN DE ELIMINACION DE AREA ****/
	}

		
	echo $valor;
	
}

?>