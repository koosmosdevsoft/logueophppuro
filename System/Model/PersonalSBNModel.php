<?php

class PersonalSBNModel{
	
   private $oDBManager;
    	
	public function __construct(){
		$this->oDBManager	=	new Database;
	}
	
    	
	function TOTAL_REGISTRO_PERSONAL_SBN_X_PARAMETROS($txb_nombes, $txb_apepat, $txb_apemat, $txb_num_doc, $cbob_estado, $COD_MODALIDAD){
		if($this->oDBManager->conectar()==true){
			
			if($txb_nombes != ''){
				$nombre = " AND NOMBRES_PERS LIKE '%$txb_nombes%' ";
			}else{
				$nombre = "  ";
			}
			
			if($txb_apepat != ''){
				$apepat = " AND APE_PATERNO_PERS LIKE '%$txb_apepat%' ";
			}else{
				$apepat = "  ";
			}
			
			if($txb_apemat != ''){
				$apemat = " AND APE_MATERNO_PERS LIKE '%$txb_apemat%' ";
			}else{
				$apemat = "  ";
			}
			
			if($txb_num_doc != ''){
				$num_doc = " AND NRO_DNI LIKE '%$txb_num_doc%' ";
			}else{
				$num_doc = "  ";
			}
			
			if($cbob_estado != ''){
				$estado = " AND CONDICION_PERS LIKE '$cbob_estado%' ";
			}else{
				$estado = "  ";
			}
			
			if($COD_MODALIDAD != '0'){
				$MODALIDAD = " AND COD_MODALIDAD = '$COD_MODALIDAD' ";
			}else{
				$MODALIDAD = "  ";
			}
			
			$query		= "SELECT COUNT(*) AS TOT_REG FROM TBL_SBN_PERSONAL WHERE ID_ESTADO = '1' $nombre $apepat $apemat $num_doc $estado $MODALIDAD";
			$resultado 	= $this->oDBManager->execute($query);
			
			return $resultado;
		}
	}
	
	
	function LISTA_PERSONAL_SBN_X_PARAMETROS($INI, $FIN, $txb_nombes, $txb_apepat, $txb_apemat, $txb_num_doc, $cbob_estado, $COD_MODALIDAD){
		if($this->oDBManager->conectar()==true){
			
			try{
			
				if($txb_nombes != ''){
					$nombre = " AND P.NOMBRES_PERS LIKE '%$txb_nombes%' ";
				}else{
					$nombre = "  ";
				}
				
				if($txb_apepat != ''){
					$apepat = " AND P.APE_PATERNO_PERS LIKE '%$txb_apepat%' ";
				}else{
					$apepat = "  ";
				}
				
				if($txb_apemat != ''){
					$apemat = " AND P.APE_MATERNO_PERS LIKE '%$txb_apemat%' ";
				}else{
					$apemat = "  ";
				}
				
				if($txb_num_doc != ''){
					$num_doc = " AND P.NRO_DNI LIKE '%$txb_num_doc%' ";
				}else{
					$num_doc = "  ";
				}
				
				if($cbob_estado != ''){
					$estado = " AND P.CONDICION_PERS LIKE '$cbob_estado%' ";
				}else{
					$estado = "  ";
				}
				
				if($COD_MODALIDAD != '0'){
					$MODALIDAD = " AND P.COD_MODALIDAD = '$COD_MODALIDAD' ";
				}else{
					$MODALIDAD = "  ";
				}
					
				$SQL_TABLA ="
					SELECT 
					P.*,
					M.DESC_MODALIDAD,
					ROW_NUMBER() OVER (ORDER BY FECHA_REGISTRO DESC ) AS ROW_NUMBER_ID
					FROM TBL_SBN_PERSONAL  P
					LEFT JOIN TBL_INC_MODALIDAD M ON (P.COD_MODALIDAD = M.COD_MODALIDAD)
					WHERE P.ID_ESTADO = 1 $nombre $apepat $apemat $num_doc $estado $MODALIDAD
					";
				$query = "SELECT * FROM ( ".$SQL_TABLA." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN $INI AND $FIN ";
				$resultado = $this->oDBManager->execute($query);

				return $resultado;
			
			
			}catch(Exception $e){
				die($e->getMessage());
			}
			
		}
	}
	
	function LISTA_FOTO_PERSONAL_X_CODIGO($COD_PERSONAL){
		if($this->oDBManager->conectar()==true){
			$query="SELECT * FROM TBL_INTRANET_PERSONAL_FOTO WHERE COD_PERSONAL = '$COD_PERSONAL' ";
			$resultado = $this->oDBManager->execute($query);
			return $resultado;
		}
	}
	
	
	function MOSTRAR_DATOS_PERSONAL_x_COD_PERSONAL($COD_PERSONAL){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_SBN_PERSONAL WHERE COD_PERSONAL = '$COD_PERSONAL' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function ACTUALIZAR_DATOS_DEL_PERSONAL($COD_PERSONAL, $NOMBRES_PERS, $APE_MATERNO_PERS, $APE_PATERNO_PERS, $SEXO_PERS, $ID_ESTADO_CIVIL, $ID_GRADO_INSTRUCCION, $NRO_DNI, $FEC_NACIMIENTO, $CORREO_PERSONAL, $CORREO_SBN, $TEL_FIJO, $TEL_CELULAR, $TEL_ANEXO, $DISCAPACITADO, $CONDICION_PERS, $COD_DEPA_PERS, $COD_PROV_PERS, $COD_DIST_PERS, $COD_ZONA, $NOMBRE_ZONA, $NOMBRE_MANZANA, $NRO_LOTE, $NRO_INT, $NRO_DEPART, $COD_TVIA, $NOMBRE_VIA, $NRO_VIA, $DIRECCION_PERS, $DIRRECCION_REF_PERS, $COD_BANCO, $NRO_CTA_BANCO, $NRO_CTA_INTERBANCARIO, $COD_AFP, $COD_CUSPP, $COD_SALUD, $OBSERVACION, $USUARIO_MODIFICA, $ID_ESTADO, $COD_MODALIDAD){
		if($this->oDBManager->conectar()==true){
			$consulta="
			UPDATE TBL_SBN_PERSONAL SET 
				NOMBRES_PERS = '$NOMBRES_PERS',
				APE_MATERNO_PERS = '$APE_MATERNO_PERS',
				APE_PATERNO_PERS = '$APE_PATERNO_PERS',
				SEXO_PERS = '$SEXO_PERS',
				ID_ESTADO_CIVIL = '$ID_ESTADO_CIVIL',
				ID_GRADO_INSTRUCCION = '$ID_GRADO_INSTRUCCION',
				NRO_DNI = '$NRO_DNI',
				FEC_NACIMIENTO = '$FEC_NACIMIENTO',
				CORREO_PERSONAL = '$CORREO_PERSONAL',
				CORREO_SBN = '$CORREO_SBN',
				TEL_FIJO = '$TEL_FIJO',
				TEL_CELULAR = '$TEL_CELULAR',				
				TEL_ANEXO = '$TEL_ANEXO',
				DISCAPACITADO = '$DISCAPACITADO',
				CONDICION_PERS = '$CONDICION_PERS',
				COD_DEPA_PERS = '$COD_DEPA_PERS',
				COD_PROV_PERS = '$COD_PROV_PERS',
				COD_DIST_PERS = '$COD_DIST_PERS',
				COD_ZONA = '$COD_ZONA',
				NOMBRE_ZONA = '$NOMBRE_ZONA',
				NOMBRE_MANZANA = '$NOMBRE_MANZANA',
				NRO_LOTE = '$NRO_LOTE',
				NRO_INT = '$NRO_INT',
				NRO_DEPART = '$NRO_DEPART',
				COD_TVIA = '$COD_TVIA',
				NOMBRE_VIA = '$NOMBRE_VIA',
				NRO_VIA = '$NRO_VIA',
				DIRECCION_PERS = '$DIRECCION_PERS',
				DIRRECCION_REF_PERS = '$DIRRECCION_REF_PERS',
				COD_BANCO = '$COD_BANCO',
				NRO_CTA_BANCO = '$NRO_CTA_BANCO',
				NRO_CTA_INTERBANCARIO = '$NRO_CTA_INTERBANCARIO',
				COD_AFP = '$COD_AFP',
				COD_CUSPP = '$COD_CUSPP',
				COD_SALUD = '$COD_SALUD',
				OBSERVACION = '$OBSERVACION',
				USUARIO_MODIFICA = '$USUARIO_MODIFICA',
				FECHA_MODIFICA = GETDATE(),
				ID_ESTADO = '$ID_ESTADO',
				COD_MODALIDAD = '$COD_MODALIDAD'
			WHERE COD_PERSONAL = '$COD_PERSONAL'
			";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function INSERTAR_DATOS_DEL_PERSONAL($COD_PERSONAL, $NOMBRES_PERS, $APE_MATERNO_PERS, $APE_PATERNO_PERS, $SEXO_PERS, $ID_ESTADO_CIVIL, $ID_GRADO_INSTRUCCION, $NRO_DNI, $FEC_NACIMIENTO, $CORREO_PERSONAL, $CORREO_SBN, $TEL_FIJO, $TEL_CELULAR, $TEL_ANEXO, $DISCAPACITADO, $CONDICION_PERS, $COD_DEPA_PERS, $COD_PROV_PERS, $COD_DIST_PERS, $COD_ZONA, $NOMBRE_ZONA, $NOMBRE_MANZANA, $NRO_LOTE, $NRO_INT, $NRO_DEPART, $COD_TVIA, $NOMBRE_VIA, $NRO_VIA, $DIRECCION_PERS, $DIRRECCION_REF_PERS, $COD_BANCO, $NRO_CTA_BANCO, $NRO_CTA_INTERBANCARIO, $COD_AFP, $COD_CUSPP, $COD_SALUD, $OBSERVACION, $USUARIO_CREACION, $ID_ESTADO, $COD_MODALIDAD){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_SBN_PERSONAL (COD_PERSONAL, NOMBRES_PERS, APE_MATERNO_PERS, APE_PATERNO_PERS, SEXO_PERS, ID_ESTADO_CIVIL, ID_GRADO_INSTRUCCION, NRO_DNI, FEC_NACIMIENTO, CORREO_PERSONAL, CORREO_SBN, TEL_FIJO, TEL_CELULAR, TEL_ANEXO, DISCAPACITADO, CONDICION_PERS, COD_DEPA_PERS, COD_PROV_PERS, COD_DIST_PERS, COD_ZONA, NOMBRE_ZONA, NOMBRE_MANZANA, NRO_LOTE, NRO_INT, NRO_DEPART, COD_TVIA, NOMBRE_VIA, NRO_VIA, DIRECCION_PERS, DIRRECCION_REF_PERS, COD_BANCO, NRO_CTA_BANCO, NRO_CTA_INTERBANCARIO, COD_AFP, COD_CUSPP, COD_SALUD, OBSERVACION, FECHA_REGISTRO, USUARIO_CREACION, FECHA_CREACION, ID_ESTADO, COD_MODALIDAD)
	VALUES ('$COD_PERSONAL', '$NOMBRES_PERS', '$APE_MATERNO_PERS', '$APE_PATERNO_PERS', '$SEXO_PERS', '$ID_ESTADO_CIVIL', '$ID_GRADO_INSTRUCCION', '$NRO_DNI', '$FEC_NACIMIENTO', '$CORREO_PERSONAL', '$CORREO_SBN', '$TEL_FIJO', '$TEL_CELULAR', '$TEL_ANEXO', '$DISCAPACITADO', '$CONDICION_PERS', '$COD_DEPA_PERS', '$COD_PROV_PERS', '$COD_DIST_PERS', '$COD_ZONA', '$NOMBRE_ZONA', '$NOMBRE_MANZANA', '$NRO_LOTE', '$NRO_INT', '$NRO_DEPART', '$COD_TVIA', '$NOMBRE_VIA', '$NRO_VIA', '$DIRECCION_PERS', '$DIRRECCION_REF_PERS', '$COD_BANCO', '$NRO_CTA_BANCO', '$NRO_CTA_INTERBANCARIO', '$COD_AFP', '$COD_CUSPP', '$COD_SALUD', '$OBSERVACION', GETDATE(), '$USUARIO_CREACION', GETDATE(), '$ID_ESTADO', '$COD_MODALIDAD')";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}


	function INSERTAR_DATOS_DEL_PERSONAL_ACCESO($Crypttext,$NOM_USUARIO){
		if($this->oDBManager->conectar()==true){
			// SELECT (MAX(ISNULL(id_usuario,0)) + 1) as CODIGO from TBL_INTRANET_USUARIO  -- ID_USUARIO
			$consulta="SELECT (MAX(ISNULL(id_usuario,0)) + 1) as CODIGO from TBL_INTRANET_USUARIO";
			$result = $this->oDBManager->execute($consulta);
			$ID_USUARIO	= 	odbc_result($result,"CODIGO");

			// SELECT TOP 1 COD_PERSONAL,NOM_USUARIO FROM TBL_SBN_PERSONAL ORDER BY 1 DESC  -- ID_PERSONAL
			$consulta2="SELECT TOP 1 COD_PERSONAL FROM TBL_SBN_PERSONAL ORDER BY 1 DESC";
			$result2 = $this->oDBManager->execute($consulta2);
			$ID_PERSONAL	= 	odbc_result($result2,"COD_PERSONAL");
			$sqlQuery = "
				INSERT INTO TBL_INTRANET_USUARIO (ID_USUARIO, NOM_USUARIO, CLAVE_SQL, ID_PERSONAL, FECH_REG, ID_ESTADO) 
				VALUES ($ID_USUARIO, '$NOM_USUARIO', ENCRYPTBYPASSPHRASE('$Crypttext', '$NOM_USUARIO'), '$ID_PERSONAL', GETDATE(), 1)
			";
			// echo $sqlQuery;
			$result = $this->oDBManager->execute($sqlQuery);

			$this->oDBManager->close();
			return $result;
		}
	}

	
	function GENERAR_CODIGO_PERSONAL(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT 'PR'+RIGHT('00000'+CONVERT(VARCHAR,ISNULL(MAX(SUBSTRING(COD_PERSONAL, 3,LEN(COD_PERSONAL))),0)+1),6) AS CODIGO FROM TBL_SBN_PERSONAL ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function VERIFICA_SI_EXISTE_DATOS_PERSONALES($NRO_DNI){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT COUNT(*) as total_registro FROM TBL_SBN_PERSONAL WHERE NRO_DNI = '$NRO_DNI' AND ID_ESTADO = 1 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function VERIFICA_EXISTE_FOTO_PERSONAL($COD_PERSONAL){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT COUNT(*) AS TOT_REG FROM TBL_INTRANET_PERSONAL_FOTO WHERE COD_PERSONAL = '$COD_PERSONAL' ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	
	function INSERTAR_FOTO_PERSONAL($COD_PERSONAL, $NOM_FOTO, $PESO_FOTO){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_INTRANET_PERSONAL_FOTO VALUES ('$COD_PERSONAL', '$NOM_FOTO', '$PESO_FOTO', GETDATE(), '1' )";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	
	function ACTUALIZAR_FOTO_PERSONAL($COD_PERSONAL, $NOM_FOTO, $PESO_FOTO){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_INTRANET_PERSONAL_FOTO 
			SET NOM_FOTO= '$NOM_FOTO', PESO_FOTO = '$PESO_FOTO', FECHA_REGISTRO = GETDATE()  
			WHERE COD_PERSONAL = '$COD_PERSONAL' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	

	function VERIFICA_EXISTE_FOTO_CATALOGO($COD_CATALOGO){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT COUNT(*) AS TOT_REG FROM TBL_MUEBLES_CATALOGO_FOTO WHERE ID_CATALOGO = '$COD_CATALOGO' ";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
		
	function INSERTAR_FOTO_CATALOGO($COD_CATALOGO, $NOM_FOTO, $PESO_FOTO){
		if($this->oDBManager->conectar()==true){
			$consulta="INSERT INTO TBL_MUEBLES_CATALOGO_FOTO (ID_CATALOGO, NOMB_ARCHIVO, PESO_ARCHIVO, FECHA_ARCHIVO, ID_ESTADO) VALUES ('$COD_CATALOGO', '$NOM_FOTO', '$PESO_FOTO', GETDATE(), '1' )";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}

	function ACTUALIZAR_FOTO_CATALOGO($COD_CATALOGO, $NOM_FOTO, $PESO_FOTO){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_MUEBLES_CATALOGO_FOTO 
			SET NOMB_ARCHIVO= '$NOM_FOTO', PESO_ARCHIVO = '$PESO_FOTO', FECHA_ARCHIVO = GETDATE()  
			WHERE ID_CATALOGO = '$COD_CATALOGO' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}	
	
	function TOTAL_REGISTRO_SI_EXISTE_COD_PERSONAL_SID($COD_PERSONAL_SID){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT COUNT(*) AS NRO_REG FROM TBL_SBN_PERSONAL WHERE ISNULL(codigo_personal_sid,'') = '$COD_PERSONAL_SID' ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	/**********************************************************************************************************/
	/**********************************************************************************************************/
	/**********************************************************************************************************/
	
	function Lista_Estado_Civil(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_ESTADO_CIVIL WHERE ID_ESTADO = 1 ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Lista_Grado_Instruccion(){
		if($this->oDBManager->conectar()==true){
			$sql=" SELECT * FROM TBL_GRADO_INSTRUCCION WHERE ID_ESTADO=1";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Lista_Tipo_Zona(){
		if($this->oDBManager->conectar()==true){
			$sql=" SELECT * FROM TBL_TIPO_ZONA WHERE EST_ZONA=1";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Lista_Tipo_Zona_Inmueble(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_TIPO_ZONA WHERE EST_ZONA = 1 AND INDICADOR = 'INMUEBLES'";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Lista_Tipo_Via(){
		if($this->oDBManager->conectar()==true){
			$sql=" SELECT * FROM TBL_TIPO_VIA";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Lista_Bancos(){
		if($this->oDBManager->conectar()==true){
			$sql=" SELECT * FROM TBL_TIPO_BANCO WHERE EST_BANCO = 1";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Lista_Seguro_AFP(){
		if($this->oDBManager->conectar()==true){
			$sql=" SELECT * FROM TBL_TIPO_AFP WHERE EST_AFP = 1";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Listar_Tipo_Modalidad(){
		if($this->oDBManager->conectar()==true){
			// $sql=" SELECT * FROM TBL_INC_MODALIDAD WHERE ID_ESTADO = 1";
			$sql="SELECT * FROM TBL_INC_MODALIDAD";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	//************* ORGANIGRAMA
	
	function Listar_Areas_SBN(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_AREA WHERE ID_ESTADO = 1 ORDER BY POSICION ASC";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Listar_Division_SBN_X_Area($COD_AREA){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_AREA_DIVISION WHERE ID_ESTADO = 1 and COD_AREA = '$COD_AREA' ORDER BY NOM_DIVISION ASC";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Listar_Unidad_SBN_X_Division($COD_AREA_DIV){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_AREA_DIVISION_UNIDAD WHERE ID_ESTADO = 1 and COD_AREA_DIV = '$COD_AREA_DIV' ORDER BY NOM_UNIDAD ASC ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Listar_Cargo_SBN_X_Unidad($COD_AREA_DIV_UNID){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_AREA_DIVISION_UNIDAD_CARGO WHERE ID_ESTADO = 1 and COD_AREA_DIV_UNID = '$COD_AREA_DIV_UNID' ORDER BY NOM_CARGO ASC";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Listar_Modalidad(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT * FROM TBL_SBN_MODALIDAD WHERE ESTADO = 1";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	//****************** cargos personal
	function GENERAR_CODIGO_CARGO_PERSONAL(){
		if($this->oDBManager->conectar()==true){
			$sql="SELECT ISNULL(MAX(COD_PERS_CARGO),0)+1 AS CODIGO FROM TBL_PERSONAL_CARGO  ";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function Listar_Cargos_Pers_x_IDPers($COD_PERSONAL){
		if($this->oDBManager->conectar()==true){
			$sql="
SELECT 
C.*,
A.NOM_AREA,
D.NOM_DIVISION,
U.NOM_UNIDAD,
U.ABREVIATURA,
G.NOM_CARGO,
G.NRO_PLAZA,
ESTADO_ACTIVO = (CASE WHEN C.ACTIVO = 1 THEN 'ACTIVO' ELSE 'INACTIVO' END )
FROM TBL_PERSONAL_CARGO C
LEFT JOIN TBL_AREA A ON (A.COD_AREA = C.COD_AREA)
LEFT JOIN TBL_AREA_DIVISION D ON (D.COD_AREA_DIV = C.COD_AREA_DIV)
LEFT JOIN TBL_AREA_DIVISION_UNIDAD U ON (U.COD_AREA_DIV_UNID = C.COD_AREA_DIV_UNID)
LEFT JOIN TBL_AREA_DIVISION_UNIDAD_CARGO G ON (G.COD_CARGO = C.COD_CARGO)
WHERE C.ID_ESTADO = 1
AND C.COD_PERSONAL = '$COD_PERSONAL'
			";
			$resultado = $this->oDBManager->execute($sql);
			return $resultado;
		}
	}
	
	function INSERTAR_CARGO_PERSONAL($COD_PERS_CARGO, $COD_PERSONAL, $COD_AREA, $COD_AREA_DIV, $COD_AREA_DIV_UNID, $COD_CARGO){
		if($this->oDBManager->conectar()==true){
			$consulta="
INSERT TBL_PERSONAL_CARGO (COD_PERS_CARGO, COD_PERSONAL, COD_AREA, COD_AREA_DIV, COD_AREA_DIV_UNID, COD_CARGO, FECHA_REGISTRO, ID_ESTADO) 
VALUES('$COD_PERS_CARGO', '$COD_PERSONAL', '$COD_AREA', '$COD_AREA_DIV', '$COD_AREA_DIV_UNID', '$COD_CARGO', GETDATE(), '1')
			";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function ACTIVAR_CARGO_PERSONAL($COD_PERSONAL, $COD_PERS_CARGO){
		if($this->oDBManager->conectar()==true){
			$consulta="
UPDATE TBL_PERSONAL_CARGO SET ACTIVO = 0 WHERE ID_ESTADO = 1 AND COD_PERSONAL = '$COD_PERSONAL';
UPDATE TBL_PERSONAL_CARGO SET ACTIVO = 1 WHERE ID_ESTADO = 1 AND COD_PERS_CARGO = '$COD_PERS_CARGO'; 
			";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
	function ELIMINAR_CARGO_PERSONAL($COD_PERS_CARGO){
		if($this->oDBManager->conectar()==true){
			$consulta="UPDATE TBL_PERSONAL_CARGO SET ID_ESTADO = 2 WHERE  COD_PERS_CARGO = '$COD_PERS_CARGO' ";
			$result = $this->oDBManager->execute($consulta);
			$this->oDBManager->close();
			return $result;
		}
	}
	
}
?>