<?php

    class M_TBL_MUEBLES_UE_BIEN_PATRIMONIAL_Model extends Database
    {
        //Metodo Contructor
        public function __construct()
        {
            parent::__construct();
        }

        function F_ListadoBienesPatrimoniales(
            $ACCION, $INI, $FIN, $COD_ENTIDAD, $ANIO_ADQUIS, $COD_GRUPO, $COD_CLASE,
            $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN )
        {
            if ($this->conectar()==true)
            {
                 $consulta = "EXEC SP_SIMI_LISTADO_BIENES_PATRIMONIALES
                 @ACCION = '$ACCION', @INI = '$INI', @FIN = '$FIN', @COD_ENTIDAD = '$COD_ENTIDAD',
                 @ANIO_ADQUIS = '$ANIO_ADQUIS', @COD_GRUPO = '$COD_GRUPO', @COD_CLASE = '$COD_CLASE',
                @CODIGO_PATRIMONIAL = '$CODIGO_PATRIMONIAL', @DENOMINACION_BIEN = '$DENOMINACION_BIEN'";

                $rs_Resultado = $this->execute($consulta);
                return $rs_Resultado;
            }
        }

        function F_TotalBienesPatrimoniales(
             $ACCION, $INI, $FIN, $COD_ENTIDAD, $ANIO_ADQUIS, $COD_GRUPO, $COD_CLASE,
            $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN )
        {
            if ($this->conectar()==true)
            {
                $consulta = "EXEC SP_SIMI_LISTADO_BIENES_PATRIMONIALES
                 @ACCION = '$ACCION', @INI = '$INI', @FIN = '$FIN', @COD_ENTIDAD = '$COD_ENTIDAD',
                 @ANIO_ADQUIS = '$ANIO_ADQUIS', @COD_GRUPO = '$COD_GRUPO', @COD_CLASE = '$COD_CLASE',
                @CODIGO_PATRIMONIAL = '$CODIGO_PATRIMONIAL', @DENOMINACION_BIEN = '$DENOMINACION_BIEN'";

                $total = 0;
                $rs_Resultado = $this->execute($consulta);
                $total = odbc_result($rs_Resultado,'TOT_REG');
                $this->close();
                return $total;
            }
        }


        function F_BuscarBienPatrimonial_X_Codigo( $COD_UE_BIEN_PATRI )
        {
            if ($this->conectar()==true)
            {
                $consulta = "EXEC SP_SIMI_BUSCAR_BIEN_PATRIMONIAL_X_CODIGO
                 @COD_UE_BIEN_PATRI = '$COD_UE_BIEN_PATRI'";

                $rs_Resultado = $this->execute($consulta);
                return $rs_Resultado;
            }
        }


        function F_ListadoBienesPatrimoniales_X_Predios( $ACCION, $COD_ENTIDAD, $ID_PREDIO_SBN, $INI, $FIN )
        {
            if ($this->conectar()==true)
            {
                 $consulta = "EXEC SP_SP_SIMI_LISTADO_BIENES_X_PREDIO
                 @ACCION = '$ACCION', @COD_ENTIDAD = '$COD_ENTIDAD', @ID_PREDIO_SBN = '$ID_PREDIO_SBN', @INI = '$INI', @FIN = '$FIN'";


                $rs_Resultado = $this->execute($consulta);
                return $rs_Resultado;
            }
        }

        function F_Clasificacion_Bienes_X_Depreciacion( $COD_ENTIDAD, $ANIO_DEP, $MES_DEP )
        {
            if ($this->conectar()==true)
            {
                 $consulta = "EXEC SP_SIMI_CLASIFICACION_BIENES_X_DEPRECIACION
                 @COD_ENTIDAD = '$COD_ENTIDAD', @ANIO_DEP = '$ANIO_DEP', @MES_DEP = '$MES_DEP'";


                $rs_Resultado = $this->execute($consulta);
                return $rs_Resultado;
            }
        }

        function F_ListadoClasificacion_Bienes
        (
            $ACCION, $COD_ENTIDAD, $ANIO_DEP, $MES_DEP, $GRUPO_BIEN_DEP, $INI, $FIN
        )
        {
            if ($this->conectar()==true)
            {
                 $consulta = "EXEC SP_SIMI_LISTADO_CLASIFICACION_BIENES
                 @ACCION = '$ACCION',  @COD_ENTIDAD = '$COD_ENTIDAD', @ANIO_DEP = '$ANIO_DEP',
                 @MES_DEP = '$MES_DEP', @GRUPO_BIEN_DEP = '$GRUPO_BIEN_DEP', @INI = '$INI', @FIN = '$FIN'";

                if ($ACCION == 1)
                {
                    $total = 0;

                    $rs_Resultado = $this->execute($consulta);
                    $total = odbc_result($rs_Resultado,'TOTAL_REG');
                    $this->close();
                    return $total;
                }
                else
                {

                    $rs_Resultado = $this->execute($consulta);
                    return $rs_Resultado;
                }


            }
        }

        /* <Funcion>        :  Listado de Bienes Activos No Catalogados */
        function F_ListadoBienesActivos_NO_Catalogados( $COD_ENTIDAD )
        {
            if ($this->conectar()==true)
            {
                $consulta = "EXEC SP_SIMI_LISTADO_BIENES_ACTIVOS_NO_CATALOGADOS @COD_ENTIDAD = '$COD_ENTIDAD'";


                $rs_Resultado = $this->execute($consulta);
                return $rs_Resultado;

            }

        }

        /* <Funcion>        :  Listado de Bienes de Baja No Catalogados */
        function F_ListadoBienesBaja_NO_Catalogados( $COD_ENTIDAD )
        {
            if ($this->conectar()==true)
            {
                $consulta = "EXEC SP_SIMI_LISTADO_BIENES_BAJA_NO_CATALOGADOS @COD_ENTIDAD = '$COD_ENTIDAD'";


                $rs_Resultado = $this->execute($consulta);
                return $rs_Resultado;

            }

        }

        /* <Funcion>        :  Lista bienes con Componentes */
        function F_ListadoBienesComponentes
        (
            $ACCION, $INI, $FIN, $COD_ENTIDAD, $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN
        )
        {
            if ($this->conectar()==true)
            {
                 $consulta = "EXEC SP_SIMI_REPORTE_BIENES_CON_COMPONENTES
                 @ACCION = '$ACCION',  @INI = '$INI', @FIN = '$FIN', @COD_ENTIDAD = '$COD_ENTIDAD',
                 @CODIGO_PATRIMONIAL = '$CODIGO_PATRIMONIAL', @DENOMINACION_BIEN = '$DENOMINACION_BIEN'";

                if ($ACCION == 1)
                {
                    $total = 0;


                    $rs_Resultado = $this->execute($consulta);
                    $total = odbc_result($rs_Resultado,'TOTAL_REG');
                    $this->close();
                    return $total;
                }
                else
                {

                    $rs_Resultado = $this->execute($consulta);
                    return $rs_Resultado;
                }


            }
        }

        /* <Funcion>        : Total de Bienes de Tipo Vehicular*/
        function F_Total_Bienes_Vehiculares($ACCION, $COD_ENTIDAD, $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN)
        {

          if ($this->conectar() == true)
          {
            $consulta = "EXEC SP_SIMI_LISTADO_BIENES_VEHICULARES
            @ACCION             = '$ACCION',
            @INI                = 0,
            @FIN                = 0,
            @COD_ENTIDAD        = '$COD_ENTIDAD',
            @CODIGO_PATRIMONIAL = '$CODIGO_PATRIMONIAL',
            @DENOMINACION_BIEN  = '$DENOMINACION_BIEN'";

            $rs_Resultado = $this->execute($consulta);
            return $rs_Resultado;

          }
        }

        /* <Funcion>        : Listado de Bienes de Tipo Vehicular*/
        function F_Listado_Bienes_Vehiculares($ACCION, $COD_ENTIDAD, $CODIGO_PATRIMONIAL, $DENOMINACION_BIEN, $INI, $FIN)
        {

          if ($this->conectar() == true)
          {
            $consulta = "EXEC SP_SIMI_LISTADO_BIENES_VEHICULARES
            @ACCION             = '$ACCION',
            @COD_ENTIDAD        = '$COD_ENTIDAD',
            @CODIGO_PATRIMONIAL = '$CODIGO_PATRIMONIAL',
            @DENOMINACION_BIEN  = '$DENOMINACION_BIEN',
            @INI                = '$INI',
            @FIN                = '$FIN'";


            $rs_Resultado = $this->execute($consulta);
            return $rs_Resultado;

          }
        }


    }

?>
