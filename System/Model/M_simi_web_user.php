<? session_start();
$url="http://".$_SERVER['HTTP_HOST'].":".$_SERVER['SERVER_PORT'].$_SERVER['REQUEST_URI'];

//
define("Muppet",true);


$operacion	=	$_GET['operacion'];
require_once('../Controller/C_sbn_contactenos_muebles.php');
require_once('../Controller/C_simi_web_user.php');
require_once('../Controller/SuperAdminController.php');
require_once('../Controller/C_simi_web_user_permiso.php');
require_once('../../utils/funciones/funciones.php');
include_once("../../utils/funciones/stringencrypt.php");


/* CREACION:        WILLIAMS ARENAS */
/* FECHA CREACION:  26-05-2020 */
/* DESCRIPCION:     IMPORTACION DE LIBRERIAS PDO */
require_once("../Controller/BaseBD_PDO.php");
require_once('../Model/BaseBD_PDOModel.php');
require_once("../Model/BienPatrimonialModel.php");
/* FIN DE IMPORTACION DE LIBRERIAS PDO */

require_once("../../class/nocsrf.php");
require "../../vendor/autoload.php";
use \Firebase\JWT\JWT;

$oContactenosMuebles	=	new ContactenosMuebles;
// $oSinabip_Windows		=	new Sinabip_Windows;
$rdo 					= 	new Encryption();
$oSimi_Web_User			=	new Simi_Web_User;
$oMuebles_Menu_Permiso	=	new Muebles_Menu_Permiso;
$objModel 				= new BienPatrimonialModel();



if($operacion === 'validar_usuarios_muebles'){
	
	header("Access-Control-Allow-Origin: * ");
	header("Content-Type: application/json; charset=UTF-8");
	header("Access-Control-Allow-Methods: POST");
	header("Access-Control-Max-Age: 3600");
	header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
	try
	{
		// Run CSRF check, on POST data, in exception mode, with a validity of 10 minutes, in one-time mode.
		NoCSRF::check( 'csrf_token', $_POST, true, 60*10, true );
		// form parsing, DB inserts, etc.
	
		$v_Sistemas		= trim(stripslashes(htmlspecialchars($_POST['v_Sistemas'])));
		$v_usuario		= trim(stripslashes(htmlspecialchars($_POST['v_usuario'])));
		$v_contrasena	= trim(stripslashes(htmlspecialchars($_POST['v_contrasena'])));
		$v_txh_url		= trim(stripslashes(htmlspecialchars($_POST['txh_url'])));
		$cod_entidad	= trim(stripslashes(htmlspecialchars($_POST['txt_entidad'])));
	
		//$cod_entidad	='';
		$sistema 	= str_replace("\\","<",str_replace("'", '-', str_replace('"', '--', $v_Sistemas )));
		$usuario 	= str_replace("\\","<",str_replace("'", '-', str_replace('"', '--', $v_usuario )));
		$clave 		= str_replace("\\","<",str_replace("'", '-', str_replace('"', '--', $v_contrasena )));
		$entidad 	= str_replace("\\","<",str_replace("'", '-', str_replace('"', '--', $cod_entidad )));
		
		$PAGINA = '';
		
		if($sistema== 'SINABIP'  || $sistema== 'DECREURG' ){
			$usuario 	=	$usuario;
			//$clave_ok	=	$rdo->encode($clave);
			$clave_ok	=	$clave;
			$Crypttext	=	$rdo->enviarCrypttext();
			$valida_decreto = 0;
			if($sistema == 'DECREURG')
			{
				$valida_decreto = 1;
			}
	
			/*
			$RS					=	$oSimi_Web_User->Validar_Solo_Usuario_Simi($usuario,$valida_decreto,$entidad);
			$TOT_USER_VAL		= 	odbc_result($RS,"TOT_USER");
			*/
			/* CREACION:        WILLIAMS ARENAS */
			/* FECHA CREACION:  28-05-2020 */
			/* DESCRIPCION:     VALIDAR USUARIO */
			
			$data = array();    
			$COD_USUARIO = $usuario;				
			
			$dataModel_01 = [
				'COD_USUARIO'	=>$COD_USUARIO
			];
			
			$data['DataValidarUsuario'] = $objModel->Validar_Solo_Usuario_SimiPDO($dataModel_01);
			// print_r(count($data['DataValidarUsuario']));
			if(count($data['DataValidarUsuario'])>0){
				http_response_code(200);
				$TOT_USER_VAL = $data['DataValidarUsuario'][0]['TOT_USER'];
			}else{
				$TOT_USER_VAL = 0;
			}
			//echo 'resultado: ' . $TOT_USER_VAL . 'OK';
			//echo $TOT_USER_VAL; return;
			/* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */
			if(isset($_COOKIE['SNB'.$usuario])){
				$valor = 10;
				echo json_encode(array(
					"message" 	=> "El usuario ".$usuario." ha sido bloqueado por 1 minuto", 
					"msg" 		=> "El usuario ".$usuario." ha sido bloqueado por 1 minuto", 
					"valor" 	=> $valor,
					"nombre" 	=> ''
				));
			}else{
				if($TOT_USER_VAL == 1){
					//============================== TIENE UN USUARIO UNICO =================================
					$RS	=	$oSimi_Web_User->Validar_Usuario_Simi($usuario, $clave_ok, $Crypttext,$valida_decreto,$entidad);
					$reg_tot	= 	odbc_result($RS,"tot_reg");
					/* CREACION:        WILLIAMS ARENAS */
					/* FECHA CREACION:  29-05-2020 */
					/* DESCRIPCION:     VALIDAR USUARIO */
					/*
					$data = array();    			
					$dataModel_01 = [
						'usuario'			=>$usuario,
						'Crypttext'		=>$Crypttext,
						'clave_ok'		=>$clave_ok,
						'valida_decreto'	=>$valida_decreto, 
						'entidad'			=>$entidad
					];
					//echo '<pre>'; print_r($dataModel_01);
					$data['DataValidar'] = $objModel->Validar_Usuario_SimiPDO($dataModel_01);
					echo '<pre>'; print_r($data['DataValidar']);
					die();
					$reg_tot = $data['DataValidar'][0]['tot_reg'];
					*/
					//echo $TOT_USER_VAL; return;
					/* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */
					if($reg_tot==1){
						/*
							$dataValidacionEntidad =	$oSimi_Web_User->Validar_Acceso_Entidad_a_Modulo($usuario,$entidad);
							$TOT_CANTIDAD_USER	   = 	odbc_result($dataValidacionEntidad,"TOT_CANTIDAD_USER");
						*/
						/* CREACION:        WILLIAMS ARENAS */
						/* FECHA CREACION:  29-05-2020 */
						/* DESCRIPCION:     VALIDAR ACCESO MODULO */
						$data = array();    			
						$dataModel_01 = [
							'usuario'			=>$usuario,
							'entidad'			=>$entidad
						];
						$data['DataValidarAccesoModulo'] = $objModel->Validar_Acceso_Entidad_a_ModuloPDO($dataModel_01);
						//echo '<pre>'; print_r($data['DataValidar']);
						//die();
						$TOT_CANTIDAD_USER = $data['DataValidarAccesoModulo'][0]['TOT_CANTIDAD_USER'];
						//echo $TOT_USER_VAL; return;
						/* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */
		
						if($TOT_CANTIDAD_USER == ''){
							// $DESC_1 = 'SU ENTIDAD NO CONFORMA EL SISTEMA NACIONAL DE BIENES ESTATALES (SBNE)';
							// $DESC_2 = 'Clave incorrecta';
							// session_destroy();
							if(isset($_COOKIE["$usuario"])){
								$cont = $_COOKIE["$usuario"];
								$cont++;
								setcookie($usuario,$cont,time()+120);
								if($cont >=3){
									setcookie("SNB".$usuario,$cont,time()+60);
								}
							}else{
								setcookie($usuario,1,time()+120);
							}

							$valor = 6;
							echo json_encode(array(
								"message" => "SU ENTIDAD NO EXISTE", 
								"msg" => 'Clave incorrecta',
								"valor" => $valor,
								"nombre" => $PAGINA
							));
							die();
						}else{
							//=========== USUARIO Y CONTRASEÑA CORRECTO
							$RESULT		=	$oSimi_Web_User->Mostrar_Datos_Usuario_Simi($usuario, $clave_ok, $Crypttext);
							$_SESSION['th_SIMI_COD_USUARIO']		=	odbc_result($RESULT,"COD_USUARIO");
							$_SESSION['th_SIMI_NOM_USUARIO']		=	odbc_result($RESULT,"NOM_USUARIO");
							$_SESSION['th_SIMI_COD_ENTIDAD']		=	odbc_result($RESULT,"COD_ENTIDAD");
							$_SESSION['tSISTEMA']					=	'SINABIP';
							$_SESSION['th_SIMI_SUPERADMIN']			=	'0';
							// $_SESSION['PRUEBA']		=	'WALAS';
							/* CREACION:        WILLIAMS ARENAS */
							/* FECHA CREACION:  29-05-2020 */
							/* DESCRIPCION:     MOSTRAR DATOS USUARIO */
							/*
							$data = array();    			
							$dataModel_01 = [
								'usuario'		=>$usuario,
								'clave_ok'	=>$clave_ok,
								'Crypttext'	=>$Crypttext
							];
							
							$data['DataUsuario'] = $objModel->Mostrar_Datos_Usuario_SimiPDO($dataModel_01);
							//echo '<pre>'; print_r($data['DataValidar']);
							//die();
							$_SESSION['th_SIMI_COD_USUARIO']		=	$data['DataUsuario'][0]['COD_USUARIO'];
							$_SESSION['th_SIMI_NOM_USUARIO']		=	$data['DataUsuario'][0]['NOM_USUARIO'];
							$_SESSION['th_SIMI_COD_ENTIDAD']		=	$data['DataUsuario'][0]['COD_ENTIDAD'];
							$_SESSION['tSISTEMA']					=	'SINABIP';
							$_SESSION['th_SIMI_SUPERADMIN']			=	'0';
							$_SESSION['PRUEBA']		=	'WALAS';
							*/
							//echo $TOT_USER_VAL; return;
							/* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */
		
							$secret_key = "sinabip_dsdvvvkvn";
							$issuer_claim = "SBN"; // this can be the servername
							$audience_claim = "THE_AUDIENCE";
							$issuedat_claim = time(); // issued at
							$notbefore_claim = $issuedat_claim + 10; //not before in seconds
							$expire_claim = $issuedat_claim + 60; // expire time in seconds
							$token = array(
								"iss" => $issuer_claim,
								"aud" => $audience_claim,
								// "iat" => $issuedat_claim,
								// "nbf" => $notbefore_claim,
								"iat" => 1356999524,
								"nbf" => 1357000000,
								// "exp" => $expire_claim,
								"data" => array(
									"th_SIMI_COD_USUARIO" => odbc_result($RESULT,"COD_USUARIO"),
									"th_SIMI_NOM_USUARIO" => odbc_result($RESULT,"NOM_USUARIO"),
									"th_SIMI_COD_ENTIDAD" => odbc_result($RESULT,"COD_ENTIDAD"),
									"tSISTEMA" => 'SINABIP',
									"th_SIMI_SUPERADMIN" => 0
							));							
	
							http_response_code(200);
							$valor 	= 1;
							$DESC_1 = 'CORRECTO LOGUEO - MUEBLES';
							$DESC_2 = 'Usuario y clave correcto';
							
							$PAGINA = 'sinabip.php';
							if($sistema == 'DECREURG')
							{
								$PAGINA = 'decreto_urgencia.php?idm=4';
							}
							$jwt = JWT::encode($token, $secret_key);
							echo json_encode(
								array(
									"message" => "Successful login.",
									"jwt" => $jwt,
									"expireAt" => $expire_claim,
									"valor" => $valor,
									"nombre" => $PAGINA
								)
							);
							die();
							// $clave 	= $clave_ok;
		
						}
					}else{
						if(isset($_COOKIE["$usuario"])){
							$cont = $_COOKIE["$usuario"];
							$cont++;
							setcookie($usuario,$cont,time()+120);
							if($cont >=3){
								setcookie("SNB".$usuario,$cont,time()+60);
							}
						}else{
							setcookie($usuario,1,time()+120);
						}
						//=========== USUARIO Y CONTRASEÑA INCORRECTO
						$valor = 4;
						echo json_encode(array(
							"message" => "ERROR LOGUEO - MUEBLES", 
							"msg" => 'Usuario no existe',
							"valor" => $valor,
							"nombre" => $PAGINA
						));
					}		
				}else if($TOT_USER_VAL > 1){
					if(isset($_COOKIE["$usuario"])){
						$cont = $_COOKIE["$usuario"];
						$cont++;
						setcookie($usuario,$cont,time()+120);
						if($cont >=3){
							setcookie("SNB".$usuario,$cont,time()+60);
						}
					}else{
						setcookie($usuario,1,time()+120);
					}
					//============================== TIEN MAS DE UN USUARIO UNICO =================================
					$valor = 3;
					$DESC_1 = 'ERROR LOGUEO - MUEBLES';
					$DESC_2 = 'Existe mas de un usuario';
					echo json_encode(array(
						"message" => "ERROR LOGUEO - MUEBLES", 
						"msg" => 'Existe mas de un usuario',
						"valor" => $valor,
						"nombre" => $PAGINA
					));
					die();
				}else if($TOT_USER_VAL == 0 || $TOT_USER_VAL == ''){
					if(isset($_COOKIE["$usuario"])){
						$cont = $_COOKIE["$usuario"];
						$cont++;
						setcookie($usuario,$cont,time()+120);
						if($cont >=3){
							setcookie("SNB".$usuario,$cont,time()+60);
						}
					}else{
						setcookie($usuario,1,time()+120);
					}
					//============================== USUARIO NO EXISTE =================================
					$valor = 0;
					// http_response_code(401);
					// echo json_encode(array("message" => "ERROR LOGUEO - MUEBLES", "password" => 'Usuario no existe'));
					$DESC_1 = 'ERROR LOGUEO - MUEBLES';
					$DESC_2 = 'Usuario no existe';
					// session_destroy();	
					echo json_encode(array(
						"message" => "ERROR LOGUEO - MUEBLES", 
						"msg" => 'Usuario no existe',
						"valor" => $valor,
						"nombre" => $PAGINA
					));
				}
			}


		}
	
		// echo $valor.'[*]'.$PAGINA;
	}
	catch ( Exception $e )
	{
		// CSRF attack detected
		// echo $e->getMessage() . ' Form ignored.';
		echo json_encode(array(
			"message" => $e->getMessage() . ' Form ignored.', 
			"msg" => 'Usuario no existe',
			"valor" => 2,
			"nombre" => ''
		));
	}


}else if($operacion=='validar_ruc_decreto'){

	$v_Sistemas	= $_REQUEST['v_Sistemas'];
	$v_ruc		= $_REQUEST['v_ruc'];
	$v_txh_url	= $_REQUEST['txh_url'];
	
	
	$sistema 	= str_replace("\\","<",str_replace("'", '-', str_replace('"', '--', $v_Sistemas )));
	$txtRuc 	= str_replace("\\","<",str_replace("'", '-', str_replace('"', '--', $v_ruc )));
	
	if( $sistema== 'DECREURG' ){
		$txtRuc 		=	$txtRuc;
		$RS				=	$oSimi_Web_User->Validar_RUC_SBN($txtRuc);
		$TOT_RUC_VAL	= 	odbc_result($RS,"TOT_RUC");
		$ID_ESTADO_CONCLUIDO	= 	odbc_result($RS,"ID_ESTADO_CONCLUIDO");


			if($TOT_RUC_VAL == 1){
				$NOM_ENTIDAD	= 	odbc_result($RS,"NOMBRE_ENTIDAD");
				$COD_ENTIDAD	= 	odbc_result($RS,"COD_ENTIDAD");
				$valor 	= 1;
			}else if($TOT_RUC_VAL == 0 || $TOT_RUC_VAL == ''){
				$valor = 0;
				$NOM_ENTIDAD = '';
				$COD_ENTIDAD = '';
			}
		// }
	}

	echo $valor.'[*]'.$NOM_ENTIDAD.'[*]'.$COD_ENTIDAD;

}else if($operacion=='validar_usuarios_muebles_superadmin'){
	
	$v_Sistemas		= $_REQUEST['v_Sistemas'];
	$v_usuario		= $_REQUEST['v_usuario'];
	$v_contrasena	= $_REQUEST['v_contrasena'];
	$v_txh_url		= $_REQUEST['txh_url'];
	$v_admint		= $_REQUEST['v_admint'];
	$cod_entidad	= $_REQUEST['txt_entidad'];
	
	$S_SIMI_COD_USUARIO = $_SESSION['th_SIMI_COD_USUARIO'];
	$S_SIMI_NOM_USUARIO = $_SESSION['th_SIMI_NOM_USUARIO'];
	$S_SIMI_COD_ENTIDAD = $_SESSION['th_SIMI_COD_ENTIDAD'];
	//echo $S_SIMI_NOM_USUARIO;
	$sistema 	= str_replace("\\","<",str_replace("'", '-', str_replace('"', '--', $v_Sistemas )));
	$usuario 	= str_replace("\\","<",str_replace("'", '-', str_replace('"', '--', $v_usuario )));
	$clave 		= str_replace("\\","<",str_replace("'", '-', str_replace('"', '--', $v_contrasena )));
	$entidad 	= str_replace("\\","<",str_replace("'", '-', str_replace('"', '--', $cod_entidad )));
	
	
	$PAGINA = '';
	
	if($sistema== 'SINABIP'  || $sistema== 'DECREURG' ){
		$usuario 	=	$usuario;
		//$clave_ok	=	$rdo->encode($clave);
		$clave_ok	=	$clave;
		$Crypttext	=	$rdo->enviarCrypttext();
		$valida_decreto = 0;
		if($sistema == 'DECREURG')
		{
			$valida_decreto = 1;
		}


		$RS					=	$oSimi_Web_User->Validar_Solo_Usuario_Simi($usuario,$valida_decreto,$entidad);
		$TOT_USER_VAL		= 	odbc_result($RS,"TOT_USER");
		
		if($TOT_USER_VAL == 1){			
				//============================== TIENE UN USUARIO UNICO =================================
			
				$RS			=	$oSimi_Web_User->Validar_Usuario_Simi($usuario, $clave_ok, $Crypttext,$valida_decreto,$entidad);
				$reg_tot	= 	odbc_result($RS,"tot_reg");	

				if($reg_tot==1){
					
					//=========== USUARIO Y CONTRASEÑA CORRECTO
					$RESULT		=	$oSimi_Web_User->Mostrar_Datos_Usuario_Simi($usuario, $clave_ok, $Crypttext);
					
					// var_dump($RESULT);die();
					$_SESSION['th_SIMI_COD_USUARIO']		=	odbc_result($RESULT,"COD_USUARIO");
					$_SESSION['th_SIMI_NOM_USUARIO']		=	odbc_result($RESULT,"NOM_USUARIO");
					$_SESSION['th_SIMI_COD_ENTIDAD']		=	odbc_result($RESULT,"COD_ENTIDAD");
					$_SESSION['tSISTEMA']					=	'SINABIP';
					
					if($v_admint=='1'){
						$_SESSION['th_SIMI_SUPERADMIN']		=	'1';
						$_SESSION['admin_coduser']				= $S_SIMI_COD_USUARIO;
						$_SESSION['admin_nombuser']				= $S_SIMI_NOM_USUARIO;
						$_SESSION['admin_codenti']				= $S_SIMI_COD_ENTIDAD;

					} 
 					//var_dump($_SESSION);
					$valor 	= 1;
					$DESC_1 = 'CORRECTO LOGUEO - MUEBLES';
					$DESC_2 = 'Usuario y clave correcto';
					
					$PAGINA = 'sinabip.php';
					if($sistema == 'DECREURG')
					{
						$PAGINA = 'decreto_urgencia.php?idm=4';
					}
					

					$clave 	= $clave_ok;
					
				}else{
					
					//=========== USUARIO Y CONTRASEÑA INCORRECTO
					$valor = 4;
					$DESC_1 = 'ERROR LOGUEO - MUEBLES';
					$DESC_2 = 'Clave incorrecta';
					session_destroy();
					
				}		
				
		}else if($TOT_USER_VAL > 1){
			
			//============================== TIEN MAS DE UN USUARIO UNICO =================================
			$valor = 3;
			$DESC_1 = 'ERROR LOGUEO - MUEBLES';
			$DESC_2 = 'Existe mas de un usuario';
			session_destroy();
		
		}else if($TOT_USER_VAL == 0 || $TOT_USER_VAL == ''){

			//============================== USUARIO NO EXISTE =================================
			$valor = 0;
			$DESC_1 = 'ERROR LOGUEO - MUEBLES';
			$DESC_2 = 'Usuario no existe';
			session_destroy();	
		}
		
		
	}
	
	echo $valor.'[*]'.$PAGINA;

}else if($operacion=='Cerrar_Session_Modulo_Muebles'){
	
	$sistema 	= $_REQUEST['NOM_MODULO'];
	$usuario 	= $_REQUEST['TXH_NOM_USUARIO'];
	$url_mod 	= $_REQUEST['TXH_URL_MODULO'];
		
	session_destroy();

	


}else if($operacion=='Cambiar_Clave_Sinabip'){
				
	$COD_USUARIO 		= filtrar_caracteres($_REQUEST['v_TXH_SIMI_COD_USUARIO']);
	$NOM_USUARIO 		= filtrar_caracteres($_REQUEST['v_TXH_SIMI_NOM_USUARIO']);
	$NOM_CLAVE 			= filtrar_caracteres($_REQUEST['v_txt_clave_actual_simi']);
	$NUEVO_NOM_CLAVE 	= filtrar_caracteres($_REQUEST['v_txt_clave_nueva_simi']);
	
	$Crypttext	=	$rdo->enviarCrypttext();
	
	/*
		$RU			=	$oSimi_Web_User->Verifica_Existe_Usuario_Simi_Activo($NOM_USUARIO, $NOM_CLAVE, $COD_USUARIO, $Crypttext);
		$TOT_USER	= 	odbc_result($RU,"TOT_USER");
	*/

	/* CREACION:        WILLIAMS ARENAS */
    /* FECHA CREACION:  26-05-2020 */
    /* DESCRIPCION:     VERIFICAR EXISTENCIA DE USUARIOS */
    $data = array();    
    
    $dataModel_01 = [
      'COD_USUARIO'	=>$COD_USUARIO,
      'NOM_USUARIO'	=>$NOM_USUARIO,
      'NOM_CLAVE'	=>$NOM_CLAVE,
      'Crypttext'	=>$Crypttext
    ];
    
    $data['DataVerificarUsuario'] = $objModel->Verifica_Existe_Usuario_Simi_Activo2($dataModel_01);
    $TOT_USER = $data['DataVerificarUsuario'][0]['TOT_USER'];
    /* FIN DE OBTENER TOTAL DE REGISTROS DE PREDIOS */
	//echo $TOT_USER;
	//die();
	if($TOT_USER == 1 ){
		
		$oSimi_Web_User->Actualiza_Clave_Usuario_Simi($NOM_USUARIO, $NUEVO_NOM_CLAVE, $COD_USUARIO, $Crypttext);
		$valor = 1;
		
	}else{
		
		$valor = 5;
	}
	
	echo $valor;
	
}
	

?>