<?php

class CtaContableModel extends BaseModel{
	
   private $oDBManager;
    	
	public function __construct(){
		$this->connect();
	}
	
    	
	
	
	function Simi_Listar_Cuenta_Contable_x_Parametros($filtros=[]){
		$params = [];	
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['USO_CUENTA'], "tipo"=>"string"];	
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['TIP_CUENTA'], "tipo"=>"string"];	
		
		$sqlQuery = "
			SELECT *, 
			TEXT_NRO_CTA_CONTABLE = LEFT(NRO_CTA_CONTABLE+'**************',(SELECT max(len(NRO_CTA_CONTABLE)) 
			FROM TBL_MUEBLES_CUENTA_CONTABLE)+5)
			FROM TBL_MUEBLES_CUENTA_CONTABLE 
			WHERE ID_ESTADO = 1 AND USO_CUENTA = (?) AND TIP_CUENTA = (?)
		";
		return $this->Consultar($sqlQuery, $params);
	}
	
	
	function Simi_Ver_Cuenta_Contable_x_Grupo_Clase($filtros=[]){
		$params = [];	
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_GRUPO'], "tipo"=>"int"];	
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_CLASE'], "tipo"=>"int"];	
		
		$sqlQuery = "
			SELECT 
			GC.COD_DET_GRUPO_CLASE,
			GC.COD_GRUPO,
			GC.COD_CLASE,
			GC.COD_CTA_CONTABLE_DEPREC,
			GC.COD_CTA_CONTABLE,
			CC.USO_CUENTA,
			CC.TIP_CUENTA,
			CC.NRO_MAYOR,
			CC.NRO_SUBCUENTA,
			CC.NRO_CTA_CONTABLE,
			CC.NOM_CTA_CONTABLE
			FROM TBL_MUEBLES_GRUPO_CLASE GC
			LEFT JOIN TBL_MUEBLES_CUENTA_CONTABLE CC ON (GC.COD_CTA_CONTABLE = CC.COD_CTA_CONTABLE AND CC.ID_ESTADO = '1' )
			WHERE GC.ID_ESTADO = '1' AND GC.COD_GRUPO = (?) AND GC.COD_CLASE = (?)
		";
		return $this->Consultar($sqlQuery, $params);
	}
	
	
}
?>