<?php
//require_once("../../Controller/C_Interconexion_SQL.php");

class UbigeoModel{
	
   private $oDBManager;
    	
	public function __construct(){
		$this->oDBManager	=	new Database;
	}
    
	
	function Lista_Region(){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT 
						CODUBIGEO,
						CODREGION AS COD_REGION,
						CODDEP AS COD_DPTO,
						CODPROV AS COD_PROV,
						CODDIST AS COD_DIST,
						NMBUBIGEO AS REGION,
						FLAG,
						EQUIVALE
						FROM TBL_UBIGEO WHERE FLAG = 'R' AND CODDEP ='00' AND CODPROV = '00' AND CODDIST = '00'
						ORDER BY NMBUBIGEO
						";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Lista_Departamento(){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT 
						CODUBIGEO,
						CODREGION AS COD_REGION,
						CODDEP AS COD_DPTO,
						CODPROV AS COD_PROV,
						CODDIST AS COD_DIST,
						NMBUBIGEO AS DEPARTAMENTO,
						FLAG,
						EQUIVALE
						FROM TBL_UBIGEO WHERE FLAG = 'D' AND CODREGION !='00' AND CODDIST = '00' AND CODPROV = '00' 
						ORDER BY NMBUBIGEO
						";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Lista_Provincia($cod_dpto){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT 
						CODUBIGEO,
						CODREGION AS COD_REGION,
						CODDEP AS COD_DPTO,
						CODPROV AS COD_PROV,
						CODDIST AS COD_DIST,
						NMBUBIGEO AS PROVINCIA,
						FLAG,
						EQUIVALE
						FROM TBL_UBIGEO WHERE FLAG = 'P' AND CODDEP = '$cod_dpto' AND CODDIST = '00'
						ORDER BY NMBUBIGEO
						";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Lista_Distrito($cod_dpto, $cod_prov){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT 
						CODUBIGEO,
						CODREGION AS COD_REGION,
						CODDEP AS COD_DPTO,
						CODPROV AS COD_PROV,
						CODDIST AS COD_DIST,
						NMBUBIGEO AS DISTRITO,
						FLAG,
						EQUIVALE
						FROM TBL_UBIGEO WHERE FLAG = 'T' AND CODDEP = '$cod_dpto' AND CODPROV = '$cod_prov'
						ORDER BY NMBUBIGEO
						";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	//------------------------------------------------------
	
	function Lista_Departamento_x_ID($CODDEP){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT 
						CODUBIGEO,
						CODREGION AS COD_REGION,
						CODDEP AS COD_DPTO,
						CODPROV AS COD_PROV,
						CODDIST AS COD_DIST,
						NMBUBIGEO AS DEPARTAMENTO,
						FLAG,
						EQUIVALE
						FROM TBL_UBIGEO WHERE FLAG = 'D' AND CODREGION !='00' AND CODDIST = '00' AND CODPROV = '00' AND CODDEP = '$CODDEP'
						";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Lista_Provincia_x_ID($cod_dpto, $CODPROV){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT 
						CODUBIGEO,
						CODREGION AS COD_REGION,
						CODDEP AS COD_DPTO,
						CODPROV AS COD_PROV,
						CODDIST AS COD_DIST,
						NMBUBIGEO AS PROVINCIA,
						FLAG,
						EQUIVALE
						FROM TBL_UBIGEO WHERE FLAG = 'P' AND CODDEP = '$cod_dpto' AND CODDIST = '00' AND CODPROV = '$CODPROV'
						";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Lista_Distrito_x_ID($cod_dpto, $cod_prov, $CODDIST){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT 
						CODUBIGEO,
						CODREGION AS COD_REGION,
						CODDEP AS COD_DPTO,
						CODPROV AS COD_PROV,
						CODDIST AS COD_DIST,
						NMBUBIGEO AS DISTRITO,
						FLAG,
						EQUIVALE
						FROM TBL_UBIGEO WHERE FLAG = 'T' AND CODDEP = '$cod_dpto' AND CODPROV = '$cod_prov' AND CODDIST = '$CODDIST'
						";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	/***************************manuel ***************************/
	
	function Lista_Departamento_SUNAT(){
		if($this->oDBManager->conectar()==true){
			$consulta="  SELECT 
					  coddep AS COD_DPTO, 
					  nmbubigeo AS DEPARTAMENTO
					  from tbl_ubigeo_sunat 
					  WHERE flag = 'D' 
					  ORDER BY nmbubigeo 
						";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	function Lista_Provincia_SUNAT($cod_dpto){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT 
					  coddep AS COD_DPTO, 
					  codprov AS COD_PROV, 
					  nmbubigeo AS PROVINCIA 
					  from tbl_ubigeo_sunat 
					  WHERE flag = 'P' and coddep = '$cod_dpto' 
					  ORDER BY nmbubigeo 					  
						";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	
	function Lista_Distrito_SUNAT($cod_dpto, $cod_prov){
		if($this->oDBManager->conectar()==true){
			$consulta="SELECT 
					  coddep AS COD_DPTO, 
					  codprov AS COD_PROV, 
					  coddist AS COD_DIST, 
					  nmbubigeo AS DISTRITO
					  from TBL_UBIGEO_SUNAT 
					  WHERE flag = 'T' and coddep = '$cod_dpto' and codprov = '$cod_prov' 
					  ORDER BY nmbubigeo 
						";
			$result = $this->oDBManager->execute($consulta);
			return $result;
		}
	}
	
	
}
?>