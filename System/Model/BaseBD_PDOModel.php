<?php
class BaseModel extends DB{

  public function Ejecutar($psentencia='',$parametros=[])
  {
    // echo "<br />Model.Ejecutar : ";

    for ($i = 0; $i < count($parametros); $i++) {
        $this->createParameter($parametros[$i]["nombre"], $parametros[$i]["valor"], $parametros[$i]["tipo"]);
    }
    return $this->ExecuteNonQuery($psentencia);
     // $this->ExecuteNonQuery($psentencia);
     // return true;
  }

  public function Consultar($psentencia='', $parametros=[])
  {
    // echo "<br />Model.Consultar : ";
	
    for ($i = 0; $i < count($parametros); $i++) {      
        $this->createParameter($parametros[$i]["nombre"], $parametros[$i]["valor"], $parametros[$i]["tipo"]);
    }
    //var_dump($parametros);
	 //print_r($psentencia); die();
    return $this->ExecuteReader($psentencia);
  }

  public function ObtenerValor($psentencia='', $parametros=[])
  {
    // echo "<br />Model.ObtenerValor : ";

    for ($i = 0; $i < count($parametros); $i++) {
        $this->createParameter($parametros[$i]["nombre"], $parametros[$i]["valor"], $parametros[$i]["tipo"]);
    }
    return $this->ExecuteScalar($psentencia);
  }
  
  public function ImprimirSentencia($psentencia='', $parametros=[]) {
	
		$values = array();
				
		foreach ($parametros as $key => $value) {
			
			$marker = $key;
			$values[$key] = $value['valor'];
			
			if (is_numeric($marker)) {
				$marker = "\?";
			} else {
				$marker = (preg_match("/^:/", $marker)) ? $marker : ":" . $marker;
			}
			
			
			$testParam = "/({$marker}(?!\w))(?=(?:[^\"']|[\"'][^\"']*[\"'])*$)/";
		
        	$psentencia = preg_replace($testParam, $values[$key], $psentencia, 1);
			
		}
		
		//print_r($psentencia);
		//print_r($values);
		return $psentencia;
	}
}
