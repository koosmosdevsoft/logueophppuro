<?php
class BienPatrimonialModel extends BaseModel{

   private $oDBManager;

	public function __construct(){
		$this->connect();
	}

	function Ver_Datos_Bien_Patrimonial_x_ID($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_UE_BIEN_PATRI'], "tipo"=>"int"];

		$sqlQuery = "
			SELECT
			BP.*,
			NOM_FAMILIA_BIEN = CAT.DENOM_BIEN,
			G.NRO_GRUPO,
			G.DESC_GRUPO,
			CL.NRO_CLASE,
			CL.DESC_CLASE,
			CAT.COD_GRUPO,
			CAT.COD_CLASE,
			CL.DEPRECIACION,
			ABREV_ALTA = FA.CONDICION,
			FA.NOM_FORM_ADQUIS,
			AB.NRO_DOCUMENTO_ADQUIS,
			FECHA_DOCUMENTO_ADQUIS = CONVERT(VARCHAR(10),AB.FECHA_DOCUMENTO_ADQUIS,105)
			FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL BP
			LEFT JOIN TBL_MUEBLES_CATALOGO  CAT ON (CAT.ID_CATALOGO = BP.ID_CATALOGO)
			LEFT JOIN TBL_MUEBLES_GRUPO G ON (G.COD_GRUPO = CAT.COD_GRUPO)
			LEFT JOIN TBL_MUEBLES_CLASE CL ON (CL.COD_CLASE = CAT.COD_CLASE)
			LEFT JOIN TBL_MUEBLES_UE_BIEN_ALTA AB ON (AB.COD_UE_BIEN_ALTA = BP.COD_UE_BIEN_ALTA)
			LEFT JOIN TBL_MUEBLES_FORM_ADQUISICION FA ON (FA.COD_FORM_ADQUIS = AB.COD_FORM_ADQUIS)
			WHERE COD_UE_BIEN_PATRI = ?
		";
		return $this->Consultar($sqlQuery, $params);
	}


	function Actualiza_Datos_Bien_Patrimonial_Caracteristicas($filtros=[]){
			$params = [];

			$params[0] = ["nombre"=>(1), "valor"=>$filtros['DENOMINACION_BIEN'], "tipo"=>"string"];
			$params[1] = ["nombre"=>(2), "valor"=>$filtros['USO_CUENTA'], "tipo"=>"string"];
			$params[2] = ["nombre"=>(3), "valor"=>$filtros['TIP_CUENTA'], "tipo"=>"string"];
			$params[3] = ["nombre"=>(4), "valor"=>$filtros['COD_CTA_CONTABLE'], "tipo"=>"int"];
			$params[4] = ["nombre"=>(5), "valor"=>$filtros['VALOR_ADQUIS'], "tipo"=>"string"];
			$params[5] = ["nombre"=>(6), "valor"=>$filtros['PORC_DEPREC'], "tipo"=>"string"];
			$params[6] = ["nombre"=>(7), "valor"=>$filtros['OPC_ASEGURADO'], "tipo"=>"string"];
			$params[7] = ["nombre"=>(8), "valor"=>$filtros['COD_ESTADO_BIEN'], "tipo"=>"int"];
			$params[8] = ["nombre"=>(9), "valor"=>$filtros['OBSERVACION'], "tipo"=>"string"];
			$params[9] = ["nombre"=>(10), "valor"=>$filtros['MARCA'], "tipo"=>"string"];
			$params[10] = ["nombre"=>(11), "valor"=>$filtros['MODELO'], "tipo"=>"string"];
			$params[11] = ["nombre"=>(12), "valor"=>$filtros['TIPO'], "tipo"=>"string"];
			$params[12] = ["nombre"=>(13), "valor"=>$filtros['COLOR'], "tipo"=>"string"];
			$params[13] = ["nombre"=>(14), "valor"=>$filtros['SERIE'], "tipo"=>"string"];
			$params[14] = ["nombre"=>(15), "valor"=>$filtros['NRO_MOTOR'], "tipo"=>"string"];
			$params[15] = ["nombre"=>(16), "valor"=>$filtros['PLACA'], "tipo"=>"string"];
			$params[16] = ["nombre"=>(17), "valor"=>$filtros['DIMENSION'], "tipo"=>"string"];
			$params[17] = ["nombre"=>(18), "valor"=>$filtros['NRO_CHASIS'], "tipo"=>"string"];
			$params[18] = ["nombre"=>(19), "valor"=>$filtros['MATRICULA'], "tipo"=>"string"];
			$params[19] = ["nombre"=>(20), "valor"=>$filtros['ANIO_FABRICACION'], "tipo"=>"string"];
			$params[20] = ["nombre"=>(21), "valor"=>$filtros['LONGITUD'], "tipo"=>"string"];
			$params[21] = ["nombre"=>(22), "valor"=>$filtros['ALTURA'], "tipo"=>"string"];
			$params[22] = ["nombre"=>(23), "valor"=>$filtros['ANCHO'], "tipo"=>"string"];
			$params[23] = ["nombre"=>(24), "valor"=>$filtros['RAZA'], "tipo"=>"string"];
			$params[24] = ["nombre"=>(25), "valor"=>$filtros['ESPECIE'], "tipo"=>"string"];
			$params[25] = ["nombre"=>(26), "valor"=>$filtros['EDAD'], "tipo"=>"string"];
			$params[26] = ["nombre"=>(27), "valor"=>$filtros['PAIS'], "tipo"=>"string"];
			$params[27] = ["nombre"=>(28), "valor"=>$filtros['OTRAS_CARACT'], "tipo"=>"string"];
			$params[28] = ["nombre"=>(29), "valor"=>$filtros['SIMI_USUARIO'], "tipo"=>"int"];
			$params[29] = ["nombre"=>(30), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
			$params[30] = ["nombre"=>(31), "valor"=>$filtros['COD_UE_BIEN_PATRI'], "tipo"=>"int"];

			//print_r($params);

			$sqlQuery = "
				UPDATE TBL_MUEBLES_UE_BIEN_PATRIMONIAL SET
				DENOMINACION_BIEN = ?,
				USO_CUENTA = ?,
				TIP_CUENTA = ?,
				COD_CTA_CONTABLE = ?,
				VALOR_ADQUIS = ?,
				PORC_DEPREC = ?,
				OPC_ASEGURADO = ?,
				COD_ESTADO_BIEN = ?,
				OBSERVACION = ?,
				MARCA = ?,
				MODELO = ?,
				TIPO = ?,
				COLOR = ?,
				SERIE = ?,
				NRO_MOTOR = ?,
				PLACA = ?,
				DIMENSION = ?,
				NRO_CHASIS = ?,
				MATRICULA = ?,
				ANIO_FABRICACION = ?,
				LONGITUD = ?,
				ALTURA = ?,
				ANCHO = ?,
				RAZA = ?,
				ESPECIE = ?,
				EDAD = ?,
				PAIS = ?,
				OTRAS_CARACT = ?,
				SIMI_USUARIO_MODIFICA = ?,
				SIMI_FECHA_MODIFICA = GETDATE(),
				ID_ESTADO = 1
				WHERE COD_ENTIDAD = ? AND COD_UE_BIEN_PATRI = ?
			";
			return $this->Ejecutar($sqlQuery, $params);
	}


	function Eliminar_Bien_Patrimonial($filtros=[]){
			$params = [];
			$params[0] = ["nombre"=>(1), "valor"=>$filtros['MOTIVO_ELIMINACION'], "tipo"=>"string"];
			$params[1] = ["nombre"=>(2), "valor"=>$filtros['SIMI_USUARIO'], "tipo"=>"int"];
			$params[2] = ["nombre"=>(3), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
			$params[3] = ["nombre"=>(4), "valor"=>$filtros['COD_UE_BIEN_PATRI'], "tipo"=>"int"];

			//print_r($params);
			$sqlQuery = "
				UPDATE TBL_MUEBLES_UE_BIEN_PATRIMONIAL SET
				ID_ESTADO = 2,
				ALMACEN = NULL,
				COD_TIPO_MOV = 8, --ELIMINACION
				MOTIVO_ELIMINACION = ?,
				SIMI_USUARIO_ELIMINA = ?,
				SIMI_FECHA_ELIMINA = GETDATE()
				WHERE COD_ENTIDAD = ? AND COD_UE_BIEN_PATRI = ?
			";
			return $this->Ejecutar($sqlQuery, $params);
	}

	function Total_Bienes_Patrimoniales_x_Parametros($filtros=[]){
			$params = [];
			$params[0] = ["nombre"=>(1), "valor"=>$filtros['TXH_COD_ENTIDAD'], "tipo"=>"int"];
			$params[1] = ["nombre"=>(2), "valor"=>$filtros['busq_Grupo_Generico'], "tipo"=>"string"];
			$params[2] = ["nombre"=>(3), "valor"=>$filtros['busq_Grupo_Generico'], "tipo"=>"string"];
			$params[3] = ["nombre"=>(4), "valor"=>$filtros['busq_Clase_generico'], "tipo"=>"string"];
			$params[4] = ["nombre"=>(5), "valor"=>$filtros['busq_Clase_generico'], "tipo"=>"string"];
			$params[5] = ["nombre"=>(6), "valor"=>$filtros['busq_cod_bien'], "tipo"=>"string"];
			$params[6] = ["nombre"=>(7), "valor"=>$filtros['busq_cod_bien'], "tipo"=>"string"];
			$params[7] = ["nombre"=>(8), "valor"=>$filtros['busq_denom_bien'], "tipo"=>"string"];
			$params[8] = ["nombre"=>(9), "valor"=>$filtros['busq_denom_bien'], "tipo"=>"string"];

			$sqlQuery = "
				SELECT COUNT(*) AS TOT_REG
				FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL B
				LEFT JOIN TBL_MUEBLES_CATALOGO CAT ON (CAT.ID_CATALOGO = B.ID_CATALOGO)
				LEFT JOIN TBL_MUEBLES_GRUPO G ON (G.COD_GRUPO = CAT.COD_GRUPO)
				LEFT JOIN TBL_MUEBLES_CLASE CL ON (CL.COD_CLASE = CAT.COD_CLASE)
				WHERE B.ID_ESTADO = 1
				AND B.COD_ENTIDAD = ?
				AND B.COD_TIPO_MOV IN(1,2,3,4)
				AND (( ? = '') OR (CAT.COD_GRUPO = ?))
				AND (( ? = '') OR (CAT.COD_CLASE = ?))
				AND (( ? = '') OR (B.CODIGO_PATRIMONIAL LIKE '%'+ ? +'%'))
				AND (( ? = '') OR (B.DENOMINACION_BIEN LIKE '%'+ ? +'%'))
			";
			return $this->ObtenerValor($sqlQuery, $params);
	}


	function Listar_Bienes_Patrimoniales_x_Parametros($filtros=[]){
			$params = [];
			$params[0] = ["nombre"=>(1), "valor"=>$filtros['TXH_COD_ENTIDAD'], "tipo"=>"int"];
			$params[1] = ["nombre"=>(2), "valor"=>$filtros['busq_Grupo_Generico'], "tipo"=>"string"];
			$params[2] = ["nombre"=>(3), "valor"=>$filtros['busq_Grupo_Generico'], "tipo"=>"string"];
			$params[3] = ["nombre"=>(4), "valor"=>$filtros['busq_Clase_generico'], "tipo"=>"string"];
			$params[4] = ["nombre"=>(5), "valor"=>$filtros['busq_Clase_generico'], "tipo"=>"string"];
			$params[5] = ["nombre"=>(6), "valor"=>$filtros['busq_cod_bien'], "tipo"=>"string"];
			$params[6] = ["nombre"=>(7), "valor"=>$filtros['busq_cod_bien'], "tipo"=>"string"];
			$params[7] = ["nombre"=>(8), "valor"=>$filtros['busq_denom_bien'], "tipo"=>"string"];
			$params[8] = ["nombre"=>(9), "valor"=>$filtros['busq_denom_bien'], "tipo"=>"string"];
			$params[9] = ["nombre"=>(10), "valor"=>$filtros['Item_Ini'], "tipo"=>"int"];
			$params[10] = ["nombre"=>(11), "valor"=>$filtros['Item_Fin'], "tipo"=>"int"];

			//print_r($params);

			$SQL_TABLA = "
				SELECT
				ROW_NUMBER() OVER (order by B.CODIGO_PATRIMONIAL) AS ROW_NUMBER_ID,
				B.*,
				CAT.COD_GRUPO,
				CAT.NRO_GRUPO,
				G.DESC_GRUPO,
				CAT.COD_CLASE,
				CAT.NRO_CLASE,
				CL.DESC_CLASE,
				AB.NRO_DOCUMENTO_ADQUIS,
				FECHA_DOCUMENTO_ADQUIS = CONVERT(VARCHAR(10),AB.FECHA_DOCUMENTO_ADQUIS,105)
				FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL B
				LEFT JOIN TBL_MUEBLES_CATALOGO CAT ON (CAT.ID_CATALOGO = B.ID_CATALOGO)
				LEFT JOIN TBL_MUEBLES_GRUPO G ON (G.COD_GRUPO = CAT.COD_GRUPO)
				LEFT JOIN TBL_MUEBLES_CLASE CL ON (CL.COD_CLASE = CAT.COD_CLASE)
				LEFT JOIN TBL_MUEBLES_UE_BIEN_ALTA AB ON (AB.COD_UE_BIEN_ALTA = B.COD_UE_BIEN_ALTA)
				WHERE B.ID_ESTADO = 1
				AND B.COD_ENTIDAD = ? 
				AND B.COD_TIPO_MOV IN(1,2,3,4)
				AND (( ? = '') OR (CAT.COD_GRUPO = ?))
				AND (( ? = '') OR (CAT.COD_CLASE = ?))
				AND (( ? = '') OR (B.CODIGO_PATRIMONIAL LIKE '%'+ ? +'%'))
				AND (( ? = '') OR (B.DENOMINACION_BIEN LIKE '%'+ ? +'%'))";
			$sqlQuery = "SELECT * FROM ( ".$SQL_TABLA." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN ? AND ? ";
			return $this->Consultar($sqlQuery, $params);
	}


	//===============================================================================


	function Listar_Bien_Disponible_x_Parametros_Para_Devolucion($filtros=[]){

			$params = [];
			$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
			$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_UE_PERS'], "tipo"=>"int"];
			$params[2] = ["nombre"=>(3), "valor"=>$filtros['COD_GRUPO'], "tipo"=>"int"];
			$params[3] = ["nombre"=>(4), "valor"=>$filtros['COD_GRUPO'], "tipo"=>"int"];
			$params[4] = ["nombre"=>(5), "valor"=>$filtros['COD_CLASE'], "tipo"=>"int"];
			$params[5] = ["nombre"=>(6), "valor"=>$filtros['COD_CLASE'], "tipo"=>"int"];
			$params[6] = ["nombre"=>(7), "valor"=>$filtros['CODIGO_PATRIMONIAL'], "tipo"=>"string"];
			$params[7] = ["nombre"=>(8), "valor"=>$filtros['CODIGO_PATRIMONIAL'], "tipo"=>"string"];
			$params[8] = ["nombre"=>(9), "valor"=>$filtros['DENOM_BIEN_PATRIMONIAL'], "tipo"=>"string"];
			$params[9] = ["nombre"=>(10), "valor"=>$filtros['DENOM_BIEN_PATRIMONIAL'], "tipo"=>"string"];
			$params[10] = ["nombre"=>(11), "valor"=>$filtros['Item_Ini'], "tipo"=>"int"];
			$params[11] = ["nombre"=>(12), "valor"=>$filtros['Item_Fin'], "tipo"=>"int"];

			//print_r($params);

			$SQL_TABLA = "
				SELECT
				ROW_NUMBER() OVER (ORDER BY C.NUMERO_ASIG, BP.CODIGO_PATRIMONIAL) AS ROW_NUMBER_ID,
				BP.COD_UE_BIEN_PATRI,
				BP.ID_CATALOGO,
				BP.CODIGO_PATRIMONIAL,
				BP.DENOMINACION_BIEN,
				CA.COD_GRUPO,
				G.NRO_GRUPO,
				G.DESC_GRUPO,
				CA.COD_CLASE,
				CL.NRO_CLASE,
				CL.DESC_CLASE,
				BP.ALMACEN,
				BP.COD_TIPO_MOV,
				TM.DES_TIPO_MOV,
				C.RECEP_COD_UE_PERS,
				C.NUMERO_ASIG,
				FECHA_ASIG = CONVERT(VARCHAR(10),C.FECHA_REGISTRO,105),
				D.COD_UE_BIEN_ASIG,
				D.COD_UE_BIEN_ASIG_DET
				FROM TBL_MUEBLES_UE_BIEN_ASIGNACION_DET D
				LEFT JOIN TBL_MUEBLES_UE_BIEN_ASIGNACION C ON (D.COD_ENTIDAD = C.COD_ENTIDAD AND D.COD_UE_BIEN_ASIG = C.COD_UE_BIEN_ASIG)
				LEFT JOIN TBL_MUEBLES_UE_BIEN_PATRIMONIAL BP ON (D.COD_ENTIDAD = BP.COD_ENTIDAD AND D.COD_UE_BIEN_PATRI = BP.COD_UE_BIEN_PATRI)
				LEFT JOIN TBL_MUEBLES_CATALOGO CA ON (BP.ID_CATALOGO = CA.ID_CATALOGO)
				LEFT JOIN TBL_MUEBLES_GRUPO G ON (CA.COD_GRUPO = G.COD_GRUPO)
				LEFT JOIN TBL_MUEBLES_CLASE CL ON (CA.COD_CLASE = CL.COD_CLASE)
				LEFT JOIN TBL_MUEBLES_UE_TIPO_MOV TM ON (TM.COD_TIPO_MOV = BP.COD_TIPO_MOV)
				WHERE D.ID_ESTADO = 1
				AND C.CONDICION = 1
				AND BP.ID_ESTADO = 1
				AND ISNULL(D.DEVUELTO,'') != 'X'
				--AND BP.COD_TIPO_MOV IN (2) --ASIGNACION
				--AND BP.COD_TIPO_MOV NOT IN (1, 3, 4, 5, 6, 7, 8)
				AND BP.COD_TIPO_MOV NOT IN (1, 3, 5, 6, 7, 8)
				AND D.COD_ENTIDAD = ?
				AND C.RECEP_COD_UE_PERS = ?
				AND (( ? = '') OR (CA.COD_GRUPO = ?))
				AND (( ? = '') OR (CA.COD_CLASE = ?))
				AND (( ? = '') OR (BP.CODIGO_PATRIMONIAL LIKE '%'+ ? +'%'))
				AND (( ? = '') OR (BP.DENOMINACION_BIEN LIKE '%'+ ? +'%'))
				";

			$sqlQuery = "SELECT * FROM ( ".$SQL_TABLA." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN (?) AND (?) ";
			
			return $this->Consultar($sqlQuery, $params);
	}


	function Listar_Bien_Disponible_x_Parametros_Para_Traslado($filtros=[]){

			$params = [];
			$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
			$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_UE_PERS'], "tipo"=>"int"];
			$params[2] = ["nombre"=>(3), "valor"=>$filtros['COD_GRUPO'], "tipo"=>"int"];
			$params[3] = ["nombre"=>(4), "valor"=>$filtros['COD_GRUPO'], "tipo"=>"int"];
			$params[4] = ["nombre"=>(5), "valor"=>$filtros['COD_CLASE'], "tipo"=>"int"];
			$params[5] = ["nombre"=>(6), "valor"=>$filtros['COD_CLASE'], "tipo"=>"int"];
			$params[6] = ["nombre"=>(7), "valor"=>$filtros['CODIGO_PATRIMONIAL'], "tipo"=>"string"];
			$params[7] = ["nombre"=>(8), "valor"=>$filtros['CODIGO_PATRIMONIAL'], "tipo"=>"string"];
			$params[8] = ["nombre"=>(9), "valor"=>$filtros['DENOM_BIEN_PATRIMONIAL'], "tipo"=>"string"];
			$params[9] = ["nombre"=>(10), "valor"=>$filtros['DENOM_BIEN_PATRIMONIAL'], "tipo"=>"string"];
			$params[10] = ["nombre"=>(11), "valor"=>$filtros['Item_Ini'], "tipo"=>"int"];
			$params[11] = ["nombre"=>(12), "valor"=>$filtros['Item_Fin'], "tipo"=>"int"];
			
			ECHO $xCOD_UE_BIEN_TRAS = $filtros['COD_UE_BIEN_TRAS'];		
			//print_r($params);

			$SQL_TABLA = "
				SELECT
				ROW_NUMBER() OVER (ORDER BY C.NUMERO_ASIG, BP.CODIGO_PATRIMONIAL) AS ROW_NUMBER_ID,
				BP.COD_UE_BIEN_PATRI,
				BP.ID_CATALOGO,
				BP.CODIGO_PATRIMONIAL,
				BP.DENOMINACION_BIEN,
				CA.COD_GRUPO,
				G.NRO_GRUPO,
				G.DESC_GRUPO,
				CA.COD_CLASE,
				CL.NRO_CLASE,
				CL.DESC_CLASE,
				BP.ALMACEN,
				BP.COD_TIPO_MOV,
				TM.DES_TIPO_MOV,
				C.RECEP_COD_UE_PERS,
				C.NUMERO_ASIG,
				FECHA_ASIG = CONVERT(VARCHAR(10),C.FECHA_REGISTRO,105),
				D.COD_UE_BIEN_ASIG,
				D.COD_UE_BIEN_ASIG_DET,
				D.COD_UE_OFIC_UBIC,
				SIGLA_OFIC = (CASE WHEN ISNULL(OFIC.SIGLA_OFIC,'') = '' THEN 'Sin Abrev.' ELSE OFIC.SIGLA_OFIC END)
				FROM TBL_MUEBLES_UE_BIEN_ASIGNACION_DET D
				LEFT JOIN TBL_MUEBLES_UE_BIEN_ASIGNACION C ON (D.COD_ENTIDAD = C.COD_ENTIDAD AND D.COD_UE_BIEN_ASIG = C.COD_UE_BIEN_ASIG)
				LEFT JOIN TBL_MUEBLES_UE_BIEN_PATRIMONIAL BP ON (D.COD_ENTIDAD = BP.COD_ENTIDAD AND D.COD_UE_BIEN_PATRI = BP.COD_UE_BIEN_PATRI)
				LEFT JOIN TBL_MUEBLES_CATALOGO CA ON (BP.ID_CATALOGO = CA.ID_CATALOGO)
				LEFT JOIN TBL_MUEBLES_GRUPO G ON (CA.COD_GRUPO = G.COD_GRUPO)
				LEFT JOIN TBL_MUEBLES_CLASE CL ON (CA.COD_CLASE = CL.COD_CLASE)
				LEFT JOIN TBL_MUEBLES_UE_TIPO_MOV TM ON (TM.COD_TIPO_MOV = BP.COD_TIPO_MOV)
				LEFT JOIN TBL_MUEBLES_UE_OFICINA OFIC ON (OFIC.COD_UE_OFIC = D.COD_UE_OFIC_UBIC)
				WHERE D.ID_ESTADO = 1
				AND C.CONDICION = 1
				AND BP.ID_ESTADO = 1
				AND ISNULL(D.DEVUELTO,'') != 'X'
				--AND ISNULL(D.TRASLADADO,'') != 'X'
				--AND BP.COD_TIPO_MOV IN (2) --ASIGNACION
				--AND BP.COD_TIPO_MOV NOT IN (1, 3, 4, 5, 6, 7, 8)
				AND BP.COD_TIPO_MOV NOT IN (1, 3, 5, 6, 7, 8)
				AND D.COD_ENTIDAD = ?
				AND C.RECEP_COD_UE_PERS = ?
				AND (( ? = '') OR (CA.COD_GRUPO = ?))
				AND (( ? = '') OR (CA.COD_CLASE = ?))
				AND (( ? = '') OR (BP.CODIGO_PATRIMONIAL LIKE '%'+ ? +'%'))
				AND (( ? = '') OR (BP.DENOMINACION_BIEN LIKE '%'+ ? +'%'))
				AND D.COD_UE_BIEN_PATRI NOT IN (SELECT DT.COD_UE_BIEN_PATRI FROM TBL_MUEBLES_UE_BIEN_TRASLADO_DET DT WHERE COD_UE_BIEN_TRAS = '$xCOD_UE_BIEN_TRAS')
				";

			$sqlQuery = "SELECT * FROM ( ".$SQL_TABLA." ) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN (?) AND (?) ";
			
			return $this->Consultar($sqlQuery, $params);
	}


	//=========================================================================

	function Simi_Listar_Estado_Bien(){
		$params = [];
		$sqlQuery = "SELECT * FROM TBL_MUEBLES_ESTADO_BIEN WHERE ID_ESTADO = '1' ORDER BY 1 ";
		return $this->Consultar($sqlQuery, $params);
	}


	//=========================================================================

	function Insertar_Bien_Historico($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['SIMI_USUARIO'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_UE_BIEN_PATRI'], "tipo"=>"int"];

		$sqlQuery = "
		INSERT INTO TBL_MUEBLES_UE_BIEN_PATRIMONIAL_UPDATE (COD_UE_BIEN_PATRI,
		COD_ENTIDAD,
		ID_CATALOGO,
		CODIGO_PATRIMONIAL,
		DENOMINACION_BIEN,
		NRO_BIEN,
		NRO_BIEN_CORRELATIVO,
		COD_CORRELATIVO,
		COD_UE_BIEN_ALTA,
		USO_CUENTA,
		TIP_CUENTA,
		COD_CTA_CONTABLE,
		VALOR_ADQUIS,
		VALOR_NETO,
		COD_ESTADO_BIEN,
		OPC_ASEGURADO,
		COND_BIEN,
		OBSERVACION,
		COD_UE_BIEN_DEPREC,
		ALMACEN,
		ENVIADO_SBN,
		FECHA_ENVIADO_SBN,
		FECHA_DEVUELTO,
		ID_PREDIO_SBN,
		COD_UE_AREA,
		COD_UE_OFIC,
		COD_UE_PERS,
		COD_UE_BIEN_ASIG,
		COD_UE_BIEN_BAJA,
		COD_UE_BIEN_DISPOSIC,
		COD_UE_BIEN_ADMINIS,
		COD_UE_BIEN_DEVOL,
		FECHA_REGISTRO,
		SIMI_USUARIO_CREACION,
		SIMI_FECHA_CREACION,
		SIMI_USUARIO_MODIFICA,
		SIMI_FECHA_MODIFICA,
		SIMI_USUARIO_ELIMINA,
		SIMI_FECHA_ELIMINA,
		ID_ESTADO,
		COD_IMPORT_EXCEL_FILE,
		DOC_ALTA_SBN,
		DOC_BAJA_SBN,
		ID_PREDIO_EXCEL,
		MARCA,
		MODELO,
		TIPO,
		COLOR,
		SERIE,
		DIMENSION,
		PLACA,
		NRO_MOTOR,
		NRO_CHASIS,
		MATRICULA,
		ANIO_FABRICACION,
		LONGITUD,
		ALTURA,
		ANCHO,
		RAZA,
		ESPECIE,
		EDAD,
		PAIS,
		OTRAS_CARACT,
		PORC_DEPREC,
		COD_TIPO_MOV,
		FECHA_1RA_PECOSA,
		MOTIVO_ELIMINACION
		)
		SELECT
		COD_UE_BIEN_PATRI,
		COD_ENTIDAD,
		ID_CATALOGO,
		CODIGO_PATRIMONIAL,
		DENOMINACION_BIEN,
		NRO_BIEN,
		NRO_BIEN_CORRELATIVO,
		COD_CORRELATIVO,
		COD_UE_BIEN_ALTA,
		USO_CUENTA,
		TIP_CUENTA,
		COD_CTA_CONTABLE,
		VALOR_ADQUIS,
		VALOR_NETO,
		COD_ESTADO_BIEN,
		OPC_ASEGURADO,
		COND_BIEN,
		OBSERVACION,
		COD_UE_BIEN_DEPREC,
		ALMACEN,
		ENVIADO_SBN,
		FECHA_ENVIADO_SBN,
		FECHA_DEVUELTO,
		ID_PREDIO_SBN,
		COD_UE_AREA,
		COD_UE_OFIC,
		COD_UE_PERS,
		COD_UE_BIEN_ASIG,
		COD_UE_BIEN_BAJA,
		COD_UE_BIEN_DISPOSIC,
		COD_UE_BIEN_ADMINIS,
		COD_UE_BIEN_DEVOL,
		FECHA_REGISTRO,
		SIMI_USUARIO_CREACION,
		SIMI_FECHA_CREACION,
		SIMI_USUARIO_MODIFICA = ?,
		SIMI_FECHA_MODIFICA = GETDATE(),
		SIMI_USUARIO_ELIMINA,
		SIMI_FECHA_ELIMINA,
		ID_ESTADO,
		COD_IMPORT_EXCEL_FILE,
		DOC_ALTA_SBN,
		DOC_BAJA_SBN,
		ID_PREDIO_EXCEL,
		MARCA,
		MODELO,
		TIPO,
		COLOR,
		SERIE,
		DIMENSION,
		PLACA,
		NRO_MOTOR,
		NRO_CHASIS,
		MATRICULA,
		ANIO_FABRICACION,
		LONGITUD,
		ALTURA,
		ANCHO,
		RAZA,
		ESPECIE,
		EDAD,
		PAIS,
		OTRAS_CARACT,
		PORC_DEPREC,
		COD_TIPO_MOV,
		FECHA_1RA_PECOSA,
		MOTIVO_ELIMINACION
		FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL
		WHERE COD_UE_BIEN_PATRI = ?
		";
		return $this->Ejecutar($sqlQuery, $params);
	}

	
	//*************************************************************************************
	
	
	function Total_RPT_CtaCble_x_Entidad($filtros=[]){
		$params = [];
		$params[0] = ['nombre'=>(1), 'valor'=>$filtros['COD_ENTIDAD'],'tipo'=>'int'];
		
		//print_r($params);
		
		$sqlQuery = "
		SELECT TOTAL_REGISTRO = COUNT(*) 
		FROM (
		SELECT
		CC.NRO_CTA_CONTABLE,
		CC.NOM_CTA_CONTABLE,
		ADQ = SUM(CAST(BP.VALOR_ADQUIS AS DECIMAL(20,2))),
		NETO = SUM(CAST(BP.VALOR_NETO AS DECIMAL(20,2)))
		FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL BP
		LEFT JOIN TBL_MUEBLES_UE_BIEN_ALTA A ON (BP.COD_UE_BIEN_ALTA = A.COD_UE_BIEN_ALTA AND A.ID_ESTADO = 1)
		LEFT JOIN TBL_MUEBLES_CUENTA_CONTABLE CC ON (CC.COD_CTA_CONTABLE = BP.COD_CTA_CONTABLE AND CC.ID_ESTADO = 1)
		WHERE BP.COD_ENTIDAD = ?
		AND BP.ID_ESTADO = 1
		AND BP.COND_BIEN = 'A'
		GROUP BY CC.NRO_CTA_CONTABLE, CC.NOM_CTA_CONTABLE
		) TBL";
		return $this->ObtenerValor($sqlQuery, $params);
	}


	function Lista_RPT_CtaCble_x_Entidad($filtros=[]){
		$params = [];
		$params[0] = ['nombre'=>(1), 'valor'=>$filtros['COD_ENTIDAD'],'tipo'=>'int'];
		$params[1] = ['nombre'=>(2), 'valor'=>$filtros['Item_Ini'],'tipo'=>'int'];
		$params[2] = ['nombre'=>(3), 'valor'=>$filtros['Item_Fin'],'tipo'=>'int'];

			$SQL_TABLA = "
			SELECT
			ROW_NUMBER() OVER (ORDER BY CC.NRO_CTA_CONTABLE) AS ROW_NUMBER_ID,
			CC.NRO_CTA_CONTABLE,
			CC.NOM_CTA_CONTABLE,
			ADQ = SUM(CAST(BP.VALOR_ADQUIS AS DECIMAL(20,2))),
			NETO = SUM(CAST(BP.VALOR_NETO AS DECIMAL(20,2)))
			FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL BP
			LEFT JOIN TBL_MUEBLES_UE_BIEN_ALTA A ON (BP.COD_UE_BIEN_ALTA = A.COD_UE_BIEN_ALTA AND A.ID_ESTADO = 1)
			LEFT JOIN TBL_MUEBLES_CUENTA_CONTABLE CC ON (CC.COD_CTA_CONTABLE = BP.COD_CTA_CONTABLE AND CC.ID_ESTADO = 1)
			WHERE BP.COD_ENTIDAD = ? 
			AND BP.ID_ESTADO = 1
			AND BP.COND_BIEN = 'A'
			GROUP BY CC.NRO_CTA_CONTABLE, CC.NOM_CTA_CONTABLE
			";
			$sqlQuery = "SELECT * FROM ( ".$SQL_TABLA." ) AS NTABLE WHERE ROW_NUMBER_ID BETWEEN ? AND ? ";
			return $this->Consultar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  25-05-2020 */
	/* DESCRIPCION:     DATOS DE MODULOS */
	function Ver_Datos_Modulos_Simi_x_Codigo2($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['TXH_COD_MODULO'], "tipo"=>"int"];
		//print_r($params);

		$sqlQuery = "SELECT * FROM TBL_MUEBLES_MODULO WHERE COD_MUEBLE_MODULO = ? ";
		// echo $sqlQuery;
		return $this->Consultar($sqlQuery, $params);
	}

	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  25-05-2020 */
	/* DESCRIPCION:     OBTENER TOTAL DE REGISTROS DE PREDIOS */
	function TOTAL_REGISTRO_PREDIOS_X_PARAMETROS2($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['DENOMINACION_PREDIO'], "tipo"=>"string"	];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['DENOMINACION_PREDIO'], "tipo"=>"string"	];
		//print_r($params); 

		$sqlQuery = "SELECT COUNT(*) AS TOT_REG 
			FROM TBL_PADRON_PREDIOS P (NOLOCK)
			WHERE P.ID_ESTADO  IN ('1', '5') 
			AND P.COD_ENTIDAD = ? 
			AND ((? = '') OR (P.DENOMINACION_PREDIO = ?)) ";
			//echo $sqlQuery;
		return $this->Consultar($sqlQuery, $params);
	}



	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  25-05-2020 */
	/* DESCRIPCION:     OBTENER TOTAL DE REGISTROS DE PREDIOS */
	function LISTA_PREDIOS_X_PARAMETROS2($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['DENOMINACION_PREDIO'], "tipo"=>"string"	];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['DENOMINACION_PREDIO'], "tipo"=>"string"	];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['ITEM_INI'], "tipo"=>"int"];
		$params[4] = ["nombre"=>(5), "valor"=>$filtros['ITEM_FIN'], "tipo"=>"int"];

		$sqlQuery ="SELECT * FROM
		(
			SELECT P.*, EP.DESC_EST_PROPIEDAD, TP.DESC_TIP_PROPIEDAD,
			ROW_NUMBER() OVER (ORDER BY P.ID_PREDIO_SBN) AS ROW_NUMBER_ID
			FROM TBL_PADRON_PREDIOS P (NOLOCK)
			LEFT JOIN TBL_TIPO_PROPIEDAD TP (NOLOCK) ON (P.COD_TIP_PROPIEDAD = TP.COD_TIP_PROPIEDAD)		
			LEFT JOIN TBL_ESTADO_PROPIEDAD EP (NOLOCK) ON (P.ID_ESTADO = EP.COD_EST_PROPIEDAD)
			WHERE P.ID_ESTADO IN ('1', '3', '5') AND P.ID_ESTADO_OCULTAR <> 2
			AND P.COD_ENTIDAD = ? 
			AND ((? = '') OR (P.DENOMINACION_PREDIO LIKE '%' + ? + '%'))
		) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN ? AND ?
			";
		//echo '<br>'; print_r($params);
		//echo $sqlQuery;

		return $this->Consultar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  26-05-2020 */
	/* DESCRIPCION:     OBTENER TOTAL DE REGISTROS DE PREDIOS */
	function TOTAL_BIENES_MUEBLES_x_PREDIO2($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];

		$sqlQuery ="SELECT 
			BP.ID_PREDIO_SBN,
			COUNT(BP.COD_UE_BIEN_PATRI) AS TOT_BIEN
			FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL BP (NOLOCK)
			WHERE BP.ID_ESTADO = '1'
			AND BP.COD_ENTIDAD = ?
			AND BP.ID_PREDIO_SBN = ?
			GROUP BY 
			BP.ID_PREDIO_SBN";
		//echo $sqlQuery;

		return $this->Consultar($sqlQuery, $params);
	}




	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  26-05-2020 */
	/* DESCRIPCION:     VERIFICAR EXISTENCIA DE USUARIOS */
	function Verifica_Existe_Usuario_Simi_Activo2($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['NOM_USUARIO'], "tipo"=>"string"];  
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['Crypttext'], "tipo"=>"string"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['NOM_CLAVE'], "tipo"=>"string"];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['COD_USUARIO'], "tipo"=>"int"];
		
		//echo '<pre>'; print_r($filtros);
		$sqlQuery ="SELECT count(*) AS TOT_USER 
			FROM TBL_MUEBLES_USUARIO  
			WHERE NOM_USUARIO=? 
			AND DECRYPTBYPASSPHRASE(?, CLAVE_SQL) = ?
			AND COD_USUARIO = ?
			AND ID_ESTADO = 1";
		//echo $sqlQuery;

		return $this->Consultar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  27-05-2020 */
	/* DESCRIPCION:     LISTADO DE DISTRITOS */
	function Lista_DistritoPDO($filtros=[]){

		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['cod_dpto'], "tipo"=>"string"];  
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['cod_prov'], "tipo"=>"string"];
		
		//echo '<pre>'; print_r($filtros);
		$sqlQuery ="SELECT 
						CODUBIGEO,
						CODREGION AS COD_REGION,
						CODDEP AS COD_DPTO,
						CODPROV AS COD_PROV,
						CODDIST AS COD_DIST,
						NMBUBIGEO AS DISTRITO,
						FLAG,
						EQUIVALE
						FROM TBL_UBIGEO WHERE FLAG = 'T' AND CODDEP = ? AND CODPROV = ?
						ORDER BY NMBUBIGEO";
		//echo $sqlQuery;

		return $this->Consultar($sqlQuery, $params);

	}




	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  27-05-2020 */
	/* DESCRIPCION:     LISTADO DE DISTRITOS */
	function Lista_ProvinciaPDO($filtros=[]){

		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['cod_dpto'], "tipo"=>"string"];  
		
		//echo '<pre>'; print_r($filtros);
		$sqlQuery ="SELECT 
					CODUBIGEO,
					CODREGION AS COD_REGION,
					CODDEP AS COD_DPTO,
					CODPROV AS COD_PROV,
					CODDIST AS COD_DIST,
					NMBUBIGEO AS PROVINCIA,
					FLAG,
					EQUIVALE
					FROM TBL_UBIGEO WHERE FLAG = 'P' AND CODDEP = ? AND CODDIST = '00'
					ORDER BY NMBUBIGEO";
		//echo $sqlQuery;

		return $this->Consultar($sqlQuery, $params);

	}



	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  28-05-2020 */
	/* DESCRIPCION:     LISTADO DE DISTRITOS */
	function Validar_Solo_Usuario_SimiPDO($filtros=[]){

		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_USUARIO'], "tipo"=>"string"];
		
		//echo '<pre>'; print_r($filtros);
		$sqlQuery ="SELECT TOP 1 U.COD_USUARIO, U.NOM_USUARIO, count(U.COD_USUARIO) as TOT_USER
					FROM TBL_MUEBLES_USUARIO U
					WHERE U.ID_ESTADO = 1 AND U.NOM_USUARIO = ?
					group by U.COD_USUARIO, U.NOM_USUARIO";
		//echo $sqlQuery;

		return $this->Consultar($sqlQuery, $params);

	}



	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  29-05-2020 */
	/* DESCRIPCION:     VALIDACION DE USUARIO */
	function Validar_Usuario_SimiPDO($filtros=[]){

		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['usuario'], "tipo"=>"string"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['Crypttext'], "tipo"=>"string"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['clave_ok'], "tipo"=>"string"];
		
		
		
		//echo '<pre>'; print_r($params);
		$sqlQuery ="SELECT count(*) as tot_reg
					FROM TBL_MUEBLES_USUARIO  
					WHERE ID_ESTADO = '1' AND NOM_USUARIO = ? AND DECRYPTBYPASSPHRASE(?, CLAVE_SQL) = ?";
		echo $sqlQuery;

		return $this->Consultar($sqlQuery, $params);

	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  29-05-2020 */
	/* DESCRIPCION:      */
	function Validar_Acceso_Entidad_a_ModuloPDO($filtros=[]){

		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['usuario'], "tipo"=>"string"];
		
		//echo '<pre>'; print_r($filtros);
		$sqlQuery ="EXEC PA_VALIDAR_ACCESO_ENTIDAD_A_MODULO ?";
		//echo $sqlQuery;

		return $this->Consultar($sqlQuery, $params);

	}



	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  29-05-2020 */
	/* DESCRIPCION:      */
	function Mostrar_Datos_Usuario_SimiPDO($filtros=[]){

		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['usuario'], "tipo"=>"string"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['Crypttext'], "tipo"=>"string"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['clave_ok'], "tipo"=>"string"];
		
		
		//echo '<pre>'; print_r($filtros);
		$sqlQuery ="SELECT U.*, E.RUC_ENTIDAD, E.NOM_ENTIDAD 
				FROM TBL_MUEBLES_USUARIO U
				LEFT JOIN TBL_PADRON_ENTIDAD E ON (E.COD_ENTIDAD = U.COD_ENTIDAD)
				WHERE U.ID_ESTADO = '1' AND U.NOM_USUARIO = ? AND DECRYPTBYPASSPHRASE(?, U.CLAVE_SQL) = ?";
		//echo $sqlQuery;

		return $this->Consultar($sqlQuery, $params); 

	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  06-06-2020 */
	/* DESCRIPCION:     OBTENER DATOS DE PREDIO */
	function Ver_Datos_Local_x_CODIGO_PDO($filtros=[]){

		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['xID_PREDIO_SBN'], "tipo"=>"int"];
		
		//echo '<pre>'; print_r($filtros);
		$sqlQuery ="SELECT * 
		FROM TBL_PADRON_PREDIOS (NOLOCK) 
		WHERE convert(CHAR, ID_PREDIO_SBN) = ? ";
		//echo $sqlQuery;

		return $this->Consultar($sqlQuery, $params); 

	}

	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  06-06-2020 */
	/* DESCRIPCION:     REGISTRAR PREDIO */
	function Padron_Insertar_Local_Predios_x_Muebles_PDO($filtros=[]){


		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['DENOMINACION_PREDIO'], "tipo"=>"string"];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['COD_DEPA'], "tipo"=>"int"];
		$params[4] = ["nombre"=>(5), "valor"=>$filtros['COD_PROV'], "tipo"=>"int"];
		$params[5] = ["nombre"=>(6), "valor"=>$filtros['COD_DIST'], "tipo"=>"int"];
		$params[6] = ["nombre"=>(7), "valor"=>$filtros['COD_ZONA'], "tipo"=>"int"];
		$params[7] = ["nombre"=>(8), "valor"=>$filtros['COD_TVIA'], "tipo"=>"int"];
		$params[8] = ["nombre"=>(9), "valor"=>$filtros['NOMBRE_VIA'], "tipo"=>"string"];
		$params[9] = ["nombre"=>(10), "valor"=>$filtros['NRO_PRED'], "tipo"=>"string"];
		$params[10] = ["nombre"=>(11), "valor"=>$filtros['MZA_PRED'], "tipo"=>"string"];
		$params[11] = ["nombre"=>(12), "valor"=>$filtros['LTE_PRED'], "tipo"=>"string"];
		$params[12] = ["nombre"=>(13), "valor"=>$filtros['UBICADO_PISO'], "tipo"=>"string"];
		$params[13] = ["nombre"=>(14), "valor"=>$filtros['DET_PRED'], "tipo"=>"int"];
		$params[14] = ["nombre"=>(15), "valor"=>$filtros['DET_PRED_NRO'], "tipo"=>"string"];
		$params[15] = ["nombre"=>(16), "valor"=>$filtros['COD_THABILITACION'], "tipo"=>"int"];
		$params[16] = ["nombre"=>(17), "valor"=>$filtros['NOM_THABILITACION'], "tipo"=>"string"];
		$params[17] = ["nombre"=>(18), "valor"=>$filtros['NOM_SECTOR'], "tipo"=>"string"];
		$params[18] = ["nombre"=>(19), "valor"=>$filtros['COD_TIP_PROPIEDAD'], "tipo"=>"int"];
		$params[19] = ["nombre"=>(20), "valor"=>$filtros['MODULO_REGISTRO'], "tipo"=>"string"];
		$params[20] = ["nombre"=>(21), "valor"=>$filtros['USUARIO'], "tipo"=>"int"];
		$params[21] = ["nombre"=>(22), "valor"=>$filtros['ID_ESTADO'], "tipo"=>"int"];

		//print_r($params);
		$sqlQuery = "
		INSERT INTO TBL_PADRON_PREDIOS  
		(
			ID_PREDIO_SBN, COD_ENTIDAD, DENOMINACION_PREDIO, COD_DEPA, COD_PROV, COD_DIST, COD_ZONA, COD_TVIA, NOMBRE_VIA, 
			NRO_PRED, MZA_PRED, LTE_PRED, UBICADO_PISO, DET_PRED, DET_PRED_NRO, COD_THABILITACION, NOM_THABILITACION, NOM_SECTOR, 
			COD_TIP_PROPIEDAD, FECHA_REGISTRO, MODULO_REGISTRO, MUEBLES_USUARIO_CREACION, MUEBLES_FECHA_CREACION, ID_ESTADO
		)
		VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,GETDATE(),?,?,GETDATE(),?)";
		return $this->Ejecutar($sqlQuery, $params);

	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     REGISTRAR PREDIO */
	function Padron_Actualizar_Local_Predios_x_Muebles_PDO($filtros=[]){
		//echo '<pre>'; print_r($filtros);
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['DENOMINACION_PREDIO'], "tipo"=>"string"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['COD_DEPA'], "tipo"=>"int"];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['COD_PROV'], "tipo"=>"int"];
		$params[4] = ["nombre"=>(5), "valor"=>$filtros['COD_DIST'], "tipo"=>"int"];
		$params[5] = ["nombre"=>(6), "valor"=>$filtros['COD_ZONA'], "tipo"=>"int"];
		$params[6] = ["nombre"=>(7), "valor"=>$filtros['COD_TVIA'], "tipo"=>"int"];
		$params[7] = ["nombre"=>(8), "valor"=>$filtros['NOMBRE_VIA'], "tipo"=>"string"];
		$params[8] = ["nombre"=>(9), "valor"=>$filtros['NRO_PRED'], "tipo"=>"string"];
		$params[9] = ["nombre"=>(10), "valor"=>$filtros['MZA_PRED'], "tipo"=>"string"];
		$params[10] = ["nombre"=>(11), "valor"=>$filtros['LTE_PRED'], "tipo"=>"string"];
		$params[11] = ["nombre"=>(12), "valor"=>$filtros['UBICADO_PISO'], "tipo"=>"string"];
		$params[12] = ["nombre"=>(13), "valor"=>$filtros['DET_PRED'], "tipo"=>"int"];
		$params[13] = ["nombre"=>(14), "valor"=>$filtros['DET_PRED_NRO'], "tipo"=>"string"];
		$params[14] = ["nombre"=>(15), "valor"=>$filtros['COD_THABILITACION'], "tipo"=>"int"];
		$params[15] = ["nombre"=>(16), "valor"=>$filtros['NOM_THABILITACION'], "tipo"=>"string"];
		$params[16] = ["nombre"=>(17), "valor"=>$filtros['NOM_SECTOR'], "tipo"=>"string"];
		$params[17] = ["nombre"=>(18), "valor"=>$filtros['COD_TIP_PROPIEDAD'], "tipo"=>"int"];
		$params[18] = ["nombre"=>(19), "valor"=>$filtros['MODULO_REGISTRO'], "tipo"=>"string"];
		$params[19] = ["nombre"=>(20), "valor"=>$filtros['USUARIO'], "tipo"=>"int"];
		$params[20] = ["nombre"=>(21), "valor"=>$filtros['ID_ESTADO'], "tipo"=>"int"];
		$params[21] = ["nombre"=>(22), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];

		//print_r($params);
		$sqlQuery = "
		UPDATE TBL_PADRON_PREDIOS  SET  
			COD_ENTIDAD = ?, 
			DENOMINACION_PREDIO = ?, 
			COD_DEPA = ?, 
			COD_PROV = ?, 
			COD_DIST = ?, 
			COD_ZONA = ?, 
			COD_TVIA = ?, 
			NOMBRE_VIA = ?, 
			NRO_PRED = ?, 
			MZA_PRED = ?, 
			LTE_PRED = ?, 			
			UBICADO_PISO = ?, 
			DET_PRED = ?, 
			DET_PRED_NRO = ?, 
			COD_THABILITACION = ?, 
			NOM_THABILITACION = ?, 			
			NOM_SECTOR = ?, 
			COD_TIP_PROPIEDAD = ?, 
			MODULO_REGISTRO = ?,
			MUEBLES_USUARIO_MODIFICA = ?, 
			MUEBLES_FECHA_MODIFICA = GETDATE(),
			ID_ESTADO = ?,
			FECHA_ENVIO_REG_A_SBN = GETDATE()
			WHERE ID_PREDIO_SBN = ? ";
			//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);

	}




	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     MIGRAR AREAS */
	function Migrar_predio_areas_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['cbo_predio'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];

		//print_r($params);
		$sqlQuery = "UPDATE TBL_MUEBLES_UE_AREA SET ID_PREDIO_SBN = ? WHERE ID_PREDIO_SBN = ? AND ID_ESTADO = '1'
		";
			//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);
	}

	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     MIGRAR PERSONAS */
	function Migrar_predio_personas_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['cbo_predio'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];

		//print_r($params);
		$sqlQuery = "UPDATE TBL_MUEBLES_UE_PERSONAL SET ID_PREDIO_SBN = ? WHERE COD_ENTIDAD = ? AND ID_PREDIO_SBN = ? AND ID_ESTADO = '1'
		";
			//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);
	}

	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     MIGRAR MUEBLES */
	function Migrar_predio_muebles_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['cbo_predio'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];

		//print_r($params);
		$sqlQuery = "UPDATE TBL_MUEBLES_UE_BIEN_PATRIMONIAL SET ID_PREDIO_SBN = ? WHERE COD_ENTIDAD = ? AND ID_PREDIO_SBN = ? AND ID_ESTADO = '1'
		";
			//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     MIGRAR MUEBLES */
	function Contar_muebles_predio_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];

		//print_r($params);
		$sqlQuery = "SELECT COUNT(COD_UE_BIEN_PATRI)[CANTIDAD_P] FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL WHERE COD_ENTIDAD = ? AND ID_PREDIO_SBN = ? AND ID_ESTADO = '1'
		";
		//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     MIGRAR MUEBLES */
	function Contar_areas_predio_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];

		//print_r($params);
		$sqlQuery = "SELECT COUNT(COD_UE_AREA)[CANTIDAD_A] FROM TBL_MUEBLES_UE_AREA WHERE ID_PREDIO_SBN = ? AND ID_ESTADO = '1'
		";
			//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);
	}

	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     MIGRAR MUEBLES */
	function Contar_personal_predio_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];

		//print_r($params);
		$sqlQuery = "SELECT COUNT(COD_UE_PERS)[CANTIDAD_S] FROM TBL_MUEBLES_UE_PERSONAL WHERE COD_ENTIDAD = ? AND ID_PREDIO_SBN = ? AND ID_ESTADO = '1'
		";
			//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     MIGRAR MUEBLES */
	function Devolver_nombre_predio_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];

		//print_r($params);
		$sqlQuery = "SELECT DENOMINACION_PREDIO FROM TBL_PADRON_PREDIOS P WHERE COD_ENTIDAD = ? AND ID_PREDIO_SBN = ?
		";
			//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     MIGRAR MUEBLES */
	function Listar_predios_migrar_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];

		//print_r($params);
		$sqlQuery = "select ID_PREDIO_SBN, DENOMINACION_PREDIO from TBL_PADRON_PREDIOS P WHERE COD_ENTIDAD = ? AND ID_PREDIO_SBN <> ? AND ID_ESTADO IN ('1')
		";
			//echo $sqlQuery;
		return $this->Consultar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     MIGRAR MUEBLES */
	function Eliminar_Predio_PDO($filtros=[]){
		
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[0] = ["nombre"=>(1), "valo"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		

		if($filtros['COD_TIP_PROPIEDAD'] == 1){
			$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '2' WHERE ID_PREDIO_SBN = ? AND COD_ENTIDAD = ?";
		}elseif ($filtros['COD_TIP_PROPIEDAD'] == 2){
			$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '3' WHERE ID_PREDIO_SBN = ? AND COD_ENTIDAD = ?";
		}elseif ($filtros['COD_TIP_PROPIEDAD'] == 3){
			$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '4' WHERE ID_PREDIO_SBN = ? AND COD_ENTIDAD = ?";
		}elseif ($filtros['COD_TIP_PROPIEDAD'] == 4){
			$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '4' WHERE ID_PREDIO_SBN = ? AND COD_ENTIDAD = ?";
		}else{
			$sql="UPDATE TBL_PADRON_PREDIOS SET ID_ESTADO = '2' WHERE ID_PREDIO_SBN = ? AND COD_ENTIDAD = ?";
		} 
		
		return $this->Ejecutar($sql, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     TOTAL PREDIOS */
	function TOTAL_PADRON_PREDIOS_X_PARAMETROS_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['DENOMINACION_PREDIO'], "tipo"=>"string"];

		//print_r($params);
		$sqlQuery = "SELECT COUNT(*) AS TOT_REG FROM TBL_PADRON_PREDIOS P WHERE P.ID_ESTADO  IN ('1', '5') 
			AND (P.DAR_BAJA IS NULL OR P.DAR_BAJA = '') AND COD_ENTIDAD = ?  AND P.ID_ESTADO_OCULTAR <> '2' 
			AND P.DENOMINACION_PREDIO LIKE '%' + ? + '%'";
			//echo $sqlQuery;
		return $this->Consultar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     LISTADO PREDIOS */
	function LISTA_PADRON_PREDIOS_X_PARAMETROS_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['DENOMINACION_PREDIO'], "tipo"=>"string"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['INI'], "tipo"=>"int"];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['FIN'], "tipo"=>"int"];


		//print_r($params);
		$sqlQuery = "SELECT * FROM 
		(
			SELECT P.*,  
			TP.DESC_TIP_PROPIEDAD,
			ROW_NUMBER() OVER (ORDER BY P.ID_PREDIO_SBN) AS ROW_NUMBER_ID
			FROM TBL_PADRON_PREDIOS P
			LEFT JOIN TBL_TIPO_PROPIEDAD TP ON (P.COD_TIP_PROPIEDAD = TP.COD_TIP_PROPIEDAD)						
			WHERE P.ID_ESTADO IN ('1','5') AND P.ID_ESTADO_OCULTAR = '1' 
			AND (P.DAR_BAJA IS NULL OR P.DAR_BAJA = '') AND P.COD_ENTIDAD = ? 
			AND P.DENOMINACION_PREDIO LIKE '%' + ? + '%'
		) AS TABLEWITHROW_NUMBER WHERE ROW_NUMBER_ID BETWEEN ? AND ? ";
			//echo $sqlQuery;
		return $this->Consultar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  08-06-2020 */
	/* DESCRIPCION:     REGISTRO DE AREAS */
	function Insertar_Simi_UE_Area_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['DESC_AREA'], "tipo"=>"string"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['SIGLAS_AREA'], "tipo"=>"string"];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['COD_USUARIO'], "tipo"=>"int"];

		//print_r($params);
		$sqlQuery = "INSERT INTO TBL_MUEBLES_UE_AREA (ID_PREDIO_SBN, DESC_AREA, SIGLAS_AREA, FECHA_REGISTRO, USUARIO_CREACION, FECHA_CREACION, ID_ESTADO)
		VALUES (?, ?, ?, GETDATE(), ?, GETDATE(), '1')	";
			//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);
	}

	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  09-06-2020 */
	/* DESCRIPCION:     ACTUALIZACION DE AREAS */
	function Actualizar_Simi_UE_Area_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['ID_PREDIO_SBN'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['DESC_AREA'], "tipo"=>"string"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['SIGLAS_AREA'], "tipo"=>"string"];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['COD_USUARIO'], "tipo"=>"int"];
		$params[4] = ["nombre"=>(5), "valor"=>$filtros['COD_UE_AREA'], "tipo"=>"int"];
		
		//print_r($params);
		$sqlQuery = "UPDATE TBL_MUEBLES_UE_AREA 
		SET ID_PREDIO_SBN = ?, DESC_AREA = ?, SIGLAS_AREA = ?, USUARIO_MODIFICA = ?, FECHA_MODIFICA = GETDATE() 
		WHERE COD_UE_AREA = ?";
			//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  09-06-2020 */
	/* DESCRIPCION:     VALIDACION DE ELIMIANCION DE AREAS */
	function Eliminar_Area_Validacion_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_UE_AREA'], "tipo"=>"int"];
		
		//print_r($params);
		$sqlQuery = "SELECT COUNT(COD_UE_BIEN_PATRI)[CANTIDAD] FROM TBL_MUEBLES_UE_BIEN_PATRIMONIAL 
		WHERE COD_ENTIDAD = ? AND COD_UE_AREA = ? AND ID_ESTADO = '1' and COD_TIPO_MOV in (2)";
			//echo $sqlQuery;
		return $this->Consultar($sqlQuery, $params);
	}

	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  09-06-2020 */
	/* DESCRIPCION:     ELIMINACION DE AREA */
	function Eliminar_Area_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['USUARIO_ELIMINA'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_UE_AREA'], "tipo"=>"int"];
		
		
		//print_r($params);
		$sqlQuery = "UPDATE TBL_MUEBLES_UE_AREA SET ID_ESTADO = 2, USUARIO_ELIMINA = ?, FECHA_ELIMINA = GETDATE() WHERE COD_UE_AREA = ? ";
		//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);
	}

	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  09-06-2020 */
	/* DESCRIPCION:     REGISTRO DE RESPONSABLE */
	function Insertar_Simi_UE_Responsable_Patrimonial_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['ID_PREDIO_SBN_RESP'], "tipo"=>"int"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['COD_UE_AREA_RESP'], "tipo"=>"int"];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['COD_UE_OFIC_RESP'], "tipo"=>"int"];
		$params[4] = ["nombre"=>(5), "valor"=>$filtros['COD_UE_PERS_RESP'], "tipo"=>"int"];
		$params[5] = ["nombre"=>(6), "valor"=>$filtros['COD_USUARIO'], "tipo"=>"int"];
		
		//print_r($params);
		$sqlQuery = "INSERT INTO TBL_MUEBLES_UE_RESPONSABLE_PATRIMONIAL 
		(COD_ENTIDAD, ID_PREDIO_SBN_RESP, COD_UE_AREA_RESP, COD_UE_OFIC_RESP, COD_UE_PERS_RESP, FECHA_REGISTRO, 
		SIMI_USUARIO_CREACION, SIMI_FECHA_CREACION, ID_ESTADO)
		VALUES (?, ?, ?, ?, ?, GETDATE(), ?, GETDATE(), '1')";
		//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  09-06-2020 */
	/* DESCRIPCION:     ACTUALIZACION DE RESPONSABLE */
	function Actualiza_Simi_UE_Responsable_Patrimonial_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_ENTIDAD'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['ID_PREDIO_SBN_RESP'], "tipo"=>"int"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['COD_UE_AREA_RESP'], "tipo"=>"int"];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['COD_UE_OFIC_RESP'], "tipo"=>"int"];
		$params[4] = ["nombre"=>(5), "valor"=>$filtros['COD_UE_PERS_RESP'], "tipo"=>"int"];
		$params[5] = ["nombre"=>(6), "valor"=>$filtros['COD_USUARIO'], "tipo"=>"int"];
		$params[6] = ["nombre"=>(7), "valor"=>$filtros['COD_UE_RESP_PATRI'], "tipo"=>"int"];
		
		//print_r($params);
		$sqlQuery = "UPDATE TBL_MUEBLES_UE_RESPONSABLE_PATRIMONIAL SET 
				COD_ENTIDAD = ?,
				ID_PREDIO_SBN_RESP = ?,
				COD_UE_AREA_RESP = ?,
				COD_UE_OFIC_RESP = ?,
				COD_UE_PERS_RESP = ?,
				SIMI_USUARIO_MODIFICA = ?,
				SIMI_FECHA_MODIFICA = GETDATE()
			WHERE COD_UE_RESP_PATRI = ?";
		//echo $sqlQuery;
		return $this->Ejecutar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  09-06-2020 */
	/* DESCRIPCION:     TOTAL DE BIENES VEHICULARES */
	function F_Total_Bienes_Vehiculares_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['ACCION'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['txt_entidad'], "tipo"=>"int"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['txt_codigo'], "tipo"=>"string"];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['txt_denominacion'], "tipo"=>"string"];
		
		//print_r($params);
		$sqlQuery = "EXEC SP_SIMI_LISTADO_BIENES_VEHICULARES
		@ACCION             = ?,
		@INI                = 0,
		@FIN                = 0,
		@COD_ENTIDAD        = ?,
		@CODIGO_PATRIMONIAL = ?,
		@DENOMINACION_BIEN  = ?";
			//echo $sqlQuery;
		return $this->Consultar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  09-06-2020 */
	/* DESCRIPCION:     LISTADO DE BIENES VEHICULARES */
	function F_Listado_Bienes_Vehiculares_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['ACCION'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['txt_entidad'], "tipo"=>"int"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['txt_codigo'], "tipo"=>"string"];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['txt_denominacion'], "tipo"=>"string"];
		$params[4] = ["nombre"=>(5), "valor"=>$filtros['Item_Ini'], "tipo"=>"int"];
		$params[5] = ["nombre"=>(6), "valor"=>$filtros['Item_Fin'], "tipo"=>"int"];
		
		//print_r($params);
		$sqlQuery = "EXEC SP_SIMI_LISTADO_BIENES_VEHICULARES
		@ACCION             = ?,
		@COD_ENTIDAD        = ?,
		@CODIGO_PATRIMONIAL = ?,
		@DENOMINACION_BIEN  = ?,
		@INI                = ?,
		@FIN                = ?";
			//echo $sqlQuery;
		return $this->Consultar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  09-06-2020 */
	/* DESCRIPCION:     TOTAL DE BUSQUEDA DE CATALOGO */
	function Total_Catalogo_x_Denominacion_Bien_BUSQUEDA_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_GRUPO'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_CLASE'], "tipo"=>"int"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['NRO_BIEN'], "tipo"=>"string"];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['DENOM_BIEN'], "tipo"=>"string"];
		$params[4] = ["nombre"=>(5), "valor"=>$filtros['CONDICION'], "tipo"=>"string"];
		$params[5] = ["nombre"=>(6), "valor"=>$filtros['COD_RESOL_CATALOGO'], "tipo"=>"string"];
		
		//echo '<br>'; print_r($params);
		$sqlQuery = "EXEC PA_TOTAL_BUSQUEDA_CATALOGO_BIENES 
			@COD_GRUPO             	= ?,
			@COD_CLASE              = ?,
			@NRO_BIEN        		= ?,
			@DENOM_BIEN 			= ?,
			@CONDICION  			= ?,
			@COD_RESOL_CATALOGO     = ?";
			
			//echo $sqlQuery;
		return $this->Consultar($sqlQuery, $params);
	}


	/* CREACION: 		WILLIAMS ARENAS */
	/* FECHA CREACION:  09-06-2020 */
	/* DESCRIPCION:     LISTADO DE CATALOGO */
	function LISTA_Catalogo_x_Denominacion_Bien_BUSQUEDA_PDO($filtros=[]){
		$params = [];
		$params[0] = ["nombre"=>(1), "valor"=>$filtros['COD_GRUPO'], "tipo"=>"int"];
		$params[1] = ["nombre"=>(2), "valor"=>$filtros['COD_CLASE'], "tipo"=>"int"];
		$params[2] = ["nombre"=>(3), "valor"=>$filtros['NRO_BIEN'], "tipo"=>"string"];
		$params[3] = ["nombre"=>(4), "valor"=>$filtros['DENOM_BIEN'], "tipo"=>"string"];
		$params[4] = ["nombre"=>(5), "valor"=>$filtros['CONDICION'], "tipo"=>"string"];
		$params[5] = ["nombre"=>(6), "valor"=>$filtros['COD_RESOL_CATALOGO'], "tipo"=>"string"];
		$params[6] = ["nombre"=>(7), "valor"=>$filtros['INI'], "tipo"=>"int"];
		$params[7] = ["nombre"=>(8), "valor"=>$filtros['FIN'], "tipo"=>"int"];

		//print_r($params);
		$sqlQuery = "EXEC PA_LISTADO_BUSQUEDA_CATALOGO_BIENES 
			@COD_GRUPO             	= ?,
			@COD_CLASE              = ?,
			@NRO_BIEN        		= ?,
			@DENOM_BIEN 			= ?,
			@CONDICION  			= ?,
			@COD_RESOL_CATALOGO     = ?,
			@INI					= ?,
			@FIN					= ?";
			//echo $sqlQuery;
		return $this->Consultar($sqlQuery, $params);
	}
	
	


}
?>
