<?
ini_set('max_execution_time', 99999); //600 seconds = 10 minutes
$operacion	=	$_GET['operacion'];
require_once('../Controller/C_simi_m_ue_inventario.php');
require_once('../../utils/funciones/funciones.php');

$oSimi_UE_Bien_Inventario				=	new Simi_UE_Bien_Inventario;


if($operacion=='Finalizar_Todo_Inventario_General_x_Entidad'){
	
	$xCOD_ENTIDAD 			=	$_REQUEST['xCOD_ENTIDAD'];
	$sCOD_INVENTARIO 		=	$_REQUEST['sCOD_INVENTARIO'];
	$txh_migra_cod_inv_ent 	=	$_REQUEST['txh_migra_cod_inv_ent'];
	$sAnio 					=	$_REQUEST['sAnio'];
	$COD_USUARIO			=	$_REQUEST['TXH_SIMI_COD_USUARIO'];
	

	if($txh_migra_cod_inv_ent == ''){
		
		$RESULT_INV = $oSimi_UE_Bien_Inventario->Finalizar_Inventario_Creacion_Cabecera_Inventario($xCOD_ENTIDAD, $sCOD_INVENTARIO, $sAnio, $COD_USUARIO);	
		$TOTAL		= odbc_result($RESULT_INV,"TOTAL");
		
	}
		
	echo $TOTAL;
	
}elseif($operacion=='Adjuntar_Informe_Inv_Transf'){
	$COD_ENTIDAD		=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];
	$COD_INVENTARIO_ENT	=	$_REQUEST['txh_COD_INVENTARIO'];

	$UPLOADDIR='../../../repositorio_muebles/inventario/doc_informe_muebles_inv_transf';

	print_r($ESTADO);
	$ARCHIVO			= $_FILES['filex']['tmp_name'];
	$ARCHIVO_NOMBRE		= basename($_FILES['filex']['name']);
	$ARCHIVO_PESO		= $_FILES['filex']['size'];
	$ARCHIVO_TIPO		= $_FILES['filex']['type'];
	$EXTENSION			= extension_archivo($_FILES['filex']['name']);
	
	$NUEVA_CADENA		= substr( md5(microtime()), 1, 4);
	$FECHA				= date('Y').''.date('m').''.date('d');
	
	$ARCHIVO_NOMBRE_GENERADO	= 'Inf_Mueble_Transf_Inv'.'_'.$COD_ENTIDAD.'_'.$COD_INVENTARIO_ENT.'_'.$FECHA.'_'.$NUEVA_CADENA.'.'.$EXTENSION;
	$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;
	
	if (move_uploaded_file($ARCHIVO, $UPLOADFILE)) {

		$RESULT2 = $oSimi_UE_Bien_Inventario->Adjunta_Archivo_Informe_Sust_Inv_mueble($COD_ENTIDAD, $COD_INVENTARIO_ENT, $ARCHIVO_NOMBRE_GENERADO, $ARCHIVO_PESO);
	
		$Ver_carga = $oSimi_UE_Bien_Inventario->UE_Total_Subido_Muebles($COD_ENTIDAD,$COD_INVENTARIO_ENT);
  		$CANT    = utf8_encode(odbc_result($Ver_carga,"CANT_SUST"));
		
		$valor = 1;
		
	}else{
		$CANT = 0;
		$valor = 0;	
	}
	
	$arr = array('valor'=>$valor,'cantidad'=>$CANT,'descarga_inv'=>$ARCHIVO_NOMBRE_GENERADO );
	echo json_encode($arr);

}elseif($operacion=='Adjuntar_Informe_Inv_Transf_Firmado'){
	$COD_ENTIDAD		=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];
	$COD_INVENTARIO_ENT	=	$_REQUEST['txh_COD_INVENTARIO'];

	$UPLOADDIR='../../../repositorio_muebles/inventario/doc_informe_muebles_inv_transf';
	//print_r($_FILES['filex']);
	$ARCHIVO			= $_FILES['filex']['tmp_name'];
	$ARCHIVO_NOMBRE		= basename($_FILES['filex']['name']);
	$ARCHIVO_PESO		= $_FILES['filex']['size'];
	$ARCHIVO_TIPO		= $_FILES['filex']['type'];
	$EXTENSION			= extension_archivo($_FILES['filex']['name']);
	
	$NUEVA_CADENA		= substr( md5(microtime()), 1, 4);
	$FECHA				= date('Y').''.date('m').''.date('d');
	
	$ARCHIVO_NOMBRE_GENERADO	= 'Inf_Mueble_Transf_Inv_Firma'.'_'.$COD_ENTIDAD.'_'.$COD_INVENTARIO_ENT.'_'.$FECHA.'_'.$NUEVA_CADENA.'.'.$EXTENSION;
	$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;
	
	if (move_uploaded_file($ARCHIVO, $UPLOADFILE)) {		
		$RESULT2 = $oSimi_UE_Bien_Inventario->Adjunta_Archivo_Informe_Sust_Inv_firma_mueble($COD_ENTIDAD, $COD_INVENTARIO_ENT, $ARCHIVO_NOMBRE_GENERADO, $ARCHIVO_PESO);		
		
		$Ver_carga = $oSimi_UE_Bien_Inventario->UE_Total_Subido_Firmado_Muebles($COD_ENTIDAD,$COD_INVENTARIO_ENT);
  		$CANT    = utf8_encode(odbc_result($Ver_carga,"CANT_FIRMA"));
		
		$valor = 1;
		
	}else{
		$CANT = 0;
		$valor = 0;	
	}
	
	$arr = array('valor'=>$valor,'cantidad'=>$CANT,'descarga_firma'=>$ARCHIVO_NOMBRE_GENERADO );
	echo json_encode($arr);

}elseif($operacion=='Adjuntar_Informe_Inmueble_Inv_transf'){

	$COD_ENTIDAD	=	$_REQUEST['TXH_SIMI_COD_ENTIDAD'];

	$UPLOADDIR='../../../repositorio_muebles/inventario/doc_informe_inmuebles_inv_transf';

	
	$ARCHIVO			= $_FILES['filex']['tmp_name'];
	$ARCHIVO_NOMBRE		= basename($_FILES['filex']['name']);
	$ARCHIVO_PESO		= $_FILES['filex']['size'];
	$ARCHIVO_TIPO		= $_FILES['filex']['type'];
	$EXTENSION			= extension_archivo($_FILES['filex']['name']);
	$NUEVA_CADENA		= substr( md5(microtime()), 1, 4);
	$FECHA				= date('Y').''.date('m').''.date('d');
	
	$ARCHIVO_NOMBRE_GENERADO	= 'Inf_Inmueble_Transf_Inv_Firma'.'_'.$COD_ENTIDAD.'_'.$FECHA.'_'.$NUEVA_CADENA.'.'.$EXTENSION;
	$UPLOADFILE 				= $UPLOADDIR.'/'.$ARCHIVO_NOMBRE_GENERADO;
	//echo $ARCHIVO_NOMBRE_GENERADO;
	if (move_uploaded_file($ARCHIVO, $UPLOADFILE)) {
		
		$RESULT2 = $oSimi_UE_Bien_Inventario->Adjunta_Archivo_Informe_Sust_Inv_firma_inmueble($COD_ENTIDAD, $ARCHIVO_NOMBRE_GENERADO, $ARCHIVO_PESO);		
		//$valor = ($RESULT2) ? '1'.'[*]'.$ARCHIVO_NOMBRE_GENERADO : 0 ;

		$Ver_carga = $oSimi_UE_Bien_Inventario->UE_Total_Subido($COD_ENTIDAD);
  		$CANT    = utf8_encode(odbc_result($Ver_carga,"CANT"));

		$valor = 1;
		
	}else{
		$CANT = 0;
		$valor = 0;	
	}
	
	$arr = array('valor'=>$valor,'cantidad'=>$CANT,'descarga_inmueble_firma'=>$ARCHIVO_NOMBRE_GENERADO);
	echo json_encode($arr);
	// echo $valor;

}else if($operacion=='Finalizar_Inventario_Todo_Inf_Final_Acta_Conciliacion'){
	
	$xCOD_ENTIDAD 			=	$_REQUEST['xCOD_ENTIDAD'];
	$wCOD_INVENTARIO_ENT 	=	$_REQUEST['wCOD_INVENTARIO_ENT'];

	if($xCOD_ENTIDAD != '' && $wCOD_INVENTARIO_ENT != '' ){
		$RESULT_INV = $oSimi_UE_Bien_Inventario->Finaliza_Inv_Informe_Final_Acta_Conciliacion($xCOD_ENTIDAD, $wCOD_INVENTARIO_ENT);	
	}
	
	$valor = ($RESULT_INV) ? 1 : 0 ;
		
	echo $valor;

}else if($operacion=='Finalizacion_Proceso_Inventario'){
	
	$xCOD_ENTIDAD 			=	$_REQUEST['xCOD_ENTIDAD'];
	$wCOD_INVENTARIO_ENT 	=	$_REQUEST['wCOD_INVENTARIO_ENT'];
	$xanio 					=	$_REQUEST['xanio'];
	$TXH_SIMI_COD_USUARIO	=	$_REQUEST['TXH_SIMI_COD_USUARIO'];

	if($xCOD_ENTIDAD != '' && $wCOD_INVENTARIO_ENT != '' ){
		$RESULT_INV = $oSimi_UE_Bien_Inventario->Finalizacion_Proceso_Inventario($xCOD_ENTIDAD, $wCOD_INVENTARIO_ENT, $xanio, $TXH_SIMI_COD_USUARIO);
	}
	
	$valor = ($RESULT_INV) ? 1 : 0 ;
		
	echo $valor;
	
}else if($operacion=='Limpiar_Bienes'){
	
	$xCOD_ENTIDAD 		=	$_REQUEST['xCOD_ENTIDAD'];
	$xID_PREDIO_SBN 	=	$_REQUEST['ID_PREDIO_SBN'];
	$xCOD_INVENTARIO 	=	$_REQUEST['sCOD_INVENTARIO'];
	$uPERIODO			=	$_REQUEST['txh_anio'];
	
	$xRS_Inventario 			= $oSimi_UE_Bien_Inventario->INV_Mostrar_Datos_Inventario_x_Anio_Local($xCOD_ENTIDAD, $xID_PREDIO_SBN, $xCOD_INVENTARIO, $uPERIODO);
	$sCOD_IMPORT_EXCEL_FILE		= utf8_encode(odbc_result($xRS_Inventario,"COD_IMPORT_EXCEL_FILE"));

	$RESULT_INV = $oSimi_UE_Bien_Inventario->Limpiar_Bien_x_Predio_Inventario($xCOD_ENTIDAD, $sCOD_IMPORT_EXCEL_FILE);	
	
	$valor = ($RESULT_INV) ? '1'.'[*]'.$sCOD_IMPORT_EXCEL_FILE : 0 ;	
		
	echo $valor;
	
}else if($operacion=='Regresar_a_Estado_Pendiente'){

	$xCOD_ENTIDAD 			=	$_REQUEST['xCOD_ENTIDAD'];
	$sCOD_INVENTARIO 		=	$_REQUEST['sCOD_INVENTARIO'];
	$wCOD_INVENTARIO_ENT 	=	$_REQUEST['wCOD_INVENTARIO_ENT'];
	
	$xRS_Inventario = $oSimi_UE_Bien_Inventario->Regresar_Estado_Pendiente_Inventario($xCOD_ENTIDAD, $sCOD_INVENTARIO, $wCOD_INVENTARIO_ENT);
	$valor 			= ($xRS_Inventario) ? '1' : 0 ;	
		
	echo $valor;

}
?>