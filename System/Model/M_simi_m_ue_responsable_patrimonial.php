<?
$operacion	=	$_GET['operacion'];
require_once('../Controller/C_simi_m_ue_responsable_patrimonial.php');
require_once('../../utils/funciones/funciones.php');
require_once("../Controller/BaseBD_PDO.php");
require_once('../Model/BaseBD_PDOModel.php');
require_once('../Controller/BienPatrimonialController.php');
require_once('../Model/BienPatrimonialModel.php');

$oSimi_UE_Responsable_Patrimonial	=	new Simi_UE_Responsable_Patrimonial;
$objModel = new BienPatrimonialModel();

if($operacion=='Guardar_UE_Responsable_Patrimonial'){

	$COD_USUARIO 				= convertir_a_utf8(filtrar_caracteres($_REQUEST['TXH_SIMI_COD_USUARIO']));
	$txt_COD_UE_RESP_PATRI		= convertir_a_utf8(filtrar_caracteres($_REQUEST['txt_COD_UE_RESP_PATRI']));
	
	$COD_ENTIDAD				= convertir_a_utf8(filtrar_caracteres($_REQUEST['TXH_SIMI_COD_ENTIDAD']));
	$ID_PREDIO_SBN_RESP			= convertir_a_utf8(filtrar_caracteres($_REQUEST['cbo_Local_Predio']));
	$COD_UE_AREA_RESP			= convertir_a_utf8(filtrar_caracteres($_REQUEST['cbo_Area']));
	$COD_UE_OFIC_RESP			= '';
	$COD_UE_PERS_RESP			= convertir_a_utf8(filtrar_caracteres($_REQUEST['cbo_Personal']));

	if($txt_COD_UE_RESP_PATRI == ''){
		/*
		$RESULTADO			=	$oSimi_UE_Responsable_Patrimonial->Insertar_Simi_UE_Responsable_Patrimonial(
			$COD_ENTIDAD, $ID_PREDIO_SBN_RESP, $COD_UE_AREA_RESP, $COD_UE_OFIC_RESP, $COD_UE_PERS_RESP, $COD_USUARIO);
		*/	
		
		/* CREACION:        WILLIAMS ARENAS */
		/* FECHA CREACION:  09-06-2020 */
		/* DESCRIPCION:     REGISTRO DE RESPONSABLE PATRIMONIAL */  
		$dataModel_01 = [
			'COD_ENTIDAD'			=>$COD_ENTIDAD,
			'ID_PREDIO_SBN_RESP'	=>$ID_PREDIO_SBN_RESP,
			'COD_UE_AREA_RESP'		=>$COD_UE_AREA_RESP,
			'COD_UE_OFIC_RESP'		=>$COD_UE_OFIC_RESP,
			'COD_UE_PERS_RESP'		=>$COD_UE_PERS_RESP,
			'COD_USUARIO'			=>$COD_USUARIO	
		];
		
		$RESULTADO = $objModel->Insertar_Simi_UE_Responsable_Patrimonial_PDO($dataModel_01);
		$valor = ($RESULTADO>=0) ? '1':'0';
		/**** FIN DE ELIMINACION DE AREA ****/

	}else{
		
		$COD_UE_RESP_PATRI	=	$txt_COD_UE_RESP_PATRI;	
		/*	
		$RESULTADO			=	$oSimi_UE_Responsable_Patrimonial->Actualiza_Simi_UE_Responsable_Patrimonial(
			$COD_UE_RESP_PATRI, $COD_ENTIDAD, $ID_PREDIO_SBN_RESP, $COD_UE_AREA_RESP, $COD_UE_OFIC_RESP, $COD_UE_PERS_RESP, 
			$COD_USUARIO);
		*/

		/* CREACION:        WILLIAMS ARENAS */
		/* FECHA CREACION:  09-06-2020 */
		/* DESCRIPCION:     REGISTRO DE RESPONSABLE PATRIMONIAL */  
		$dataModel_01 = [
			'COD_UE_RESP_PATRI'		=>$COD_UE_RESP_PATRI,
			'COD_ENTIDAD'			=>$COD_ENTIDAD,
			'ID_PREDIO_SBN_RESP'	=>$ID_PREDIO_SBN_RESP,
			'COD_UE_AREA_RESP'		=>$COD_UE_AREA_RESP,
			'COD_UE_OFIC_RESP'		=>$COD_UE_OFIC_RESP,
			'COD_UE_PERS_RESP'		=>$COD_UE_PERS_RESP,
			'COD_USUARIO'			=>$COD_USUARIO	
		];
		
		$RESULTADO = $objModel->Actualiza_Simi_UE_Responsable_Patrimonial_PDO($dataModel_01);
		$valor = ($RESULTADO>=0) ? '1':'0';
		/**** FIN DE ELIMINACION DE AREA ****/
	}	
	
	echo $valor = ($RESULTADO) ? 1 : 0;
	
}
?>