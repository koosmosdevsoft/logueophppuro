<?
require_once('../utils/funciones/funciones.php'); 
require_once('Controller/C_simi_web_user.php');
require_once('Controller/C_simi_web_user_permiso.php');
require_once('Controller/C_simi_web_modulos.php');
require_once('Controller/C_simi_m_ue_inventario.php');

$oSimi_Web_User				=	new Simi_Web_User;
$oMuebles_Menu_Permiso		=	new Muebles_Menu_Permiso;
$oSimi_Modulos				=	new Simi_Modulos;
$oSimi_UE_Bien_Inventario	=	new Simi_UE_Bien_Inventario;

$S_SIMI_COD_USUARIO = $_SESSION['th_SIMI_COD_USUARIO'];
$S_SIMI_COD_ENTIDAD = $_SESSION['th_SIMI_COD_ENTIDAD'];
$z_COD_MODULO 		= $_GET['idm'];


//$RS_Inventario 	= $oSimi_UE_Bien_Inventario->UE_Listar_Total_Inventario_Habilitado_x_Entidad($S_SIMI_COD_ENTIDAD);
$RS_Inventario 	= $oSimi_UE_Bien_Inventario->UE_Listar_Total_Inventario_Finalizados($S_SIMI_COD_ENTIDAD);

$TOTAL_REG_INV		= odbc_result($RS_Inventario,"TOT_INV_FINALIZADO");//ULTIMO INVENTARIO HABILITADO
//$X_PERIODO			= odbc_result($RS_Inventario,"PERIODO");//ULTIMO PERIODO INVENTARIO HABILITADO
//$X_COD_INVENTARIO	= odbc_result($RS_Inventario,"COD_INVENTARIO");//ULTIMO CODIGO INVENTARIO HABILITADO

//$Result_Inv		= $oMuebles_Menu_Permiso->Total_Entidades_Reg_Altas_Sin_Registrar_Inv2016($S_SIMI_COD_ENTIDAD, $X_PERIODO, $X_COD_INVENTARIO);
//$TOT_ENTIDADES_REG_ALTAS	= odbc_result($Result_Inv,"TOT_REGISTRO"); //TOTAL SI REGISTRO ALTAS SIN REGISTRAR INVENTARIO

/*
echo "<BR>TOTAL_REG_INV->".$TOTAL_REG_INV;
echo "<BR>PERIODO->".$X_PERIODO;
echo "<BR>INVENTARIO->".$X_COD_INVENTARIO;
echo "<BR>TOT_ENTIDADES_REG_ALTAS->".$TOT_ENTIDADES_REG_ALTAS;
*/

?>

<link rel="stylesheet" type="text/css" href="../utils/menuBS_leftnavi/css/bs_leftnavi_V2.css">
<script src="../utils/menuBS_leftnavi/js/bs_leftnavi.js"></script>

<style>
.div_container{padding: 0px 0px 0px 0px; }
</style>

<div class="div_container">

<div class="gw-sidebar">
  <div id="gw-sidebar" class="gw-sidebar">
    <div class="nano-content">
      <ul class="gw-nav gw-nav-list">
      
        <li class="init-un-active" > <a href="sinabip_modulos.php?idm=<?=$z_COD_MODULO?>"> <span class="gw-menu-text">
        <img src="../webimages/iconos/home_01.png" width="25" height="25" border="0" align="absmiddle" /> &nbsp;&nbsp; <B>PÁGINA PRINCIPAL</B></span> </a> 
        </li>
        
<?
	if($z_COD_MODULO == '1' || $z_COD_MODULO == '2'){//muebles O INMUEBLES
	
		if($z_COD_MODULO == '1'){//Muebles
			
			
			if($TOTAL_REG_INV == 0){ //SI NO REGISTRO SU ULTIMO INVENTARIO
			
				$resulMenuOK = $oMuebles_Menu_Permiso->Listar_Menu_Permiso_Basico_Si_No_Registro_Inv($z_COD_MODULO);

				/*
				if($TOT_ENTIDADES_REG_ALTAS > 0){ //SI REGISTRO ALTAS  Y NO HIZO INVENTARIO						
					$resulMenuOK = $oMuebles_Menu_Permiso->Listar_Menu_x_COD_MUEBLE_MODULO($z_COD_MODULO);
				}else{
					$resulMenuOK = $oMuebles_Menu_Permiso->Listar_Menu_Permiso_Basico_Si_No_Registro_Inv($z_COD_MODULO);							
				}
				*/
				
			}else{ //SI REGISTRO SU ULTIMO INVENTARIO						
				$resulMenuOK = $oMuebles_Menu_Permiso->Listar_Menu_x_COD_MUEBLE_MODULO($z_COD_MODULO);
			}					
			
		}else if($z_COD_MODULO == '2'){//Inmuebles
			$resulMenuOK = $oMuebles_Menu_Permiso->Listar_Menu_x_COD_MUEBLE_MODULO($z_COD_MODULO);
		}
		
		
	}else{
		//MENU PARA EL MODULO PERSONALIZADO
		$resulMenuOK = $oMuebles_Menu_Permiso->Listar_Permisos_Menu_x_USUARIO($z_COD_MODULO, $S_SIMI_COD_USUARIO);
	}
	
	if($resulMenuOK){
		while (odbc_fetch_row($resulMenuOK)){
			
			$z_COD_MUEBLE_MENU	 	= odbc_result($resulMenuOK,"COD_MUEBLE_MENU");
			$DESC_MUEBLE_MENU	 	= utf8_encode(odbc_result($resulMenuOK,"DESC_MUEBLE_MENU"));
			
?>
            

        
        <li class="init-arrow-down">
       
        <a href="javascript:void(0)">
        <span class="gw-menu-text"><span class="glyphicon glyphicon-file text-info"></span>
        <img src="../webimages/iconos/vineta_cuadro_B.png" width="14" height="14" align="absmiddle" />&nbsp;&nbsp;<?=$DESC_MUEBLE_MENU?></span> 
        <b class="gw-arrow icon-arrow-up8"></b>
        </a>
        
          <ul class="gw-submenu">
          
          
<?
					
					if($z_COD_MODULO == '1' || $z_COD_MODULO == '2'){//muebles O INMUEBLES
						
						
						if($z_COD_MODULO == '1'){//Muebles
				 	
					
							if($TOTAL_REG_INV == 0){ //SI NO REGISTRO SU ULTIMO INVENTARIO
								
								$resulMenusubOK = $oMuebles_Menu_Permiso->Listar_SubMenu_Permiso_Basico_Si_No_Registro_Inv($z_COD_MUEBLE_MENU);

								/*
								if($TOT_ENTIDADES_REG_ALTAS > 0){ //SI REGISTRO ALTAS  Y NO HIZO INVENTARIO						
									$resulMenusubOK = $oMuebles_Menu_Permiso->Listar_SubMenu_x_COD_MUEBLE_MENU($z_COD_MUEBLE_MENU);
								}else{
									$resulMenusubOK = $oMuebles_Menu_Permiso->Listar_SubMenu_Permiso_Basico_Si_No_Registro_Inv($z_COD_MUEBLE_MENU);							
								}
								*/
								
							}else{ //SI REGISTRO SU ULTIMO INVENTARIO						
								$resulMenusubOK = $oMuebles_Menu_Permiso->Listar_SubMenu_x_COD_MUEBLE_MENU($z_COD_MUEBLE_MENU);
							}					
							
						}else if($z_COD_MODULO == '2'){//Inmuebles
							$resulMenusubOK = $oMuebles_Menu_Permiso->Listar_SubMenu_x_COD_MUEBLE_MENU($z_COD_MUEBLE_MENU);
						}
						
						
					}else{
						$resulMenusubOK = $oMuebles_Menu_Permiso->Listar_Permisos_SubMenu_x_USUARIO($z_COD_MUEBLE_MENU, $S_SIMI_COD_USUARIO);
					}
                    
                    if($resulMenusubOK){
                        while (odbc_fetch_row($resulMenusubOK)){
                            
                            $z_COD_MUEBLE_MENUSUB	 	= odbc_result($resulMenusubOK,"COD_MUEBLE_MENUSUB");
                            $DESC_MUEBLE_MENUSUB	 	= utf8_encode(odbc_result($resulMenusubOK,"DESC_MUEBLE_MENUSUB"));
                            $LINK_MUEBLE_MENUSUB	 	= odbc_result($resulMenusubOK,"LINK_MUEBLE_MENUSUB");
?>
          
            <li>
            
            <a href="#" onclick='javascript: Actualiza_Menu_Direccionar_URL("<?=$LINK_MUEBLE_MENUSUB?>","Cuerpo", "<?=$z_COD_MODULO?>")' title="<?=$DESC_MUEBLE_MENUSUB?>"><?=$DESC_MUEBLE_MENUSUB?></a></li>
            
<?
                        }
                    }
?>
            
          </ul>
        </li>
        

        

<?
                }
            }
?>           
                
        
      </ul>
    </div>
  </div>
</div>


</div>


                