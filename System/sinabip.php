<? session_start();
require_once('../utils/funciones/funciones.php');
require_once('Controller/C_simi_web_user.php');
require_once('Controller/C_simi_web_user_permiso.php');
require_once('Controller/C_simi_web_modulos.php');
require_once('../class/sesion_time.php');
require_once("../class/nocsrf.php");

$oSimi_Web_User	=	new Simi_Web_User;
$oMuebles_Menu_Permiso	=	new Muebles_Menu_Permiso;
$oSimi_Modulos	=	new Simi_Modulos;

$S_SIMI_COD_USUARIO = isset($_SESSION['th_SIMI_COD_USUARIO'])?$_SESSION['th_SIMI_COD_USUARIO']:'';
$S_SIMI_NOM_USUARIO = isset($_SESSION['th_SIMI_NOM_USUARIO'])?$_SESSION['th_SIMI_NOM_USUARIO']:'';
$S_SIMI_COD_ENTIDAD = isset($_SESSION['th_SIMI_COD_ENTIDAD'])?$_SESSION['th_SIMI_COD_ENTIDAD']:'';
$supeadmin = isset($_SESSION['th_SIMI_SUPERADMIN'])?$_SESSION['th_SIMI_SUPERADMIN']:'';

// if($S_SIMI_COD_USUARIO=='') 
// try{
  // print_r($S_SIMI_COD_USUARIO); die();
  // NoCSRF::check( 'csrf_token', $_POST, true, 60*10, false );
  if ( !isset( $S_SIMI_COD_USUARIO ) || $S_SIMI_COD_USUARIO == '' ) {
    echo '<script language="javascript">parent.location.href="../sinabip.php";</script>';
  }
// }catch ( Exception $e ){
//   echo '<script language="javascript">alert("../sinabip.php");</script>';
//   echo '<script language="javascript">parent.location.href="../sinabip.php";</script>';
// }


if ( $supeadmin == '1' ){
    
    $resultUserSuperadmin   = $oSimi_Web_User->Ver_Datos_Usuario_SIMI_x_CODIGO_2($S_SIMI_COD_USUARIO);
    if($resultUserSuperadmin){
      $F_COD_ENTIDAD_Superadmin   = utf8_encode(odbc_result($resultUserSuperadmin,"COD_ENTIDAD"));
      $F_RUC_ENTIDAD_Superadmin   = utf8_encode(odbc_result($resultUserSuperadmin,"RUC_ENTIDAD"));
      $F_NOM_ENTIDAD_Superadmin   = utf8_encode(odbc_result($resultUserSuperadmin,"NOM_ENTIDAD"));
    }

    $S_SIMI_COD_USUARIO= $_SESSION['admin_coduser'];
    $S_SIMI_NOM_USUARIO = $_SESSION['admin_nombuser'];
    $S_SIMI_COD_ENTIDAD = $_SESSION['admin_codenti'];
}

$resultUserSimi		=	$oSimi_Web_User->Ver_Datos_Usuario_SIMI_x_CODIGO_2($S_SIMI_COD_USUARIO);

if($resultUserSimi){
		$F_NOM_USUARIO	 	= utf8_encode(odbc_result($resultUserSimi,"NOM_USUARIO"));
		$F_FUNCIONARIO	 	= utf8_encode(odbc_result($resultUserSimi,"FUNCIONARIO"));
		$F_CARGO 			= utf8_encode(odbc_result($resultUserSimi,"CARGO"));
		$F_COD_ENTIDAD		= utf8_encode(odbc_result($resultUserSimi,"COD_ENTIDAD"));
		$F_RUC_ENTIDAD		= utf8_encode(odbc_result($resultUserSimi,"RUC_ENTIDAD"));
		$F_NOM_ENTIDAD		= utf8_encode(odbc_result($resultUserSimi,"NOM_ENTIDAD"));

}

if ( $supeadmin == '1' ){
  $F_COD_ENTIDAD    = $F_COD_ENTIDAD_Superadmin;
  $F_RUC_ENTIDAD    = $F_RUC_ENTIDAD_Superadmin;
  $F_NOM_ENTIDAD    = $F_NOM_ENTIDAD_Superadmin;
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <LINK href="../utils/css/SGI_Estilos.css" rel="stylesheet">
  <!-- <LINK href="../utils/css/font-awesome.css" rel="stylesheet"> -->
  <link rel="stylesheet" href="../assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">

  <script language="javascript" src="../utils/jquery_vs/jquery-ui-1.11.1.custom/external/jquery/jquery.js"></script>
  <script src="../utils/jquery_vs/jquery-ui-1.11.1.custom/jquery-ui.js"></script>
  <link rel="stylesheet" href="../utils/jquery_vs/jquery-ui-1.11.1.custom/jquery-ui.css">
  <script language="javascript" src="../utils/javascript/funcion.js"></script>
  <script language="javascript" type="text/javascript" src="../utils/javascript/ajaxupload.3.5.js"></script>


  <title>SINABIP</title>

  <SCRIPT type="text/javascript">

    $(function() { 	
      //NiDivDisplay('Mflotante');
    });

  </SCRIPT>

  <style type="text/css">
    .menu_arriba span{
      padding: 0 !important;
    }
  </style>
</head>

<body>


<div id="Nav">
  <div id="BarraSup">
    <div id="BarraSupLogoMenus">
      <div style="width: 42%; text-align: left; float: left;">
        <span><strong>MINISTERIO DE ECONOMIA Y FINANZAS </strong></span> 
      </div>
      <div style="width: 57%; text-align: right; float: left;" class="menu_arriba"> <?php if($supeadmin == '1'){ ?>
        <input name="Sistemas" type="hidden" id="Sistemas" value="SINABIP" />
        <input name="txh_url" type="hidden" id="txh_url" value="https://wstest.mineco.gob.pe/SGISBN/System/sinabip.php" size="150" /> 
        <input name="admint" type="hidden" id="admint" value="2" size="150" /> 
        <span><a href="#" onclick="Enviar_Datos_Sinabip_superadmin('-','-','-')"><i class="fa fa-sign-out" aria-hidden="true" style="font-size:18px"></i>Regresar</a></span><?php } ?> 
        <span><a href="sinabip.php"><i class="fa fa-home fa-fw" aria-hidden="true" style="font-size:18px"></i>Página Principal</a></span>&nbsp;&nbsp;&nbsp;|&nbsp; 
        <span><a href="#" onclick="ajaxpage('View/intranet_contendios/cambio_clave_sinabip.php','Cuerpo')" ><i class="fa fa-cog fa-fw" aria-hidden="true" style="font-size:18px"></i> Cambiar clave&nbsp;</a></span>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp; 
        <span><a href="sinabip.php" target="_blank"><i class="fa fa-file-text-o" aria-hidden="true" style="font-size:18px"></i>(+)</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp; 
        <span><a href="#" onclick="cerrar_session('SINABIP','sinabip.php')"><i class="fa fa-sign-out" aria-hidden="true" style="font-size:18px"></i> Cerrar Sesion</a></span> 
      </div>
    </div>
  </div>
</div>
            
<DIV class="SiteBienes">
  <TABLE style="width: 100%;">
  <TR>
    <TD width="1402" height="45">
      <DIV style="width: 100px; float: left; padding:25px 0px 10px 10px;"><a href="sinabip.php">
        
      </DIV>
      <DIV id="div_nom_titulo_mod" class="Titulo_plomo_20px" style="width: 260px; float: left; padding-left:12px; "><h2>SISTEMA WEB</h2></DIV>
      <DIV style="float: left;">
        <table width="500" border="0" cellspacing="0" cellpadding="0">
            <tr>
              <td width="123" rowspan="5" align="center">&nbsp;</td>
              <td align="left">&nbsp;</td>
            </tr>
            <tr>
              <td width="884" align="left"><span class="texto_arial_azul_n_14">Bienvenido: 
                <span class="texto_arial_plomo_n_12">
                <?=$F_FUNCIONARIO?>
                </span>
                <input type="hidden" name="TXH_SIMI_COD_USUARIO" id="TXH_SIMI_COD_USUARIO" value="<?=$S_SIMI_COD_USUARIO?>" />
                <input type="hidden" name="TXH_SIMI_NOM_USUARIO" id="TXH_SIMI_NOM_USUARIO" value="<?=$F_NOM_USUARIO?>" />
                <input type="hidden" name="TXH_SIMI_COD_ENTIDAD" id="TXH_SIMI_COD_ENTIDAD" value="<?=$F_COD_ENTIDAD?>" />
                <input type="hidden" name="TXH_URL_MODULO" id="TXH_URL_MODULO" value="<?=ObtenerURLPage()?>" />
                <input type="hidden" name="TXH_NOM_USUARIO" id="TXH_NOM_USUARIO" value="<?=$F_NOM_USUARIO?>" />
                
              </span></td>
            </tr>
          <tr>
            <td align="left"><span class="texto_arial_plomito_11"><?=$F_NOM_ENTIDAD?>
              </span></td>
          </tr>
          <tr>
            <td align="left"><span class="texto_arial_plomito_11">
              <?=$F_CARGO?>
              </span></td>
          </tr>
          <tr>
            <td align="left">&nbsp;</td>
          </tr>
          </table>
    </DIV>
    </TD>
    <TD width="31">&nbsp;</TD>
  </TR>
</TABLE>
</DIV>

<div><HR class="hr_01" /></div>

<div id="Cuerpo">

<table width="100%" border="0" cellspacing="2" cellpadding="0">
  <tr>
    <td width="50%" valign="top">
      

      
    </td>
    </tr>
  <tr>
    <td valign="top"><table width="1274" border="0" cellspacing="3" cellpadding="0">
      <tr>
        <td width="40" height="30">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td width="85">
          <!-- <img src="../webimages/iconos/stats.png" width="80" height="110" /> -->
        </td>
        <td width="1137">
          
          <div id="DIV_TITULO">
            <div style=" width: 990px; text-align: left; font-family: roboto,Arial; margin-top: 10PX; ">
              <div style="color: rgb(102, 102, 119); font-size: 20px; ">SISTEMA DE INFORMACIÓN NACIONAL DE</div>
              <div style="color: rgb(102, 102, 119); font-size: 50px; ">BIENES PATRIMONIALES - SINABIP WEB</div>
              </div>
            </div>
          
          </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2"><hr /></td>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">
          
          <div id="Div_menus_sinabip"><? include_once('sinabip_menu.php')?></div>
        </td>
        </tr>
      <tr>
        <td height="45">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        </tr>
      <tr>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  </table>
<table width="100%" border="0" cellspacing="3" cellpadding="0">
  <tr>
    <td></td>
  </tr>
</table>

</div>




<div id="DIV_PIE">
<table width="100%" border="0" cellspacing="3" cellpadding="0">
    <tr>
      <td colspan="3"><hr class="hr_01" /></td>
  </tr>
    <tr>
      <td width="40%">&nbsp;</td> 
      <td width="32%"><span class="Pie_centro">Derecho Reservado por la Superintendecia Nacional de Bienes Estatales - 2017 &reg;</span></td>
      <td width="28%">&nbsp;</td>
    </tr>
</table>
</div>


</body>
</html>