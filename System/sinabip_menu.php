<?php //session_start(); 
 
$S_SIMI_COD_USUARIO = $_SESSION['th_SIMI_COD_USUARIO'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link rel="stylesheet" type="text/css" href="../utils/css/style_menu_sinabip.css" />

<div class="container">
  <div class="content">
    <ul class="ca-menu">
  
  <?
    $resulModulo = $oMuebles_Menu_Permiso->Listar_Permisos_Modulos_x_USUARIO($S_SIMI_COD_USUARIO);
    if($resulModulo){
    while (odbc_fetch_row($resulModulo)){
      
      $COD_MUEBLE_MODULO	 	= odbc_result($resulModulo,"COD_MUEBLE_MODULO");
      $DESC_MUEBLE_MODULO	 	= utf8_encode(odbc_result($resulModulo,"DESC_MUEBLE_MODULO"));
      $NOM_ARCHIVO			= utf8_encode(odbc_result($resulModulo,"NOM_ARCHIVO"));

      //$id_modulo      = utf8_encode(odbc_result($resulModulo,"id"));
                    
      $link_modulo = "modulos.php?idm=$COD_MUEBLE_MODULO";

	?>
    <li>
        <a href="sinabip_modulos.php?idm=<?=$COD_MUEBLE_MODULO?>" class="menu_sb" data-id="<?=$COD_MUEBLE_MODULO?>">
            <span class="ca-icon"><img src="../webimages/sinabip_modulos/<?=$NOM_ARCHIVO?>" width="69" height="50" align="absmiddle"></span>
            <div class="ca-content">
                <h2 class="ca-main"><?=$DESC_MUEBLE_MODULO?></h2>
                <h3 class="ca-sub">SINABIP WEB</h3>
            </div>
        </a>
    </li>
  <?
						}
					}
	  ?>
  
    </ul>

    <script type="text/javascript">
          function utf8_to_b64( str ) {
          return window.btoa(unescape(encodeURIComponent( str )));
        }
      $(function(){
        $('.menu_sb').each(function(){
          if($(this).data('id')== "1"){
            var Cod_Entidad = $('#TXH_SIMI_COD_ENTIDAD').val();
            var Cod_Usuario = $('#TXH_SIMI_COD_USUARIO').val();
            var base64 = utf8_to_b64(Cod_Entidad+"-"+Cod_Usuario+"-SINASINA");
            let t = sessionStorage.getItem("token");
            if(t == null){
              sessionStorage.clear();
              localStorage.clear(); 
              alert('No se genero el token correctamente vuelva a iniciar sesión');
              var TXH_NOM_USUARIO	=	$("#TXH_NOM_USUARIO").val();
	            var TXH_URL_MODULO	=	$("#TXH_URL_MODULO").val();
              var URL = "Model/M_simi_web_user.php?operacion=Cerrar_Session_Modulo_Muebles";
              $.post(URL,{ 
                NOM_MODULO:'SINABIP',
                TXH_NOM_USUARIO:TXH_NOM_USUARIO,
                TXH_URL_MODULO:TXH_URL_MODULO
              },function(data){
                if(data==0){
                  location.href='../sinabip.php';
                }
              })
            }
            $(this).attr('href',"http://localhost:4200/acceso/"+t+'__'+base64);
            //$(this).attr('href',"/sinabip/acceso/"+t+'__'+base64);
          }

        });
        // $('.menu_sb').on('click',function(){
        //   if($(this).data('id')== "1"){
        //     var Cod_Entidad = $('#TXH_SIMI_COD_ENTIDAD').val();
        //     var Cod_Usuario = $('#TXH_SIMI_COD_USUARIO').val();
        //     var base64 = utf8_to_b64(Cod_Entidad+"-"+Cod_Usuario+"-SINASINA");
        //     let t = sessionStorage.getItem("token");
        //     window.location.replace("/sinabip/acceso/"+base64);
        //     return false;
        //   }
        // });
      });
    </script>

    </div><!-- content -->
</div>