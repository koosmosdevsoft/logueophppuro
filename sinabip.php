<?php
    session_start();
    include('class/nocsrf.php');
    $token = NoCSRF::generate( 'csrf_token' );
    // $S_SIMI_COD_USUARIO = $_SESSION['th_SIMI_COD_USUARIO'];
    if ( isset( $_SESSION['th_SIMI_COD_USUARIO'] )  ) {
        echo '<script language="javascript">parent.location.href="System/sinabip.php";</script>';
    }
?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Logueo de SINABIP</title>
        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/form-elements.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script> 
        <![endif]-->
        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/favicon.png">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
        <title>SINABIP - M&oacute;dulo Muebles e Inmuebles</title>
        <LINK href="favicon.ico" rel="icon" type="image/x-icon">
        <LINK href="favicon.ico" rel="shortcut icon" type="image/x-icon">
        <script language="javascript" src="utils/jquery_vs/jquery-ui-1.11.1.custom/external/jquery/jquery.js"></script>
        <script src="utils/jquery_vs/jquery-ui-1.11.1.custom/jquery-ui.js"></script>
        <script language="javascript" src="utils/javascript/funcion.js?v=<?php echo time() ?>"></script>
        <LINK href="utils/css/SGI_Estilos.css" rel="stylesheet">
        <link rel="stylesheet" href="utils/jquery_vs/jquery-ui-1.11.1.custom/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="portal_sinabip/css/redes_sociales.css"/>
        <link rel="stylesheet" type="text/css" href="portal_sinabip/css/fondo.css"/>
        <link rel="stylesheet" type="text/css" href="portal_sinabip/css/stylesPaginaWeb.css"/>
        <link rel="stylesheet" type="text/css" href="utils/css/boton_web.css"/>
        <? include_once('utils/funciones/funciones.php') ?>
        <script>
            function inicio(){
                $("#Usuario").focus();
            }
        </script>
    </head>
    <body onLoad="inicio()" oncontextmenu="return false">
         <input name="Sistemas" type="hidden" id="Sistemas" value="SINABIP" />
         <input name="cod_entidad" type="hidden" id="cod_entidad" value="" />
        <!-- Top content -->
        <div class="top-content" style="text-align: center">
            <div class="inner-bg">
                <div class="container">
                    <!-- <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <div class="description">
                            	<p>
                            	</p>
                            </div>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                            <div class="alert alert-danger" style="display:none" id="msg" role="alert">...</div>
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Ingreso al Sinabip</h3>
                            		<p>Ingresa tu usuario y contraseña para acceder:</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-lock"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
                                <div id="div_loading"></div>
			                    <form role="form" action="" method="post" class="login-form" autocomplete="off">
                                    <input type="hidden" name="csrf_token" id="csrf_token" value="<?php echo $token; ?>">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="Usuario">Username</label>
			                        	<input type="text" name="Usuario" placeholder="Usuario..." class="form-username form-control" id="Usuario" onkeydown="if(event.keyCode==13){Enviar_Datos_Sinabip()}" onfocus="this.select();" onkeyup="this.value=this.value.toUpperCase()">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="password">Password</label>
			                        	<input type="password" name="password" placeholder="Contraseña..." class="form-password form-control" id="password" onkeydown="if(event.keyCode==13){Enviar_Datos_Sinabip()}" onfocus="this.select();" onkeyup="this.value=this.value.toUpperCase()">
			                        </div>
			                        <button type="button" class="btn" id="iniciar" onclick="Enviar_Datos_Sinabip()">Iniciar sesión</button>
			                    </form>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         <input name="txh_url" type="hidden" id="txh_url" value="<?=ObtenerURLPage()?>" size="150" /> 
        <!-- Javascript -->
        <script src="assets/js/jquery-1.11.1.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.backstretch.min.js"></script>
        <script src="assets/js/scripts.js"></script>
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->
    </body>
</html>