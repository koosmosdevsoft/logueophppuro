/**************************************************************************************
***************************************************************************************
							Validaci�n de Datos de Ingreso
****************************************************************************************
***************************************************************************************/
document.oncontextmenu = function(){return false}

function validar_solo_numero(){
	if (event.keyCode >= 48 && event.keyCode <= 57) {
	}else{
		event.returnValue = false;	
	}
}

function validar_solo_numero_puntos(){
	if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode == 46)) {
	}else{
		event.returnValue = false;	
	}
}

function validar_solo_texto(){
	if ((event.keyCode >= 65 && event.keyCode <= 90) ||  (event.keyCode >= 95 && event.keyCode <= 122) || ( event.keyCode == 32) || ( event.keyCode == 44) || (event.keyCode >= 192 && event.keyCode <= 255)) {
	}else{
		event.returnValue = false;	
	}
}

function validar_solo_texto_numero(){
	if ((event.keyCode >= 45 && event.keyCode <= 90) ||  (event.keyCode >= 95 && event.keyCode <= 122) || ( event.keyCode == 32)  || (event.keyCode >= 192 && event.keyCode <= 255)) {
	}else{
		event.returnValue = false;	
	}
}

function validar_telefono(){
	if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode == 40) || (event.keyCode == 41) || (event.keyCode == 45) || (event.keyCode == 20)) {
	}else{
		event.returnValue = false;	
	}
}

function validar_numero_guion(){
	if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode == 40) || (event.keyCode == 41) || (event.keyCode == 45) || (event.keyCode == 20)) {
	}else{
		event.returnValue = false;	
	}
}

function valida_mun_decimal(num) { 
	re=/^0[.]{0,1}[0-9]{0,2}$/
    if(!re.exec(num))    {
        alert("El valor " + num + " es incorrecta.");
    }else{
        return true;
    }
} 

function valEmail(valor){
    re=/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/
    if(!re.exec(valor))    {
        alert("La direcci�n de email " + valor + " es incorrecta.");
    }else{
        return true;
    }
}



/**************************************************************************************
***************************************************************************************
							Utilitarios
****************************************************************************************
***************************************************************************************/


function maximaLongitud(texto,maxlong) {
	var tecla, in_value, out_value;

	if (texto.value.length > maxlong) {
		in_value = texto.value;
		out_value = in_value.substring(0,maxlong);
		texto.value = out_value;
		return false;
	}
	return true;
}

function Left(str, n){
	if (n <= 0)
	    return "";
	else if (n > String(str).length)
	    return str;
	else
	    return String(str).substring(0,n);
}

function Right(str, n){
    if (n <= 0)
       return "";
    else if (n > String(str).length)
       return str;
    else {
       var iLen = String(str).length;
       return String(str).substring(iLen, iLen - n);
    }
}

function redondeo2decimales(numero){
	var original=parseFloat(numero);
	var result=Math.round(original*100)/100 ;
	return result;
}

//setTimeout(function(){ $(".mensaje").fadeOut(800).fadeIn(800).fadeOut(500).fadeIn(500).fadeOut(300);}, 3000);
//$("#mensaje").stop().animate({opacity: "show", top: "0"}, "slow");
//$("#mensaje").stop().animate({opacity: "hide", top: "-300"}, 1);
//$("#mensaje").fadeOut(800).fadeIn(800).fadeOut(500).fadeIn(500).fadeOut(300);

function mensagebox(box,contenido){
	$("#"+box+"").animate({left:"+=10px"}).animate({left:"-5000px"});
	$("#"+box+"").html('<center>'+contenido+'</center>');
	$("#"+box+"").fadeOut(800).fadeIn(800).fadeOut(800).fadeIn(800).fadeOut(800);
	$("#"+box+"").stop().animate({opacity: "hide", top: "-300"}, 1000);
}

function mensaje_resultado(div, clase, mensaje){
	//clase='exito'; 	mensaje="Se registro correctamente un registro.";
	//clase='info'; 	mensaje="Se actualizo correctamente un registro.";
	//clase='alerta'; mensaje="Se elimino un registro  correctamente.";
	//clase='error';	mensaje="No se registro correctamente.";
	
	$("#"+div+"").stop().animate({opacity: "show", top: "0"}, "slow");
	$("#"+div+"").removeClass(clase,1000);
	$("#"+div+"").html(mensaje).addClass(clase);
	$("#"+div+"").fadeIn(700).fadeOut(600).fadeIn(700).fadeOut(600);
	$("#"+div+"").stop().animate({opacity: "show", top: "0"}, "slow");
}

function mostrarmensaje(divmsg,tipo,msg){
	//div_mensaje = document.getElementById(divmsg);
	//div_mensaje.innerHTML = "<div class="+tipo+">" + msg + "</div>";
	$("#"+divmsg+"").stop().animate({opacity: "show", top: "0"}, "slow");	
	$("#"+divmsg+"").removeClass("error",1000);
	$("#"+divmsg+"").html(msg).addClass(tipo); 
}

function fcnjquery_AccionMensaje(opt) { 
	var myArray = new Array(2);
	if(opt==1){
		myArray[0]="exito";
		myArray[1] ="Se registro correctamente un registro.";
	}else if($opt==2){
		myArray[0]='info';
		myArray[1]="Se actualizo correctamente un registro.";
	}else if($opt==3){
		myArray[0]='alerta';
		myArray[1]="Se elimino un registro  correctamente.";
	}else if($opt==4){
		myArray[0]='error';
		myArray[1]="Este Registro ya existe en la Base de Datos.";
	}
	return myArray; 
}

//****************************************** validar fecha **************************************//

function esDigito(sChr){
var sCod = sChr.charCodeAt(0);
return ((sCod > 47) && (sCod < 58));
}

function valSep(oTxt){
var bOk = false;
bOk = bOk || ((oTxt.value.charAt(2) == "-") && (oTxt.value.charAt(5) == "-"));
//bOk = bOk || ((oTxt.value.charAt(2) == "/") && (oTxt.value.charAt(5) == "/"));
return bOk;
}

function finMes(oTxt){
var nMes = parseInt(oTxt.value.substr(3, 2), 10);
var nRes = 0;
switch (nMes){
case 1: nRes = 31; break;
case 2: nRes = 29; break;
case 3: nRes = 31; break;
case 4: nRes = 30; break;
case 5: nRes = 31; break;
case 6: nRes = 30; break;
case 7: nRes = 31; break;
case 8: nRes = 31; break;
case 9: nRes = 30; break;
case 10: nRes = 31; break;
case 11: nRes = 30; break;
case 12: nRes = 31; break;
}
return nRes;
}

function valDia(oTxt){
var bOk = false;
var nDia = parseInt(oTxt.value.substr(0, 2), 10);
bOk = bOk || ((nDia >= 1) && (nDia <= finMes(oTxt)));
return bOk;
}

function valMes(oTxt){
var bOk = false;
var nMes = parseInt(oTxt.value.substr(3, 2), 10);
bOk = bOk || ((nMes >= 1) && (nMes <= 12));
return bOk;
}

function valAno(oTxt){
var bOk = true;
var nAno = oTxt.value.substr(6);
bOk = bOk && ((nAno.length == 2) || (nAno.length == 4));
if (bOk){
for (var i = 0; i < nAno.length; i++){
bOk = bOk && esDigito(nAno.charAt(i));
}
}
return bOk;
}

function valFecha(oTxt){
var bOk = true;
if (oTxt.value != ""){
bOk = bOk && (valAno(oTxt));
bOk = bOk && (valMes(oTxt));
bOk = bOk && (valDia(oTxt));
bOk = bOk && (valSep(oTxt));
if (!bOk){
alert("Error. El formato de la fecha es dd-mm-aaaa");
oTxt.value = "";
oTxt.focus();
}
}
}
//****************************************** validar fecha **************************************//


function validar_fechas_horas (fecha1,fecha2) {   
	var result='';
	
	n_fecha1=new Array();
	n_fecha2=new Array();
	
	n_fecha1 = fecha1.split('-');
	nDi0=n_fecha1[0];
	nMe0=n_fecha1[1];
	nAn0=n_fecha1[2];
	nHora0=n_fecha1[3];
	

	n_fecha2 = fecha2.split('-');
	nDi1=n_fecha2[0];
	nMe1=n_fecha2[1];
	nAn1=n_fecha2[2];
	nHora1=n_fecha2[3];

//alert("Fecha_Inicio:"+nAn0+","+nMe0+","+nDi0+","+nHora0+"===Fecha_Fin:"+nAn1+","+nMe1+","+nDi1+","+nHora1);


	var Fecha_Inicio = new Date(nAn0,nMe0,nDi0,nHora0)    
	var Fecha_Fin = new Date(nAn1,nMe1,nDi1,nHora1)     
	if(Fecha_Inicio > Fecha_Fin)    {      
		result=false;
	}else{
		result=true;
	}
	
	return result; 
}   
  
function DiferenciaFechas_horas (CadenaFecha1,CadenaFecha2) {   
	var myArray = new Array(4);
	
	Fecha1_AUX = CadenaFecha1.split("-"); 
	fecha_ini = new Date() 
	fecha_ini.setDate(Fecha1_AUX[0]); 
	fecha_ini.setMonth(eval(Fecha1_AUX[1])-1); 
	fecha_ini.setYear(Fecha1_AUX[2]); 
	fecha_ini.setHours(Fecha1_AUX[3]); 
	fecha_ini.setMinutes(0); 
	fecha_ini.setSeconds(0); 

	Fecha2_AUX = CadenaFecha2.split("-"); 
	fecha_fin = new Date() 
	fecha_fin.setDate(Fecha2_AUX[0]); 
	fecha_fin.setMonth(eval(Fecha2_AUX[1])-1); 
	fecha_fin.setYear(Fecha2_AUX[2]); 
	fecha_fin.setHours(Fecha2_AUX[3]); 
	fecha_fin.setMinutes(0); 
	fecha_fin.setSeconds(0); 

	num_dias = Math.floor((fecha_fin.getTime()/(1000 * 60 * 60 * 24))-(fecha_ini.getTime()/(1000 * 60 * 60 * 24))); 
	num_horas = Math.floor((fecha_fin.getTime()/(1000 * 60 * 60))-(fecha_ini.getTime()/(1000 * 60 * 60)));
	num_minutos = (fecha_fin.getTime()/(1000 * 60))-(fecha_ini.getTime()/(1000 * 60)); 
	num_segundos = (fecha_fin.getTime()/(1000))-(fecha_ini.getTime()/(1000)); 

    myArray[0]	=	num_dias;
	myArray[1] 	=	num_horas;
	myArray[2] 	=	num_minutos;
	myArray[3] 	=	num_segundos;
		
	return myArray; 
} 

function imprimir(que) {
	var ventana = window.open("../funciones/print.php","ventana","width=760");
	var contenido = "<html><head><style type='text/css'> @import url('../../../utils/css/stylesWebContenido.css');</style></head><body onload='window.print();window.close();'>" + document.getElementById(que).innerHTML + "</body></html>";
	ventana.document.open();
	ventana.document.write(contenido);
	ventana.document.close();
}

/*****************************   VALIDAR FECHA MEJORADO   ***********************************/

function Validar_fecha_new(obj_fecha){ 

    var Fecha= new String(obj_fecha.value)   // Crea un string   
    var RealFecha= new Date()   // Para sacar la fecha de hoy   
    // Cadena A�o   
    var Ano= new String(Fecha.substring(Fecha.lastIndexOf("-")+1,Fecha.length))   
    // Cadena Mes   
    var Mes= new String(Fecha.substring(Fecha.indexOf("-")+1,Fecha.lastIndexOf("-")))   
    // Cadena D�a   
    var Dia= new String(Fecha.substring(0,Fecha.indexOf("-")))   
  
    // Valido el a�o   
    if (isNaN(Ano) || Ano.length<4 || parseFloat(Ano)<1900){   
		alert('A\xf1o Inv\xe1lida')   
        obj_fecha.value = "";
		obj_fecha.focus();
        //return false     
    }   
    // Valido el Mes   
    if (isNaN(Mes) || parseFloat(Mes)<1 || parseFloat(Mes)>12){   
      	alert('Mes inv\xe1lida') 
        obj_fecha.value = "";
		obj_fecha.focus();
        //return false     
    }   
    // Valido el Dia   
    if (isNaN(Dia) || parseInt(Dia, 10)<1 || parseInt(Dia, 10)>31){   
     	alert('D\xeda inv\xe1lida') 
		obj_fecha.value = "";
		obj_fecha.focus();
        //return false   
    }   
    if (Mes==4 || Mes==6 || Mes==9 || Mes==11 || Mes==2) {   
        if (Mes==2 && Dia > 28 || Dia>30) {   
           alert('D\xeda inv\xe1lida') 
            obj_fecha.value = "";
			obj_fecha.focus();
			//return false    
        }   
    }   
 
  //para que envie los datos, quitar las  2 lineas siguientes   
  //alert("Fecha correcta.")   
 //return false     
}  

function checkTodos (id,pID) {
				
 $( "#" + pID + " :checkbox").attr('checked', $('#' + id).is(':checked')); 
				   
}
/*
function caracter(valor){
	var nRes = '';
	switch (valor){
		case "�": nRes = "e1"; break;
		case "�": nRes = "e9"; break;
		case "�": nRes = "ed"; break;
		case "�": nRes = "f3"; break;
		case "�": nRes = "fa"; break;
		case "�": nRes = "c1"; break;
		case "�": nRes = "c9"; break;
		case "�": nRes = "cd"; break;
		case "�": nRes = "d3"; break;
		case "�": nRes = "da"; break;
		case "�": nRes = "b0"; break;
	}
	n_result="\x"+nRes;
}
*/
/************************************************/


function div_mostrar(div){
	$("#"+div+"").show();
}

function div_ocultar(div){
	$("#"+div+"").hide();
}

function div_limpiar(div){
	$("#"+div+"").val('');
}

/**************************************************************************************
***************************************************************************************
							Mostrar Cargas Loading
****************************************************************************************
***************************************************************************************/

function cargar_loading(div_id){
	$("#"+div_id+"").html('<img src="../webimages/iconos/loading_save.gif" />');
}

function cargar_loading_barra(div_id){
	$("#"+div_id+"").html('<img src="../webimages/iconos/loading_time.gif" />');
}


function mostrarCargaLogin(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="99%" border="0" align="center" cellpadding="0" cellspacing="0" class="TABLE_border1" ><tr><td height="40" bgcolor="#FFFFFF"><div align="center"><table width="162" border="0" cellspacing="0" cellpadding="0"><tr><td width="54"><div align="center"><img src="webimages/iconos/load.gif" width="32" height="32"></div></td><td width="99" class="texto_arial_azul_12">'+contenido+' </td></tr></table></div></td></tr></table>');
}

function mostrarCargaContenido(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#004669"><tr><td>&nbsp;</td></tr><tr><td bgcolor="#FFFFFF" class="TABLE_border4"><center><p><img src="../webimages/iconos/ajax-loader.gif" width="125" height="123" /><br />&nbsp;&nbsp;&nbsp;<span class="arial_13_black_negrita">'+contenido+' </span><img src="../webimages/iconos/loadingpuntos01.gif" width="18" height="7" /><br></p></center></td></tr><tr><td>&nbsp;</td></tr></table>');
}

function mostrarCargaContenido_externo(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#004669"><tr><td>&nbsp;</td></tr><tr><td bgcolor="#FFFFFF" class="TABLE_border4"><center><p><img src="Administrator/webimages/iconos/ajax-loader.gif" width="125" height="123" /><br />&nbsp;&nbsp;&nbsp;<span class="arial_13_black_negrita">'+contenido+' </span><img src="Administrator/webimages/iconos/loadingpuntos01.gif" width="18" height="7" /><br></p></center></td></tr><tr><td>&nbsp;</td></tr></table>');
}

function mostrarCargaContenido_externo_2(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#004669"><tr><td>&nbsp;</td></tr><tr><td bgcolor="#FFFFFF" class="TABLE_border4"><center><p><img src="../Administrator/webimages/iconos/ajax-loader.gif" width="125" height="123" /><br />&nbsp;&nbsp;&nbsp;<span class="arial_13_black_negrita">'+contenido+' </span><img src="../Administrator/webimages/iconos/loadingpuntos01.gif" width="18" height="7" /><br></p></center></td></tr><tr><td>&nbsp;</td></tr></table>');
}

function mostrarCargaContenido_externo_3(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="98%" border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#004669"><tr><td>&nbsp;</td></tr><tr><td bgcolor="#FFFFFF" class="TABLE_border4"><center><p><img src="../../../webimages/iconos/ajax-loader.gif" width="125" height="123" /><br />&nbsp;&nbsp;&nbsp;<span class="arial_13_black_negrita">'+contenido+' </span><img src="../../../webimages/iconos/loadingpuntos01.gif" width="18" height="7" /><br></p></center></td></tr><tr><td>&nbsp;</td></tr></table>');
}
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/
/*************************************************************************************/


function mostrarCarga02(div_mensaje){
	$("#"+div_mensaje+"").html('<center><table width="95%" border="1" bordercolor="#004669"><tr><td bgcolor="#FFFFFF"><center><img src="../imagesweb/ajax-loader.gif" width="125" height="123" /></center></td></tr></table></center>');
}



function mostrarCarga04(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<center><table width="95%" border="1" bordercolor="#004669"><tr><td bgcolor="#FFFFFF"><center><img src="../imagesweb/ajax-loader.gif" width="125" height="123" /><br>'+contenido+'<br><br></center></td></tr></table></center><br>');
}

function mostrarCarga05(div_mensaje,contenido){
	$("#"+div_mensaje+"").html('<table width="96%" border="0" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" class="table_Listing2" align="center"><tr><td height="130"><div align="center"><img src="Administrator/imagesweb/ajax-loader.gif" width="125" height="123" /></div></td></tr><tr><td height="30"><div align="center"><font color="#333333">'+contenido+'</font> <img src="Administrator/imagesweb/loadingpuntos.gif" width="18" height="7" /></div></td></tr></table>');
}



function mostrarCarga(div_mensaje){
	$("#"+div_mensaje+"").html('<center><img src="../../../imagesweb/ajax-loader.gif" width="125" height="123" /></center>');
}	
	

function mostrarPequenio(div_mensaje){
	$("#"+div_mensaje+"").animate({left:"+=10px"}).animate({left:"-5000px"});
	$("#"+div_mensaje+"").html('<div align="left">&nbsp;&nbsp;<img src="../imagesweb/load.gif" width="18" height="18" /></div>');
	$("#"+div_mensaje+"").stop().animate({opacity: "hide", top: "-300"}, 1000);
}

